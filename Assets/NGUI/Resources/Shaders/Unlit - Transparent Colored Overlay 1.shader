Shader "Hidden/Unlit/Transparent Colored Overlay 1"
 {
     Properties 
     {
         _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     }
     
     SubShader
     {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
         ZWrite Off Lighting Off Cull Off Fog { Mode Off } Blend DstColor SrcColor
         LOD 110
		 Offset -1, -1
         
         Pass 
         {
             CGPROGRAM
             #pragma vertex vert_vct
             #pragma fragment frag_mult 
             #pragma fragmentoption ARB_precision_hint_fastest
             #include "UnityCG.cginc"
 
             sampler2D _MainTex;
             float4 _MainTex_ST;
			 float4 _ClipRange0 = float4(0.0, 0.0, 1.0, 1.0);
			 float2 _ClipArgs0 = float2(1000.0, 1000.0);
 
             struct vin_vct 
             {
                 float4 vertex : POSITION;
                 float4 color : COLOR;
                 float2 texcoord : TEXCOORD0;
             };
 
             struct v2f_vct
             {
                 float4 vertex : POSITION;
                 fixed4 color : COLOR;
                 half2 texcoord : TEXCOORD0;
				 float2 worldPos : TEXCOORD1;
             };
 
             v2f_vct vert_vct(vin_vct v)
             {
                 v2f_vct o;
                 o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                 o.color = v.color;
                 o.texcoord = v.texcoord;
				 o.worldPos = v.vertex.xy * _ClipRange0.zw + _ClipRange0.xy;
                 return o;
             }
 
             float4 frag_mult(v2f_vct i) : COLOR
             {
			     // Softness factor
				 float2 factor = (float2(1.0, 1.0) - abs(i.worldPos)) * _ClipArgs0;

                 float4 tex = tex2D(_MainTex, i.texcoord);
                 
                 float4 final;                
                 final.rgb = i.color.rgb * tex.rgb * 1;
                 final.a = i.color.a * tex.a;
				 final.a *= clamp( min(factor.x, factor.y), 0.0, 1.0);
                 return lerp(float4(0.5f,0.5f,0.5f,0.5f), final, final.a);
                 
             }
 
             ENDCG
             
         }
         
           
     }
  
 }
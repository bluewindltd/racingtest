Shader "Hidden/Unlit/Transparent Colored Overlay 3"
 {
     Properties 
     {
         _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     }
     
     SubShader
     {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
         ZWrite Off Lighting Off Cull Off Fog { Mode Off } Blend DstColor SrcColor
         LOD 110
		 Offset -1, -1
         
         Pass 
         {
             CGPROGRAM
             #pragma vertex vert_vct
             #pragma fragment frag_mult 
             #pragma fragmentoption ARB_precision_hint_fastest
             #include "UnityCG.cginc"
 
             sampler2D _MainTex;
             float4 _MainTex_ST;
			 float4 _ClipRange0 = float4(0.0, 0.0, 1.0, 1.0);
			 float4 _ClipArgs0 = float4(1000.0, 1000.0, 0.0, 1.0);
			 float4 _ClipRange1 = float4(0.0, 0.0, 1.0, 1.0);
			 float4 _ClipArgs1 = float4(1000.0, 1000.0, 0.0, 1.0);
			 float4 _ClipRange2 = float4(0.0, 0.0, 1.0, 1.0);
			 float4 _ClipArgs2 = float4(1000.0, 1000.0, 0.0, 1.0);
 
             struct vin_vct 
             {
                 float4 vertex : POSITION;
                 float4 color : COLOR;
                 float2 texcoord : TEXCOORD0;
             };
 
             struct v2f_vct
             {
                 float4 vertex : POSITION;
                 fixed4 color : COLOR;
                 half2 texcoord : TEXCOORD0;
				 float4 worldPos : TEXCOORD1;
				 float2 worldPos2 : TEXCOORD2;
             };
 
             float2 Rotate (float2 v, float2 rot)
			 {
				 float2 ret;
				 ret.x = v.x * rot.y - v.y * rot.x;
				 ret.y = v.x * rot.x + v.y * rot.y;
				 return ret;
			 } 

             v2f_vct vert_vct(vin_vct v)
             {
                 v2f_vct o;
                 o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                 o.color = v.color;
                 o.texcoord = v.texcoord;
				 o.worldPos.xy = v.vertex.xy * _ClipRange0.zw + _ClipRange0.xy;
				 o.worldPos.zw = Rotate(v.vertex.xy, _ClipArgs1.zw) * _ClipRange1.zw + _ClipRange1.xy;
				 o.worldPos2 = Rotate(v.vertex.xy, _ClipArgs2.zw) * _ClipRange2.zw + _ClipRange2.xy;
                 return o;
             }
 
             float4 frag_mult(v2f_vct i) : COLOR
             {
			     // First clip region
				 float2 factor = (float2(1.0, 1.0) - abs(i.worldPos.xy)) * _ClipArgs0.xy;
				 float f = min(factor.x, factor.y);
				 
				 // Second clip region
				 factor = (float2(1.0, 1.0) - abs(i.worldPos.zw)) * _ClipArgs1.xy;
				 f = min(f, min(factor.x, factor.y));
				 
				 // Third clip region
				 factor = (float2(1.0, 1.0) - abs(i.worldPos2)) * _ClipArgs2.xy;
				 f = min(f, min(factor.x, factor.y));

                 float4 tex = tex2D(_MainTex, i.texcoord);
                 
                 float4 final;                
                 final.rgb = i.color.rgb * tex.rgb * 1;
                 final.a = i.color.a * tex.a;
				 final.a *= clamp(f, 0.0, 1.0);
                 return lerp(float4(0.5f,0.5f,0.5f,0.5f), final, final.a);
                 
             }
 
             ENDCG
             
         }
         
           
     }
  
 }
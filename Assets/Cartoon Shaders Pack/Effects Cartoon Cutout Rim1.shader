// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:7940,x:32719,y:32712,varname:node_7940,prsc:2|diff-5529-OUT,diffpow-1719-OUT,emission-1784-OUT,clip-4337-A;n:type:ShaderForge.SFN_TexCoord,id:1320,x:31817,y:33561,varname:node_1320,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:795,x:32078,y:33387,varname:node_795,prsc:2,uv:0;n:type:ShaderForge.SFN_Fresnel,id:1009,x:31790,y:32565,varname:node_1009,prsc:2|EXP-9276-OUT;n:type:ShaderForge.SFN_Slider,id:9228,x:31005,y:32539,ptovrint:False,ptlb:Shadow Edges,ptin:_ShadowEdges,varname:node_9228,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:9276,x:31563,y:32565,varname:node_9276,prsc:2|A-139-OUT,B-8412-OUT,T-9228-OUT;n:type:ShaderForge.SFN_Vector1,id:139,x:31146,y:32281,varname:node_139,prsc:2,v1:5;n:type:ShaderForge.SFN_Vector1,id:8412,x:31146,y:32385,varname:node_8412,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:4876,x:32207,y:32507,varname:node_4876,prsc:2|A-6764-RGB,B-4261-OUT,C-4261-OUT;n:type:ShaderForge.SFN_Add,id:4261,x:31984,y:32528,varname:node_4261,prsc:2|A-1009-OUT,B-1009-OUT;n:type:ShaderForge.SFN_OneMinus,id:1719,x:32381,y:32497,varname:node_1719,prsc:2|IN-4876-OUT;n:type:ShaderForge.SFN_Color,id:6764,x:31898,y:32197,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6764,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5962,x:32736,y:32279,varname:node_5962,prsc:2|A-6764-RGB,B-2137-OUT;n:type:ShaderForge.SFN_Multiply,id:1784,x:32389,y:32853,varname:node_1784,prsc:2|A-6764-RGB,B-8368-OUT,C-5529-OUT;n:type:ShaderForge.SFN_Slider,id:8368,x:31965,y:33044,ptovrint:False,ptlb:Emissive Intensity,ptin:_EmissiveIntensity,varname:node_8368,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:2;n:type:ShaderForge.SFN_Desaturate,id:2137,x:32539,y:32345,varname:node_2137,prsc:2|COL-1719-OUT;n:type:ShaderForge.SFN_Tex2d,id:4337,x:32218,y:31977,ptovrint:False,ptlb:Diffuse (Cutout A),ptin:_DiffuseCutoutA,varname:node_4337,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:5529,x:32920,y:32161,varname:node_5529,prsc:2|A-4337-RGB,B-5962-OUT;proporder:6764-9228-8368-4337;pass:END;sub:END;*/

Shader "Ciconia Studio/Effects/Cartoon/Cutout/Rim1" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _ShadowEdges ("Shadow Edges", Range(0, 1)) = 1
        _EmissiveIntensity ("Emissive Intensity", Range(0, 2)) = 0.5
        _DiffuseCutoutA ("Diffuse (Cutout A)", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _ShadowEdges;
            uniform float4 _Color;
            uniform float _EmissiveIntensity;
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float node_1009 = pow(1.0-max(0,dot(normalDirection, viewDirection)),lerp(5.0,1.0,_ShadowEdges));
                float node_4261 = (node_1009+node_1009);
                float3 node_1719 = (1.0 - (_Color.rgb*node_4261*node_4261));
                float3 directDiffuse = pow(max( 0.0, NdotL), node_1719) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 node_5529 = (_DiffuseCutoutA_var.rgb*(_Color.rgb*dot(node_1719,float3(0.3,0.59,0.11))));
                float3 diffuseColor = node_5529;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_Color.rgb*_EmissiveIntensity*node_5529);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _ShadowEdges;
            uniform float4 _Color;
            uniform float _EmissiveIntensity;
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float node_1009 = pow(1.0-max(0,dot(normalDirection, viewDirection)),lerp(5.0,1.0,_ShadowEdges));
                float node_4261 = (node_1009+node_1009);
                float3 node_1719 = (1.0 - (_Color.rgb*node_4261*node_4261));
                float3 directDiffuse = pow(max( 0.0, NdotL), node_1719) * attenColor;
                float3 node_5529 = (_DiffuseCutoutA_var.rgb*(_Color.rgb*dot(node_1719,float3(0.3,0.59,0.11))));
                float3 diffuseColor = node_5529;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

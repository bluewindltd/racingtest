// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:7940,x:32719,y:32712,varname:node_7940,prsc:2|diff-6064-OUT,emission-5166-OUT,clip-4863-A;n:type:ShaderForge.SFN_TexCoord,id:1320,x:31817,y:33561,varname:node_1320,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:795,x:32037,y:33506,varname:node_795,prsc:2,uv:0;n:type:ShaderForge.SFN_Fresnel,id:1009,x:31833,y:32553,varname:node_1009,prsc:2|EXP-9276-OUT;n:type:ShaderForge.SFN_Slider,id:9228,x:31011,y:32575,ptovrint:False,ptlb:Shadow Distance,ptin:_ShadowDistance,varname:node_9228,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Lerp,id:9276,x:31600,y:32684,varname:node_9276,prsc:2|A-8412-OUT,B-139-OUT,T-9228-OUT;n:type:ShaderForge.SFN_Vector1,id:139,x:31146,y:32457,varname:node_139,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:8412,x:31146,y:32385,varname:node_8412,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:6764,x:31953,y:32349,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6764,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Posterize,id:5039,x:32056,y:32597,varname:node_5039,prsc:2|IN-1009-OUT,STPS-3401-OUT;n:type:ShaderForge.SFN_Vector1,id:3401,x:31737,y:32828,varname:node_3401,prsc:2,v1:5;n:type:ShaderForge.SFN_Tex2d,id:4863,x:31948,y:31904,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_4863,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:8438,x:32281,y:32440,varname:node_8438,prsc:2|A-4863-RGB,B-6764-RGB;n:type:ShaderForge.SFN_Add,id:6064,x:32517,y:32612,varname:node_6064,prsc:2|A-8438-OUT,B-5460-OUT;n:type:ShaderForge.SFN_Color,id:6380,x:32056,y:32761,ptovrint:False,ptlb:Shadow Tint,ptin:_ShadowTint,varname:node_6380,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5460,x:32259,y:32770,varname:node_5460,prsc:2|A-5039-OUT,B-6380-RGB,C-7271-OUT;n:type:ShaderForge.SFN_Slider,id:1650,x:32231,y:33215,ptovrint:False,ptlb:Emissive Intensity,ptin:_EmissiveIntensity,varname:node_1650,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_Multiply,id:5166,x:32493,y:32944,varname:node_5166,prsc:2|A-8438-OUT,B-1650-OUT;n:type:ShaderForge.SFN_Lerp,id:7271,x:32056,y:32962,varname:node_7271,prsc:2|A-7539-OUT,B-9247-OUT,T-7258-OUT;n:type:ShaderForge.SFN_Slider,id:7258,x:31683,y:33105,ptovrint:False,ptlb:Shadow Intensity,ptin:_ShadowIntensity,varname:node_3704,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_Vector1,id:9247,x:31808,y:32996,varname:node_9247,prsc:2,v1:5;n:type:ShaderForge.SFN_Vector1,id:7539,x:31840,y:32924,varname:node_7539,prsc:2,v1:0;proporder:6764-6380-7258-9228-4863-1650;pass:END;sub:END;*/

Shader "Ciconia Studio/Effects/Cartoon/Cutout/Rim3" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _ShadowTint ("Shadow Tint", Color) = (1,1,1,1)
        _ShadowIntensity ("Shadow Intensity", Range(0, 1)) = 0.2
        _ShadowDistance ("Shadow Distance", Range(0, 1)) = 0.5
        _Diffuse ("Diffuse", 2D) = "white" {}
        _EmissiveIntensity ("Emissive Intensity", Range(0, 1)) = 0.2
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _ShadowDistance;
            uniform float4 _Color;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _ShadowTint;
            uniform float _EmissiveIntensity;
            uniform float _ShadowIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                clip(_Diffuse_var.a - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 node_8438 = (_Diffuse_var.rgb*_Color.rgb);
                float node_3401 = 5.0;
                float3 diffuseColor = (node_8438+(floor(pow(1.0-max(0,dot(normalDirection, viewDirection)),lerp(5.0,0.0,_ShadowDistance)) * node_3401) / (node_3401 - 1)*_ShadowTint.rgb*lerp(0.0,5.0,_ShadowIntensity)));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (node_8438*_EmissiveIntensity);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _ShadowDistance;
            uniform float4 _Color;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _ShadowTint;
            uniform float _EmissiveIntensity;
            uniform float _ShadowIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                clip(_Diffuse_var.a - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 node_8438 = (_Diffuse_var.rgb*_Color.rgb);
                float node_3401 = 5.0;
                float3 diffuseColor = (node_8438+(floor(pow(1.0-max(0,dot(normalDirection, viewDirection)),lerp(5.0,0.0,_ShadowDistance)) * node_3401) / (node_3401 - 1)*_ShadowTint.rgb*lerp(0.0,5.0,_ShadowIntensity)));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                clip(_Diffuse_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:7940,x:32719,y:32712,varname:node_7940,prsc:2|emission-7276-OUT,clip-4255-A,olwid-1995-OUT,olcol-252-RGB;n:type:ShaderForge.SFN_TexCoord,id:1320,x:31520,y:33831,varname:node_1320,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:795,x:31545,y:34101,varname:node_795,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:4255,x:32083,y:32419,ptovrint:False,ptlb:Diffuse (Cutout A),ptin:_DiffuseCutoutA,varname:node_4255,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:1825,x:32083,y:32578,varname:node_1825,prsc:2|A-937-OUT,B-6849-OUT,T-3348-OUT;n:type:ShaderForge.SFN_Vector1,id:937,x:31817,y:32436,varname:node_937,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:6849,x:31817,y:32495,varname:node_6849,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Slider,id:3348,x:31772,y:32668,ptovrint:False,ptlb:Emissive Intensity,ptin:_EmissiveIntensity,varname:node_3348,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8,max:1;n:type:ShaderForge.SFN_Slider,id:4360,x:31858,y:33153,ptovrint:False,ptlb:Oultine Width,ptin:_OultineWidth,varname:node_4360,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8,max:1;n:type:ShaderForge.SFN_Vector1,id:8335,x:32000,y:33000,varname:node_8335,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:2925,x:32000,y:33065,varname:node_2925,prsc:2,v1:0.05;n:type:ShaderForge.SFN_Lerp,id:1995,x:32355,y:33041,varname:node_1995,prsc:2|A-8335-OUT,B-2925-OUT,T-4360-OUT;n:type:ShaderForge.SFN_Color,id:252,x:32355,y:33214,ptovrint:False,ptlb:Outline Color,ptin:_OutlineColor,varname:node_252,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:7276,x:32326,y:32700,varname:node_7276,prsc:2|A-4255-RGB,B-1825-OUT;proporder:3348-4255-4360-252;pass:END;sub:END;*/

Shader "Ciconia Studio/Effects/Cartoon/Cutout/Simple Diffuse" {
    Properties {
        _EmissiveIntensity ("Emissive Intensity", Range(0, 1)) = 0.8
        _DiffuseCutoutA ("Diffuse (Cutout A)", 2D) = "white" {}
        _OultineWidth ("Oultine Width", Range(0, 1)) = 0.8
        _OutlineColor ("Outline Color", Color) = (0,0,0,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            uniform float _OultineWidth;
            uniform float4 _OutlineColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*lerp(0.0,0.05,_OultineWidth),1));
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                return fixed4(_OutlineColor.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            uniform float _EmissiveIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = (_DiffuseCutoutA_var.rgb*lerp(0.0,1.5,_EmissiveIntensity));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

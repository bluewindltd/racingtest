// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:7940,x:32719,y:32712,varname:node_7940,prsc:2|diff-7545-OUT,spec-4257-OUT,gloss-4257-OUT,normal-4786-OUT,emission-6874-OUT,lwrap-7545-OUT,clip-4255-A,olwid-1995-OUT,olcol-252-RGB;n:type:ShaderForge.SFN_Tex2d,id:3456,x:31850,y:33018,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_3456,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_TexCoord,id:1320,x:31520,y:33831,varname:node_1320,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:795,x:31545,y:34101,varname:node_795,prsc:2,uv:0;n:type:ShaderForge.SFN_Fresnel,id:1009,x:31561,y:32364,varname:node_1009,prsc:2|EXP-6967-OUT;n:type:ShaderForge.SFN_Vector1,id:6638,x:30985,y:32198,varname:node_6638,prsc:2,v1:10;n:type:ShaderForge.SFN_Vector1,id:2769,x:30985,y:32249,varname:node_2769,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:6967,x:31311,y:32047,varname:node_6967,prsc:2|A-6638-OUT,B-2769-OUT,T-7021-OUT;n:type:ShaderForge.SFN_Slider,id:7021,x:30885,y:32367,ptovrint:False,ptlb:Shadows Edges,ptin:_ShadowsEdges,varname:node_7021,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Tex2d,id:4255,x:31948,y:32186,ptovrint:False,ptlb:Diffuse (Cutout A),ptin:_DiffuseCutoutA,varname:node_4255,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3256,x:31923,y:32553,varname:node_3256,prsc:2|A-1009-OUT,B-1009-OUT,C-1009-OUT;n:type:ShaderForge.SFN_Subtract,id:7545,x:32369,y:32467,varname:node_7545,prsc:2|A-1527-OUT,B-520-OUT;n:type:ShaderForge.SFN_Color,id:1773,x:31948,y:32374,ptovrint:False,ptlb:Diffuse Color,ptin:_DiffuseColor,varname:node_1773,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:1527,x:32229,y:32342,varname:node_1527,prsc:2|A-4255-RGB,B-1773-RGB;n:type:ShaderForge.SFN_Vector3,id:3264,x:32046,y:33000,varname:node_3264,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_Lerp,id:4786,x:32268,y:33000,varname:node_4786,prsc:2|A-3264-OUT,B-3456-RGB,T-2294-OUT;n:type:ShaderForge.SFN_Slider,id:2294,x:31939,y:33231,ptovrint:False,ptlb:Normal Intensity,ptin:_NormalIntensity,varname:node_2294,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:1825,x:32766,y:32207,varname:node_1825,prsc:2|A-937-OUT,B-6849-OUT,T-3348-OUT;n:type:ShaderForge.SFN_Vector1,id:937,x:32560,y:32207,varname:node_937,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:6849,x:32560,y:32266,varname:node_6849,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Slider,id:3348,x:32601,y:32403,ptovrint:False,ptlb:Emissive Intensity,ptin:_EmissiveIntensity,varname:node_3348,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:6874,x:32807,y:32071,varname:node_6874,prsc:2|A-4255-RGB,B-1825-OUT;n:type:ShaderForge.SFN_Add,id:9313,x:31936,y:32678,varname:node_9313,prsc:2|A-1009-OUT,B-1009-OUT,C-1009-OUT;n:type:ShaderForge.SFN_Multiply,id:520,x:32107,y:32553,varname:node_520,prsc:2|A-3256-OUT,B-9313-OUT;n:type:ShaderForge.SFN_Slider,id:4360,x:31860,y:33569,ptovrint:False,ptlb:Oultine Width,ptin:_OultineWidth,varname:node_4360,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Vector1,id:8335,x:32002,y:33416,varname:node_8335,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:2925,x:32002,y:33481,varname:node_2925,prsc:2,v1:0.05;n:type:ShaderForge.SFN_Lerp,id:1995,x:32422,y:33258,varname:node_1995,prsc:2|A-8335-OUT,B-2925-OUT,T-4360-OUT;n:type:ShaderForge.SFN_Color,id:252,x:32522,y:33413,ptovrint:False,ptlb:OutLine Color,ptin:_OutLineColor,varname:node_252,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Slider,id:402,x:32022,y:32883,ptovrint:False,ptlb:Specular Glow,ptin:_SpecularGlow,varname:node_402,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:4257,x:32404,y:32766,varname:node_4257,prsc:2|A-7950-OUT,B-6117-OUT,T-402-OUT;n:type:ShaderForge.SFN_Vector1,id:7950,x:32179,y:32743,varname:node_7950,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:6117,x:32179,y:32810,varname:node_6117,prsc:2,v1:1;proporder:1773-3348-7021-4255-402-3456-2294-4360-252;pass:END;sub:END;*/

Shader "Ciconia Studio/Effects/Cartoon/Cutout/Advanced" {
    Properties {
        _DiffuseColor ("Diffuse Color", Color) = (1,1,1,1)
        _EmissiveIntensity ("Emissive Intensity", Range(0, 1)) = 0
        _ShadowsEdges ("Shadows Edges", Range(0, 1)) = 1
        _DiffuseCutoutA ("Diffuse (Cutout A)", 2D) = "white" {}
        _SpecularGlow ("Specular Glow", Range(0, 1)) = 0
        _Normal ("Normal", 2D) = "bump" {}
        _NormalIntensity ("Normal Intensity", Range(0, 1)) = 1
        _OultineWidth ("Oultine Width", Range(0, 1)) = 0
        _OutLineColor ("OutLine Color", Color) = (0,0,0,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            uniform float _OultineWidth;
            uniform float4 _OutLineColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*lerp(0.0,0.05,_OultineWidth),1));
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                return fixed4(_OutLineColor.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _ShadowsEdges;
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            uniform float4 _DiffuseColor;
            uniform float _NormalIntensity;
            uniform float _EmissiveIntensity;
            uniform float _SpecularGlow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = lerp(float3(0,0,1),_Normal_var.rgb,_NormalIntensity);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float node_4257 = lerp(0.0,1.0,_SpecularGlow);
                float gloss = node_4257;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = float3(node_4257,node_4257,node_4257);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float node_1009 = pow(1.0-max(0,dot(normalDirection, viewDirection)),lerp(10.0,1.0,_ShadowsEdges));
                float3 node_7545 = ((_DiffuseCutoutA_var.rgb*_DiffuseColor.rgb)-((node_1009*node_1009*node_1009)*(node_1009+node_1009+node_1009)));
                float3 w = node_7545*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = forwardLight * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = node_7545;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_DiffuseCutoutA_var.rgb*lerp(0.0,0.5,_EmissiveIntensity));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _ShadowsEdges;
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            uniform float4 _DiffuseColor;
            uniform float _NormalIntensity;
            uniform float _EmissiveIntensity;
            uniform float _SpecularGlow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = lerp(float3(0,0,1),_Normal_var.rgb,_NormalIntensity);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float node_4257 = lerp(0.0,1.0,_SpecularGlow);
                float gloss = node_4257;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = float3(node_4257,node_4257,node_4257);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float node_1009 = pow(1.0-max(0,dot(normalDirection, viewDirection)),lerp(10.0,1.0,_ShadowsEdges));
                float3 node_7545 = ((_DiffuseCutoutA_var.rgb*_DiffuseColor.rgb)-((node_1009*node_1009*node_1009)*(node_1009+node_1009+node_1009)));
                float3 w = node_7545*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = forwardLight * attenColor;
                float3 diffuseColor = node_7545;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _DiffuseCutoutA; uniform float4 _DiffuseCutoutA_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _DiffuseCutoutA_var = tex2D(_DiffuseCutoutA,TRANSFORM_TEX(i.uv0, _DiffuseCutoutA));
                clip(_DiffuseCutoutA_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

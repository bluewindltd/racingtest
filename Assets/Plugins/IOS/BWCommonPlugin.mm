//
//  BWCommonPlugin.mm
//  
//
//  Created by 김건훈 on 2015. 9. 23..
//
//

#import "BWCommonPlugin.h"

@implementation BWCommonManager

NSString *const UNITY_GAME_OBJECT_NAME = @"BWCommonPlugin";

static BWCommonManager *_sharedInstance;


+ (id)sharedInstance {
    
    if (_sharedInstance == nil)  {
        _sharedInstance = [[self alloc] init];
    }
    
    return _sharedInstance;
}

-(void) getDisplayCountry {
    NSLocale *locale = [NSLocale currentLocale];
    //NSString *displayCountry = [locale displayNameForKey:NSLocaleIdentifier
    //                                               value:[locale localeIdentifier]];
    NSString *displayCountry = [locale displayNameForKey:NSLocaleIdentifier
                                                   value:[locale objectForKey:NSLocaleLanguageCode]];
    UnitySendMessage([UNITY_GAME_OBJECT_NAME UTF8String], "OnGetDisplayCountry", [displayCountry UTF8String]);
}

-(void) getCountry {
    NSLocale *locale =[NSLocale currentLocale];
    
    NSString *countryCode = [locale objectForKey:NSLocaleCountryCode];
    UnitySendMessage([UNITY_GAME_OBJECT_NAME UTF8String], "OnGetCountry", [countryCode UTF8String]);
}

-(void) getLanguage {
    //NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLocale *locale = [NSLocale currentLocale];
    NSString * language = [locale objectForKey:NSLocaleLanguageCode];
    UnitySendMessage([UNITY_GAME_OBJECT_NAME UTF8String], "OnGetLanguage", [language UTF8String]);
}

extern "C" {
    void _BWCommonGetDisplayCountry() {
        [[BWCommonManager sharedInstance] getDisplayCountry];
    }
    void _BWCommonGetCountry() {
        [[BWCommonManager sharedInstance] getCountry];
    }
    void _BWCommonGetLanguage() {
        [[BWCommonManager sharedInstance] getLanguage];
    }
}
@end




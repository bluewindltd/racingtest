﻿using UnityEngine;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;
using System.ComponentModel;


public class AdBrixPluginIOS : MonoBehaviour
{
	// for cohort 분석
	public const int AdBrixCustomCohort_1 = 1;
	public const int AdBrixCustomCohort_2 = 2;
	public const int AdBrixCustomCohort_3 = 3;


	#region Events
#if UNITY_IPHONE


#endif
	#endregion

	#region	Interface to native implementation
	[DllImport("__Internal")]
	extern public static void _FirstTimeExperience(string name);
	
	[DllImport("__Internal")]
	extern public static void _FirstTimeExperienceWithParam(string name, string param);
	
	[DllImport("__Internal")]
	extern public static void _Retention(string name);
	
	[DllImport("__Internal")]
	extern public static void _RetentionWithParam(string name, string param);
	
	[DllImport("__Internal")]
	extern public static void _Buy(string name);
	
	[DllImport("__Internal")]
	extern public static void _BuyWithParam(string name, string param);

	[DllImport("__Internal")]
	extern public static void _ShowViralCPINotice();
	
	[DllImport("__Internal")]
	extern public static void _SetCustomCohort(int customCohortType, string filterName);
	
	[DllImport("__Internal")]
	extern public static void _CrossPromotionShowAD(string activityName);

	#endregion

	#region Declarations for non-native for AdBrix

	/// <summary>
	/// first time experience의 activity에 해당할때 호출한다.
	/// </summary>
	/// <param name="activityName">
	/// A <see cref="System.String"/>
	/// </param>
	public static void FirstTimeExperience(string name)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_FirstTimeExperience(name);
		#endif
	}
	
	/// <summary>
	/// first time experience의 activity에 해당할때 호출한다.
	/// </summary>
	/// <param name="activityName">
	/// A <see cref="System.String"/>
	/// </param>
	/// <param name="param">
	/// A <see cref="System.String"/>
	/// </param>
	public static void FirstTimeExperienceWithParam(string name, string param)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_FirstTimeExperienceWithParam(name, param);
		#endif
	}
	
	/// <summary>
	/// retention activity에 해당할때 호출한다.
	/// </summary>
	/// <param name="activityName">
	/// A <see cref="System.String"/>
	/// </param>
	public static void Retention(string name)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_Retention(name);
		#endif
	}
	
	/// <summary>
	/// retention activity에 해당할때 호출한다.
	/// </summary>
	/// <param name="activityName">
	/// A <see cref="System.String"/>
	/// </param>
	/// <param name="param">
	/// A <see cref="System.String"/>
	/// </param>
	public static void RetentionWithParam(string name, string param)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_RetentionWithParam(name, param);
		#endif
	}
	
	/// <summary>
	/// buy의 activity에 해당할때 호출한다.
	/// </summary>
	/// <param name="activityName">
	/// A <see cref="System.String"/>
	/// </param>
	public static void Buy(string activityName)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_Buy(activityName);
		#endif
	}
	
	/// <summary>
	/// buy의 activity에 해당할때 호출한다.
	/// </summary>
	/// <param name="activityName">
	/// A <see cref="System.String"/>
	/// </param>
	/// <param name="param">
	/// A <see cref="System.String"/>
	/// </param>
	public static void BuyWithParam(string name, string param)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_BuyWithParam(name, param);
		#endif
	}

	/// <summary>
	/// CPI + 친구초대 이벤트 요청시 호출한다.
	/// </summary>
	public static void ShowViralCPINotice()
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;

		_ShowViralCPINotice();
		#endif
	}
	
	/// <summary>
	/// Cohort 분석 요청시 호출한다.
	/// </summary>
	/// <param name="customCohortType">
	/// A <see cref="int"/>
	/// </param>
	/// <param name="filterName">
	/// A <see cref="System.String"/>
	/// </param>
	public static void SetCustomCohort(int customCohortType, string filterName)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_SetCustomCohort(customCohortType, filterName);
		#endif
	}
	
	#endregion	
	
	
	#region Declarations for non-native for CrossPromotion
	/// <summary>
	/// Cross Promotion 광고를 노출하고자 할때 호출한다.
	/// </summary>
	/// <param name="activityName">
	/// A <see cref="System.String"/>
	/// </param>
	public static void CrossPromotionShowAD(string activityName)
	{
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.OSXEditor)
			return;
		
		_CrossPromotionShowAD(activityName);
		#endif
	}

	#endregion
}

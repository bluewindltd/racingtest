//
//  BWCommonPlugin.h
//  
//
//  Created by 김건훈 on 2015. 9. 23..
//
//

#import <Foundation/Foundation.h>

@interface BWCommonManager : NSObject

+ (id) sharedInstance;

-(void) getDisplayCountry;
-(void) getCountry;
-(void) getLanguage;


@end

//
//  AdPopcornSDKPlugin.m
//  IgaworksAd
//
//  Created by wonje,song on 2014. 1. 21..
//  Copyright (c) 2014년 wonje,song. All rights reserved.
//

#import "AdBrixPlugin.h"


UIViewController *UnityGetGLViewController();

static AdBrixPlugin *_sharedInstance = nil; //To make AdBrixPlugin Singleton

@implementation AdBrixPlugin



+ (void)initialize
{
	if (self == [AdBrixPlugin class])
	{
		_sharedInstance = [[self alloc] init];
	}
}


+ (AdBrixPlugin *)sharedAdBrixPlugin
{
	return _sharedInstance;
}

- (id)init
{
	self = [super init];
    
    if (self)
    {
        
    }
	return self;
}






// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {
	
    
    void _FirstTimeExperience(const char* name)
    {
        NSLog(@"_FirstTimeExperience : %s", name);
        
        [AdBrix firstTimeExperience:[NSString stringWithUTF8String:name]];
    }
    
    void _FirstTimeExperienceWithParam(const char* name, const char* param)
    {
        [AdBrix firstTimeExperience:[NSString stringWithUTF8String:name] param:[NSString stringWithUTF8String:param]];
    }
    
    void _Retention(const char* name)
    {
        [AdBrix retention:[NSString stringWithUTF8String:name]];
    }
    
    void _RetentionWithParam(const char* name, const char* param)
    {
        [AdBrix retention:[NSString stringWithUTF8String:name] param:[NSString stringWithUTF8String:param]];
    }
    
    void _Buy(const char* name)
    {
        [AdBrix buy:[NSString stringWithUTF8String:name]];
    }
    
    void _BuyWithParam(const char* name, const char* param)
    {
        [AdBrix buy:[NSString stringWithUTF8String:name] param:[NSString stringWithUTF8String:param]];
    }
    
    
    void _ShowViralCPINotice()
    {
        NSLog(@"AdBrixPlugin : _ShowViralCPINotice");
      
        [AdBrix showViralCPINotice:UnityGetGLViewController()];
    }
    
    void _SetCustomCohort(AdBrixCustomCohortType customCohortType, const char* filterName)
    {
        [AdBrix setCustomCohort:customCohortType filterName:[NSString stringWithUTF8String:filterName]];
    }
    
    void _CrossPromotionShowAD(const char* activityName)
    {
        [CrossPromotion showAD:[NSString stringWithUTF8String:activityName] parentViewController:UnityGetGLViewController()];
    }

}

@end


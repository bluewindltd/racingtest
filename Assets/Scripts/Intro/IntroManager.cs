﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using CodeStage.AntiCheat.ObscuredTypes;
using AESENC;
using SimpleJSON;
using MadUtil;
using BW.Common;

public class IntroManager : Singleton<IntroManager>
{
    public GameObject commonPopupPanel;

    public GameObject tempBGObject;
    public GameObject introBGAndroidObject;
	public GameObject introBGiOSObject;
    public GameObject loadingObject;

    private bool m_IsStart = false;

    UserManager m_UserMgr = null;

    AES testAES = new AES();

    bool m_PlatformLogoEnded = false;
    bool m_IntroEnded = false;

    string[] m_AndroidPermissions = { "android.permission.READ_PHONE_STATE", "android.permission.WRITE_EXTERNAL_STORAGE" };
    List<string> m_RequestPermissionList = new List<string>();

    void Awake()
    {
        m_PlatformLogoEnded = false;
        m_IntroEnded = false;

        m_UserMgr = UserManager.instance;
        m_UserMgr.intialize = true;
        m_UserMgr.encryptKey = "k76z1cea12l0g19eo84y41207f9h12ff";
        m_UserMgr.startCash = 50;

#if UNITY_ANDROID// && !UNITY_EDITOR
        BWCommonPlugin.OnShouldShowRequestPermissionRationaleResult += OnShouldShowRequestPermissionRationaleResult;
        BWCommonPlugin.OnRequestPermissionsResult += OnRequestPermissionsResult;
        BWCommonPlugin.OnGoDetailSettingResult += OnGoDetailSettingResult;
#endif

        ////Vector3 player = new Vector3(-19.4f, 10000.0f, 19.6f);
        ////Vector3 building = new Vector3(-35.6f, -39400.0f, 31.3f);
        //Vector3 player = new Vector3(1000.0f, 10000.0f, 0);
        //Vector3 building = new Vector3(-1000.0f, -39400.0f, 0);

        //Vector3 haha = Vector3.Cross(player, building);
        //Vector3 haha2 = Vector3.Cross(player.normalized, building.normalized);
        //float aa2 = Vector3.Dot(haha2, new Vector3(0, 1, 0));
        //float test2 = haha2.sqrMagnitude;
        //float test22 = haha2.magnitude;
        //Vector3 haha1 = Vector3.Cross(building.normalized, player.normalized);
        //float aa1 = Vector3.Dot(haha1, new Vector3(0, 1, 0));
        //float test1 = haha1.sqrMagnitude;
        //float test11 = haha1.magnitude;
        //int aa = 0;
        //aa = 10;
    }

    void Start()
    {
        //ObscuredPrefs.DeleteAll();
        //PlayerPrefs.DeleteAll();

        /*
        string systemInfo = "!!! systemInfo";
        systemInfo += "\n deviceModel : ";
        systemInfo += SystemInfo.deviceModel;
        systemInfo += "\n deviceName : ";
        systemInfo += SystemInfo.deviceName;
        systemInfo += "\n deviceType : ";
        systemInfo += SystemInfo.deviceType;
        systemInfo += "\n deviceUniqueIdentifier : ";
        systemInfo += SystemInfo.deviceUniqueIdentifier;
        systemInfo += "\n graphicsDeviceID : ";
        systemInfo += SystemInfo.graphicsDeviceID;
        systemInfo += "\n graphicsDeviceName : ";
        systemInfo += SystemInfo.graphicsDeviceName;
        systemInfo += "\n graphicsDeviceType : ";
        systemInfo += SystemInfo.graphicsDeviceType;
        systemInfo += "\n graphicsDeviceVendor : ";
        systemInfo += SystemInfo.graphicsDeviceVendor;
        systemInfo += "\n graphicsDeviceVendorID : ";
        systemInfo += SystemInfo.graphicsDeviceVendorID;
        systemInfo += "\n graphicsDeviceVersion : ";
        systemInfo += SystemInfo.graphicsDeviceVersion;
        systemInfo += "\n graphicsMemorySize : ";
        systemInfo += SystemInfo.graphicsMemorySize;
        systemInfo += "\n graphicsMultiThreaded : ";
        systemInfo += SystemInfo.graphicsMultiThreaded;
        systemInfo += "\n graphicsShaderLevel : ";
        systemInfo += SystemInfo.graphicsShaderLevel;
        systemInfo += "\n maxTextureSize : ";
        systemInfo += SystemInfo.maxTextureSize;
        systemInfo += "\n npotSupport : ";
        systemInfo += SystemInfo.npotSupport;
        systemInfo += "\n operatingSystem : ";
        systemInfo += SystemInfo.operatingSystem;
        systemInfo += "\n processorCount : ";
        systemInfo += SystemInfo.processorCount;
        systemInfo += "\n processorFrequency : ";
        systemInfo += SystemInfo.processorFrequency;
        systemInfo += "\n processorType : ";
        systemInfo += SystemInfo.processorType;
        systemInfo += "\n supportedRenderTargetCount : ";
        systemInfo += SystemInfo.supportedRenderTargetCount;
        systemInfo += "\n supports3DTextures : ";
        systemInfo += SystemInfo.supports3DTextures;
        systemInfo += "\n supportsAccelerometer : ";
        systemInfo += SystemInfo.supportsAccelerometer;
        systemInfo += "\n supportsComputeShaders : ";
        systemInfo += SystemInfo.supportsComputeShaders;
        systemInfo += "\n supportsGyroscope : ";
        systemInfo += SystemInfo.supportsGyroscope;
        systemInfo += "\n supportsImageEffects : ";
        systemInfo += SystemInfo.supportsImageEffects;
        systemInfo += "\n supportsInstancing : ";
        systemInfo += SystemInfo.supportsInstancing;
        systemInfo += "\n supportsLocationService : ";
        systemInfo += SystemInfo.supportsLocationService;
        systemInfo += "\n supportsRawShadowDepthSampling : ";
        systemInfo += SystemInfo.supportsRawShadowDepthSampling;
        Debug.Log(systemInfo);
        string systemInfo1 = "";
        systemInfo1 += "supportsRenderTextures : ";
        systemInfo1 += SystemInfo.supportsRenderTextures;
        systemInfo1 += "\n supportsRenderToCubemap : ";
        systemInfo1 += SystemInfo.supportsRenderToCubemap;
        systemInfo1 += "\n supportsShadows : ";
        systemInfo1 += SystemInfo.supportsShadows;
        systemInfo1 += "\n supportsSparseTextures : ";
        systemInfo1 += SystemInfo.supportsSparseTextures;
        systemInfo1 += "\n supportsStencil : ";
        systemInfo1 += SystemInfo.supportsStencil;
        systemInfo1 += "\n supportsVibration : ";
        systemInfo1 += SystemInfo.supportsVibration;
        systemInfo1 += "\n systemMemorySize : ";
        systemInfo1 += SystemInfo.systemMemorySize;
        Debug.Log(systemInfo1);
        */

        //List<int> testList = new List<int>();
        //testList.Add(0);
        //testList.Add(1);
        //testList.Add(2);
        //testList.Add(3);
        //testList.Add(4);

        //int aa = testList.Find(x => x == 0);
        //int tt = 0;
        //tt = 10;

        PopupManager.instance.commonPopupPanel = commonPopupPanel;

        m_UserMgr.InitializeResolution();
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        if (PlayerPrefs.GetInt("CheckShadow") == 0)
        {
            int systemMemorySize = SystemInfo.systemMemorySize;
            //Debug.Log("#### CheckShadow : " + systemMemorySize);
            if (systemMemorySize >= 3500)
            {
                PlayerPrefs.SetInt("ShadowOn", 1);
            }
            else
            {
                PlayerPrefs.SetInt("ShadowOn", 0);
                m_UserMgr.ChangeLowResolution();
            }

            PlayerPrefs.SetInt("CheckShadow", 1);
        }
        else
        {
            if (PlayerPrefs.GetInt("ShadowOn") == 0)
            {
                m_UserMgr.ChangeLowResolution();
            }
        }
#elif UNITY_IOS
        if (PlayerPrefs.GetInt("CheckShadow") == 0)
        {
            int systemMemorySize = SystemInfo.systemMemorySize;
            //Debug.Log("#### CheckShadow : " + systemMemorySize);
            if (systemMemorySize >= 1900)
            {
                PlayerPrefs.SetInt("ShadowOn", 1);
            }
            else
            {
                PlayerPrefs.SetInt("ShadowOn", 0);
            }

            PlayerPrefs.SetInt("CheckShadow", 1);
        }

		iCloudManager.OnCloudInitAction += OnCloudInit;
#endif        

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        GDEDataManager.Init("gde_data_enc", true);

        m_UserMgr = UserManager.instance;

        m_UserMgr.isShowStartInterstitialAd = false;
        m_UserMgr.isPrevSceneIntro = true;

        m_UserMgr.LoadUserData();
        m_UserMgr.LoadOwnVehicleData();

        SupportLanguageDocs.instance.Initialize();
        m_UserMgr.InitializeLanguage();
#if UNITY_ANDROID && !UNITY_EDITOR
        //m_UserMgr.SSLTrust();
        m_UserMgr.TrustAllHosts();
#endif

        m_IsStart = true;

        TextDocs.instance.Initialize();
        ChaserDocs.instance.Initialize();
        ChaserCountDocs.instance.Initialize();
        EvilCountDocs.instance.Initialize();
        PointDocs.instance.Initialize();
        VehicleDocs.instance.Initialize();
        PackageDocs.instance.Initialize();
        PetDocs.instance.Initialize();
        GachaDocs.instance.Initialize();
        GiftDocs.instance.Initialize();
        AchievementDocs.instance.Initialize();
#if BWQUEST
        QuestDocs.instance.Initialize();
#endif
        TipDocs.instance.Initialize();
        EtcDocs.instance.Initialize();

#if BWQUEST
        QuestManager.instance.LoadQuestData();
#endif

        //IGAWorksManager.instance.FirstTimeExperience("Enter_IntroScene");

        StartCoroutine(ChangeIntroBG());
#if UNITY_ANDROID && !UNITY_EDITOR
#else
        StartCoroutine(PrepareGame());
#endif
        StartCoroutine(ChangeScene());
    }

	void OnDestroy()
	{
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        BWCommonPlugin.OnShouldShowRequestPermissionRationaleResult -= OnShouldShowRequestPermissionRationaleResult;
        BWCommonPlugin.OnRequestPermissionsResult -= OnRequestPermissionsResult;
#elif UNITY_IOS
		iCloudManager.OnCloudInitAction -= OnCloudInit;
#endif
    }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_IOS
    void OnCloudInit (ISN_Result result)
    {
		if (result.IsSucceeded)
		{
			Debug.Log ("!OnCloud Initialize Success!");
			UserManager.instance.isCloudInit = true;
		}
		else
		{
			Debug.Log ("!OnCloud Initialize Fail!");
			UserManager.instance.isCloudInit = false;
		}
    	
    }
#endif

    void ShowPermissionExplainDialog()
    {
        AndroidDialog dialog = AndroidDialog.Create(TextDocs.content("POPUP_PERMISSION_EXPLAIN_TITLE"), TextDocs.content("POPUP_PERMISSION_EXPLAIN_TEXT_1"), TextDocs.content("BT_OK_TEXT"), TextDocs.content("BT_CANCEL_TEXT"));
        dialog.ActionComplete += OnPermissionExplainDialogComplete;
    }

    private void OnPermissionExplainDialogComplete(AndroidDialogResult result)
    {
        //parsing result
        switch (result)
        {
            case AndroidDialogResult.YES:
                BWCommonPlugin.instance.RequestPermissions(m_RequestPermissionList.ToArray());
                break;
            case AndroidDialogResult.NO:
                Application.Quit();
                break;
            case AndroidDialogResult.CLOSED:
                Application.Quit();
                break;
        }
    }

    void ShowPermissionInduceDialog()
    {
        AndroidDialog dialog = AndroidDialog.Create(TextDocs.content("POPUP_PERMISSION_INDUCE_TITLE"), TextDocs.content("POPUP_PERMISSION_INDUCE_TEXT_1"), TextDocs.content("SETUP_TITLE_TEXT"), TextDocs.content("BT_CANCEL_TEXT"));
        dialog.ActionComplete += OnPermissionInduceDialogComplete;
    }

    private void OnPermissionInduceDialogComplete(AndroidDialogResult result)
    {
        //parsing result
        switch (result)
        {
            case AndroidDialogResult.YES:
                BWCommonPlugin.instance.GoDetailSetting();
                break;
            case AndroidDialogResult.NO:
                Application.Quit();
                break;
            case AndroidDialogResult.CLOSED:
                Application.Quit();
                break;
        }
    }

    void OnShouldShowRequestPermissionRationaleResult(Dictionary<string, string> resultDic)
    {
        m_RequestPermissionList.Clear();
        GameConfig.PermissionState permissionState = GameConfig.PermissionState.PASS;

        if (resultDic.Count > 0)
        {
            Dictionary<string, string>.Enumerator etor = resultDic.GetEnumerator();
            while (etor.MoveNext())
            {
                string state = etor.Current.Value;
                if (state.CompareTo("BLOCKED") == 0)
                {
                    int isFirst = PlayerPrefs.GetInt(etor.Current.Key);
                    // 한번도 권한 요청이 안된 상태
                    if (isFirst == 0)
                    {
                        m_RequestPermissionList.Add(etor.Current.Key);
                        permissionState = GameConfig.PermissionState.NEED_REQUEST;
                    }
                    else // 유저가 의도적으로 권한을 Block 한 상태
                    {
                        permissionState = GameConfig.PermissionState.NON_PASS;
                        break;
                    }
                }
                else if (state.CompareTo("DENIED") == 0)
                {
                    m_RequestPermissionList.Add(etor.Current.Key);
                    permissionState = GameConfig.PermissionState.NEED_REQUEST;
                }
                else // "GRANTED"
                {

                }
            }
        }
        else
        {
            permissionState = GameConfig.PermissionState.NON_PASS;
        }

        switch(permissionState)
        {
            case GameConfig.PermissionState.PASS:
                {
                    StartCoroutine(PrepareGame());
                }
                break;
            case GameConfig.PermissionState.NON_PASS:
                {
                    // 권한이 블럭당해있으므로 권한 유도 팝업을 띄운다.
                    ShowPermissionInduceDialog();
                }
                break;
            case GameConfig.PermissionState.NEED_REQUEST:
                {
                    // 권한 설명 팝업 띄운다.
                    ShowPermissionExplainDialog();
                }
                break;
        }
    }

    void OnRequestPermissionsResult(Dictionary<string, string> resultDic)
    {
        int isSuccessCount = 0;
        if (resultDic.Count > 0)
        {
            Dictionary<string, string>.Enumerator etor = resultDic.GetEnumerator();
            while (etor.MoveNext())
            {
                PlayerPrefs.SetInt(etor.Current.Key, 1);
                string state = etor.Current.Value;
                if (state.CompareTo("TRUE") == 0)
                {
                    isSuccessCount += 1;
                }
            }
        }

        if(isSuccessCount != 0 && resultDic.Count <= isSuccessCount)
        {
            StartCoroutine(PrepareGame());
        }
        else
        {
            // 권한을 허용 안했으므로 권한 유도 팝업을 띄운다.
            ShowPermissionInduceDialog();
        }
    }

    void OnGoDetailSettingResult()
    {
        // 설정으로 유도 후 권한을 취득했는지 다시 확인 한다.
        BWCommonPlugin.instance.ShouldShowRequestPermissionRationale(m_AndroidPermissions);
    }

    IEnumerator ChangeIntroBG()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        NGUITools.SetActive(tempBGObject, true);
        NGUITools.SetActive(introBGAndroidObject, false);
        NGUITools.SetActive(loadingObject, false);

        yield return new WaitForSeconds(0.2f);

        Handheld.PlayFullScreenMovie("googleplaylogomovie.mp4", Color.white, FullScreenMovieControlMode.Hidden);

        Debug.Log("!@# EndLogo");

        yield return null;

        BWCommonPlugin.instance.ShouldShowRequestPermissionRationale(m_AndroidPermissions);

        m_IntroEnded = true;
        m_PlatformLogoEnded = true;

        NGUITools.SetActive(tempBGObject, false);
		NGUITools.SetActive(introBGAndroidObject, false);
        NGUITools.SetActive(loadingObject, true);
#else
        m_PlatformLogoEnded = true;

        NGUITools.SetActive(tempBGObject, false);
        NGUITools.SetActive(introBGiOSObject, true);

        yield return null;

        if (!m_IntroEnded)
        {
            m_IntroEnded = true;

            SoundManager.instance.PlaySFX("intro_engine");

            yield return new WaitForSeconds(2.0f);

			NGUITools.SetActive(introBGiOSObject, false);
            NGUITools.SetActive(loadingObject, true);
        }
#endif
    }

    IEnumerator PrepareGame()
    {
        //Debug.Log("PrepareGame 0 : " + Application.internetReachability + ", " + InternetReachabilityVerifier.Instance.status);

        while (!m_UserMgr.intialize || !m_IsStart)
        {
            yield return null;
        }

        //Debug.Log("PrepareGame 0-1 : " + m_UserMgr.isFirstInternetCheck + ", " + InternetReachabilityVerifier.Instance.status);

        while (!m_UserMgr.isFirstInternetCheck)
        {
            yield return null;
        }

        //Debug.Log("PrepareGame 0-2 : " + m_UserMgr.isFirstInternetCheck + ", " + InternetReachabilityVerifier.Instance.status);

        yield return null;

        //Debug.Log("PrepareGame 1");

        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
			Debug.Log("PrepareGame 1-1");
            CommonAdManager.instance.Initialize();
            CommonAdManager.instance.Fetch(CommonAdManager.AdType.Video);
            CommonAdManager.instance.Fetch(CommonAdManager.AdType.Interstitial);
        }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_IOS
		if(UserManager.instance.isCloudInit == false)
		{
			iCloudManager.Instance.init();
		}
#endif

        Debug.Log("PrepareGame 2");

		//StartCoroutine (m_UserMgr.AESTest ());

		Debug.Log("PrepareGame 2-1");

#if UNITY_ANDROID && !UNITY_EDITOR
        if (m_PlatformLogoEnded && m_IntroEnded)
        {
            NGUITools.SetActive(introBGAndroidObject, false);
            NGUITools.SetActive(loadingObject, true);
        }
#else
        if (m_PlatformLogoEnded && !m_IntroEnded)
        {
            m_IntroEnded = true;

            SoundManager.instance.PlaySFX("intro_engine");

            yield return new WaitForSeconds(2.0f);
            
            NGUITools.SetActive(introBGiOSObject, false);
            NGUITools.SetActive(loadingObject, true);
        }
#endif
		Debug.Log("PrepareGame 2-2");

        yield return new WaitForSeconds(0.1f);

        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
			m_UserMgr.LoadStore(UserManager.IAPPlatform.Android, UserManager.IAPState.Init);
#elif UNITY_IOS
			m_UserMgr.LoadStore(UserManager.IAPPlatform.IOS, UserManager.IAPState.Init);
#endif
            //yield return StartCoroutine(LoadStore());
        }

        Debug.Log("PrepareGame 3");

		/*
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_IOS
		IOSNotificationController.Instance.RequestNotificationPermissions();
#endif
		*/

        while(!m_PlatformLogoEnded)
        {
            yield return null;
        }

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED)
            {
                int tryFirstGPLogin = ObscuredPrefs.GetInt("TryFirstGPLogin", 0);
                int beCameGPLogin = ObscuredPrefs.GetInt("BeCameGPLogin", 0);
                if (tryFirstGPLogin == 0 ||
                    (tryFirstGPLogin == 1 && beCameGPLogin == 1))
                {
                    ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
                    Debug.Log("구글 플레이 접속 시도");
                    GooglePlayConnection.Instance.Connect();
                }
            }
            else
            {
                AchievementDocs.instance.UpdateAchievement();
            }
        }
#elif UNITY_IOS
		//Debug.Log("@@ GameCenter Init 0");
		if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
		{
			//Debug.Log("@@ GameCenter Init 1 : " + GameCenterManager.IsPlayerAuthenticated);
			if(GameCenterManager.IsPlayerAuthenticated == false)
			{
				int tryFirstGPLogin = ObscuredPrefs.GetInt("TryFirstGPLogin", 0);
				int beCameGPLogin = ObscuredPrefs.GetInt("BeCameGPLogin", 0);
				//Debug.Log("@@ GameCenter Init 2 : " + tryFirstGPLogin + ", " + beCameGPLogin);
				if(tryFirstGPLogin == 0 ||
					(tryFirstGPLogin == 1 && beCameGPLogin == 1))
				{
					ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
					//Debug.Log("@@ GameCenter Init 3");
					UserManager.instance.GameCenterInit(true);
				}
			}
		}
#endif

        UserManager.instance.isPrepare = true;
    }

		/*
    IEnumerator LoadStore()
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
        yield return new WaitForEndOfFrame();
#else
        if(!UserManager.instance.loadStore)
        {
            while (!UserManager.instance.loadStore)
            {
                yield return new WaitForEndOfFrame();
            }
        }
#endif
    }
    */

    IEnumerator ChangeScene()
    {
        MissionManager.instance.Initialize();
        Debug.Log("미션 매니저 초기화 완료");

        yield return new WaitForSeconds(0.1f);

        GroundManager.instance.Initialize();
        Debug.Log("그라운드 매니저 초기화 완료");

        yield return new WaitForSeconds(0.1f);

        PoliceLineManager.instance.Initialize();

        yield return new WaitForSeconds(0.1f);

        ItemManager.instance.Initialize();

        yield return new WaitForSeconds(0.1f);

        RegionManager.instance.Initialize();
        Debug.Log("리젼 매니저 초기화 완료");

        yield return new WaitForSeconds(0.1f);

        NPCManager.instance.Initialize();
        Debug.Log("엔피씨 매니저 초기화 완료");

        while (!m_UserMgr.isPrepare || !m_PlatformLogoEnded || !m_IntroEnded)
        {
			//Debug.Log ("m_UserMgr.isPrepare || !m_PlatformLogoEnded || !m_IntroEnded" + m_UserMgr.isPrepare + ", " + m_PlatformLogoEnded + ", " + m_IntroEnded);
            yield return new WaitForSeconds(0.1f);
        }

        // 구글 전면 광고 미리 로딩
        CommonAdManager.instance.PrepareLoadInterstitialAd();

        //if (TimeManager.instance.GetTimeState())
        //{
        //    long time = TimeManager.instance.GetOnlineTime();
        //    string timeStr = StringUtil.ConvertStringElapseTime(7170);
        //    int aa = 0;
        //    aa = 10;
        //}

        SceneFadeInOut.LoadLevel((int)GameConfig.Scene.GameScene);
    }
}

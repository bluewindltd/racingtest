﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VehicleStockTab : MonoBehaviour
{
    Dictionary<int, VehicleStockCell> m_VehicleStockCellDic = new Dictionary<int, VehicleStockCell>();
    public Dictionary<int, VehicleStockCell> vehicleStockCellDic
    {
        get
        {
            return m_VehicleStockCellDic;
        }
    }

    public void ChangeStockCell(int index, VehicleInfo.State state)
    {
        if(m_VehicleStockCellDic.ContainsKey(index))
        {
            VehicleStockCell vehicleStockCell = m_VehicleStockCellDic[index];
            vehicleStockCell.ChangeState(state);
        }
    }

    public void Refresh()
    {
        UserManager userMgr = UserManager.instance;
        VehicleInfo vehicleInfo;
        VehicleStockCell vehicleStockCell;
        Dictionary<int, VehicleStockCell>.Enumerator etor = m_VehicleStockCellDic.GetEnumerator();
        while(etor.MoveNext())
        {
            vehicleStockCell = etor.Current.Value;

            if(userMgr.vehicleInfoDic.ContainsKey(etor.Current.Key))
            {
                vehicleInfo = userMgr.vehicleInfoDic[etor.Current.Key];
                vehicleStockCell.ChangeState(vehicleInfo.state);
            }
            else
            {
                vehicleStockCell.ChangeState(VehicleInfo.State.NotHave);
            }
        }
    }
}

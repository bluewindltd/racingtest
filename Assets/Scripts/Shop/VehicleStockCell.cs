﻿using UnityEngine;
using System.Collections;

public class VehicleStockCell : MonoBehaviour
{
    public UIDragCamera uiDragCamera;
    public GameObject[] cellObjectArray;
    public Transform[] vehicleRootArray;
    public UILabel[] nameLabel;
    public GameObject[] descButtonObjectArray;
    public UIDragCamera[] buttonUIDragCameras;
#if BWQUEST
    public GameObject[] normalCellObjectArray;
    public Transform[] normalVehicleRootArray;
#endif
    public UISprite tokenSprite;
    public UILabel tokenCountLabel;
    public UILabel priceLabel;
#if BWQUEST
    public UILabel questRewardButtonLabel;
#endif
    public UILabel purchasedLabel;
    public UILabel chageCarButtonLabel;
    public UILabel boardingLabel;

    int m_State = 0;
#if BWQUEST
    int m_EarnType = 0;
#endif
    GameObject m_VehicleObject;
    VehicleStockTab m_VehicleStockTab;
    public VehicleStockTab vehicleStockTab
    {
        set
        {
            m_VehicleStockTab = value;
        }
    }
    VehicleData m_VehicleData = null;
    ShopVehicle m_ShopVehicle = null;
    int m_PreSelectVehicleIndex = -1;

    public void Initialize(VehicleData vehicleData, UIDraggableCamera draggableCamera, GameObject vehiclePrefab, VehicleInfo.State state)
    {
        m_VehicleData = vehicleData;

        m_State = (int)state;

#if BWQUEST
        m_EarnType = (int)vehicleData.earnType;
        if (state == VehicleInfo.State.NotHave)
        {
            NGUITools.SetActive(normalCellObjectArray[m_EarnType], true);
            m_VehicleObject = NGUITools.AddChild(normalVehicleRootArray[m_EarnType].gameObject, vehiclePrefab);
        }
        else
        {
            NGUITools.SetActive(cellObjectArray[m_State], true);
            m_VehicleObject = NGUITools.AddChild(vehicleRootArray[m_State].gameObject, vehiclePrefab);
        }

        m_ShopVehicle = m_VehicleObject.GetComponent<ShopVehicle>();
        if (state == VehicleInfo.State.Boarding)
        {
            m_ShopVehicle.animator.Play("Shop_Vehicle_Select_Idle");
        }
        else
        {
            m_ShopVehicle.animator.Play("Shop_Vehicle_None");
        }

        for (int i = 0; i < nameLabel.Length; i++)
        {
            nameLabel[i].text = TextDocs.content(m_VehicleData.name);
        }

        if (m_VehicleData.desc.CompareTo("") != 0)
        {
            for (int i = 0; i < descButtonObjectArray.Length; i++)
            {
                NGUITools.SetActive(descButtonObjectArray[i], true);
            }
        }

        tokenSprite.spriteName = GameConfig.TokenMiniMark[(int)m_VehicleData.tokenType];

        tokenCountLabel.text = string.Format("x {0}", m_VehicleData.tokenCount);
        if (m_VehicleData.stockPrice.CompareTo("") == 0)
        {
            priceLabel.text = m_VehicleData.tempPrice;
        }
        else
        {
            priceLabel.text = m_VehicleData.stockPrice;
        }

        uiDragCamera.draggableCamera = draggableCamera;
        for (int i = 0; i < buttonUIDragCameras.Length; i++)
        {
            buttonUIDragCameras[i].draggableCamera = draggableCamera;
        }

        questRewardButtonLabel.text = TextDocs.content("BT_QUEST_REWORD");
        purchasedLabel.text = TextDocs.content("UI_PURCHASED_TEXT");
        chageCarButtonLabel.text = TextDocs.content("BT_BOARDING_TEXT");
        boardingLabel.text = TextDocs.content("UI_BOARDING_TEXT");
#else
        NGUITools.SetActive(cellObjectArray[m_State], true);

        m_VehicleObject = NGUITools.AddChild(vehicleRootArray[m_State].gameObject, vehiclePrefab);
        m_ShopVehicle = m_VehicleObject.GetComponent<ShopVehicle>();

        if (state == VehicleInfo.State.Boarding)
        {
            m_ShopVehicle.animator.Play("Shop_Vehicle_Select_Idle");
        }
        else
        {
            m_ShopVehicle.animator.Play("Shop_Vehicle_None");
        }

        for (int i = 0; i < nameLabel.Length; i++)
        {
            nameLabel[i].text = TextDocs.content(m_VehicleData.name);
        }

        if(m_VehicleData.desc.CompareTo("") != 0)
        {
            for (int i = 0; i < descButtonObjectArray.Length; i++)
            {
                NGUITools.SetActive(descButtonObjectArray[i], true);
            }
        }

        tokenSprite.spriteName = GameConfig.TokenMiniMark[(int)m_VehicleData.tokenType];

        tokenCountLabel.text = string.Format("x {0}", m_VehicleData.tokenCount);
        if (m_VehicleData.stockPrice.CompareTo("") == 0)
        {
            priceLabel.text = m_VehicleData.tempPrice;
        }
        else
        {
            priceLabel.text = m_VehicleData.stockPrice;
        }

        uiDragCamera.draggableCamera = draggableCamera;
        for(int i = 0; i < buttonUIDragCameras.Length; i++)
        {
            buttonUIDragCameras[i].draggableCamera = draggableCamera;
        }

        purchasedLabel.text = TextDocs.content("UI_PURCHASED_TEXT");
        chageCarButtonLabel.text = TextDocs.content("BT_BOARDING_TEXT");
        boardingLabel.text = TextDocs.content("UI_BOARDING_TEXT");
#endif
    }

    public void ChangeState(VehicleInfo.State state)
    {
        if(m_State != (int)state)
        {
            if (state == VehicleInfo.State.Boarding)
            {
                m_ShopVehicle.animator.Play("Shop_Vehicle_Select");
            }
            else
            {
                m_ShopVehicle.animator.Play("Shop_Vehicle_None");
            }

#if BWQUEST
            if(m_State == 0)
            {
                NGUITools.SetActive(normalCellObjectArray[m_EarnType], false);             
            }
            else
            {
                NGUITools.SetActive(cellObjectArray[m_State], false);
            }
            m_State = (int)state;
            NGUITools.SetActive(cellObjectArray[m_State], true);

            CommonUtil.MoveChild(vehicleRootArray[m_State].gameObject, m_VehicleObject);

            if (state == VehicleInfo.State.Boarding)
            {
                m_ShopVehicle.animator.Play("Shop_Vehicle_Select");
            }
            else
            {
                m_ShopVehicle.animator.Play("Shop_Vehicle_None");
            }
#else
            NGUITools.SetActive(cellObjectArray[m_State], false);
            m_State = (int)state;
            NGUITools.SetActive(cellObjectArray[m_State], true);

            CommonUtil.MoveChild(vehicleRootArray[m_State].gameObject, m_VehicleObject);

            if (state == VehicleInfo.State.Boarding)
            {
                m_ShopVehicle.animator.Play("Shop_Vehicle_Select");
            }
            else
            {
                m_ShopVehicle.animator.Play("Shop_Vehicle_None");
            }
#endif
        }        
    }

    public void OnClickDesc()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowSmallOK(TextDocs.content(m_VehicleData.name),
            TextDocs.content(m_VehicleData.desc), null, null);
    }

    public void OnClickBuyToToken()
    {
        if (ShopManager.instance.enableMenu)
        {
            SoundManager.PlayButtonClickSound();

            PopupManager.instance.ShowLargeYesNo(TextDocs.content("POPUP_TOKENBUY_TITLE"),
                string.Format(TextDocs.content("POPUP_TOKENBUY_DESC"), GameConfig.TokenName[(int)m_VehicleData.tokenType], m_VehicleData.tokenCount) + 
                "\n" + TextDocs.content("GET_TOKEN_HELP_TEXT"),
                delegate
                {
                    m_PreSelectVehicleIndex = UserManager.instance.selectVehicleIndex;
                    GameConfig.BuyResultState buyResultState = UserManager.instance.VehicleBuyToToken(m_VehicleData);
                    if(buyResultState == GameConfig.BuyResultState.Success)
                    {
                        if(m_PreSelectVehicleIndex != -1)
                        {
                            m_VehicleStockTab.ChangeStockCell(m_PreSelectVehicleIndex, VehicleInfo.State.Purchased);
                        }                        
                        ChangeState(VehicleInfo.State.Boarding);

                        ShopManager.instance.RefreshTokenMoney();
                    }
                    else if(buyResultState == GameConfig.BuyResultState.NotEnoughToken)
                    {
                        PopupManager.instance.ShowSmallOK(TextDocs.content("POPUP_TOKENBUYFAIL_TITLE"), TextDocs.content("POPUP_TOKENBUYFAIL_DESC"), null, null);
                    }
                },
                null, null);
        }        
    }

    void PurchaseComplete(UserManager.PurchaseCompleteState purchaseCompleteState)
    {
        PopupManager.instance.HideLoadingPopup();

        UserManager.OnPurchaseComplete -= PurchaseComplete;

        if(purchaseCompleteState == UserManager.PurchaseCompleteState.Success)
        {
            if (m_PreSelectVehicleIndex != -1)
            {
                m_VehicleStockTab.ChangeStockCell(m_PreSelectVehicleIndex, VehicleInfo.State.Purchased);
            }
            ChangeState(VehicleInfo.State.Boarding);
        }
        else if(purchaseCompleteState == UserManager.PurchaseCompleteState.PurchaseError)
        {
            PopupManager.instance.ShowSmallOK(TextDocs.content("POPUP_PURCHASE_FAIL_TITLE"),
                TextDocs.content("POPUP_PURCHASE_FAIL_TEXT"), null, null);
        }
    }

    public void OnClickBuyToCash()
    {
        if (ShopManager.instance.enableMenu)
        {
            SoundManager.PlayButtonClickSound();

            PopupManager.instance.ShowLoadingPopup();

            m_PreSelectVehicleIndex = UserManager.instance.selectVehicleIndex;
            UserManager.OnPurchaseComplete += PurchaseComplete;
            UserManager.instance.Purchase(m_VehicleData, null, null);
        }
    }

    public void OnClickBoarding()
    {
        if (ShopManager.instance.enableMenu)
        {
            SoundManager.PlayButtonClickSound();

            int preSelectVehicleIndex = UserManager.instance.selectVehicleIndex;
            if(UserManager.instance.VehicleBoarding(m_VehicleData))
            {
                m_VehicleStockTab.ChangeStockCell(preSelectVehicleIndex, VehicleInfo.State.Purchased);
                ChangeState(VehicleInfo.State.Boarding);
            }
        }
    }

#if BWQUEST
    public void OnClickQuestRewardDesc()
    {
        if (ShopManager.instance.enableMenu)
        {
            SoundManager.PlayButtonClickSound();

#if BWTEEST
            m_PreSelectVehicleIndex = UserManager.instance.selectVehicleIndex;

            UserManager.instance.VehicleBuyToIAP(m_VehicleData, null, null);

            if (m_PreSelectVehicleIndex != -1)
            {
                m_VehicleStockTab.ChangeStockCell(m_PreSelectVehicleIndex, VehicleInfo.State.Purchased);
            }
            ChangeState(VehicleInfo.State.Boarding);
#else
            PopupManager.instance.ShowSmallOK(TextDocs.content(m_VehicleData.name), TextDocs.content("POPUP_SHOP_NOTICE01"), null, null);
#endif
        }
    }
#endif
        }

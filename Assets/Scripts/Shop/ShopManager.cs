﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopManager : Singleton<ShopManager>
{
    public GameObject behindScrollPopupPanel;
    public GameObject popupPanel;
    public GameObject frontScrollPopupPanel;
    public GameObject commonPopupPanel;

    public Animator shopAnimator;
    public GameObject[] vehicleStockTabObjectArray;
    public GameObject[] gradeButtonObjectArray;
    public GameObject[] gradeTabOnObjectArray;
    public VehicleStockTab[] vehicleStockTabArray;
    public UITable[] vehicleStockTabTableArray;
    public UIDraggableCamera[] draggableCameraArray;
    public GameObject vehicleStockCellPrefab;
    public UISprite gradeMarkSprite;
    public UILabel[] tokenLabel;
    public UILabel moneyLabel;
    public UILabel gachaButtonLabel;
    public UILabel exitButtonLabel;
    public UILabel restoreButtonLabel;

    VehicleDocs m_VehicleDocs;
    UserManager m_UserMgr;
    GoodsManager m_GoodsMgr;

    bool m_EnableMenu = false;
    public bool enableMenu
    {
        get
        {
            return m_EnableMenu;
        }
    }

    int m_ShowGrade = -1;
    bool m_IsRestore = false;
    int m_RestoreProductCount = 0;

    void Start()
    {
        m_UserMgr = UserManager.instance;
        m_GoodsMgr = GoodsManager.instance;

        if (!m_UserMgr.isPrepare)
        {
            SceneFadeInOut.LoadLevel((int)GameConfig.Scene.IntroScene);
        }
        else
        {
            IGAWorksManager.instance.FirstTimeExperience("Enter_ShopScene");

            PopupManager.instance.behindScrollPopupPanel = behindScrollPopupPanel;
            PopupManager.instance.popupPanel = popupPanel;
            PopupManager.instance.frontScrollPopupPanel = frontScrollPopupPanel;
            PopupManager.instance.commonPopupPanel = commonPopupPanel;

            SoundManager soundMgr = SoundManager.instance;
            soundMgr.AllStopLoopSFX();

            m_VehicleDocs = VehicleDocs.instance;
            m_EnableMenu = false;
            m_ShowGrade = -1;

            RefreshTokenMoney();

            gachaButtonLabel.text = TextDocs.content("UI_GACHA_BUTTON_TEXT");
            exitButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");
            restoreButtonLabel.text = TextDocs.content("UI_RESTORE_BUTTON_TEXT");

            m_IsRestore = false;
            UserManager.OnPurchaseComplete += PurchaseComplete;
            UserManager.OnGetRestoreInfo += GetRestoreInfo;

            StartCoroutine(InitializeTaps());
        }
    }

    void OnDestroy()
    {
        UserManager.OnPurchaseComplete -= PurchaseComplete;
        UserManager.OnGetRestoreInfo -= GetRestoreInfo;
    }

    IEnumerator InitializeTaps()
    {
        yield return null;

        UIDraggableCamera draggableCamera;
        VehicleStockTab vehicleStockTab;
        GameObject vehiclePrefab;
        VehicleData vehicleData;
        VehicleInfo vehicleInfo;
        VehicleInfo.State state;
        List<VehicleData> vehicleDataList;
        UITable table;
        int grade = 0;
        int showGrade = 0;
        Dictionary<VehicleData.Grade, List<VehicleData>>.Enumerator etor = m_VehicleDocs.vehicleGradeDic.GetEnumerator();
        while (etor.MoveNext())
        {
            grade = (int)etor.Current.Key;
            table = vehicleStockTabTableArray[grade];
            draggableCamera = draggableCameraArray[grade];
            vehicleDataList = etor.Current.Value;
            for (int i = 0; i < vehicleDataList.Count; i++)
            {
                vehicleData = vehicleDataList[i];
                vehiclePrefab = m_GoodsMgr.vehicleObjectArray[vehicleData.index];

                if (m_UserMgr.vehicleInfoDic.ContainsKey(vehicleData.index))
                {
                    vehicleInfo = m_UserMgr.vehicleInfoDic[vehicleData.index];
                    if (vehicleInfo.state == VehicleInfo.State.Boarding)
                    {
                        showGrade = grade;
                    }

                    state = vehicleInfo.state;
                }
                else
                {
                    vehicleInfo = null;
                    state = VehicleInfo.State.NotHave;
                }

                vehicleStockTab = vehicleStockTabArray[(int)vehicleData.grade];
                VehicleStockCell vehicleStockCell = NGUITools.AddChild(table.gameObject, vehicleStockCellPrefab).GetComponent<VehicleStockCell>();
                vehicleStockCell.vehicleStockTab = vehicleStockTab;
                vehicleStockTab.vehicleStockCellDic.Add(vehicleData.index, vehicleStockCell);
                vehicleStockCell.Initialize(vehicleData, draggableCamera, vehiclePrefab, state);
            }

            table.repositionNow = true;
        }

        ChangeTab(showGrade, true);

        shopAnimator.Play("Shop_Show");
    }

    public void RefreshTokenMoney()
    {
        Dictionary<int, int>.Enumerator etor = m_UserMgr.tokenDic.GetEnumerator();
        while (etor.MoveNext())
        {
            if (0 <= etor.Current.Key &&
                etor.Current.Key < tokenLabel.Length)
            {
                tokenLabel[etor.Current.Key].text = string.Format("{0}", etor.Current.Value);
            }
        }
        moneyLabel.text = string.Format("{0}", m_UserMgr.cash);
    }

#if UNITY_ANDROID
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PopupManager.instance.IsShowLoadingPopup() == false)
            {
                if (!PopupManager.instance.BackProcess())
                {
                    if (!BackProcess())
                    {
                        SoundManager.PlayButtonClickSound();

                        m_EnableMenu = false;

                        shopAnimator.Play("Shop_Hide");
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
        }
    }
#endif

    bool BackProcess()
    {
        return false;
    }

    public void ChangeTab(int grade, bool isFirst)
    {
        if (m_ShowGrade != grade)
        {
            m_ShowGrade = grade;
            gradeMarkSprite.spriteName = GameConfig.VehicleGradeMark[grade];
            for (int i = 0; i < vehicleStockTabObjectArray.Length; i++)
            {
                if (i == grade)
                {
                    NGUITools.SetActive(vehicleStockTabObjectArray[i], true);
                    VehicleStockTab vehicleStockTab = vehicleStockTabArray[i];
                    vehicleStockTab.Refresh();

                    NGUITools.SetActive(gradeButtonObjectArray[i], false);
                    NGUITools.SetActive(gradeTabOnObjectArray[i], true);
                }
                else
                {
                    NGUITools.SetActive(vehicleStockTabObjectArray[i], false);

                    NGUITools.SetActive(gradeButtonObjectArray[i], true);
                    NGUITools.SetActive(gradeTabOnObjectArray[i], false);
                }
            }
        }
    }

    public void EndShow()
    {
        m_EnableMenu = true;

        IGAWorksManager.instance.ShowPopup("shop_notice");
    }

    public void EndHide()
    {
        SceneFadeInOut.LoadLevel((int)GameConfig.Scene.GameScene);
    }

    //---------- Start UI Implementation ----------//
    public void OnClickSClass()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            ChangeTab((int)VehicleData.Grade.S, false);
        }
    }

    public void OnClickAClass()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            ChangeTab((int)VehicleData.Grade.A, false);
        }
    }

    public void OnClickBClass()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            ChangeTab((int)VehicleData.Grade.B, false);
        }
    }

    public void OnClickCClass()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            ChangeTab((int)VehicleData.Grade.C, false);
        }
    }

    public void OnClickChangeGachaScene()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_UserMgr.preScene = GameConfig.Scene.ShopScene;
            SceneFadeInOut.LoadLevel((int)GameConfig.Scene.GachaScene);
        }
    }

    public void OnClickGetTokenHelp()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();
            PopupManager.instance.ShowLargeOK(TextDocs.content("UI_TOKEN_TEXT2"), TextDocs.content("GET_TOKEN_HELP_TEXT"), null, null);
        }
    }

    public void OnClickExit()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EnableMenu = false;

            shopAnimator.Play("Shop_Hide");
        }
    }

    public void OnClickRestore()
    {
        if(m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            PopupManager.instance.ShowLoadingPopup();

            m_IsRestore = true;
            m_RestoreProductCount = 0;
            UserManager.instance.RestorePurchase();
        }
    }

    void GetRestoreInfo(int restoreProductCount)
    {
        m_RestoreProductCount = restoreProductCount;
    }

    void PurchaseComplete(UserManager.PurchaseCompleteState purchaseCompleteState)
    {
        if(m_IsRestore)
        {
            m_RestoreProductCount--;

            if (purchaseCompleteState == UserManager.PurchaseCompleteState.Success)
            {
                // 복구한 상품이 현재 탭에 있을 수 있으므로 복구 성공시에 현재 탭만 갱신해준다.
                for (int i = 0; i < vehicleStockTabObjectArray.Length; i++)
                {
                    if (i == m_ShowGrade)
                    {
                        VehicleStockTab vehicleStockTab = vehicleStockTabArray[i];
                        vehicleStockTab.Refresh();
                    }
                }
            }
            else if (purchaseCompleteState == UserManager.PurchaseCompleteState.PurchaseError)
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("POPUP_PURCHASE_FAIL_TITLE"),
                    TextDocs.content("POPUP_PURCHASE_FAIL_TEXT"), null, null);
            }

            if (m_RestoreProductCount <= 0)
            {
                PopupManager.instance.HideLoadingPopup();
            }            
        }        
    }
    //---------- End UI Implementation ----------//
}

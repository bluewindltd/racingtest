﻿using UnityEngine;
using System.Collections;

public class ShopVehicle : MonoBehaviour
{
    public Transform vehicleRootTransform;
    public Vector3 gachaPosition;
    public Vector3 questRewardPosition;
    public Vector3 questRewardScale;
    public Animator animator;

    public void SetGacha()
    {
        vehicleRootTransform.localEulerAngles = Vector3.zero;
        vehicleRootTransform.localPosition = gachaPosition;
        vehicleRootTransform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }

    public void SetQuestReward()
    {
        vehicleRootTransform.localPosition = questRewardPosition;
        vehicleRootTransform.localScale = questRewardScale;
    }
}

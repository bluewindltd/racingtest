﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GachaManager : Singleton<GachaManager>
{
    enum State
    {
        Wait = 0,
        Playing = 1,
        Finish = 2,
    };

    public GameObject behindScrollPopupPanel;
    public GameObject popupPanel;
    public GameObject frontScrollPopupPanel;
    public GameObject commonPopupPanel;

    public GameObject adButtonObject;
    public UIButton gachaButton;
    public GameObject gachaButtonObject;
    public GameObject exitButtonObject;

    public GameObject[] tokenObjectArray;

    public Animator messageAnimator;
    public GameObject waitGachObject;
    public GameObject playingGachaObject;
    public GameObject gachaInfoObject;
    public Animator gachaInfoAnimator;
    public UISprite gachaInfoGradeSprite;
    public UILabel gachaInfoNameLabel;

    public Animator playingGachaAnimator;

    public Transform gachaVehicleRootTransform;
    public UILabel moneyLabel;
    public UILabel commnetLabel;
    public UILabel adButtonLabel;
    public UILabel gachaButtonLabel;
    public UILabel gachaButtonCashLabel;
    public UILabel exitButtonLabel;

    public GameObject[] logoObjectArray;

    GachaDocs m_GachaDocs;
    UserManager m_UserMgr;
    GoodsManager m_GoodsMgr;

    bool m_EnableMenu = false;
    public bool enableMenu
    {
        get
        {
            return m_EnableMenu;
        }
    }

    State m_State = State.Wait;
    int m_GachaResultIndex = -1;
    bool m_DuplicationVehicle = false;
    bool m_IsStaticGacha = false;
    int m_TokenIndex = -1;
    int m_TokenCount = 0;

    // 가챠 테스트 관련
    public GameObject testButtonObject;
    public UITable testResultTable;
    public GameObject testResultLabelPrefab;

    void Start()
    {
        m_UserMgr = UserManager.instance;
        m_GoodsMgr = GoodsManager.instance;

        UserManager.OnRefreshCash += OnRefreshCash;
        CommonAdManager.OnHideVideoAd += OnHideVideoAd;

        if (!m_UserMgr.isPrepare)
        {
            SceneFadeInOut.LoadLevel((int)GameConfig.Scene.IntroScene);
        }
        else
        {
            IGAWorksManager.instance.FirstTimeExperience("Enter_GachaScene");

            PopupManager.instance.behindScrollPopupPanel = behindScrollPopupPanel;
            PopupManager.instance.popupPanel = popupPanel;
            PopupManager.instance.frontScrollPopupPanel = frontScrollPopupPanel;
            PopupManager.instance.commonPopupPanel = commonPopupPanel;

            SoundManager soundMgr = SoundManager.instance;
            soundMgr.AllStopLoopSFX();

            m_GachaDocs = GachaDocs.instance;
            m_EnableMenu = false;

            Initialize();
        }
    }

    void OnDestroy()
    {
        UserManager.OnRefreshCash -= OnRefreshCash;
        CommonAdManager.OnHideVideoAd -= OnHideVideoAd;
    }

#if UNITY_ANDROID
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PopupManager.instance.IsShowLoadingPopup() == false)
            {
                if (!PopupManager.instance.BackProcess())
                {
                    if (!BackProcess())
                    {
                        if (m_State == State.Wait || m_State == State.Finish)
                        {
                            if (m_UserMgr.preScene == GameConfig.Scene.ShopScene)
                            {
                                SceneFadeInOut.LoadLevel((int)GameConfig.Scene.ShopScene);
                            }
                            else
                            {
                                m_UserMgr.isDirectGamePlay = false;
                                m_UserMgr.isDirectGamePlayWithAD = false;
                                SceneFadeInOut.LoadLevel((int)GameConfig.Scene.GameScene);
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
        }
    }
#endif

    bool BackProcess()
    {
        return false;
    }

    void Initialize()
    {
        m_GachaResultIndex = -1;
        m_EnableMenu = true;

        moneyLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));
        adButtonLabel.text = string.Format(TextDocs.content("BT_ADVIEW_TEXT"), EtcDocs.instance.idleAdRewardCash);
        commnetLabel.text = TextDocs.content("UI_GACHA_DESC");
        gachaButtonLabel.text = TextDocs.content("BT_GACHA_NOTICE");
        gachaButtonCashLabel.text = string.Format("{0}", EtcDocs.instance.gachaCash);
        exitButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");

        for (int i = 0; i < logoObjectArray.Length; i++)
        {
            NGUITools.SetActive(logoObjectArray[i], false);
        }
        GameConfig.SupportLanguage selectLanguage = m_UserMgr.selectLanguage;
        if (selectLanguage == GameConfig.SupportLanguage.Japanese)
        {
            NGUITools.SetActive(logoObjectArray[1], true);
        }
        else if (selectLanguage == GameConfig.SupportLanguage.Chinese_S)
        {
            NGUITools.SetActive(logoObjectArray[2], true);
        }
        else if (selectLanguage == GameConfig.SupportLanguage.Chinese_T)
        {
            NGUITools.SetActive(logoObjectArray[3], true);
        }
        else
        {
            NGUITools.SetActive(logoObjectArray[0], true);
        }

        NGUITools.SetActive(gachaButtonObject, true);
        if (m_UserMgr.cash >= EtcDocs.instance.gachaCash)
        {
            gachaButton.isEnabled = true;
        }
        else
        {
            gachaButton.isEnabled = false;
        }

        gachaInfoAnimator.Play("Lotto_Carinfo_Start");

#if BWTEST
        NGUITools.SetActive(testButtonObject, true);
#endif
    }

    void OnRefreshCash()
    {
        moneyLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));
    }

    public void OnClickGetTokenHelp()
    {
        SoundManager.PlayButtonClickSound();
        PopupManager.instance.ShowLargeOK(TextDocs.content("UI_TOKEN_TEXT2"), TextDocs.content("GET_TOKEN_HELP_TEXT"), null, null);
    }

    public void OnClickExit()
    {
        if(m_State == State.Wait ||
           m_State == State.Finish)
        {
            SoundManager.PlayButtonClickSound();

            if (m_UserMgr.preScene == GameConfig.Scene.ShopScene)
            {
                SceneFadeInOut.LoadLevel((int)GameConfig.Scene.ShopScene);
            }
            else
            {
                m_UserMgr.isDirectGamePlay = false;
                m_UserMgr.isDirectGamePlayWithAD = false;
                SceneFadeInOut.LoadLevel((int)GameConfig.Scene.GameScene);
            }
        }
    }

    void OnHideVideoAd(bool isSuccess)
    {
        if (isSuccess)
        {
            m_UserMgr.cash += EtcDocs.instance.idleAdRewardCash;
            m_UserMgr.SaveUserData();

            moneyLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));

            if (m_UserMgr.cash >= EtcDocs.instance.gachaCash)
            {
                gachaButton.isEnabled = true;
            }
        }
    }

    public void OnClickShowAd()
    {
        SoundManager.PlayButtonClickSound();

        if (m_EnableMenu)
        {
            CommonAdManager.instance.Show(CommonAdManager.AdType.Video);
        }
    }

    public void OnClickGachTest()
    {
#if BWTEST
        int testCount = 50;
        string temp = "";

        CommonUtil.DestroyChildren(testResultTable.transform);

        for (int i = 0; i < testCount; i++)
        {
            int result = GachaDocs.instance.PlayGacha(true, ref m_IsStaticGacha, ref m_TokenIndex, ref m_TokenCount);
            if (m_TokenIndex != -1)
            {
                temp = string.Format("{0}번 차량", result);
            }
            else
            {
                if(m_TokenIndex == (int)GachaDocs.GachaToken.C)
                {
                    temp = "C 조각";
                }
                else if (m_TokenIndex == (int)GachaDocs.GachaToken.B)
                {
                    temp = "B 조각";
                }
                else if (m_TokenIndex == (int)GachaDocs.GachaToken.A)
                {
                    temp = "A 조각";
                }
                else if (m_TokenIndex == (int)GachaDocs.GachaToken.S)
                {
                    temp = "S 조각";
                }
            }

            UILabel testResultLabel = NGUITools.AddChild(testResultTable.gameObject, testResultLabelPrefab).GetComponent<UILabel>();

            testResultLabel.text = string.Format("{0}번째 시도 -> 결과 {1} - {2} 획득", i + 1, result, temp);
        }

        testResultTable.repositionNow = true;
#endif
    }

    public void OnClickPlayGacha()
    {
        SoundManager.PlayButtonClickSound();

        if (m_EnableMenu &&
           m_UserMgr.cash >= EtcDocs.instance.gachaCash)
        {
            gachaInfoAnimator.Play("Lotto_Carinfo_Start");

            commnetLabel.text = TextDocs.content("UI_GACHA_ACCEPT_DESC");
            
            m_GachaResultIndex = GachaDocs.instance.PlayGacha(false, ref m_IsStaticGacha, ref m_TokenIndex, ref m_TokenCount);

            NGUITools.SetActive(adButtonObject, false);
            NGUITools.SetActive(gachaButtonObject, false);
            NGUITools.SetActive(exitButtonObject, false);

            CommonUtil.DestroyChildren(gachaVehicleRootTransform);

            GameObject gachaObject;
            if (m_GachaResultIndex != -1)
            {
                gachaObject = Instantiate(m_GoodsMgr.vehicleObjectArray[m_GachaResultIndex]) as GameObject;
                gachaObject.transform.parent = gachaVehicleRootTransform;
                gachaObject.transform.localPosition = Vector3.zero;
                gachaObject.transform.localEulerAngles = Vector3.zero;
                gachaObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                //gachaObject.transform.position = Vector3.zero;
                //gachaObject.transform.eulerAngles = Vector3.zero;
                ShopVehicle shopVehicle = gachaObject.GetComponent<ShopVehicle>();
                shopVehicle.SetGacha();
                gachaObject.layer = LayerMask.NameToLayer("Default");
                CommonUtil.ChangeLayersRecursively(gachaObject.transform, "Default");

                // 중복 차량
                if(m_TokenIndex != -1)
                {
                    m_DuplicationVehicle = true;
                }
                else
                {
                    m_DuplicationVehicle = false;
                    m_TokenIndex = -1;
                    m_TokenCount = 0;
                }
            }
            else
            {
                // 오류 났을 경우
                gachaObject = Instantiate(tokenObjectArray[GachaDocs.tokenGI[m_TokenIndex]]) as GameObject;
                gachaObject.transform.parent = gachaVehicleRootTransform;
                gachaObject.transform.localPosition = Vector3.zero;
                gachaObject.transform.localEulerAngles = Vector3.zero;
                gachaObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }

            if (m_State == State.Wait)
            {
                waitGachObject.SetActive(false);
                playingGachaObject.SetActive(true);
            }
            else
            {
                playingGachaAnimator.Play("Lotto_Scene2_Idle");
            }

            m_State = State.Playing;     
        }
    }

    public void ShowGachaInfo()
    {
        if (m_GachaResultIndex != -1)
        {
            VehicleData vehicleData = VehicleDocs.instance.vehicleDataDic[m_GachaResultIndex];
            if (vehicleData != null)
            {
                gachaInfoGradeSprite.spriteName = GameConfig.TokenMark[(int)vehicleData.tokenType];
                gachaInfoNameLabel.text = TextDocs.content(vehicleData.name);
                gachaInfoAnimator.Play("Lotto_Carinfo_Show");
            }
        }
    }

    public void FinishGacha()
    {
        if (m_DuplicationVehicle == false)
        {
            m_State = State.Finish;

            NGUITools.SetActive(adButtonObject, true);
            NGUITools.SetActive(gachaButtonObject, true);
            // 금액이 가챠 뽑을 금액 이상일 때만 활성화
            if (m_UserMgr.cash >= EtcDocs.instance.gachaCash)
            {
                gachaButton.isEnabled = true;
            }
            else
            {
                gachaButton.isEnabled = false;
            }
            NGUITools.SetActive(exitButtonObject, true);

            VehicleData vehicleData = VehicleDocs.instance.vehicleDataDic[m_GachaResultIndex];
            if (vehicleData != null)
            {
                UserManager userMgr = UserManager.instance;
                if (m_IsStaticGacha ||
                    (!m_IsStaticGacha && userMgr.playGachaCount <= 2 && vehicleData.tokenType == GameConfig.TokenType.B) ||
                    (!m_IsStaticGacha && userMgr.playGachaCount <= 4 && vehicleData.tokenType == GameConfig.TokenType.A) ||
                    (!m_IsStaticGacha && userMgr.playGachaCount <= 4 && vehicleData.tokenType == GameConfig.TokenType.S))
                {
                    int reviewed = PlayerPrefs.GetInt("AcceptReviewPopup", 0);
                    if (reviewed == 0)
                    {
                        PopupManager.instance.ShowReviewPopup();
                    }
                }
            }
        }
        else
        {
            commnetLabel.text = string.Format(TextDocs.content("UI_GACHA_OVERLAP_DESC"), m_TokenCount);

            playingGachaAnimator.Play("Lotto_Scene2_Change");
        }        
    }

    public void ChangeGachaVehicle()
    {
        CommonUtil.DestroyChildren(gachaVehicleRootTransform);

        GameObject gachaObject = Instantiate(tokenObjectArray[GachaDocs.tokenGI[m_TokenIndex]]) as GameObject;
        gachaObject.transform.parent = gachaVehicleRootTransform;
        gachaObject.transform.localPosition = Vector3.zero;
        gachaObject.transform.localEulerAngles = Vector3.zero;
        gachaObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

        m_DuplicationVehicle = false;
        m_TokenIndex = -1;
    }
}

﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using IgaworksUnityAOS;

public class IGAWorksManager : Singleton<IGAWorksManager>
{
    void Awake()
    {
        DontDestroyOnLoad(this);

        Debug.Log("IGAWorksManager Awake");

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        IgaworksUnityPluginAOS.InitPlugin();
        IgaworksUnityPluginAOS.Common.startApplication();

        IgaworksUnityPluginAOS.LiveOps.setNotificationIconStyle("notification_icon", "notification_large_icon", "ff9d261c");
#endif
    }

    void Start()
    {
        Debug.Log("IGAWorksManager Start");
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.Common.startSession();
#elif UNITY_IOS
		IgaworksCorePluginIOS.SetCallbackHandler("IGAWorksManager");
		LiveOpsPluginIOS.LiveOpsSetCallbackHandler("IGAWorksManager");
		LiveOpsPluginIOS.liveOpsPopupGetPopupsResponded += HandleLiveOpsPopupGetPopupsResponded;

		IgaworksCorePluginIOS.IgaworksCoreWithAppKey("874134913", "63f8f49fc4e24dda");

#if REAL
        IgaworksCorePluginIOS.SetLogLevel(IgaworksCorePluginIOS.IgaworksCoreLogInfo);
#else
        IgaworksCorePluginIOS.SetLogLevel(IgaworksCorePluginIOS.IgaworksCoreLogTrace);
#endif
#endif

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID || UNITY_IOS
        IgaworksCorePluginIOS.SetIgaworksCoreDelegate ();

        SetUserId();
        RequestPopupResource();
#endif

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.LiveOps.setLiveOpsPopupEventListener ();

        IgaworksUnityPluginAOS.OnLiveOpsCancelPopupBtnClick = mOnLiveOpsPopupCancel;
#elif UNITY_IOS
        // 델리게이트 핸들러 등록
		LiveOpsPluginIOS.LiveOpsSetPopupCloseListener();

		// 델리게이트 등록
		LiveOpsPluginIOS.liveOpsPopupSetPopupCloseListenerCalled += HandleLiveOpsPopupSetPopupCloseListenerCalled;
#endif
    }

    void OnDisable()
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_IOS
		LiveOpsPluginIOS.liveOpsPopupGetPopupsResponded -= HandleLiveOpsPopupGetPopupsResponded;
        LiveOpsPluginIOS.liveOpsPopupSetPopupCloseListenerCalled -= HandleLiveOpsPopupSetPopupCloseListenerCalled;
#endif
    }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
    void mOnLiveOpsPopupCancel()
    {
        //Debug.Log("@@ PopupCancel call");
    }
#elif UNITY_IOS
    // 델리게이트 구현
    public void HandleLiveOpsPopupSetPopupCloseListenerCalled(string popupCloseListenerResult)
    {
        // 팝업 닫기 결과가 델리게이트를 통해 전달
        string[] results = popupCloseListenerResult.Split(',');
        //results[0] : 닫기를 누른 팝업의 팝업 스페이스키
        //results[1] : 닫기를 누른 팝업의 팝업 캠페인 이름
        //results[2] : json형태의 딥링크를 등록해두었다면 해당 json객체의 json string
        //results[3] : 이 팝업 이후 열려야 할 팝업 수 (string 타입)
        //Debug.Log ("IgaworksADSample : HandleLiveOpsPopupSetPopupCloseListenerCalled " + results[0] + ", " + results[1] + ", " + results[2] + ", " + results[3]);    
    }
#endif

    void OnApplicationPause(bool pauseStatus)
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        if (pauseStatus)
        {
            Debug.Log("IGAWorksManager go to Background");
            IgaworksUnityPluginAOS.Common.endSession();
        }
        else
        {
            Debug.Log("IGAWorksManager go to Foreground");
            IgaworksUnityPluginAOS.Common.startSession();
            // 라이브옵스 활성화
            IgaworksUnityPluginAOS.LiveOps.resume();
        }
#endif
    }

    public void SetUserId()
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.Common.setUserId(SystemInfo.deviceUniqueIdentifier);
        //라이브옵스 초기화
        IgaworksUnityPluginAOS.LiveOps.initialize();
#elif UNITY_IOS
		string userId = Convert.ToBase64String(Encoding.UTF8.GetBytes(SystemInfo.deviceUniqueIdentifier));
		Debug.Log("IOS IGAWorks SetUserId : " + SystemInfo.deviceUniqueIdentifier + ", " + userId);
		IgaworksCorePluginIOS.SetUserId(userId);
		//라이브옵스를 초기화합니다.
		LiveOpsPluginIOS.LiveOpsInitPush();
#endif
    }

    public void RequestPopupResource()
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.LiveOps.requestPopupResource();
#elif UNITY_IOS
		Debug.Log("!IGAWorksManager LiveOpsPluginIOS.LiveOpsPopupGetPopups");
        LiveOpsPluginIOS.LiveOpsPopupGetPopups();
#endif
    }

    public void ShowPopup(string spaceKey)
    {
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
            IgaworksUnityPluginAOS.LiveOps.showPopUp(spaceKey);
#elif UNITY_IOS
		    Debug.Log("!IGAWorksManager LiveOpsPluginIOS.LiveOpsPopupShowPopups : " + spaceKey);
            LiveOpsPluginIOS.LiveOpsPopupShowPopups(spaceKey);
#endif
        }
    }

    public void ShowPromotionAd(string spaceKey)
    {
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
            Debug.Log("IGAWorksManager ShowPromotionAd : " + spaceKey);
            IgaworksUnityPluginAOS.Promotion.showAD(spaceKey);
#elif UNITY_IOS
            Debug.Log("IGAWorksManager ShowPromotionAd : " + spaceKey);
            AdBrixPluginIOS.CrossPromotionShowAD(spaceKey);
#endif
        }
    }

    public void StartSession()
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        Debug.Log("IGAWorksManager StartSession");

        IgaworksUnityPluginAOS.Common.startSession();
        // 라이브옵스 활성화
        IgaworksUnityPluginAOS.LiveOps.resume();
#endif
    }

    public void EndSession()
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        Debug.Log("IGAWorksManager EndSession");

        IgaworksUnityPluginAOS.Common.endSession();
#endif
    }

    public void FirstTimeExperience(string activityName)
    {
        Debug.Log("IGAWorksManager firstTimeExperience : " + activityName);
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.Adbrix.firstTimeExperience(activityName);
#elif UNITY_IOS
        AdBrixPluginIOS.FirstTimeExperience(activityName);
#endif
    }

    public void Retention(string activityName)
    {
        Debug.Log("IGAWorksManager retention : " + activityName);
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.Adbrix.retention(activityName);
#elif UNITY_IOS
        AdBrixPluginIOS.Retention(activityName);
#endif
    }

    public void Buy(string activityName)
    {
        Debug.Log("IGAWorksManager Buy : " + activityName);
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.Adbrix.buy(activityName);
#elif UNITY_IOS
        AdBrixPluginIOS.Buy(activityName);
#endif
    }

    public void SetCustomCohort(IgaworksUnityPluginAOS.CohortVariable cohortVariable, string cohort)
    {
        Debug.Log("IGAWorksManager SetCustomCohort : " + cohortVariable + ", " + cohort);
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.Adbrix.setCustomCohort(cohortVariable, cohort);
#elif UNITY_IOS
        AdBrixPluginIOS.SetCustomCohort((int)cohortVariable, cohort);
#endif
    }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_IOS
	public void HandleLiveOpsPopupGetPopupsResponded()
	{
		Debug.Log("!IGAWorksManager HandleLiveOpsPopupGetPopupsResponded");
	}
#endif

    public void SetNormalClientPushEvent(int second, string message, int id, bool alwaysIsShown)
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.LiveOps.setNormalClientPushEvent(second, message, id, alwaysIsShown);
#elif UNITY_IOS
		DateTime pushDateTime = DateTime.Now.AddSeconds (second);
		Debug.Log("SetNormalClientPushEvent : " + pushDateTime.ToString("yyyyMMddHHmmss") + ", " + message);
		LiveOpsPluginIOS.LiveOpsRegisterLocalPushNotification (id, pushDateTime.ToString("yyyyMMddHHmmss"), message, message, null, 1, null);
#endif
    }

    public void CancelClientPushEvent(int id)
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.LiveOps.cancelClientPushEvent(id);
#elif UNITY_IOS
		LiveOpsPluginIOS.LiveOpsCancelLocalPush(id);
#endif

    }

    public void SetTargetingData(string userGroup, int userData)
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        IgaworksUnityPluginAOS.LiveOps.setTargetingData(userGroup, userData);
#elif UNITY_IOS
		LiveOpsPluginIOS.LiveOpsSetTargetingNumberData(userData, userGroup);
#endif
    }
}

﻿using UnityEngine;
using System.Collections;
using Heyzap;

public class CommonAdManager : Singleton<CommonAdManager>
{
    public enum AdType
    {
        Interstitial = 0,
        Video,
    }

    public delegate void OnAdmobInterstitialResultDelegate(bool isSuccess);
    public static OnAdmobInterstitialResultDelegate OnAdmobInterstitialResult = delegate (bool isSuccess) { };

    public delegate void OnHideInterstitialAdDelegate(bool isSuccess);
    public static OnHideInterstitialAdDelegate OnHideInterstitialAd = delegate (bool isSuccess) { };

    public delegate void OnHideVideoAdDelegate(bool isSuccess);
    public static OnHideVideoAdDelegate OnHideVideoAd = delegate (bool isSuccess) { };

    private bool m_Initialize = false;
    public bool initialize
    {
        get
        {
            return m_Initialize;
        }
        set
        {
            m_Initialize = value;
        }
    }

    private bool m_IsDirectShowInterstisial = false;
    //private bool m_IsLoadShowInterstisial = false;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        GoogleMobileAd.OnInterstitialLoaded += OnInterstisialsLoaded;
        GoogleMobileAd.OnInterstitialOpened += OnInterstisialsOpen;
        GoogleMobileAd.OnInterstitialClosed += OnInterstisialsClosed;
        GoogleMobileAd.OnInterstitialFailedLoading += OnInterstitialFailedLoading;
    }

    void OnDestroy()
    {
        GoogleMobileAd.OnInterstitialLoaded -= OnInterstisialsLoaded;
        GoogleMobileAd.OnInterstitialOpened -= OnInterstisialsOpen;
        GoogleMobileAd.OnInterstitialClosed -= OnInterstisialsClosed;
        GoogleMobileAd.OnInterstitialFailedLoading -= OnInterstitialFailedLoading;
    }

    public void Initialize()
    {
//#if !UNITY_EDITOR_WIN && !UNITY_EDITOR
        if (m_Initialize == false)
        {
            HeyzapAds.Start("92ac1ebcad5dae69687edea3b85b4320", HeyzapAds.FLAG_DISABLE_AUTOMATIC_FETCHING);
            //HeyzapAds.Start("b91a9e15fc48836d53d4cbe4c3af9f0e", HeyzapAds.FLAG_DISABLE_AUTOMATIC_FETCHING);

            GoogleMobileAd.Init();

            //HZInterstitialAd.SetDisplayListener(delegate (string adState, string adTag)
            //{
            //    Debug.Log("@#$ INTERSTITIAL: " + adState + " Tag : " + adTag);
            //});
            //HeyzapAds.NetworkCallbackListener networkCallbackListner = delegate (string network, string callback)
            //{
            //    Debug.Log("@#$ [" + network + "]: " + callback);
            //};
            //HeyzapAds.SetNetworkCallbackListener(networkCallbackListner);

            //HZVideoAd.SetDisplayListener(delegate (string adState, string adTag)
            //{
            //    Debug.Log("@#$ VIDEO: " + adState + " Tag : " + adTag);
            //});

            //HZInterstitialAd.ChartboostFetchForLocation("Default");
            //HZInterstitialAd.ChartboostIsAvailableForLocation("Default");
            //HZInterstitialAd.ChartboostShowForLocation("Default");

            HZInterstitialAd.SetDisplayListener(InterstitialAdDisplayListener);
            //HZVideoAd.SetDisplayListener(VideoAdDisplayListener);
            HZIncentivizedAd.SetDisplayListener(VideoAdDisplayListener);
            HeyzapAds.SetNetworkCallbackListener(NetworkCallbackListener);

            m_Initialize = true;
        }
//#endif
    }

    IEnumerator ShowAdWithInitialize(AdType adType)
    {
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            PopupManager.instance.ShowLoadingPopup();

            Initialize();

            yield return new WaitForSeconds(1.0f);

            PopupManager.instance.HideLoadingPopup();

            if(adType == AdType.Video)
            {
                ShowVideoAd();
            }
            else
            {
                ShowInterstitialAd();
            }
        }
    }

    public void Show(AdType adType)
    {
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            if (m_Initialize == false)
            {
                StartCoroutine(ShowAdWithInitialize(adType));
            }
            else
            {
                if (adType == AdType.Video)
                {
                    ShowVideoAd();
                }
                else
                {
                    ShowInterstitialAd();
                }
            }                
        }
        else
        {
            if (adType == AdType.Video)
            {
                if (OnHideVideoAd != null)
                {
                    OnHideVideoAd(false);
                }
            }
            else
            {
                if (OnHideInterstitialAd != null)
                {
                    OnHideInterstitialAd(false);
                }
            }            

            PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                    TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);
        }
    }

    public void ShowMediationTestSuite()
    {
        HeyzapAds.ShowMediationTestSuite();
    }

    IEnumerator FetchAdWithInitialize(AdType adType)
    {
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            PopupManager.instance.ShowLoadingPopup();

            Initialize();

            yield return new WaitForSeconds(1.0f);

            PopupManager.instance.HideLoadingPopup();

            if (adType == AdType.Video)
            {
                HZVideoAd.Fetch();
            }
            else
            {
                HZInterstitialAd.Fetch();
            }
        }
    }

    public void Fetch(AdType adType)
    {
		Debug.Log ("@@ Fetch 0 : " + adType);
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            if (m_Initialize == false)
            {
				Debug.Log ("@@ Fetch 1 : " + adType);
                StartCoroutine(FetchAdWithInitialize(adType));
            }
            else
            {
                if (adType == AdType.Video)
                {
					Debug.Log ("@@ Fetch 2 : " + adType);
                    //HZVideoAd.Fetch();
                    HZIncentivizedAd.Fetch();
                }
                else
                {
					Debug.Log ("@@ Fetch 2 : " + adType);
                    HZInterstitialAd.Fetch();
                }
            }
        }
        else
        {
			Debug.Log ("@@ Fetch xxxx : " + adType);
            //PopupManager.instance.ShowPurchaseOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
            //        TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);
        }
    }

    void ShowVideoAd()
    {
        Debug.Log("@#$ ShowVideoAd 1 -> " + HZVideoAd.IsAvailable());
        ////if (HZVideoAd.IsAvailable())
        //if (HZIncentivizedAd.IsAvailable())
        //{
        //    Debug.Log("@#$ ShowVideoAd 2");
        ////    HZVideoAd.Show();
        //    HZIncentivizedAd.Show();
        //}
        //else
        //{
        //    Debug.Log("@#$ ShowVideoAd 3");
        ////    HZVideoAd.Fetch();
        //    HZIncentivizedAd.Fetch();
        //}

#if UNITY_EDITOR_WIN || UNITY_EDITOR
        PopupManager.instance.ShowSmallOK(TextDocs.content("POPUP_AD_FAIL_TITLE"),
                TextDocs.content("POPUP_AD_FAIL_TEXT"), null, null);

        if (OnHideVideoAd != null)
        {
            OnHideVideoAd(false);
        }
#else
        //HZVideoAd.Show();
        HZIncentivizedAd.Show();
#endif
    }

    void ShowInterstitialAd()
    {
        Debug.Log("@#$ ShowInterstitialAd 1 -> " + HZInterstitialAd.IsAvailable());
        if (HZInterstitialAd.IsAvailable())
        {
            Debug.Log("@#$ ShowInterstitialAd 2");
            HZInterstitialAd.Show();
        }
        else
        {
            Debug.Log("@#$ ShowInterstitialAd 3");
            HZInterstitialAd.Fetch();

            if (OnHideInterstitialAd != null)
            {
                OnHideInterstitialAd(false);
            }
        }
    }

    void InterstitialAdDisplayListener(string state, string tag)
    {
        Debug.Log("@#$ InterstitialAdDisplayListener : " + state + ", " + tag);
        if (state.Equals("show"))
        {
            // Sent when an ad has been displayed.
            // This is a good place to pause your app, if applicable.
        }
        if (state.Equals("hide"))
        {
            // Sent when an ad has been removed from view.
            // This is a good place to unpause your app, if applicable.
            if (OnHideInterstitialAd != null)
            {
                OnHideInterstitialAd(true);
            }
        }
        if (state.Equals("click"))
        {
            // Sent when an ad has been clicked by the user.
        }
        if (state.Equals("failed"))
        {
            // Sent when you call `show`, but there isn't an ad to be shown.
            // Some of the possible reasons for show errors:
            //    - `HeyzapAds.PauseExpensiveWork()` was called, which pauses 
            //      expensive operations like SDK initializations and ad
            //      fetches, andand `HeyzapAds.ResumeExpensiveWork()` has not
            //      yet been called
            //    - The given ad tag is disabled (see your app's Publisher
            //      Settings dashboard)
            //    - An ad is already showing
            //    - A recent IAP is blocking ads from being shown (see your
            //      app's Publisher Settings dashboard)
            //    - One or more of the segments the user falls into are
            //      preventing an ad from being shown (see your Segmentation
            //      Settings dashboard)
            //    - Incentivized ad rate limiting (see your app's Publisher
            //      Settings dashboard)
            //    - One of the mediated SDKs reported it had an ad to show
            //      but did not display one when asked (a rare case)
            //    - The SDK is waiting for a network request to return before an
            //      ad can show
            if (OnHideInterstitialAd != null)
            {
                OnHideInterstitialAd(false);
            }
        }
        if (state.Equals("available"))
        {
            // Sent when an ad has been loaded and is ready to be displayed,
            //   either because we autofetched an ad or because you called
            //   `Fetch`.
        }
        if (state.Equals("fetch_failed"))
        {
            // Sent when an ad has failed to load.
            // This is sent with when we try to autofetch an ad and fail, and also
            //    as a response to calls you make to `Fetch` that fail.
            // Some of the possible reasons for fetch failures:
            //    - Incentivized ad rate limiting (see your app's Publisher
            //      Settings dashboard)
            //    - None of the available ad networks had any fill
            //    - Network connectivity
            //    - The given ad tag is disabled (see your app's Publisher
            //      Settings dashboard)
            //    - One or more of the segments the user falls into are
            //      preventing an ad from being fetched (see your
            //      Segmentation Settings dashboard)
        }
        if (state.Equals("audio_starting"))
        {
            // The ad about to be shown will need audio.
            // Mute any background music.
        }
        if (state.Equals("audio_finished"))
        {
            // The ad being shown no longer needs audio.
            // Any background music can be resumed.
        }
    }

    void VideoAdDisplayListener(string state, string tag)
    {
        Debug.Log("@#$ VideoAdDisplayListener : " + state + ", " + tag);
        if (state.Equals("show"))
        {
            // Sent when an ad has been displayed.
            // This is a good place to pause your app, if applicable.
//#if UNITY_ANDROID
//            if (OnHideVideoAd != null)
//            {
//                OnHideVideoAd(true);
//            }
//#endif
        }
        if (state.Equals("hide"))
        {
            // Sent when an ad has been removed from view.
            // This is a good place to unpause your app, if applicable.
//#if UNITY_IOS
//			if (OnHideVideoAd != null)
//			{
//				OnHideVideoAd(true);
//			}
//#endif
        }
        if (state.Equals("click"))
        {
            // Sent when an ad has been clicked by the user.
        }
        if (state.Equals("failed"))
        {
            // Sent when you call `show`, but there isn't an ad to be shown.
            // Some of the possible reasons for show errors:
            //    - `HeyzapAds.PauseExpensiveWork()` was called, which pauses 
            //      expensive operations like SDK initializations and ad
            //      fetches, andand `HeyzapAds.ResumeExpensiveWork()` has not
            //      yet been called
            //    - The given ad tag is disabled (see your app's Publisher
            //      Settings dashboard)
            //    - An ad is already showing
            //    - A recent IAP is blocking ads from being shown (see your
            //      app's Publisher Settings dashboard)
            //    - One or more of the segments the user falls into are
            //      preventing an ad from being shown (see your Segmentation
            //      Settings dashboard)
            //    - Incentivized ad rate limiting (see your app's Publisher
            //      Settings dashboard)
            //    - One of the mediated SDKs reported it had an ad to show
            //      but did not display one when asked (a rare case)
            //    - The SDK is waiting for a network request to return before an
            //      ad can show
            PopupManager.instance.ShowSmallOK(TextDocs.content("POPUP_AD_FAIL_TITLE"),
                TextDocs.content("POPUP_AD_FAIL_TEXT"), null, null);

            if (OnHideVideoAd != null)
            {
                OnHideVideoAd(false);
            }
        }
        if (state.Equals("available"))
        {
            // Sent when an ad has been loaded and is ready to be displayed,
            //   either because we autofetched an ad or because you called
            //   `Fetch`.
        }
        if (state.Equals("fetch_failed"))
        {
            // Sent when an ad has failed to load.
            // This is sent with when we try to autofetch an ad and fail, and also
            //    as a response to calls you make to `Fetch` that fail.
            // Some of the possible reasons for fetch failures:
            //    - Incentivized ad rate limiting (see your app's Publisher
            //      Settings dashboard)
            //    - None of the available ad networks had any fill
            //    - Network connectivity
            //    - The given ad tag is disabled (see your app's Publisher
            //      Settings dashboard)
            //    - One or more of the segments the user falls into are
            //      preventing an ad from being fetched (see your
            //      Segmentation Settings dashboard)
        }
        if (state.Equals("audio_starting"))
        {
            // The ad about to be shown will need audio.
            // Mute any background music.
        }
        if (state.Equals("audio_finished"))
        {
            // The ad being shown no longer needs audio.
            // Any background music can be resumed.
        }
        if (state.Equals("incentivized_result_complete"))
        {
            if (OnHideVideoAd != null)
            {
                OnHideVideoAd(true);
            }
        }
        if (state.Equals("incentivized_result_incomplete"))
        {
            if (OnHideVideoAd != null)
            {
                OnHideVideoAd(false);
            }
        }
    }

    void NetworkCallbackListener(string network, string callback)
    {
        Debug.Log("@#$ The network(" + network + "), Callback(" + callback + ")");
        //if (callback.Equals(HeyzapAds.NetworkCallback.SHOW))
        //{
        //    Debug.Log("The network: " + network + " showed an ad");
        //}
    }

    // #################################################################################
    IEnumerator ShowInterstitialAdmobWithInitialize()
    {
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            Debug.Log("@#$ ShowInterstitialAdmobWithInitialize 0");

            Initialize();

            yield return new WaitForSeconds(0.1f);

            m_IsDirectShowInterstisial = true;
            GoogleMobileAd.LoadInterstitialAd();
        }
        else
        {
            Debug.Log("@#$ ShowInterstitialAdmobWithInitialize 1");

            if (OnAdmobInterstitialResult != null)
            {
                OnAdmobInterstitialResult(false);
            }
        }
    }

    public void ShowInterstitialAdmob()
    {
        Debug.Log("@#$ ShowInterstitialAdmob 0");
        if (m_Initialize == false)
        {
            Debug.Log("@#$ ShowInterstitialAdmob 0-1");
            StartCoroutine(ShowInterstitialAdmobWithInitialize());
        }
        else
        {
            Debug.Log("@#$ ShowInterstitialAdmob 1");
            if (GoogleMobileAd.IsInterstitialReady)
            {
                Debug.Log("@#$ ShowInterstitialAdmob 1-1");
                GoogleMobileAd.ShowInterstitialAd();
            }
            else
            {
                Debug.Log("@#$ ShowInterstitialAdmob 1-2");
                m_IsDirectShowInterstisial = true;
                GoogleMobileAd.LoadInterstitialAd();
            }
        }
    }

	IEnumerator PrepareLoadInterstitialAdWithInitialize()
	{
		if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
		{
			Debug.Log("@#$ PrepareLoadInterstitialAdWithInitialize 0");

			Initialize();

			yield return new WaitForSeconds(0.1f);

			m_IsDirectShowInterstisial = false;
			GoogleMobileAd.LoadInterstitialAd();
		}
		else
		{
			Debug.Log("@#$ PrepareLoadInterstitialAdWithInitialize 1");
		}
	}

    public void PrepareLoadInterstitialAd()
    {
        Debug.Log("@#$ PrepareLoadInterstitialAd");

		if (m_Initialize == false)
		{
			StartCoroutine (PrepareLoadInterstitialAdWithInitialize ());
		}
		else
		{
			m_IsDirectShowInterstisial = false;
			//m_IsLoadShowInterstisial = false;
			GoogleMobileAd.LoadInterstitialAd();
		}        
    }

    private void OnInterstisialsLoaded()
    {
        Debug.Log("@#$ OnInterstisialsLoaded");

        //m_IsLoadShowInterstisial = true;

        if (m_IsDirectShowInterstisial)
        {
            GoogleMobileAd.ShowInterstitialAd();
        }
    }

    private void OnInterstisialsOpen()
    {
        Debug.Log("@#$ OnInterstisialsOpen");

        SoundManager.instance.PauseBGM();
    }

    private void OnInterstisialsClosed()
    {
        Debug.Log("@#$ OnInterstisialsClosed");

        SoundManager.instance.UnPauseBGM();

        if(OnAdmobInterstitialResult != null)
        {
            OnAdmobInterstitialResult(true);
        }
    }

    private void OnInterstitialFailedLoading()
    {
        Debug.Log("@#$ OnInterstitialFailedLoading");

        SoundManager.instance.UnPauseBGM();

        if (OnAdmobInterstitialResult != null)
        {
            OnAdmobInterstitialResult(false);
        }
    }
}

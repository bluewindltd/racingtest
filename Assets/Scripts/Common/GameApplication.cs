﻿using UnityEngine;
using System;
using System.Collections;
using Facebook.Unity;

public class GameApplication : Singleton<GameApplication>
{
    long m_StartPlayTimeTicks;
    int m_StartPlayTime;
    int m_PlayTime;
    public int playTime
    {
        get
        {
            return m_PlayTime;
        }
    }

    long m_ElapsedTimeL = 0;
    int m_ElapsedTime = 0;
    DateTime m_NowTime;

    bool m_isForground = false;
    public bool isForground
    {
        get
        {
            return m_isForground;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#else
        Debug.Log("!! Facebook Init! : " + FacebookSettings.AppId);
        FB.Init(InitComplete);
#endif

        m_StartPlayTime = PlayerPrefs.GetInt("PlayTime");
        m_NowTime = System.DateTime.Now;
        m_StartPlayTimeTicks = m_NowTime.Ticks;

        int systemMemorySize = SystemInfo.systemMemorySize;
        Debug.Log("FrameCheck : " + systemMemorySize);
        if (systemMemorySize >= 3500)
        {
            Application.targetFrameRate = GameConfig.HighFrameRate;
            Time.fixedDeltaTime = 1.0f / GameConfig.HighFixedFrameRate;
        }
        else
        {
            Application.targetFrameRate = GameConfig.LowFrameRate;
            Time.fixedDeltaTime = 1.0f / GameConfig.LowFixedFrameRate;
        }

#if UNITY_IOS
        //IOSNotificationController.OnNotificationScheduleResult += OnNotificationScheduleResult;
#endif

        StartCoroutine(CheckPlayTime());
    }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#else
    void InitComplete()
    {
        Debug.Log("!! Facebook InitComplete!");

        FB.ActivateApp();
        if (Constants.IsMobile)
        {
            FB.Mobile.FetchDeferredAppLinkData(DeepLinkCallback);
        }
    }

    void DeepLinkCallback(IAppLinkResult response)
    {
        Debug.Log("!! Facebook DeepLinkCallback");
        if (response.Error != null)
        {
            Debug.Log("DeepLinkCallback Error -> " + response.Error);
        }
    }
#endif

    void OnDestroy()
    {
#if UNITY_IOS
        //IOSNotificationController.OnNotificationScheduleResult -= OnNotificationScheduleResult;
#endif
    }

    IEnumerator CheckPlayTime()
    {
        while (true)
        {
            m_NowTime = System.DateTime.Now;
            m_ElapsedTimeL = m_NowTime.Ticks - m_StartPlayTimeTicks;
            TimeSpan elapsedSpan = new TimeSpan(m_ElapsedTimeL);
            m_ElapsedTime = (int)elapsedSpan.TotalSeconds;
            m_PlayTime = m_StartPlayTime + m_ElapsedTime;

            yield return new WaitForSeconds(1.0f);
        }
    }

#if UNITY_IOS
    //void OnNotificationScheduleResult(ISN_Result res)
    //{
    //}
#endif

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Debug.Log("GameApplication go to Background");

            PlayerPrefs.SetInt("PlayTime", m_PlayTime);

            int pushSecond = EtcDocs.instance.freeGiftTime * 60;
            if (TimeManager.instance.GetTimeState())
            {
                long time = TimeManager.instance.GetOnlineTime();
                int tempPushSecond = (int)((float)(UserManager.instance.freeGiftTime - time) / 1000.0f);
                // 무료 선물 받기까지 10분도 안남은 경우는 10분으로 고정 시킨다.
                if(tempPushSecond <= 600)
                {
                    pushSecond = 600;
                }
                else
                {
                    pushSecond = tempPushSecond;
                }
            }

            Debug.Log("ScheduleLocalNotification! : " + pushSecond);

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#else
            //IGAWorksManager.instance.SetNormalClientPushEvent(pushSecond, TextDocs.content("PUSH_TEXT"), 77, false);

            //AndroidNotificationManager.Instance.ScheduleLocalNotification(TextDocs.content("PUSH_TITLE"), TextDocs.content("PUSH_TEXT"), pushSecond);
            //AndroidNotificationManager.Instance.ScheduleLocalNotification(TextDocs.content("PUSH_TITLE"), TextDocs.content("PUSH_TEXT"), 10);
			/*
			ISN_LocalNotification notification = new ISN_LocalNotification(DateTime.Now.AddSeconds(pushSecond), TextDocs.content("PUSH_TEXT"), true);
            //notification.SetData("some_test_data");
            //notification.SetSoundName("purchase_ok.wav");
            notification.SetBadgesNumber(1);
            notification.Schedule();
            */
#endif

            m_isForground = false;
        }
        else
        {
            Debug.Log("GameApplication go to Foreground");

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#else
            if (FB.IsInitialized)
            {
                Debug.Log("!! Facebook OnApplicationPause Forground -> ActivateApp!");
                FB.ActivateApp();
            }
#endif

            m_StartPlayTime = PlayerPrefs.GetInt("PlayTime");
            m_StartPlayTimeTicks = System.DateTime.Now.Ticks;

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#else
            Debug.Log("CancelAllLocalNotifications!");
            //IGAWorksManager.instance.CancelClientPushEvent(77);

            //AndroidNotificationManager.Instance.CancelAllLocalNotifications();
            //IOSNotificationController.Instance.ApplicationIconBadgeNumber(0);
            //IOSNotificationController.Instance.CancelAllLocalNotifications();
#endif

            m_isForground = true;
        }
    }

#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_ANDROID
    public void OnInjectionDetected()
    {
        Debug.Log("Injection Detected!");
        //IGAWorksManager.instance.Retention("InjectionDetected");
    }
#endif
}

﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using BW.Common;
using CodeStage.AntiCheat.ObscuredTypes;
using MadUtil;

public class VehicleInfo
{
    public enum State
    {
        NotHave = 0,
        Purchased = 1,
        Boarding = 2,
    }

    public int index;
    public VehicleData vehicleData;
    public State state;
}

public class PetInfo
{
    public enum State
    {
        NotHave = 0,
        Purchased = 1,
        Boarding = 2,
    }

    public int index;
    public PetData petData;
    public State state;
}

public class UserManager : Singleton<UserManager>
{
    enum GPCConnectRequest
    {
        NONE = 0,
        SAVE,
    }
    GPCConnectRequest m_GPConnectRequest;
    bool m_RequestGPConnect = false;

    public enum IAPPlatform
    {
        Android = 0,
        IOS = 1,
    }

    public enum IAPState
    {
        None = -1,
        Init = 0,
        Purchase = 1,
        Restore = 2,
    }
    IAPState m_IAPState = IAPState.None;

    public enum PurchaseCompleteState
    {
        Fail = 0,
        PurchaseError = 1,
        Success = 2,
    }

    // Delegate
    public delegate void OnGPConnectionResultDelegate(bool isSuccess);
    public static OnGPConnectionResultDelegate OnGPConnectionResult = delegate (bool isSuccess) { };

    public delegate void RefreshCashDelegate();
    public static RefreshCashDelegate OnRefreshCash = delegate () { };

    public delegate void OnGameCenterAuthCompleteDelegate(bool isLogin);
    public static OnGameCenterAuthCompleteDelegate OnGameCenterAuthComplete = delegate (bool isLogin) { };

    public delegate void PurchaseCompleteDelegate(PurchaseCompleteState purchaseCompleteState);
    public static PurchaseCompleteDelegate OnPurchaseComplete = delegate (PurchaseCompleteState purchaseCompleteState) { };
    public delegate void GetRestoreInfoDelegate(int restoreProductCount);
    public static GetRestoreInfoDelegate OnGetRestoreInfo = delegate (int restoreProductCount) { };

    bool m_isPrepare = false;
    public bool isPrepare
    {
        get
        {
            return m_isPrepare;
        }
        set
        {
            m_isPrepare = value;
        }
    }

    private string m_EncryptKey = "";
    public string encryptKey
    {
        get
        {
            return m_EncryptKey;
        }
        set
        {
            m_EncryptKey = value;
        }
    }

    private int m_StartCash = 0;
    public int startCash
    {
        get
        {
            return m_StartCash;
        }
        set
        {
            m_StartCash = value;
        }
    }

    Dictionary<int, VehicleInfo> m_VehicleInfoDic = new Dictionary<int, VehicleInfo>();
    public Dictionary<int, VehicleInfo> vehicleInfoDic
    {
        get
        {
            return m_VehicleInfoDic;
        }
    }

    Dictionary<int, PetInfo> m_PetInfoDic = new Dictionary<int, PetInfo>();
    public Dictionary<int, PetInfo> petInfoDic
    {
        get
        {
            return m_PetInfoDic;
        }
    }

    //VehicleDocs m_VehicleDocs = null;

    private GameConfig.SupportLanguage m_SelectLanguage = GameConfig.SupportLanguage.None;
    public GameConfig.SupportLanguage selectLanguage
    {
        get
        {
            return m_SelectLanguage;
        }
        set
        {
            m_SelectLanguage = value;
        }
    }

    int m_SelectVehicleIndex = 0;
    public int selectVehicleIndex
    {
        get
        {
            return m_SelectVehicleIndex;
        }
        set
        {
            m_SelectVehicleIndex = value;
        }
    }

    int m_SelectPetIndex = 0;
    public int selectPetIndex
    {
        get
        {
            return m_SelectPetIndex;
        }
        set
        {
            m_SelectPetIndex = value;
        }
    }

    private int m_Cash = 0;
    public int cash
    {
        get
        {
            return m_Cash;
        }
        set
        {
            m_Cash = value;

            SaveUserData();

            if (OnRefreshCash != null)
            {
                OnRefreshCash();
            }
        }
    }

    private Dictionary<int, int> m_TokenDic = new Dictionary<int, int>();
    public Dictionary<int, int> tokenDic
    {
        get
        {
            return m_TokenDic;
        }
    }

    int m_DuplicationGachaCount = 0;
    public int duplicationGachaCount
    {
        get
        {
            return m_DuplicationGachaCount;
        }
        set
        {
            m_DuplicationGachaCount = value;
        }
    }

    private int m_PlayGiftCount = 0;
    public int playGiftCount
    {
        get
        {
            return m_PlayGiftCount;
        }
        set
        {
            m_PlayGiftCount = value;
        }
    }

    private long m_FreeGiftTime = 0;
    public long freeGiftTime
    {
        get
        {
            return m_FreeGiftTime;
        }
        set
        {
            m_FreeGiftTime = value;
        }
    }

    private int m_PlayGachaCount = 0;
    public int playGachaCount
    {
        get
        {
            return m_PlayGachaCount;
        }
        set
        {
            m_PlayGachaCount = value;
        }
    }

    private bool m_IsPlayBStaticGacha = false;
    public bool isPlayBStaticGacha
    {
        get
        {
            return m_IsPlayBStaticGacha;
        }
        set
        {
            m_IsPlayBStaticGacha = value;
        }
    }

    private bool m_IsPlayAStaticGacha = false;
    public bool isPlayAStaticGacha
    {
        get
        {
            return m_IsPlayAStaticGacha;
        }
        set
        {
            m_IsPlayAStaticGacha = value;
        }
    }

    private bool m_IsPurchased = false;
    public bool isPurchased
    {
        get
        {
            return m_IsPurchased;
        }
        set
        {
            m_IsPurchased = value;
        }
    }

    private int m_PurchaseCount = 0;
    public int purchaseCount
    {
        get
        {
            return m_PurchaseCount;
        }
        set
        {
            m_PurchaseCount = value;
        }
    }

    private Dictionary<int, bool> m_PackagePurchaseDic = new Dictionary<int, bool>();
    public Dictionary<int, bool> packagePurchaseDic
    {
        get
        {
            return m_PackagePurchaseDic;
        }
    }

    private int m_CurPetExperienceCount = 0;
    public int curPetExperienceCount
    {
        get
        {
            return m_CurPetExperienceCount;
        }
        set
        {
            m_CurPetExperienceCount = value;
        }
    }

    private bool m_EnablePetExperience = false;
    public bool enablePetExperience
    {
        get
        {
            return m_EnablePetExperience;
        }
        set
        {
            m_EnablePetExperience = value;
        }
    }

#if UNITY_ANDROID
    private long m_AOSLeaderBoardTime = 0;
    public long aosLeaderBoardTime
    {
        get
        {
            return m_AOSLeaderBoardTime;
        }
        set
        {
            m_AOSLeaderBoardTime = value;
        }
    }

    private long m_AOSLeaderBoardCount = 0;
    public long aosLeaderBoardCount
    {
        get
        {
            return m_AOSLeaderBoardCount;
        }
        set
        {
            m_AOSLeaderBoardCount = value;
        }
    }
#endif

    private bool m_Intialize = false;
    public bool intialize
    {
        get
        {
            return m_Intialize;
        }
        set
        {
            m_Intialize = value;
        }
    }

    private Dictionary<string, string> m_RestoreProductDic = new Dictionary<string, string>();
    public Dictionary<string, string> restoreProductDic
    {
        get
        {
            return m_RestoreProductDic;
        }
    }

    private int m_CheckRestoreCount = 0;
    public int checkRestoreCount
    {
        get
        {
            return m_CheckRestoreCount;
        }
    }

    private bool m_IsRestore = false;
    private VehicleData m_ProccessVehicleData = null;
    private PackageData m_ProccessPackageData = null;
    private PetData m_ProccessPetData = null;

#if UNITY_IOS
	private bool m_IsCloudInit = false;
	public bool isCloudInit
	{
		get
		{
			return m_IsCloudInit;
		}
		set
		{
			m_IsCloudInit = value;
		}
	}

	private bool m_IsFirstLoginGameCenter = false;
#endif

    private bool m_LoadStore = false;
    public bool loadStore
    {
        get
        {
            return m_LoadStore;
        }
    }

    private bool m_IsDirectGamePlay = false;
    public bool isDirectGamePlay
    {
        get
        {
            return m_IsDirectGamePlay;
        }
        set
        {
            m_IsDirectGamePlay = value;
        }
    }

    private bool m_IsDirectGamePlayWithAD = false;
    public bool isDirectGamePlayWithAD
    {
        get
        {
            return m_IsDirectGamePlayWithAD;
        }
        set
        {
            m_IsDirectGamePlayWithAD = value;
        }
    }

    private bool m_IsShowStartInterstitialAd = false;
    public bool isShowStartInterstitialAd
    {
        get
        {
            return m_IsShowStartInterstitialAd;
        }
        set
        {
            m_IsShowStartInterstitialAd = value;
        }
    }

    private bool m_IsSSLTrust = false;
    public bool isSSLTrust
    {
        get
        {
            return m_IsSSLTrust;
        }
        set
        {
            m_IsSSLTrust = value;
        }
    }

    private GameConfig.Scene m_PreScene = GameConfig.Scene.GameScene;
    public GameConfig.Scene preScene
    {
        get
        {
            return m_PreScene;
        }
        set
        {
            m_PreScene = value;
        }
    }

    InternetReachabilityVerifier.Status m_PreInternetState = InternetReachabilityVerifier.Status.PendingVerification;
    bool m_IsFirstInternetCheck = false;
    public bool isFirstInternetCheck
    {
        get
        {
            return m_IsFirstInternetCheck;
        }
    }
    float m_InternetCheckTime = 0.0f;

    bool m_IsPrevSceneIntro = true;
    public bool isPrevSceneIntro
    {
        get
        {
            return m_IsPrevSceneIntro;
        }
        set
        {
            m_IsPrevSceneIntro = value;
        }
    }

    int m_ScreenWidth = 0;
    int m_ScreenHeight = 0;

    ObscuredString m_DeveloperPayLoad = "";
    ObscuredString m_DaeriUrl = "https://pf.daerisoft.com";
    ObscuredString m_ServiceName = "BurnOut";
    ObscuredString m_DaeriKey = "qmf7ek0fn2dp1dnls3fl";
    //                           12345678901234567890123456789012


    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        InternetReachabilityVerifier.Instance.statusChangedDelegate += OnNetStatusChanged;
        StartCoroutine(StartInternetCheck());

        //#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        GooglePlayConnection.ActionConnectionResultReceived += OnConnectionResultReceived;

        AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;
        AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;
        AndroidInAppPurchaseManager.ActionProductConsumed += OnProductConsumed;

        GooglePlaySavedGamesManager.ActionConflict += OnConflict;
#elif UNITY_IOS
		GameCenterManager.OnAuthFinished += OnAuthFinished;

		IOSInAppPurchaseManager.OnStoreKitInitComplete += OnStoreKitInitComplete;
		IOSInAppPurchaseManager.OnVerificationComplete += OnVerificationComplete;
		IOSInAppPurchaseManager.OnTransactionComplete += OnTransactionComplete;
		IOSInAppPurchaseManager.OnRestoreComplete += OnRestoreComplete;
#endif
    }

    void OnDestroy()
    {
        //#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        GooglePlayConnection.ActionConnectionResultReceived -= OnConnectionResultReceived;

        AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;
        AndroidInAppPurchaseManager.ActionProductPurchased -= OnProductPurchased;
        AndroidInAppPurchaseManager.ActionProductConsumed -= OnProductConsumed;

        GooglePlaySavedGamesManager.ActionConflict -= OnConflict;
#elif UNITY_IOS
		GameCenterManager.OnAuthFinished -= OnAuthFinished;

		IOSInAppPurchaseManager.OnStoreKitInitComplete -= OnStoreKitInitComplete;
		IOSInAppPurchaseManager.OnVerificationComplete -= OnVerificationComplete;
		IOSInAppPurchaseManager.OnTransactionComplete -= OnTransactionComplete;
		IOSInAppPurchaseManager.OnRestoreComplete -= OnRestoreComplete;
#endif
    }

    IEnumerator StartInternetCheck()
    {
        Debug.Log("Prepare StartInternetCheck 0");
        while (m_IsFirstInternetCheck == false && m_InternetCheckTime > 5.0f)
        {
            m_InternetCheckTime += Time.deltaTime;

            Debug.Log("Prepare StartInternetCheck 1 : " + m_InternetCheckTime);

            yield return null;
        }

        Debug.Log("Prepare StartInternetCheck 2 : " + m_IsFirstInternetCheck + ", " + m_PreInternetState);

        // 5초가 지났는데도 PendingVerification 상태라면 걍 m_IsFirstInternetCheck를 true로 바꿔준다.
        if (m_IsFirstInternetCheck == false &&
            m_PreInternetState == InternetReachabilityVerifier.Status.PendingVerification)
        {
            Debug.Log("Prepare StartInternetCheck 3 : " + m_IsFirstInternetCheck);
            m_IsFirstInternetCheck = true;
        }
    }

    void OnNetStatusChanged(InternetReachabilityVerifier.Status newStatus)
    {
        Debug.Log("Prepare OnNetStatusChanged Status : " + newStatus + ", " + m_IsFirstInternetCheck);

        if (m_IsFirstInternetCheck == false &&
            newStatus != InternetReachabilityVerifier.Status.PendingVerification)
        {
            m_IsFirstInternetCheck = true;
        }

        m_PreInternetState = newStatus;
    }

    public string SaveUserData()
    {
        JSONClass rootNode = new JSONClass();
        JSONClass jsonUserData = new JSONClass();
        rootNode.Add("user_data", jsonUserData);

        jsonUserData.Add("selectLanguage", new JSONData((int)m_SelectLanguage));
        jsonUserData.Add("selectVehicleIndex", new JSONData((int)m_SelectVehicleIndex));
        jsonUserData.Add("selectPetIndex", new JSONData((int)m_SelectPetIndex));
        jsonUserData.Add("cash", new JSONData(m_Cash));
        JSONArray jsonTokens = new JSONArray();
        jsonUserData.Add("tokens", jsonTokens);
        Dictionary<int, int>.Enumerator etor = m_TokenDic.GetEnumerator();
        while (etor.MoveNext())
        {
            JSONClass jsonTokenClass = new JSONClass();
            jsonTokenClass.Add("index", new JSONData(etor.Current.Key));
            jsonTokenClass.Add("count", new JSONData(etor.Current.Value));
            jsonTokens.Add(jsonTokenClass);
        }
        jsonUserData.Add("duplicationGachaCount", new JSONData(m_DuplicationGachaCount));
        jsonUserData.Add("playGiftCount", new JSONData(m_PlayGiftCount));
        jsonUserData.Add("freeGiftTime", new JSONData(m_FreeGiftTime));
        jsonUserData.Add("playGachaCount", new JSONData(m_PlayGachaCount));
        jsonUserData.Add("playBStaticGacha", new JSONData(m_IsPlayBStaticGacha));
        jsonUserData.Add("playAStaticGacha", new JSONData(m_IsPlayAStaticGacha));
        jsonUserData.Add("purchased", new JSONData(m_IsPurchased));
        jsonUserData.Add("purchaseCount", new JSONData(m_PurchaseCount));
        JSONArray jsonPackagePurchases = new JSONArray();
        jsonUserData.Add("packagePurchases", jsonPackagePurchases);
        Dictionary<int, bool>.Enumerator packageEtor = m_PackagePurchaseDic.GetEnumerator();
        while (packageEtor.MoveNext())
        {
            JSONClass jsonPackagePurchaseClass = new JSONClass();
            jsonPackagePurchaseClass.Add("index", new JSONData(packageEtor.Current.Key));
            jsonPackagePurchaseClass.Add("complete", new JSONData(packageEtor.Current.Value));
            jsonPackagePurchases.Add(jsonPackagePurchaseClass);
        }
        jsonUserData.Add("curPetExperienceCount", new JSONData(m_CurPetExperienceCount));
        jsonUserData.Add("enablePetExperience", new JSONData(m_EnablePetExperience));

#if UNITY_ANDROID
        jsonUserData.Add("aosLeaderBoardTime", new JSONData(m_AOSLeaderBoardTime));
        jsonUserData.Add("aosLeaderBoardCount", new JSONData(m_AOSLeaderBoardCount));
#endif

#if UNITY_EDITOR_WIN || UNITY_EDITOR
        string saveString = Convert.ToBase64String(Encoding.UTF8.GetBytes(rootNode.ToString()));
#else
        string saveString = Convert.ToBase64String(easy.Crypto.encrypt(rootNode.ToString(), m_EncryptKey));
#endif

        ObscuredPrefs.SetString("UserData", saveString);

        return saveString;
    }

    public void LoadUserData()
    {
        string loadString = ObscuredPrefs.GetString("UserData", "");
        if (loadString.CompareTo("") != 0)
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
            JSONNode json = JSON.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(loadString)));
#else
            JSONNode json = JSON.Parse(easy.Crypto.decrypt(Convert.FromBase64String(loadString), m_EncryptKey));
#endif
            JSONNode jsonUserData = json["user_data"];

            m_SelectLanguage = (GameConfig.SupportLanguage)jsonUserData["selectLanguage"].AsInt;
            m_SelectVehicleIndex = jsonUserData["selectVehicleIndex"].AsInt;
            m_SelectPetIndex = jsonUserData["selectPetIndex"].AsInt;
            m_Cash = jsonUserData["cash"].AsInt;
            m_TokenDic.Clear();
            JSONArray jsonTokens = jsonUserData["tokens"].AsArray;
            for (int i = 0; i < jsonTokens.Count; i++)
            {
                JSONNode jsonTokenClass = jsonTokens[i];
                int tokenIndex = jsonTokenClass["index"].AsInt;
                int tokenCount = jsonTokenClass["count"].AsInt;
                m_TokenDic.Add(tokenIndex, tokenCount);
            }
            m_DuplicationGachaCount = jsonUserData["duplicationGachaCount"].AsInt;
            m_PlayGiftCount = jsonUserData["playGiftCount"].AsInt;
            m_FreeGiftTime = (long)jsonUserData["freeGiftTime"].AsDouble;
            m_PlayGachaCount = jsonUserData["playGachaCount"].AsInt;
            m_IsPlayBStaticGacha = jsonUserData["playBStaticGacha"].AsBool;
            m_IsPlayAStaticGacha = jsonUserData["playAStaticGacha"].AsBool;
            m_IsPurchased = jsonUserData["purchased"].AsBool;
            m_PurchaseCount = jsonUserData["purchaseCount"].AsInt;
            m_PackagePurchaseDic.Clear();
            JSONArray jsonPackagePurchases = jsonUserData["packagePurchases"].AsArray;
            for (int i = 0; i < jsonPackagePurchases.Count; i++)
            {
                JSONNode jsonPackagePurchaseClass = jsonPackagePurchases[i];
                int packageIndex = jsonPackagePurchaseClass["index"].AsInt;
                bool complete = jsonPackagePurchaseClass["complete"].AsBool;
                m_PackagePurchaseDic.Add(packageIndex, complete);
            }
            m_CurPetExperienceCount = jsonUserData["curPetExperienceCount"].AsInt;
            m_EnablePetExperience = jsonUserData["enablePetExperience"].AsBool;
#if UNITY_ANDROID
            m_AOSLeaderBoardTime = (long)jsonUserData["aosLeaderBoardTime"].AsDouble;
            m_AOSLeaderBoardCount = jsonUserData["aosLeaderBoardCount"].AsInt;
#endif
        }
        else
        {
            m_SelectVehicleIndex = 59;
            m_SelectPetIndex = -1;
            m_Cash = m_StartCash;
            for (int i = 0; i < (int)GameConfig.TokenType.Count; i++)
            {
                m_TokenDic.Add(i, 0);
            }

            m_TokenDic[0] = 0;
            m_TokenDic[1] = 0;
            m_TokenDic[2] = 0;
            m_TokenDic[3] = 0;
            m_DuplicationGachaCount = 0;
            m_PlayGiftCount = 0;
            m_FreeGiftTime = 0;
            m_PlayGachaCount = 0;
            m_IsPlayBStaticGacha = false;
            m_IsPlayAStaticGacha = false;
            m_IsPurchased = false;
            m_PurchaseCount = 0;
            m_PackagePurchaseDic.Clear();
            m_CurPetExperienceCount = 0;
            m_EnablePetExperience = false;
#if UNITY_ANDROID
            m_AOSLeaderBoardTime = 0;
            m_AOSLeaderBoardCount = 0;
#endif
        }
    }

    public string SaveOwnVehicleData()
    {
        JSONClass rootNode = new JSONClass();
        JSONClass jsonUserData = new JSONClass();
        rootNode.Add("user_data", jsonUserData);

        JSONArray jsonOwnVehicles = new JSONArray();
        jsonUserData.Add("ownVehicles", jsonOwnVehicles);
        Dictionary<int, VehicleInfo>.Enumerator etor = m_VehicleInfoDic.GetEnumerator();
        while (etor.MoveNext())
        {
            VehicleInfo vehicleInfo = etor.Current.Value;
            JSONClass jsonOwnVehicleClass = new JSONClass();
            jsonOwnVehicleClass.Add("index", new JSONData(vehicleInfo.index));
            jsonOwnVehicles.Add(jsonOwnVehicleClass);
        }

        JSONArray jsonOwnPets = new JSONArray();
        jsonUserData.Add("ownPets", jsonOwnPets);
        Dictionary<int, PetInfo>.Enumerator petEtor = m_PetInfoDic.GetEnumerator();
        while (petEtor.MoveNext())
        {
            PetInfo petInfo = petEtor.Current.Value;
            JSONClass jsonOwnPetClass = new JSONClass();
            jsonOwnPetClass.Add("index", new JSONData(petInfo.index));
            jsonOwnPets.Add(jsonOwnPetClass);
        }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
        string saveString = Convert.ToBase64String(Encoding.UTF8.GetBytes(rootNode.ToString()));
#else
        string saveString = Convert.ToBase64String(easy.Crypto.encrypt(rootNode.ToString(), m_EncryptKey));
#endif

        ObscuredPrefs.SetString("OwnVehiclesData", saveString);

        return saveString;
    }

    public void LoadOwnVehicleData()
    {
        VehicleDocs vehicleDocs = VehicleDocs.instance;
        string loadString = ObscuredPrefs.GetString("OwnVehiclesData", "");
        if (loadString.CompareTo("") != 0)
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
            JSONNode json = JSON.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(loadString)));
#else
            JSONNode json = JSON.Parse(easy.Crypto.decrypt(Convert.FromBase64String(loadString), m_EncryptKey));
#endif
            JSONNode jsonUserData = json["user_data"];
            JSONArray jsonOwnVehicles = jsonUserData["ownVehicles"].AsArray;
            LoadJsonOwnVehicleData(jsonOwnVehicles);

            JSONArray jsonOwnPets = jsonUserData["ownPets"].AsArray;
            LoadJsonOwnPetData(jsonOwnPets);
        }
        else
        {
            m_SelectVehicleIndex = 59;

            m_VehicleInfoDic.Clear();
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.index = 0;
            vehicleInfo.state = VehicleInfo.State.Purchased;
            if (vehicleDocs.vehicleDataDic.ContainsKey(vehicleInfo.index))
            {
                vehicleInfo.vehicleData = vehicleDocs.vehicleDataDic[vehicleInfo.index];
            }
            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

            vehicleInfo = new VehicleInfo();
            vehicleInfo.index = 59;
            vehicleInfo.state = VehicleInfo.State.Boarding;
            if (vehicleDocs.vehicleDataDic.ContainsKey(vehicleInfo.index))
            {
                vehicleInfo.vehicleData = vehicleDocs.vehicleDataDic[vehicleInfo.index];
            }
            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

            m_SelectPetIndex = -1;

            m_PetInfoDic.Clear();
        }
    }

    public string SaveDataToCloud()
    {
        JSONClass rootNode = new JSONClass();
        JSONClass jsonCloudData = new JSONClass();
        rootNode.Add("cloud_data", jsonCloudData);

        jsonCloudData.Add("selectVehicleIndex", new JSONData((int)m_SelectVehicleIndex));
        jsonCloudData.Add("selectPetIndex", new JSONData((int)m_SelectPetIndex));
        jsonCloudData.Add("cash", new JSONData(m_Cash));
        JSONArray jsonTokens = new JSONArray();
        jsonCloudData.Add("tokens", jsonTokens);
        Dictionary<int, int>.Enumerator etor = m_TokenDic.GetEnumerator();
        while (etor.MoveNext())
        {
            JSONClass jsonTokenClass = new JSONClass();
            jsonTokenClass.Add("index", new JSONData(etor.Current.Key));
            jsonTokenClass.Add("count", new JSONData(etor.Current.Value));
            jsonTokens.Add(jsonTokenClass);
        }
        jsonCloudData.Add("duplicationGachaCount", new JSONData(m_DuplicationGachaCount));
        jsonCloudData.Add("playGiftCount", new JSONData(m_PlayGiftCount));
        jsonCloudData.Add("freeGiftTime", new JSONData(m_FreeGiftTime));
        jsonCloudData.Add("playGachaCount", new JSONData(m_PlayGachaCount));
        jsonCloudData.Add("playBStaticGacha", new JSONData(m_IsPlayBStaticGacha));
        jsonCloudData.Add("playAStaticGacha", new JSONData(m_IsPlayAStaticGacha));
        jsonCloudData.Add("purchased", new JSONData(m_IsPurchased));
        jsonCloudData.Add("purchaseCount", new JSONData(m_PurchaseCount));
        JSONArray jsonPackagePurchases = new JSONArray();
        jsonCloudData.Add("packagePurchases", jsonPackagePurchases);
        Dictionary<int, bool>.Enumerator packageEetor = m_PackagePurchaseDic.GetEnumerator();
        while (packageEetor.MoveNext())
        {
            JSONClass jsonPackagePurchaseClass = new JSONClass();
            jsonPackagePurchaseClass.Add("index", new JSONData(packageEetor.Current.Key));
            jsonPackagePurchaseClass.Add("complete", new JSONData(packageEetor.Current.Value));
            jsonPackagePurchases.Add(jsonPackagePurchaseClass);
        }
        jsonCloudData.Add("curPetExperienceCount", new JSONData(m_CurPetExperienceCount));
        jsonCloudData.Add("enablePetExperience", new JSONData(m_EnablePetExperience));

        JSONArray jsonOwnVehicles = new JSONArray();
        jsonCloudData.Add("ownVehicles", jsonOwnVehicles);
        Dictionary<int, VehicleInfo>.Enumerator etor1 = m_VehicleInfoDic.GetEnumerator();
        while (etor1.MoveNext())
        {
            VehicleInfo vehicleInfo = etor1.Current.Value;
            JSONClass jsonOwnVehicleClass = new JSONClass();
            jsonOwnVehicleClass.Add("index", new JSONData(vehicleInfo.index));
            jsonOwnVehicles.Add(jsonOwnVehicleClass);
        }

        JSONArray jsonOwnPets = new JSONArray();
        jsonCloudData.Add("ownPets", jsonOwnPets);
        Dictionary<int, PetInfo>.Enumerator etor2 = m_PetInfoDic.GetEnumerator();
        while (etor2.MoveNext())
        {
            PetInfo petInfo = etor2.Current.Value;
            JSONClass jsonOwnPetClass = new JSONClass();
            jsonOwnPetClass.Add("index", new JSONData(petInfo.index));
            jsonOwnPets.Add(jsonOwnPetClass);
        }

#if BWQUEST
        JSONClass jsonQuestData = QuestManager.instance.SaveJsonQuestData();
        jsonCloudData.Add("quest_data", jsonQuestData);
#endif

#if UNITY_EDITOR_WIN || UNITY_EDITOR
        string saveString = Convert.ToBase64String(Encoding.UTF8.GetBytes(rootNode.ToString()));
#else
        string saveString = Convert.ToBase64String(easy.Crypto.encrypt(rootNode.ToString(), m_EncryptKey));
#endif

        return saveString;
    }

    public void LoadDataToCloud(string loadString)
    {
        if (loadString.CompareTo("") != 0)
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
            JSONNode json = JSON.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(loadString)));
#else
            JSONNode json = JSON.Parse(easy.Crypto.decrypt(Convert.FromBase64String(loadString), m_EncryptKey));
#endif
            JSONNode jsonCloudData = json["cloud_data"];
            JSONArray jsonOwnVehicles = jsonCloudData["ownVehicles"].AsArray;
            bool isExistPrePlay = false;
            Debug.Log(string.Format("기존 차량 : {0}, 세이브 차량 : {1}", m_VehicleInfoDic.Count, jsonOwnVehicles.Count));
            if (jsonOwnVehicles.Count < m_VehicleInfoDic.Count)
            {
                isExistPrePlay = true;
            }

            JSONArray jsonOwnPets = jsonCloudData["ownPets"].AsArray;
            Debug.Log(string.Format("기존 펫 : {0}, 세이브 펫 : {1}", m_PetInfoDic.Count, jsonOwnPets.Count));
            if (jsonOwnPets.Count < m_PetInfoDic.Count)
            {
                isExistPrePlay = true;
            }

            int cloudSelectVehicleIndex = jsonCloudData["selectVehicleIndex"].AsInt;
            int cloudSelectPetIndex = jsonCloudData["selectPetIndex"].AsInt;
            int cloudCash = jsonCloudData["cash"].AsInt;
            Dictionary<int, int> cloudTokenDic = new Dictionary<int, int>();
            JSONArray jsonTokens = jsonCloudData["tokens"].AsArray;
            for (int i = 0; i < jsonTokens.Count; i++)
            {
                JSONNode jsonTokenClass = jsonTokens[i];
                int tokenIndex = jsonTokenClass["index"].AsInt;
                int tokenCount = jsonTokenClass["count"].AsInt;
                cloudTokenDic.Add(tokenIndex, tokenCount);
            }
            int cloudDuplicationGachaCount = jsonCloudData["duplicationGachaCount"].AsInt;
            int cloudPlayGiftCount = jsonCloudData["playGiftCount"].AsInt;
            long cloudFreeGiftTime = (long)jsonCloudData["freeGiftTime"].AsDouble;
            int cloudPlayGachaCount = jsonCloudData["playGachaCount"].AsInt;
            bool cloudPlayBStaticGacha = jsonCloudData["playBStaticGacha"].AsBool;
            bool cloudPlayAStaticGacha = jsonCloudData["playAStaticGacha"].AsBool;
            bool cloudPurchased = jsonCloudData["purchased"].AsBool;
            int cloudPurchaseCount = jsonCloudData["purchaseCount"].AsInt;
            Dictionary<int, bool> cloudPackagePurchaseDic = new Dictionary<int, bool>();
            JSONArray jsonPackagePurchases = jsonCloudData["packagePurchases"].AsArray;
            for (int i = 0; i < jsonPackagePurchases.Count; i++)
            {
                JSONNode jsonPackagePurchaseClass = jsonPackagePurchases[i];
                int packageIndex = jsonPackagePurchaseClass["index"].AsInt;
                bool complete = jsonPackagePurchaseClass["complete"].AsBool;
                cloudPackagePurchaseDic.Add(packageIndex, complete);
            }
            int cloudCurPetExperienceCount = jsonCloudData["curPetExperienceCount"].AsInt;
            bool cloudEnablePetExperience = jsonCloudData["enablePetExperience"].AsBool;

#if BWQUEST
            JSONNode jsonQuestData = jsonCloudData["quest_data"];
#endif

            // 클라우드에서 로드한 정보가 현재 진행 중인 정보보다 이전 정보일 경우
            if (isExistPrePlay)
            {
                // 경고 팝업
                PopupManager.instance.ShowLargeYesNo(TextDocs.content("LOAD_WARNING_TITLE"), TextDocs.content("LOAD_WARNING_TEXT"),
                    delegate
                    {
                        m_SelectVehicleIndex = cloudSelectVehicleIndex;
                        m_SelectPetIndex = cloudSelectPetIndex;

                        LoadJsonOwnVehicleData(jsonOwnVehicles);
                        LoadJsonOwnPetData(jsonOwnPets);
#if BWQUEST
                        QuestManager.instance.LoadJsonQuestData(jsonQuestData);
#endif

                        // 캐쉬, 토큰 정보는 롤백 안해준다.(세이브->로드 후 가챠 무한 반복 어뷰징 방지)

                        m_DuplicationGachaCount = cloudDuplicationGachaCount;
                        m_PlayGachaCount = cloudPlayGachaCount;
                        m_IsPlayBStaticGacha = cloudPlayBStaticGacha;
                        m_IsPlayAStaticGacha = cloudPlayAStaticGacha;
                        m_IsPurchased = cloudPurchased;
                        m_PurchaseCount = cloudPurchaseCount;
                        Dictionary<int, bool>.Enumerator packageEtor = cloudPackagePurchaseDic.GetEnumerator();
                        while (packageEtor.MoveNext())
                        {
                            if (m_PackagePurchaseDic.ContainsKey(packageEtor.Current.Key))
                            {
                                m_PackagePurchaseDic[packageEtor.Current.Key] = packageEtor.Current.Value;
                            }
                            else
                            {
                                m_PackagePurchaseDic.Add(packageEtor.Current.Key, packageEtor.Current.Value);
                            }
                        }
                        m_CurPetExperienceCount = cloudCurPetExperienceCount;
                        m_EnablePetExperience = cloudEnablePetExperience;

                        SaveUserData();
                        SaveOwnVehicleData();
#if BWQUEST
                        QuestManager.instance.SaveQuestData();
#endif
                        GameManager.instance.RestoreGameScene(GameConfig.Scene.GameScene);
                    },
                    null, null);
            }
            else
            {
                CommonPopup popup = PopupManager.instance.ShowSmallOK(TextDocs.content("LOAD_SUCCESS_TITLE"), TextDocs.content("LOAD_SUCCESS_TEXT"),
                    delegate
                    {
                        m_SelectVehicleIndex = cloudSelectVehicleIndex;
                        m_SelectPetIndex = cloudSelectPetIndex;

                        LoadJsonOwnVehicleData(jsonOwnVehicles);
                        LoadJsonOwnPetData(jsonOwnPets);
#if BWQUEST
                        QuestManager.instance.LoadJsonQuestData(jsonQuestData);
#endif

                        cash = cloudCash;
                        Dictionary<int, int>.Enumerator etor = cloudTokenDic.GetEnumerator();
                        while (etor.MoveNext())
                        {
                            if (m_TokenDic.ContainsKey(etor.Current.Key))
                            {
                                m_TokenDic[etor.Current.Key] = etor.Current.Value;
                            }
                            else
                            {
                                m_TokenDic.Add(etor.Current.Key, etor.Current.Value);
                            }
                        }
                        m_DuplicationGachaCount = cloudDuplicationGachaCount;
                        //m_PlayGiftCount = cloudPlayGiftCount;
                        //m_FreeGiftTime = cloudFreeGiftTime;
                        m_PlayGachaCount = cloudPlayGachaCount;
                        m_IsPlayBStaticGacha = cloudPlayBStaticGacha;
                        m_IsPlayAStaticGacha = cloudPlayAStaticGacha;
                        m_IsPurchased = cloudPurchased;
                        m_PurchaseCount = cloudPurchaseCount;
                        Dictionary<int, bool>.Enumerator packageEtor = cloudPackagePurchaseDic.GetEnumerator();
                        while (packageEtor.MoveNext())
                        {
                            if (m_PackagePurchaseDic.ContainsKey(packageEtor.Current.Key))
                            {
                                m_PackagePurchaseDic[packageEtor.Current.Key] = packageEtor.Current.Value;
                            }
                            else
                            {
                                m_PackagePurchaseDic.Add(packageEtor.Current.Key, packageEtor.Current.Value);
                            }
                        }
                        m_CurPetExperienceCount = cloudCurPetExperienceCount;
                        m_EnablePetExperience = cloudEnablePetExperience;

                        SaveUserData();
                        SaveOwnVehicleData();
#if BWQUEST
                        QuestManager.instance.SaveQuestData();
#endif
                        GameManager.instance.RestoreGameScene(GameConfig.Scene.GameScene);
                    }, null);
                popup.cancelable = false;
            }
        }
    }

    void LoadJsonOwnVehicleData(JSONArray jsonOwnVehicles)
    {
        VehicleDocs vehicleDocs = VehicleDocs.instance;
        m_VehicleInfoDic.Clear();
        for (int i = 0; i < jsonOwnVehicles.Count; i++)
        {
            VehicleInfo vehicleInfo = new VehicleInfo();
            JSONNode jsonOwnVehicleData = jsonOwnVehicles[i];
            vehicleInfo.index = jsonOwnVehicleData["index"].AsInt;
            if (m_SelectVehicleIndex == vehicleInfo.index)
            {
                vehicleInfo.state = VehicleInfo.State.Boarding;
            }
            else
            {
                vehicleInfo.state = VehicleInfo.State.Purchased;
            }
            if (vehicleDocs.vehicleDataDic.ContainsKey(vehicleInfo.index))
            {
                vehicleInfo.vehicleData = vehicleDocs.vehicleDataDic[vehicleInfo.index];
            }
            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);
        }

        if(!m_VehicleInfoDic.ContainsKey(59))
        {
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.index = 59;
            vehicleInfo.state = VehicleInfo.State.Purchased;
            if (vehicleDocs.vehicleDataDic.ContainsKey(vehicleInfo.index))
            {
                vehicleInfo.vehicleData = vehicleDocs.vehicleDataDic[vehicleInfo.index];
            }
            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);
        }
    }

    void LoadJsonOwnPetData(JSONArray jsonOwnPets)
    {
        PetDocs petDocs = PetDocs.instance;
        m_PetInfoDic.Clear();
        for (int i = 0; i < jsonOwnPets.Count; i++)
        {
            PetInfo petInfo = new PetInfo();
            JSONNode jsonOwnPetData = jsonOwnPets[i];
            petInfo.index = jsonOwnPetData["index"].AsInt;
            if (m_SelectPetIndex == petInfo.index)
            {
                petInfo.state = PetInfo.State.Boarding;
            }
            else
            {
                petInfo.state = PetInfo.State.Purchased;
            }
            if (petDocs.petDataDic.ContainsKey(petInfo.index))
            {
                petInfo.petData = petDocs.petDataDic[petInfo.index];
            }
            m_PetInfoDic.Add(petInfo.index, petInfo);
        }
    }

#if UNITY_ANDROID
    private IEnumerator MakeScreenshotAndSaveGameData()
    {
        yield return new WaitForEndOfFrame();
        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        Texture2D Screenshot = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        Screenshot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        Screenshot.Apply();

        long TotalPlayedTime = GameApplication.instance.playTime * 1000;
        //string currentSaveName =  "snapshotTemp-" + Random.Range(1, 281).ToString();
        string currentSaveName = "AOS_BURNOUTCITY";
        string description = System.DateTime.Now.ToString("MM/dd/yyyy H:mm:ss");

        GooglePlaySavedGamesManager.ActionGameSaveResult += OnGameSaveResult;
        GooglePlaySavedGamesManager.ActionConflict += OnConflict;
        string saveJson = SaveDataToCloud();
        GooglePlaySavedGamesManager.Instance.CreateNewSnapshot(currentSaveName, description, Screenshot, saveJson, TotalPlayedTime);

        Destroy(Screenshot);
    }

    // Save 완료시 호출 된다.
    private void OnGameSaveResult(GP_SpanshotLoadResult result)
    {
        PopupManager.instance.HideLoadingPopup();

        GooglePlaySavedGamesManager.ActionGameSaveResult -= OnGameSaveResult;
        Debug.Log("OnGameSaveResult : " + result.Message);

        if (result.IsSucceeded)
        {
            Debug.Log("Games Saved: " + result.Snapshot.meta.Title);
        }
        else
        {
            Debug.Log("Games Save Failed");
        }
    }

    private void OnConflict(GP_SnapshotConflict result)
    {
        Debug.Log("Conflict Detected: ");

        GP_Snapshot snapshot = result.Snapshot;
        GP_Snapshot conflictSnapshot = result.ConflictingSnapshot;

        // Resolve between conflicts by selecting the newest of the conflicting snapshots.
        GP_Snapshot mResolvedSnapshot = snapshot;

        if (snapshot.meta.LastModifiedTimestamp < conflictSnapshot.meta.LastModifiedTimestamp)
        {
            mResolvedSnapshot = conflictSnapshot;
        }

        result.Resolve(mResolvedSnapshot);
    }
#endif

    void TrySavedGames()
    {
        Debug.Log("TrySavedGames");
#if UNITY_ANDROID
        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            if (GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED)
            {
                AutoSavedGames();
            }
            else
            {
                m_RequestGPConnect = true;
                m_GPConnectRequest = GPCConnectRequest.SAVE;
                ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
                GooglePlayConnection.Instance.Connect();
            }
        }
        else
        {
            //PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
            //        TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);
        }
#elif UNITY_IOS
        AutoSavedGames();
#endif
    }

    void AutoSavedGames()
    {
        Debug.Log("AutoSavedGames");
#if UNITY_ANDROID
        StartCoroutine(MakeScreenshotAndSaveGameData());

        PopupManager.instance.ShowLoadingPopup();
#elif UNITY_IOS
        if(m_IsCloudInit)
		{
            iCloudManager.Instance.setString("IOS_BurnoutCity", SaveDataToCloud());        
		}
		else
		{
		}
#endif
    }

    void OnHideInterstitialAd(bool isSuccess)
    {
        CommonAdManager.OnHideInterstitialAd -= OnHideInterstitialAd;
        CommonAdManager.instance.Fetch(CommonAdManager.AdType.Interstitial);
    }

    //void OnAdmobInterstitialResult(bool isSuccess)
    //{
    //    CommonAdManager.OnAdmobInterstitialResult -= OnAdmobInterstitialResult;
    //    PopupManager.instance.HideLoadingPopup();
    //}

    // 게임시작, 게임 오버 시에 전면 광고 띄우기 위한 함수
    public IEnumerator ShowInterstitialAd()
    {
        if (!m_IsPurchased)
        {
            //int rate = UnityEngine.Random.Range(0, 300);
            int rate = UnityEngine.Random.Range(0, 500);
            Debug.Log("ShowInterstitialAd : " + rate);

            //if (rate >= 0)
            if (0 <= rate && rate < 100)
            {
                Debug.Log("ShowInterstitialAd Show : " + rate);
                PopupManager.instance.ShowLoadingPopup();

                yield return new WaitForSeconds(0.5f);

                //PopupManager.instance.HideLoadingPopup();
                //CommonAdManager.OnHideInterstitialAd += OnHideInterstitialAd;
                //CommonAdManager.instance.Show(CommonAdManager.AdType.Interstitial);

                //CommonAdManager.OnAdmobInterstitialResult += OnAdmobInterstitialResult;
                CommonAdManager.instance.ShowInterstitialAdmob();

                yield return new WaitForSeconds(0.5f);

                PopupManager.instance.HideLoadingPopup();
            }
            else
            {
                yield return null;
            }
        }
        else
        {
            yield return null;
        }
    }

    public void VehicleBuyToGacha(int index)
    {
        VehicleData vehicleData = VehicleDocs.instance.vehicleDataDic[index];

        // 처음 B급 차량이 나왔다면 B급 차량 고정 가챠를 못돌리게 막는다.
        if (vehicleData.grade == VehicleData.Grade.B && !m_IsPlayBStaticGacha)
        {
            m_IsPlayBStaticGacha = true;
        }
        // 처음 A급 차량이 나왔다면 A급 차량 고정 가챠를 못돌리게 막는다.
        else if (vehicleData.grade == VehicleData.Grade.A && !m_IsPlayAStaticGacha)
        {
            m_IsPlayAStaticGacha = true;
        }

        // 기존 선택한 차량의 상태를 변경한다.
        VehicleInfo vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
        vehicleInfo.state = VehicleInfo.State.Purchased;

        // 새로 구입한 차량 정보를 추가한다.
        vehicleInfo = new VehicleInfo();
        vehicleInfo.index = vehicleData.index;
        vehicleInfo.state = VehicleInfo.State.Boarding;
        vehicleInfo.vehicleData = vehicleData;
        m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

        // 구매한 차량으로 선택된 차량 정보를 수정한다.
        m_SelectVehicleIndex = vehicleInfo.index;

        // 선택한 차량 정보와 소유한 차량 목록을 갱신해준다.
        SaveUserData();
        SaveOwnVehicleData();
    }

    public GameConfig.BuyResultState VehicleBuyToToken(VehicleData vehicleData)
    {
        GameConfig.BuyResultState buyResultState = GameConfig.BuyResultState.Success;
        int tokenType = (int)vehicleData.tokenType;
        // 보유한 토큰 수량이 부족할 경우
        if (m_TokenDic[tokenType] < vehicleData.tokenCount)
        {
            buyResultState = GameConfig.BuyResultState.NotEnoughToken;
            return buyResultState;
        }
        else
        {
            // 기존 선택한 차량의 상태를 변경한다.
            VehicleInfo vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
            vehicleInfo.state = VehicleInfo.State.Purchased;

            // 소요된 토큰 수치를 깎는다.
            m_TokenDic[tokenType] -= vehicleData.tokenCount;

            // 새로 구입한 차량 정보를 추가한다.
            vehicleInfo = new VehicleInfo();
            vehicleInfo.index = vehicleData.index;
            vehicleInfo.state = VehicleInfo.State.Boarding;
            vehicleInfo.vehicleData = vehicleData;
            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

            // 구매한 차량으로 선택된 차량 정보를 수정한다.
            m_SelectVehicleIndex = vehicleInfo.index;

            // 선택한 차량 정보와 소유한 차량 목록을 갱신해준다.
            SaveUserData();
            SaveOwnVehicleData();

            return buyResultState;
        }
    }

    public void VehicleBuyToIAP(VehicleData vehicleData, PackageData packageData, PetData petData)
    {
        if (vehicleData != null)
        {
            // 기존 선택한 차량의 상태를 변경한다.
            VehicleInfo vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
            vehicleInfo.state = VehicleInfo.State.Purchased;

            // 새로 구입한 차량 정보를 추가한다.
            vehicleInfo = new VehicleInfo();
            vehicleInfo.index = vehicleData.index;
            vehicleInfo.state = VehicleInfo.State.Boarding;
            vehicleInfo.vehicleData = vehicleData;
            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

            // 구매한 차량으로 선택된 차량 정보를 수정한다.
            m_SelectVehicleIndex = vehicleInfo.index;
        }
        else if (packageData != null)
        {
            VehicleData packageVehicleData = null;
            PetData packagePetData = null;
            for (int i = 0; i < packageData.stockList.Count; i++)
            {
                PackageStockData packageStockData = packageData.stockList[i];
                if (packageStockData.type == GameConfig.RewardType.Vehicle)
                {
                    if (VehicleDocs.instance.vehicleDataDic.ContainsKey(packageStockData.value))
                    {
                        packageVehicleData = VehicleDocs.instance.vehicleDataDic[packageStockData.value];
                        if (packageVehicleData != null)
                        {
                            // 기존 선택한 차량의 상태를 변경한다.
                            VehicleInfo vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
                            vehicleInfo.state = VehicleInfo.State.Purchased;

                            // 새로 구입한 차량 정보를 추가한다.
                            vehicleInfo = new VehicleInfo();
                            vehicleInfo.index = packageVehicleData.index;
                            vehicleInfo.state = VehicleInfo.State.Boarding;
                            vehicleInfo.vehicleData = packageVehicleData;
                            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

                            // 구매한 차량으로 선택된 차량 정보를 수정한다.
                            m_SelectVehicleIndex = vehicleInfo.index;
                        }
                    }
                }
                else if (packageStockData.type == GameConfig.RewardType.AllVehicle)
                {
                    VehicleData allVehicleData = null;
                    VehicleInfo vehicleInfo = null;

                    // 기존 선택한 차량의 상태를 변경한다.
                    vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
                    vehicleInfo.state = VehicleInfo.State.Purchased;

                    Dictionary<int, VehicleData>.Enumerator etor = VehicleDocs.instance.vehicleDataDic.GetEnumerator();
                    while (etor.MoveNext())
                    {
                        allVehicleData = etor.Current.Value;
#if BWQUEST
                        if (!m_VehicleInfoDic.ContainsKey(allVehicleData.index) &&
                            allVehicleData.earnType == VehicleData.EarnType.Purchase)
                        {
                            // 새로 구입한 차량 정보를 추가한다.
                            vehicleInfo = new VehicleInfo();
                            vehicleInfo.index = allVehicleData.index;
                            vehicleInfo.state = VehicleInfo.State.Purchased;
                            vehicleInfo.vehicleData = allVehicleData;
                            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

                            //// 구매한 차량으로 선택된 차량 정보를 수정한다.
                            //m_SelectVehicleIndex = vehicleInfo.index;
                        }
#else
                        if(!m_VehicleInfoDic.ContainsKey(allVehicleData.index))
                        {
                            // 새로 구입한 차량 정보를 추가한다.
                            vehicleInfo = new VehicleInfo();
                            vehicleInfo.index = allVehicleData.index;
                            vehicleInfo.state = VehicleInfo.State.Purchased;
                            vehicleInfo.vehicleData = allVehicleData;
                            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

                            //// 구매한 차량으로 선택된 차량 정보를 수정한다.
                            //m_SelectVehicleIndex = vehicleInfo.index;
                        }
#endif
                    }

                    if (vehicleInfo != null)
                    {
                        vehicleInfo.state = VehicleInfo.State.Boarding;
                        // 마지막으로 구매 활성화된 차량으로 선택된 차량 정보를 수정한다.
                        m_SelectVehicleIndex = vehicleInfo.index;
                    }
                }
                else if (packageStockData.type == GameConfig.RewardType.Pet)
                {
                    if (PetDocs.instance.petDataDic.ContainsKey(packageStockData.value))
                    {
                        packagePetData = PetDocs.instance.petDataDic[packageStockData.value];
                        if (packagePetData != null)
                        {
                            if (!m_PetInfoDic.ContainsKey(packagePetData.index))
                            {
                                PetInfo petInfo = null;
                                // 기존 선택한 펫의 상태를 변경한다.
                                if (m_PetInfoDic.ContainsKey(m_SelectPetIndex))
                                {
                                    petInfo = m_PetInfoDic[m_SelectPetIndex];
                                    petInfo.state = PetInfo.State.Purchased;
                                }

                                // 새로 구입한 펫 정보를 추가한다.
                                petInfo = new PetInfo();
                                petInfo.index = packagePetData.index;
                                petInfo.state = PetInfo.State.Boarding;
                                petInfo.petData = packagePetData;
                                m_PetInfoDic.Add(petInfo.index, petInfo);

                                // 구매한 펫으로 선택된 펫 정보를 수정한다.
                                m_SelectPetIndex = petInfo.index;
                            }
                        }
                    }
                }
                else if (packageStockData.type == GameConfig.RewardType.Cash)
                {
                    cash += packageStockData.value;
                }
                else if ((int)packageStockData.type < 4)
                {
                    m_TokenDic[(int)packageStockData.type] += packageStockData.value;
                }
            }

            if (m_PackagePurchaseDic.ContainsKey(packageData.index))
            {
                m_PackagePurchaseDic[packageData.index] = true;
            }
            else
            {
                m_PackagePurchaseDic.Add(packageData.index, true);
            }
        }
        else if (petData != null)
        {
            PetInfo petInfo = null;
            // 기존 선택한 펫의 상태를 변경한다.
            if (m_PetInfoDic.ContainsKey(m_SelectPetIndex))
            {
                petInfo = m_PetInfoDic[m_SelectPetIndex];
                petInfo.state = PetInfo.State.Purchased;
            }

            // 새로 구입한 펫 정보를 추가한다.
            petInfo = new PetInfo();
            petInfo.index = petData.index;
            petInfo.state = PetInfo.State.Boarding;
            petInfo.petData = petData;
            m_PetInfoDic.Add(petInfo.index, petInfo);

            // 구매한 펫으로 선택된 펫 정보를 수정한다.
            m_SelectPetIndex = petInfo.index;
        }

        // 선택한 차량 정보와 소유한 차량 목록을 갱신해준다.
        SaveUserData();
        SaveOwnVehicleData();
    }

    // 보상으로 재화를 받았을 경우 처리
    public void EarnRewardData(GameConfig.RewardType type, int value)
    {
        switch(type)
        {
            case GameConfig.RewardType.C:
            case GameConfig.RewardType.B:
            case GameConfig.RewardType.A:
            case GameConfig.RewardType.S:
                {
                    m_TokenDic[(int)type] += value;
                }
                break;

            case GameConfig.RewardType.Cash:
                {
                    cash += value;
                }
                break;

            case GameConfig.RewardType.Vehicle:
                {
                    if(VehicleDocs.instance.vehicleDataDic.ContainsKey(value))
                    {
                        VehicleData vehicleData = VehicleDocs.instance.vehicleDataDic[value];

                        // 기존 선택한 차량의 상태를 변경한다.
                        VehicleInfo vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
                        vehicleInfo.state = VehicleInfo.State.Purchased;

                        // 새로 구입한 차량 정보를 추가한다.
                        vehicleInfo = new VehicleInfo();
                        vehicleInfo.index = vehicleData.index;
                        vehicleInfo.state = VehicleInfo.State.Boarding;
                        vehicleInfo.vehicleData = vehicleData;
                        m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

                        // 구매한 차량으로 선택된 차량 정보를 수정한다.
                        m_SelectVehicleIndex = vehicleInfo.index;
                    }                    
                }
                break;

            case GameConfig.RewardType.Pet:
                {
                    if(PetDocs.instance.petDataDic.ContainsKey(value))
                    {
                        PetData petData = PetDocs.instance.petDataDic[value];
                        PetInfo petInfo = null;
                        // 기존 선택한 펫의 상태를 변경한다.
                        if (m_PetInfoDic.ContainsKey(m_SelectPetIndex))
                        {
                            petInfo = m_PetInfoDic[m_SelectPetIndex];
                            petInfo.state = PetInfo.State.Purchased;
                        }

                        // 새로 구입한 펫 정보를 추가한다.
                        petInfo = new PetInfo();
                        petInfo.index = petData.index;
                        petInfo.state = PetInfo.State.Boarding;
                        petInfo.petData = petData;
                        m_PetInfoDic.Add(petInfo.index, petInfo);

                        // 구매한 펫으로 선택된 펫 정보를 수정한다.
                        m_SelectPetIndex = petInfo.index;
                    }                    
                }
                break;

            case GameConfig.RewardType.AllVehicle:
                {
                    VehicleData allVehicleData = null;
                    VehicleInfo vehicleInfo = null;

                    // 기존 선택한 차량의 상태를 변경한다.
                    vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
                    vehicleInfo.state = VehicleInfo.State.Purchased;

                    Dictionary<int, VehicleData>.Enumerator etor = VehicleDocs.instance.vehicleDataDic.GetEnumerator();
                    while (etor.MoveNext())
                    {
                        allVehicleData = etor.Current.Value;
#if BWQUEST
                        if (!m_VehicleInfoDic.ContainsKey(allVehicleData.index) &&
                            allVehicleData.earnType == VehicleData.EarnType.Purchase)
                        {
                            // 새로 구입한 차량 정보를 추가한다.
                            vehicleInfo = new VehicleInfo();
                            vehicleInfo.index = allVehicleData.index;
                            vehicleInfo.state = VehicleInfo.State.Purchased;
                            vehicleInfo.vehicleData = allVehicleData;
                            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

                            //// 구매한 차량으로 선택된 차량 정보를 수정한다.
                            //m_SelectVehicleIndex = vehicleInfo.index;
                        }
#else
                        if(!m_VehicleInfoDic.ContainsKey(allVehicleData.index))
                        {
                            // 새로 구입한 차량 정보를 추가한다.
                            vehicleInfo = new VehicleInfo();
                            vehicleInfo.index = allVehicleData.index;
                            vehicleInfo.state = VehicleInfo.State.Purchased;
                            vehicleInfo.vehicleData = allVehicleData;
                            m_VehicleInfoDic.Add(vehicleInfo.index, vehicleInfo);

                            //// 구매한 차량으로 선택된 차량 정보를 수정한다.
                            //m_SelectVehicleIndex = vehicleInfo.index;
                        }
#endif
                    }
                }
                break;
        }

        SaveUserData();
        SaveOwnVehicleData();
    }

    public bool VehicleBoarding(VehicleData vehicleData)
    {
        if(m_VehicleInfoDic.ContainsKey(vehicleData.index))
        {
            // 기존 선택한 차량의 상태를 변경한다.
            VehicleInfo vehicleInfo = m_VehicleInfoDic[m_SelectVehicleIndex];
            vehicleInfo.state = VehicleInfo.State.Purchased;

            // 새로 선택한 차량의 상태를 변경한다.
            vehicleInfo = m_VehicleInfoDic[vehicleData.index];
            vehicleInfo.state = VehicleInfo.State.Boarding;

            // 구매한 차량으로 선택된 차량 정보를 수정한다.
            m_SelectVehicleIndex = vehicleInfo.index;

            // 선택한 차량이 바뀌었으므로 저장해준다.
            SaveUserData();

            return true;
        }
        else
        {
            return false;
        }
    }

    public int EarnFreeGift()
    {
        int remainCash = m_Cash % EtcDocs.instance.gachaCash;
        int gap = (int)((float)EtcDocs.instance.gachaCash * 0.25f);
        int minCash = Math.Max(gap * 3,
            EtcDocs.instance.gachaCash - remainCash);
        int earnCash = UnityEngine.Random.Range(minCash, minCash + gap * 2);
        // 랜덤 범위 내에서 무료 머니 지급
        //cash += earnCash;

        
        //SaveUserData();

        return earnCash;
    }

    void SetLanguage()
    {
        if (m_SelectLanguage == GameConfig.SupportLanguage.Korean)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Korean;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.English)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.English;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Chinese_S)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Chinese_S;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Chinese_T)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Chinese_T;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Indonesia)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Indonesia;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Thai)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Thai;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Japanese)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Japanese;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Spanish)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Spanish;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Russian)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Russian;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Arabic)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Arabic;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.German)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.German;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Hindi)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Hindi;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.French)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.French;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Italian)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Italian;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Portuguese)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Portuguese;
        }
        else if (m_SelectLanguage == GameConfig.SupportLanguage.Vietnamese)
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.Vietnamese;
        }
        else
        {
            TextDocs.instance.selectLanguage = GameConfig.SupportLanguage.English;
        }
    }

    public void InitializeLanguage()
    {
        GameConfig.SupportLanguage selectLanguage = m_SelectLanguage;
        if (selectLanguage == GameConfig.SupportLanguage.None)
        {
            if (Application.systemLanguage == SystemLanguage.Unknown)
            {
                BWCommonPlugin.OnGetLanguageResult += OnGetLanguageResult;
                BWCommonPlugin.instance.GetLanguage();
            }
            else
            {
                int systemLanguageIndex = (int)Application.systemLanguage;
                m_SelectLanguage = (GameConfig.SupportLanguage)SupportLanguageDocs.languageIndex(systemLanguageIndex);
                Debug.Log("LLLang -> " + Application.systemLanguage + ", " + m_SelectLanguage);
                SetLanguage();
                SaveUserData();
            }
        }
        else
        {
            //m_SelectLanguage = GameConfig.SupportLanguage.Chinese_S;
            SetLanguage();
        }
    }

    void OnGetLanguageResult(string data)
    {
        Debug.Log("OnGetLanguageResult : " + data);

        BWCommonPlugin.OnGetLanguageResult -= OnGetLanguageResult;

        //if (data.CompareTo("ms") == 0 || data.CompareTo("may") == 0 || data.CompareTo("my") == 0)
        //{
        //    m_SelectLanguage = GameConfig.SupportLanguage.Indonesia;
        //}
        //else if (data.CompareTo("in") == 0 || data.CompareTo("id") == 0)
        //{
        //    m_SelectLanguage = GameConfig.SupportLanguage.Indonesia;
        //}

        //else if (data.CompareTo("hi") == 0 || data.CompareTo("hin") == 0 ||
        //        data.CompareTo("bh") == 0 || data.CompareTo("bih") == 0 ||
        //        data.CompareTo("ks") == 0 || data.CompareTo("kas") == 0)
        //{
        //    //#if UNITY_ANDROID
        //    m_SelectLanguage = GameConfig.SupportLanguage.Hindi;
        //    //#else
        //    //_selectLanguage = GameConfig.SupportLanguage.English;
        //    //#endif
        //}
        //else
        //{
        m_SelectLanguage = GameConfig.SupportLanguage.English;
        //}

        SetLanguage();
        SaveUserData();
    }

    public void TrustAllHosts()
    {
        BWCommonPlugin.OnTrustAllHostsResult += OnTrustAllHostsResult;
        BWCommonPlugin.instance.TrustAllHosts();
    }

    void OnTrustAllHostsResult(bool isSuccess)
    {
        BWCommonPlugin.OnTrustAllHostsResult -= OnTrustAllHostsResult;
        Debug.Log("OnTrustAllHostsResult : " + isSuccess);

        if (isSuccess)
        {
            m_DaeriUrl = "https://pf.daerisoft.com";
            m_IsSSLTrust = true;
            //StartCoroutine(AESTest());
        }
        else
        {
            m_DaeriUrl = "http://pf.daerisoft.com";
            m_IsSSLTrust = false;
        }
    }

    public void SSLTrust()
    {
        string cert = @"-----BEGIN CERTIFICATE-----
MIIDADCCAmmgAwIBAgIJAIlE/BjMVHYCMA0GCSqGSIb3DQEBCwUAMIGYMQswCQYD
VQQGEwJLUjEOMAwGA1UECAwFU2VvdWwxEjAQBgNVBAcMCUd1ZW1jaGVvbjESMBAG
A1UECgwJRGFlcmlzb2Z0MREwDwYDVQQLDAhwbGF0Zm9ybTEZMBcGA1UEAwwQcGYu
ZGFlcmlzb2Z0LmNvbTEjMCEGCSqGSIb3DQEJARYUZHNzb2Z0QGRhZXJpc29mdC5j
b20wHhcNMTYwNDI4MDgyOTIwWhcNMjYwNDI2MDgyOTIwWjCBmDELMAkGA1UEBhMC
S1IxDjAMBgNVBAgMBVNlb3VsMRIwEAYDVQQHDAlHdWVtY2hlb24xEjAQBgNVBAoM
CURhZXJpc29mdDERMA8GA1UECwwIcGxhdGZvcm0xGTAXBgNVBAMMEHBmLmRhZXJp
c29mdC5jb20xIzAhBgkqhkiG9w0BCQEWFGRzc29mdEBkYWVyaXNvZnQuY29tMIGf
MA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIV2SzSoBR2Syr+RrdpYuhqJHEz46q
kurBn0Sn3flcbCcEF7efNtspRi6mS1S1qPTgho+/D6feEsjKx5hUBfUYXStkCDRe
3hSwz6RDNfZW/5ABgPjmTgdGU0I6nhrKMnCOlKfEmrFNIGtNZCpnS7ogz4cGZw9K
+UtkBA4KNiuIjQIDAQABo1AwTjAdBgNVHQ4EFgQUcP3jZK236UY9gcsO0lFlqPia
4KowHwYDVR0jBBgwFoAUcP3jZK236UY9gcsO0lFlqPia4KowDAYDVR0TBAUwAwEB
/zANBgkqhkiG9w0BAQsFAAOBgQBbP6f3ykEAsSfTLUXsHFAnTuGlcmXog0fgoFh4
cfKenxGFq9xAW/MFvfuiehkNnB+QaD2aGyr2u0u/IJEuzHxscM0VCzkYzEm9D736
d3vIOIVQJ7JOdy+EAGsGsbw9s6SNZ/5YklYasQ61Mau/jUyXeG8lOhl5PIJoi6mi
ArT6HA==
-----END CERTIFICATE-----
    ";

        BWCommonPlugin.OnSSLTrustResult += OnSSLTrustResult;
        BWCommonPlugin.instance.SSLTrust(System.Text.Encoding.ASCII.GetBytes(cert));
    }

    void OnSSLTrustResult(bool isSuccess)
    {
        BWCommonPlugin.OnSSLTrustResult -= OnSSLTrustResult;
        Debug.Log("OnSSLTrustResult : " + isSuccess);

        if (isSuccess)
        {
            m_DaeriUrl = "https://pf.daerisoft.com";
            m_IsSSLTrust = true;
            //StartCoroutine(AESTest());
        }
        else
        {
            m_DaeriUrl = "http://pf.daerisoft.com";
            m_IsSSLTrust = false;
        }
    }

    public IEnumerator AESTest()
    {
		Debug.Log ("@@ AESTest Start 0");
        int ivSize = 32;

        JSONClass rootNode = new JSONClass();
        JSONClass jsonUserData = new JSONClass();
        rootNode.Add("text", new JSONData("Hello World!!"));
        var cryptStr = MadCrypt.AESEncrypt256(rootNode.ToString(), m_DaeriKey, ivSize);

		Debug.Log ("@@ AESTest Start 1");

        WWWForm form = new WWWForm();

        form.AddField("serviceName", m_ServiceName);
        form.AddField("function", "cryptTest");
        form.AddField("data", cryptStr);
        form.AddField("length", rootNode.ToString().Length);

		Debug.Log ("@@ AESTest Start 2");

		WWW www = new WWW("http://pf.daerisoft.com", form);
		Debug.Log ("@@ AESTest Start 3");
        yield return www;

		Debug.Log ("@@ AESTest Start 4");

        if (www.error == null)
        {
            var plainStr = MadCrypt.AESDecrypt256(www.text, m_DaeriKey, ivSize);
            Debug.Log("@@ AESTest Success www.text = " + www.text + " (" + plainStr + ")");
            
        }
        else {
            Debug.Log("@@ AESTest Failed www.error = " + www.error);
        }
    }

	public IEnumerator AESTestSS()
	{
		Debug.Log ("@@ AESTestSS Start 0");
		int ivSize = 32;

		JSONClass rootNode = new JSONClass();
		JSONClass jsonUserData = new JSONClass();
		rootNode.Add("text", new JSONData("Hello World!!"));
		var cryptStr = MadCrypt.AESEncrypt256(rootNode.ToString(), m_DaeriKey, ivSize);

		Debug.Log ("@@ AESTestSS Start 1");

		WWWForm form = new WWWForm();

		form.AddField("serviceName", m_ServiceName);
		form.AddField("function", "cryptTest");
		form.AddField("data", cryptStr);
		form.AddField("length", rootNode.ToString().Length);

		Debug.Log ("@@ AESTestSS Start 2");

		WWW www = new WWW("https://pf.daerisoft.com", form);
		Debug.Log ("@@ AESTestSS Start 3");
		yield return www;

		Debug.Log ("@@ AESTestSS Start 4");

		if (www.error == null)
		{
			var plainStr = MadCrypt.AESDecrypt256(www.text, m_DaeriKey, ivSize);
			Debug.Log("@@ AESTestSS Success www.text = " + www.text + " (" + plainStr + ")");

		}
		else {
			Debug.Log("@@ AESTestSS Failed www.error = " + www.error);
		}
	}

    public void Purchase(VehicleData vehicleData, PackageData packageData, PetData petData)
    {
        //// 테스트
        //VehicleBuyToIAP(vehicleData, packageData, petData);
        //if (OnPurchaseComplete != null)
        //{
        //    OnPurchaseComplete(PurchaseCompleteState.Success);
        //}

#if UNITY_EDITOR_WIN || UNITY_EDITOR
        // 테스트
        VehicleBuyToIAP(vehicleData, packageData, petData);
        if (OnPurchaseComplete != null)
        {
            OnPurchaseComplete(PurchaseCompleteState.Success);
        }
#elif UNITY_ANDROID
        if (m_LoadStore)
        {
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
                m_IsRestore = false;
                StartCoroutine(GetGooglePayLoad(vehicleData, packageData, petData));
                //AndroidInAppPurchaseManager.Client.Purchase(vehicleData.googleID);
            }
            else
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                    TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.Fail);
                }
            }
        }
        else
        {
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
                m_ProccessVehicleData = vehicleData;                
                m_ProccessPackageData = packageData;
                m_ProccessPetData = petData;
				LoadStore(IAPPlatform.Android, IAPState.Purchase);
            }
            else
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                    TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.Fail);
                }
            }
        }
#elif UNITY_IOS
        if (IOSInAppPurchaseManager.Instance.IsStoreLoaded)
        {
			if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
			{
				m_IsRestore = false;
				if(vehicleData != null)
				{
					IOSInAppPurchaseManager.Instance.BuyProduct(vehicleData.appleID);
				}
				else if(packageData != null)
				{
					IOSInAppPurchaseManager.Instance.BuyProduct(packageData.appleID);
				}
                else if(petData != null)
				{
					IOSInAppPurchaseManager.Instance.BuyProduct(petData.appleID);
				}
			}
			else
			{
				PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
				    TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

				if (OnPurchaseComplete != null)
				{
				    OnPurchaseComplete(PurchaseCompleteState.Fail);
				}
			}
        }
        else
        {
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
				m_ProccessVehicleData = vehicleData;                
                m_ProccessPackageData = packageData;
                m_ProccessPetData = petData;
				LoadStore(IAPPlatform.IOS, IAPState.Purchase);
            }
            else
            {
				PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
					TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

				if (OnPurchaseComplete != null)
				{
					OnPurchaseComplete(PurchaseCompleteState.Fail);
				}
            }
        }
#endif
    }

    public void LoadStore(IAPPlatform iapPlatform, IAPState iapState)
    {
		m_IAPState = iapState;
		if (m_IAPState != IAPState.Purchase)
        {
            m_ProccessVehicleData = null;            
            m_ProccessPackageData = null;
            m_ProccessPetData = null;
        }

		if (iapPlatform == IAPPlatform.Android)
		{
			AndroidInAppPurchaseManager.Client.Connect ();
		}
		else if (iapPlatform == IAPPlatform.IOS)
		{
			IOSInAppPurchaseManager.Instance.LoadStore();
		}
    }

#if UNITY_ANDROID
    void OnConnectionResultReceived(GooglePlayConnectionResult result)
    {
        if (result.IsSuccess)
        {
            Debug.Log("UserManager Connected!");

            ObscuredPrefs.SetInt("BeCameGPLogin", 1);
            AchievementDocs.instance.UpdateAchievement();

            if (OnGPConnectionResult != null)
            {
                OnGPConnectionResult(true);
            }

            if (m_RequestGPConnect)
            {
                m_RequestGPConnect = false;

                if (m_GPConnectRequest == GPCConnectRequest.SAVE)
                {
                    m_GPConnectRequest = GPCConnectRequest.NONE;
                    AutoSavedGames();
                }
            }
        }
        else
        {
            Debug.Log("UserManager Connection failed with code : " + result.code.ToString());

            if (OnGPConnectionResult != null)
            {
                OnGPConnectionResult(false);
            }
        }
    }

    private void OnBillingConnected(BillingResult result)
    {
        if (result.isSuccess)
        {
            AndroidInAppPurchaseManager.Client.RetrieveProducDetails();

            if (m_IAPState != IAPState.Restore)
            {
                AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetriveProductsFinised;
            }
            else
            {
                AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetriveProductsFinisedForRestore;
            }
        }
        else
        {
            m_LoadStore = false;

            if (OnPurchaseComplete != null)
            {
                OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
            }
        }

        Debug.Log("@@ OnBillingConnected : " + result.isSuccess + ", " + result.response.ToString() + ", " + result.message);
    }

    private void OnRetriveProductsFinised(BillingResult result)
    {
        Debug.Log("@@ OnRetriveProductsFinised Connection Responce: " + result.isSuccess + ", " + result.response.ToString() + " $$ " + result.message);

        AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetriveProductsFinised;

        if (result.isSuccess)
        {
            VehicleDocs.instance.UpdateProduct();
            PackageDocs.instance.UpdateProduct();
            PetDocs.instance.UpdateProduct();
        }

        m_LoadStore = true;

        if (m_ProccessVehicleData != null)
        {
            m_IsRestore = false;
            StartCoroutine(GetGooglePayLoad(m_ProccessVehicleData, null, null));
            //AndroidInAppPurchaseManager.Client.Purchase(m_ProccessVehicleData.googleID);
        }
        else if(m_ProccessPackageData != null)
        {
            m_IsRestore = false;
            StartCoroutine(GetGooglePayLoad(null, m_ProccessPackageData, null));
        }
        else if (m_ProccessPetData != null)
        {
            m_IsRestore = false;
            StartCoroutine(GetGooglePayLoad(null, null, m_ProccessPetData));
        }
    }

    private void OnRetriveProductsFinisedForRestore(BillingResult result)
    {
        Debug.Log("@@ OnRetriveProductsFinisedForRestore Connection Responce: " + result.isSuccess + ", " + result.response.ToString() + " $$ " + result.message);

        AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetriveProductsFinisedForRestore;

        m_RestoreProductDic.Clear();
        m_CheckRestoreCount = 0;
        if (result.isSuccess)
        {
            foreach (GooglePurchaseTemplate p in AndroidInAppPurchaseManager.Client.Inventory.Purchases)
            {
                Debug.Log("@@ Loaded purchases: " + p.orderId + ", " + p.SKU + ", " + p.state + ", " + p.signature);
                m_RestoreProductDic.Add(p.orderId, p.SKU);
            }

            if(OnGetRestoreInfo != null)
            {
                OnGetRestoreInfo(m_RestoreProductDic.Count);
            }

            ProcessRestore();
        }

        m_LoadStore = true;
    }

    private void OnProductPurchased(BillingResult result)
    {
        if (result.isSuccess)
        {
            Debug.Log("@@ OnProductPurchased Success : " + result.purchase.SKU);
            AndroidInAppPurchaseManager.Client.Consume(result.purchase.SKU);
        }
        else
        {
            if (OnPurchaseComplete != null)
            {
                OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
            }
        }

        //Debug.Log("@@ Purchased Responce: " + result.response.ToString() + " " + result.message);
    }

    private void OnProductConsumed(BillingResult result)
    {
        if (result.isSuccess)
        {
            Debug.Log("@@ OnProductConsumed Success : " + result.purchase.SKU);
            VehicleData vehicleData = VehicleDocs.instance.GetVehicleForGoogle(result.purchase.SKU);
            PackageData packageData = PackageDocs.instance.GetPackageForGoogle(result.purchase.SKU);
            PetData petData = PetDocs.instance.GetPetForGoogle(result.purchase.SKU);
            if (vehicleData != null || packageData != null || petData != null)
            {
                Debug.Log("@@ OnProductConsumed Success11 : " + result.purchase.SKU);
                StartCoroutine(VerifyGooglePayLoad(result, vehicleData, packageData, petData));
                //StartCoroutine(GoogleReceiptRequest(result, vehicleData));
            }
        }
        else
        {
            if (OnPurchaseComplete != null)
            {
                OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
            }
            //PopupManager.instance.ShowPurchaseOK(TextDocs.content("POPUP_PURCHASE_FAIL_TITLE"),
            //    TextDocs.content("POPUP_PURCHASE_FAIL_TEXT"), null, null);
        }

        if (m_IsRestore)
        {
            m_IsRestore = false;
            m_CheckRestoreCount += 1;
            ProcessRestore();
        }

        Debug.Log("@@ Cousume Responce: " + result.response.ToString() + " " + result.message);
    }

    void ProcessRestore()
    {
        // 복구 할 수 있는 상품이 없을 때 콜백만 없애주기 위해 PurchaseCompleteState.Fail를 전송
        if (m_RestoreProductDic.Count <= 0)
        {
            if(OnPurchaseComplete != null)
            {
                OnPurchaseComplete(PurchaseCompleteState.Fail);
            }
        }

        if (m_RestoreProductDic.Count != m_CheckRestoreCount)
        {
            m_IsRestore = true;

            Dictionary<string, string>.Enumerator etor = m_RestoreProductDic.GetEnumerator();
            etor.MoveNext();

            for (int i = 0; i < m_CheckRestoreCount; i++)
            {
                etor.MoveNext();
            }

            Debug.Log("$$ ProcessRestore : " + etor.Current.Value + ", " + etor.Current.Key);
            AndroidInAppPurchaseManager.Client.Consume(etor.Current.Value);
        }
    }
#elif UNITY_IOS
    public void GameCenterInit(bool isFirst)
    {
		m_IsFirstLoginGameCenter = isFirst;
        GameCenterManager.Init();
    }

    void OnAuthFinished(ISN_Result res)
    {
        if (res.IsSucceeded)
        {
            Debug.Log("@@ GameCenter Player Authored -> " + "ID: " + GameCenterManager.Player.Id + "\n" + "Alias: " + GameCenterManager.Player.Alias + "\n" + "IsInitialized: " + GameCenterManager.IsInitialized + "\n" + "IsPlayerAuthenticated: " + GameCenterManager.IsPlayerAuthenticated + "\n" + "IsAchievementsInfoLoaded: " + GameCenterManager.IsAchievementsInfoLoaded);
			if(m_IsFirstLoginGameCenter)
            {
                IGAWorksManager.instance.FirstTimeExperience("Login_GameCenter");
            }

            PlayerPrefs.SetInt("BeCameGPLogin", 1);            

			AchievementDocs.instance.UpdateAchievement();

			OnGameCenterAuthComplete(true);
        }
        else
        {
			Debug.Log("@@ GameCenter Player auth failed : " + res.Error.Code + ", " + res.Error.Description);
			Debug.Log("@@ GameCenter Player auth failed : " + res.ToString());
            OnGameCenterAuthComplete(false);
        }
    }

	private void OnTransactionComplete(IOSStoreKitResult result)
	{
		Debug.Log("@@@ OnTransactionComplete : ProductIdentifier : " + result.ProductIdentifier);
		Debug.Log("@@@ OnTransactionComplete : Receipt : " + result.Receipt);
        Debug.Log("@@@ OnTransactionComplete : Sㅕtate : " + result.State);
		
		switch(result.State)
		{
		case InAppPurchaseState.Purchased:
		case InAppPurchaseState.Restored:
			{
				//Our product been succsesly purchased or restored
				//So we need to provide content to our user depends on productIdentifier
				//UnlockProducts(result.ProductIdentifier);
				//StockData stockData = ShopDocs.instance.stockDataList.Find(x => x.appleID.CompareTo(result.ProductIdentifier) == 0);
				//if (stockData != null)
				//{
				//	string stockName = string.Format(TextDocs.content(stockData.stockName), stockData.stockAmount);
    //                IGAWorksManager.instance.Buy(stockData.stockDesc);
				//	PopupManager.instance.ShowPurchaseOK(TextDocs.content("POPUP_PURCHASE_SUCCESS_TITLE"),
				//	                                     string.Format(TextDocs.content("POPUP_PURCHASE_SUCCESS_TEXT"), stockName),
				//	delegate
				//	{
				//		_hintCount += stockData.stockAmount;
				//		SaveUserData();
				//		OnRefreshHint(_hintCount);
				//	},
				//	delegate
				//	{
				//		_hintCount += stockData.stockAmount;
				//		SaveUserData();
				//		OnRefreshHint(_hintCount);
				//	});
				//}
    
                VehicleData vehicleData = VehicleDocs.instance.GetVehicleForApple(result.ProductIdentifier);
                PackageData packageData = PackageDocs.instance.GetPackageForApple(result.ProductIdentifier);
                PetData petData = PetDocs.instance.GetPetForApple(result.ProductIdentifier);
                if (vehicleData != null || packageData != null || petData != null)
                {
                    Debug.Log("@@@ OnTransactionComplete Success11 : " + result.ProductIdentifier);
                    StartCoroutine(VerifyiOSPurchase(result, vehicleData, packageData, petData));
                }
			}
			break;
		case InAppPurchaseState.Deferred:
			//iOS 8 introduces Ask to Buy, which lets parents approve any purchases initiated by children
			//You should update your UI to reflect this deferred state, and expect another Transaction Complete  to be called again with a new transaction state 
			//reflecting the parent’s decision or after the transaction times out. Avoid blocking your UI or gameplay while waiting for the transaction to be updated.
			if (OnPurchaseComplete != null)
			{
				OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
			}
			break;
		case InAppPurchaseState.Failed:
			//Our purchase flow is failed.
			//We can unlock intrefase and repor user that the purchase is failed.
			{
				Debug.Log("Transaction failed with error, code: " + result.Error.Code);
				Debug.Log("Transaction failed with error, description: " + result.Error.Description);
				if (OnPurchaseComplete != null)
				{
					OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
				}
				//PopupManager.instance.ShowPurchaseOK(TextDocs.content("POPUP_PURCHASE_FAIL_TITLE"),
			    //                                 TextDocs.content("POPUP_PURCHASE_FAIL_TEXT"), null, null);
			}
			break;
		}

		/*
		if(result.State == InAppPurchaseState.Failed)
		{
			if(result.Error.Code != 2)
			{
				IOSNativePopUpManager.showMessage("Transaction Failed", "Error code: " + result.Error.Code + "\n" + "Error description:" + result.Error.Description);
			}
		}
		else
		{
			//IOSNativePopUpManager.showMessage("Store Kit Response", "product " + result.ProductIdentifier + " state: " + result.State.ToString());
		}
		*/
	}	
	
	private void OnRestoreComplete(IOSStoreKitRestoreResult res)
	{
		if(res.IsSucceeded)
		{
			//IOSNativePopUpManager.showMessage("Success", "Restore Compleated");
		}
		else
		{
			IOSNativePopUpManager.showMessage("Error: " + res.Error.Code, res.Error.Description);

			if (OnPurchaseComplete != null)
			{
				OnPurchaseComplete(PurchaseCompleteState.Fail);
			}
		}
	}	
	
	private void OnVerificationComplete(IOSStoreKitVerificationResponse response)
	{
		//IOSNativePopUpManager.showMessage("Verification", "Transaction verification status: " + response.status.ToString());
		
		Debug.Log("ORIGINAL JSON: " + response.originalJSON);
	}

	void OnStoreKitInitComplete(ISN_Result result)
	{
		VehicleDocs.instance.UpdateProduct();
        PackageDocs.instance.UpdateProduct();
        PetDocs.instance.UpdateProduct();
		
		if(result.IsSucceeded)
		{
			Debug.Log("StoreKit Init Succeeded Available products count: " + IOSInAppPurchaseManager.instance.Products.Count.ToString());
			if (m_IAPState != IAPState.Restore)
			{
				if(m_ProccessVehicleData != null)
				{
					Debug.Log ("Init and BuyProduct");
					IOSInAppPurchaseManager.Instance.BuyProduct(m_ProccessVehicleData.appleID);
				}
                else if(m_ProccessPackageData != null)
                {
                    Debug.Log ("Init and BuyProduct");
					IOSInAppPurchaseManager.Instance.BuyProduct(m_ProccessPackageData.appleID);
                }
                else if(m_ProccessPetData != null)
                {
                    Debug.Log("Init and BuyProduct");
                    IOSInAppPurchaseManager.Instance.BuyProduct(m_ProccessPetData.appleID);
                }
			}
			else
			{
				IOSInAppPurchaseManager.Instance.RestorePurchases();
			}
		}
		else
		{
			Debug.Log("StoreKit Init Failed Error code: " + result.Error.Code + ", " + "Error description: " + result.Error.Description);
				
			if (OnPurchaseComplete != null)
			{
				OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
			}
		}
	}
#endif

    // ##################################################################################################
    //private IEnumerator GoogleReceiptRequest(BillingResult result, VehicleData vehicleData)
    //{
    //    //if (m_gameLoading.activeSelf == false)
    //    //    NGUITools.SetActive(m_gameLoading, true);

    //    WWWForm form = new WWWForm();
    //    form.AddField("ReceiptJson", result.purchase.originalJson);
    //    form.AddField("Signature", result.purchase.signature);
    //    form.AddField("UUID", SystemInfo.deviceUniqueIdentifier);


    //    WWW www = new WWW("http://burnoutcityiap-env.ap-northeast-2.elasticbeanstalk.com/google_validation", form);
    //    yield return www;

    //    if (www.error == null)
    //    {
    //        Debug.Log("www.text = " + www.text);

    //        JSONNode json = JSON.Parse(www.text);
    //        if (json["returnvalue"].AsInt == 0)
    //        {
    //            VehicleBuyToIAP(vehicleData);

    //            if (OnPurchaseComplete != null)
    //            {
    //                OnPurchaseComplete(PurchaseCompleteState.Success);
    //            }

    //            // 확인
    //            WWWForm form2 = new WWWForm();
    //            form2.AddField("ORDERID", result.purchase.orderId);
    //            form2.AddField("PRICE", vehicleData.tempPrice);

    //            WWW www2 = new WWW("http://burnoutcityiap-env.ap-northeast-2.elasticbeanstalk.com/google_payout", form2);
    //        }
    //        else
    //        {
    //            //NGUITools.SetActive(m_msgPopupObj, true);
    //            //m_msgPopup.SetMsg(3);

    //            if (OnPurchaseComplete != null)
    //            {
    //                OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
    //            }
    //        }
    //    }
    //    else {
    //        //NGUITools.SetActive(m_msgPopupObj, true);
    //        //m_msgPopup.SetMsg(3);

    //        Debug.Log(www.error);

    //        if (OnPurchaseComplete != null)
    //        {
    //            OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
    //        }
    //    }

    //    //if (m_gameLoading.activeSelf)
    //    //    NGUITools.SetActive(m_gameLoading, false);

    //    //if (_isBuying)
    //    //    _isBuying = false;
    //}

    // ##################################################################################################
    //private IEnumerator IosReceiptRequest(IOSStoreKitResult result, int stockIdx)
    //{
    //    if (m_gameLoading.activeSelf == false)
    //        NGUITools.SetActive(m_gameLoading, true);

    //    WWWForm form = new WWWForm();
    //    form.AddField("ReceiptData", result.Receipt);
    //    form.AddField("UUID", SystemInfo.deviceUniqueIdentifier);

    //    WWW www = new WWW("http://fisherinapp.ap-northeast-2.elasticbeanstalk.com/apple_validation", form);
    //    yield return www;

    //    if (www.error == null)
    //    {
    //        Debug.Log("www.text = " + www.text);

    //        JSONNode json = JSON.Parse(www.text);
    //        if (json["returnvalue"].AsInt == 0)
    //        {
    //            PaymentProducts(_stockIdx);

    //            // 확인
    //            string key = StockTable.key(stockIdx);

    //            WWWForm form2 = new WWWForm();
    //            form2.AddField("TRANSACTION_ID", result.TransactionIdentifier);
    //            form2.AddField("PRICE", StockTable.price(key).ToString());

    //            WWW www2 = new WWW("http://fisherinapp.ap-northeast-2.elasticbeanstalk.com/apple_payout", form2);
    //        }
    //        else
    //        {
    //            NGUITools.SetActive(m_msgPopupObj, true);
    //            m_msgPopup.SetMsg(3);
    //        }
    //    }
    //    else {
    //        NGUITools.SetActive(m_msgPopupObj, true);
    //        m_msgPopup.SetMsg(3);

    //        Debug.Log(www.error);
    //    }

    //    if (m_gameLoading.activeSelf)
    //        NGUITools.SetActive(m_gameLoading, false);
    //}

    public void RestorePurchase()
    {
#if UNITY_ANDROID
        if (m_LoadStore)
        {
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
                AndroidInAppPurchaseManager.Client.RetrieveProducDetails();
                AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetriveProductsFinisedForRestore;
            }
            else
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                    TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.Fail);
                }
            }
        }
        else
        {
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
				LoadStore(IAPPlatform.Android, IAPState.Restore);
            }
            else
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                    TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.Fail);
                }
            }
        }
#elif UNITY_IOS
		m_IsRestore = true;
		if(IOSInAppPurchaseManager.Instance.IsStoreLoaded)
		{
			if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
			{
				IOSInAppPurchaseManager.Instance.RestorePurchases();
			}
			else
			{
				PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
					TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

				if (OnPurchaseComplete != null)
				{
					OnPurchaseComplete(PurchaseCompleteState.Fail);
				}
			}
		}
		else
		{
			if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
			{
				LoadStore(IAPPlatform.IOS, IAPState.Restore);
			}
			else
			{
				PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
					TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);

				if (OnPurchaseComplete != null)
				{
					OnPurchaseComplete(PurchaseCompleteState.Fail);
				}
			}
		}
#endif
    }

    // ##################################################################################################
    // ##################################################################################################
    // ##################################################################################################
    public IEnumerator GetGooglePayLoad(VehicleData vehicleData, PackageData packageData, PetData petData)
    {
        int ivSize = 32;
        JSONClass rootNode = new JSONClass();
        JSONClass jsonUserData = new JSONClass();
        rootNode.Add("userId", new JSONData(SystemInfo.deviceUniqueIdentifier));
        if(vehicleData != null)
        {
            rootNode.Add("itemId", new JSONData(vehicleData.index));
            rootNode.Add("price", new JSONData(vehicleData.recordPrice));
        }
        else if(packageData != null)
        {
            rootNode.Add("itemId", new JSONData(packageData.index));
            rootNode.Add("price", new JSONData(packageData.recordPrice));
        }
        else if(petData != null)
        {
            rootNode.Add("itemId", new JSONData(petData.index));
            rootNode.Add("price", new JSONData(petData.recordPrice));
        }
        
        var cryptStr = MadCrypt.AESEncrypt256(rootNode.ToString(), m_DaeriKey, ivSize);

        WWWForm form = new WWWForm();

        form.AddField("serviceName", m_ServiceName);
        form.AddField("function", "getGooglePayLoad");
        form.AddField("data", cryptStr);
        form.AddField("length", rootNode.ToString().Length);

        WWW www = new WWW(m_DaeriUrl, form);
        yield return www;

        if (www.error == null)
        {
            string plainStr = MadCrypt.AESDecrypt256(www.text, m_DaeriKey, ivSize);
            //Debug.Log("@@ getGooglePayLoad Success www.text = " + www.text + " (" + plainStr + ")");

            JSONNode json = JSON.Parse(plainStr);
            string developerPayLoad = json["developerPayLoad"];
            m_DeveloperPayLoad = developerPayLoad;
            long create_time = (long)json["create_time"].AsDouble;

            //Debug.Log("@@ getGooglePayLoad developerPayLoad : " + developerPayLoad);
            //Debug.Log("@@ getGooglePayLoad create_time : " + create_time);

            if(vehicleData != null)
            {
                AndroidInAppPurchaseManager.Client.Purchase(vehicleData.googleID, developerPayLoad);
            }
            else if(packageData != null)
            {
                AndroidInAppPurchaseManager.Client.Purchase(packageData.googleID, developerPayLoad);
            }
            else if (petData != null)
            {
                AndroidInAppPurchaseManager.Client.Purchase(petData.googleID, developerPayLoad);
            }
        }
        else {
            //Debug.Log("@@ getGooglePayLoad Failed www.error = " + www.error);

            if (OnPurchaseComplete != null)
            {
                OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
            }
        }
    }

    public IEnumerator VerifyGooglePayLoad(BillingResult result, VehicleData vehicleData, PackageData packageData, PetData petData)
    {
        //Debug.Log("@@ VerifyGooglePayLoad Start");
        int ivSize = 32;
        JSONClass rootNode = new JSONClass();
        JSONClass jsonUserData = new JSONClass();
        //Debug.Log("@@ VerifyGooglePayLoad originalJson : " + result.purchase.originalJson);
        //Debug.Log("@@ VerifyGooglePayLoad developerPayload : " + result.purchase.developerPayload);
        //Debug.Log("@@ VerifyGooglePayLoad m_DeveloperPayLoad : " + m_DeveloperPayLoad);
        //Debug.Log("@@ VerifyGooglePayLoad signature : " + result.purchase.signature);
        //Debug.Log("@@ VerifyGooglePayLoad token : " + result.purchase.token);
        rootNode.Add("purchaseData", JSON.Parse(result.purchase.originalJson));
        //rootNode.Add("purchaseData", new JSONData(result.purchase.originalJson));
        //Debug.Log("@@ VerifyGooglePayLoad rootNode.ToString() : " + rootNode.ToString());
        var cryptStr = MadCrypt.AESEncrypt256(rootNode.ToString(), m_DaeriKey, ivSize);

        WWWForm form = new WWWForm();

        form.AddField("serviceName", m_ServiceName);
        form.AddField("function", "verifyGooglePayLoad");
        form.AddField("data", cryptStr);
        form.AddField("length", rootNode.ToString().Length);

        //Debug.Log("@@ VerifyGooglePayLoad 0");

        WWW www = new WWW(m_DaeriUrl, form);
        yield return www;

        //Debug.Log("@@ VerifyGooglePayLoad 1");

        if (www.error == null)
        {
            //Debug.Log("@@ verifyGooglePayLoad Success www.text = " + www.text);
            string plainStr = MadCrypt.AESDecrypt256(www.text, m_DaeriKey, ivSize);
            //Debug.Log("@@ verifyGooglePayLoad Success plainStr = " + plainStr);

            JSONNode json = JSON.Parse(plainStr);
            bool success = json["success"].AsBool;

            //Debug.Log("@@ verifyGooglePayLoad success : " + success);

            if (success)
            {
                m_IsPurchased = true;
                m_PurchaseCount++;
                VehicleBuyToIAP(vehicleData, packageData, petData);

                if(m_PurchaseCount >= 3)
                {
                    IGAWorksManager.instance.SetTargetingData("PurchaseCount", 3);
                }

                IGAWorksManager.instance.Buy(result.purchase.SKU);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.Success);
                }

                TrySavedGames();
            }
            else
            {
                int errorDetail = json["detail"].AsInt;

                //Debug.Log("@@ verifyGooglePayLoad errorDetail : " + errorDetail);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
                }
            }            
        }
        else {

            //Debug.Log("@@ verifyGooglePayLoad Failed www.error = " + www.error);

            if (OnPurchaseComplete != null)
            {
                OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
            }
        }
    }

    public IEnumerator VerifyiOSPurchase(IOSStoreKitResult result, VehicleData vehicleData, PackageData packageData, PetData petData)
    {
        Debug.Log("@@@ VerifyiOSPurchase Start");
        int ivSize = 32;
        JSONClass rootNode = new JSONClass();
        JSONClass jsonUserData = new JSONClass();
        rootNode.Add("userId", new JSONData(SystemInfo.deviceUniqueIdentifier));
        if(vehicleData != null)
        {
            rootNode.Add("price", new JSONData(vehicleData.recordPrice));
        }
        else if(packageData != null)
        {
            rootNode.Add("price", new JSONData(packageData.recordPrice));
        }
        else if (petData != null)
        {
            rootNode.Add("price", new JSONData(petData.recordPrice));
        }
        rootNode.Add("receipt", new JSONData(result.Receipt));
		Debug.Log("@@@ VerifyiOSPurchase 0 : " + rootNode.ToString());
        var cryptStr = MadCrypt.AESEncrypt256(rootNode.ToString(), m_DaeriKey, ivSize);
		Debug.Log("@@@ VerifyiOSPurchase 1 : " + cryptStr);
        
        WWWForm form = new WWWForm();

        form.AddField("serviceName", m_ServiceName);
        form.AddField("function", "verifyiOSPurchase");
        form.AddField("data", cryptStr);
        form.AddField("length", rootNode.ToString().Length);

        Debug.Log("@@@ VerifyiOSPurchase 2");

        WWW www = new WWW(m_DaeriUrl, form);
        yield return www;

        Debug.Log("@@@ VerifyiOSPurchase 3");

        if (www.error == null)
        {
            Debug.Log("@@@ VerifyiOSPurchase Success www.text = " + www.text);
            string plainStr = MadCrypt.AESDecrypt256(www.text, m_DaeriKey, ivSize);
            Debug.Log("@@@ VerifyiOSPurchase Success plainStr = " + plainStr);

            JSONNode json = JSON.Parse(plainStr);
            bool success = json["success"].AsBool;

            Debug.Log("@@@ VerifyiOSPurchase success : " + success);

            if (success)
            {
                m_IsPurchased = true;
                m_PurchaseCount++;
                VehicleBuyToIAP(vehicleData, packageData, petData);

                if (m_PurchaseCount >= 3)
                {
                    IGAWorksManager.instance.SetTargetingData("PurchaseCount", 3);
                }

                IGAWorksManager.instance.Buy(result.ProductIdentifier);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.Success);
                }

                TrySavedGames();
            }
            else
            {
                int errorDetail = json["detail"].AsInt;

                Debug.Log("@@@ VerifyiOSPurchase errorDetail : " + errorDetail);

                if (OnPurchaseComplete != null)
                {
                    OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
                }
            }
        }
        else {

            Debug.Log("@@@ VerifyiOSPurchase Failed www.error = " + www.error);

            if (OnPurchaseComplete != null)
            {
                OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
            }
        }
    }

    public IEnumerator ConfirmCoupon(string couponNumber)
    {
#if !UNITY_IOS
        PopupManager.instance.ShowLoadingPopup();

        Debug.Log("@@ ConfirmCoupon Start");
        int ivSize = 32;
        JSONClass rootNode = new JSONClass();
        JSONClass jsonUserData = new JSONClass();
        rootNode.Add("coupon", new JSONData(couponNumber));
        rootNode.Add("userId", new JSONData(SystemInfo.deviceUniqueIdentifier));
        var cryptStr = MadCrypt.AESEncrypt256(rootNode.ToString(), m_DaeriKey, ivSize);

        WWWForm form = new WWWForm();

        form.AddField("serviceName", m_ServiceName);
        form.AddField("function", "confirmCoupon");
        form.AddField("data", cryptStr);
        form.AddField("length", rootNode.ToString().Length);

        WWW www = new WWW(m_DaeriUrl, form);
        yield return www;

        if (www.error == null)
        {
            Debug.Log("@@ confirmCoupon Success www.text = " + www.text);
            string plainStr = MadCrypt.AESDecrypt256(www.text, m_DaeriKey, ivSize);
            Debug.Log("@@ confirmCoupon Success plainStr = " + plainStr);

            JSONNode json = JSON.Parse(plainStr);
            bool success = json["success"].AsBool;
            int detail = json["detail"].AsInt;
            Debug.Log("@@ confirmCoupon success : " + success);
            if(success)
            {
                string item = json["item"];
                int giftRewardType = -1;
                int amount = 0;
                for(int i = 0; i < GameConfig.CouponRewardName.Length; i++)
                {
                    if(item.StartsWith(GameConfig.CouponRewardName[i]))
                    {
                        giftRewardType = i;
                        amount = int.Parse(item.Substring(GameConfig.CouponRewardName[i].Length, item.Length - GameConfig.CouponRewardName[i].Length));
                        break;
                    }
                }

                if(giftRewardType != -1)
                {
                    string giftName = "";
                    if (giftRewardType < 4)
                    {
                        m_TokenDic[giftRewardType] += amount;
                        SaveUserData();
                        giftName = string.Format(TextDocs.content("UI_TOKEN_TEXT"), GameConfig.TokenName[giftRewardType]);
                    }
                    else if (giftRewardType == 4)
                    {
                        cash += amount;
                        giftName = TextDocs.content("UI_CASH_TEXT");
                    }

                    PopupManager.instance.ShowSmallOK(TextDocs.content("COUPON_SUCCESS_TITLE"), string.Format(TextDocs.content("BT_GIFT_GET_TEXT"), giftName, amount), null, null);
                }
            }
            else
            {
                string errorText = "COUPON_INPUT_ERROR";
                if(detail == 1)
                {
                    errorText = "COUPON_ERROR_MSG_1";
                }
                else if (detail == 2)
                {
                    errorText = "COUPON_ERROR_MSG_2";
                }
                else if (detail == 3)
                {
                    errorText = "COUPON_ERROR_MSG_3";
                }

                PopupManager.instance.ShowSmallOK(TextDocs.content("COUPON_FAIL_TITLE"), TextDocs.content(errorText), null, null);
            }
        }
        else {
            Debug.Log("@@ confirmCoupon Failed www.error = " + www.error);
        }

        PopupManager.instance.HideLoadingPopup();
#else
		yield return null;
#endif
    }

    public void InitializeResolution()
    {
        m_ScreenWidth = Screen.width;
        m_ScreenHeight = Screen.height;
    }

    public void RestoreResolution()
    {
        Screen.SetResolution(m_ScreenWidth, m_ScreenHeight, true);
    }

    public void ChangeLowResolution()
    {
        if(Screen.width > 1280)
        {
            float ratio = (float)((float)Screen.height / (float)Screen.width);
            float changeHeight = 1280.0f * ratio;
            Screen.SetResolution(1280, (int)changeHeight, true);
        }
    }
}

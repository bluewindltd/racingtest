﻿using UnityEngine;
using System;
using System.Collections;
using SimpleJSON;

public class TimeManager : Singleton<TimeManager>
{
    bool m_onlineTime = false;
    long m_time = 0;
    long m_localtime = 0;
    float m_deltaTime = 0;
    float m_currentGameTime = 0;
    public float currentGameTime
    {
        get
        {
            return m_currentGameTime;
        }
    }

    // ##################################################################################################
    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    // ##################################################################################################
    // Use this for initialization
    void Start()
    {
        StartCoroutine("TimeRequest");
    }

    // ##################################################################################################
    // Update is called once per frame
    void FixedUpdate()
    {
        m_currentGameTime = Time.time;

        //if (m_onlineTime)
        //{
        //    m_deltaTime += Time.deltaTime;

        //    // 초기화 했을 경우 게임 시간을 더해준다.
        //    m_time += (long)(Time.deltaTime * 1000);
        //    //Debug.Log("time : " + m_time);
        //}
        //else
        //{
        //    m_deltaTime += Time.deltaTime;

        //    // 초기화 못했을 경우 2분에 한번씩 재요청
        //    if (m_deltaTime > 120)
        //    {
        //        m_deltaTime = 0;
        //        StartCoroutine("TimeRequest");
        //    }
        //}

        if (!m_onlineTime)
        {
            m_deltaTime += Time.deltaTime;

            // 초기화 못했을 경우 2분에 한번씩 재요청
            if (m_deltaTime > 120)
            {
                m_deltaTime = 0;
                StartCoroutine("TimeRequest");
            }
        }
    }

    // ##################################################################################################
    private IEnumerator TimeRequest()
    {
        WWW www = new WWW("http://timeserver-env.elasticbeanstalk.com/time");
        yield return www;

        if (www.error == null)
        {
            Debug.Log("www.text = " + www.text);

            JSONNode json = JSON.Parse(www.text);
            string time = json["time"];
            m_time = System.Convert.ToInt64(time);
            m_localtime = System.DateTime.Now.Ticks;
            Debug.Log("time = " + m_time);

            m_onlineTime = true;
        }
        else
        {
            Debug.Log("www.error = " + www.error);
        }
    }

    // ##################################################################################################
    public void ForceTimeRequest()
    {
        m_deltaTime = 0;
        StartCoroutine("TimeRequest");
    }

    // ##################################################################################################
    public bool GetTimeState()
    {
        return m_onlineTime;
    }

    // ##################################################################################################
    public long GetOnlineTime()
    {       
        return m_time + ((System.DateTime.Now.Ticks - m_localtime) / 10000);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class ChaserCountDocs : Singleton<ChaserCountDocs>
{
    Dictionary<int, int> m_ChaserCountDic = new Dictionary<int, int>();
    public Dictionary<int, int> chaserCountDic
    {
        get
        {
            return m_ChaserCountDic;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_ChaserCountDic.Count <= 0)
        {
            Dictionary<string, object> allDataByChaserCountTableSchema;
            GDEDataManager.GetAllDataBySchema("ChaserCountTable", out allDataByChaserCountTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByChaserCountTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> chaserCountTableDic = etorTable.Current.Value as Dictionary<string, object>;

                int count = 0;
                m_ChaserCountDic.Add(0, count);
                chaserCountTableDic.TryGetInt("star1", out count);
                m_ChaserCountDic.Add(1, count);
                chaserCountTableDic.TryGetInt("star2", out count);
                m_ChaserCountDic.Add(2, count);
                chaserCountTableDic.TryGetInt("star3", out count);
                m_ChaserCountDic.Add(3, count);
                chaserCountTableDic.TryGetInt("star4", out count);
                m_ChaserCountDic.Add(4, count);
                chaserCountTableDic.TryGetInt("star5", out count);
                m_ChaserCountDic.Add(5, count);
                chaserCountTableDic.TryGetInt("star6", out count);
                m_ChaserCountDic.Add(6, count);

                break;
            }
        }
    }
}

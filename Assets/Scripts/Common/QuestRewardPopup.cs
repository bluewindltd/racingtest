﻿using UnityEngine;
using System.Collections;

public class QuestRewardPopup : BasePopup
{
    public UILabel titleLabel;
    public UISprite profileSprite;
    public UILabel completeLabel;
    public UILabel questContentTitleLabel;
    public UILabel questContentLabel;
    public UILabel rewardTitleLabel;
    public UILabel completeRewardNameLabel;
    public UILabel levelupRewardNameLabel;
    public UILabel friendshipTitleLabel;
    public UILabel getExpLabel;
    public Transform getExpTransform;
    public UILabel levelExpLabel;
    public UISprite expGaugeSprite;
    public UILabel okButtonLabel;

    public Animator levelProgressAnimator;
    public Animator talkboxAnimator;
    public Animator rewardAnimator;

    public Transform rewardRootTransform;
    public GameObject moneyRewardObject;        // 토큰, 캐쉬 같은 화폐
    public UISprite moneyRewardSprite;          // 토큰, 캐쉬 같은 화폐

    int m_PreLevel = 0;
    int m_PreCurExp = 0;
    int m_PreEndExp = 0;
    float m_CurrentFillAmount = 0.0f;
    float m_DestFillAmount = 0.0f;

    QuestClientInfo m_QuestClientInfo = null;
    QuestClientData m_QuestClientData = null;

    public static QuestRewardPopup Show(GameObject parent, GameObject prefab)
    {
        QuestRewardPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<QuestRewardPopup>();

        popup.titleLabel.text = TextDocs.content("UI_QUEST_SUCCESS");
        popup.questContentTitleLabel.text = TextDocs.content("UI_QUEST_DESC");
        popup.rewardTitleLabel.text = TextDocs.content("UI_QUEST_REWORD");
        popup.friendshipTitleLabel.text = TextDocs.content("UI_QUEST_FRIENDLY");
        popup.okButtonLabel.text = TextDocs.content("BT_OK_TEXT");

        popup.OnCancel = delegate
        {
            SoundManager.instance.StopLoopSFX("FriendshipLevelupGage");
        };

        return popup;
    }

    public void Initialize(QuestClientInfo questClientInfo)
    {
        QuestManager.OnQuestComplete += OnQuestComplete;

        m_QuestClientInfo = questClientInfo;
        m_QuestClientData = m_QuestClientInfo.questClientData;
        QuestFriendshipData questFriendshipData = null;
        int curExp = 0;
        int endExp = 0;

        profileSprite.spriteName = m_QuestClientData.imageName;

        if (m_QuestClientData.lastFriendshipLevel <= m_QuestClientInfo.friendshipLevel)
        {
            if (m_QuestClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel - 1))
            {
                questFriendshipData = m_QuestClientData.friendshipDic[m_QuestClientInfo.friendshipLevel - 1];
                curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
                    m_QuestClientInfo.friendshipLevel, curExp, endExp);
                m_CurrentFillAmount = Mathf.Clamp01((float)curExp / (float)endExp);
                expGaugeSprite.fillAmount = m_CurrentFillAmount;
                getExpTransform.localPosition = new Vector3((160.0f * m_CurrentFillAmount) - 80.0f, 9.0f, 0.0f);

                m_PreLevel = m_QuestClientInfo.friendshipLevel;
                m_PreCurExp = curExp;
                m_PreEndExp = endExp;
            }
        }
        else
        {
            if (m_QuestClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel))
            {
                questFriendshipData = m_QuestClientData.friendshipDic[m_QuestClientInfo.friendshipLevel];
                curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
                    m_QuestClientInfo.friendshipLevel, curExp, endExp);
                m_CurrentFillAmount = Mathf.Clamp01((float)curExp / (float)endExp);
                expGaugeSprite.fillAmount = m_CurrentFillAmount;
                getExpTransform.localPosition = new Vector3((160.0f * m_CurrentFillAmount) - 80.0f, 9.0f, 0.0f);

                m_PreLevel = m_QuestClientInfo.friendshipLevel;
                m_PreCurExp = curExp;
                m_PreEndExp = endExp;
            }
        }

        //if (m_QuestClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel))
        //{
        //    questFriendshipData = m_QuestClientData.friendshipDic[m_QuestClientInfo.friendshipLevel];
        //    curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
        //    endExp = questFriendshipData.endExp - questFriendshipData.startExp;
        //    levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
        //        m_QuestClientInfo.friendshipLevel, curExp, endExp);
        //    m_CurrentFillAmount = Mathf.Clamp01((float)curExp / (float)endExp);
        //    expGaugeSprite.fillAmount = m_CurrentFillAmount;
        //    getExpTransform.localPosition = new Vector3((160.0f * m_CurrentFillAmount) - 80.0f, 9.0f, 0.0f);

        //    m_PreLevel = m_QuestClientInfo.friendshipLevel;
        //    m_PreCurExp = curExp;
        //    m_PreEndExp = endExp;
        //}

        QuestData questData = QuestManager.instance.curQuestData;
        int questIndex = QuestManager.instance.questProgressInfo.curQuestIndex;
        if (questData != null)
        {
            string questContent = "{0}";
            if (questIndex == 0)
            {
                questContent = m_QuestClientData.questContent1;
            }
            else if (questIndex == 1)
            {
                questContent = m_QuestClientData.questContent2;
            }
            questContentLabel.text = string.Format(TextDocs.content(questContent), questData.param);
        }
        else
        {
            questContentLabel.text = "";
        }

        completeLabel.text = TextDocs.content(m_QuestClientData.questComplete);
    }

    IEnumerator StartComplete(int exp, int completeRewardAmount, bool isLevelup, bool isLevelupReward, QuestFriendshipData nextFriendshipData)
    {
        SoundManager.instance.PlaySFX("QuestComplete");

        talkboxAnimator.Play("Quest_Top_Talkbox_Show");
        rewardAnimator.Play("Quest_Reward_1_Show");
        levelProgressAnimator.Play("Quest_Progress_Normal");
        completeRewardNameLabel.text = GameConfig.GetRewardName(GameConfig.RewardType.Cash, completeRewardAmount);
        getExpLabel.text = string.Format("+{0}", exp);

        SoundManager.instance.PlayLoopSFX("FriendshipLevelupGage");

        yield return StartCoroutine(ProduceFillExpGauge(exp, isLevelup, isLevelupReward, nextFriendshipData));
    }

    IEnumerator ProduceFillExpGauge(int exp, bool isLevelup, bool isLevelupReward, QuestFriendshipData nextFriendshipData)
    {
        int curExp = 0;
        int endExp = 0;
        int levelupCount = 0;

        // 레벨업 될 때
        if (isLevelup)
        {
            m_DestFillAmount = 1.0f;
            levelupCount = 1;
        }
        else
        {
            m_DestFillAmount = Mathf.Clamp01((float)(m_PreCurExp + exp) / (float)m_PreEndExp);
        }
        while (m_DestFillAmount > m_CurrentFillAmount)
        {
            expGaugeSprite.fillAmount = m_CurrentFillAmount;
            getExpTransform.localPosition = new Vector3((160.0f * m_CurrentFillAmount) - 80.0f, 9.0f, 0.0f);

            yield return null;

            m_CurrentFillAmount = Mathf.Lerp(m_CurrentFillAmount, m_DestFillAmount, Time.deltaTime * 4.0f);
            curExp = Mathf.FloorToInt(m_CurrentFillAmount * (float)m_PreEndExp);
            Debug.Log("@@ ProduceFillExpGauge : " + m_PreLevel + ", " + curExp + ", " + m_PreEndExp + ", " + m_CurrentFillAmount + ", " + m_DestFillAmount);
            levelExpLabel.text = string.Format("LV.{0}   {1}/{2}", m_PreLevel, curExp, m_PreEndExp);

            // 선형 보간의 경우 목표 수치와 동일 해질 수 없기 때문에 일정 여분 수치만 남았을 때는,
            // 목표 수치까지 도달했다고 보고 보간을 끝낸다.
            if (Mathf.Abs(m_CurrentFillAmount - m_DestFillAmount) < 0.01f)
            {
                m_CurrentFillAmount = m_DestFillAmount;
                expGaugeSprite.fillAmount = m_CurrentFillAmount;
                getExpTransform.localPosition = new Vector3((160.0f * m_CurrentFillAmount) - 80.0f, 9.0f, 0.0f);

                if (levelupCount > 0)
                {
                    if(isLevelupReward)
                    {
                        rewardAnimator.Play("Quest_Reward_2_Show");

                        if (GameConfig.RewardType.C <= nextFriendshipData.levelUpRewardType &&
                           nextFriendshipData.levelUpRewardType <= GameConfig.RewardType.Cash)
                        {
                            NGUITools.SetActive(moneyRewardObject, true);
                            moneyRewardSprite.spriteName = GameConfig.MoneyImage[(int)nextFriendshipData.levelUpRewardType];
                        }
                        else if (nextFriendshipData.levelUpRewardType == GameConfig.RewardType.Vehicle)
                        {
                            NGUITools.SetActive(moneyRewardObject, false);
                            if (nextFriendshipData.levelUpRewardValue < GoodsManager.instance.vehicleObjectArray.Length)
                            {
                                ShopVehicle shopVehicle = NGUITools.AddChild(rewardRootTransform.gameObject, GoodsManager.instance.vehicleObjectArray[nextFriendshipData.levelUpRewardValue]).GetComponent<ShopVehicle>();
                                shopVehicle.SetQuestReward();
                            }
                        }
                        else if (nextFriendshipData.levelUpRewardType == GameConfig.RewardType.Pet)
                        {
                            NGUITools.SetActive(moneyRewardObject, false);
                        }

                        levelupRewardNameLabel.text = GameConfig.GetRewardName(nextFriendshipData.levelUpRewardType, nextFriendshipData.levelUpRewardValue);
                    }

                    SoundManager.instance.PlaySFX("FriendshipLevelup");

                    talkboxAnimator.Play("Quest_Top_Talkbox_Hide");
                    levelProgressAnimator.Play("Quest_Progress_Levelup");

                    m_CurrentFillAmount = 0.0f;

                    curExp = m_QuestClientInfo.allExp - nextFriendshipData.startExp;
                    endExp = nextFriendshipData.endExp - nextFriendshipData.startExp;

                    if(curExp == 0 && endExp != 0)          // 경험치가 딱 떨어지게 레벨업 했을 시
                    {
                        m_CurrentFillAmount = 0.0f;
                        m_DestFillAmount = 0.0f;
                        expGaugeSprite.fillAmount = m_CurrentFillAmount;
                        getExpTransform.localPosition = new Vector3((160.0f * m_CurrentFillAmount) - 80.0f, 9.0f, 0.0f);

                        levelExpLabel.text = string.Format("LV.{0}   {1}/{2}", m_PreLevel, m_PreEndExp, m_PreEndExp);

                        m_PreLevel = m_QuestClientInfo.friendshipLevel;
                        m_PreCurExp = curExp;
                        m_PreEndExp = endExp;
                    }
                    else if(curExp == 0 && endExp == 0)     // 마지막 레벨업시
                    {
                        m_CurrentFillAmount = 1.0f;
                        m_DestFillAmount = 1.0f;
                        expGaugeSprite.fillAmount = m_CurrentFillAmount;
                        getExpTransform.localPosition = new Vector3((160.0f * m_CurrentFillAmount) - 80.0f, 9.0f, 0.0f);

                        m_PreLevel = m_QuestClientInfo.friendshipLevel;
                        levelExpLabel.text = string.Format("LV.{0}   {1}/{2}", m_PreLevel, m_PreEndExp, m_PreEndExp);
                    }
                    else                                    // 일반적인 레벨업 상황
                    {
                        m_DestFillAmount = Mathf.Clamp01((float)curExp / (float)endExp);

                        levelExpLabel.text = string.Format("LV.{0}   {1}/{2}", m_PreLevel, m_PreEndExp, m_PreEndExp);

                        m_PreLevel = m_QuestClientInfo.friendshipLevel;
                        m_PreCurExp = curExp;
                        m_PreEndExp = endExp;
                    }

                    levelupCount--;
                }
                else
                {
                    // 레벨 업 시에는 레벨업 넘어가는 시점에 다음 경험치를 m_PreCurExp에 설정해놓았다.
                    if (isLevelup)
                    {
                        levelExpLabel.text = string.Format("LV.{0}   {1}/{2}", m_PreLevel, m_PreCurExp, m_PreEndExp);
                    }
                    else
                    {
                        levelExpLabel.text = string.Format("LV.{0}   {1}/{2}", m_PreLevel, m_PreCurExp + exp, m_PreEndExp);
                    }
                }
            }
        }

        SoundManager.instance.StopLoopSFX("FriendshipLevelupGage");

        if (isLevelup)
        {
            yield return new WaitForSeconds(0.4f);

            completeLabel.text = TextDocs.content(m_QuestClientData.questLevelUp);

            talkboxAnimator.Play("Quest_Top_Talkbox_Show");
        }
    }

    void OnQuestComplete(int exp, int completeRewardAmount, bool isLevelup, bool isLevelupReward, QuestFriendshipData nextFriendshipData)
    {
        QuestManager.OnQuestComplete -= OnQuestComplete;

        StartCoroutine(StartComplete(exp, completeRewardAmount, isLevelup, isLevelupReward, nextFriendshipData));
    }

    public void OnClickOk()
    {
        SoundManager.PlayButtonClickSound();

        SoundManager.instance.StopLoopSFX("FriendshipLevelupGage");

        Hide();
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : Singleton<SoundManager> {
    private Dictionary<string, AudioClip> m_AudioClipList = new Dictionary<string, AudioClip>();
    private Dictionary<string, AudioSource> m_LoopSFXAudioSource = new Dictionary<string, AudioSource>();

    AudioSource m_BGMAudioSource;
    AudioSource m_FXAudioSource;

    float m_GameSFXVolumeControl = 1.0f;
    public float gameSFXVolumeControl
    {
        get
        {
            return m_GameSFXVolumeControl;
        }
        set
        {
            m_GameSFXVolumeControl = value;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this); // 씬이 변경되어도 파괴되지 않게 하기 위해서.

        m_BGMAudioSource = gameObject.AddComponent<AudioSource>();
        m_BGMAudioSource.playOnAwake = false;
        m_BGMAudioSource.loop = true;

        m_FXAudioSource = gameObject.AddComponent<AudioSource>();
        m_FXAudioSource.playOnAwake = false;

        SetBGMVolume(PlayerPrefs.GetFloat("BGMVolume", 1.0f));
        SetSFXVolume(PlayerPrefs.GetFloat("SFXVolume", 1.0f));

        // BGM
        m_AudioClipList.Add("GAME_BGM", Resources.Load("Sound/BGM/GAME_BGM") as AudioClip);
        //m_AudioClipList.Add("STAGE_BGM", Resources.Load("Sounds/BGM/STAGE_BGM") as AudioClip);
        //m_AudioClipList.Add("INTRO", Resources.Load("Sounds/BGM/INTRO") as AudioClip);

        // SFX
        m_AudioClipList.Add("ApacheShot", Resources.Load("Sound/Effect/ApacheShot") as AudioClip);
        m_AudioClipList.Add("BT", Resources.Load("Sound/Effect/BT") as AudioClip);
        m_AudioClipList.Add("Busted", Resources.Load("Sound/Effect/Busted") as AudioClip);
        m_AudioClipList.Add("carhit0", Resources.Load("Sound/Effect/carhit0") as AudioClip);
        m_AudioClipList.Add("carhit1", Resources.Load("Sound/Effect/carhit1") as AudioClip);
        m_AudioClipList.Add("carhit2", Resources.Load("Sound/Effect/carhit2") as AudioClip);
        m_AudioClipList.Add("carhit3", Resources.Load("Sound/Effect/carhit3") as AudioClip);
        m_AudioClipList.Add("carhit4", Resources.Load("Sound/Effect/carhit4") as AudioClip);
        m_AudioClipList.Add("ChopperShot", Resources.Load("Sound/Effect/ChopperShot") as AudioClip);
        m_AudioClipList.Add("EvilCount", Resources.Load("Sound/Effect/EvilCount") as AudioClip);
        m_AudioClipList.Add("Gacha", Resources.Load("Sound/Effect/Gacha") as AudioClip);
        m_AudioClipList.Add("GangsterShot", Resources.Load("Sound/Effect/GangsterShot") as AudioClip);
        m_AudioClipList.Add("GiftGet", Resources.Load("Sound/Effect/GiftGet") as AudioClip);
        m_AudioClipList.Add("GiftOpen", Resources.Load("Sound/Effect/GiftOpen") as AudioClip);
        m_AudioClipList.Add("Healing", Resources.Load("Sound/Effect/Healing") as AudioClip);
        m_AudioClipList.Add("HummerShot", Resources.Load("Sound/Effect/HummerShot") as AudioClip);
        m_AudioClipList.Add("Intro", Resources.Load("Sound/Effect/Intro") as AudioClip);
        m_AudioClipList.Add("intro_engine", Resources.Load("Sound/Effect/intro_engine") as AudioClip);
        m_AudioClipList.Add("MissionFail", Resources.Load("Sound/Effect/MissionFail") as AudioClip);
        m_AudioClipList.Add("MissionStart", Resources.Load("Sound/Effect/MissionStart") as AudioClip);
        m_AudioClipList.Add("MissionSuccess", Resources.Load("Sound/Effect/MissionSuccess") as AudioClip);
        m_AudioClipList.Add("result", Resources.Load("Sound/Effect/result") as AudioClip);
        m_AudioClipList.Add("ShopIn", Resources.Load("Sound/Effect/ShopIn") as AudioClip);
        m_AudioClipList.Add("ShopOut", Resources.Load("Sound/Effect/ShopOut") as AudioClip);
        m_AudioClipList.Add("siren", Resources.Load("Sound/Effect/siren") as AudioClip);
        m_AudioClipList.Add("tirescreech", Resources.Load("Sound/Effect/tirescreech") as AudioClip);
        m_AudioClipList.Add("Wasted", Resources.Load("Sound/Effect/Wasted") as AudioClip);
        m_AudioClipList.Add("DroneShot", Resources.Load("Sound/Effect/DroneShot") as AudioClip);
        m_AudioClipList.Add("QuestSelecting", Resources.Load("Sound/Effect/QuestSelecting") as AudioClip);
        m_AudioClipList.Add("QuestSelectingComplete", Resources.Load("Sound/Effect/QuestSelectingComplete") as AudioClip);
        m_AudioClipList.Add("QuestSelectShow", Resources.Load("Sound/Effect/QuestSelectShow") as AudioClip);
        m_AudioClipList.Add("FriendshipLevelupGage", Resources.Load("Sound/Effect/FriendshipLevelupGage") as AudioClip);
        m_AudioClipList.Add("FriendshipLevelup", Resources.Load("Sound/Effect/FriendshipLevelup") as AudioClip);
        m_AudioClipList.Add("QuestComplete", Resources.Load("Sound/Effect/QuestComplete") as AudioClip);
        m_AudioClipList.Add("battleball_laser", Resources.Load("Sound/Effect/battleball_laser") as AudioClip);
        m_AudioClipList.Add("starwing_laser", Resources.Load("Sound/Effect/starwing_laser") as AudioClip);
        m_AudioClipList.Add("PumpkinShot", Resources.Load("Sound/Effect/PumpkinShot") as AudioClip);

        //m_AudioClipList.Add("WORD_CLICK_1", Resources.Load("Sounds/Effect/WORD_CLICK_1") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_2", Resources.Load("Sounds/Effect/WORD_CLICK_2") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_3", Resources.Load("Sounds/Effect/WORD_CLICK_3") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_4", Resources.Load("Sounds/Effect/WORD_CLICK_4") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_5", Resources.Load("Sounds/Effect/WORD_CLICK_5") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_6", Resources.Load("Sounds/Effect/WORD_CLICK_6") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_7", Resources.Load("Sounds/Effect/WORD_CLICK_7") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_8", Resources.Load("Sounds/Effect/WORD_CLICK_8") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_9", Resources.Load("Sounds/Effect/WORD_CLICK_9") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_10", Resources.Load("Sounds/Effect/WORD_CLICK_10") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_11", Resources.Load("Sounds/Effect/WORD_CLICK_11") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_12", Resources.Load("Sounds/Effect/WORD_CLICK_12") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_13", Resources.Load("Sounds/Effect/WORD_CLICK_13") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_14", Resources.Load("Sounds/Effect/WORD_CLICK_14") as AudioClip);
        //m_AudioClipList.Add("WORD_CLICK_15", Resources.Load("Sounds/Effect/WORD_CLICK_15") as AudioClip);
        //m_AudioClipList.Add("START", Resources.Load("Sounds/Effect/START") as AudioClip);
        //m_AudioClipList.Add("CLEAR", Resources.Load("Sounds/Effect/CLEAR") as AudioClip);
        //m_AudioClipList.Add("AREA_CLEAR", Resources.Load("Sounds/Effect/AREA_CLEAR") as AudioClip);
        //m_AudioClipList.Add("HINT", Resources.Load("Sounds/Effect/HINT") as AudioClip);
        //m_AudioClipList.Add("RESET", Resources.Load("Sounds/Effect/RESET") as AudioClip);
        //m_AudioClipList.Add("SUCCESS", Resources.Load("Sounds/Effect/SUCCESS") as AudioClip);
        //m_AudioClipList.Add("FAIL", Resources.Load("Sounds/Effect/FAIL") as AudioClip);
    }

    public float GetBGMVolume()
    {
        return m_BGMAudioSource.volume;
    }

    public float GetSFXVolume()
    {
        return m_FXAudioSource.volume;
    }

    public void SetBGMVolume(float volume)
    {
        m_BGMAudioSource.volume = volume;
    }

    public void SetSFXVolume(float volume)
    {
        m_FXAudioSource.volume = volume;

        Dictionary<string, AudioSource>.Enumerator enumerator = m_LoopSFXAudioSource.GetEnumerator();
        while (enumerator.MoveNext())
        {
            AudioSource audioSource = enumerator.Current.Value;
            audioSource.volume = volume;
        }
    }

    public void PlayBGM(string audioClipName)
    {
        Debug.Log("PlayBGM = " + audioClipName);
        if (m_AudioClipList.ContainsKey(audioClipName) == true)
        {
            AudioClip audioClip = m_AudioClipList[audioClipName];
            if (m_BGMAudioSource.clip != audioClip)
            {
                if (m_BGMAudioSource.isPlaying == true)
                {
                    m_BGMAudioSource.Stop();
                }
            }

            m_BGMAudioSource.clip = audioClip;
            m_BGMAudioSource.Play();            
        }
    }

    public void StopBGM()
    {
        Debug.Log("StopBGM");

        if (m_BGMAudioSource.isPlaying == true)
        {
            m_BGMAudioSource.Stop();
        }
    }

    public void PauseBGM()
    {
        Debug.Log("StopBGM");

        if (m_BGMAudioSource.isPlaying == true)
        {
            m_BGMAudioSource.Pause();
        }
    }

    public void UnPauseBGM()
    {
        Debug.Log("StopBGM");

        if (m_BGMAudioSource.isPlaying == false)
        {
            m_BGMAudioSource.UnPause();
        }
    }

    public void PlaySFX(string audioClipName)
    {
        //Debug.Log("PlaySFX = " + audioClipName);
        if (m_AudioClipList.ContainsKey(audioClipName) == true)
        {
            m_FXAudioSource.PlayOneShot(m_AudioClipList[audioClipName]);
        }        
    }

    public void PlayLoopSFX(string audioClipName)
    {
        Debug.Log("PlayLoopSFX = " + audioClipName);
        if (m_AudioClipList.ContainsKey(audioClipName) == true &&
            m_LoopSFXAudioSource.ContainsKey(audioClipName) == false)
        {
            AudioSource loopFXAudioSource = gameObject.AddComponent<AudioSource>();
            loopFXAudioSource.playOnAwake = false;
            loopFXAudioSource.loop = true;
            // 반복 효과음 같은 경우 동적으로 생성하기 때문에 그때 그때 볼륨을 맞춰준다.
            loopFXAudioSource.volume = m_FXAudioSource.volume;

            loopFXAudioSource.clip = m_AudioClipList[audioClipName];
            loopFXAudioSource.Play();

            m_LoopSFXAudioSource.Add(audioClipName, loopFXAudioSource);
        }
    }

    public void StopLoopSFX(string audioClipName)
    {
        Debug.Log("StopLoopSFX = " + audioClipName);
        if (m_AudioClipList.ContainsKey(audioClipName) == true &&
            m_LoopSFXAudioSource.ContainsKey(audioClipName) == true)
        {
            AudioSource loopFXAudioSource = m_LoopSFXAudioSource[audioClipName];
            loopFXAudioSource.Stop();
            m_LoopSFXAudioSource.Remove(audioClipName);
            Destroy(loopFXAudioSource);
        }
    }

    public void AllStopLoopSFX()
    {
        Dictionary<string, AudioSource>.Enumerator enumerator = m_LoopSFXAudioSource.GetEnumerator();
        while (enumerator.MoveNext())
        {
            AudioSource loopFXAudioSource = enumerator.Current.Value;
            loopFXAudioSource.Stop();
            Destroy(loopFXAudioSource);
        }

        m_LoopSFXAudioSource.Clear();
    }

    public static void PlayButtonClickSound()
    {
        SoundManager.instance.PlaySFX("BT");        
    }

    public static void PlayShowPopupSound()
    {
        //SoundManager.instance.PlaySFX("POPUP");
    }
}

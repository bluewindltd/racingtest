﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class PointData
{
    public enum Type
    {
        None = -1,
        Police = 0,
        SWAT = 1,
        Hummer = 2,
        Tank = 3,
        Gangster = 4,
        NPC = 5,
        Building = 6,
        Obstacle = 7,
        Tree = 8,
    }

    public Type type = Type.None;
    public int colEvilPoint = 0;
    public int destroyEvilPoint = 0;
    public int colScore = 0;
    public int destroyScore = 0;
}

public class PointDocs : Singleton<PointDocs>
{
    Dictionary<int, PointData> m_PointDataDic = new Dictionary<int, PointData>();
    public Dictionary<int, PointData> pointDataDic
    {
        get
        {
            return m_PointDataDic;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_PointDataDic.Count <= 0)
        {
            Dictionary<string, object> allDataByPointTableSchema;
            GDEDataManager.GetAllDataBySchema("PointTable", out allDataByPointTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByPointTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> pointTableDic = etorTable.Current.Value as Dictionary<string, object>;

                PointData pointData = new PointData();

                int type = 0;
                pointTableDic.TryGetInt("type", out type);
                pointData.type = (PointData.Type)type;
                pointTableDic.TryGetInt("colEvilPoint", out pointData.colEvilPoint);
                pointTableDic.TryGetInt("destroyEvilPoint", out pointData.destroyEvilPoint);
                pointTableDic.TryGetInt("colScore", out pointData.colScore);
                pointTableDic.TryGetInt("destroyScore", out pointData.destroyScore);

                m_PointDataDic.Add((int)pointData.type, pointData);
            }
        }
    }
}
﻿using UnityEngine;
using System.Collections;

public class CommonPopup : BasePopup
{
    public UILabel titleLabel;
    public UILabel textLabel;

    public UILabel buttonOKLabel;    
    public UILabel buttonCancelLabel;

    public delegate void OKDelegate();
    public OKDelegate OnOK = delegate () { };

    public delegate void YesDelegate();
    public YesDelegate OnYes = delegate () { };

    public delegate void NoDelegate();
    public NoDelegate OnNo = delegate () { };

    void Start()
    {
        if (buttonOKLabel)
        {
            buttonOKLabel.text = TextDocs.content("BT_OK_TEXT");
        }

        if (buttonCancelLabel)
        {
            buttonCancelLabel.text = TextDocs.content("BT_CANCEL_TEXT");
        }
    }

    public static CommonPopup Show(GameObject parent, GameObject prefab, string title, string text, OKDelegate onOK, YesDelegate onYes, NoDelegate onNo, CancelDelegate onCancel)
    {
        //SoundManager.PlayShowPopupSound();

        CommonPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<CommonPopup>();

        popup.titleLabel.text = title;
        popup.textLabel.text = text;

        popup.OnOK = onOK;
        popup.OnYes = onYes;
        popup.OnNo = onNo;
        popup.OnCancel = onCancel;

        return popup;
    }

    public void OnClickOK()
    {
        SoundManager.PlayButtonClickSound();

        if (OnOK != null)
        {
            OnOK();
        }

        Hide();
    }

    public void OnClickYes()
    {
        SoundManager.PlayButtonClickSound();

        if (OnYes != null)
        {
            OnYes();
        }

        Hide();
    }

    public void OnClickNo()
    {
        SoundManager.PlayButtonClickSound();

        if (OnNo != null)
        {
            OnNo();
        }

        Hide();
    }
}

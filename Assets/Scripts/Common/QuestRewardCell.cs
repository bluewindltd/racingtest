﻿using UnityEngine;
using System.Collections;

public class QuestRewardCell : MonoBehaviour
{
    public UISprite bgSprite;
    public UILabel levelLabel;
    public UILabel rewardLabel;

    public void Initialize(QuestFriendshipData questFriendshipData)
    {
        if(questFriendshipData.specialReward)
        {
            bgSprite.color = new Color(0.7f, 0.04f, 0.04f);
            Color textColor = new Color(0.94f, 0.91f, 0.835f);
            levelLabel.color = textColor;
            rewardLabel.color = textColor;
        }
        levelLabel.text = string.Format("Lv.{0}", questFriendshipData.level);
        rewardLabel.text = GameConfig.GetRewardName(questFriendshipData.levelUpRewardType, questFriendshipData.levelUpRewardValue);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class VehicleData
{
    public enum Grade
    {
        C = 0,
        B = 1,
        A = 2,
        S = 3,
    }

#if BWQUEST
    public enum EarnType
    {
        Purchase = 0,
        QuestReward = 1,
    }
#endif

    public string name = "";
    public string desc = "";
    public int index = 0;
#if BWQUEST
    public EarnType earnType = EarnType.Purchase;
#endif
    public int order = 0;
    public Grade grade = Grade.C;
    public GameConfig.TokenType tokenType = GameConfig.TokenType.C;
    public int tokenCount = 0;
    public string googleID = "";
    public string appleID = "";
    public string recordPrice = "";
    public string tempPrice = "";
    public string stockPrice = "";
    public int health = 0;
    public int damage = 0;
    public int attackDamage = 0;
    public float attackDelay = 0;
    public float rotorSpeed = 0;
    public float shellSpeed = 0;
    public float explosionRadius = 0;
}

public class VehicleDocs : Singleton<VehicleDocs>
{
    private Dictionary<int, VehicleData> m_VehicleDataDic = new Dictionary<int, VehicleData>();
    public Dictionary<int, VehicleData> vehicleDataDic
    {
        get
        {
            return m_VehicleDataDic;
        }
    }

    Dictionary<VehicleData.Grade, List<VehicleData>> m_VehicleGradeDic = new Dictionary<VehicleData.Grade, List<VehicleData>>();
    public Dictionary<VehicleData.Grade, List<VehicleData>> vehicleGradeDic
    {
        get
        {
            return m_VehicleGradeDic;
        }
    }

    private bool m_IsUpdateProduct = false;
    public bool isUpdateProduct
    {
        get
        {
            return m_IsUpdateProduct;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_VehicleDataDic.Count <= 0)
        {
            int earnType = 0;
            int grade = 0;
            int tokenType = 0;

            m_VehicleGradeDic.Add(VehicleData.Grade.A, new List<VehicleData>());
            m_VehicleGradeDic.Add(VehicleData.Grade.B, new List<VehicleData>());
            m_VehicleGradeDic.Add(VehicleData.Grade.C, new List<VehicleData>());
            m_VehicleGradeDic.Add(VehicleData.Grade.S, new List<VehicleData>());

            Dictionary<string, object> allDataByVehicleTableSchema;
            GDEDataManager.GetAllDataBySchema("VehicleTable", out allDataByVehicleTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByVehicleTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> vehicleTableDic = etorTable.Current.Value as Dictionary<string, object>;

                VehicleData vehicleData = new VehicleData();

                vehicleTableDic.TryGetString("name", out vehicleData.name);
                vehicleTableDic.TryGetString("desc", out vehicleData.desc);
                vehicleTableDic.TryGetInt("index", out vehicleData.index);
#if BWQUEST
                vehicleTableDic.TryGetInt("earnType", out earnType);
                vehicleData.earnType = (VehicleData.EarnType)earnType;
#endif
                vehicleTableDic.TryGetInt("order", out vehicleData.order);
                vehicleTableDic.TryGetInt("grade", out grade);
                vehicleData.grade = (VehicleData.Grade)grade;
                vehicleTableDic.TryGetInt("tokenType", out tokenType);
                vehicleData.tokenType = (GameConfig.TokenType)tokenType;
                vehicleTableDic.TryGetInt("tokenCount", out vehicleData.tokenCount);
                vehicleTableDic.TryGetString("googleID", out vehicleData.googleID);
                vehicleTableDic.TryGetString("appleID", out vehicleData.appleID);
                vehicleTableDic.TryGetString("recordPrice", out vehicleData.recordPrice);
                vehicleTableDic.TryGetString("tempPrice", out vehicleData.tempPrice);
                vehicleTableDic.TryGetInt("health", out vehicleData.health);
                vehicleTableDic.TryGetInt("damage", out vehicleData.damage);
                vehicleTableDic.TryGetInt("attackDamage", out vehicleData.attackDamage);
                vehicleTableDic.TryGetFloat("attackDelay", out vehicleData.attackDelay);
                vehicleTableDic.TryGetFloat("rotorSpeed", out vehicleData.rotorSpeed);
                vehicleTableDic.TryGetFloat("shellSpeed", out vehicleData.shellSpeed);
                vehicleTableDic.TryGetFloat("explosionRadius", out vehicleData.explosionRadius);

                if (vehicleData.googleID == null)
                {
                    vehicleData.googleID = "";
                }
                if (vehicleData.appleID == null)
                {
                    vehicleData.appleID = "";
                }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
                if (vehicleData.googleID.CompareTo("") != 0)
                {
                    Debug.Log("@@ AddProduct : " + vehicleData.googleID);
                    AndroidInAppPurchaseManager.Client.AddProduct(vehicleData.googleID);
                }
#elif UNITY_IOS
                if (vehicleData.appleID.CompareTo("") != 0)
                {
                    Debug.Log("@@ AddProduct : " + vehicleData.appleID);
                    IOSInAppPurchaseManager.Instance.AddProductId(vehicleData.appleID);
                }
#endif

                m_VehicleDataDic.Add(vehicleData.index, vehicleData);

                m_VehicleGradeDic[vehicleData.grade].Add(vehicleData);
            }

            Dictionary<VehicleData.Grade, List<VehicleData>>.Enumerator etor = m_VehicleGradeDic.GetEnumerator();
            while(etor.MoveNext())
            {
                etor.Current.Value.Sort((a, b) => a.order - b.order);
            }
        }
    }

    public VehicleData GetVehicleForGoogle(string googleID)
    {
        VehicleData vehicleData = null;
        Dictionary<int, VehicleData>.Enumerator etor = m_VehicleDataDic.GetEnumerator();

        while(etor.MoveNext())
        {
            if(etor.Current.Value.googleID.CompareTo(googleID) == 0)
            {
                vehicleData = etor.Current.Value;
            }
        }

        return vehicleData;
    }

    public VehicleData GetVehicleForApple(string appleID)
    {
        VehicleData vehicleData = null;
        Dictionary<int, VehicleData>.Enumerator etor = m_VehicleDataDic.GetEnumerator();

        while (etor.MoveNext())
        {
            if (etor.Current.Value.appleID.CompareTo(appleID) == 0)
            {
                vehicleData = etor.Current.Value;
            }
        }

        return vehicleData;
    }

    public void UpdateProduct()
    {
        if (m_IsUpdateProduct == false)
        {
            m_IsUpdateProduct = true;
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
            VehicleData vehicleData = null;
            Dictionary<int, VehicleData>.Enumerator etor = m_VehicleDataDic.GetEnumerator();
            while(etor.MoveNext())
            {
                vehicleData = etor.Current.Value;
                if(vehicleData != null)
                {
                    GoogleProductTemplate product = AndroidInAppPurchaseManager.Client.Inventory.GetProductDetails(vehicleData.googleID);
                    if (product != null)
                    {
                        //Debug.Log("@@ GoogleProduct : " + product.SKU + ", " + product.Title + ", " + product.Description + ", " + product.LocalizedPrice + ", " + product.Price + ", " + product.PriceCurrencyCode);
                        vehicleData.stockPrice = product.LocalizedPrice;
                    }
                }
            }
#elif UNITY_IOS
			int testCount = 0;
			VehicleData vehicleData = null;
			Dictionary<int, VehicleData>.Enumerator etor = m_VehicleDataDic.GetEnumerator();
			while(etor.MoveNext())
			{
				vehicleData = etor.Current.Value;
				if(vehicleData != null)
				{
					IOSProductTemplate product = IOSInAppPurchaseManager.Instance.GetProductById(vehicleData.appleID);					
					if(product != null)
					{
						Debug.Log("@@ IOSProductTemplate " + testCount + " Success : " + vehicleData.appleID + ", " + product.DisplayName + ", " + product.LocalizedPrice + ", " + product.Id);
						vehicleData.stockPrice = product.LocalizedPrice;
					}
					else
					{
						Debug.Log("@@ IOSProductTemplate " + testCount + " Failed");
					}
				}

				testCount++;
			}
#endif
        }
    }
}

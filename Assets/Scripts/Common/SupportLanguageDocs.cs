﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
//using esLibs.Detector.Type;
//using DocsPin;

public class SupportLanguageData
{
    public string srcLang = "";
    public int langIDX = 0;
    public string supportLang = "";
}

public class SupportLanguageDocs : Singleton<SupportLanguageDocs>
{
    // Data Keys.
    private const string SRC_LANG = "srcLang";
    private const string LANG_IDX = "langIDX";
    private const string SUPPORT_LANG = "supportLang";

    private ArrayList _rowKeyList = null;
    public ArrayList rowKeyList
    {
        get
        {
            return _rowKeyList;
        }
    }

    Dictionary<int, SupportLanguageData> _supportLanguageDic = new Dictionary<int, SupportLanguageData>();
    public Dictionary<int, SupportLanguageData> supportLanguageDic
    {
        get
        {
            return _supportLanguageDic;
        }
    }

    #region Data Interface
    // Getting SourceLanguageName.
    public static string srcLanguage(int key)
    {
        return SupportLanguageDocs.instance.getSrcLanguage(key);
    }
    public string getSrcLanguage(int key)
    {
        if (_supportLanguageDic.ContainsKey(key))
        {
            return _supportLanguageDic[key].srcLang;
        }
        else
        {
            return "";
        }
        //if (string.IsNullOrEmpty(key) == true)
        //    return "";
        //return this.get<string>(key, SRC_LANG);
    }

    // Getting LanguageIndex.
    public static int languageIndex(int key)
    {
        return SupportLanguageDocs.instance.getLanguageIndex(key);
    }
    public int getLanguageIndex(int key)
    {
        if (_supportLanguageDic.ContainsKey(key))
        {
            return _supportLanguageDic[key].langIDX;
        }
        else
        {
            return 0;
        }
        //if (string.IsNullOrEmpty(key) == true)
        //    return 0;
        //return this.get<int>(key, LANG_IDX);
    }

    // Getting SupportLanguageName.
    public static string supportLanguage(int key)
    {
        return SupportLanguageDocs.instance.getSupportLanguage(key);
    }
    public string getSupportLanguage(int key)
    {
        if (_supportLanguageDic.ContainsKey(key))
        {
            return _supportLanguageDic[key].supportLang;
        }
        else
        {
            return "";
        }
        //if (string.IsNullOrEmpty(key) == true)
        //    return "";
        //return this.get<string>(key, SUPPORT_LANG);
    }
    #endregion

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (_supportLanguageDic.Count <= 0)
        {
            Dictionary<string, object> allDataBySupportLanguageTableSchema;
            GDEDataManager.GetAllDataBySchema("SupportLanguageTable", out allDataBySupportLanguageTableSchema);
            Dictionary<string, object>.Enumerator etor = allDataBySupportLanguageTableSchema.GetEnumerator();
            int sysIDX = 0;
            while (etor.MoveNext())
            {
                Dictionary<string, object> supportLanguageTableDic = etor.Current.Value as Dictionary<string, object>;

                SupportLanguageData supportLanguageData = new SupportLanguageData();

                supportLanguageTableDic.TryGetInt("sysIDX", out sysIDX);
                supportLanguageTableDic.TryGetString("srcLang", out supportLanguageData.srcLang);
                supportLanguageTableDic.TryGetInt("langIDX", out supportLanguageData.langIDX);
                supportLanguageTableDic.TryGetString("supportLang", out supportLanguageData.supportLang);

                _supportLanguageDic.Add(sysIDX, supportLanguageData);
            }
        }
    }

    //public void OnLoadSuccess(DocsData docsData)
    //{
    //    if (_rowKeyList == null)
    //    {
    //        _rowKeyList = getRowKeyList();
    //    }
    //}
}
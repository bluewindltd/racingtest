﻿using UnityEngine;
using System.Collections;

public class ExitPopup : CommonPopup
{
    float m_PrevSFXVolume = 0.0f;

    public OKDelegate OnOK_Stop = delegate () { };
    public NoDelegate OnNo_Stop = delegate () { };

    public static ExitPopup Show(GameObject parent, GameObject prefab, bool isStopGame, CommonPopup.OKDelegate onOK_Stop, CommonPopup.NoDelegate onNo_Stop)
    {   
        IGAWorksManager.instance.EndSession();

        ExitPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<ExitPopup>();

        popup.titleLabel.text = TextDocs.content("POPUP_PLAYEXIT_TITLE");
        popup.textLabel.text = TextDocs.content("POPUP_EXIT_TEXT");

        popup.OnOK_Stop = onOK_Stop;
        popup.OnNo_Stop = onNo_Stop;

        if (isStopGame)
        {
            if(popup.OnOK_Stop != null)
            {
                popup.OnOK_Stop();
            }

            popup.m_PrevSFXVolume = SoundManager.instance.GetSFXVolume();
            SoundManager.instance.SetSFXVolume(0.0f);
            SoundManager.instance.PauseBGM();
            Time.timeScale = 0.0f;            
        }

        popup.OnOK = delegate ()
        {
            //int currentHour = System.DateTime.Now.Hour;
            //int lastDayHour = 24 - currentHour - 5;
            //if (lastDayHour <= 0)
            //{
            //    lastDayHour += 19;  // 항상 저녁 7시에 푸시가 도착하도록 세팅
            //}
            //int pushHour = 48 + lastDayHour;
            //int pushSecond = pushHour * 60 * 60;
            //Debug.Log("ExitPopup : ScheduleLocalNotification! : " + lastDayHour + ", " + pushHour + ", " + pushSecond);

            int pushSecond = EtcDocs.instance.freeGiftTime * 60;
            if (TimeManager.instance.GetTimeState())
            {
                long time = TimeManager.instance.GetOnlineTime();
                int tempPushSecond = (int)((float)(UserManager.instance.freeGiftTime - time) / 1000.0f);
                // 무료 선물 받기까지 10분도 안남은 경우는 10분으로 고정 시킨다.
                if (tempPushSecond <= 600)
                {
                    pushSecond = 600;
                }
                else
                {
                    pushSecond = tempPushSecond;
                }
            }
            Debug.Log("ExitPopup : ScheduleLocalNotification! : " + pushSecond);
            //IGAWorksManager.instance.SetNormalClientPushEvent(pushSecond, TextDocs.content("PUSH_TEXT"), 77, false);

            //AndroidNotificationManager.Instance.ScheduleLocalNotification(TextDocs.content("PUSH_TITLE"), TextDocs.content("PUSH_TEXT"), pushSecond);

            Debug.Log("ExitPopup OK!");
            Application.Quit();
        };

        popup.OnNo = delegate ()
        {
            Debug.Log("ExitPopup No!");

            if (isStopGame)
            {
                if (popup.OnNo_Stop != null)
                {
                    popup.OnNo_Stop();
                }

                Time.timeScale = 1.0f;
                SoundManager.instance.SetSFXVolume(popup.m_PrevSFXVolume);
                SoundManager.instance.UnPauseBGM();
            }

            IGAWorksManager.instance.StartSession();
        };

        popup.OnCancel = delegate ()
        {
            Debug.Log("ExitPopup Canel!");

            if (isStopGame)
            {
                if (popup.OnNo_Stop != null)
                {
                    popup.OnNo_Stop();
                }

                Time.timeScale = 1.0f;
                SoundManager.instance.SetSFXVolume(popup.m_PrevSFXVolume);
                SoundManager.instance.UnPauseBGM();
            }

            IGAWorksManager.instance.StartSession();
        };

        return popup;
    }
}

﻿using UnityEngine;
using System.Collections;

public class AchievementCell : MonoBehaviour
{
    public GameObject[] onOffObject;

    public UISprite[] onOffSprite;
    public UILabel[] onOffNameLabel;
    public UILabel[] onOffProgressLabel;

    private AchievementData m_AchievementData = null;

    public void SetAchievement(AchievementData acData)
    {
        m_AchievementData = acData;

        NGUITools.SetActive(onOffObject[0], false);
        NGUITools.SetActive(onOffObject[1], false);

        onOffSprite[0].spriteName = m_AchievementData.iconName;
        onOffSprite[1].spriteName = m_AchievementData.iconName;

        onOffNameLabel[0].text = TextDocs.content(m_AchievementData.acName);
        onOffNameLabel[1].text = TextDocs.content(m_AchievementData.acName);

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
//#elif UNITY_ANDROID
#if UNITY_ANDROID
        if (acData.state == GPAchievementState.STATE_UNLOCKED)
        {
            NGUITools.SetActive(onOffObject[0], true);
        }
        else if (acData.state == GPAchievementState.STATE_REVEALED)
        {
            NGUITools.SetActive(onOffObject[1], true);
        }
#elif UNITY_IOS
		if(acData.progress >= 100.0f)
		{
			NGUITools.SetActive(onOffObject[0], true);
		}
		else if(acData.progress < 100.0f)
		{
			NGUITools.SetActive(onOffObject[1], true);
		}			
#endif

        if(m_AchievementData.acKind == AchievementData.Kind.SCORE)
        {
            onOffProgressLabel[0].text = TextDocs.content("UI_SCORE_TEXT") + " " + m_AchievementData.acGoal;
            onOffProgressLabel[1].text = TextDocs.content("UI_SCORE_TEXT") + " " + m_AchievementData.acGoal;
        }
    }
}

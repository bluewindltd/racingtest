﻿using UnityEngine;
using System.Collections;

public class QuestInfoPopup : BasePopup
{
    public UILabel titleLabel;
    public UILabel nameLabel;
    public UISprite profileSprite;
    public UILabel levelExpLabel;
    public UISprite expGaugeSprite;
    public UILabel questContentTitleLabel;
    public UILabel questContentLabel;
    public UILabel timeTitleLabel;
    public UILabel timeLabel;
    public UILabel questDescLabel;
    public UILabel okButtonLabel;

    QuestClientInfo m_QuestClientInfo = null;

    public static QuestInfoPopup Show(GameObject parent, GameObject prefab)
    {
        QuestInfoPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<QuestInfoPopup>();

        popup.titleLabel.text = TextDocs.content("UI_QUEST_CHECK");
        popup.questContentTitleLabel.text = TextDocs.content("UI_QUEST_DESC");
        popup.timeTitleLabel.text = TextDocs.content("UI_QUEST_TIMELIMIT");
        popup.okButtonLabel.text = TextDocs.content("BT_OK_TEXT");

        return popup;
    }

    public void Initialize(QuestClientInfo questClientInfo)
    {
        m_QuestClientInfo = questClientInfo;
        QuestClientData questClientData = m_QuestClientInfo.questClientData;
        QuestFriendshipData questFriendshipData = null;
        int curExp = 0;
        int endExp = 0;

        profileSprite.spriteName = questClientData.imageName;
        nameLabel.text = TextDocs.content(questClientData.name);

        if (questClientData.lastFriendshipLevel <= m_QuestClientInfo.friendshipLevel)
        {
            if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel - 1))
            {
                questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel - 1];
                curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
                    m_QuestClientInfo.friendshipLevel, curExp, endExp);
                expGaugeSprite.fillAmount = (float)((float)curExp / (float)endExp);
            }
        }
        else
        {
            if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel))
            {
                questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel];
                curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
                    m_QuestClientInfo.friendshipLevel, curExp, endExp);
                expGaugeSprite.fillAmount = (float)((float)curExp / (float)endExp);
            }
        }

        //if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel))
        //{
        //    questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel];
        //    curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
        //    endExp = questFriendshipData.endExp - questFriendshipData.startExp;
        //    levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
        //        m_QuestClientInfo.friendshipLevel, curExp, endExp);
        //    expGaugeSprite.fillAmount = Mathf.Clamp01((float)curExp / (float)endExp);
        //}

        QuestData questData = QuestManager.instance.curQuestData;
        int questIndex = QuestManager.instance.questProgressInfo.curQuestIndex;
        if (questData != null)
        {
            string questContent = "{0}";
            string questDesc = "";
            if(questIndex == 0)
            {
                questContent = questClientData.questContent1;
                questDesc = questClientData.questDesc1;
            }
            else if(questIndex == 1)
            {
                questContent = questClientData.questContent2;
                questDesc = questClientData.questDesc2;
            }
            //FFFFFFB1
            string contentString = string.Format(TextDocs.content(questContent), questData.param) + "\\n" +
                string.Format("[FFFFFFB1]({0}/{1})[-]", QuestManager.instance.questProgressInfo.progressParam, questData.param);
            contentString = contentString.Replace("\\n", "\n");
            questContentLabel.text = contentString;
            questDescLabel.text = TextDocs.content(questDesc);

            timeLabel.text = StringUtil.ConvertStringElapseTime(questData.time);
        }
        else
        {
            questContentLabel.text = "";
            questDescLabel.text = "";
        }
    }

    public void OnClickOk()
    {
        SoundManager.PlayButtonClickSound();

        Hide();
    }
}

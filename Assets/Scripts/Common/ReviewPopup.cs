﻿using UnityEngine;
using System.Collections;

public class ReviewPopup : CommonPopup
{
    public static ReviewPopup Show(GameObject parent, GameObject prefab)
    {
        SoundManager.PlayShowPopupSound();

        ReviewPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<ReviewPopup>();

        popup.titleLabel.text = TextDocs.content("POPUP_RIVIEW_TITLE");
        popup.textLabel.text = TextDocs.content("POPUP_RIVIEW_TEXT");

        popup.OnYes = delegate ()
        {
            PlayerPrefs.SetInt("AcceptReviewPopup", 1);
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
            AndroidNativeUtility.OpenAppRatingPage("market://details?id=com.daerisoft.burnout");
#elif UNITY_IOS
            IOSNativeUtility.RedirectToAppStoreRatingPage();
#endif
        };

        return popup;
    }
}

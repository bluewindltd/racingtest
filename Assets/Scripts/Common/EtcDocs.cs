﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using CodeStage.AntiCheat.ObscuredTypes;

public class EtcDocs : Singleton<EtcDocs>
{
    bool m_Initialize = false;

    ObscuredInt m_PlayGiftCount = 0;
    public ObscuredInt playGiftCount
    {
        get
        {
            return m_PlayGiftCount;
        }
    }

    int m_FreeGiftTime = 0;
    public int freeGiftTime
    {
        get
        {
            return m_FreeGiftTime;
        }
    }

    ObscuredInt m_GachaCash = 0;
    public ObscuredInt gachaCash
    {
        get
        {
            return m_GachaCash;
        }
    }

    ObscuredFloat m_ArrestTime = 0;
    public ObscuredFloat arrestTime
    {
        get
        {
            return m_ArrestTime;
        }
    }

    ObscuredFloat m_EscapeArrestDistance = 0;
    public ObscuredFloat escapeArrestDistance
    {
        get
        {
            return m_EscapeArrestDistance;
        }
    }

    ObscuredInt m_FirstAdRewardCash = 0;
    public ObscuredInt firstAdRewardCash
    {
        get
        {
            return m_FirstAdRewardCash;
        }
    }

    ObscuredInt m_IdleAdRewardCash = 0;
    public ObscuredInt idleAdRewardCash
    {
        get
        {
            return m_IdleAdRewardCash;
        }
    }

    ObscuredInt m_ItemRecoveryHP = 0;
    public ObscuredInt itemRecoveryHP
    {
        get
        {
            return m_ItemRecoveryHP;
        }
    }

    ObscuredInt m_QuestRetryCash = 0;
    public ObscuredInt questRetryCash
    {
        get
        {
            return m_QuestRetryCash;
        }
    }

    ObscuredInt m_PetExperienceCount = 0;
    public ObscuredInt petExperienceCount
    {
        get
        {
            return m_PetExperienceCount;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (!m_Initialize)
        {
            m_Initialize = true;
            Dictionary<string, object> allDataByEtcTableSchema;
            GDEDataManager.GetAllDataBySchema("EtcTable", out allDataByEtcTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByEtcTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> etcTableDic = etorTable.Current.Value as Dictionary<string, object>;

                int temp = 0;
                float tempFloat = 0.0f;
                etcTableDic.TryGetInt("PlayGift_Count", out temp);
                m_PlayGiftCount = temp;
                etcTableDic.TryGetInt("FreeGift_Time", out m_FreeGiftTime);
                etcTableDic.TryGetInt("GachaCash", out temp);
                m_GachaCash = temp;
                etcTableDic.TryGetFloat("ArrestTime", out tempFloat);
                m_ArrestTime = tempFloat;
                etcTableDic.TryGetFloat("EscapeArrestDistance", out tempFloat);
                m_EscapeArrestDistance = tempFloat;
                etcTableDic.TryGetInt("First_Ad_RewardCash", out temp);
                m_FirstAdRewardCash = temp;
                etcTableDic.TryGetInt("Idle_Ad_RewardCash", out temp);
                m_IdleAdRewardCash = temp;
                etcTableDic.TryGetInt("Item_RecoveryHP", out temp);
                m_ItemRecoveryHP = temp;
                etcTableDic.TryGetInt("Quest_RetryCash", out temp);
                m_QuestRetryCash = temp;
                etcTableDic.TryGetInt("Pet_ExperienceCount", out temp);
                m_PetExperienceCount = temp;
            }
        }
    }
}
﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class AchievementData : MonoBehaviour
{
    public enum Kind
    {
        SCORE = 0,
    }

    public string acID = "";
    public string acName = "";
    public GPAchievementType acType = GPAchievementType.TYPE_STANDARD;
    public Kind acKind = Kind.SCORE;
    public int acOrder = 0;
    public int acGoal = 0;
    public int acScore = 0;
    public string iconName = "";
    public string googleID = "";
    public string appleID = "";
    public GPAchievementState state = GPAchievementState.STATE_REVEALED;
    public int currentStep = -1;
    public int totalStep = -1;
	public float progress = 0.0f;	// ios
}

public class AchievementDocs : Singleton<AchievementDocs>
{
    public delegate void UpdateAchievementDelegate();
    public static UpdateAchievementDelegate OnUpdateAchievement = delegate () { };

    private List<AchievementData> m_AchievementDataList = new List<AchievementData>();
    public List<AchievementData> acDataList
    {
        get
        {
            return m_AchievementDataList;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_AchievementDataList.Count <= 0)
        {
            Dictionary<string, object> allDataByAchievementTableSchema;
            GDEDataManager.GetAllDataBySchema("AchievementTable", out allDataByAchievementTableSchema);
            Dictionary<string, object>.Enumerator etor = allDataByAchievementTableSchema.GetEnumerator();
            while (etor.MoveNext())
            {
                Dictionary<string, object> achievementTableDic = etor.Current.Value as Dictionary<string, object>;

                AchievementData acData = new AchievementData();

                achievementTableDic.TryGetString("acID", out acData.acID);
                achievementTableDic.TryGetString("acName", out acData.acName);
                int acType = 0;
                achievementTableDic.TryGetInt("acType", out acType);
                acData.acType = (GPAchievementType)acType;
                int acKind = 0;
                achievementTableDic.TryGetInt("acKind", out acKind);
                acData.acKind = (AchievementData.Kind)acKind;
                achievementTableDic.TryGetInt("acOrder", out acData.acOrder);
                achievementTableDic.TryGetInt("acGoal", out acData.acGoal);
                achievementTableDic.TryGetInt("acScore", out acData.acScore);
                achievementTableDic.TryGetString("iconName", out acData.iconName);
                achievementTableDic.TryGetString("googleID", out acData.googleID);
                achievementTableDic.TryGetString("appleID", out acData.appleID);

                m_AchievementDataList.Add(acData);
            }

            if (m_AchievementDataList.Count > 0)
            {
                m_AchievementDataList.Sort((a, b) => a.acOrder - b.acOrder);
            }
        }
    }

    public void UpdateAchievement()
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
		GooglePlayManager.ActionAchievementsLoaded += OnAchievmentsLoaded;

        GooglePlayManager.Instance.LoadAchievements();
#elif UNITY_IOS
		Debug.Log("@@ UpdateAchievement 0");
		GameCenterManager.OnAchievementsLoaded += OnAchievmentsLoaded;

		GameCenterManager.LoadAchievements();
#endif
    }

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
    private void OnAchievmentsLoaded(GooglePlayResult result)
    {
        GooglePlayManager.ActionAchievementsLoaded -= OnAchievmentsLoaded;

        if (result.IsSucceeded)
        {
            Debug.Log("@@ UpdateAchievement : " + m_AchievementDataList.Count);
            for (int i = 0; i < m_AchievementDataList.Count; i++)
            {
                AchievementData acData = m_AchievementDataList[i];

                GPAchievement achievement = GooglePlayManager.Instance.GetAchievement(acData.googleID);
                if (achievement != null)
                {
                    Debug.Log("@@ UpdateAchievement : " + acData.googleID + ", " + achievement.type + ", " + achievement.state + ", " + achievement.currentSteps + ", " + achievement.totalSteps);
                    acData.acType = achievement.Type;
                    acData.state = achievement.State;
                    acData.currentStep = achievement.CurrentSteps;
                    acData.totalStep = achievement.TotalSteps;
                }
            }
        }
        else
        {
            Debug.Log("Achievements Loaded error: " + result.message);
        }

        if(OnUpdateAchievement != null)
        {
            OnUpdateAchievement();
        }
    }
#elif UNITY_IOS
	private void OnAchievmentsLoaded(ISN_Result result) {
		GameCenterManager.OnAchievementsLoaded -= OnAchievmentsLoaded;

		if(result.IsSucceeded)
		{
			Debug.Log("@@ UpdateAchievement : " + GameCenterManager.Achievements.Count);

			for (int i = 0; i < m_AchievementDataList.Count; i++)
			{
				AchievementData acData = m_AchievementDataList[i];

				GK_AchievementTemplate achievement = GameCenterManager.GetAchievement (acData.appleID);
				if (achievement != null)
				{
					Debug.Log("@@ UpdateAchievement : " + acData.appleID + ", " + achievement.Id + ", " + achievement.Title + ", " + achievement.Progress + ", " + achievement.IsOpen + ", " + achievement.GetType());
					acData.progress = achievement.Progress;
				}
			}
		}
		else
		{
			Debug.Log("@@ UpdateAchievement Loaded error: " + result.Error.Code + " , " + result.Error.Description);
		}

		if(OnUpdateAchievement != null)
		{
			OnUpdateAchievement();
		}
	}
#endif

    public void CheckAchievement(AchievementData.Kind kind, int srcGoal)
    {
        Debug.Log("CheckAchievement : " + kind + ", " + srcGoal);

        for (int i = 0; i < m_AchievementDataList.Count; i++)
        {
            AchievementData acData = m_AchievementDataList[i];

            if (acData.acKind == kind)
            {
                if (acData.acGoal <= srcGoal)
                {
                    Debug.Log("CheckAchievement UnlockAchievementById : " + acData.googleID + ", " + acData.appleID + ", " + acData.acName);
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
                    GooglePlayManager.Instance.UnlockAchievementById(acData.googleID);
#elif UNITY_IOS
					GameCenterManager.SubmitAchievement(100.0f, acData.appleID, true);
#endif
                }
            }
        }
    }
}

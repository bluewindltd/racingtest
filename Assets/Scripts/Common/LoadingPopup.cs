﻿using UnityEngine;
using System.Collections;

public class LoadingPopup : MonoBehaviour
{
    public UILabel loadingLabel;
    public GameObject[] logoObjectArray;

    void Start()
    {
        loadingLabel.text = TextDocs.content("LOADING_TEXT");

        for(int i = 0; i < logoObjectArray.Length; i++)
        {
            NGUITools.SetActive(logoObjectArray[i], false);
        }
        GameConfig.SupportLanguage selectLanguage = UserManager.instance.selectLanguage;
        if(selectLanguage == GameConfig.SupportLanguage.Japanese)
        {
            NGUITools.SetActive(logoObjectArray[1], true);
        }
        else if (selectLanguage == GameConfig.SupportLanguage.Chinese_S)
        {
            NGUITools.SetActive(logoObjectArray[2], true);
        }
        else if (selectLanguage == GameConfig.SupportLanguage.Chinese_T)
        {
            NGUITools.SetActive(logoObjectArray[3], true);
        }
        else
        {
            NGUITools.SetActive(logoObjectArray[0], true);
        }
    }

    public void Show()
    {
        NGUITools.SetActive(gameObject, true);
    }

    public void Hide()
    {
        NGUITools.SetActive(gameObject, false);
    }

    public bool IsShow()
    {
        return NGUITools.GetActive(gameObject);
    }
}

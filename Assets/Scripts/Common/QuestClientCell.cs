﻿using UnityEngine;
using System.Collections;

public class QuestClientCell : MonoBehaviour
{
    public int clientIndex = 0;

    public UISprite[] profileSprites;
    public UILabel[] nameLabels;
    public UILabel[] levelExpLabels;
    public UISprite[] expGaugeSprites;
    public UILabel decsLabel;
    public UILabel questSelectButtonLabel;
    public UILabel questTimeLabel;
    public UILabel questSuccessButtonLabel;
    public UILabel questFailLabel;    

    public GameObject normalObject;
    public GameObject selectObject;
    public GameObject successObject;
    public GameObject failObject;

    public Animator normalAnimator;
    public Animator selectAnimator;
    public Animator successAnimator;
    public Animator failAnimator;

    public GameObject selectingObject;

    Coroutine m_QuestEndTimeCoroutine = null;

    QuestPopup m_QuestPopup = null;
    QuestClientInfo m_QuestClientInfo = null;

    QuestClientInfo.State m_State = QuestClientInfo.State.Normal;

    QuestManager m_QuestMgr;
    TimeManager m_TimeMgr;

    void Awake()
    {
        m_QuestMgr = QuestManager.instance;
        m_TimeMgr = TimeManager.instance;

        QuestManager.OnQuestReturn += OnQuestReturn;
    }

    void OnDestroy()
    {
        QuestManager.OnQuestReturn -= OnQuestReturn;

        if (m_QuestClientInfo.state == QuestClientInfo.State.Select)
        {
            QuestManager.OnQuestSuccess -= OnQuestSuccess;
            QuestManager.OnQuestFail -= OnQuestFail;
        }
    }

    public void Initialize(QuestPopup questPopup)
    {
        m_QuestPopup = questPopup;

        if (m_QuestClientInfo == null)
        {
            if (m_QuestMgr.questClientInfoDic.ContainsKey(clientIndex))
            {
                m_QuestClientInfo = m_QuestMgr.questClientInfoDic[clientIndex];
            }
        }

        if (m_QuestClientInfo != null)
        {
            QuestClientData questClientData = m_QuestClientInfo.questClientData;
            for (int i = 0; i < nameLabels.Length; i++)
            {
                profileSprites[i].spriteName = questClientData.imageName;
                nameLabels[i].text = TextDocs.content(questClientData.name);
            }

            RefreshLevelAndExp();

            decsLabel.text = TextDocs.content(questClientData.desc);
            questSelectButtonLabel.text = TextDocs.content("UI_QUEST_CHECK");
            questSuccessButtonLabel.text = TextDocs.content("UI_QUEST_REPORT");
            questFailLabel.text = TextDocs.content("UI_QUEST_FAIL");

            m_State = m_QuestClientInfo.state;

            switch (m_QuestClientInfo.state)
            {
                case QuestClientInfo.State.Normal:
                    {
                        NGUITools.SetActive(normalObject, true);
                    }
                    break;
                case QuestClientInfo.State.Select:
                    {
                        NGUITools.SetActive(selectObject, true);
                        SetSelectQuestData();
                        selectAnimator.Play("Quest_Cell_Selected_Show");
                    }
                    break;
                case QuestClientInfo.State.Success:
                    {
                        NGUITools.SetActive(successObject, true);
                        successAnimator.Play("Quest_Cell_Success_Idle");
                    }
                    break;
                case QuestClientInfo.State.Fail:
                    {
                        NGUITools.SetActive(failObject, true);
                        failAnimator.Play("Quest_Cell_Failed_Idle");
                    }
                    break;
            }
        }
    }

    public void SelectingOn()
    {
        NGUITools.SetActive(selectingObject, true);
    }

    public void SelectingOff()
    {
        NGUITools.SetActive(selectingObject, false);
    }

    public void SelectQuest()
    {
        if(m_QuestClientInfo.state == QuestClientInfo.State.Normal)
        {
            // 해당 클라이언트가 선택된 정보로 갱신한다.
            m_QuestMgr.SelectQuest(m_QuestClientInfo.questClientData.seasonIndex, clientIndex);

            normalAnimator.Play("Quest_Cell_Normal_Selected");
        }
    }

    IEnumerator UpdateQuestEndTime()
    {
        while(true)
        {
            if (m_QuestClientInfo.state == QuestClientInfo.State.Select)
            {
                long time = m_TimeMgr.GetOnlineTime();
                long elapsedTimeL = m_QuestMgr.questProgressInfo.endTime - time;
                if (elapsedTimeL < 0)
                {
                    elapsedTimeL = 0;
                }
                questTimeLabel.text = StringUtil.ConvertStringElapseTime(elapsedTimeL / 1000);

                yield return new WaitForSeconds(0.5f);
            }
        }        
    }

    // 퀘스트 진행 중일 때 퀘스트 정보를 세팅한다
    void SetSelectQuestData()
    {
        if(m_QuestMgr.curQuestData != null)
        {
            if (m_QuestEndTimeCoroutine == null)
            {
                m_QuestEndTimeCoroutine = StartCoroutine(UpdateQuestEndTime());
            }

            // 델리게이트 등록
            QuestManager.OnQuestSuccess += OnQuestSuccess;
            QuestManager.OnQuestFail += OnQuestFail;
        }
    }

    // 레벨과 경험치 정보를 갱신한다.
    void RefreshLevelAndExp()
    {
        QuestClientData questClientData = m_QuestClientInfo.questClientData;
        QuestFriendshipData questFriendshipData = null;
        int curExp = 0;
        int endExp = 0;
        for (int i = 0; i < levelExpLabels.Length; i++)
        {
            if(questClientData.lastFriendshipLevel <= m_QuestClientInfo.friendshipLevel)
            {
                if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel - 1))
                {
                    questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel - 1];
                    curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                    endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                    levelExpLabels[i].text = string.Format("LV.{0}   {1}/{2}",
                        m_QuestClientInfo.friendshipLevel, curExp, endExp);
                    expGaugeSprites[i].fillAmount = (float)((float)curExp / (float)endExp);
                }
            }
            else
            {
                if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel))
                {
                    questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel];
                    curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                    endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                    levelExpLabels[i].text = string.Format("LV.{0}   {1}/{2}",
                        m_QuestClientInfo.friendshipLevel, curExp, endExp);
                    expGaugeSprites[i].fillAmount = (float)((float)curExp / (float)endExp);
                }
            }
        }
    }

    //---------------------- 퀘스트 상태 변화를 감지하는 델리게이트 ----------------------
    void OnQuestSuccess()
    {
        QuestManager.OnQuestSuccess -= OnQuestSuccess;
        QuestManager.OnQuestFail -= OnQuestFail;

        m_State = QuestClientInfo.State.Success;

        if (m_QuestEndTimeCoroutine != null)
        {
            StopCoroutine(m_QuestEndTimeCoroutine);
            m_QuestEndTimeCoroutine = null;
        }

        NGUITools.SetActive(selectObject, false);
        NGUITools.SetActive(successObject, true);        

        successAnimator.Play("Quest_Cell_Success_Idle");
    }

    void OnQuestFail()
    {
        QuestManager.OnQuestSuccess -= OnQuestSuccess;
        QuestManager.OnQuestFail -= OnQuestFail;

        m_State = QuestClientInfo.State.Fail;

        if (m_QuestEndTimeCoroutine != null)
        {
            StopCoroutine(m_QuestEndTimeCoroutine);
            m_QuestEndTimeCoroutine = null;
        }

        NGUITools.SetActive(selectObject, false);
        NGUITools.SetActive(failObject, true);

        failAnimator.Play("Quest_Cell_Failed_Idle");
    }

    void OnQuestReturn()
    {
        switch (m_State)
        {
            case QuestClientInfo.State.Normal:
                {
                }
                break;
            case QuestClientInfo.State.Select:
                {
                    QuestManager.OnQuestSuccess -= OnQuestSuccess;
                    QuestManager.OnQuestFail -= OnQuestFail;

                    if (m_QuestEndTimeCoroutine != null)
                    {
                        StopCoroutine(m_QuestEndTimeCoroutine);
                        m_QuestEndTimeCoroutine = null;
                    }

                    selectAnimator.Play("Quest_Cell_Selected_Hide");
                }
                break;
            case QuestClientInfo.State.Success:
                {
                    successAnimator.Play("Quest_Cell_Success_Hide");
                }
                break;
            case QuestClientInfo.State.Fail:
                {
                    failAnimator.Play("Quest_Cell_Failed_Hide");
                }
                break;
        }

        m_State = QuestClientInfo.State.Normal;
    }

    //---------------------- AnimationEvent ----------------------
    // Normal 상태에서 Select 상태로 전환 되는 과정
    public void AE_ChangeNormalToSelect()
    {
        m_State = QuestClientInfo.State.Select;

        NGUITools.SetActive(normalObject, false);
        NGUITools.SetActive(selectObject, true);

        SetSelectQuestData();

        SoundManager.instance.PlaySFX("QuestSelectShow");
        selectAnimator.Play("Quest_Cell_Selected_Show");
    }

    // Select 되는 연출이 끝나고 호출
    public void AE_EndShowSelect()
    {
        m_QuestPopup.EndSelecting();
    }

    public void AE_EndShowNormal()
    {
        m_QuestPopup.isCompleteInitialize = true;
    }

    public void AE_EndHideSelect()
    {
        NGUITools.SetActive(selectObject, false);
        NGUITools.SetActive(normalObject, true);

        NGUITools.SetActive(selectingObject, false);

        RefreshLevelAndExp();

        normalAnimator.Play("Quest_Cell_Normal_Show");
    }

    public void AE_EndHideSuccess()
    {
        NGUITools.SetActive(successObject, false);
        NGUITools.SetActive(normalObject, true);

        NGUITools.SetActive(selectingObject, false);

        RefreshLevelAndExp();

        normalAnimator.Play("Quest_Cell_Normal_Show");        
    }

    public void AE_EndHideFail()
    {
        NGUITools.SetActive(failObject, false);
        NGUITools.SetActive(normalObject, true);

        NGUITools.SetActive(selectingObject, false);

        RefreshLevelAndExp();

        normalAnimator.Play("Quest_Cell_Normal_Show");
    }

    //---------------------- UI 콜백 ----------------------
    public void OnClickClientInfo()
    {
        if(m_QuestPopup.enableMenu)
        {
            SoundManager.PlayButtonClickSound();

            QuestClientInfoPopup questClientInfoPopup = PopupManager.instance.ShowQuestClientInfoPopup();
            questClientInfoPopup.Initialize(m_QuestClientInfo);
        }        
    }

    public void OnClickQuestInfo()
    {
        if (m_QuestPopup.enableMenu)
        {
            SoundManager.PlayButtonClickSound();

            QuestInfoPopup questInfoPopup = PopupManager.instance.ShowQuestInfoPopup();
            questInfoPopup.Initialize(m_QuestClientInfo);
        }
    }

    public void OnClickQuestComplete()
    {
        if (m_QuestPopup.enableMenu)
        {
            SoundManager.PlayButtonClickSound();

            QuestRewardPopup questRewardPopup = PopupManager.instance.ShowQuestRewardPopup();
            questRewardPopup.Initialize(m_QuestClientInfo);

            QuestManager.instance.CompleteQuest();
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestClientInfoPopup : BasePopup
{
    public UILabel titleLabel;
    public UILabel nameLabel;
    public UISprite profileSprite;
    public UILabel levelExpLabel;
    public UISprite expGaugeSprite;
    public UILabel detailDecsLabel;
    public UILabel friendshipLevelLabel;
    public UILabel rewardLabel;
    public UILabel okButtonLabel;
    public UITable rewardTable;
    public GameObject rewardCellPrefab;

    QuestClientInfo m_QuestClientInfo = null;

    public static QuestClientInfoPopup Show(GameObject parent, GameObject prefab)
    {
        QuestClientInfoPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<QuestClientInfoPopup>();

        popup.titleLabel.text = TextDocs.content("UI_NPC_DESC");
        popup.friendshipLevelLabel.text = TextDocs.content("UI_QUEST_FRIENDLY");
        popup.rewardLabel.text = TextDocs.content("UI_FRIENDLY_GIFT");
        popup.okButtonLabel.text = TextDocs.content("BT_OK_TEXT");        

        return popup;
    }

    public void Initialize(QuestClientInfo questClientInfo)
    {
        m_QuestClientInfo = questClientInfo;
        QuestClientData questClientData = m_QuestClientInfo.questClientData;
        QuestFriendshipData questFriendshipData = null;
        int curExp = 0;
        int endExp = 0;

        profileSprite.spriteName = questClientData.imageName;
        nameLabel.text = TextDocs.content(questClientData.name);
        detailDecsLabel.text = TextDocs.content(questClientData.detailDesc);

        if (questClientData.lastFriendshipLevel <= m_QuestClientInfo.friendshipLevel)
        {
            if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel - 1))
            {
                questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel - 1];
                curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
                    m_QuestClientInfo.friendshipLevel, curExp, endExp);
                expGaugeSprite.fillAmount = (float)((float)curExp / (float)endExp);
            }
        }
        else
        {
            if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel))
            {
                questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel];
                curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
                endExp = questFriendshipData.endExp - questFriendshipData.startExp;
                levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
                    m_QuestClientInfo.friendshipLevel, curExp, endExp);
                expGaugeSprite.fillAmount = (float)((float)curExp / (float)endExp);
            }
        }

        //if (questClientData.friendshipDic.ContainsKey(m_QuestClientInfo.friendshipLevel))
        //{
        //    questFriendshipData = questClientData.friendshipDic[m_QuestClientInfo.friendshipLevel];
        //    curExp = m_QuestClientInfo.allExp - questFriendshipData.startExp;
        //    endExp = questFriendshipData.endExp - questFriendshipData.startExp;
        //    levelExpLabel.text = string.Format("LV.{0}   {1}/{2}",
        //        m_QuestClientInfo.friendshipLevel, curExp, endExp);
        //    expGaugeSprite.fillAmount = Mathf.Clamp01((float)curExp / (float)endExp);
        //}

        Dictionary<int, QuestFriendshipData>.Enumerator etor = questClientData.friendshipDic.GetEnumerator();
        while(etor.MoveNext())
        {
            questFriendshipData = etor.Current.Value;
            if(questFriendshipData.levelUpRewardType >= GameConfig.RewardType.C)
            {
                QuestRewardCell questRewardCell = NGUITools.AddChild(rewardTable.gameObject, rewardCellPrefab).GetComponent<QuestRewardCell>();
                questRewardCell.Initialize(questFriendshipData);
            }            
        }
        rewardTable.repositionNow = true;
    }

    public void OnClickOk()
    {
        SoundManager.PlayButtonClickSound();

        Hide();
    }
}

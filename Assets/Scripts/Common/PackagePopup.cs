﻿using UnityEngine;
using System.Collections;
using System.Text;

public class PackagePopup : BasePopup
{
    public UILabel titleLabel;
    public UILabel descLabel;
    public UILabel priceLabel;

    public UILabel buyButtonLabel;
    public UILabel cancelbuttonLabel;

    public PackageData packageData;

    bool m_RefreshPlayerVehicle = true;

    public static PackagePopup Show(GameObject parent, GameObject prefab, PackageData packageData, bool refreshPlayerVehicle)
    {
        PackagePopup popup = NGUITools.AddChild(parent, prefab).GetComponent<PackagePopup>();

        popup.packageData = packageData;

        popup.titleLabel.text = TextDocs.content(packageData.name);
        popup.m_RefreshPlayerVehicle = refreshPlayerVehicle;

        StringBuilder desc = new StringBuilder();
        PackageStockData packageStockData;
        for (int i = 0; i < packageData.stockList.Count; i++)
        {
            packageStockData = packageData.stockList[i];
            if (packageStockData.type == GameConfig.RewardType.Vehicle)
            {
                VehicleData vehicleData = VehicleDocs.instance.vehicleDataDic[packageStockData.value];
                if (vehicleData != null)
                {
                    //\n[99ff00]토큰은 가챠에서 동일한 자동차가\n나오면 얻거나 게임 플레이 보상으로\n얻을 수 있습니다.[-]
                    desc.Append(GameConfig.TokenName[(int)vehicleData.grade]);
                    desc.Append(" ");
                    desc.Append(TextDocs.content(vehicleData.name));

                    if (i < packageData.stockList.Count - 1)
                    {
                        desc.Append(" + ");
                    }
                }
            }
            else if (packageStockData.type == GameConfig.RewardType.AllVehicle)
            {
                desc.Append(TextDocs.content("ALL_VEHICLE_STOCK_NAME"));

                if (i < packageData.stockList.Count - 1)
                {
                    desc.Append(" + ");
                }
            }
            else if(packageStockData.type == GameConfig.RewardType.Pet)
            {
                PetData petData = PetDocs.instance.petDataDic[packageStockData.value];
                if (petData != null)
                {
                    desc.Append(TextDocs.content(petData.name));

                    if (i < packageData.stockList.Count - 1)
                    {
                        desc.Append(" + ");
                    }
                }                
            }
            else if (packageStockData.type == GameConfig.RewardType.Cash)
            {
                desc.Append(TextDocs.content("UI_CASH_TEXT"));
                desc.Append(" ");
                desc.Append(packageStockData.value);

                if (i < packageData.stockList.Count - 1)
                {
                    desc.Append(" + ");
                }
            }
            else if ((int)packageStockData.type < 4)
            {
                desc.Append(string.Format(TextDocs.content("UI_TOKEN_TEXT"), GameConfig.TokenName[(int)packageStockData.type]));
                desc.Append(" ");
                desc.Append(packageStockData.value);

                if (i < packageData.stockList.Count - 1)
                {
                    desc.Append(" + ");
                }
            }           
        }
        popup.descLabel.text = desc.ToString();
        
        if (packageData.stockPrice.CompareTo("") == 0)
        {
            popup.priceLabel.text = packageData.tempPrice;
        }
        else
        {
            popup.priceLabel.text = packageData.stockPrice;
        }
        popup.buyButtonLabel.text = TextDocs.content("BT_BUY_TEXT");
        popup.cancelbuttonLabel.text = TextDocs.content("BT_CANCEL_TEXT");

        return popup;
    }

    void PurchaseComplete(UserManager.PurchaseCompleteState purchaseCompleteState)
    {
        PopupManager.instance.HideLoadingPopup();

        UserManager.OnPurchaseComplete -= PurchaseComplete;

        if (purchaseCompleteState == UserManager.PurchaseCompleteState.Success)
        {
            if(m_RefreshPlayerVehicle)
            {
                // 패키지 구입에 성공했으므로 패키지 버튼 제거 및 패키지내 있는 차량으로 변경
                GameManager.instance.PackagePurchaseComplete(packageData, m_RefreshPlayerVehicle);
            }            

            Hide();            
        }
        else if (purchaseCompleteState == UserManager.PurchaseCompleteState.PurchaseError)
        {
            PopupManager.instance.ShowSmallOK(TextDocs.content("POPUP_PURCHASE_FAIL_TITLE"),
                TextDocs.content("POPUP_PURCHASE_FAIL_TEXT"), null, null);
        }
    }

    public void OnClickBuy()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowLoadingPopup();

        UserManager.OnPurchaseComplete += PurchaseComplete;
        UserManager.instance.Purchase(null, packageData, null);
    }

    public void OnClickCancle()
    {
        SoundManager.PlayButtonClickSound();

        Hide();
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupManager : Singleton<PopupManager>
{
    GameObject m_BehindScrollPopupPanel;
    public GameObject behindScrollPopupPanel
    {
        set
        {
            m_BehindScrollPopupPanel = value;
        }
    }
    GameObject m_PopupPanel;
    public GameObject popupPanel
    {
        set
        {
            m_PopupPanel = value;
        }
    }
    GameObject m_FrontScrollPopupPanel;
    public GameObject frontScrollPopupPanel
    {
        set
        {
            m_FrontScrollPopupPanel = value;
        }
    }
    GameObject m_CommonPopupPanel;
    public GameObject commonPopupPanel
    {
        set
        {
            m_CommonPopupPanel = value;
        }
    }

    public GameObject popupPurchaseOKPrefab;
    public GameObject popupAchievementPrefab;
    public GameObject popupSetupPrefab;
    public GameObject popupEveryPlayPrefab;
    public GameObject popupCouponPrefab;
    public GameObject popupReviewPrefab;
    public GameObject popupQuestPrefab;
    public GameObject popupQuestClientInfoPrefab;
    public GameObject popupQuestInfoPrefab;
    public GameObject popupQuestRewardPrefab;
    public GameObject popupQuestRetryPrefab;
    public GameObject popupPackage0Prefab;
    public GameObject popupPackage1Prefab;
    public GameObject popupPackage2Prefab;
    public GameObject popupPackage3Prefab;
    public GameObject popupExitPrefab;

    public GameObject popupLargeOKPrefab;
    public GameObject popupLargeYesNoPrefab;
    public GameObject popupSmallOKPrefab;
    public GameObject popupSmallYesNoPrefab;

    public LoadingPopup loadingPopup;

    List<BasePopup> m_BackProcessStack = new List<BasePopup>();
    List<BasePopup> m_PopupQueue = new List<BasePopup>();
    BasePopup m_CurrPopup = null;

    public void ShowLoadingPopup()
    {
        if(loadingPopup != null)
        {
            loadingPopup.Show();
        }        
    }

    public void HideLoadingPopup()
    {
        if (loadingPopup != null)
        {
            loadingPopup.Hide();
        }
    }

    public bool IsShowLoadingPopup()
    {
        if (loadingPopup != null)
        {
            return loadingPopup.IsShow();
        }
        else
        {
            return false;
        }
    }

    public CommonPopup ShowLargeOK(string title, string text, CommonPopup.OKDelegate onOK, BasePopup.CancelDelegate onCancel)
    {
        CommonPopup popup = CommonPopup.Show(m_CommonPopupPanel, popupLargeOKPrefab, title, text, onOK, null, null, onCancel);
        AddBackProcessStack(popup);
        AddPopupQueue(popup);

        return popup;
    }

    public CommonPopup ShowLargeYesNo(string title, string text, CommonPopup.YesDelegate onYes, CommonPopup.NoDelegate onNo, BasePopup.CancelDelegate onCancel)
    {
        CommonPopup popup = CommonPopup.Show(m_CommonPopupPanel, popupLargeYesNoPrefab, title, text, null, onYes, onNo, null);
        AddBackProcessStack(popup);
        AddPopupQueue(popup);

        return popup;
    }

    public CommonPopup ShowSmallOK(string title, string text, CommonPopup.OKDelegate onOK, BasePopup.CancelDelegate onCancel)
    {
        CommonPopup popup = CommonPopup.Show(m_CommonPopupPanel, popupSmallOKPrefab, title, text, onOK, null, null, onCancel);
        AddBackProcessStack(popup);
        AddPopupQueue(popup);

        return popup;
    }

    public CommonPopup ShowSmallYesNo(string title, string text, CommonPopup.YesDelegate onYes, CommonPopup.NoDelegate onNo, BasePopup.CancelDelegate onCancel)
    {
        CommonPopup popup = CommonPopup.Show(m_CommonPopupPanel, popupSmallYesNoPrefab, title, text, null, onYes, onNo, null);
        AddBackProcessStack(popup);
        AddPopupQueue(popup);

        return popup;
    }

    //public CommonPopup ShowPurchaseOK(string title, string text, CommonPopup.OKDelegate onOK, BasePopup.CancelDelegate onCancel)
    //{
    //    CommonPopup popup = CommonPopup.Show(m_CommonPopupPanel, popupPurchaseOKPrefab, title, text, onOK, null, null, onCancel);
    //    AddBackProcessStack(popup);
    //    AddPopupQueue(popup);

    //    return popup;
    //}

    public AchievementPopup ShowAchievementPopup()
    {
        AchievementPopup popup = AchievementPopup.Show(m_FrontScrollPopupPanel, popupAchievementPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public SetupPopup ShowSetupPopup()
    {
        SetupPopup popup = SetupPopup.Show(m_PopupPanel, popupSetupPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public EveryPlayPopup ShowEveryPlayPopup()
    {
        EveryPlayPopup popup = EveryPlayPopup.Show(m_PopupPanel, popupEveryPlayPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public CouponPopup ShowCouponPopup()
    {
        CouponPopup popup = CouponPopup.Show(m_PopupPanel, popupCouponPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public QuestPopup ShowQuestPopup()
    {
        QuestPopup popup = QuestPopup.Show(m_PopupPanel, popupQuestPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public QuestClientInfoPopup ShowQuestClientInfoPopup()
    {
        QuestClientInfoPopup popup = QuestClientInfoPopup.Show(m_FrontScrollPopupPanel, popupQuestClientInfoPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public QuestInfoPopup ShowQuestInfoPopup()
    {
        QuestInfoPopup popup = QuestInfoPopup.Show(m_FrontScrollPopupPanel, popupQuestInfoPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public QuestRewardPopup ShowQuestRewardPopup()
    {
        QuestRewardPopup popup = QuestRewardPopup.Show(m_FrontScrollPopupPanel, popupQuestRewardPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public QuestRetryPopup ShowQuestRetryPopup()
    {
        QuestRetryPopup popup = QuestRetryPopup.Show(m_FrontScrollPopupPanel, popupQuestRetryPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public PackagePopup ShowPackage0Popup(PackageData packageData, bool refreshPlayerVehicle)
    {
        PackagePopup popup = PackagePopup.Show(m_PopupPanel, popupPackage0Prefab, packageData, refreshPlayerVehicle);
        AddBackProcessStack(popup);

        return popup;
    }

    public PackagePopup ShowPackage1Popup(PackageData packageData, bool refreshPlayerVehicle)
    {
        PackagePopup popup = PackagePopup.Show(m_PopupPanel, popupPackage1Prefab, packageData, refreshPlayerVehicle);
        AddBackProcessStack(popup);

        return popup;
    }

    public PackagePopup ShowPackage2Popup(PackageData packageData, bool refreshPlayerVehicle)
    {
        PackagePopup popup = PackagePopup.Show(m_PopupPanel, popupPackage2Prefab, packageData, refreshPlayerVehicle);
        AddBackProcessStack(popup);

        return popup;
    }

    public PackagePopup ShowPackage3Popup(PackageData packageData, bool refreshPlayerVehicle)
    {
        PackagePopup popup = PackagePopup.Show(m_PopupPanel, popupPackage3Prefab, packageData, refreshPlayerVehicle);
        AddBackProcessStack(popup);

        return popup;
    }

    public ReviewPopup ShowReviewPopup()
    {
        ReviewPopup popup = ReviewPopup.Show(m_PopupPanel, popupReviewPrefab);
        AddBackProcessStack(popup);

        return popup;
    }

    public ExitPopup ShowExitPopup(bool isStopGame, CommonPopup.OKDelegate onOK_Stop, CommonPopup.NoDelegate onNo_Stop)
    {
        ExitPopup popup = ExitPopup.Show(m_CommonPopupPanel, popupExitPrefab, isStopGame, onOK_Stop, onNo_Stop);
        AddBackProcessStack(popup);
        AddPopupQueue(popup);

        return popup;
    }

    // 뒤로가기 버튼이 눌렸을 경우 가장 마지막에 출력된 팝업부터 닫도록 처리 
    public bool BackProcess()
    {
        Debug.Log("PopupManager.BackProcess() stack count=" + m_BackProcessStack);
        if (m_BackProcessStack.Count > 0)
        {
            //BasePopup popup = m_BackProcessStack.Peek();
            BasePopup popup = m_BackProcessStack[m_BackProcessStack.Count - 1];
            Debug.Log("PopupManager.BackProcess() popup=" + popup);
            if (popup)
            {
                Debug.Log("PopupManager.BackProcess() cancelable=" + popup.cancelable);
                if (popup.cancelable == true)
                {
                    if (popup.OnCancel != null)
                    {
                        popup.OnCancel();
                    }
                    popup.Hide();
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void AddBackProcessStack(BasePopup popup)
    {
        //m_BackProcessStack.Push(popup);
        m_BackProcessStack.Add(popup);
    }

    // CommonPopup은 동시에 여러개가 출력될 수 없기 때문에 다음에 출력되야할 공통 팝업을 큐에 저장한다 
    // CommonPopup이 아닌 다른 팝업(상점, 상품 자세히 보기, 매치 상대 찾기 등의 팝업에서는 사용되지 않는다) 
    public void AddPopupQueue(BasePopup popup)
    {
        m_PopupQueue.Add(popup);

        if (m_CurrPopup != null)    // 현재 팝업이 출력되고 있는 상황이면 새로 출력되어야할 팝업은 Hide상태로 한다 
        {
            NGUITools.SetActive(popup.gameObject, false);
        }
        else // 현재 출력되어있는 팝업이 없으면 바로 출력되도록 한다 
        {
            m_CurrPopup = popup;
        }
    }

    // CommonPopup이 닫힐 때 처리되어야 하는 공통 로직(이 함수가 호출되지 않으면 다음 팝업이 출력되어야 할 팝업이 호출되지 않는다) 
    public void Hide(BasePopup popup)
    {
        if (popup != null)  // 팝업을 제거한다 
        {
            NGUITools.Destroy(popup.gameObject);
        }

        //if (popup == m_BackProcessStack.Peek()) // 뒤로가기 처리를 위한 스택에서 현재 닫히는 팝업을 제거한다 
        //{
        //    Debug.Log("m_BackProcessStack.Pop()");
        //    m_BackProcessStack.Pop();
        //}
        int backPopupIndex = m_BackProcessStack.FindIndex(x => x == popup);
        if (backPopupIndex != -1) // 뒤로가기 처리를 위한 스택에서 현재 닫히는 팝업을 제거한다 
        {
            Debug.Log("m_BackProcessStack.Pop()");
            //m_BackProcessStack.Pop();
            m_BackProcessStack.RemoveAt(backPopupIndex);
        }

        // 현재 닫히는 팝업을 공통 팝업 큐에서 제거 해준다 
        int popupIndex = m_PopupQueue.FindIndex(x => x == popup);
        if (popupIndex != -1)
        {
            if (popup == m_CurrPopup)
            {
                m_CurrPopup = null;
            }

            m_PopupQueue.RemoveAt(popupIndex);
            Debug.Log("m_PopupQueue.Remove()");

            // 공통 팝업 큐에 다른 팝업이 존재한다면 현재 팝업이 닫힌 후에 다음 팝업이 출력 될 수 있도록 Active를 활성화 해준다 
            if (m_PopupQueue.Count > 0)
            {
                m_CurrPopup = m_PopupQueue[0];
                NGUITools.SetActive(m_CurrPopup.gameObject, true);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class TipDocs : Singleton<TipDocs>
{    
    private List<string> m_TipList = new List<string>();
    public List<string> tipList
    {
        get
        {
            return m_TipList;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_TipList.Count <= 0)
        {
            string tipContents = "";
            Dictionary<string, object> allDataByTipTableSchema;
            GDEDataManager.GetAllDataBySchema("TipTable", out allDataByTipTableSchema);
            Dictionary<string, object>.Enumerator etor = allDataByTipTableSchema.GetEnumerator();
            while (etor.MoveNext())
            {
                Dictionary<string, object> tipTableDic = etor.Current.Value as Dictionary<string, object>;
                
                tipTableDic.TryGetString("contents", out tipContents);

                m_TipList.Add(tipContents);
            }
        }
    }

    public string GetTip()
    {
        int index = UnityEngine.Random.Range(0, m_TipList.Count);
        return m_TipList[index];
    }
}
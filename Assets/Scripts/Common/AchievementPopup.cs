﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

public class AchievementPopup : BasePopup
{
    public GameObject cellPrefab;

    public GameObject achievementButtonObject;
    public GameObject leaderboardButtonObject;

    public Animator animator;
    public UILabel popupTitleLabel;
    public UILabel[] tokenLabel;
    public UILabel cashLabel;
    public UILabel closeButtonLabel;
    public UIScrollView scrollView;
    public UITable table;

    bool m_EnableMenu = false;
    public bool enableMenu
    {
        get
        {
            return m_EnableMenu;
        }
    }

    UserManager m_UserMgr;

    void Start()
    {
        m_UserMgr = UserManager.instance;

        popupTitleLabel.text = TextDocs.content("ACHIEVEMENT_TITLE_TEXT");
        closeButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");

        Dictionary<int, int>.Enumerator etor = UserManager.instance.tokenDic.GetEnumerator();
        while (etor.MoveNext())
        {
            if (0 <= etor.Current.Key &&
                etor.Current.Key < tokenLabel.Length)
            {
                tokenLabel[etor.Current.Key].text = string.Format("{0}", etor.Current.Value);
            }
        }
        cashLabel.text = string.Format("{0}", UserManager.instance.cash);

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
//        StartCoroutine(Initialize(true));
//#elif UNITY_ANDROID
#if UNITY_ANDROID
        StartCoroutine(Initialize(true));

        AchievementDocs.OnUpdateAchievement += OnUpdateAchievement;

        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
        {
            if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED)
            {
                if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
                {
                    ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
                    GooglePlayConnection.Instance.Connect();
                }
            }
            else
            {
                AchievementDocs.instance.UpdateAchievement();
            }
        }
#elif UNITY_IOS
		StartCoroutine(Initialize(true));

		AchievementDocs.OnUpdateAchievement += OnUpdateAchievement;

		if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
		{
			Debug.Log("@@ GameCenter Init 1 : " + GameCenterManager.IsPlayerAuthenticated);
			if(GameCenterManager.IsPlayerAuthenticated == false)
			{
				ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
				Debug.Log("@@ GameCenter Init 3");
				UserManager.instance.GameCenterInit(false);
			}
			else
			{
				AchievementDocs.instance.UpdateAchievement();
			}
		}
#endif
    }

    void OnDestroy()
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#else
		AchievementDocs.OnUpdateAchievement -= OnUpdateAchievement;
#endif
    }

    void OnUpdateAchievement()
    {
        StartCoroutine(Initialize(false));
    }

    IEnumerator Initialize(bool needShowAni)
    {
        yield return null;

        CommonUtil.DestroyChildren(table.transform);

        List<AchievementData> acDataList = AchievementDocs.instance.acDataList;
        for (int i = 0; i < acDataList.Count; i++)
        {
            AchievementData achievementData = acDataList[i];
            Debug.Log(string.Format("AchievementData {0} : {1}", i, achievementData.state));
            AchievementCell achievementCell = NGUITools.AddChild(table.gameObject, cellPrefab).GetComponent<AchievementCell>();
            achievementCell.SetAchievement(achievementData);
        }

        table.repositionNow = true;

        yield return null;

        if(needShowAni)
        {
            animator.Play("Popup_Show");
        }
    }

    public void EndShow()
    {
        m_EnableMenu = true;
    }

    public void EndHide()
    {
        Hide();
    }

    public static AchievementPopup Show(GameObject parent, GameObject prefab)
    {
        AchievementPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<AchievementPopup>();

        return popup;
    }

    public void OnClickPlatformAchievement()
    {
        if(m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

#if UNITY_ANDROID
            GooglePlayManager.Instance.ShowAchievementsUI();
#elif UNITY_IOS
			GameCenterManager.ShowAchievements();
#endif
        }
    }

    public void OnClickPlatformLeaderboard()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

#if UNITY_ANDROID
            if (TimeManager.instance.GetTimeState())
            {
                long time = TimeManager.instance.GetOnlineTime();
                long elapsedTimeL = m_UserMgr.aosLeaderBoardTime - time;
                if (elapsedTimeL <= 0 || m_UserMgr.aosLeaderBoardCount < 50)
                {
                    Debug.Log("@@ ShowLeaderBoardCount Popup : " + m_UserMgr.aosLeaderBoardCount);
                    if (elapsedTimeL <= 0)
                    {
                        m_UserMgr.aosLeaderBoardCount = 0;
                        m_UserMgr.aosLeaderBoardTime = time + (24 * 60 * 60 * 1000);
                    }
                    m_UserMgr.aosLeaderBoardCount++;
                    m_UserMgr.SaveUserData();
                    GooglePlayManager.Instance.ShowLeaderBoardById("CgkImbbs5L8bEAIQBQ");
                }
            }

            //GooglePlayManager.Instance.ShowLeaderBoardsUI();
#elif UNITY_IOS
			GameCenterManager.ShowLeaderboard("Veteran_racer");
#endif
        }
    }

    public void OnClickGetTokenHelp()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();
            PopupManager.instance.ShowLargeOK(TextDocs.content("UI_TOKEN_TEXT2"), TextDocs.content("GET_TOKEN_HELP_TEXT"), null, null);
        }
    }

    public void OnClickClose()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EnableMenu = false;

            animator.Play("Popup_Hide");
        }
    }
}

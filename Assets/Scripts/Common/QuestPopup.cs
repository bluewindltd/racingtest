﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestClientRateInfo
{
    public int index;
    public int startRate;
    public int endRate;
}

public class QuestPopup : BasePopup
{
    public QuestClientCell[] season1ClientClientCells;

    QuestClientCell[] m_ClientCells;
    int[] m_SelectingOrders = { 0, 1, 3, 2 };

    public UIButton receiveQuestButton;
    public UILabel receiveQuestButtonLabel;
    public UIButton closeButton;
    public UILabel closeButtonLabel;

    QuestDocs m_QuestDocs;
    QuestManager m_QuestMgr;
    TimeManager m_TimeMgr;

    bool m_EnableMenu = false;
    public bool enableMenu
    {
        get
        {
            return m_EnableMenu;
        }
    }

    int m_CurSeasonIndex = 1;
    bool m_IsCheckReceiveTime = false;
    bool m_IsCompleteInitialize = false;
    public bool isCompleteInitialize
    {
        get
        {
            return m_IsCompleteInitialize;
        }
        set
        {
            m_IsCompleteInitialize = value;
        }
    }

    Coroutine m_UpdateTimeCoroutine = null;

    QuestClientInfo.State m_State = QuestClientInfo.State.Normal;
    public QuestClientInfo.State state
    {
        get
        {
            return m_State;
        }
    }

    Dictionary<int, QuestClientInfo> m_QuestClientInfo = new Dictionary<int, QuestClientInfo>();
    int m_QuestClientRateSum = 0;
    List<QuestClientRateInfo> m_QuestClientRateInfoList = new List<QuestClientRateInfo>();

    void Start()
    {
        QuestManager.OnQuestSuccess += OnQuestSuccess;
        QuestManager.OnQuestFail += OnQuestFail;
        QuestManager.OnQuestReturn += OnQuestReturn;

        m_QuestDocs = QuestDocs.instance;
        m_QuestMgr = QuestManager.instance;
        m_TimeMgr = TimeManager.instance;
        
        closeButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");

        if(m_TimeMgr.GetTimeState())
        {
            m_EnableMenu = true;

            m_QuestMgr.RefreshCurrentQuest();

            ConstructSeaon();

            if (m_UpdateTimeCoroutine == null)
            {
                m_UpdateTimeCoroutine = StartCoroutine(UpdateTime());
            }
        }
        else
        {
            // 네트워크 연결하라는 팝업을 띄운다.
            PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                    TextDocs.content("NETWORK_CONNECT_TEXT"),
                    delegate
                    {
                        m_TimeMgr.ForceTimeRequest();
                        Hide();
                    }
                    ,delegate
                    {
                        m_TimeMgr.ForceTimeRequest();
                        Hide();
                    });

            m_EnableMenu = false;
        }
    }

    void OnDestroy()
    {
        QuestManager.OnQuestSuccess -= OnQuestSuccess;
        QuestManager.OnQuestFail -= OnQuestFail;
        QuestManager.OnQuestReturn -= OnQuestReturn;

        if (m_UpdateTimeCoroutine != null)
        {
            StopCoroutine(m_UpdateTimeCoroutine);
            m_UpdateTimeCoroutine = null;
        }
    }

    void ConstructSeaon()
    {
        // 현재 시즌에 해당하는 클라이언트 UI를 연결한다.
        switch(m_CurSeasonIndex)
        {
            case 1:
                {
                    m_ClientCells = season1ClientClientCells;
                }
                break;
        }

        // 현재 시즌에 해당하는 클라이언트 정보를 정리해서 보관한다.        
        QuestClientInfo questClientInfo = null;
        m_QuestClientInfo.Clear();
        m_QuestClientRateSum = 0;
        m_QuestClientRateInfoList.Clear();        
        QuestClientRateInfo questClientRateInfoInfo;
        Dictionary<int, QuestClientInfo>.Enumerator etor = m_QuestMgr.questClientInfoDic.GetEnumerator();
        while(etor.MoveNext())
        {
            questClientInfo = etor.Current.Value;
            if(m_CurSeasonIndex == questClientInfo.questClientData.seasonIndex)
            {
                if(questClientInfo.state != QuestClientInfo.State.Normal)
                {
                    m_State = questClientInfo.state;
                }
                m_QuestClientInfo.Add(questClientInfo.questClientData.index, questClientInfo);

                // 시작 구간 설정
                questClientRateInfoInfo = new QuestClientRateInfo();
                questClientRateInfoInfo.index = questClientInfo.questClientData.index;
                questClientRateInfoInfo.startRate = m_QuestClientRateSum;
                m_QuestClientRateSum = questClientRateInfoInfo.startRate + questClientInfo.questClientData.rate;
                questClientRateInfoInfo.endRate = m_QuestClientRateSum;
                m_QuestClientRateInfoList.Add(questClientRateInfoInfo);
            }
        }

        // 현재 시즌에 해당하는 클라이언트 UI를 초기화 한다.
        for (int i = 0; i < m_ClientCells.Length; i++)
        {
            m_ClientCells[i].Initialize(this);
        }
    }

    public void Refresh()
    {
        //// 현재 시즌에 해당하는 클라이언트 UI를 재설정 한다.
        //for (int i = 0; i < m_ClientCells.Length; i++)
        //{
        //    m_ClientCells[i].Initialize(this);
        //}
    }

    IEnumerator UpdateTime()
    {
        while (true)
        {
            long time = m_TimeMgr.GetOnlineTime();
            long elapsedTimeL = m_QuestMgr.questProgressInfo.endReceiveTime - time;
            if (elapsedTimeL <= 0)
            {
                if (!m_IsCheckReceiveTime)
                {
                    m_IsCheckReceiveTime = true;
                    receiveQuestButtonLabel.text = TextDocs.content("UI_QUEST_RECEIVE");
                }
            }
            else
            {
                string temp = TextDocs.content("UI_QUEST_RECEIVE") + "\\n" + StringUtil.ConvertStringElapseTime(elapsedTimeL / 1000);
                temp = temp.Replace("\\n", "\n");
                receiveQuestButtonLabel.text = temp;
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    public static QuestPopup Show(GameObject parent, GameObject prefab)
    {
        QuestPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<QuestPopup>();

        //popup.OnCancel = delegate
        //{
        //    GameManager.instance.RefreshQuestTip();
        //};

        return popup;
    }

    IEnumerator StartSelectingQuest()
    {
        yield return new WaitForEndOfFrame();

        QuestClientRateInfo questClientRateInfo;
        int selectClientIndex = -1;
        int gachaRate = UnityEngine.Random.Range(0, m_QuestClientRateSum);
        for (int i = 0; i < m_QuestClientRateInfoList.Count; i++)
        {
            questClientRateInfo = m_QuestClientRateInfoList[i];
            if (questClientRateInfo.startRate <= gachaRate &&
                gachaRate < questClientRateInfo.endRate)
            {
                selectClientIndex = questClientRateInfo.index;
                Debug.Log("Select -> " + selectClientIndex);
                break;
            }
        }

        // clientIndex 해당 클라이언트 퀘스트 선택됨!
        // 연출 시작~
        int index = 0;
        int selectingClientIndex = 0;
        float spinSpeedTime = 0.1f;
        for(int i = 0; i < 15; i++)
        {
            Debug.Log("Selecting -> " + i);

            selectingClientIndex = m_ClientCells[m_SelectingOrders[index]].clientIndex;
            ClientSelectOn(selectingClientIndex);

            index++;
            if(m_SelectingOrders.Length <= index)
            {
                index = 0;
            }

            yield return new WaitForSeconds(spinSpeedTime);

            spinSpeedTime = Mathf.Lerp(spinSpeedTime, 2.3f, RealTime.deltaTime * 0.5f);
        }
        
        // 속도 줄이기 연출까지 끝난 뒤 마지막 클라이언트가 선택된 클라이언트가 아닐 경우
        // 선택된 클라이언트까지 이동하는 연출을 마저 해준다.
        while (selectClientIndex != selectingClientIndex)
        {
            selectingClientIndex = m_ClientCells[m_SelectingOrders[index]].clientIndex;
            ClientSelectOn(selectingClientIndex);

            index++;
            if (m_SelectingOrders.Length <= index)
            {
                index = 0;
            }

            yield return new WaitForSeconds(spinSpeedTime);
        }

        ConfirmSelectClient(selectClientIndex);
    }

    public void EndSelecting()
    {
        m_EnableMenu = true;

        receiveQuestButton.isEnabled = true;
        closeButton.isEnabled = true;

        m_State = QuestClientInfo.State.Select;
    }

    // 퀘스트 선택 한다.
    public void SelectingQuest()
    {
        m_IsCheckReceiveTime = false;

        m_EnableMenu = false;

        receiveQuestButton.isEnabled = false;
        closeButton.isEnabled = false;

        StartCoroutine(StartSelectingQuest());
    }

    void ClientSelectOn(int clientIndex)
    {
        QuestClientCell questClientCell = null;
        for(int i = 0; i < m_ClientCells.Length; i++)
        {
            questClientCell = m_ClientCells[i];
            if(questClientCell.clientIndex == clientIndex)
            {
                questClientCell.SelectingOn();
            }
            else
            {
                questClientCell.SelectingOff();
            }
        }

        SoundManager.instance.PlaySFX("QuestSelecting");
    }

    // 선택 연출이 끝난 뒤 선택된 클라이언트의 상태를 Select로 변경해준다.
    void ConfirmSelectClient(int selectClientIndex)
    {
        QuestClientCell questClientCell = null;
        for (int i = 0; i < m_ClientCells.Length; i++)
        {
            questClientCell = m_ClientCells[i];
            if (questClientCell.clientIndex == selectClientIndex)
            {
                break;
            }
        }

        questClientCell.SelectingOff();
        questClientCell.SelectQuest();
    }

    IEnumerator StartRetryQuest()
    {
        // 퀘스트 상태를 원래대로 돌려놓는다.
        m_QuestMgr.RefreshQuest();

        while(!m_IsCompleteInitialize)
        {
            yield return new WaitForEndOfFrame();
        }        

        yield return StartCoroutine(StartSelectingQuest());
    }

    // 퀘스트를 다시 받는다.
    public void RetryQuest()
    {
        if (m_State != QuestClientInfo.State.Normal)
        {
            m_State = QuestClientInfo.State.Normal;

            // 모든 클라이언트 상태가 초기화 되었는지 체크하는 변수
            m_IsCompleteInitialize = false;

            m_IsCheckReceiveTime = false;

            m_EnableMenu = false;

            receiveQuestButton.isEnabled = false;
            closeButton.isEnabled = false;

            StartCoroutine(StartRetryQuest());
        }
        else
        {
            SelectingQuest();
        }
    }

    //---------------------- 퀘스트 상태 변화를 감지하는 델리게이트 ----------------------
    void OnQuestSuccess()
    {
        m_State = QuestClientInfo.State.Success;
    }

    void OnQuestFail()
    {
        m_State = QuestClientInfo.State.Fail;
    }

    void OnQuestReturn()
    {
        m_State = QuestClientInfo.State.Normal;
    }

    //---------------------- UI 콜백 ----------------------
    public void OnClickRecevieQuest()
    {
        if(m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            long time = m_TimeMgr.GetOnlineTime();
            long elapsedTimeL = m_QuestMgr.questProgressInfo.endReceiveTime - time;

            switch (m_State)
            {
                case QuestClientInfo.State.Normal:
                    {
                        if (elapsedTimeL <= 0)
                        {
                            SelectingQuest();
                        }
                        else
                        {
                            // 돈주고 의뢰 다시 하겠냐고 팝업 띄움
                            QuestRetryPopup questRetryPopup = PopupManager.instance.ShowQuestRetryPopup();
                            questRetryPopup.Initialize(this);
                        }
                    }
                    break;

                case QuestClientInfo.State.Select:
                case QuestClientInfo.State.Success:
                    {
                        // 퀘스트 진행 중이거나 완료되었을 때 의뢰 수주가 가능할 때
                        if (elapsedTimeL <= 0)
                        {
                            // 기존 의뢰 사라진다는 경고 팝업 띄움
                            PopupManager.instance.ShowLargeYesNo(TextDocs.content("LOAD_WARNING_TITLE"), TextDocs.content("POPUP_QUEST_NOTICE01"),
                                delegate
                                {
                                    RetryQuest();
                                }, null, null);
                        }
                        // 퀘스트 진행 중이거나 완료되었으나 의뢰 수주가 가능하지 않을 때
                        else
                        {
                            // 돈주고 의뢰 다시 하겠냐고 팝업 띄움
                            QuestRetryPopup questRetryPopup = PopupManager.instance.ShowQuestRetryPopup();
                            questRetryPopup.Initialize(this);
                        }
                    }
                    break;

                case QuestClientInfo.State.Fail:
                    {
                        // 퀘스트 실패시 의뢰 수주가 가능할 때
                        if (elapsedTimeL <= 0)
                        {
                            // 실패 된 의뢰인 상태 갱신 후 바로 의뢰인 선택 연출 시작
                            RetryQuest();
                        }
                        // 퀘스트 실패시 의뢰 수주가 가능하지 않을 때
                        else
                        {
                            // 돈주고 의뢰 다시 하겠냐고 팝업 띄움
                            QuestRetryPopup questRetryPopup = PopupManager.instance.ShowQuestRetryPopup();
                            questRetryPopup.Initialize(this);
                        }
                    }
                    break;
            }            
        }
    }

    public void OnClickExit()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            //GameManager.instance.RefreshQuestTip();

            Hide();
        }
    }
}

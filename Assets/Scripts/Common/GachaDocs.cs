﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class GachaInfo
{
    public int rate;
    public int preventDuplicationCount;
}

public class GachaRateInfo
{
    public int index;
    public int startRate;
    public int endRate;
}

public class GachaTokenRateInfo
{
    public int index;
    public int startRate;
    public int endRate;
    public int minAmount;
    public int maxAmount;
}

public class GachaDocs : Singleton<GachaDocs>
{
    public enum GachaToken
    {
        None = -1,
        C = 100000,
        B = 100001,
        A = 100002,
        S = 100003,
    };

    public static Dictionary<int, int> tokenIG = new Dictionary<int, int>() { { (int)VehicleData.Grade.C, (int)GachaToken.C }, { (int)VehicleData.Grade.B, (int)GachaToken.B }, { (int)VehicleData.Grade.A, (int)GachaToken.A }, { (int)VehicleData.Grade.S, (int)GachaToken.S } };
    public static Dictionary<int, int> tokenGI = new Dictionary<int, int>() { { (int)GachaToken.C, (int)VehicleData.Grade.C }, { (int)GachaToken.B, (int)VehicleData.Grade.B }, { (int)GachaToken.A, (int)VehicleData.Grade.A }, { (int)GachaToken.S, (int)VehicleData.Grade.S } };

    Dictionary<int, GachaInfo> m_GachaInfoDic = new Dictionary<int, GachaInfo>();

    List<GachaRateInfo> m_GachaRateInfoList = new List<GachaRateInfo>();
    public List<GachaRateInfo> gachaRateInfoList
    {
        get
        {
            return m_GachaRateInfoList;
        }
    }

    List<GachaRateInfo> m_B_StaticGachaRateInfoList = new List<GachaRateInfo>();
    public List<GachaRateInfo> b_StaticGachaRateInfoList
    {
        get
        {
            return m_B_StaticGachaRateInfoList;
        }
    }

    List<GachaRateInfo> m_A_StaticGachaRateInfoList = new List<GachaRateInfo>();
    public List<GachaRateInfo> a_StaticGachaRateInfoList
    {
        get
        {
            return m_A_StaticGachaRateInfoList;
        }
    }

    List<GachaTokenRateInfo> m_GachaTokenRateInfoList = new List<GachaTokenRateInfo>();
    public List<GachaTokenRateInfo> gachaTokenRateInfoList
    {
        get
        {
            return m_GachaTokenRateInfoList;
        }
    }

    int m_B_StaticGachaRateSum = 0;
    public int b_StaticGachaRateSum
    {
        get
        {
            return m_B_StaticGachaRateSum;
        }
    }

    int m_A_StaticGachaRateSum = 0;
    public int a_StaticGachaRateSum
    {
        get
        {
            return m_A_StaticGachaRateSum;
        }
    }

    int m_GachaRateSum = 0;
    public int gachaRateSum
    {
        get
        {
            return m_GachaRateSum;
        }
    }

    int m_GachaTokenRateSum = 0;
    public int gachaTokenRateSum
    {
        get
        {
            return m_GachaTokenRateSum;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_GachaInfoDic.Count <= 0)
        {
            int index = 0;
            int rate = 0;
            int minAmount = 0;
            int maxAmount = 0;
            Dictionary<string, object> allDataByGachaTableSchema;
            GDEDataManager.GetAllDataBySchema("GachaTable", out allDataByGachaTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByGachaTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> gachaTableDic = etorTable.Current.Value as Dictionary<string, object>;

                GachaInfo gachaInfo = new GachaInfo();

                gachaTableDic.TryGetInt("index", out index);
                gachaTableDic.TryGetInt("rate", out gachaInfo.rate);
                gachaTableDic.TryGetInt("preventDuplication", out gachaInfo.preventDuplicationCount);
                m_GachaInfoDic.Add(index, gachaInfo);
            }

            m_GachaTokenRateSum = 0;
            Dictionary<string, object> allDataByGachaTokenTableSchema;
            GDEDataManager.GetAllDataBySchema("GachaTokenTable", out allDataByGachaTokenTableSchema);
            etorTable = allDataByGachaTokenTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> gachaTokenTableDic = etorTable.Current.Value as Dictionary<string, object>;

                GachaTokenRateInfo gachaTokenRateInfo = new GachaTokenRateInfo();

                gachaTokenTableDic.TryGetInt("index", out index);
                gachaTokenTableDic.TryGetInt("rate", out rate);
                gachaTokenTableDic.TryGetInt("minAmount", out minAmount);
                gachaTokenTableDic.TryGetInt("maxAmount", out maxAmount);

                gachaTokenRateInfo.index = index;
                gachaTokenRateInfo.startRate = m_GachaTokenRateSum;
                m_GachaTokenRateSum += rate;
                gachaTokenRateInfo.endRate = m_GachaTokenRateSum;
                gachaTokenRateInfo.minAmount = minAmount;
                gachaTokenRateInfo.maxAmount = maxAmount;

                m_GachaTokenRateInfoList.Add(gachaTokenRateInfo);
            }

            ConstructStaticGachaRate();
            ConstructGachaRate();
        }
    }

    // 3번째 가챠에서 B급 차량,
    // 5번째 가챠에서 A급 차량을 무조건 주기 위해 확률을 계산 해둔다.
    void ConstructStaticGachaRate()
    {
        UserManager userMgr = UserManager.instance;
        VehicleDocs vehicleDocs = VehicleDocs.instance;        

        m_B_StaticGachaRateInfoList.Clear();
        m_A_StaticGachaRateInfoList.Clear();
        m_B_StaticGachaRateSum = 0;
        m_A_StaticGachaRateSum = 0;
        int index = -1;
        GachaInfo gachaInfo;
        GachaRateInfo gachaRateInfo;
        int revisionRate = 0;
        Dictionary<int, GachaInfo>.Enumerator etor = m_GachaInfoDic.GetEnumerator();
        while (etor.MoveNext())
        {
            index = etor.Current.Key;
            if(vehicleDocs.vehicleDataDic.ContainsKey(index))
            {
                VehicleData vehicleData = vehicleDocs.vehicleDataDic[index];
                if(vehicleData.grade == VehicleData.Grade.B)
                {
                    gachaInfo = etor.Current.Value;

                    gachaRateInfo = new GachaRateInfo();
                    gachaRateInfo.index = index;
                    // 시작 구간 설정
                    gachaRateInfo.startRate = m_B_StaticGachaRateSum;

                    revisionRate = gachaInfo.rate;
                    m_B_StaticGachaRateSum = gachaRateInfo.startRate + revisionRate;
                    gachaRateInfo.endRate = m_B_StaticGachaRateSum;

                    m_B_StaticGachaRateInfoList.Add(gachaRateInfo);
                }
                else if(vehicleData.grade == VehicleData.Grade.A)
                {
                    gachaInfo = etor.Current.Value;

                    gachaRateInfo = new GachaRateInfo();
                    gachaRateInfo.index = index;
                    // 시작 구간 설정
                    gachaRateInfo.startRate = m_A_StaticGachaRateSum;

                    revisionRate = gachaInfo.rate;
                    m_A_StaticGachaRateSum = gachaRateInfo.startRate + revisionRate;
                    gachaRateInfo.endRate = m_A_StaticGachaRateSum;

                    m_A_StaticGachaRateInfoList.Add(gachaRateInfo);
                }
            }
        }
    }

    void ConstructGachaRate()
    {
        UserManager userMgr = UserManager.instance;
        VehicleDocs vehicleDocs = VehicleDocs.instance;

        bool allVehicles = false;
        if(userMgr.vehicleInfoDic.Count >= vehicleDocs.vehicleDataDic.Count)
        {
            allVehicles = true;
        }
        m_GachaRateInfoList.Clear();
        m_GachaRateSum = 0;
        GachaInfo gachaInfo;
        GachaRateInfo gachaRateInfo;
        int duplicationGachaCount = 0;
        int revisionRate = 0;
        int userDuplicationGachaCount = userMgr.duplicationGachaCount;
        Dictionary<int, GachaInfo>.Enumerator etor = m_GachaInfoDic.GetEnumerator();
        while (etor.MoveNext())
        {
            gachaInfo = etor.Current.Value;

            // 중복된 상품의 경우 제외 시키기 위해 중복 한도 설정값과 현재 중복값 중 작은 값을 구한다.
            duplicationGachaCount = Mathf.Min(gachaInfo.preventDuplicationCount, userDuplicationGachaCount);

            gachaRateInfo = new GachaRateInfo();
            gachaRateInfo.index = etor.Current.Key;
            // 시작 구간 설정
            gachaRateInfo.startRate = m_GachaRateSum;

            if(gachaInfo.preventDuplicationCount > 0 &&
               userMgr.vehicleInfoDic.ContainsKey(etor.Current.Key) &&
               !allVehicles)
            {
                if(userDuplicationGachaCount > 0)
                {
                    int aa = 0;
                    aa = 10;
                }
                // 중복 한도값과 현재 중복 값을 계산하여 끝 구간을 설정한다.
                if(gachaInfo.preventDuplicationCount <= userDuplicationGachaCount)
                {
                    // 나눗셈 소수점 연산 때문에 딱 안맞을 경우가 있을거 같아서 중복횟수가 중복 방지 횟수랑 같을 경우는,
                    // 나올 확률이 딱 0이 되게 맞춰준다.
                    revisionRate = gachaInfo.rate;
                }
                else
                {
                    revisionRate = (int)((float)gachaInfo.rate / (float)gachaInfo.preventDuplicationCount) * duplicationGachaCount;
                }                
                revisionRate = gachaInfo.rate - revisionRate;
            }
            else
            {
                revisionRate = gachaInfo.rate;
            }
            
            // 0보다 낮을 경우 예외처리
            if(revisionRate < 0)
            {
                revisionRate = 0;
            }
            m_GachaRateSum = gachaRateInfo.startRate + revisionRate;
            gachaRateInfo.endRate = m_GachaRateSum;

            m_GachaRateInfoList.Add(gachaRateInfo);
        }
    }

    void PlayTokenGacha(ref int index, ref int amount)
    {
        GachaTokenRateInfo gachaTokenRateInfo;
        index = -1;
        amount = 0;
        int gachaRate = UnityEngine.Random.Range(0, m_GachaTokenRateSum);
        for (int i = 0; i < m_GachaTokenRateInfoList.Count; i++)
        {
            gachaTokenRateInfo = m_GachaTokenRateInfoList[i];
            if (gachaTokenRateInfo.startRate <= gachaRate &&
                gachaRate < gachaTokenRateInfo.endRate)
            {
                index = gachaTokenRateInfo.index;
                if (index != 4)
                {
                    amount = UnityEngine.Random.Range(gachaTokenRateInfo.minAmount, gachaTokenRateInfo.maxAmount + 1);
                }
                break;
            }
        }
    }
    static int haha = 1;
    public int PlayGacha(bool isTest, ref bool isStaticGacha, ref int tokenIndex, ref int tokenCount)
    {
        string logString = "";
        UserManager userMgr = UserManager.instance;
        VehicleDocs vehicleDocs = VehicleDocs.instance;
        GachaRateInfo gachaRateInfo;
        int resultIndex = -1;
        int gachaRate = 0;

        // 3번째 가챠이고 B급 차량이 없을 경우 B급 차량이 고정으로 나오는 가챠를 돌린다.
        if(userMgr.playGachaCount == 2 && userMgr.isPlayBStaticGacha == false)
        {            
            Debug.Log("B급 차량 고정 가챠 실행");
            isStaticGacha = true;
            gachaRate = UnityEngine.Random.Range(0, m_B_StaticGachaRateSum);
            for (int i = 0; i < m_B_StaticGachaRateInfoList.Count; i++)
            {
                gachaRateInfo = m_B_StaticGachaRateInfoList[i];
                if (gachaRateInfo.startRate <= gachaRate &&
                    gachaRate < gachaRateInfo.endRate)
                {
                    resultIndex = gachaRateInfo.index;
                    break;
                }
            }

            userMgr.isPlayBStaticGacha = true;
        }
        // 5번째 가챠이고 A급 차량이 없을 경우 A급 차량이 고정으로 나오는 가챠를 돌린다.
        else if (userMgr.playGachaCount == 4 && userMgr.isPlayAStaticGacha == false)
        {
            Debug.Log("A급 차량 고정 가챠 실행");
            isStaticGacha = true;
            gachaRate = UnityEngine.Random.Range(0, m_A_StaticGachaRateSum);
            for (int i = 0; i < m_A_StaticGachaRateInfoList.Count; i++)
            {
                gachaRateInfo = m_A_StaticGachaRateInfoList[i];
                if (gachaRateInfo.startRate <= gachaRate &&
                    gachaRate < gachaRateInfo.endRate)
                {
                    resultIndex = gachaRateInfo.index;
                    break;
                }
            }

            userMgr.isPlayAStaticGacha = true;
        }
        else
        {
            Debug.Log("일반 가챠 실행");
            isStaticGacha = false;
            gachaRate = UnityEngine.Random.Range(0, m_GachaRateSum);
            for (int i = 0; i < m_GachaRateInfoList.Count; i++)
            {
                gachaRateInfo = m_GachaRateInfoList[i];
                if (gachaRateInfo.startRate <= gachaRate &&
                    gachaRate < gachaRateInfo.endRate)
                {
                    resultIndex = gachaRateInfo.index;
                    break;
                }
            }
        }

        // 가챠 돌린 횟수를 증가시켜준다.
        userMgr.playGachaCount++;

        //// 모든 차량을 가지고 있지 않을 때만 차량 가챠를 돌린다.
        // 수정 모든 차량을 가지고 있어도 가챠를 돌리고 해당 클래스에 맞는 토큰을 지급한다.
        //if(userMgr.vehicleInfoDic.Count < vehicleDocs.vehicleDataDic.Count)
        //{
        //  for (int i = 0; i < m_GachaRateInfoList.Count; i++)
        //    {
        //        gachaRateInfo = m_GachaRateInfoList[i];
        //        if (gachaRateInfo.startRate <= gachaRate &&
        //            gachaRate < gachaRateInfo.endRate)
        //        {
        //            resultIndex = gachaRateInfo.index;
        //            break;
        //        }
        //    }
        //}

        // test
        if (isTest)
        {
            int testIndex = -1;
            for(int i = haha; i < 77; i++)
            {
                if(vehicleDocs.vehicleDataDic.ContainsKey(i))
                {
                    testIndex = i;
                    break;
                }
            }
            if(testIndex != -1)
            {
                haha = testIndex;
                resultIndex = vehicleDocs.vehicleDataDic[haha].index;
                haha++;
            }
            else
            {
                haha = 1;
                resultIndex = vehicleDocs.vehicleDataDic[haha].index;
                haha++;
            }
        }

        logString = string.Format("{0}/{1} -> {2}", gachaRate, m_GachaRateSum, resultIndex);

        if (resultIndex != -1)
        {
            // 이미 가지고 있는 차량의 경우
            if(userMgr.vehicleInfoDic.ContainsKey(resultIndex))
            {
                logString += string.Format(" 중복({0})", resultIndex);
                // 중복 카운트를 늘려준다.
                userMgr.duplicationGachaCount++;

                // 동급의 조각을 지급한다.
                if(vehicleDocs.vehicleDataDic.ContainsKey(resultIndex))
                {
                    VehicleData vehicleData = vehicleDocs.vehicleDataDic[resultIndex];
                    tokenIndex = (int)tokenIG[(int)vehicleData.grade];

                    //userMgr.tokenDic[(int)vehicleData.grade] += 1;

                    GachaTokenRateInfo gachaTokenRateInfo;
                    for (int i = 0; i < m_GachaTokenRateInfoList.Count; i++)
                    {
                        gachaTokenRateInfo = m_GachaTokenRateInfoList[i];
                        if(gachaTokenRateInfo.index == (int)vehicleData.grade)
                        {
                            tokenCount = UnityEngine.Random.Range(gachaTokenRateInfo.minAmount, gachaTokenRateInfo.maxAmount + 1);
                            Debug.Log(string.Format("{0}토큰 -> {1}에서 {2} 사이값 = {3}", (int)vehicleData.grade,
                                gachaTokenRateInfo.minAmount, gachaTokenRateInfo.maxAmount, tokenCount));
                            userMgr.tokenDic[(int)vehicleData.grade] += tokenCount;
                        }
                    }
                }
                else
                {
                    // 오류
                    // 이 경우는 토큰 가챠로 결과를 정한다.
                    resultIndex = -1;
                    int tokenGrade = -1;
                    int amount = 0;
                    PlayTokenGacha(ref tokenGrade, ref amount);
                    tokenIndex = (int)tokenIG[tokenGrade];

                    tokenCount = amount;
                    userMgr.tokenDic[tokenGrade] += amount;
                }

                userMgr.SaveUserData();
            }
            else
            {
                tokenIndex = -1;
                tokenCount = 0;

                logString += string.Format(" 새로운 차량 획득({0})", resultIndex);

                // 없는 차량이 나왔으므로 중복 카운트를 초기화 한다.
                userMgr.duplicationGachaCount = 0;

                // 새로 얻은 차량을 추가한다.
                userMgr.VehicleBuyToGacha(resultIndex);
            }

            // 다시 가챠 확률 테이블을 구축한다.
            ConstructGachaRate();
        }
        else
        {
            //! 주의 : 예전 구현...지금은 예외처리상 남겨둠.
            //logString += " 모든 차량 획득 -> 조각 가챠";
            logString += " 결과 -1 에러 -> 조각 가챠";

            // 모든 차량을 다 가지고 있을 경우 토큰 가챠로 결과를 정한다.
            int tokenGrade = -1;
            int amount = 0;
            PlayTokenGacha(ref tokenGrade, ref amount);
            resultIndex = (int)tokenIG[tokenGrade];

            userMgr.tokenDic[tokenGrade] += amount;

            userMgr.SaveUserData();
        }

        Debug.Log(logString);

        if(!isTest &&
           resultIndex != -1)
        {
            userMgr.cash -= EtcDocs.instance.gachaCash;
        }

        return resultIndex;
    }
}

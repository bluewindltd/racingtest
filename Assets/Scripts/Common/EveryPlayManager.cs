﻿using UnityEngine;
using System.Collections;

public class EveryPlayManager : Singleton<EveryPlayManager>
{
    public delegate void OnRecordingStoppedDelegate();
    public static OnRecordingStoppedDelegate OnRecordingStopped = delegate () { };

    bool m_ReadyForRecording = false;
    public bool readyForRecording
    {
        get
        {
            return m_ReadyForRecording;
        }
    }

    string m_RecordingCarName = "";
    public string recordingCarName
    {
        get
        {
            return m_RecordingCarName;
        }
        set
        {
            m_RecordingCarName = value;
        }
    }

    int m_RecordingScore = 0;
    public int recordingScore
    {
        get
        {
            return m_RecordingScore;
        }
        set
        {
            m_RecordingScore = value;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        Everyplay.ReadyForRecording += OnReadyForRecording;
        Everyplay.RecordingStarted += RecordingStarted;
        Everyplay.RecordingStopped += RecordingStopped;
        Everyplay.FaceCamSessionStarted += FaceCamSessionStarted;
        Everyplay.FaceCamSessionStopped += FaceCamSessionStopped;
    }

    void OnDestroy()
    {
        Everyplay.ReadyForRecording -= OnReadyForRecording;
        Everyplay.RecordingStarted -= RecordingStarted;
        Everyplay.RecordingStopped -= RecordingStopped;
        Everyplay.FaceCamSessionStarted -= FaceCamSessionStarted;
        Everyplay.FaceCamSessionStopped -= FaceCamSessionStopped;
    }

    public bool IsSupported()
    {
        return Everyplay.IsSupported() && Everyplay.IsRecordingSupported();
    }

    public bool IsRecording()
    {
        return Everyplay.IsRecording();
    }

    public void StartRecording()
    {
        Everyplay.StartRecording();
    }

    public void StopRecording()
    {
        Everyplay.StopRecording();
    }

    public bool FaceCamIsVideoRecordingSupported()
    {
        return Everyplay.FaceCamIsVideoRecordingSupported();
    }

    public bool FaceCamIsAudioRecordingSupported()
    {
        return Everyplay.FaceCamIsAudioRecordingSupported();
    }

    public bool FaceCamIsSessionRunning()
    {
        return Everyplay.FaceCamIsSessionRunning();
    }

    public void FaceCamStartSession()
    {
        Debug.Log("EveryplayManager FaceCamStartSession");
        Everyplay.FaceCamStartSession();
    }

    public void FaceCamRequestRecordingPermission()
    {
        Everyplay.FaceCamRequestRecordingPermission();
    }

    public void FaceCamStopSession()
    {
        Everyplay.FaceCamStopSession();
    }

    public void Show()
    {
        Everyplay.Show();
    }

    public void ShowSharingModal()
    {
        Everyplay.SetMetadata("carname", m_RecordingCarName);
        Everyplay.SetMetadata("score", m_RecordingScore);
        Everyplay.ShowSharingModal();
    }    

    void OnReadyForRecording(bool enabled)
    {
        if(enabled)
        {
            //Debug.Log("OnReadyForRecording : 성공 - " + enabled);
            m_ReadyForRecording = true;

            FaceCamRequestRecordingPermission();

            Everyplay.FaceCamSetPreviewVisible(true);
            Everyplay.FaceCamSetPreviewScaleRetina(true);
            Everyplay.FaceCamSetPreviewOrigin(Everyplay.FaceCamPreviewOrigin.TopRight);
            //if(FaceCamIsVideoRecordingSupported())
            //{
            //    Debug.Log("페이스 캠 전면 카메라 사용 할 수 있음");
            //}
            //else
            //{
            //    Debug.Log("페이스 캠 전면 카메라 사용 할 수 없음");
            //}

            //if(FaceCamIsAudioRecordingSupported())
            //{
            //    Debug.Log("페이스 캠 오디오 사용 할 수 있음");
            //}
            //else
            //{
            //    Debug.Log("페이스 캠 오디오 사용 할 수 없음");
            //}
        }
        else
        {
            //Debug.Log("OnReadyForRecording : 실패 - " + enabled);
            m_ReadyForRecording = false;
        }
    }

    void RecordingStarted()
    {
        //Debug.Log("녹화 시작 되었다!");

        m_RecordingCarName = "";
        m_RecordingScore = 0;
    }

    void RecordingStopped()
    {
        //Debug.Log("녹화 중단 되었다!");

        if(OnRecordingStopped != null)
        {
            OnRecordingStopped();
        }
    }

    void FaceCamSessionStarted()
    {
        //Debug.Log("페이스캠 활성화");
    }

    void FaceCamSessionStopped()
    {
        //Debug.Log("페이스캠 비활성화");
    }
}

﻿using UnityEngine;
using System.Collections;

public class CouponPopup : BasePopup
{
    public UILabel titleLabel;
    public UILabel inputTextLabel;
    public UIInput couponInput;
    public UILabel textLabel;

    public UILabel buttonOKLabel;
    public UILabel buttonCancelLabel;

    string m_CouponNumber = "";

    public static CouponPopup Show(GameObject parent, GameObject prefab)
    {
        CouponPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<CouponPopup>();

        popup.titleLabel.text = TextDocs.content("COUPON_POPUP_TITLE");
        popup.inputTextLabel.text = TextDocs.content("COUPON_POPUP_INPUT");
        popup.textLabel.text = TextDocs.content("COUPON_POPUP_TEXT");
        popup.buttonOKLabel.text = TextDocs.content("BT_OK_TEXT");
        popup.buttonCancelLabel.text = TextDocs.content("BT_CANCEL_TEXT");

        return popup;
    }

    public void OnClickCheckCoupon()
    {
#if !UNITY_IOS
        SoundManager.PlayButtonClickSound();

        bool isValid = true;
        if (0 < couponInput.value.Length && couponInput.value.Length <= 16)
        {
            m_CouponNumber = couponInput.value;
        }
        else
        {
            isValid = false;
        }

        if (isValid)
        {
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
                // 쿠폰 확인
                StartCoroutine(UserManager.instance.ConfirmCoupon(m_CouponNumber));
            }
            else
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                    TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);
            }
        }
        else
        {
            PopupManager.instance.ShowSmallOK(TextDocs.content("ERROR"), TextDocs.content("COUPON_INPUT_ERROR"), null, null);
        }

        DisableNavUI.Start();
#endif
    }

    public void OnClickNo()
    {
        SoundManager.PlayButtonClickSound();

        DisableNavUI.Start();

        Hide();
    }

    public void OnDisableNavUI()
    {
        DisableNavUI.Start();
    }
}

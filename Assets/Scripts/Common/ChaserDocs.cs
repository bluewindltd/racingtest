﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class ChaserData
{
    public enum Type
    {
        None = -1,
        Police = 0,
        SWAT = 1,
        Chopper = 2,
        Hummer = 3,
        Apache = 4,
        Tank = 5,
        Gangster = 6,
    }

    public enum Classes
    {
        Legal = 0,
        Illegal = 1,
    }

    public enum Kind
    {
        Car = 0,
        Helicopter = 1,
    }

    public Type type;
    public Classes classes;
    public Kind kind;
    public List<float> appearRate = new List<float>();
    public int health;
    public int damage;
    public int attackDamage;
    public float attackDelay;
    public float rotorSpeed;
    public float shellSpeed;
    public float explosionRadius;
}

public class ChaserAppearRate
{
    public float startRate;
    public float endRate;
    public ChaserData.Type type;
}

public class ChaserAppearRateInfo
{
    public float sum = 0;
    public List<ChaserAppearRate> rateList = new List<ChaserAppearRate>();
    public List<ChaserData.Type> forceAppearList = new List<ChaserData.Type>();
}

public class ChaserDocs : Singleton<ChaserDocs>
{
    private Dictionary<ChaserData.Type, ChaserData> m_ChaserDataDic = new Dictionary<ChaserData.Type, ChaserData>();
    public Dictionary<ChaserData.Type, ChaserData> chaserDataDic
    {
        get
        {
            return m_ChaserDataDic;
        }
    }

    Dictionary<int, ChaserAppearRateInfo> m_AppearRateDic = new Dictionary<int, ChaserAppearRateInfo>();
    public Dictionary<int, ChaserAppearRateInfo> appearRateDic
    {
        get
        {
            return m_AppearRateDic;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if(m_ChaserDataDic.Count <= 0)
        {
            Dictionary<string, object> allDataByChaserTableSchema;
            GDEDataManager.GetAllDataBySchema("ChaserTable", out allDataByChaserTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByChaserTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> chaserTableDic = etorTable.Current.Value as Dictionary<string, object>;

                ChaserData chaserData = new ChaserData();

                int type = 0;
                chaserTableDic.TryGetInt("type", out type);
                chaserData.type = (ChaserData.Type)type;
                int classes = 0;
                chaserTableDic.TryGetInt("classes", out classes);
                chaserData.classes = (ChaserData.Classes)classes;
                int kind = 0;
                chaserTableDic.TryGetInt("kind", out kind);
                chaserData.kind = (ChaserData.Kind)kind;
                int tempStar = 0;
                chaserTableDic.TryGetInt("star1", out tempStar);
                chaserData.appearRate.Add(tempStar);
                chaserTableDic.TryGetInt("star2", out tempStar);
                chaserData.appearRate.Add(tempStar);
                chaserTableDic.TryGetInt("star3", out tempStar);
                chaserData.appearRate.Add(tempStar);
                chaserTableDic.TryGetInt("star4", out tempStar);
                chaserData.appearRate.Add(tempStar);
                chaserTableDic.TryGetInt("star5", out tempStar);
                chaserData.appearRate.Add(tempStar);
                chaserTableDic.TryGetInt("star6", out tempStar);
                chaserData.appearRate.Add(tempStar);
                chaserTableDic.TryGetInt("health", out chaserData.health);
                chaserTableDic.TryGetInt("damage", out chaserData.damage);
                chaserTableDic.TryGetInt("attackDamage", out chaserData.attackDamage);
                chaserTableDic.TryGetFloat("attackDelay", out chaserData.attackDelay);
                chaserTableDic.TryGetFloat("rotorSpeed", out chaserData.rotorSpeed);
                chaserTableDic.TryGetFloat("shellSpeed", out chaserData.shellSpeed);
                chaserTableDic.TryGetFloat("explosionRadius", out chaserData.explosionRadius);

                m_ChaserDataDic.Add(chaserData.type, chaserData);
            }

            m_AppearRateDic.Clear();
            m_AppearRateDic.Add(1, new ChaserAppearRateInfo());
            m_AppearRateDic.Add(2, new ChaserAppearRateInfo());
            m_AppearRateDic.Add(3, new ChaserAppearRateInfo());
            m_AppearRateDic.Add(4, new ChaserAppearRateInfo());
            m_AppearRateDic.Add(5, new ChaserAppearRateInfo());
            m_AppearRateDic.Add(6, new ChaserAppearRateInfo());

            Dictionary<ChaserData.Type, ChaserData>.Enumerator etor = m_ChaserDataDic.GetEnumerator();
            while (etor.MoveNext())
            {
                ChaserData chaserData = etor.Current.Value;
                if (chaserData.classes == ChaserData.Classes.Legal)
                {
                    for (int i = 0; i < chaserData.appearRate.Count; i++)
                    {
                        float rate = chaserData.appearRate[i];
                        if (rate == -1)
                        {
                            ChaserAppearRateInfo appearRateInfo = m_AppearRateDic[i + 1];
                            appearRateInfo.forceAppearList.Add(chaserData.type);
                        }
                        else if (rate > 0)
                        {
                            ChaserAppearRateInfo appearRateInfo = m_AppearRateDic[i + 1];
                            ChaserAppearRate chaserAppearRate = new ChaserAppearRate();
                            chaserAppearRate.startRate = appearRateInfo.sum;
                            chaserAppearRate.type = chaserData.type;
                            appearRateInfo.sum += rate;
                            chaserAppearRate.endRate = appearRateInfo.sum;
                            appearRateInfo.rateList.Add(chaserAppearRate);
                        }
                    }
                }
            }
        }        
    }
}
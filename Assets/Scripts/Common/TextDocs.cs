﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class TextDocs : Singleton<TextDocs>
{
    Dictionary<GameConfig.SupportLanguage, Dictionary<string, string>> m_TextDic = new Dictionary<GameConfig.SupportLanguage, Dictionary<string, string>>();
    public Dictionary<GameConfig.SupportLanguage, Dictionary<string, string>> textDic
    {
        get
        {
            return m_TextDic;
        }
    }

    GameConfig.SupportLanguage m_SelectLanguage = GameConfig.SupportLanguage.None;
    public GameConfig.SupportLanguage selectLanguage
    {
        get
        {
            return m_SelectLanguage;
        }
        set
        {
            m_SelectLanguage = value;
        }
    }

    #region Data Interface
    public static string content(string key)
    {
        return TextDocs.instance.getContent(key);
    }
    public string getContent(string key)
    {
        if (m_TextDic[m_SelectLanguage].ContainsKey(key))
        {
            return m_TextDic[m_SelectLanguage][key];
        }
        else
        {
            return "";
        }
    }
    #endregion

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_TextDic.Count <= 0)
        {
            Dictionary<string, string> koreanTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Korean, koreanTextDic);
            Dictionary<string, string> chineseSTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Chinese_S, chineseSTextDic);
            Dictionary<string, string> chineseTTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Chinese_T, chineseTTextDic);
            Dictionary<string, string> englishTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.English, englishTextDic);
            //Dictionary<string, string> indoneisaTextDic = new Dictionary<string, string>();
            //m_TextDic.Add(GameConfig.SupportLanguage.Indonesia, indoneisaTextDic);
            Dictionary<string, string> thaiTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Thai, thaiTextDic);
            Dictionary<string, string> japaneseTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Japanese, japaneseTextDic);
            Dictionary<string, string> spanishTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Spanish, spanishTextDic);
            Dictionary<string, string> russianTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Russian, russianTextDic);
            //Dictionary<string, string> arabicTextDic = new Dictionary<string, string>();
            //m_TextDic.Add(GameConfig.SupportLanguage.Arabic, arabicTextDic);
            Dictionary<string, string> germanTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.German, germanTextDic);
            //Dictionary<string, string> hindiTextDic = new Dictionary<string, string>();
            //m_TextDic.Add(GameConfig.SupportLanguage.Hindi, hindiTextDic);
            Dictionary<string, string> frenchTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.French, frenchTextDic);
            Dictionary<string, string> italianTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Italian, italianTextDic);
            Dictionary<string, string> portugueseTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Portuguese, portugueseTextDic);
            Dictionary<string, string> vietnameseTextDic = new Dictionary<string, string>();
            m_TextDic.Add(GameConfig.SupportLanguage.Vietnamese, vietnameseTextDic);

            Dictionary<string, object> allDataByTextTableSchema;
            GDEDataManager.GetAllDataBySchema("TextTable", out allDataByTextTableSchema);
            Dictionary<string, object>.Enumerator etor = allDataByTextTableSchema.GetEnumerator();
            while (etor.MoveNext())
            {
                Dictionary<string, object> textTableDic = etor.Current.Value as Dictionary<string, object>;
                string text = "";
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Korean], out text);
                text = text.Replace("\\n", "\n");
                koreanTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Chinese_S], out text);
                text = text.Replace("\\n", "\n");
                chineseSTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Chinese_T], out text);
                text = text.Replace("\\n", "\n");
                chineseTTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.English], out text);
                text = text.Replace("\\n", "\n");
                englishTextDic.Add(etor.Current.Key, text);
                //textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Indonesia], out text);
                //text = text.Replace("\\n", "\n");
                //indoneisaTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Thai], out text);
                text = text.Replace("\\n", "\n");
                thaiTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Japanese], out text);
                text = text.Replace("\\n", "\n");
                japaneseTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Spanish], out text);
                text = text.Replace("\\n", "\n");
                spanishTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Russian], out text);
                text = text.Replace("\\n", "\n");
                russianTextDic.Add(etor.Current.Key, text);
                //textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Arabic], out text);
                //text = text.Replace("\\n", "\n");
                //text = ArabicFixer.Fix(text, true, false);
                //arabicTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.German], out text);
                text = text.Replace("\\n", "\n");
                germanTextDic.Add(etor.Current.Key, text);
                //textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Hindi], out text);
                //text = text.Replace("\\n", "\n");
                //hindiTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.French], out text);
                text = text.Replace("\\n", "\n");
                frenchTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Italian], out text);
                text = text.Replace("\\n", "\n");
                italianTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Portuguese], out text);
                text = text.Replace("\\n", "\n");
                portugueseTextDic.Add(etor.Current.Key, text);
                textTableDic.TryGetString(GameConfig.SupportLanguageName[(int)GameConfig.SupportLanguage.Vietnamese], out text);
                text = text.Replace("\\n", "\n");
                vietnameseTextDic.Add(etor.Current.Key, text);
            }
        }
    }
}
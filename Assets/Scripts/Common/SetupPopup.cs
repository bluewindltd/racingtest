﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

public class SetupPopup : BasePopup
{
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
    enum GPCConnectRequest
    {
        NONE = 0,
        SAVE,
        LOAD,
    }
    GPCConnectRequest m_GPConnectRequest;
    bool m_RequestGPConnect = false;
#endif

    public Animator animator;
    public UILabel popupTitleLabel;
    public UILabel[] tokenLabel;
    public UILabel cashLabel;
    public UILabel saveButtonLabel;
    public UILabel loadButtonLabel;
    public UILabel audioLabel;
    public UISlider bgmVolumeSlider;
    public UISlider sfxVolumeSlider;
    public UILabel bgmLabel;
    public UILabel sfxLabel;
    public UILabel shadowButtonLabel;
    public UILabel closeButtonLabel;
    public GameObject shadowOnObject;
    public GameObject shadowOffObject;

    bool m_EnableMenu = false;
    public bool enableMenu
    {
        get
        {
            return m_EnableMenu;
        }
    }

    int m_ShadowOn = 0;

    void Start()
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        m_RequestGPConnect = false;

        GooglePlayConnection.ActionPlayerDisconnected += OnPlayerDisconnected;
        UserManager.OnGPConnectionResult += OnGPConnectionResult;

        GooglePlaySavedGamesManager.ActionNewGameSaveRequest += OnNewGameSaveRequest;
        GooglePlaySavedGamesManager.ActionGameSaveLoaded += OnGameSaveLoaded;
#elif UNITY_IOS
		iCloudManager.OnCloudDataReceivedAction += OnCloudDataReceivedAction;
#endif

        popupTitleLabel.text = TextDocs.content("SETUP_TITLE_TEXT");
        closeButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");        

        saveButtonLabel.text = TextDocs.content("BT_SAVE_TEXT");
        loadButtonLabel.text = TextDocs.content("BT_LOAD_TEXT");
        audioLabel.text = TextDocs.content("UI_VOLUME_TEXT");
        bgmLabel.text = TextDocs.content("UI_BGM_TEXT");
        sfxLabel.text = TextDocs.content("UI_FX_TEXT");
        shadowButtonLabel.text = TextDocs.content("BT_SHADOW_TEXT");

        bgmVolumeSlider.value = SoundManager.instance.GetBGMVolume();
        sfxVolumeSlider.value = SoundManager.instance.GetSFXVolume();
        
        m_ShadowOn = PlayerPrefs.GetInt("ShadowOn");
        if(m_ShadowOn == 1)
        {
            NGUITools.SetActive(shadowOnObject, true);
            NGUITools.SetActive(shadowOffObject, false);
        }
        else
        {
            NGUITools.SetActive(shadowOnObject, false);
            NGUITools.SetActive(shadowOffObject, true);
        }

        Dictionary<int, int>.Enumerator etor = UserManager.instance.tokenDic.GetEnumerator();
        while (etor.MoveNext())
        {
            if (0 <= etor.Current.Key &&
                etor.Current.Key < tokenLabel.Length)
            {
                tokenLabel[etor.Current.Key].text = string.Format("{0}", etor.Current.Value);
            }
        }
        cashLabel.text = string.Format("{0}", UserManager.instance.cash);

        animator.Play("Popup_Show");
    }

    void OnDestroy()
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        GooglePlayConnection.ActionPlayerDisconnected -= OnPlayerDisconnected;
        UserManager.OnGPConnectionResult -= OnGPConnectionResult;

        GooglePlaySavedGamesManager.ActionNewGameSaveRequest -= OnNewGameSaveRequest;
        GooglePlaySavedGamesManager.ActionGameSaveLoaded -= OnGameSaveLoaded;
#elif UNITY_IOS
		iCloudManager.OnCloudDataReceivedAction -= OnCloudDataReceivedAction;
#endif
    }

    public void EndShow()
    {
        m_EnableMenu = true;
    }

    public void EndHide()
    {
        Hide();
    }

    public static SetupPopup Show(GameObject parent, GameObject prefab)
    {
        SetupPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<SetupPopup>();

        return popup;
    }

    public void OnClickSave()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
                if (GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED)
                {
                    ShowSavedGamesUI();
                }
                else
                {
                    m_RequestGPConnect = true;
                    m_GPConnectRequest = GPCConnectRequest.SAVE;
                    ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
                    GooglePlayConnection.Instance.Connect();
                }
            }
            else
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                        TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);
            }
#elif UNITY_IOS
            ShowSavedGamesUI();
#endif
        }
    }

    private void ShowSavedGamesUI()
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        int maxNumberOfSavedGamesToShow = 1;
        GooglePlaySavedGamesManager.Instance.ShowSavedGamesUI("Save", maxNumberOfSavedGamesToShow, true, false);
#elif UNITY_IOS
		if(UserManager.instance.isCloudInit)
		{
            PopupManager.instance.ShowSmallYesNo(TextDocs.content("SAVE_QUESTION_TITLE"), TextDocs.content("SAVE_QUESTION_TEXT"),
                delegate
                {
                    iCloudManager.Instance.setString("IOS_BurnoutCity", UserManager.instance.SaveDataToCloud());

                    PopupManager.instance.ShowSmallOK(TextDocs.content("SAVE_SUCCESS_TITLE"), TextDocs.content("SAVE_SUCCESS_TEXT"),
                                                      null, null);
                }
                , null, null);            
		}
		else
		{
			PopupManager.instance.ShowSmallOK(TextDocs.content("SAVE_FAIL_TITLE"), TextDocs.content("SAVE_FAIL_TEXT"),
			                                  null, null);
		}
#endif
    }

    public void OnClickLoad()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
            if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
            {
                if (GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED)
                {
                    ShowLoadedGamesUI();
                }
                else
                {
                    m_RequestGPConnect = true;
                    m_GPConnectRequest = GPCConnectRequest.LOAD;
                    ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
                    GooglePlayConnection.Instance.Connect();
                }
            }
            else
            {
                PopupManager.instance.ShowSmallOK(TextDocs.content("NETWORK_CONNECT_TITLE"),
                        TextDocs.content("NETWORK_CONNECT_TEXT"), null, null);
            }
#elif UNITY_IOS
            ShowLoadedGamesUI();
#endif
        }
    }

    private void ShowLoadedGamesUI()
    {
//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
        int maxNumberOfSavedGamesToShow = 1;
        GooglePlaySavedGamesManager.Instance.ShowSavedGamesUI("Load", maxNumberOfSavedGamesToShow, false, false);
#elif UNITY_IOS
		if(UserManager.instance.isCloudInit)
		{
            PopupManager.instance.ShowSmallYesNo(TextDocs.content("LOAD_QUESTION_TITLE"), TextDocs.content("LOAD_QUESTION_TEXT"),
                delegate
                {
					PopupManager.instance.ShowLoadingPopup();
					iCloudManager.Instance.requestDataForKey("IOS_BurnoutCity");
                }, null, null);
		}
#endif
    }

    public void OnSliderBGMVolume()
    {
        if (m_EnableMenu)
        {
            SoundManager.instance.SetBGMVolume(bgmVolumeSlider.value);

            PlayerPrefs.SetFloat("BGMVolume", SoundManager.instance.GetBGMVolume());
        }
    }

    public void OnSliderSFXVolume()
    {
        if (m_EnableMenu)
        {
            SoundManager.instance.SetSFXVolume(sfxVolumeSlider.value);

            PlayerPrefs.SetFloat("SFXVolume", SoundManager.instance.GetSFXVolume());
        }
    }

    public void OnClickShadowOn()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            if(m_ShadowOn == 0)
            {
                NGUITools.SetActive(shadowOnObject, true);
                NGUITools.SetActive(shadowOffObject, false);

                PlayerPrefs.SetInt("ShadowOn", 1);
                m_ShadowOn = 1;

                QualitySettings.SetQualityLevel(1);

                UserManager.instance.RestoreResolution();
            }
        }
    }

    public void OnClickShadowOff()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            if (m_ShadowOn == 1)
            {
                NGUITools.SetActive(shadowOnObject, false);
                NGUITools.SetActive(shadowOffObject, true);

                PlayerPrefs.SetInt("ShadowOn", 0);
                m_ShadowOn = 0;

                QualitySettings.SetQualityLevel(0);

                UserManager.instance.ChangeLowResolution();
            }
        }
    }

    public void OnClickGetTokenHelp()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();
            PopupManager.instance.ShowLargeOK(TextDocs.content("UI_TOKEN_TEXT2"), TextDocs.content("GET_TOKEN_HELP_TEXT"), null, null);
        }
    }

    public void OnClickExit()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EnableMenu = false;

            animator.Play("Popup_Hide");
        }
    }

//#if UNITY_EDITOR_WIN || UNITY_EDITOR
#if UNITY_ANDROID
    #region Android Cloud Save/Load Function
    void OnPlayerDisconnected()
    {
        Debug.Log("Player Disconnected");
    }

    void OnGPConnectionResult(bool isSuccess)
    {
        Debug.Log("SetupPopup ConnectionResult :  " + isSuccess);

        if (isSuccess)
        {
            Debug.Log("StageSideMenu Connected!");

            if (m_RequestGPConnect)
            {
                m_RequestGPConnect = false;

                if (m_GPConnectRequest == GPCConnectRequest.SAVE)
                {
                    ShowSavedGamesUI();
                }
                else if (m_GPConnectRequest == GPCConnectRequest.LOAD)
                {
                    ShowLoadedGamesUI();
                }
            }            
        }
        else
        {
            Debug.Log("SetupPopup Connection failed with code : " + isSuccess);
        }
    }

    private IEnumerator MakeScreenshotAndSaveGameData()
    {
        yield return new WaitForEndOfFrame();
        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        Texture2D Screenshot = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        Screenshot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        Screenshot.Apply();

        long TotalPlayedTime = GameApplication.instance.playTime * 1000;
        //string currentSaveName =  "snapshotTemp-" + Random.Range(1, 281).ToString();
        string currentSaveName = "AOS_BURNOUTCITY";
        string description = System.DateTime.Now.ToString("MM/dd/yyyy H:mm:ss");

        GooglePlaySavedGamesManager.ActionGameSaveResult += OnGameSaveResult;
        string saveJson = UserManager.instance.SaveDataToCloud();
        GooglePlaySavedGamesManager.Instance.CreateNewSnapshot(currentSaveName, description, Screenshot, saveJson, TotalPlayedTime);

        Destroy(Screenshot);
    }

    // SaveGameUI에서 Save Game 버튼을 눌렀을 때 호출 된다.
    private void OnNewGameSaveRequest()
    {
        Debug.Log("New Game Save Requested, Creating new save..");

        StartCoroutine(MakeScreenshotAndSaveGameData());

        PopupManager.instance.ShowLoadingPopup();
    }

    // Save 완료시 호출 된다.
    private void OnGameSaveResult(GP_SpanshotLoadResult result)
    {
        PopupManager.instance.HideLoadingPopup();

        GooglePlaySavedGamesManager.ActionGameSaveResult -= OnGameSaveResult;
        Debug.Log("OnGameSaveResult : " + result.Message);

        if (result.IsSucceeded)
        {
            Debug.Log("Games Saved: " + result.Snapshot.meta.Title);

            PopupManager.instance.ShowSmallOK(TextDocs.content("SAVE_SUCCESS_TITLE"), TextDocs.content("SAVE_SUCCESS_TEXT"),
                    null, null);
        }
        else
        {
            Debug.Log("Games Save Failed");

            PopupManager.instance.ShowSmallOK(TextDocs.content("SAVE_FAIL_TITLE"), TextDocs.content("SAVE_FAIL_TEXT"),
                        null, null);
        }
    }

    // SaveGameUI에서 Load 할 Snapshot을 선택 했을 때 호출 된다.
    private void OnGameSaveLoaded(GP_SpanshotLoadResult result)
    {
        Debug.Log("OnGameSaveLoaded: " + result.Message);
        if (result.IsSucceeded)
        {
            Debug.Log("Snapshot.Title: " + result.Snapshot.meta.Title);
            Debug.Log("Snapshot.Description: " + result.Snapshot.meta.Description);
            Debug.Log("Snapshot.CoverImageUrl): " + result.Snapshot.meta.CoverImageUrl);
            Debug.Log("Snapshot.LastModifiedTimestamp: " + result.Snapshot.meta.LastModifiedTimestamp);

            Debug.Log("Snapshot.stringData: " + result.Snapshot.stringData);
            Debug.Log("Snapshot.bytes.Length: " + result.Snapshot.bytes.Length);

            UserManager.instance.LoadDataToCloud(result.Snapshot.stringData);
        }
    }

    //private void OnConflict(GP_SnapshotConflict result)
    //{
    //    Debug.Log("Conflict Detected: ");

    //    GP_Snapshot snapshot = result.Snapshot;
    //    GP_Snapshot conflictSnapshot = result.ConflictingSnapshot;

    //    // Resolve between conflicts by selecting the newest of the conflicting snapshots.
    //    GP_Snapshot mResolvedSnapshot = snapshot;

    //    if (snapshot.meta.LastModifiedTimestamp < conflictSnapshot.meta.LastModifiedTimestamp)
    //    {
    //        mResolvedSnapshot = conflictSnapshot;
    //    }

    //    result.Resolve(mResolvedSnapshot);
    //}
    #endregion
#elif UNITY_IOS
	void OnCloudDataReceivedAction(iCloudData data)
	{
		PopupManager.instance.HideLoadingPopup ();

		if(data.IsEmpty)
		{
			//IOSNativePopUpManager.showMessage(data.key, "data is empty");
		}
		else
		{
			//IOSNativePopUpManager.showMessage(data.key, data.stringValue);

			UserManager.instance.LoadDataToCloud(data.stringValue);
		}
	}
#endif
}

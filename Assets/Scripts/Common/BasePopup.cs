﻿using UnityEngine;
using System.Collections;

public class BasePopup : MonoBehaviour
{
    bool _cancelable = true;
    public bool cancelable
    {
        get
        {
            return _cancelable;
        }

        set
        {
            _cancelable = value;
        }
    }

    public delegate void CancelDelegate();
    public CancelDelegate OnCancel = delegate () { };

    public void SetCancelable(bool val)
    {
        _cancelable = val;
    }

    public void Hide()
    {
        PopupManager.instance.Hide(this);
    }
}

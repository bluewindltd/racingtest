﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class GiftRateInfo
{
    public int index;
    public int startRate;
    public int endRate;
    public int minAmount;
    public int maxAmount;
}

public class GiftDocs : Singleton<GiftDocs>
{
    List<GiftRateInfo> m_GiftRateInfoList = new List<GiftRateInfo>();
    public List<GiftRateInfo> giftRateInfoList
    {
        get
        {
            return m_GiftRateInfoList;
        }
    }

    int m_GiftRateSum = 0;
    public int giftRateSum
    {
        get
        {
            return m_GiftRateSum;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if(m_GiftRateInfoList.Count <= 0)
        {
            int index = 0;
            int rate = 0;
            int minAmount = 0;
            int maxAmount = 0;
            m_GiftRateSum = 0;
            Dictionary<string, object> allDataByGiftTableSchema;
            GDEDataManager.GetAllDataBySchema("GiftTable", out allDataByGiftTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByGiftTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> giftTableDic = etorTable.Current.Value as Dictionary<string, object>;

                GiftRateInfo giftRateInfo = new GiftRateInfo();

                giftTableDic.TryGetInt("index", out index);
                giftTableDic.TryGetInt("rate", out rate);
                giftTableDic.TryGetInt("minAmount", out minAmount);
                giftTableDic.TryGetInt("maxAmount", out maxAmount);

                giftRateInfo.index = index;
                giftRateInfo.startRate = m_GiftRateSum;
                m_GiftRateSum += rate;
                giftRateInfo.endRate = m_GiftRateSum;
                giftRateInfo.minAmount = minAmount;
                giftRateInfo.maxAmount = maxAmount;

                m_GiftRateInfoList.Add(giftRateInfo);
            }
        }        
    }

    public void PlayGift(ref int index, ref int amount)
    {
        GiftRateInfo giftRateInfo;
        index = -1;
        amount = 0;
        int giftRate = UnityEngine.Random.Range(0, m_GiftRateSum);
        for (int i = 0; i < m_GiftRateInfoList.Count; i++)
        {
            giftRateInfo = m_GiftRateInfoList[i];
            if (giftRateInfo.startRate <= giftRate &&
                giftRate < giftRateInfo.endRate)
            {
                index = giftRateInfo.index;
                if(index != 4)
                {
                    amount = UnityEngine.Random.Range(giftRateInfo.minAmount, giftRateInfo.maxAmount + 1);
                }                
                break;
            }
        }
    }
}

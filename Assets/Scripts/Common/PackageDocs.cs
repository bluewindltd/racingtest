﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class PackageStockData
{
    public GameConfig.RewardType type;
    public int value;
}

public class PackageData
{
    public string name = "";
    public string desc = "";
    public int index = 0;
    public string googleID = "";
    public string appleID = "";
    public string recordPrice = "";
    public string tempPrice = "";
    public string stockPrice = "";
    public List<PackageStockData> stockList = new List<PackageStockData>();
}

public class PackageDocs : Singleton<PackageDocs>
{
    private Dictionary<int, PackageData> m_PackageDataDic = new Dictionary<int, PackageData>();
    public Dictionary<int, PackageData> packageDataDic
    {
        get
        {
            return m_PackageDataDic;
        }
    }

    private bool m_IsUpdateProduct = false;
    public bool isUpdateProduct
    {
        get
        {
            return m_IsUpdateProduct;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_PackageDataDic.Count <= 0)
        {
            Dictionary<string, object> allDataByPackageTableSchema;
            GDEDataManager.GetAllDataBySchema("PackageTable", out allDataByPackageTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByPackageTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> packageTableDic = etorTable.Current.Value as Dictionary<string, object>;

                PackageData packageData = new PackageData();

                packageTableDic.TryGetString("name", out packageData.name);
                packageTableDic.TryGetInt("index", out packageData.index);
                packageTableDic.TryGetString("googleID", out packageData.googleID);
                packageTableDic.TryGetString("appleID", out packageData.appleID);
                packageTableDic.TryGetString("recordPrice", out packageData.recordPrice);
                packageTableDic.TryGetString("tempPrice", out packageData.tempPrice);
                for(int i = 0; i < 3; i++)
                {
                    int type = 0;
                    packageTableDic.TryGetInt(string.Format("pack_{0}_param0", i), out type);
                    if (type != -1)
                    {
                        PackageStockData packageStockData = new PackageStockData();
                        packageStockData.type = (GameConfig.RewardType)type;
                        packageTableDic.TryGetInt(string.Format("pack_{0}_param1", i), out packageStockData.value);

                        packageData.stockList.Add(packageStockData);
                    }
                }

                if (packageData.googleID == null)
                {
                    packageData.googleID = "";
                }
                if (packageData.appleID == null)
                {
                    packageData.appleID = "";
                }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
                if (packageData.googleID.CompareTo("") != 0)
                {
                    Debug.Log("@@ Package AddProduct : " + packageData.googleID);
                    AndroidInAppPurchaseManager.Instance.AddProduct(packageData.googleID);
                }
#elif UNITY_IOS
                if (packageData.appleID.CompareTo("") != 0)
                {
				    Debug.Log("@@ Package AddProduct : " + packageData.appleID);
				    IOSInAppPurchaseManager.Instance.AddProductId(packageData.appleID);
                }
#endif

                m_PackageDataDic.Add(packageData.index, packageData);
            }
        }
    }

    public PackageData GetPackageForGoogle(string googleID)
    {
        PackageData packageData = null;
        Dictionary<int, PackageData>.Enumerator etor = m_PackageDataDic.GetEnumerator();

        while (etor.MoveNext())
        {
            if (etor.Current.Value.googleID.CompareTo(googleID) == 0)
            {
                packageData = etor.Current.Value;
            }
        }

        return packageData;
    }

    public PackageData GetPackageForApple(string appleID)
    {
        PackageData packageData = null;
        Dictionary<int, PackageData>.Enumerator etor = m_PackageDataDic.GetEnumerator();

        while (etor.MoveNext())
        {
            if (etor.Current.Value.appleID.CompareTo(appleID) == 0)
            {
                packageData = etor.Current.Value;
            }
        }

        return packageData;
    }

    public void UpdateProduct()
    {
        if (m_IsUpdateProduct == false)
        {
            m_IsUpdateProduct = true;
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
            PackageData packageData = null;
            Dictionary<int, PackageData>.Enumerator etor = m_PackageDataDic.GetEnumerator();
            while(etor.MoveNext())
            {
                packageData = etor.Current.Value;
                if(packageData != null)
                {
                    GoogleProductTemplate product = AndroidInAppPurchaseManager.Client.Inventory.GetProductDetails(packageData.googleID);
                    if (product != null)
                    {
                        packageData.stockPrice = product.LocalizedPrice;
                    }
                }
            }
#elif UNITY_IOS
			int testCount = 0;
			PackageData packageData = null;
			Dictionary<int, PackageData>.Enumerator etor = m_PackageDataDic.GetEnumerator();
			while(etor.MoveNext())
			{
				packageData = etor.Current.Value;
				if(packageData != null)
				{
					IOSProductTemplate product = IOSInAppPurchaseManager.Instance.GetProductById(packageData.appleID);					
					if(product != null)
					{
						Debug.Log("@@ IOSProductTemplate " + testCount + " Success : " + packageData.appleID + ", " + product.DisplayName + ", " + product.LocalizedPrice + ", " + product.Id);
						packageData.stockPrice = product.LocalizedPrice;
					}
					else
					{
						Debug.Log("@@ IOSProductTemplate " + testCount + " Failed");
					}
				}

				testCount++;
			}
#endif
        }
    }
}

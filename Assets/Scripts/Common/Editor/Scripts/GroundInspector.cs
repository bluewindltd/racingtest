﻿using UnityEditor;
using UnityEngine;
using System.Collections;

using System;
using System.IO;
using System.Text;

using SimpleJSON;

[CustomEditor(typeof(Ground))]
public class GroundInspector : Editor
{
    private Ground ground;

    void OnEnable()
    {
        ground = (Ground)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        bool errorFound = false;

        GUI.changed = false;
        Undo.RecordObject(target, "GroundChanged");

        //ground.groundType = (GameConfig.GroundType)EditorGUILayout.EnumPopup("Ground Type", ground.groundType);
        ground.theme = EditorGUILayout.IntField("Theme", ground.theme);
        ground.objectRoot = (GameObject)EditorGUILayout.ObjectField("Object Root", ground.objectRoot, typeof(GameObject), true);
        ground.objectRootTransform = (Transform)EditorGUILayout.ObjectField("Object Root Transform", ground.objectRootTransform, typeof(Transform), true);

        GUILayout.Space(5);
        GUILayout.Box("", GUILayout.Height(1), GUILayout.ExpandWidth(true));
        GUILayout.Space(5);

        if (GUILayout.Button("Clear MapData", GUILayout.ExpandWidth(true)))
        {
            CommonUtil.DestroyChildrenEditor(ground.objectRoot.transform);
        }

        GUILayout.Space(5);
        GUILayout.Box("", GUILayout.Height(1), GUILayout.ExpandWidth(true));
        GUILayout.Space(5);

        if (GUILayout.Button("Load MapData", GUILayout.ExpandWidth(true)))
        {
            string loadPath = EditorUtility.OpenFilePanel("Load MapData", "Assets\\Resources\\MapData", "bytes");
            if(loadPath.CompareTo("") != 0)
            {
                ground.LoadMapData(loadPath, true, false);
            }
        }

        if (errorFound) GUI.enabled = false;
        if (GUILayout.Button("Save MapData", GUILayout.ExpandWidth(true)))
        {
            string savePath = EditorUtility.SaveFilePanel("Save MapData", "Assets\\Resources\\MapData", string.Format("ground_{0}_{1}_0", ground.theme, (int)ground.groundType), "bytes");
            if (ground.objectRoot != null && savePath.CompareTo("") != 0)
            {
                JSONClass rootNode = new JSONClass();
                JSONClass jsonMapData = new JSONClass();
                rootNode.Add("map_data", jsonMapData);

                //jsonMapData.Add("groundType", new JSONData((int)ground.groundType));
                //jsonMapData.Add("theme", new JSONData(ground.theme));

                JSONArray jsonObjectList = new JSONArray();
                jsonMapData.Add("object_list", jsonObjectList);
                Transform objectRootTransform = ground.objectRoot.transform;
                for (int i = 0; i < objectRootTransform.childCount; i++)
                {
                    Transform objectTransform = objectRootTransform.GetChild(i);
                    MapObject mapObject = objectTransform.gameObject.GetComponent<MapObject>();

                    JSONClass jsonObjectClass = new JSONClass();

                    jsonObjectClass.Add("theme", new JSONData(mapObject.theme));
                    jsonObjectClass.Add("index", new JSONData(mapObject.index));
                    jsonObjectClass.Add("pos_x", new JSONData(objectTransform.localPosition.x));
                    jsonObjectClass.Add("pos_y", new JSONData(objectTransform.localPosition.y));
                    jsonObjectClass.Add("pos_z", new JSONData(objectTransform.localPosition.z));
                    jsonObjectClass.Add("rot_x", new JSONData(objectTransform.localEulerAngles.x));
                    jsonObjectClass.Add("rot_y", new JSONData(objectTransform.localEulerAngles.y));
                    jsonObjectClass.Add("rot_z", new JSONData(objectTransform.localEulerAngles.z));
                    jsonObjectClass.Add("sc_x", new JSONData(objectTransform.localScale.x));
                    jsonObjectClass.Add("sc_y", new JSONData(objectTransform.localScale.y));
                    jsonObjectClass.Add("sc_z", new JSONData(objectTransform.localScale.z));
                    jsonObjectList.Add(jsonObjectClass);
                }

                //System.IO.File.WriteAllText(@savePath, Convert.ToBase64String(Encoding.UTF8.GetBytes(rootNode.ToString())));
                System.IO.File.WriteAllText(@savePath, rootNode.ToString());
            }
        }
        //GUI.enabled = true;

        if (GUI.changed)
        {
            EditorUtility.SetDirty(ground);
        }
        serializedObject.ApplyModifiedProperties();
    }
}

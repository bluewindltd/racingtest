﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

[CustomEditor(typeof(NPCManager))]
public class NPCManagerInspector : Editor
{
    private NPCManager npcManager;

    private Texture2D addButton;
    private Texture2D removeButton;

    private GUIStyle addNPCCarLabelStyle;

    private static bool npcCarsFoldout = true;

    private void OnEnable()
    {
        npcManager = (NPCManager)target;

        addButton = InspectorUtils.getAsset<Texture2D>("AddButton t:texture2D");
        removeButton = InspectorUtils.getAsset<Texture2D>("RemoveButton t:texture2D");
    }

    private void initStyle()
    {
        addNPCCarLabelStyle = new GUIStyle(GUI.skin.label);
        addNPCCarLabelStyle.normal.textColor = Color.magenta;
    }

    public override void OnInspectorGUI()
    {
        if (addNPCCarLabelStyle == null)
        {
            initStyle();
        }

        serializedObject.Update();

        GUI.changed = false;
        Undo.RecordObject(target, "NPCManagerChanged");

        npcCarsFoldout = EditorGUILayout.Foldout(npcCarsFoldout, "NPC Cars");
        if (npcCarsFoldout)
        {
            drawNPCCars();
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(npcManager);
        }
        serializedObject.ApplyModifiedProperties();
    }

    private void drawNPCCars()
    {
        GUILayout.BeginVertical();
        {
            //drawTheme();
            DictionaryOfNPCCarCreator npcCarDic = npcManager.npcCarCreatorDic;
            GUILayout.BeginHorizontal();
            {
                GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
                {
                    if (!npcCarDic.ContainsKey(npcCarDic.Count))
                    {
                        NPCCarCreator npcCarCreator = new NPCCarCreator();
                        npcCarDic.Add(npcCarDic.Count, npcCarCreator);
                    }
                }
                GUILayout.Label("Add NPC Car", addNPCCarLabelStyle, GUILayout.Width(150f));
            }
            GUILayout.EndHorizontal();

            if (npcCarDic.Count > 0)
            {
                foreach (var key2 in npcCarDic.Keys.ToList())
                {
                    NPCCarCreator npcCarCreator = npcCarDic[key2];
                    bool removeNPCCar = false;
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                        removeNPCCar = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                        GUILayout.Label(string.Format("{0} NPC Car", key2), GUILayout.Width(150f));
                    }
                    GUILayout.EndHorizontal();

                    EditorGUI.indentLevel++;
                    npcCarCreator.npcCarPrefab = (GameObject)EditorGUILayout.ObjectField("NPC Car Prefab", npcCarCreator.npcCarPrefab, typeof(GameObject), true);
                    if (npcCarCreator.npcCarPrefab != null)
                    {
                        //MapObject mapObject = npcCarCreator.npcCarPrefab.GetComponent<>();
                        //mapObjectCreator.mapObjectType = mapObject.objectType;
                    }
                    npcCarCreator.objectPoolCount = EditorGUILayout.IntField("NPC Car PoolCount", npcCarCreator.objectPoolCount);
                    EditorGUI.indentLevel--;

                    if (removeNPCCar)
                    {
                        npcCarDic.Remove(key2);
                    }
                }
            }
        }
        GUILayout.EndVertical();
    }
}

﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

[CustomEditor(typeof(MapObjectManager))]
public class MapObjectManagerInspector : Editor
{
    private MapObjectManager mapObjectManager;

    private Texture2D addButton;
    private Texture2D removeButton;

    private GUIStyle addThemeLabelStyle;
    private GUIStyle addMapObjectLabelStyle;

    private static bool mapObjectsFoldout = true;
    private static Dictionary<int, bool> mapObjectThemeFoldoutDic = new Dictionary<int, bool>();

    private void OnEnable()
    {
        mapObjectManager = (MapObjectManager)target;

        addButton = InspectorUtils.getAsset<Texture2D>("AddButton t:texture2D");
        removeButton = InspectorUtils.getAsset<Texture2D>("RemoveButton t:texture2D");
    }

    private void initStyle()
    {
        addThemeLabelStyle = new GUIStyle(GUI.skin.label);
        addThemeLabelStyle.normal.textColor = Color.red;

        addMapObjectLabelStyle = new GUIStyle(GUI.skin.label);
        addMapObjectLabelStyle.normal.textColor = Color.magenta;
    }

    public override void OnInspectorGUI()
    {
        if(addThemeLabelStyle == null || addMapObjectLabelStyle == null)
        {
            initStyle();
        }

        serializedObject.Update();

        GUI.changed = false;
        Undo.RecordObject(target, "MapObjectManagerChanged");

        mapObjectManager.missionPointObject.mapObjectPrefab = (GameObject)EditorGUILayout.ObjectField("MissionPoint Prefab", mapObjectManager.missionPointObject.mapObjectPrefab, typeof(GameObject), true);
        if (mapObjectManager.missionPointObject.mapObjectPrefab != null)
        {
            MapObject mapObject = mapObjectManager.missionPointObject.mapObjectPrefab.GetComponent<MapObject>();
            mapObjectManager.missionPointObject.mapObjectType = mapObject.objectType;
        }

        mapObjectManager.itemPointObject.mapObjectPrefab = (GameObject)EditorGUILayout.ObjectField("ItemPoint Prefab", mapObjectManager.itemPointObject.mapObjectPrefab, typeof(GameObject), true);
        if (mapObjectManager.itemPointObject.mapObjectPrefab != null)
        {
            MapObject mapObject = mapObjectManager.itemPointObject.mapObjectPrefab.GetComponent<MapObject>();
            mapObjectManager.itemPointObject.mapObjectType = mapObject.objectType;
        }

        mapObjectsFoldout = EditorGUILayout.Foldout(mapObjectsFoldout, "Themes");
        if (mapObjectsFoldout)
        {
            drawMapObjects();
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(mapObjectManager);
        }
        serializedObject.ApplyModifiedProperties();
    }

    private void drawMapObjects()
    {
        GUILayout.BeginVertical();
        {
            drawTheme();
        }
        GUILayout.EndVertical();
    }

    private void drawTheme()
    {
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
            {
                mapObjectManager.AddTheme(mapObjectManager.themeDic.Count);
            }
            
            GUILayout.Label("Add Theme", addThemeLabelStyle, GUILayout.Width(150f));
        }
        GUILayout.EndHorizontal();

        if (mapObjectManager.themeDic.Count > 0)
        {            
            foreach (var key in mapObjectManager.themeDic.Keys.ToList())
            {
                bool removeTheme = false;
                GUILayout.BeginHorizontal();
                {
                    removeTheme = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                    GUILayout.Label(string.Format("{0} Theme", key), GUILayout.Width(150f));
                }
                GUILayout.EndHorizontal();

                if(!mapObjectThemeFoldoutDic.ContainsKey(key))
                {
                    mapObjectThemeFoldoutDic.Add(key, true);
                }

                EditorGUI.indentLevel++;
                mapObjectThemeFoldoutDic[key] = EditorGUILayout.Foldout(mapObjectThemeFoldoutDic[key], "MapObjects");
                if (mapObjectThemeFoldoutDic[key])
                {
                    DictionaryOfMapObjectCreator mapObjectCreatorDic = mapObjectManager.themeDic[key];                    
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                        if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
                        {
                            if (!mapObjectCreatorDic.ContainsKey(mapObjectCreatorDic.Count))
                            {
                                MapObjectCreator mapObjectCreator = new MapObjectCreator();
                                mapObjectCreatorDic.Add(mapObjectCreatorDic.Count, mapObjectCreator);
                            }
                        }
                        GUILayout.Label("Add MapObject", addMapObjectLabelStyle, GUILayout.Width(150f));
                    }
                    GUILayout.EndHorizontal();

                    if (mapObjectCreatorDic.Count > 0)
                    {
                        foreach (var key2 in mapObjectCreatorDic.Keys.ToList())
                        {
                            MapObjectCreator mapObjectCreator = mapObjectCreatorDic[key2];
                            bool removeMapObject = false;
                            GUILayout.BeginHorizontal();
                            {
                                GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                                removeMapObject = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                                GUILayout.Label(string.Format("{0} MapObject", key2), GUILayout.Width(150f));
                            }
                            GUILayout.EndHorizontal();

                            EditorGUI.indentLevel++;
                            mapObjectCreator.mapObjectPrefab = (GameObject)EditorGUILayout.ObjectField("MapObject Prefab", mapObjectCreator.mapObjectPrefab, typeof(GameObject), true);
                            if (mapObjectCreator.mapObjectPrefab != null)
                            {
                                MapObject mapObject = mapObjectCreator.mapObjectPrefab.GetComponent<MapObject>();
                                mapObjectCreator.mapObjectType = mapObject.objectType;
                            }
                            //mapObjectCreator.objectPoolRoot = (Transform)EditorGUILayout.ObjectField("MapObject Pool Root", mapObjectCreator.objectPoolRoot, typeof(Transform), true);
                            //mapObjectCreator.objectPoolCount = EditorGUILayout.IntField("MapObject PoolCount", mapObjectCreator.objectPoolCount);
                            EditorGUI.indentLevel--; 

                            if (removeMapObject)
                            {
                                mapObjectCreatorDic.Remove(key2);
                            }
                        }                        
                    }
                }
                EditorGUI.indentLevel--;

                if (removeTheme)
                {
                    mapObjectManager.themeDic.Remove(key);
                }
            }
        }
        else
        {
            GUILayout.BeginHorizontal();
            {
                //GUILayout.Space(20);
                //GameObject gameObject = (GameObject)EditorGUILayout.ObjectField(null, typeof(GameObject), true, GUILayout.MinWidth(80));
                //if (gameObject != null) piece.geometryList.Add(gameObject);
            }
            GUILayout.EndHorizontal();
        }
    }
}

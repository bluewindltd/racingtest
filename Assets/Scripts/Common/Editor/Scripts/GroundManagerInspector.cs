﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

[CustomEditor(typeof(GroundManager))]
public class GroundManagerInspector : Editor
{
    private GroundManager groundManager;

    private Texture2D addButton;
    private Texture2D removeButton;

    private GUIStyle addThemeLabelStyle;
    private GUIStyle addMapObjectLabelStyle;

    private SerializedProperty groundPrefabs;
    private SerializedProperty groundParents;

    private SerializedProperty groundPoolCounts;

    private static bool groundCountsFoldout = true;
    private static Dictionary<int, bool> groundCountThemeFoldoutDic = new Dictionary<int, bool>();

    private static bool straightGroundPoolObjectsFoldout = true;
    private static Dictionary<int, bool> straightGroundPoolObjectThemeFoldoutDic = new Dictionary<int, bool>();
    private static bool cornerGroundPoolObjectsFoldout = true;
    private static Dictionary<int, bool> cornerGroundPoolObjectThemeFoldoutDic = new Dictionary<int, bool>();
    private static bool pieceGroundPoolObjectsFoldout = true;
    private static Dictionary<int, bool> pieceGroundPoolObjectThemeFoldoutDic = new Dictionary<int, bool>();

    private void OnEnable()
    {
        groundPrefabs = serializedObject.FindProperty("groundPrefabs");
        groundParents = serializedObject.FindProperty("groundParents");
        groundPoolCounts = serializedObject.FindProperty("groundPoolCounts");

        groundManager = (GroundManager)target;

        addButton = InspectorUtils.getAsset<Texture2D>("AddButton t:texture2D");
        removeButton = InspectorUtils.getAsset<Texture2D>("RemoveButton t:texture2D");
    }

    private void initStyle()
    {
        addThemeLabelStyle = new GUIStyle(GUI.skin.label);
        addThemeLabelStyle.normal.textColor = Color.red;

        addMapObjectLabelStyle = new GUIStyle(GUI.skin.label);
        addMapObjectLabelStyle.normal.textColor = Color.magenta;
    }

    public override void OnInspectorGUI()
    {
        if (addThemeLabelStyle == null || addMapObjectLabelStyle == null)
        {
            initStyle();
        }

        serializedObject.Update();

        GUI.changed = false;
        Undo.RecordObject(target, "GroundManagerChanged");

        groundCountsFoldout = EditorGUILayout.Foldout(groundCountsFoldout, "Themes");
        if (groundCountsFoldout)
        {
            drawGroundCounts();
        }
        
        EditorGUILayout.PropertyField(groundPrefabs, true);
        EditorGUILayout.PropertyField(groundParents, true);
        EditorGUILayout.PropertyField(groundPoolCounts, true);

        GUILayout.Space(5);
        GUILayout.Box("", GUILayout.Height(1), GUILayout.ExpandWidth(true));
        GUILayout.Space(5);

        if (GUILayout.Button("Make Ground Prefabs", GUILayout.ExpandWidth(true)))
        {
            MakeGroundPrefabs();
            //groundManager.MakeGroundPrefabs();
        }

        GUILayout.Space(5);
        GUILayout.Box("", GUILayout.Height(1), GUILayout.ExpandWidth(true));
        GUILayout.Space(5);

        straightGroundPoolObjectsFoldout = EditorGUILayout.Foldout(straightGroundPoolObjectsFoldout, "Straight GroundPoolObject Themes");
        if (straightGroundPoolObjectsFoldout)
        {
            drawStraightGroundPoolObjects();
        }
        cornerGroundPoolObjectsFoldout = EditorGUILayout.Foldout(cornerGroundPoolObjectsFoldout, "Corner GroundPoolObject Themes");
        if (cornerGroundPoolObjectsFoldout)
        {
            drawCornerGroundPoolObjects();
        }
        pieceGroundPoolObjectsFoldout = EditorGUILayout.Foldout(pieceGroundPoolObjectsFoldout, "Piece GroundPoolObject Themes");
        if (pieceGroundPoolObjectsFoldout)
        {
            drawPieceGroundPoolObjects();
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(groundManager);
        }
        serializedObject.ApplyModifiedProperties();

        EditorGUIUtility.LookLikeControls();
    }

    private void MakeGroundPrefabs()
    {
        int theme = 0;
        string mapDataName = "";
        string groundName = "";
        MapData mapData = null;
        Ground ground = null;
        
        DictionaryOfGroundCountTheme.Enumerator etor = groundManager.groundCountDic.GetEnumerator();
        // 테마별로 3가지 종류의 Ground가 몇개씩 있는지 순회
        while (etor.MoveNext())
        {
            theme = etor.Current.Key;

            GroundCountInfo groundCountInfo = etor.Current.Value;
            int straightPoolCount = Mathf.CeilToInt((float)groundManager.groundPoolCounts[0] / (float)groundCountInfo.straightGroundCount);
            for (int i = 0; i < groundCountInfo.straightGroundCount; i++)
            {
                mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", theme, 0, i);
                groundName = string.Format("ground_{0}_{1}_{2}", theme, 0, i);
                mapData = groundManager.LoadMapData(mapDataName);
                if (mapData != null)
                {
                    groundManager.mapDataDic.Add(mapDataName, mapData);
                }

                GameObject groundStraightObject = (GameObject)MonoBehaviour.Instantiate(groundManager.groundPrefabs[0], Vector3.zero, Quaternion.identity);
                ground = groundStraightObject.GetComponent<Ground>();
                ground.LoadMapData(mapDataName, false, false);

                UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/Map/grounds/" + groundName + ".prefab");
                PrefabUtility.ReplacePrefab(groundStraightObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

                DestroyImmediate(groundStraightObject);
            }

            int cornerPoolCount = Mathf.CeilToInt((float)groundManager.groundPoolCounts[1] / (float)groundCountInfo.cornerGroundCount);
            for (int i = 0; i < groundCountInfo.cornerGroundCount; i++)
            {
                mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", theme, 1, i);
                groundName = string.Format("ground_{0}_{1}_{2}", theme, 1, i);
                mapData = groundManager.LoadMapData(mapDataName);
                if (mapData != null)
                {
                    groundManager.mapDataDic.Add(mapDataName, mapData);
                }

                GameObject groundCornerObject = (GameObject)MonoBehaviour.Instantiate(groundManager.groundPrefabs[1], Vector3.zero, Quaternion.identity);
                ground = groundCornerObject.GetComponent<Ground>();
                ground.LoadMapData(mapDataName, false, false);

                UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/Map/grounds/" + groundName + ".prefab");
                PrefabUtility.ReplacePrefab(groundCornerObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

                DestroyImmediate(groundCornerObject);
            }

            int piecePoolCount = Mathf.CeilToInt((float)groundManager.groundPoolCounts[2] / (float)groundCountInfo.pieceGroundCount);
            for (int i = 0; i < groundCountInfo.pieceGroundCount; i++)
            {
                mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", theme, 2, i);
                groundName = string.Format("ground_{0}_{1}_{2}", theme, 2, i);
                mapData = groundManager.LoadMapData(mapDataName);
                if (mapData != null)
                {
                    groundManager.mapDataDic.Add(mapDataName, mapData);
                }

                GameObject groundPieceObject = (GameObject)MonoBehaviour.Instantiate(groundManager.groundPrefabs[2], Vector3.zero, Quaternion.identity);
                ground = groundPieceObject.GetComponent<Ground>();
                ground.LoadMapData(mapDataName, false, false);

                UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/Map/grounds/" + groundName + ".prefab");
                PrefabUtility.ReplacePrefab(groundPieceObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

                DestroyImmediate(groundPieceObject);
            }
        }
    }

    private void drawGroundCounts()
    {
        GUILayout.BeginVertical();
        {
            drawGroundCountTheme();
        }
        GUILayout.EndVertical();
    }

    private void drawGroundCountTheme()
    {
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
            {
                int addIndex = groundManager.groundCountDic.Count;
                if(!groundManager.groundCountDic.ContainsKey(addIndex))
                {
                    GroundCountInfo groundCountInfo = new GroundCountInfo();
                    groundManager.groundCountDic.Add(addIndex, groundCountInfo);
                }
            }

            GUILayout.Label("Add Theme", addThemeLabelStyle, GUILayout.Width(150f));
        }
        GUILayout.EndHorizontal();

        if (groundManager.groundCountDic.Count > 0)
        {
            foreach (var key in groundManager.groundCountDic.Keys.ToList())
            {
                bool removeTheme = false;
                GUILayout.BeginHorizontal();
                {
                    removeTheme = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                    GUILayout.Label(string.Format("{0} Theme", key), GUILayout.Width(150f));
                }
                GUILayout.EndHorizontal();

                if (!groundCountThemeFoldoutDic.ContainsKey(key))
                {
                    groundCountThemeFoldoutDic.Add(key, true);
                }

                EditorGUI.indentLevel++;
                groundCountThemeFoldoutDic[key] = EditorGUILayout.Foldout(groundCountThemeFoldoutDic[key], "GroundTypes");
                if (groundCountThemeFoldoutDic[key])
                {
                    GroundCountInfo groundCountInfo = groundManager.groundCountDic[key];
                    groundCountInfo.straightGroundCount = EditorGUILayout.IntField("Straight", groundCountInfo.straightGroundCount);
                    groundCountInfo.cornerGroundCount = EditorGUILayout.IntField("Corner", groundCountInfo.cornerGroundCount);
                    groundCountInfo.pieceGroundCount = EditorGUILayout.IntField("Piece", groundCountInfo.pieceGroundCount);
                }
                EditorGUI.indentLevel--;

                if (removeTheme)
                {
                    groundManager.groundCountDic.Remove(key);
                }
            }
        }
        else
        {
            GUILayout.BeginHorizontal();
            {
                //GUILayout.Space(20);
                //GameObject gameObject = (GameObject)EditorGUILayout.ObjectField(null, typeof(GameObject), true, GUILayout.MinWidth(80));
                //if (gameObject != null) piece.geometryList.Add(gameObject);
            }
            GUILayout.EndHorizontal();
        }
    }

    private void drawStraightGroundPoolObjects()
    {
        GUILayout.BeginVertical();
        {
            drawStraightGroundPoolObjectTheme();
        }
        GUILayout.EndVertical();
    }

    private void drawStraightGroundPoolObjectTheme()
    {
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
            {
                int addIndex = groundManager.straightPoolPrefabDic.Count;
                if (!groundManager.straightPoolPrefabDic.ContainsKey(addIndex))
                {
                    DictionaryOfGroundPoolObjectCreator groundPoolObjectCreatorDic = new DictionaryOfGroundPoolObjectCreator();
                    groundManager.straightPoolPrefabDic.Add(addIndex, groundPoolObjectCreatorDic);
                }
            }

            GUILayout.Label("Add Theme", addThemeLabelStyle, GUILayout.Width(150f));
        }
        GUILayout.EndHorizontal();

        if (groundManager.straightPoolPrefabDic.Count > 0)
        {
            foreach (var key in groundManager.straightPoolPrefabDic.Keys.ToList())
            {
                bool removeTheme = false;
                GUILayout.BeginHorizontal();
                {
                    removeTheme = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                    GUILayout.Label(string.Format("{0} Theme", key), GUILayout.Width(150f));
                }
                GUILayout.EndHorizontal();

                if (!straightGroundPoolObjectThemeFoldoutDic.ContainsKey(key))
                {
                    straightGroundPoolObjectThemeFoldoutDic.Add(key, true);
                }

                EditorGUI.indentLevel++;
                straightGroundPoolObjectThemeFoldoutDic[key] = EditorGUILayout.Foldout(straightGroundPoolObjectThemeFoldoutDic[key], "Straight Ground");
                if (straightGroundPoolObjectThemeFoldoutDic[key])
                {
                    DictionaryOfGroundPoolObjectCreator groundPoolObjectCreatorDic = groundManager.straightPoolPrefabDic[key];
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                        if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
                        {
                            if (!groundPoolObjectCreatorDic.ContainsKey(groundPoolObjectCreatorDic.Count))
                            {
                                GroundPoolObjectCreator groundPoolObjectCreator = new GroundPoolObjectCreator();
                                groundPoolObjectCreatorDic.Add(groundPoolObjectCreatorDic.Count, groundPoolObjectCreator);
                            }
                        }
                        GUILayout.Label("Add GroundPoolObject", addMapObjectLabelStyle, GUILayout.Width(150f));
                    }
                    GUILayout.EndHorizontal();

                    if (groundPoolObjectCreatorDic.Count > 0)
                    {
                        foreach (var key2 in groundPoolObjectCreatorDic.Keys.ToList())
                        {
                            GroundPoolObjectCreator groundPoolObjectCreator = groundPoolObjectCreatorDic[key2];
                            bool removeGroundPoolObject = false;
                            GUILayout.BeginHorizontal();
                            {
                                GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                                removeGroundPoolObject = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                                GUILayout.Label(string.Format("{0} GroundPoolObject", key2), GUILayout.Width(150f));
                            }
                            GUILayout.EndHorizontal();

                            EditorGUI.indentLevel++;
                            groundPoolObjectCreator.poolObjectPrefab = (GameObject)EditorGUILayout.ObjectField("GroundPoolObject Prefab", groundPoolObjectCreator.poolObjectPrefab, typeof(GameObject), true);
                            //mapObjectCreator.objectPoolRoot = (Transform)EditorGUILayout.ObjectField("MapObject Pool Root", mapObjectCreator.objectPoolRoot, typeof(Transform), true);
                            //mapObjectCreator.objectPoolCount = EditorGUILayout.IntField("MapObject PoolCount", mapObjectCreator.objectPoolCount);
                            EditorGUI.indentLevel--;

                            if (removeGroundPoolObject)
                            {
                                groundPoolObjectCreatorDic.Remove(key2);
                            }
                        }
                    }
                }
                EditorGUI.indentLevel--;

                if (removeTheme)
                {
                    groundManager.straightPoolPrefabDic.Remove(key);
                }
            }
        }
        else
        {
            GUILayout.BeginHorizontal();
            {
                //GUILayout.Space(20);
                //GameObject gameObject = (GameObject)EditorGUILayout.ObjectField(null, typeof(GameObject), true, GUILayout.MinWidth(80));
                //if (gameObject != null) piece.geometryList.Add(gameObject);
            }
            GUILayout.EndHorizontal();
        }
    }

    private void drawCornerGroundPoolObjects()
    {
        GUILayout.BeginVertical();
        {
            drawCornerGroundPoolObjectTheme();
        }
        GUILayout.EndVertical();
    }

    private void drawCornerGroundPoolObjectTheme()
    {
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
            {
                int addIndex = groundManager.cornerPoolPrefabDic.Count;
                if (!groundManager.cornerPoolPrefabDic.ContainsKey(addIndex))
                {
                    DictionaryOfGroundPoolObjectCreator groundPoolObjectCreatorDic = new DictionaryOfGroundPoolObjectCreator();
                    groundManager.cornerPoolPrefabDic.Add(addIndex, groundPoolObjectCreatorDic);
                }
            }

            GUILayout.Label("Add Theme", addThemeLabelStyle, GUILayout.Width(150f));
        }
        GUILayout.EndHorizontal();

        if (groundManager.cornerPoolPrefabDic.Count > 0)
        {
            foreach (var key in groundManager.cornerPoolPrefabDic.Keys.ToList())
            {
                bool removeTheme = false;
                GUILayout.BeginHorizontal();
                {
                    removeTheme = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                    GUILayout.Label(string.Format("{0} Theme", key), GUILayout.Width(150f));
                }
                GUILayout.EndHorizontal();

                if (!cornerGroundPoolObjectThemeFoldoutDic.ContainsKey(key))
                {
                    cornerGroundPoolObjectThemeFoldoutDic.Add(key, true);
                }

                EditorGUI.indentLevel++;
                cornerGroundPoolObjectThemeFoldoutDic[key] = EditorGUILayout.Foldout(cornerGroundPoolObjectThemeFoldoutDic[key], "Corner Ground");
                if (cornerGroundPoolObjectThemeFoldoutDic[key])
                {
                    DictionaryOfGroundPoolObjectCreator groundPoolObjectCreatorDic = groundManager.cornerPoolPrefabDic[key];
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                        if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
                        {
                            if (!groundPoolObjectCreatorDic.ContainsKey(groundPoolObjectCreatorDic.Count))
                            {
                                GroundPoolObjectCreator groundPoolObjectCreator = new GroundPoolObjectCreator();
                                groundPoolObjectCreatorDic.Add(groundPoolObjectCreatorDic.Count, groundPoolObjectCreator);
                            }
                        }
                        GUILayout.Label("Add GroundPoolObject", addMapObjectLabelStyle, GUILayout.Width(150f));
                    }
                    GUILayout.EndHorizontal();

                    if (groundPoolObjectCreatorDic.Count > 0)
                    {
                        foreach (var key2 in groundPoolObjectCreatorDic.Keys.ToList())
                        {
                            GroundPoolObjectCreator groundPoolObjectCreator = groundPoolObjectCreatorDic[key2];
                            bool removeGroundPoolObject = false;
                            GUILayout.BeginHorizontal();
                            {
                                GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                                removeGroundPoolObject = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                                GUILayout.Label(string.Format("{0} GroundPoolObject", key2), GUILayout.Width(150f));
                            }
                            GUILayout.EndHorizontal();

                            EditorGUI.indentLevel++;
                            groundPoolObjectCreator.poolObjectPrefab = (GameObject)EditorGUILayout.ObjectField("GroundPoolObject Prefab", groundPoolObjectCreator.poolObjectPrefab, typeof(GameObject), true);
                            //mapObjectCreator.objectPoolRoot = (Transform)EditorGUILayout.ObjectField("MapObject Pool Root", mapObjectCreator.objectPoolRoot, typeof(Transform), true);
                            //mapObjectCreator.objectPoolCount = EditorGUILayout.IntField("MapObject PoolCount", mapObjectCreator.objectPoolCount);
                            EditorGUI.indentLevel--;

                            if (removeGroundPoolObject)
                            {
                                groundPoolObjectCreatorDic.Remove(key2);
                            }
                        }
                    }
                }
                EditorGUI.indentLevel--;

                if (removeTheme)
                {
                    groundManager.cornerPoolPrefabDic.Remove(key);
                }
            }
        }
        else
        {
            GUILayout.BeginHorizontal();
            {
                //GUILayout.Space(20);
                //GameObject gameObject = (GameObject)EditorGUILayout.ObjectField(null, typeof(GameObject), true, GUILayout.MinWidth(80));
                //if (gameObject != null) piece.geometryList.Add(gameObject);
            }
            GUILayout.EndHorizontal();
        }
    }

    private void drawPieceGroundPoolObjects()
    {
        GUILayout.BeginVertical();
        {
            drawPieceGroundPoolObjectTheme();
        }
        GUILayout.EndVertical();
    }

    private void drawPieceGroundPoolObjectTheme()
    {
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
            {
                int addIndex = groundManager.piecePoolPrefabDic.Count;
                if (!groundManager.piecePoolPrefabDic.ContainsKey(addIndex))
                {
                    DictionaryOfGroundPoolObjectCreator groundPoolObjectCreatorDic = new DictionaryOfGroundPoolObjectCreator();
                    groundManager.piecePoolPrefabDic.Add(addIndex, groundPoolObjectCreatorDic);
                }
            }

            GUILayout.Label("Add Theme", addThemeLabelStyle, GUILayout.Width(150f));
        }
        GUILayout.EndHorizontal();

        if (groundManager.piecePoolPrefabDic.Count > 0)
        {
            foreach (var key in groundManager.piecePoolPrefabDic.Keys.ToList())
            {
                bool removeTheme = false;
                GUILayout.BeginHorizontal();
                {
                    removeTheme = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                    GUILayout.Label(string.Format("{0} Theme", key), GUILayout.Width(150f));
                }
                GUILayout.EndHorizontal();

                if (!pieceGroundPoolObjectThemeFoldoutDic.ContainsKey(key))
                {
                    pieceGroundPoolObjectThemeFoldoutDic.Add(key, true);
                }

                EditorGUI.indentLevel++;
                pieceGroundPoolObjectThemeFoldoutDic[key] = EditorGUILayout.Foldout(pieceGroundPoolObjectThemeFoldoutDic[key], "Piece Ground");
                if (pieceGroundPoolObjectThemeFoldoutDic[key])
                {
                    DictionaryOfGroundPoolObjectCreator groundPoolObjectCreatorDic = groundManager.piecePoolPrefabDic[key];
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                        if (GUILayout.Button(addButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20)))
                        {
                            if (!groundPoolObjectCreatorDic.ContainsKey(groundPoolObjectCreatorDic.Count))
                            {
                                GroundPoolObjectCreator groundPoolObjectCreator = new GroundPoolObjectCreator();
                                groundPoolObjectCreatorDic.Add(groundPoolObjectCreatorDic.Count, groundPoolObjectCreator);
                            }
                        }
                        GUILayout.Label("Add GroundPoolObject", addMapObjectLabelStyle, GUILayout.Width(150f));
                    }
                    GUILayout.EndHorizontal();

                    if (groundPoolObjectCreatorDic.Count > 0)
                    {
                        foreach (var key2 in groundPoolObjectCreatorDic.Keys.ToList())
                        {
                            GroundPoolObjectCreator groundPoolObjectCreator = groundPoolObjectCreatorDic[key2];
                            bool removeGroundPoolObject = false;
                            GUILayout.BeginHorizontal();
                            {
                                GUILayout.Space(EditorGUI.indentLevel * 20.0f);
                                removeGroundPoolObject = GUILayout.Button(removeButton, GUIStyle.none, GUILayout.Width(16), GUILayout.Height(20));
                                GUILayout.Label(string.Format("{0} GroundPoolObject", key2), GUILayout.Width(150f));
                            }
                            GUILayout.EndHorizontal();

                            EditorGUI.indentLevel++;
                            groundPoolObjectCreator.poolObjectPrefab = (GameObject)EditorGUILayout.ObjectField("GroundPoolObject Prefab", groundPoolObjectCreator.poolObjectPrefab, typeof(GameObject), true);
                            //mapObjectCreator.objectPoolRoot = (Transform)EditorGUILayout.ObjectField("MapObject Pool Root", mapObjectCreator.objectPoolRoot, typeof(Transform), true);
                            //mapObjectCreator.objectPoolCount = EditorGUILayout.IntField("MapObject PoolCount", mapObjectCreator.objectPoolCount);
                            EditorGUI.indentLevel--;

                            if (removeGroundPoolObject)
                            {
                                groundPoolObjectCreatorDic.Remove(key2);
                            }
                        }
                    }
                }
                EditorGUI.indentLevel--;

                if (removeTheme)
                {
                    groundManager.piecePoolPrefabDic.Remove(key);
                }
            }
        }
        else
        {
            GUILayout.BeginHorizontal();
            {
                //GUILayout.Space(20);
                //GameObject gameObject = (GameObject)EditorGUILayout.ObjectField(null, typeof(GameObject), true, GUILayout.MinWidth(80));
                //if (gameObject != null) piece.geometryList.Add(gameObject);
            }
            GUILayout.EndHorizontal();
        }
    }
}

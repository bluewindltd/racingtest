﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class EvilCountSection
{
    public int achievePoint = 0;
}

public class EvilCountDocs : Singleton<EvilCountDocs>
{
    Dictionary<int, EvilCountSection> m_EvilCountSectionDic = new Dictionary<int, EvilCountSection>();
    public Dictionary<int, EvilCountSection> evilCountSectionDic
    {
        get
        {
            return m_EvilCountSectionDic;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_EvilCountSectionDic.Count <= 0)
        {
            Dictionary<string, object> allDataByEvilCountTableSchema;
            GDEDataManager.GetAllDataBySchema("EvilCountTable", out allDataByEvilCountTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByEvilCountTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> evilCountTableDic = etorTable.Current.Value as Dictionary<string, object>;

                EvilCountSection evilCountSection = new EvilCountSection();

                int step = 0;
                evilCountTableDic.TryGetInt("step", out step);
                evilCountTableDic.TryGetInt("achievePoint", out evilCountSection.achievePoint);

                m_EvilCountSectionDic.Add(step, evilCountSection);
            }
        }
    }
}
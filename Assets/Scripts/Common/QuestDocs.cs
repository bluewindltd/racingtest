﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class QuestData
{
    public enum Type
    {
        DestroyGang = 0,        // 갱스터 차량 N대 부수기
        ChaseMission = 1,       // 도망 미션 N회 성공하기
        DestroyPolice = 2,      // 경찰차 N대 부수기
        DeliveryMission = 3,    // 배달 미션 N회 클리어 하기
        EvilCount = 4,          // 악명도(별) N개 얻기
        Arrest = 5,             // 체포 N번 당하기
        HPItem = 6,             // 회복 아이템 N개 먹기
        Waste = 7,           // 차량 파괴 N번 당하기
    };

    public Type type;
    public int param;
    public int time;
    public int exp;
    public int minRewardAmount;
    public int maxRewardAmount;
}

public class QuestFriendshipData
{
    public int clientIndex;
    public int level;
    public int needExp;
    public int startExp;
    public int endExp;
    public GameConfig.RewardType levelUpRewardType;
    public int levelUpRewardValue;
    public bool specialReward;
    public List<QuestData> questDataList = new List<QuestData>();
}

public class QuestClientData
{
    public int index;
    public int seasonIndex;
    public int rate;
    public string imageName;
    public string name;
    public string desc;
    public string detailDesc;
    public string questContent1;
    public string questContent2;
    public string questDesc1;
    public string questDesc2;
    public string questComplete;
    public string questLevelUp;
    public int lastFriendshipLevel = 0;
    public Dictionary<int, QuestFriendshipData> friendshipDic = new Dictionary<int, QuestFriendshipData>();
}

public class QuestSeasonData
{
    public int index;
    public string name;
    public string desc;
    public int receiveLimitTime;
    public Dictionary<int, QuestClientData> clientDic = new Dictionary<int, QuestClientData>();
}

public class QuestDocs : Singleton<QuestDocs>
{
    private Dictionary<int, QuestSeasonData> m_QuestSeasonDataDic = new Dictionary<int, QuestSeasonData>();
    public Dictionary<int, QuestSeasonData> questSeasonDataDic
    {
        get
        {
            return m_QuestSeasonDataDic;
        }
    }

    private Dictionary<int, QuestClientData> m_QuestClientDataDic = new Dictionary<int, QuestClientData>();
    public Dictionary<int, QuestClientData> questClientDataDic
    {
        get
        {
            return m_QuestClientDataDic;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
#if BWQUEST
        if (m_QuestSeasonDataDic.Count <= 0)
        {
            Dictionary<string, object> allDataByQuestSeasonTableSchema;
            GDEDataManager.GetAllDataBySchema("QuestSeaonTable", out allDataByQuestSeasonTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByQuestSeasonTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> questSeasonTableDic = etorTable.Current.Value as Dictionary<string, object>;

                QuestSeasonData questSeasonData = new QuestSeasonData();

                questSeasonTableDic.TryGetInt("seasonIndex", out questSeasonData.index);
                questSeasonTableDic.TryGetString("name", out questSeasonData.name);
                questSeasonTableDic.TryGetString("desc", out questSeasonData.desc);
                questSeasonTableDic.TryGetInt("receiveLimitTime", out questSeasonData.receiveLimitTime);

                m_QuestSeasonDataDic.Add(questSeasonData.index, questSeasonData);
            }

            Dictionary<string, object> allDataByQuestClientTableSchema;
            GDEDataManager.GetAllDataBySchema("QuestClientTable", out allDataByQuestClientTableSchema);
            etorTable = allDataByQuestClientTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> questClientTableDic = etorTable.Current.Value as Dictionary<string, object>;

                QuestClientData questClientData = new QuestClientData();

                questClientTableDic.TryGetInt("clientIndex", out questClientData.index);
                questClientTableDic.TryGetInt("seasonIndex", out questClientData.seasonIndex);
                questClientTableDic.TryGetInt("rate", out questClientData.rate);
                questClientTableDic.TryGetString("imageName", out questClientData.imageName);
                questClientTableDic.TryGetString("name", out questClientData.name);
                questClientTableDic.TryGetString("desc", out questClientData.desc);
                questClientTableDic.TryGetString("detailDesc", out questClientData.detailDesc);
                questClientTableDic.TryGetString("questContent1", out questClientData.questContent1);
                questClientTableDic.TryGetString("questContent2", out questClientData.questContent2);
                questClientTableDic.TryGetString("questDesc1", out questClientData.questDesc1);
                questClientTableDic.TryGetString("questDesc2", out questClientData.questDesc2);
                questClientTableDic.TryGetString("questComplete", out questClientData.questComplete);
                questClientTableDic.TryGetString("questLevelUp", out questClientData.questLevelUp);

                if (m_QuestSeasonDataDic.ContainsKey(questClientData.seasonIndex))
                {
                    m_QuestSeasonDataDic[questClientData.seasonIndex].clientDic.Add(questClientData.index, questClientData);
                }

                m_QuestClientDataDic.Add(questClientData.index, questClientData);
            }

            int type = 0;
            QuestFriendshipData questFriendshipData = null;
            QuestData questData = null;
            Dictionary<string, object> allDataByQuestFriendshipTableSchema;
            GDEDataManager.GetAllDataBySchema("QuestFriendshipTable", out allDataByQuestFriendshipTableSchema);
            etorTable = allDataByQuestFriendshipTableSchema.GetEnumerator();
            int clientIndex = -1;
            int preExp = 0;
            int levelUpRewardType = 0;
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> questFriendshipTableDic = etorTable.Current.Value as Dictionary<string, object>;

                questFriendshipData = new QuestFriendshipData();

                questFriendshipTableDic.TryGetInt("clientIndex", out questFriendshipData.clientIndex);

                if(clientIndex != questFriendshipData.clientIndex)
                {
                    clientIndex = questFriendshipData.clientIndex;
                    preExp = 0;
                }
                questFriendshipTableDic.TryGetInt("level", out questFriendshipData.level);
                questFriendshipData.startExp = preExp;
                questFriendshipTableDic.TryGetInt("needExp", out questFriendshipData.needExp);
                questFriendshipData.endExp = preExp + questFriendshipData.needExp;
                preExp = questFriendshipData.endExp;
                questFriendshipTableDic.TryGetInt("levelUpRewardType", out levelUpRewardType);
                questFriendshipData.levelUpRewardType = (GameConfig.RewardType)levelUpRewardType;
                questFriendshipTableDic.TryGetInt("levelUpRewardValue", out questFriendshipData.levelUpRewardValue);
                questFriendshipTableDic.TryGetBool("specialReward", out questFriendshipData.specialReward);

                questData = new QuestData();
                questFriendshipTableDic.TryGetInt("questType1", out type);
                questData.type = (QuestData.Type)type;
                questFriendshipTableDic.TryGetInt("questParam1", out questData.param);
                questFriendshipTableDic.TryGetInt("questTime1", out questData.time);
                questFriendshipTableDic.TryGetInt("questExp1", out questData.exp);
                questFriendshipTableDic.TryGetInt("minReward1", out questData.minRewardAmount);
                questFriendshipTableDic.TryGetInt("maxReward1", out questData.maxRewardAmount);
                questFriendshipData.questDataList.Add(questData);
                questData = new QuestData();
                questFriendshipTableDic.TryGetInt("questType2", out type);
                questData.type = (QuestData.Type)type;
                questFriendshipTableDic.TryGetInt("questParam2", out questData.param);
                questFriendshipTableDic.TryGetInt("questTime2", out questData.time);
                questFriendshipTableDic.TryGetInt("questExp2", out questData.exp);
                questFriendshipTableDic.TryGetInt("minReward2", out questData.minRewardAmount);
                questFriendshipTableDic.TryGetInt("maxReward2", out questData.maxRewardAmount);
                questFriendshipData.questDataList.Add(questData);

                if (m_QuestClientDataDic.ContainsKey(questFriendshipData.clientIndex))
                {
                    // 최고 레벨 수치를 갱신한다.
                    if(questFriendshipData.level > m_QuestClientDataDic[questFriendshipData.clientIndex].lastFriendshipLevel)
                    {
                        m_QuestClientDataDic[questFriendshipData.clientIndex].lastFriendshipLevel = questFriendshipData.level;
                    }
                    m_QuestClientDataDic[questFriendshipData.clientIndex].friendshipDic.Add(questFriendshipData.level, questFriendshipData);
                }
            }
        }
#endif
    }
}

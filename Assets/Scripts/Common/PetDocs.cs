﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class PetData
{
    public enum Type
    {
        ShellAttack = 0,
    }

    public string name = "";
    public string desc = "";
    public int index = 0;
    public int order = 0;
    public Type type = Type.ShellAttack;
    public string googleID = "";
    public string appleID = "";
    public string recordPrice = "";
    public string tempPrice = "";
    public string stockPrice = "";
    public int health = 0;
    public int damage = 0;
    public int attackDamage = 0;
    public float attackDelay = 0;
    public float rotorSpeed = 0;
    public float shellSpeed = 0;
    public float explosionRadius = 0;
}

public class PetDocs : Singleton<PetDocs>
{
    private Dictionary<int, PetData> m_PetDataDic = new Dictionary<int, PetData>();
    public Dictionary<int, PetData> petDataDic
    {
        get
        {
            return m_PetDataDic;
        }
    }

    private bool m_IsUpdateProduct = false;
    public bool isUpdateProduct
    {
        get
        {
            return m_IsUpdateProduct;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        if (m_PetDataDic.Count <= 0)
        {
            Dictionary<string, object> allDataByPetTableSchema;
            GDEDataManager.GetAllDataBySchema("PetTable", out allDataByPetTableSchema);
            Dictionary<string, object>.Enumerator etorTable = allDataByPetTableSchema.GetEnumerator();
            while (etorTable.MoveNext())
            {
                Dictionary<string, object> petTableDic = etorTable.Current.Value as Dictionary<string, object>;

                PetData petData = new PetData();

                petTableDic.TryGetString("name", out petData.name);
                petTableDic.TryGetString("desc", out petData.desc);
                petTableDic.TryGetInt("index", out petData.index);
                petTableDic.TryGetInt("order", out petData.order);
                int type = 0;
                petTableDic.TryGetInt("type", out type);
                petData.type = (PetData.Type)type;
                petTableDic.TryGetString("googleID", out petData.googleID);
                petTableDic.TryGetString("appleID", out petData.appleID);
                petTableDic.TryGetString("recordPrice", out petData.recordPrice);
                petTableDic.TryGetString("tempPrice", out petData.tempPrice);
                petTableDic.TryGetInt("health", out petData.health);
                petTableDic.TryGetInt("damage", out petData.damage);
                petTableDic.TryGetInt("attackDamage", out petData.attackDamage);
                petTableDic.TryGetFloat("attackDelay", out petData.attackDelay);
                petTableDic.TryGetFloat("rotorSpeed", out petData.rotorSpeed);
                petTableDic.TryGetFloat("shellSpeed", out petData.shellSpeed);
                petTableDic.TryGetFloat("explosionRadius", out petData.explosionRadius);

                if (petData.googleID == null)
                {
                    petData.googleID = "";
                }
                if (petData.appleID == null)
                {
                    petData.appleID = "";
                }

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
                if (petData.googleID.CompareTo("") != 0)
                {
                    Debug.Log("@@ Pet AddProduct : " + petData.googleID);
                    AndroidInAppPurchaseManager.Instance.AddProduct(petData.googleID);
                }
#elif UNITY_IOS
                if (petData.appleID.CompareTo("") != 0)
                {
				    Debug.Log("@@ Pet AddProduct : " + petData.appleID);
				    IOSInAppPurchaseManager.Instance.AddProductId(petData.appleID);
                }
#endif

                m_PetDataDic.Add(petData.index, petData);
            }
        }
    }

    public PetData GetPetForGoogle(string googleID)
    {
        PetData petData = null;
        Dictionary<int, PetData>.Enumerator etor = m_PetDataDic.GetEnumerator();

        while (etor.MoveNext())
        {
            if (etor.Current.Value.googleID.CompareTo(googleID) == 0)
            {
                petData = etor.Current.Value;
            }
        }

        return petData;
    }

    public PetData GetPetForApple(string appleID)
    {
        PetData petData = null;
        Dictionary<int, PetData>.Enumerator etor = m_PetDataDic.GetEnumerator();

        while (etor.MoveNext())
        {
            if (etor.Current.Value.appleID.CompareTo(appleID) == 0)
            {
                petData = etor.Current.Value;
            }
        }

        return petData;
    }

    public void UpdateProduct()
    {
        if (m_IsUpdateProduct == false)
        {
            m_IsUpdateProduct = true;
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
            PetData petData = null;
            Dictionary<int, PetData>.Enumerator etor = m_PetDataDic.GetEnumerator();
            while(etor.MoveNext())
            {
                petData = etor.Current.Value;
                if(petData != null)
                {
                    GoogleProductTemplate product = AndroidInAppPurchaseManager.Client.Inventory.GetProductDetails(petData.googleID);
                    if (product != null)
                    {
                        petData.stockPrice = product.LocalizedPrice;
                    }
                }
            }
#elif UNITY_IOS
			int testCount = 0;
			PetData petData = null;
			Dictionary<int, PetData>.Enumerator etor = m_PetDataDic.GetEnumerator();
			while(etor.MoveNext())
			{
				petData = etor.Current.Value;
				if(petData != null)
				{
					IOSProductTemplate product = IOSInAppPurchaseManager.Instance.GetProductById(petData.appleID);					
					if(product != null)
					{
						Debug.Log("@@ IOSProductTemplate " + testCount + " Success : " + petData.appleID + ", " + product.DisplayName + ", " + product.LocalizedPrice + ", " + product.Id);
						petData.stockPrice = product.LocalizedPrice;
					}
					else
					{
						Debug.Log("@@ IOSProductTemplate " + testCount + " Failed");
					}
				}

				testCount++;
			}
#endif
        }
    }
}

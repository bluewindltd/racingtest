﻿using UnityEngine;
using System.Collections;

public class QuestRetryPopup : BasePopup
{
    public UILabel titleLabel;
    public UILabel nameLabel;
    public UILabel descLabel;
    public UILabel adButtonLabel;
    public UIButton retryButton;
    public UILabel retryButtonLabel;
    public UILabel retryCashButtonLabel;
    public UILabel exitButtonLabel;
    public UILabel moneyLabel;

    QuestPopup m_QuestPopup;
    UserManager m_UserMgr;

    void Awake()
    {
        m_UserMgr = UserManager.instance;
    }

    void Start()
    {
        UserManager.OnRefreshCash += OnRefreshCash;
        CommonAdManager.OnHideVideoAd += OnHideVideoAd;
    }

    void OnDestroy()
    {
        UserManager.OnRefreshCash -= OnRefreshCash;
        CommonAdManager.OnHideVideoAd -= OnHideVideoAd;
    }

    public static QuestRetryPopup Show(GameObject parent, GameObject prefab)
    {
        int cash = UserManager.instance.cash;

        QuestRetryPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<QuestRetryPopup>();

        popup.titleLabel.text = TextDocs.content("UI_QUEST_RECEIVE");
        popup.nameLabel.text = TextDocs.content("UI_QUEST_NPCNAME");
        popup.descLabel.text = TextDocs.content("UI_BRIBE_SPEECH");
        popup.adButtonLabel.text = string.Format(TextDocs.content("BT_ADVIEW_TEXT"), EtcDocs.instance.idleAdRewardCash);
        popup.retryButtonLabel.text = TextDocs.content("BT_QUEST_BRIBE");
        popup.retryCashButtonLabel.text = string.Format("{0}", EtcDocs.instance.questRetryCash);
        popup.exitButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");

        popup.moneyLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", cash));

        if (cash >= EtcDocs.instance.questRetryCash)
        {
            popup.retryButton.isEnabled = true;
        }
        else
        {
            popup.retryButton.isEnabled = false;
        }

        return popup;
    }

    public void Initialize(QuestPopup questPopup)
    {
        m_QuestPopup = questPopup;
    }

    void OnRefreshCash()
    {
        moneyLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));
    }

    //---------------------- UI 콜백 ----------------------
    void OnHideVideoAd(bool isSuccess)
    {
        if (isSuccess)
        {
            m_UserMgr.cash += EtcDocs.instance.idleAdRewardCash;
            m_UserMgr.SaveUserData();

            moneyLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));

            if (m_UserMgr.cash >= EtcDocs.instance.questRetryCash)
            {
                retryButton.isEnabled = true;
            }
        }
    }

    public void OnClickShowAd()
    {
        SoundManager.PlayButtonClickSound();

        CommonAdManager.instance.Show(CommonAdManager.AdType.Video);
    }

    public void OnClickQuestRetry()
    {
        SoundManager.PlayButtonClickSound();

        if (m_UserMgr.cash >= EtcDocs.instance.questRetryCash)
        {
            if(m_QuestPopup.state == QuestClientInfo.State.Select ||
               m_QuestPopup.state == QuestClientInfo.State.Success)
            {
                // 기존 의뢰 사라진다는 경고 팝업 띄움
                PopupManager.instance.ShowLargeYesNo(TextDocs.content("LOAD_WARNING_TITLE"), TextDocs.content("POPUP_QUEST_NOTICE01"),
                    delegate
                    {
                        m_UserMgr.cash -= EtcDocs.instance.questRetryCash;

                        m_QuestPopup.RetryQuest();

                        Hide();
                    }, null, null);
            }
            else
            {
                m_UserMgr.cash -= EtcDocs.instance.questRetryCash;

                m_QuestPopup.RetryQuest();

                Hide();
            }
        }
    }

    public void OnClickExit()
    {
        SoundManager.PlayButtonClickSound();

        Hide();
    }
}

﻿using UnityEngine;
using System.Collections;

public class PackagePetModel : WeMonoBehaviour
{
    public Transform[] mainRotorTransforms;
    public float maxRotorVelocity = 7200;			// degrees per second
    private float rotorRotation = 0.0f; 			// degrees... used for animating rotors

    void Update()
    {
        for (int i = 0; i < mainRotorTransforms.Length; i++)
        {
            mainRotorTransforms[i].rotation = m_Tr.rotation * Quaternion.Euler(0, rotorRotation, 0);
        }
        rotorRotation += maxRotorVelocity * Time.deltaTime;
    }
}

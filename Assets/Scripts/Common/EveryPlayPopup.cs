﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using BW.Common;

public class EveryPlayPopup : BasePopup
{
    public Animator animator;
    public UILabel popupTitleLabel;
    public UILabel[] tokenLabel;
    public UILabel cashLabel;
    public UILabel recordButtonLabel;
    public UILabel faceCameraButtonLabel;
    public UILabel shareButtonLabel;
    public UILabel videoButtonLabel;
    public UILabel closeButtonLabel;
    public GameObject recordOnObject;
    public GameObject recordOffObject;
    public GameObject faceCameraOnObject;
    public GameObject faceCameraOffObject;

    bool m_EnableMenu = false;
    public bool enableMenu
    {
        get
        {
            return m_EnableMenu;
        }
    }

    string[] m_AndroidPermissions = { "android.permission.CAMERA", "android.permission.RECORD_AUDIO" };
    List<string> m_RequestPermissionList = new List<string>();

    UserManager m_UserMgr;
    EveryPlayManager m_EveryPlayMgr;

    bool m_RecordingStop = false;

    void Start()
    {
#if UNITY_ANDROID// && !UNITY_EDITOR
        BWCommonPlugin.OnShouldShowRequestPermissionRationaleResult += OnShouldShowRequestPermissionRationaleResult;
        BWCommonPlugin.OnRequestPermissionsResult += OnRequestPermissionsResult;
#endif

        EveryPlayManager.OnRecordingStopped += OnRecordingStopped;

        m_UserMgr = UserManager.instance;
        m_EveryPlayMgr = EveryPlayManager.instance;

        popupTitleLabel.text = TextDocs.content("EVERYPLAY_TITLE_TEXT");
        closeButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");

        recordButtonLabel.text = TextDocs.content("BT_EVERYPLAY_RECORD_TEXT");
        faceCameraButtonLabel.text = TextDocs.content("BT_EVERYPLAY_FACECAMERA_TEXT");
        shareButtonLabel.text = TextDocs.content("BT_EVERYPLAY_SHARE_TEXT");
        videoButtonLabel.text = TextDocs.content("BT_EVERYPLAY_VIDEOLIST_TEXT");

        if (m_EveryPlayMgr.IsRecording())
        {
            NGUITools.SetActive(recordOnObject, false);
            NGUITools.SetActive(recordOffObject, true);
        }
        else
        {
            NGUITools.SetActive(recordOnObject, true);
            NGUITools.SetActive(recordOffObject, false);
        }

        if (m_EveryPlayMgr.FaceCamIsSessionRunning())
        {
            NGUITools.SetActive(faceCameraOnObject, false);
            NGUITools.SetActive(faceCameraOffObject, true);
        }
        else
        {
            NGUITools.SetActive(faceCameraOnObject, true);
            NGUITools.SetActive(faceCameraOffObject, false);
        }

        Dictionary<int, int>.Enumerator etor = m_UserMgr.tokenDic.GetEnumerator();
        while (etor.MoveNext())
        {
            if (0 <= etor.Current.Key &&
                etor.Current.Key < tokenLabel.Length)
            {
                tokenLabel[etor.Current.Key].text = string.Format("{0}", etor.Current.Value);
            }
        }
        cashLabel.text = string.Format("{0}", m_UserMgr.cash);

        animator.Play("Popup_Show");
    }

    void OnDestroy()
    {
#if UNITY_ANDROID// && !UNITY_EDITOR
        BWCommonPlugin.OnShouldShowRequestPermissionRationaleResult -= OnShouldShowRequestPermissionRationaleResult;
        BWCommonPlugin.OnRequestPermissionsResult -= OnRequestPermissionsResult;
#endif
        EveryPlayManager.OnRecordingStopped -= OnRecordingStopped;
    }

    void ShowPermissionExplainDialog()
    {
        AndroidDialog dialog = AndroidDialog.Create(TextDocs.content("POPUP_PERMISSION_EXPLAIN_TITLE"), TextDocs.content("POPUP_PERMISSION_EXPLAIN_TEXT_2"), TextDocs.content("BT_OK_TEXT"), TextDocs.content("BT_CANCEL_TEXT"));
        dialog.ActionComplete += OnPermissionExplainDialogComplete;
    }

    private void OnPermissionExplainDialogComplete(AndroidDialogResult result)
    {
        //parsing result
        switch (result)
        {
            case AndroidDialogResult.YES:
                BWCommonPlugin.instance.RequestPermissions(m_RequestPermissionList.ToArray());
                break;
            case AndroidDialogResult.NO:
            case AndroidDialogResult.CLOSED:
                {
                    m_EnableMenu = true;
                    PopupManager.instance.HideLoadingPopup();

                    for(int i = 0; i < m_RequestPermissionList.Count; i++)
                    {
                        PlayerPrefs.SetInt(m_RequestPermissionList[i], 1);
                    }
                }
                break;
        }
    }

    void OnShouldShowRequestPermissionRationaleResult(Dictionary<string, string> resultDic)
    {
        m_RequestPermissionList.Clear();
        GameConfig.PermissionState permissionState = GameConfig.PermissionState.PASS;

        if (resultDic.Count > 0)
        {
            Dictionary<string, string>.Enumerator etor = resultDic.GetEnumerator();
            while (etor.MoveNext())
            {
                string state = etor.Current.Value;
                if (state.CompareTo("BLOCKED") == 0)
                {
                    int isFirst = PlayerPrefs.GetInt(etor.Current.Key);
                    // 한번도 권한 요청이 안된 상태
                    if (isFirst == 0)
                    {
                        m_RequestPermissionList.Add(etor.Current.Key);
                        permissionState = GameConfig.PermissionState.FIRST_NEED_REQUEST;
                    }
                    else // 유저가 의도적으로 권한을 Block 한 상태
                    {
                        permissionState = GameConfig.PermissionState.NON_PASS;
                        break;
                    }
                }
                else if (state.CompareTo("DENIED") == 0)
                {
                    m_RequestPermissionList.Add(etor.Current.Key);
                    permissionState = GameConfig.PermissionState.NEED_REQUEST;
                }
                else // "GRANTED"
                {

                }
            }
        }
        else
        {
            permissionState = GameConfig.PermissionState.NON_PASS;
        }

        switch (permissionState)
        {
            case GameConfig.PermissionState.PASS:
            case GameConfig.PermissionState.NON_PASS:
            case GameConfig.PermissionState.NEED_REQUEST:
                {
                    m_EnableMenu = true;
                    PopupManager.instance.HideLoadingPopup();
                }
                break;
            case GameConfig.PermissionState.FIRST_NEED_REQUEST:
                {
                    // 권한 설명 팝업 띄운다.
                    ShowPermissionExplainDialog();
                }
                break;
        }
    }

    void OnRequestPermissionsResult(Dictionary<string, string> resultDic)
    {
        int isSuccessCount = 0;
        if (resultDic.Count > 0)
        {
            Dictionary<string, string>.Enumerator etor = resultDic.GetEnumerator();
            while (etor.MoveNext())
            {
                PlayerPrefs.SetInt(etor.Current.Key, 1);
                string state = etor.Current.Value;
                if (state.CompareTo("TRUE") == 0)
                {
                    isSuccessCount += 1;
                }
            }
        }

        if (isSuccessCount != 0 && resultDic.Count <= isSuccessCount)
        {
            m_EnableMenu = true;
            PopupManager.instance.HideLoadingPopup();

        }
        else
        {
            m_EnableMenu = true;
            PopupManager.instance.HideLoadingPopup();
        }
    }

    public void EndShow()
    {
#if UNITY_ANDROID
        PopupManager.instance.ShowLoadingPopup();
        // 팝업 오픈 연출 끝난 뒤에 관련 권한이 있는지 확인 한다.
        BWCommonPlugin.instance.ShouldShowRequestPermissionRationale(m_AndroidPermissions);
#else
		m_EnableMenu = true;
#endif
    }

    public void EndHide()
    {
        Hide();
    }

    void OnRecordingStopped()
    {
        if(m_RecordingStop)
        {
            m_RecordingStop = false;

            m_EveryPlayMgr.ShowSharingModal();            
        }

        NGUITools.SetActive(recordOnObject, true);
        NGUITools.SetActive(recordOffObject, false);
    }

    public static EveryPlayPopup Show(GameObject parent, GameObject prefab)
    {
        EveryPlayPopup popup = NGUITools.AddChild(parent, prefab).GetComponent<EveryPlayPopup>();

        return popup;
    }
    
    public void OnClickRecordOn()
    {
        if(m_EnableMenu && !m_EveryPlayMgr.IsRecording())
        {
            SoundManager.PlayButtonClickSound();

            m_EveryPlayMgr.StartRecording();

            NGUITools.SetActive(recordOnObject, false);
            NGUITools.SetActive(recordOffObject, true);
        }
    }

    public void OnClickRecordOff()
    {
        if (m_EnableMenu && m_EveryPlayMgr.IsRecording())
        {
            SoundManager.PlayButtonClickSound();

            m_RecordingStop = true;

            m_EveryPlayMgr.StopRecording();

            NGUITools.SetActive(recordOnObject, true);
            NGUITools.SetActive(recordOffObject, false);
        }
    }

    public void OnClickFaceCameraOn()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EveryPlayMgr.FaceCamStartSession();

            NGUITools.SetActive(faceCameraOnObject, false);
            NGUITools.SetActive(faceCameraOffObject, true);
        }
    }

    public void OnClickFaceCameraOff()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EveryPlayMgr.FaceCamStopSession();

            NGUITools.SetActive(faceCameraOnObject, true);
            NGUITools.SetActive(faceCameraOffObject, false);
        }
    }

    public void OnClickShare()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EveryPlayMgr.ShowSharingModal();

        }
    }

    public void OnClickVideoList()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EveryPlayMgr.Show();
        }
    }

    public void OnClickGetTokenHelp()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();
            PopupManager.instance.ShowLargeOK(TextDocs.content("UI_TOKEN_TEXT2"), TextDocs.content("GET_TOKEN_HELP_TEXT"), null, null);
        }
    }

    public void OnClickExit()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            m_EnableMenu = false;

            animator.Play("Popup_Hide");
        }
    }
}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

public sealed class ReverseComparer<T> : IComparer<T>
{
    private readonly IComparer<T> original;

    public ReverseComparer(IComparer<T> original)
    {
        this.original = original;
    }

    public int Compare(T left, T right)
    {
        return original.Compare(right, left);
    }
}

public class CommonUtil
{
    public static void DestroyChildren(Transform transform)
    {
        for (int i = transform.childCount - 1; i >= 0; --i)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
        transform.DetachChildren();
    }

    public static void DestroyChildrenEditor(Transform transform)
    {
        for (int i = transform.childCount - 1; i >= 0; --i)
        {
            GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
        }
        transform.DetachChildren();
    }

    public static void MoveChild(GameObject parent, GameObject go)
    {
        if (go != null && parent != null)
        {
            Transform t = go.transform;
            t.parent = parent.transform;
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
            go.layer = parent.layer;
        }
    }

    public static void ChangeLayersRecursively(Transform trans, String name)
    {
        for (int i = trans.childCount - 1; i >= 0; --i)
        {
            trans.GetChild(i).gameObject.layer = LayerMask.NameToLayer(name);
            ChangeLayersRecursively(trans.GetChild(i), name);
        }
    }

    //public static string HMACSHA1_EncodeBase64(string data)
    //{
    //    string secret = "Ak5CAnMUYhM-L0VOfLLL4sxpz9oif1QkysQvAyiQi4b";

    //    byte[] secretkey = System.Text.Encoding.UTF8.GetBytes(secret);
    //    byte[] datas = System.Text.Encoding.UTF8.GetBytes(data);

    //    HMACSHA1 hmacsha1 = new HMACSHA1(secretkey);

    //    byte[] hashValue = hmacsha1.ComputeHash(datas);

    //    string signature = Convert.ToBase64String(hashValue);

    //    return signature;
    //}

    static public GameObject AddChild(GameObject parent, GameObject prefab)
    {
        GameObject go = GameObject.Instantiate(prefab) as GameObject;

        if (go != null && parent != null)
        {
            Transform t = go.transform;
            t.parent = parent.transform;
            t.localPosition = prefab.transform.localPosition;
            t.localRotation = prefab.transform.localRotation;
            t.localScale = prefab.transform.localScale;
            go.layer = parent.layer;
        }
        return go;
    }
}

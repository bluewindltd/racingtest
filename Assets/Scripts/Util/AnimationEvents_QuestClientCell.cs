﻿using UnityEngine;
using System.Collections;

public class AnimationEvents_QuestClientCell : MonoBehaviour
{
    public QuestClientCell questClientCell;

    void PlaySFX(string audioClipName)
    {
        SoundManager.instance.PlaySFX(audioClipName);
    }

    // Normal 상태가 사라지는 애니에서 Select 상태로 넘어가게 해주기 위한 함수
    void ChangeNormalToSelect()
    {
        questClientCell.AE_ChangeNormalToSelect();
    }

    // Select 상태를 보여주는 애니가 끝났을 경우 다시 버튼들을 활성화 시켜주기 위한 함수
    void EndShowSelect()
    {
        questClientCell.AE_EndShowSelect();
    }

    void EndShowNormal()
    {
        questClientCell.AE_EndShowNormal();
    }

    void EndHideSelect()
    {
        questClientCell.AE_EndHideSelect();
    }

    void EndHideSuccess()
    {
        questClientCell.AE_EndHideSuccess();
    }

    void EndHideFail()
    {
        questClientCell.AE_EndHideFail();
    }
}

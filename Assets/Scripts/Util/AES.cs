﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;
using System.Collections;
using SimpleJSON;

namespace AESENC
{
    public class AES
    {
        internal const string Inputkey = "560A18CD-6346-4CF0-A2E8-671F9B6B9EA9";

        private RijndaelManaged NewRijndaelManaged(string salt, ref string keydd, ref string iv)
        {
            if (salt == null) throw new ArgumentNullException("salt");
            var saltBytes = Encoding.ASCII.GetBytes(salt);
            var key = new Rfc2898DeriveBytes(Inputkey, saltBytes);

            var aesAlg = new RijndaelManaged();
            aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
            keydd = Encoding.UTF8.GetString(aesAlg.Key);
            aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);
            iv = keydd = Encoding.UTF8.GetString(aesAlg.IV);

            return aesAlg;
        }

        public string EncryptRijndael(string text, string salt)
        {
            if (string.IsNullOrEmpty(text))
                throw new ArgumentNullException("text");

            string keydd = "";
            string iv = "";
            var aesAlg = NewRijndaelManaged(salt, ref keydd, ref iv);

            var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
            var msEncrypt = new MemoryStream();
            using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
            using (var swEncrypt = new StreamWriter(csEncrypt))
            {
                swEncrypt.Write(text);
            }

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(msEncrypt.ToString() + iv + keydd));
            //return Convert.ToBase64String(msEncrypt.ToArray());
        }

        public IEnumerator AESTest()
        {
            //if (m_gameLoading.activeSelf == false)
            //    NGUITools.SetActive(m_gameLoading, true);

            WWWForm form = new WWWForm();

            JSONClass rootNode = new JSONClass();
            JSONClass jsonUserData = new JSONClass();
            rootNode.Add("text", new JSONData("Hello World!!"));

            //12345678901234567890123456789012
            //"dkasasdfdfj32345kasd132adsf45asd"
            //string test = AESEncrypt256(rootNode.ToString(), "dkasasdfdfj32345kasd132adsf45asd");
            string test = EncryptRijndael(rootNode.ToString(), "33f82976-ffb1-4959-bee8-8400f8805b94");
            form.AddField("data", test);
            form.AddField("length", test.Length);

            WWW www = new WWW("https://pf.daerisoft.com/cryptTest", form);
            yield return www;

            if (www.error == null)
            {
                //Debug.Log("www.text = " + www.text);

                //JSONNode json = JSON.Parse(www.text);
                //if (json["returnvalue"].AsInt == 0)
                //{
                //    VehicleBuyToIAP(vehicleData);

                //    if (OnPurchaseComplete != null)
                //    {
                //        OnPurchaseComplete(PurchaseCompleteState.Success);
                //    }

                //    // 확인
                //    WWWForm form2 = new WWWForm();
                //    form2.AddField("ORDERID", result.purchase.orderId);
                //    form2.AddField("PRICE", vehicleData.tempPrice);

                //    WWW www2 = new WWW("http://burnoutcityiap-env.ap-northeast-2.elasticbeanstalk.com/google_payout", form2);
                //}
                //else
                //{
                //    //NGUITools.SetActive(m_msgPopupObj, true);
                //    //m_msgPopup.SetMsg(3);

                //    if (OnPurchaseComplete != null)
                //    {
                //        OnPurchaseComplete(PurchaseCompleteState.PurchaseError);
                //    }
                //}
            }
            else {
                //NGUITools.SetActive(m_msgPopupObj, true);
                //m_msgPopup.SetMsg(3);

                Debug.Log(www.error);
            }

            //if (m_gameLoading.activeSelf)
            //    NGUITools.SetActive(m_gameLoading, false);

            //if (_isBuying)
            //    _isBuying = false;
        }

        //AES_256 암호화
        public string AESEncrypt256(string Input, string key)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Encoding.UTF8.GetBytes(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            string Output = Convert.ToBase64String(xBuff);
            return Output;
        }


        //AES_256 복호화
        public string AESDecrypt256(string Input, string key)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            var decrypt = aes.CreateDecryptor();
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Convert.FromBase64String(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            string Output = Encoding.UTF8.GetString(xBuff);
            return Output;
        }


        //AES_128 암호화
        public string AESEncrypt128(string Input, string key)
        {

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(Input);
            byte[] Salt = Encoding.ASCII.GetBytes(key.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(key, Salt);
            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

            cryptoStream.Write(PlainText, 0, PlainText.Length);
            cryptoStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            string EncryptedData = Convert.ToBase64String(CipherBytes);

            return EncryptedData;
        }

        //AE_S128 복호화
        public string AESDecrypt128(string Input, string key)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] EncryptedData = Convert.FromBase64String(Input);
            byte[] Salt = Encoding.ASCII.GetBytes(key.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(key, Salt);
            ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(EncryptedData);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

            byte[] PlainText = new byte[EncryptedData.Length];

            int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);

            memoryStream.Close();
            cryptoStream.Close();

            string DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

            return DecryptedData;
        }
    }
}
﻿using UnityEngine;

public class WeMonoBehaviour : MonoBehaviour
{
    #region Transform Wrapping

    // position
    public virtual Vector3 Position
    {
        get { return m_Tr.position; }
        set { m_Tr.position = value; }
    }

    // localPosition
    public virtual Vector3 LocalPosition
    {
        get { return m_Tr.localPosition; }
        set { m_Tr.localPosition = value; }
    }

    // rotation
    public virtual Quaternion Rotation
    {
        get { return m_Tr.rotation; }
        set { m_Tr.rotation = value; }
    }

    // localRotation
    public virtual Quaternion LocalRotation
    {
        get { return m_Tr.localRotation; }
        set { m_Tr.localRotation = value; }
    }

    // eulerAngles
    public virtual Vector3 EulerAngles
    {
        get { return m_Tr.eulerAngles; }
        set { m_Tr.eulerAngles = value; }
    }

    // localEulerAngles
    public virtual Vector3 LocalEulerAngles
    {
        get { return m_Tr.localEulerAngles; }
        set { m_Tr.localEulerAngles = value; }
    }

    // localScale
    public virtual Vector3 LocalScale
    {
        get { return m_Tr.localScale; }
        set { m_Tr.localScale = value; }
    }

    // transform
    public virtual Transform Tr
    {
        get { return m_Tr; }
    }
    #endregion

    protected Transform m_Tr;
      
    protected virtual void Awake()
    {
        m_Tr = transform;
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameUtil
{
    static int smokeState = -1;
    static float health = 0.0f;
    static float[] healthStep = new float[] { 0.8f, 0.6f, 0.4f, 0.2f, 0.0f };
    public static GameConfig.SmokeState CalcSmokeState(int curHealth, int maxHealth)
    {
        if(curHealth <= 0)
        {
            return GameConfig.SmokeState._4;
        }
        else
        {
            health = (float)curHealth / (float)maxHealth;
            smokeState = -1;
            for (int i = 0; i < healthStep.Length; i++)
            {
                if (healthStep[i] < health)
                {
                    // 0.9 -> -1
                    // 0.7 -> 0
                    // 0.53 -> 1
                    // 0.3 -> 2
                    // 0.1 -> 3
                    smokeState = i - 1;
                    break;
                }
            }

            return (GameConfig.SmokeState)smokeState;
        }
    }
}

﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;

using SimpleJSON;


namespace MadUtil {
	public class MadCrypt
	{
		const int HMAC_SIZE = 32;

		//reference: https://h5bak.tistory.com/148
		//AES_256 암호화
		public static string AESEncrypt256(string Input, string key, int ivSize)
		{
			RijndaelManaged aes = new RijndaelManaged();
			aes.KeySize = 256;
			aes.BlockSize = 256;
			aes.Mode = CipherMode.CBC;
			aes.Padding = PaddingMode.Zeros;

			var sha256Key = getSha256Hash (key);
			aes.Key = sha256Key;

			var ivTemp = new byte[ivSize];
			setRandIV (ivTemp);
			aes.IV= ivTemp;

			var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
			byte[] xBuff = null;
			using (var ms = new MemoryStream())
			{
				using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
				{
					byte[] xXml = System.Text.Encoding.UTF8.GetBytes(Input);
					cs.Write(xXml, 0, xXml.Length);
				}

				xBuff = ms.ToArray();
			}

			var outSize = xBuff.Length + ivSize + HMAC_SIZE;

			var outData = new byte[outSize];
			Buffer.BlockCopy (xBuff, 0, outData, 0, xBuff.Length);
			Buffer.BlockCopy (ivTemp, 0, outData, xBuff.Length, ivSize);

			var hmac= GenerateNewHMAC (key, xBuff);
			Buffer.BlockCopy (hmac, 0, outData, xBuff.Length + ivSize, HMAC_SIZE);

            string Output = Convert.ToBase64String(outData);

			return Output;
		}


		//AES_256 복호화
		public static string AESDecrypt256(string Input, string key, int ivSize)
		{
			byte[] inData = Convert.FromBase64String (Input);
			int cryptDataLen = inData.Length - ivSize - HMAC_SIZE;

			var hmac = new byte[HMAC_SIZE];
			Buffer.BlockCopy (inData, cryptDataLen +ivSize, hmac, 0, HMAC_SIZE);

			var iv = new byte[ivSize];
			Buffer.BlockCopy (inData, cryptDataLen, iv, 0, ivSize);

			var cryptData = new byte[cryptDataLen];
			Buffer.BlockCopy (inData, 0, cryptData, 0, cryptDataLen);

			var ckHmac = GenerateNewHMAC (key, cryptData);


			if (checkHmac(hmac, ckHmac) == false) 
			{
				//error!!!!
				return "";
			}	

			RijndaelManaged aes = new RijndaelManaged();
			aes.KeySize = 256;
			aes.BlockSize = 256;
			aes.Mode = CipherMode.CBC;
			aes.Padding = PaddingMode.Zeros;

			var sha256Key = getSha256Hash (key);
			aes.Key = sha256Key;

			aes.IV = iv;

			var decrypt = aes.CreateDecryptor();
			byte[] xBuff = null;
			using (var ms = new MemoryStream())
			{
				using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
				{
					byte[] xXml = cryptData;
					cs.Write(xXml, 0, xXml.Length);
				}

				xBuff = ms.ToArray();
			}

            string Output = Encoding.UTF8.GetString(xBuff);
			return Output;
		}

		//reference: https://msdn.microsoft.com/ko-kr/library/system.random.nextbytes(v=vs.110).aspx
		private static void setRandIV(byte[] arrIV) 
		{
			Random rnd = new Random ();
			rnd.NextBytes (arrIV);
		}

		//reference: https://insusu.tistory.com/entry/aspnet-MD5-SHA256-암호화-예제 
		private static byte[] getSha256Hash(string pw)
		{
			SHA256 sha= new SHA256Managed();
			byte[] hash= sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(pw));

			return hash;
			/*
			StringBuilder stringBuilder= new StringBuilder();
			foreach ( byte b in hash)
			{
				stringBuilder.AppendFormat("{0:x2}",b);
			}

			return stringBuilder.ToString();
			*/
		}

		//reference: https://wiki.inspircdorg/SHA256_HMAC_In_CSharp
		private static byte[] GenerateNewHMAC(string pw, byte[] challenge)
		{
			var sha256Key = getSha256Hash (pw);
			HMACSHA256 hmc = new HMACSHA256(sha256Key);
			byte[] hmres = hmc.ComputeHash(challenge);

			return hmres;
			//return System.Text.Encoding.UTF8.GetString(hmres);
		}

		private static bool checkHmac(byte[] hmac, byte[] checkHmac)
		{
			for (int i = 0; i < hmac.Length; ++i) 
			{
				if (hmac [i] != checkHmac [i]) 
				{
					return false;
				}
			}

			return true;
		}
	}
}
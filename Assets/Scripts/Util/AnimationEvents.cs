﻿using UnityEngine;
using System.Collections;

public class AnimationEvents : MonoBehaviour
{
    Animator m_Animator;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
    }

    void PlayBGM(string audioClipName)
    {
        SoundManager.instance.PlayBGM(audioClipName);
    }

    void StopBGM()
    {
        SoundManager.instance.StopBGM();
    }

    void PlaySFX(string audioClipName)
    {
        SoundManager.instance.PlaySFX(audioClipName);
    }

    void SetTrigger(string name)
    {
        if (m_Animator != null)
        {
            m_Animator.SetTrigger(name);
        }        
    }

    void SetBoolTrue(string name)
    {
        if (m_Animator != null)
        {
            m_Animator.SetBool(name, true);
        }
    }

    void SetBoolFalse(string name)
    {
        if (m_Animator != null)
        {
            m_Animator.SetBool(name, false);
        }
    }

    void StartShowTip()
    {
        GameManager.instance.StartShowTip();
    }

    void StartHideTip()
    {
        GameManager.instance.StartHideTip();
    }

    void RemoveIntroDecoration()
    {
        //Debug.Log("0 RemoveIntroDecoration!");
        GameManager.instance.RemoveIntroDecoration();
    }

    void PlayEngineSound()
    {
        //Debug.Log("1 PlayRollingWheel!");
        GameManager.instance.PlayEngineSound();
    }

    void PlayRollingWheel()
    {
        //Debug.Log("1 PlayRollingWheel!");
        GameManager.instance.PlayRollingWheel();
    }

    void PrepareGame()
    {
        //Debug.Log("2 PrepareGame!");
        GameManager.instance.PrepareGame();
    }

    void PlayGame()
    {
        //Debug.Log("3 PlayGame!");
        GameManager.instance.PlayGame();
    }

    void IntroCinemaPlayDriving()
    {
        GameManager.instance.IntroCinemaPlayDriving();
    }

    void EndShowShop()
    {
        ShopManager.instance.EndShow();
    }

    void EndHideShop()
    {
        ShopManager.instance.EndHide();
    }

    void ShowGachaInfo()
    {
        GachaManager.instance.ShowGachaInfo();
    }

    void FinishGacha()
    {
        GachaManager.instance.FinishGacha();
    }

    void ChangeGachaVehicle()
    {
        GachaManager.instance.ChangeGachaVehicle();
    }

    void EarnGift()
    {
        GameManager.instance.EarnGift();
    }
}

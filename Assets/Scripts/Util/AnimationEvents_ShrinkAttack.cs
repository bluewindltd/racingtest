﻿using UnityEngine;
using System.Collections;

public class AnimationEvents_ShrinkAttack : MonoBehaviour
{
    public ShrinkAttackAI shrinkAttackAI;

    void PlaySFX(string audioClipName)
    {
        SoundManager.instance.PlaySFX(audioClipName);
    }

    void EnableCollider()
    {
        shrinkAttackAI.EnableCollider();
    }

    void DisableCollider()
    {
        shrinkAttackAI.DisableCollider();
    }
}

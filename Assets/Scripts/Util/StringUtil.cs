﻿using UnityEngine;
using System;
using System.Collections;

public class StringUtil
{
    /*
    * 금액 타입으로 바꿔주는 함수
    */
    public static string LetMoneyTYPE(string numString)
    {
        if (numString.Contains("."))
        {
            //소수점 이하의 스트링을 저장한다.
            int Check = numString.Substring(numString.IndexOf('.')).Length;
            char[] delimiterChars = { '.' };
            string[] words = numString.Split(delimiterChars);
            string mntyNum = string.Format("{0:#,0}", int.Parse(words[0]));
            string ComplateNum = string.Format("{0}.{1}", mntyNum, words[1]);
            return ComplateNum;
        }
        else
        {
            string mntyNum = string.Format("{0:#,0}", int.Parse(numString));
            string ComplateNum = string.Format("{0}", mntyNum);
            return ComplateNum;
        }
    }

    public static string ConvertStringElapseTime(long time)
    {
        string timeString = "";
        if (time >= 0)
        {
            long second = time % 60;
            long allMin = time / 60;
            long min = allMin % 60;
            long hour = allMin / 60;

            timeString = string.Format("{0:00}:{1:00}:{2:00}", hour, min, second);
        }        

        return timeString;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolForNGUI
{
    private GameObject m_PoolObject;
    private List<GameObject> m_PoolList = new List<GameObject>();
    private int m_Number = 0;
    private GameObject m_ParentGameObject;

    public ObjectPoolForNGUI(int size, GameObject poolObject, GameObject parentGameObject)
    {
        m_PoolObject = poolObject;
        m_ParentGameObject = parentGameObject;

        for (int i = 0; i < size; i++)
        {
            GameObject _obj = NGUITools.AddChild(parentGameObject, m_PoolObject);
            _obj.name = m_PoolObject.name + "_" + m_Number;
            m_Number++;
            _obj.SetActive(false);
            //_obj.transform.Translate(0.0f, 100.0f, 0.0f);
            m_PoolList.Add(_obj);
        }
    }

    public GameObject GetObjectFromPool()
    {
        if (m_PoolList.Count > 0)
        {
            int objindex = Random.Range(0, m_PoolList.Count);

            GameObject _obj = m_PoolList[objindex];
            m_PoolList.RemoveAt(objindex);
            _obj.SetActive(true);
            return _obj;
        }
        else
        {
            GameObject _obj = NGUITools.AddChild(m_ParentGameObject, m_PoolObject);
            _obj.name = m_PoolObject.name + "_" + m_Number;
            m_Number++;

            return _obj;
        }
    }

    public bool IsFull()
    {
        if (m_PoolList.Count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void PutObjectInPool(GameObject obj)
    {
        obj.SetActive(false);
        //obj.transform.Translate(0.0f, 100.0f, 0.0f);
        m_PoolList.Add(obj);
    }
}

﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T m_instance;

    // 싱글턴 패턴 사용을 위한 속성.
    public static T instance
    {
        get
        {
            if (m_instance == null) // m_instance 가 널 이라면.
            {	// Find any object of one class or script name using Object.FindObjectsOfType or find the first object of one type using Object.FindObjectOfType. 
                m_instance = FindObjectOfType(typeof(T)) as T; // class 나 script의 이름으로 object를 찾거나 한 타입으로 첫번째 object를 찾아 T 객체 타입으로 캐스팅..
                if (m_instance == null) // 생성된 오브젝트가 없다면.
                {	// 새로운 GameObject 의 이름을 SingletonObject, 포함할 component에 T 스크립트를 넣어 생성하고 GetComponent를 이용해 스크립트를 가져온다.
                    m_instance = new GameObject(typeof(T).FullName + " Singleton Object", typeof(T)).GetComponent<T>(); // 새로 만들어 준다.
                }
            }
            return m_instance; // 
        }
    }

    void OnDestroy()
    {
        m_instance = null;
    }
}

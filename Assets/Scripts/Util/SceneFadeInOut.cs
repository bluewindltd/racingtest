﻿using UnityEngine;
using System.Collections;

public class SceneFadeInOut : MonoBehaviour
{
    public float m_FadeSpeed = 0.1f;          // Speed that the screen fades to and from black.
    
    UITexture m_Texture;
    string m_LevelName = "";
    int m_LevelIndex = -1;
    bool m_IsDoneFade = false;

    protected static SceneFadeInOut m_Instance;

    // 싱글턴 패턴 사용을 위한 속성.
    static SceneFadeInOut instance
    {
        get
        {
            if (m_Instance == null) // m_instance 가 널 이라면.
            {	// Find any object of one class or script name using Object.FindObjectsOfType or find the first object of one type using Object.FindObjectOfType. 
                m_Instance = FindObjectOfType(typeof(SceneFadeInOut)) as SceneFadeInOut; // class 나 script의 이름으로 object를 찾거나 한 타입으로 첫번째 object를 찾아 T 객체 타입으로 캐스팅..
                if (m_Instance == null) // 생성된 오브젝트가 없다면.
                {	// 새로운 GameObject 의 이름을 SingletonObject, 포함할 component에 T 스크립트를 넣어 생성하고 GetComponent를 이용해 스크립트를 가져온다.
                    m_Instance = new GameObject(typeof(SceneFadeInOut).FullName + " Singleton Object", typeof(SceneFadeInOut)).GetComponent<SceneFadeInOut>(); // 새로 만들어 준다.
                }
            }
            return m_Instance; // 
        }
    }

    void Start()
    {
        if(m_Texture == null)
        {
            m_Texture = GetComponent<UITexture>();
            m_Texture.color = Color.black;
        }        

        StartCoroutine(instance.StartScene());
    }

    void FadeToClear()
    {
        // Lerp the colour of the texture between itself and transparent.
        m_Texture.color = Color.Lerp(m_Texture.color, Color.clear, Time.deltaTime / m_FadeSpeed);
    }

    void FadeToBlack()
    {
        // Lerp the colour of the texture between itself and black.
        m_Texture.color = Color.Lerp(m_Texture.color, Color.black, Time.deltaTime / m_FadeSpeed);
    }

    IEnumerator StartScene()
    {
        m_Texture.color = Color.black;
        while (m_Texture.color.a > 0.05f)
        {
            yield return new WaitForEndOfFrame();

            FadeToClear();
        }

        m_Texture.color = Color.clear;
        m_Texture.enabled = false;
    }

    IEnumerator EndScene()
    {
        AsyncOperation async;
        if (m_LevelName != "")
        {
            async = Application.LoadLevelAsync(m_LevelName);
        }
        else
        {
            async = Application.LoadLevelAsync(m_LevelIndex);
        }
        async.allowSceneActivation = false;

        StartCoroutine(LoadLevelProgress(async));

        if (m_Texture == null)
        {
            m_Texture = GetComponent<UITexture>();
            m_Texture.color = Color.black;
        }
        // Make sure the texture is enabled.
        m_Texture.enabled = true;

        while (m_Texture.color.a <= 0.95f)
        {
            yield return new WaitForEndOfFrame();

            // Start fading towards black.
            FadeToBlack();
        }

        m_IsDoneFade = true;

        if (m_LevelName != "")
        {
            Application.LoadLevel(m_LevelName);
        }
        else
        {
            Application.LoadLevel(m_LevelIndex);
        }
    }

    IEnumerator LoadLevelProgress(AsyncOperation async)
    {
        while (async.isDone)
        {
            if (async.progress >= 0.9f && m_IsDoneFade == true)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        async.allowSceneActivation = true;

        yield return async;
    }

    public static void LoadLevel(int index)
    {
        instance.m_LevelIndex = index;
        instance.StartCoroutine(instance.EndScene());
    }

    public static void LoadLevel(string level)
    {
        instance.m_LevelName = level;
        instance.StartCoroutine(instance.EndScene());
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TireMarkManager : Singleton<TireMarkManager>
{
    public GameObject tireMarkPrefab;

    ObjectPool m_TireMarkObjectPool;

    Transform m_Transform;

    Dictionary<int, GameObject> m_TireObjectDic = new Dictionary<int, GameObject>();
    int m_TireMarkCount = 0;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    //void Start()
    //{
    //    m_Transform = GetComponent<Transform>();
    //    m_TireMarkObjectPool = new ObjectPool(100, tireMarkPrefab, m_Transform);
    //}

    public void Restore()
    {
        //GameObject tireMarkObject;
        //Dictionary<int, GameObject>.Enumerator etor = m_TireObjectDic.GetEnumerator();
        //while(etor.MoveNext())
        //{
        //    tireMarkObject = etor.Current.Value;
        //    tireMarkObject.transform.parent = m_Transform;
        //    m_TireMarkObjectPool.PutObjectInPool(tireMarkObject);
        //}
        //m_TireObjectDic.Clear();
        //m_TireMarkCount = 0;
    }

    public GameObject GetTireMark()
    {
        //GameObject tireMarkObject = m_TireMarkObjectPool.GetObjectFromPool();
        //CustomTireMark customTireMark = tireMarkObject.GetComponent<CustomTireMark>();
        //customTireMark.tireMarkID = m_TireMarkCount;
        //m_TireObjectDic.Add(m_TireMarkCount, tireMarkObject);
        //m_TireMarkCount++;
        //if(m_TireMarkCount > 1000)
        //{
        //    m_TireMarkCount = 0;
        //}

        //return tireMarkObject;
        return null;
    }

    public void PutTireMark(CustomTireMark customTireMark)
    {
        //customTireMark.transform.parent = m_Transform;
        //m_TireMarkObjectPool.PutObjectInPool(customTireMark.gameObject);
        //m_TireObjectDic.Remove(customTireMark.tireMarkID);
    }
}

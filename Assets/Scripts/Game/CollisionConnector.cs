﻿using UnityEngine;
using System.Collections;

public class CollisionConnector : MonoBehaviour
{
    public GameParameter gameParameter;
    public Rigidbody connectRigidbody;
    public PointData.Type pointType = PointData.Type.None;
    public ParticleSystem recoveryHPParticle;
    public ChaserAI chaserAI;   // 펫 공격 때문에 추가
}

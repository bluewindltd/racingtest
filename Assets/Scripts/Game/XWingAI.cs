﻿using UnityEngine;
using System.Collections;

public class XWingAI : WeMonoBehaviour
{
    public BattleParameter battleParam;
    public Transform[] laserTransforms;
    public Transform[] laserTargetTransforms;
    public AudioSource[] laserSFXs;
    public ParticleSystem[] laserParticles;

    Coroutine m_UpdateCoroutine = null;

    float m_CurrentAttackDelay = 0.0f;

    bool m_IsTargetVisible;

    SoundManager m_SoundMgr;
    GameManager m_GameMgr;
    int m_FlipLaser = 0;

    void Start()
    {
        m_IsTargetVisible = false;
        m_CurrentAttackDelay = 0.0f;

        m_SoundMgr = SoundManager.instance;
        m_GameMgr = GameManager.instance;

        Vector3 localTarget;
        float angleX;
        float angleY;
        Vector3 eulerAngleVelocity;
        Quaternion deltaRotation;
        for (int i = 0; i < laserTransforms.Length; i++)
        {
            localTarget = laserTransforms[i].InverseTransformPoint(laserTargetTransforms[i].position);
            angleX = Mathf.Atan2(localTarget.y, localTarget.z) * Mathf.Rad2Deg;
            angleY = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;            

            eulerAngleVelocity = new Vector3(-angleX, angleY, 0);
            deltaRotation = Quaternion.Euler(eulerAngleVelocity);
            laserTransforms[i].rotation = laserTransforms[i].rotation * deltaRotation;
        }
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    public void StopUpdate()
    {
        if (m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    void OnDrawGizmosSelected()
    {
        for (int i = 0; i < laserTransforms.Length; i++)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(laserTransforms[i].position, laserTransforms[i].position + laserTransforms[i].forward * 15.0f);
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            if (GameManager.instance.state == GameManager.State.Playing)
            {
                RaycastHit hit;

                for (int i = 0; i < 4; i++)
                {
                    //if (i == 2)
                    //{
                    //    yield return new WaitForSeconds(0.1f);

                    //    laserSFXs[i].pitch = Random.Range(0.8f, 1.2f);
                    //    laserSFXs[i].volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                    //    laserSFXs[i].Play();
                    //}

                    laserSFXs[i + (m_FlipLaser * 4)].pitch = Random.Range(0.8f, 1.2f);
                    laserSFXs[i + (m_FlipLaser * 4)].volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl * 0.25f;
                    laserSFXs[i + (m_FlipLaser * 4)].Play();

                    laserParticles[i + (m_FlipLaser * 4)].Stop();
                    laserParticles[i + (m_FlipLaser * 4)].Emit(1);

                    if (Physics.Raycast(laserTransforms[i + (m_FlipLaser * 4)].position, laserTransforms[i + (m_FlipLaser * 4)].forward, out hit, 30.0f, BattleControl.playerAttackDamageMaskStatic))
                    {
                        StartCoroutine(BulletFlight(hit.point, hit.collider.gameObject, laserTransforms[i + (m_FlipLaser * 4)].forward,
                            (Vector3.Distance(laserTransforms[i + (m_FlipLaser * 4)].transform.position, hit.point)) * 0.012f));
                    }

                    yield return new WaitForSeconds(battleParam.attackDelay);
                }

                m_FlipLaser = m_FlipLaser == 0 ? 1 : 0;

                //yield return new WaitForSeconds(battleParam.attackDelay);
            }
        }
    }

    IEnumerator BulletFlight(Vector3 point, GameObject target, Vector3 direction, float time)
    {
        yield return new WaitForSeconds(time);

        CollisionConnector collisionConnector = target.GetComponent<CollisionConnector>();

        if (collisionConnector != null)
        {
            if (collisionConnector.connectRigidbody != null)
            {
                if (target.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    collisionConnector.connectRigidbody.AddForce(direction * 50.0f, ForceMode.Acceleration);
                }
                //collisionConnector.connectRigidbody.AddForce(heliTransform.forward * 300.0f, ForceMode.Acceleration);

                m_GameMgr.ApplyDamage(collisionConnector, target, battleParam.attackDamage, false);
            }
        }
    }
}

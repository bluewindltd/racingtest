﻿using UnityEngine;
using System.Collections;

public class AttachObjectUI : WeMonoBehaviour
{
    Camera m_InGameUICamera;
    Transform m_TargetTransform = null;

    void Start()
    {
        m_InGameUICamera = GameObject.Find("InGameUIRoot/Camera").GetComponent<Camera>();
    }
    
    public void Attach(Transform targetTransform)
    {
        m_TargetTransform = targetTransform;
    }

    public void LateUpdate()
    {
        Vector3 v3 = Camera.main.WorldToViewportPoint(m_TargetTransform.position);
        // NGUI Camera ViewportToWorldPoint
        Vector3 myMenuPos = m_InGameUICamera.ViewportToWorldPoint(v3);
        float x = myMenuPos.x; // x offset
        float y = myMenuPos.y - 0.0777f;// + 0.2f; // y offset
        float z = myMenuPos.z; // y offset

        m_Tr.position = new Vector3(x, y, 0);
    }
}

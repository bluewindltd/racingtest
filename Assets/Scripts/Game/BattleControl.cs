﻿using UnityEngine;
using System.Collections;

public class BattleControl : MonoBehaviour
{
    public LayerMask enemyAttackBlockMask;
    public static LayerMask enemyAttackBlockMaskStatic;
    public LayerMask enemyAttackTargetMask;
    public static LayerMask enemyAttackTargetMaskStatic;
    public LayerMask enemyAttackDamageMask;
    public static LayerMask enemyAttackDamageMaskStatic;
    public LayerMask playerAttackBlockMask;
    public static LayerMask playerAttackBlockMaskStatic;
    public LayerMask playerAttackDamageMask;
    public static LayerMask playerAttackDamageMaskStatic;
    public LayerMask petAttackBlockMask;
    public static LayerMask petAttackBlockMaskStatic;
    public LayerMask petAttackDamageMask;
    public static LayerMask petAttackDamageMaskStatic;

    void Start ()
    {
        enemyAttackBlockMaskStatic = enemyAttackBlockMask;
        enemyAttackTargetMaskStatic = enemyAttackTargetMask;
        enemyAttackDamageMaskStatic = enemyAttackDamageMask;
        playerAttackBlockMaskStatic = playerAttackBlockMask;
        playerAttackDamageMaskStatic = playerAttackDamageMask;
        petAttackBlockMaskStatic = petAttackBlockMask;
        petAttackDamageMaskStatic = petAttackDamageMask;
    }
}

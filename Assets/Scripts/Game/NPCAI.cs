﻿using UnityEngine;
using System.Collections;

public class NPCAI : WeMonoBehaviour
{
    Rigidbody rb;
    VehicleParent vp;
    VehicleAssist va;

    public bool m_isDriving = false;

    public Vector3 targetPosition = Vector3.zero;
    Vector3 dirToTarget;
    float lookDot;
    float steerDot;

    public float fallLimit = -4;

    int m_Index = 0;
    public int index
    {
        get
        {
            return m_Index;
        }
        set
        {
            m_Index = value;
        }
    }

    int m_NPCCarID = -1;
    public int npcCarID
    {
        get
        {
            return m_NPCCarID;
        }
        set
        {
            m_NPCCarID = value;
        }
    }

    bool m_Initialize = false;

    public Vector3 spawnPos = Vector3.zero;
    public Quaternion spawnRot = Quaternion.identity;

    public float startDelay = 0.0f;
    float m_MaxSpeed = 0.0f;
    float m_CurrentSpeed = 0.0f;

    float m_MovementValue = 0.0f;

    public GameParameter gameParameter;

    RegionManager m_RegionMgr;

    public void Initialize()
    {
        gameParameter.shrink = false;

        m_MovementValue = 0.0f;

        m_RegionMgr = RegionManager.instance;

        rb = GetComponent<Rigidbody>();
        
        startDelay = UnityEngine.Random.Range(0.0f, 1.5f);
        m_MaxSpeed = UnityEngine.Random.Range(100f, 150f);
        m_CurrentSpeed = m_MaxSpeed;

        StartCoroutine(InitializePosition());
    }

    IEnumerator InitializePosition()
    {
        m_Tr.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        m_Tr.position = spawnPos;

        yield return new WaitForFixedUpdate();

        m_Tr.rotation = spawnRot;

        yield return new WaitForSeconds(startDelay);

        m_isDriving = true;
        m_Initialize = true;
    }

    void FixedUpdate()
    {        
        if (m_Initialize)
        {
            float distance = Vector3.Distance(m_RegionMgr.playerTransform.position, m_Tr.position);

            if (distance > 220.0f)
            {
                m_isDriving = false;
                m_Initialize = false;

                m_Tr.position = Vector3.zero;
                m_Tr.rotation = Quaternion.identity;
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
                NPCManager.instance.RemoveNPCCar(m_Index, m_NPCCarID, gameObject);
            }
            else
            {
                if (!RegionManager.instance.IsExistRegion(m_Tr.position.x, m_Tr.position.z))
                {
                    m_isDriving = false;
                    m_Initialize = false;

                    m_Tr.position = Vector3.zero;
                    m_Tr.rotation = Quaternion.identity;
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                    NPCManager.instance.RemoveNPCCar(m_Index, m_NPCCarID, gameObject);

                    return;
                }

                if (m_isDriving)
                {
                    Vector3 direction = m_Tr.localRotation * Vector3.forward;

                    RaycastHit hit;
                    //Debug.DrawRay(tr.position, direction * 15.0f, Color.cyan);
                    if (Physics.Raycast(m_Tr.position, direction, out hit, 25.0f, (1 << LayerMask.NameToLayer("StopPoint")) | (1 << LayerMask.NameToLayer("NPC Vehicles"))))
                    {
                        float distancef = Vector3.Distance(m_Tr.position, hit.point);
                        if (distancef > 5.0f)
                        {
                            m_MovementValue = 1.0f;
                            m_CurrentSpeed = Mathf.Lerp(m_CurrentSpeed, 40.0f, Time.deltaTime * 2.0f);
                        }
                        else
                        {
                            m_MovementValue = 0.0f;
                            m_CurrentSpeed = 0.0f;
                            m_isDriving = false;
                        }
                    }
                    else
                    {
                        m_MovementValue = 1.0f;
                        m_CurrentSpeed = m_MaxSpeed;
                    }

                    Move();
                    Turn();
                }
                else
                {
                    m_MovementValue = 0.0f;
                }

                if (m_Tr.position.y < fallLimit)
                {
                    m_isDriving = false;
                    m_Initialize = false;

                    m_Tr.position = Vector3.zero;
                    m_Tr.rotation = Quaternion.identity;
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                    NPCManager.instance.RemoveNPCCar(m_Index, m_NPCCarID, gameObject);
                }
            }                    
        }
        else
        {
            m_isDriving = false;

            m_MovementValue = 0.0f;
        }
    }

    void Move()
    {
        // Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
        Vector3 movement = m_Tr.forward * m_MovementValue * (m_CurrentSpeed * 0.2f) * Time.deltaTime;
        //Debug.Log("Move : " + m_MovementInputValue + "," + movement);

        // Apply this movement to the rigidbody's position.
        rb.MovePosition(rb.position + movement);

        //if (rb.velocity.sqrMagnitude <= 100.0f)
        //{
        //    rb.AddForce(m_MovementValue * m_Tr.forward * (10.0f * maxSpeed) * Time.deltaTime);
        //}
        //else
        //{
        //    rb.AddForce(m_MovementValue * m_Tr.forward * Time.deltaTime);
        //}     

        rb.AddRelativeForce(Vector3.down * 2.0f, ForceMode.Acceleration);
    }


    void Turn()
    {
        if(m_MovementValue != 0.0f)
        {
            //var targetDir = targetPosition - m_Tr.position;
            //var forward = m_Tr.forward;
            var localTarget = m_Tr.InverseTransformPoint(targetPosition);

            float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

            Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
            Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime);
            rb.MoveRotation(rb.rotation * deltaRotation);
        }        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles") ||
            other.gameObject.layer == LayerMask.NameToLayer("NPC Vehicles") ||
           other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles") ||
           other.gameObject.layer == LayerMask.NameToLayer("PoliceLine") ||
           other.gameObject.layer == LayerMask.NameToLayer("Building") ||
           other.gameObject.layer == LayerMask.NameToLayer("Obstacle") ||
           other.gameObject.layer == LayerMask.NameToLayer("Tree") ||
           other.gameObject.layer == LayerMask.NameToLayer("StopPoint"))
        {
            m_isDriving = false;
        }
    }

    //void OnCollisionEnter(Collision col)
    //{
    //    if (col.gameObject.layer == LayerMask.NameToLayer("Vehicles") ||
    //        col.gameObject.layer == LayerMask.NameToLayer("NPC Vehicles") ||
    //       col.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles") ||
    //       col.gameObject.layer == LayerMask.NameToLayer("Building") ||
    //       col.gameObject.layer == LayerMask.NameToLayer("Obstacle") ||
    //       col.gameObject.layer == LayerMask.NameToLayer("Tree") ||
    //       col.gameObject.layer == LayerMask.NameToLayer("StopPoint"))
    //    {
    //        m_isDriving = false;
    //    }
    //}
}

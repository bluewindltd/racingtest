﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissionData
{
    public GameConfig.MissionType type;
    public int paramX;
    public int paramY;
    public int rewardType;
    public int rewardAmount;
}

public class MissionInfo
{
    public MissionData missionData = null;
    public int destroyCar = 0;
    public Vector3 destinationGrid = Vector3.zero;
    public float time = 0.0f;
}

public class MissionManager : Singleton<MissionManager>
{
    public enum MissionState
    {
        Wait = 0,
        Start = 1,
        Progress = 2,
        End = 3,
    }
    MissionState m_MissionState;
    public MissionState missionState
    {
        get
        {
            return m_MissionState;
        }
        set
        {
            m_MissionState = value;
        }
    }

    public List<GameObject> missionTriggerPrefabList;
    List<ObjectPool> m_MissionTriggerPool = new List<ObjectPool>();

    int m_MissionTypeCount;
    int m_MissionCount = 0;

    Dictionary<int, MissionTrigger> m_MissionTriggerDic = new Dictionary<int, MissionTrigger>();
    public Dictionary<int, MissionTrigger> missionTriggerDic
    {
        get
        {
            return m_MissionTriggerDic;
        }
    }

    MissionData m_ChaserMissionData = new MissionData();
    MissionData m_DeliveryMissionData = new MissionData();

    MissionInfo m_ProgressMissionInfo = null;
    public MissionInfo progressMissionInfo
    {
        get
        {
            return m_ProgressMissionInfo;
        }
    }

    bool m_Initialize = false;
    GameManager m_GameMgr;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        m_MissionTypeCount = (int)GameConfig.MissionType.Delivery + 1;

        for (int i = 0; i < missionTriggerPrefabList.Count; i++)
        {
            ObjectPool missionTriggerObjectPool = new ObjectPool(10, missionTriggerPrefabList[i], transform);
            m_MissionTriggerPool.Add(missionTriggerObjectPool);
        }
    }

    public void StartUpdate()
    {
        m_Initialize = true;
        m_GameMgr = GameManager.instance;

        m_MissionState = MissionState.Wait;
        m_MissionCount = 0;
        m_MissionTriggerDic.Clear();

        // 테이블이 아직 없으므로 가상으로 추격, 배달 미션 파라미터 정보를 입력한다.
        m_ChaserMissionData.type = GameConfig.MissionType.Chase;
        m_ChaserMissionData.paramX = 2;      // 추격하는 갱스터 차량 수
        m_ChaserMissionData.paramY = 40;     // 미션 진행 시간
        m_ChaserMissionData.rewardType = 0;       // 보상 종류
        m_ChaserMissionData.rewardAmount = 5;   // 보상 수량

        m_DeliveryMissionData.type = GameConfig.MissionType.Delivery;
        m_DeliveryMissionData.paramX = 300;    // 배달해야할 최소 거리
        m_DeliveryMissionData.paramY = 60;     // 미션 진행 시간
        m_DeliveryMissionData.rewardType = 0;       // 보상 종류
        m_DeliveryMissionData.rewardAmount = 5;   // 보상 수량
    }

    void Update()
    {
        if(m_Initialize)
        {
            if (m_GameMgr.state != GameManager.State.GameOver &&
            m_MissionState == MissionState.Progress)
            {
                if (m_ProgressMissionInfo != null)
                {
                    m_ProgressMissionInfo.time += Time.deltaTime;
                    switch (m_ProgressMissionInfo.missionData.type)
                    {
                        case GameConfig.MissionType.Chase:
                            {
                                int chaserCarCount = m_ProgressMissionInfo.missionData.paramX - m_ProgressMissionInfo.destroyCar;
                                GameManager.instance.ProgressMission(string.Format(TextDocs.content("UI_MISSION_DESC_03"), chaserCarCount),
                                    string.Format(TextDocs.content("UI_MISSION_DESC_02"), m_ProgressMissionInfo.missionData.paramY - Mathf.FloorToInt(m_ProgressMissionInfo.time)));
                                if (m_ProgressMissionInfo.time >= (float)m_ProgressMissionInfo.missionData.paramY)
                                {
                                    // 추격을 버텨냈으므로 성공
                                    // 시간이 지나서 미션 완료시에는 현재 추격 중 인 차량을 모두 없애준다.(폭파 연출? 현재 임시 좀 더 보강하면 좋을듯)
                                    ChaserManager.instance.RemoveAllGangCar();
                                    EndMission(true);
                                }
                                else
                                {
                                    // 추격 차량을 모두 파괴했으므로 성공
                                    if (m_ProgressMissionInfo.destroyCar >= m_ProgressMissionInfo.missionData.paramX)
                                    {
                                        EndMission(true);
                                    }
                                }
                            }
                            break;

                        case GameConfig.MissionType.Delivery:
                            {
                                Transform playerTransform = RegionManager.instance.playerTransform;

                                float regionSize = RegionManager.instance.m_RegionSize;
                                Vector3 destinationPos = new Vector3(m_ProgressMissionInfo.destinationGrid.x * regionSize,
                                    0.0f,
                                    m_ProgressMissionInfo.destinationGrid.z * regionSize);
                                float distance = Vector3.Distance(playerTransform.position, destinationPos);

                                Vector3 localTarget = playerTransform.InverseTransformPoint(destinationPos);
                                float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

                                GameManager.instance.RotateArrow(angle);
                                GameManager.instance.ProgressMission(string.Format(TextDocs.content("UI_MISSION_DESC_01"), (int)distance),
                                    string.Format(TextDocs.content("UI_MISSION_DESC_02"), m_ProgressMissionInfo.missionData.paramY - Mathf.FloorToInt(m_ProgressMissionInfo.time)));

                                if (m_ProgressMissionInfo.time >= (float)m_ProgressMissionInfo.missionData.paramY)
                                {
                                    EndMission(false);
                                }
                            }
                            break;
                    }
                }
            }
        }        
    }

    public void DestroyChaserCar()
    {
        if(m_ProgressMissionInfo != null)
        {
            m_ProgressMissionInfo.destroyCar++;
        }
    }

    public MissionTrigger GenerateMission()
    {
        int missionType = UnityEngine.Random.Range(0, m_MissionTypeCount);
        //int missionType = 1;

        if (missionType < m_MissionTriggerPool.Count)
        {
            GameObject missionTriggerObject = m_MissionTriggerPool[missionType].GetObjectFromPool();
            MissionTrigger missionTrigger = missionTriggerObject.GetComponent<MissionTrigger>();
            missionTrigger.missionIndex = m_MissionCount;
            m_MissionTriggerDic.Add(m_MissionCount, missionTrigger);
            m_MissionCount++;

            if(m_MissionState != MissionState.Wait)
            {
                missionTriggerObject.SetActive(false);
            }
            else
            {
                missionTrigger.IdleMission();
            }

            return missionTrigger;
        }
        else
        {
            return null;
        }
    }

    public ObjectPool GetMissionTriggerPool(GameConfig.MissionType missionType)
    {
        if ((int)missionType < m_MissionTriggerPool.Count)
        {
            return m_MissionTriggerPool[(int)missionType];
        }
        else
        {
            return null;
        }
    }

    public void RemoveMissionTrigger(int missionIndex)
    {
        //Debug.Log("미션 트리거 제거 : " + questIndex);
        if (m_MissionTriggerDic.ContainsKey(missionIndex))
        {
            m_MissionTriggerDic.Remove(missionIndex);
        }
    }

    public void ProgressMissionTrigger(int missionIndex)
    {
        if (m_MissionTriggerDic.ContainsKey(missionIndex))
        {
            MissionTrigger progressMissionTrigger = m_MissionTriggerDic[missionIndex];

            m_ProgressMissionInfo = new MissionInfo();
            if (progressMissionTrigger.type == GameConfig.MissionType.Chase)
            {
                m_ProgressMissionInfo.missionData = m_ChaserMissionData;
                m_ProgressMissionInfo.destroyCar = 0;
                m_ProgressMissionInfo.time = 0.0f;
            }
            else if (progressMissionTrigger.type == GameConfig.MissionType.Delivery)
            {
                m_ProgressMissionInfo.missionData = m_DeliveryMissionData;
                Transform playerTransform = RegionManager.instance.playerTransform;
                float regionSize = RegionManager.instance.m_RegionSize;
                float playerX = playerTransform.position.x;
                float playerZ = playerTransform.position.z;
                float angle = UnityEngine.Random.Range(0.0f, 360.0f);
                Vector3 destinationDirection = Quaternion.AngleAxis(angle, Vector3.up) * playerTransform.forward * (float)m_ProgressMissionInfo.missionData.paramX;
                m_ProgressMissionInfo.destinationGrid.x = Mathf.RoundToInt((playerX + destinationDirection.x) / regionSize);
                m_ProgressMissionInfo.destinationGrid.y = 0.0f;
                m_ProgressMissionInfo.destinationGrid.z = Mathf.RoundToInt((playerZ + destinationDirection.z) / regionSize);

                m_ProgressMissionInfo.time = 0.0f;
            }

            Dictionary<int, MissionTrigger>.Enumerator etor = m_MissionTriggerDic.GetEnumerator();
            while (etor.MoveNext())
            {
                if(missionIndex != etor.Current.Value.missionIndex)
                {
                    etor.Current.Value.gameObject.SetActive(false);
                }
            }

            StartMission();
        }
    }

    public void AllWaitMissionTrigger()
    {
        m_MissionState = MissionState.Wait;
        m_ProgressMissionInfo = null;

        if(m_GameMgr.state != GameManager.State.GameOver)
        {
            Dictionary<int, MissionTrigger>.Enumerator etor = m_MissionTriggerDic.GetEnumerator();
            while (etor.MoveNext())
            {
                etor.Current.Value.gameObject.SetActive(true);
                etor.Current.Value.IdleMission();
            }
        }        

        GameManager.instance.WaitMission();
    }

    public void StartMission()
    {
        StartCoroutine(StartMissionCoroutine());
    }

    IEnumerator StartMissionCoroutine()
    {
        //SoundManager.instance.PlaySFX("MissionStart");

        m_MissionState = MissionState.Start;

        if (m_ProgressMissionInfo != null)
        {
            if(m_ProgressMissionInfo.missionData.type == GameConfig.MissionType.Chase)
            {
                GameManager.instance.StartMission(TextDocs.content("UI_MISSION_NOTICE_02"), m_ProgressMissionInfo.missionData.rewardAmount);
            }
            else if(m_ProgressMissionInfo.missionData.type == GameConfig.MissionType.Delivery)
            {
                GameManager.instance.StartMission(TextDocs.content("UI_MISSION_NOTICE_01"), m_ProgressMissionInfo.missionData.rewardAmount);
                RegionManager.instance.ActiveDeliveryMissionDestination(m_ProgressMissionInfo);
            }
        }

        yield return new WaitForSeconds(1.5f);

        GameManager.instance.ChangeMission();

        yield return new WaitForSeconds(0.3f);

        GameManager.instance.ShowingMission();

        yield return new WaitForSeconds(0.2f);

        m_MissionState = MissionState.Progress;
    }

    public void EndMission(bool complete)
    {
        if(m_MissionState == MissionState.Progress)
        {
            StartCoroutine(EndMissionCoroutine(complete));
        }        
    }

    IEnumerator EndMissionCoroutine(bool complete)
    {
        m_MissionState = MissionState.End;

        if (m_ProgressMissionInfo != null)
        {
            if (m_ProgressMissionInfo.missionData.type == GameConfig.MissionType.Chase)
            {
                if (complete)
                {
#if BWQUEST
                    QuestManager.instance.CheckQuestData(QuestData.Type.ChaseMission);
#endif
                    GameManager.instance.EndMission(TextDocs.content("UI_MISSIONSUCCESS_TEXT"), complete, m_ProgressMissionInfo.missionData.rewardAmount);
                }
                else
                {
                    GameManager.instance.EndMission(TextDocs.content("UI_MISSIONFAILED_TEXT"), complete, m_ProgressMissionInfo.missionData.rewardAmount);
                }
            }
            else if (m_ProgressMissionInfo.missionData.type == GameConfig.MissionType.Delivery)
            {
                if (complete)
                {
#if BWQUEST
                    QuestManager.instance.CheckQuestData(QuestData.Type.DeliveryMission);
#endif
                    GameManager.instance.EndMission(TextDocs.content("UI_MISSIONSUCCESS_TEXT"), complete, m_ProgressMissionInfo.missionData.rewardAmount);
                }
                else
                {
                    GameManager.instance.EndMission(TextDocs.content("UI_MISSIONFAILED_TEXT"), complete, m_ProgressMissionInfo.missionData.rewardAmount);
                }
            }
        }

        if(complete)
        {
            yield return new WaitForSeconds(2.0f);
        }
        else
        {
            yield return new WaitForSeconds(1.1f);
        }


        AllWaitMissionTrigger();
    }
}

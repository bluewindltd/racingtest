﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoliceLineManager : Singleton<PoliceLineManager>
{
    public enum PoliceLineGrade
    {
        NoGrade = -1,
        Grade1 = 0,
        Grade2,
        Grade3,
        Grade4,
    };
    public List<GameObject> policeLineGroupPrefabList;
    public float makeDelay = 5.0f;
    List<ObjectPool> m_PoliceLineGroupPool = new List<ObjectPool>();

    Dictionary<int, List<PoliceLineGrade>> m_PoliceLineGradeDic = new Dictionary<int, List<PoliceLineGrade>>();

    GameManager m_GameMgr = null;
    ChaserDocs m_ChaserDocs = null;
    float m_MakeTime = 0.0f;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        m_ChaserDocs = ChaserDocs.instance;

        ChaserData chaserData;
        GameParameter gameParam;
        BattleParameter battleParam;
        for (int i = 0; i < policeLineGroupPrefabList.Count; i++)
        {
            PoliceLineGroup policeLineGroup = policeLineGroupPrefabList[i].GetComponent<PoliceLineGroup>();

            if (m_ChaserDocs.chaserDataDic.ContainsKey(policeLineGroup.chaserType))
            {
                chaserData = m_ChaserDocs.chaserDataDic[policeLineGroup.chaserType];
                for(int j = 0; j < policeLineGroup.policeLineGameParamArray.Length; j++)
                {
                    gameParam = policeLineGroup.policeLineGameParamArray[j];
                    if (gameParam != null)
                    {
                        gameParam.maxHealth = chaserData.health;
                        gameParam.damage = chaserData.damage;
                    }
                }

                for (int j = 0; j < policeLineGroup.policeLineBattleParamArray.Length; j++)
                {
                    battleParam = policeLineGroup.policeLineBattleParamArray[j];
                    if (battleParam != null)
                    {
                        battleParam.attackDamage = chaserData.attackDamage;
                        battleParam.attackDelay = chaserData.attackDelay;
                        battleParam.shellSpeed = chaserData.shellSpeed;
                        battleParam.explosionRadius = chaserData.explosionRadius;
                    }
                }
            }
        }

        for (int i = 0; i < policeLineGroupPrefabList.Count; i++)
        {
            ObjectPool policeLineGroupObjectPool = new ObjectPool(9, policeLineGroupPrefabList[i], transform);
            m_PoliceLineGroupPool.Add(policeLineGroupObjectPool);
        }

        List<PoliceLineGrade> policeLineGradeList = new List<PoliceLineGrade>();
        policeLineGradeList.Add(PoliceLineGrade.NoGrade);
        m_PoliceLineGradeDic.Add(0, policeLineGradeList);

        policeLineGradeList = new List<PoliceLineGrade>();
        policeLineGradeList.Add(PoliceLineGrade.NoGrade);
        m_PoliceLineGradeDic.Add(1, policeLineGradeList);

        policeLineGradeList = new List<PoliceLineGrade>();
        policeLineGradeList.Add(PoliceLineGrade.Grade1);
        policeLineGradeList.Add(PoliceLineGrade.Grade2);
        m_PoliceLineGradeDic.Add(2, policeLineGradeList);

        policeLineGradeList = new List<PoliceLineGrade>();
        policeLineGradeList.Add(PoliceLineGrade.Grade2);
        m_PoliceLineGradeDic.Add(3, policeLineGradeList);

        policeLineGradeList = new List<PoliceLineGrade>();
        policeLineGradeList.Add(PoliceLineGrade.Grade2);
        policeLineGradeList.Add(PoliceLineGrade.Grade3);
        m_PoliceLineGradeDic.Add(4, policeLineGradeList);

        policeLineGradeList = new List<PoliceLineGrade>();
        policeLineGradeList.Add(PoliceLineGrade.Grade3);
        m_PoliceLineGradeDic.Add(5, policeLineGradeList);

        policeLineGradeList = new List<PoliceLineGrade>();
        policeLineGradeList.Add(PoliceLineGrade.Grade4);
        m_PoliceLineGradeDic.Add(6, policeLineGradeList);
    }

    public void StartUpdate()
    {
        m_GameMgr = GameManager.instance;
    }

    public PoliceLineGroup GeneratePoliceLineGroup()
    {
        int evilCount = m_GameMgr.evilCount;
        if(evilCount != 0 && evilCount != 1)
        {
            //int regenRate = UnityEngine.Random.Range(0, 100);
            //if(regenRate < 20)
            //{
            float currentTime = TimeManager.instance.currentGameTime;
            if (currentTime - m_MakeTime >= makeDelay)
            {
                m_MakeTime = currentTime;
                List<PoliceLineGrade> policeLineGradeList = m_PoliceLineGradeDic[evilCount];
                int policeLineGradeIndex = UnityEngine.Random.Range(0, policeLineGradeList.Count);
                PoliceLineGrade policeLineGrade = policeLineGradeList[policeLineGradeIndex];
                GameObject policeLineGroupObject = m_PoliceLineGroupPool[(int)policeLineGrade].GetObjectFromPool();
                PoliceLineGroup policeLineGroup = policeLineGroupObject.GetComponent<PoliceLineGroup>();
                return policeLineGroup;
            }
            else
            {
                return null;
            }
    }
        else
        {
            return null;
        }        
    }

    public void RestorePoliceLineGroup(PoliceLineGroup policeLineGroup)
    {
        m_PoliceLineGroupPool[(int)policeLineGroup.policeLineGrade].PutObjectInPool(policeLineGroup.gameObject);
    }
}

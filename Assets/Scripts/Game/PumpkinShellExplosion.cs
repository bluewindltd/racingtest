﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PumpkinShellExplosion : WeMonoBehaviour
{
    BattleParameter m_BattleParam;
    public BattleParameter battleParam
    {
        set
        {
            m_BattleParam = value;
        }
    }
    public float m_MaxLifeTime = 2f;
    Coroutine m_PutObjectPoolCoroutine = null;
    Vector3 m_ExplosionPos = Vector3.zero;

    int m_ShellID = -1;
    public int shellID
    {
        get
        {
            return m_ShellID;
        }
        set
        {
            m_ShellID = value;
        }
    }
    List<GameObject> m_ColliderObject = new List<GameObject>();

    GameManager m_GameMgr;

    private void Start()
    {
        m_PutObjectPoolCoroutine = StartCoroutine(DestroyCoroutine());

        m_GameMgr = GameManager.instance;
    }

    IEnumerator DestroyCoroutine()
    {
        yield return new WaitForSeconds(m_MaxLifeTime);

        ShellManager.instance.PutPumpkinShell(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_Tr.position.y < 0.0f)
        {
            m_ExplosionPos = new Vector3(m_Tr.position.x, 0.0f, m_Tr.position.z);
        }
        else
        {
            m_ExplosionPos = m_Tr.position;
        }
        EffectManager.instance.CreatePumpkinEffect(null, m_ExplosionPos, Quaternion.identity, m_BattleParam.explosionRadius);

        bool sameObject = false;
        CollisionConnector collisionConnector = null;
        Collider[] colliders = Physics.OverlapSphere(m_ExplosionPos, m_BattleParam.explosionRadius, BattleControl.playerAttackDamageMaskStatic);
        m_ColliderObject.Clear();

        for (int i = 0; i < colliders.Length; i++)
        {
            Collider hit = colliders[i];
            if (hit)
            {
                collisionConnector = hit.GetComponent<CollisionConnector>();
                if (collisionConnector != null)
                {
                    for (int j = 0; j < m_ColliderObject.Count; j++)
                    {
                        if (hit.gameObject == m_ColliderObject[j])
                        {
                            sameObject = true;
                            break;
                        }
                    }
                    if (!sameObject)
                    {
                        if (collisionConnector.connectRigidbody != null)
                        {
                            collisionConnector.connectRigidbody.AddForce(Vector3.up * 20.0f, ForceMode.Acceleration);
                            //collisionConnector.connectRigidbody.AddExplosionForce(30.0f, m_Tr.position, m_BattleParam.explosionRadius, 3.0f, ForceMode.Acceleration);
                        }

                        m_GameMgr.ApplyDamage(collisionConnector, hit.gameObject, m_BattleParam.attackDamage, false);

                        m_ColliderObject.Add(hit.gameObject);
                    }
                }
            }
        }

        if (m_PutObjectPoolCoroutine != null)
        {
            StopCoroutine(m_PutObjectPoolCoroutine);
        }

        ShellManager.instance.PutPumpkinShell(this);
    }
}

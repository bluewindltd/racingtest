﻿using UnityEngine;
using System.Collections;

public class HummerAI : WeMonoBehaviour
{
    public ChaserAI chaserAI;
    public BattleParameter battleParam;
    public Transform gunMuzzleTransform;
    public Transform gunMuzzleEffectTransform;
    public ParticleSystem machineGunParticle;
    public AudioSource gunMuzzleSFX;
    public Transform gunRotorTransform;
    public GameObject bulletImpactEmitterObject;

    float m_CurrentAttackDelay = 0.0f;

    Rigidbody m_Rigidbody;
    bool m_IsTargetVisible;

    float m_TempDistance = 0.0f;

    float m_GunHitRadius = 5.0f;

    SoundManager m_SoundMgr;
    RegionManager m_RegionMgr;

    protected override void Awake()
    {
        base.Awake();

        m_Rigidbody = GetComponent<Rigidbody>();
        m_SoundMgr = SoundManager.instance;
    }

    public void Initialize()
    {
        m_CurrentAttackDelay = 0.0f;
        m_RegionMgr = RegionManager.instance;
        m_IsTargetVisible = false;
        m_GunHitRadius = 5.0f;
    }
    
    void Update()
    {
        if (chaserAI.isDisable == false)
        {
            Vector3 localTarget = gunRotorTransform.InverseTransformPoint(m_RegionMgr.playerTransform.position);

            float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

            Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
            Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime * battleParam.rotorSpeed);
            gunRotorTransform.rotation = gunRotorTransform.rotation * deltaRotation;

            m_CurrentAttackDelay += Time.deltaTime;
            if (m_CurrentAttackDelay >= battleParam.attackDelay)
            {
                m_TempDistance = Vector3.Distance(m_Tr.position, m_RegionMgr.playerTransform.position);
                if (m_TempDistance < 15.0f)
                {
                    m_IsTargetVisible = !Physics.Linecast(gunMuzzleTransform.position, m_RegionMgr.playerTransform.position, BattleControl.enemyAttackBlockMaskStatic);
                    // Hummer 차량의 머신건 총구와 플레이어 간에 가리는 장애물이 없는지 체크
                    if (m_IsTargetVisible)
                    {
                        m_CurrentAttackDelay = 0.0f;

                        if (GameManager.instance.state == GameManager.State.Playing)
                        {
                            gunMuzzleSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                            gunMuzzleSFX.Play();
                        }

                        machineGunParticle.Emit(1);

                        EffectManager.instance.CreateGunEffect(m_Tr, gunMuzzleEffectTransform.position, gunMuzzleEffectTransform.rotation);

                        //Vector3 gunShootDirection = gunMuzzleTransform.forward + (-gunMuzzleTransform.up * 0.2f);
                        //Debug.DrawRay(gunMuzzleTransform.position, gunMuzzleTransform.forward * 10.0f, Color.red);
                        RaycastHit hit;
                        if (Physics.Raycast(gunMuzzleTransform.position, gunMuzzleTransform.forward, out hit, 10.0f, BattleControl.enemyAttackDamageMaskStatic))
                        {
                            StartCoroutine(BulletFlight(hit.point, hit.collider.gameObject, gunMuzzleTransform.forward,
                                    (Vector3.Distance(gunMuzzleTransform.transform.position, hit.point)) * 0.012f));
                        }
                    }                    
                }
            }

            //if (chaserAI.isTargetVisible)
            //{
            //    m_CurrentGunDelay += Time.deltaTime;
            //    if (m_CurrentGunDelay >= gunDelay)
            //    {
            //    }

            //    Vector3 gunShootDirection = gunMuzzleTransform.forward + (-gunMuzzleTransform.up * 0.2f);
            //    Debug.DrawRay(gunMuzzleTransform.position, gunShootDirection * 10.0f, Color.red);

            //    float distance = Vector3.Distance(m_Tr.position, m_RegionMgr.playerTransform.position);
            //    if (distance < 15.0f)
            //    {

            //            gunMuzzleSFX.Play();
            //            //gunMuzzleEmitter.emit = true;
            //            EffectManager.instance.CreateGunEffect(m_Tr, gunMuzzleEffectTransform.position, gunMuzzleEffectTransform.rotation);

            //            m_CurrentGunDelay = 0.0f;

            //            RaycastHit hit;
            //            if (Physics.Raycast(gunMuzzleTransform.position, gunShootDirection, out hit, 10.0f, (1 << LayerMask.NameToLayer("Vehicles"))))
            //            {
            //                Collider[] colliders = Physics.OverlapSphere(hit.point, m_GunHitRadius);
            //                for (int i = 0; i < colliders.Length; i++)
            //                {
            //                    Collider tempCollider = colliders[i];
            //                    if (tempCollider)
            //                    {
            //                        CollisionConnector collisionConnector = tempCollider.GetComponent<CollisionConnector>();
            //                        if (collisionConnector != null)
            //                        {
            //                            if (collisionConnector.connectRigidbody != null)
            //                            {
            //                                //collisionConnector.connectRigidbody.AddExplosionForce(10.0f, hit.point, m_GunHitRadius, 10.0f);
            //                                collisionConnector.connectRigidbody.AddForce(gunShootDirection.normalized * 300.0f, ForceMode.Acceleration);
            //                                Instantiate(bulletImpactEmitterObject, hit.point, Quaternion.LookRotation(hit.normal));
            //                                //GameObject emitterObject = m_BulletImpactEmitterObjectPool.GetObjectFromPool();
            //                                //Transform emitterTransform = emitterObject.transform;
            //                                //emitterTransform.position = hit.point;
            //                                //emitterTransform.rotation = Quaternion.LookRotation(hit.normal);
            //                                if (collisionConnector.gameParameter != null)
            //                                {
            //                                    collisionConnector.gameParameter.currentHealth -= gunDamage;
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            //gunMuzzleEmitter.emit = false;
            //        }
            //    }
            //    else
            //    {
            //        m_CurrentGunDelay = 0.0f;
            //    }
            //}
        }
    }

    IEnumerator BulletFlight(Vector3 point, GameObject target, Vector3 direction, float time)
    {
        yield return new WaitForSeconds(time);

        CollisionConnector collisionConnector = target.GetComponent<CollisionConnector>();

        if (collisionConnector != null)
        {
            if (collisionConnector.connectRigidbody != null)
            {
                if (target.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    collisionConnector.connectRigidbody.AddForce(direction * 500.0f, ForceMode.Acceleration);
                }
                //Instantiate(bulletImpactEmitterObject, point, Quaternion.LookRotation(hitNormal));

                if (collisionConnector.gameParameter != null)
                {
                    collisionConnector.gameParameter.currentHealth -= battleParam.attackDamage;
                }
            }
        }
    }
}

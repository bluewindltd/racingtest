﻿using UnityEngine;
using System.Collections;

public class AkiraBikeLandMine : MonoBehaviour
{
    public Transform landMineTransform;
    public Renderer landMineRenderer;
    public Color origColor;
    public Color destColor;

    int m_LandMineIndex = 0;
    public int landMineIndex
    {
        get
        {
            return m_LandMineIndex;
        }
        set
        {
            m_LandMineIndex = value;
        }
    }

    AkiraBikeAI m_AkiraBikeAI;
    public AkiraBikeAI akiraBikeAI
    {
        set
        {
            m_AkiraBikeAI = value;
        }
    }

    BattleParameter m_BattleParam;
    public BattleParameter battleParam
    {
        set
        {
            m_BattleParam = value;
        }
    }

    bool m_IsRunExplosion = false;

    Coroutine m_ExplosionTimer = null;
    float m_Timer = 0.0f;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            PlayExplosion();
        }
    }

    void PlayExplosion()
    {
        if (!m_IsRunExplosion)
        {
            EffectManager.instance.CreateLandMineEffect(null, landMineTransform.position, Quaternion.identity, m_BattleParam.explosionRadius);

            m_IsRunExplosion = true;
            Collider[] colliders = Physics.OverlapSphere(landMineTransform.position, m_BattleParam.explosionRadius, BattleControl.playerAttackDamageMaskStatic);
            foreach (Collider c in colliders)
            {
                CollisionConnector collisionConnector = c.gameObject.GetComponent<CollisionConnector>();
                if (collisionConnector != null)
                {
                    collisionConnector.connectRigidbody.AddExplosionForce(2.0f, landMineTransform.position, m_BattleParam.explosionRadius, 1.3f, ForceMode.Impulse);

                    GameManager.instance.ApplyDamage(collisionConnector, c.gameObject, m_BattleParam.attackDamage, false);
                }
            }

            if (m_ExplosionTimer != null)
            {
                StopCoroutine(m_ExplosionTimer);
                m_ExplosionTimer = null;
            }

            m_AkiraBikeAI.ReturnLandMine(this);
        }
    }

    IEnumerator StartExplosionTimer()
    {
        while (true)
        {
            m_Timer += Time.deltaTime;

            if (m_Timer > m_BattleParam.rotorSpeed)
            {
                m_Timer = 0.0f;
                landMineRenderer.material.color = origColor;
                break;
            }

            Color colorTemp = Color.Lerp(origColor, destColor, m_Timer / m_BattleParam.rotorSpeed);
            landMineRenderer.material.color = colorTemp;

            yield return null;
        }

        PlayExplosion();        
    }

    public void Fire()
    {
        m_Timer = 0.0f;
        landMineRenderer.material.color = origColor;
        m_IsRunExplosion = false;

        if(m_ExplosionTimer == null)
        {
            m_ExplosionTimer = StartCoroutine(StartExplosionTimer());
        }
    }
}

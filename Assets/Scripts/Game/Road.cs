﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Road : MonoBehaviour
{
    Transform m_ObjectPoolParent = null;
    GameConfig.RoadType m_RoadType;
    public GameConfig.RoadType roadType
    {
        get
        {
            return m_RoadType;
        }
    }
    public GameConfig.Direction m_Direction;
    public GameConfig.Direction direction
    {
        get
        {
            return m_Direction;
        }
    }

    public List<Transform> chaserSpawnPointList;
    public List<Transform> eastNPCSpawnPointList;
    public List<Transform> westNPCSpawnPointList;
    public List<Transform> southNPCSpawnPointList;
    public List<Transform> northNPCSpawnPointList;
    public List<Transform> policeLineSpawnPointList;

    Dictionary<int, bool> m_NPCCarSpawnDic = new Dictionary<int, bool>();
    public Dictionary<int, bool> npcCarSpawnDic
    {
        get
        {
            return m_NPCCarSpawnDic;
        }
    }

    public void SetRoad(Transform parentRegion, GameConfig.RoadType roadType, GameConfig.Direction direction)
    {
        m_ObjectPoolParent = transform.parent;

        m_Direction = direction;

        transform.parent = parentRegion;
        m_RoadType = roadType;
        transform.localPosition = Vector3.zero;
        transform.rotation = Quaternion.Euler(0.0f, 90.0f * (float)direction, 0f);

        m_NPCCarSpawnDic.Clear();
        m_NPCCarSpawnDic.Add((int)RegionOutput.Direction.E, false);
        m_NPCCarSpawnDic.Add((int)RegionOutput.Direction.W, false);
        m_NPCCarSpawnDic.Add((int)RegionOutput.Direction.S, false);
        m_NPCCarSpawnDic.Add((int)RegionOutput.Direction.N, false);
    }

    public void Restore()
    {
        //Debug.Log("Road Restore 0 : " + gameObject.name);
        if (m_ObjectPoolParent != null)
        {
            //Debug.Log("Road Restore 0-1 : " + gameObject.name);
            transform.parent = m_ObjectPoolParent;
            m_ObjectPoolParent = null;
            //Debug.Log("Road Restore 0-2 : " + gameObject.name);
        }
        transform.localPosition = Vector3.zero;
        transform.rotation = Quaternion.identity;

        //Debug.Log("Road Restore 1 : " + gameObject.name);

        ObjectPool roadObjectPool = RegionManager.instance.GetRoadObjectPool(m_RoadType);
        roadObjectPool.PutObjectInPool(gameObject);

        //Debug.Log("Road Restore 2 : " + gameObject.name);
    }
}
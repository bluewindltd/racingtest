﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GetCashEffectGroup : MonoBehaviour
{
    public GameObject getCashEffectPrefab;
    public GameObject startCashObject;
    public GameObject destCashObject;
    public GameObject getCashEffectPool;

    List<GameObject> m_GetCashEffectObjectList = new List<GameObject>();
    List<GetCashEffect> m_GetCashEffectList = new List<GetCashEffect>();
    List<bool> m_PoolUseList = new List<bool>();
    int m_PoolUseIndex = 0;
    Vector3 m_StartPosition;
    Vector3[] m_Path = null;
    Vector3 m_SplineStartPosition;
    Vector3 m_SplineMiddlePosition;
    Vector3 m_SplineEndPosition;
    LTSpline m_Spline = null;

    void Start()
    {
        // ScoreCourseEffect Pool을 생성한다.
        for (int i = 0; i < GameConfig.MaxGetCoin; i++)
        {
            GameObject getCashEffectObject = NGUITools.AddChild(getCashEffectPool, getCashEffectPrefab);
            if (getCashEffectObject != null)
            {
                m_GetCashEffectObjectList.Add(getCashEffectObject);
                GetCashEffect getCashEffect = getCashEffectObject.GetComponent<GetCashEffect>();
                m_GetCashEffectList.Add(getCashEffect);
                m_PoolUseList.Add(false);

                NGUITools.SetActive(getCashEffectObject, false);
            }
        }

        m_Path = new Vector3[5];

        m_StartPosition = startCashObject.transform.localPosition;
        Vector3 endPosition = destCashObject.transform.localPosition;
        float startX = Random.Range(m_StartPosition.x - 135.0f, m_StartPosition.x + 135.0f);
        float startY = Random.Range(m_StartPosition.y - 50.0f, m_StartPosition.y + 50.0f);
        m_SplineStartPosition = new Vector3(startX, startY, 0.0f);

        float middleX = Random.Range(m_SplineStartPosition.x - 100.0f, m_SplineStartPosition.x + 100.0f);
        float middleY = Random.Range(m_SplineStartPosition.y - 50.0f, m_SplineStartPosition.y + 20.0f);
        m_SplineMiddlePosition = new Vector3(middleX, middleY, 0.0f);

        float endX = Random.Range(endPosition.x - 10.0f, endPosition.x + 10.0f);
        float endY = Random.Range(endPosition.y - 10.0f, endPosition.y + 10.0f);
        m_SplineEndPosition = new Vector3(endX, endY, 0.0f);

        m_Path[0] = m_SplineStartPosition;
        m_Path[1] = m_SplineStartPosition;
        m_Path[2] = m_SplineMiddlePosition;
        m_Path[3] = m_SplineEndPosition;
        m_Path[4] = m_SplineEndPosition;

        m_Spline = new LTSpline(m_Path);
    }

    public void UseEndedEffect(int index)
    {
        m_PoolUseList[index] = false;
    }

    public void CreateGetCashEffect(int score)
    {
        if (score > GameConfig.MaxGetCoin)
        {
            score = GameConfig.MaxGetCoin;
        }
        
        int useCount = 0;
        int startPoolUseIndex = m_PoolUseIndex;
        for (int i = 0; i < GameConfig.MaxGetCoin; i++)
        {
            int index = startPoolUseIndex + i;
            if (index >= GameConfig.MaxGetCoin)
            {
                index -= GameConfig.MaxGetCoin;
            }

            bool isUse = m_PoolUseList[index];
            if (isUse == false)
            {
                m_PoolUseIndex = index;

                m_PoolUseList[index] = true;
                useCount++;

                GetCashEffect getCashEffect = m_GetCashEffectList[index];

                float ratio = (float)i / (float)score;
                float delay = Mathf.Lerp(0.0f, 0.8f, ratio);
                //float delay = Mathf.Lerp(0.0f, 1.0f, ratio);
                //Debug.Log("CreateGetCashEffect : " + ratio + ", " + delay + ", " + useCount);

                getCashEffect.Create(this, index, m_StartPosition, m_Spline, delay, 0.9f - delay);
                //scoreCourseEffect.Create(this, index, startPosition, endPosition, 0.0f, 1.0f + delay);
            }

            if (useCount >= score)
            {
                m_PoolUseIndex += 1;
                break;
            }
        }
    }
}

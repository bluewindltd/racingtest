﻿using UnityEngine;
using System.Collections;

public class MapObject : WeMonoBehaviour
{
    [HideInInspector] public GameConfig.ObjectType objectType;
    public int theme = 0;
    public int index = 0;
    
    Transform m_ObjectPoolParent = null;

    public void SetMapObject(Transform parent, Vector3 pos, Vector3 rotation, Vector3 scale)
    {
        //FixedJoint tempFixedJoint = gameObject.GetComponent<FixedJoint>();
        //if(tempFixedJoint != null)
        //{
        //    tempFixedJoint.connectedBody = tempRigidBody;
        //}
        m_ObjectPoolParent = transform.parent;

        transform.parent = parent;
        transform.localPosition = pos;
        transform.localEulerAngles = rotation;
        transform.localScale = scale;
    }

    public void Restore()
    {
        if (m_ObjectPoolParent != null)
        {
            transform.parent = m_ObjectPoolParent;
            m_ObjectPoolParent = null;
        }
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.zero;

        //MapObjectCreator mapObjectCreator = MapObjectManager.instance.GetMapObjectCreator(theme, index);
        //if(mapObjectCreator != null)
        //{
        //    mapObjectCreator.objectPool.PutObjectInPool(gameObject);
        //}
    }
}

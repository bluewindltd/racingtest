﻿using UnityEngine;
using System.Collections;

public class EffectInfo : MonoBehaviour
{
    int m_EffectID = -1;
    public int effectID
    {
        get
        {
            return m_EffectID;
        }
        set
        {
            m_EffectID = value;
        }
    }

    EffectManager.EffectType m_EffectType = EffectManager.EffectType.GunEffect;
    public EffectManager.EffectType effectType
    {
        get
        {
            return m_EffectType;
        }
        set
        {
            m_EffectType = value;
        }
    }

    float m_Radius = 0.0f;
    public float radius
    {
        get
        {
            return m_Radius;
        }
        set
        {
            m_Radius = value;
        }
    }

    // 폭파 반경을 디버깅 하려면 주석 해제
    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
    //    Gizmos.DrawWireSphere(transform.position, m_Radius);
    //}
}

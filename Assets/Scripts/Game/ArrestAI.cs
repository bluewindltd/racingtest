﻿using UnityEngine;
using System.Collections;

public class ArrestAI : WeMonoBehaviour
{
    public ChaserAI chaserAI;

    Coroutine m_StartArrestCoroutine = null;
    EtcDocs m_EtcDocs = null;
    float m_Time = 0.0f;

    void Start()
    {
        m_EtcDocs = EtcDocs.instance;
    }

    IEnumerator StartArrest()
    {
        GameManager.instance.StartArrest(chaserAI.chaserCarID, chaserAI.chaserType);
        chaserAI.preTargetPos = chaserAI.target.position;
        m_Time = 0.0f;
        while (true)
        {
            yield return new WaitForEndOfFrame();

            m_Time += Time.deltaTime;
            //Debug.Log(chaserAI.chaserCarID + " -> 체포 체크 -> " + m_Time);
            //yield return new WaitForSeconds(m_EtcDocs.arrestTime);

            if (chaserAI.isNearPlayer && chaserAI.isDisable == false)
            {
                // 체포 가시거리에 있어도 플레이어 차량이 계속 움직이고 있으면 체포가 안된다.
                float distance = Vector3.Distance(chaserAI.preTargetPos, chaserAI.target.position);
                if (distance < m_EtcDocs.escapeArrestDistance)
                {
                    if(m_Time >= m_EtcDocs.arrestTime)
                    {
                        //Debug.Log(chaserAI.chaserCarID + " -> 체포 성공!");
                        GameManager.instance.arrestPlayer = true;
                        break;
                    }                    
                }
                else
                {
                    //Debug.Log(chaserAI.chaserCarID + " -> 체포 실패! : 5M 이상 움직임");
                    GameManager.instance.StopArrest(chaserAI.chaserCarID, chaserAI.chaserType);
                    chaserAI.isNearPlayer = false;
                    break;
                }
            }
            else
            {
                GameManager.instance.StopArrest(chaserAI.chaserCarID, chaserAI.chaserType);
                chaserAI.isNearPlayer = false;
                break;
            }
        }        

        m_StartArrestCoroutine = null;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Arrest Trigger"))
        {
            if (chaserAI.isNearPlayer == false && chaserAI.isDisable == false && GameManager.instance.arrestPlayer == false)
            {
                if(m_StartArrestCoroutine == null)
                {
                    //Debug.Log(chaserAI.chaserCarID + " OnTriggerEnter -> 체포 시작");
                    m_StartArrestCoroutine = StartCoroutine(StartArrest());
                }                
            }

            chaserAI.isNearPlayer = true;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Arrest Trigger"))
        {
            if (chaserAI.isNearPlayer == false && chaserAI.isDisable == false && GameManager.instance.arrestPlayer == false)
            {
                if (m_StartArrestCoroutine == null)
                {
                    //Debug.Log(chaserAI.chaserCarID + " OnTriggerStay -> 체포 시작");
                    m_StartArrestCoroutine = StartCoroutine(StartArrest());
                }
            }

            chaserAI.isNearPlayer = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Arrest Trigger"))
        {
            //Debug.Log(chaserAI.chaserCarID + " -> 체포 실패! : 체포 영역을 벗어남");

            GameManager.instance.StopArrest(chaserAI.chaserCarID, chaserAI.chaserType);

            chaserAI.isNearPlayer = false;

            if (m_StartArrestCoroutine != null)
            {
                StopCoroutine(m_StartArrestCoroutine);
                m_StartArrestCoroutine = null;
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestMobileInput : WeMonoBehaviour
{
    public enum State
    {
        Waiting = 0,
        RollingWheel = 1,
        StartEngineSound = 2,        
        Preparing = 3,
        Playing = 4,
        GameOver = 5,
    }
    State m_State = State.Waiting;
    public State state
    {
        get
        {
            return m_State;
        }
    }

    public Rigidbody rb;
    public VehicleParent vehicleParent;
    public VehicleAssist vehicleAssist;
    public FlipControl flipControl;
    public GasMotor gasMoter;
    public HoverMotor hoverMoter;
    public AudioSource engineAudioSource;
    public SteeringControl steeringControl;
    public HoverSteer hoverSteer;
    public GearboxTransmission gearboxTransmission;
    public List<Suspension> suspensionList;
    public List<Wheel> wheelList;
    public List<HoverWheel> hoverWheelList;
    public List<CustomTireMarkCreate> customTireMarkList;
    public TireScreech tireScreech;
    public BalloonCarAI balloonCarAI;
    public SkullCarAI skullCarAI;
    public AkiraBikeAI akiraBikeAI;
    public ShrinkAttackAI shrinkAttackAI;
    public XWingAI xwingAI;
    public WitchWandAI witchWandAI;
    public bool isCashCar = false;

    private float m_CurrentForwardValue = 0.0f;
    private float m_ForwardValue = 1.0f;

    private float m_CurrentBackwordValue = 0.0f;
    private float m_BackwordValue = 1.0f;

    private bool m_isForward = true;
    private bool m_IsManualBackward = false;
    private bool m_ReadyManualBackward = false;
    private float m_ManualBackwardSideValue = 0.0f;
    private Coroutine m_ManualBackwardCoroutine = null;

    private bool m_isRespiteCheck = false;

    private float m_TurnValue;
    private float m_SideValue = 0.0f;

    private float m_CurrentBrakeValue = 0.0f;
    private float m_BrakeValue = 0.0f;

    // 후진 버튼으로 후진 할 경우 활성화 되는 변수
    private bool m_IsStraightBackward = false;
    // 좌우 방향 동시에 눌러 후진 할 경우 활성화 되는 변수
    private bool m_IsLeftRightBackward = false;
    private bool m_IsLeftTurn = false;
    private bool m_IsRightTurn = false;    

    private Vector3 m_PrePos = new Vector3(0.0f, 0.0f, 0.0f);

    private float m_BrakeCheckTime = 0.0f;

    float stoppedTime = 0.0f;

    int m_Speed = 0;
    public int speed
    {
        get
        {
            return m_Speed;
        }
    }

    bool m_isDisable = false;

    [RangeAttribute(0, 1)]
    public float strength = 1;
    public float damageFactor = 1;

    public float maxCollisionMagnitude = 100;

    [Tooltip("Maximum collision points to use when deforming, has large effect on performance")]
    public int maxCollisionPoints = 2;

    [Tooltip("Collisions underneath this local y-position will be ignored")]
    public float collisionIgnoreHeight;

    [Tooltip("Minimum time in seconds between collisions")]
    public float collisionTimeGap = 0.1f;
    float hitTime;

    float health = 1;

    public GameParameter gameParameter;
    public Material damageMaterial;
    public MeshRenderer[] bodyRendererArray;
    public MeshRenderer[] tireRendererArray;

    public ParticleSystem smoke;
    float initialSmokeEmission;

    public GameObject[] smokeObjectArray;
    int m_PreSmokeIndex = -1;

    GameManager gameMgr = null;
    SoundManager m_SoundMgr;

    UILabel m_HpTextLabel;
    PlayerHPGage m_HpGage = null;

    public int breakLimitSpeed = 60;
    public Vector3 startPosition = Vector3.zero;

    float m_BackwardLocalVelocity = 1.2f;
    bool m_EnableBackward = false;

    private void Start()
    {
        m_CurrentForwardValue = 1.0f;
        m_ForwardValue = 1.0f;        

        m_CurrentBackwordValue = 0.0f;
        m_BackwordValue = 0.0f;

        m_isForward = true;
        m_IsManualBackward = false;

        m_TurnValue = 0f;
        m_SideValue = 0.0f;

        m_IsLeftTurn = false;
        m_IsRightTurn = false;

        m_PrePos = Tr.position;

        for(int i = 0; i < smokeObjectArray.Length; i++)
        {
            smokeObjectArray[i].SetActive(false);
        }
        m_PreSmokeIndex = -1;

        gameMgr = GameManager.instance;
        m_SoundMgr = SoundManager.instance;

        //m_NearChaseCarCount = 0;

        //StartCoroutine(CheckBlock());
    }

    IEnumerator StartShake()
    {
        m_SideValue = 1.0f;

        yield return new WaitForSeconds(0.1f);

        m_SideValue = -1.0f;

        yield return new WaitForSeconds(0.1f);

        m_SideValue = 0.0f;
    }

    public void ChangeState(State newState)
    {
        if(m_State != newState)
        {
            m_State = newState;

            switch (m_State)
            {
                case State.Preparing:
                    {
                        rb.useGravity = true;
                        rb.isKinematic = false;
                        vehicleParent.enabled = true;
                        vehicleAssist.enabled = true;
                        flipControl.enabled = true;
                        if(gasMoter != null)
                        {
                            gasMoter.enabled = true;
                        }
                        if(hoverMoter != null)
                        {
                            hoverMoter.enabled = true;
                        }
                        if (engineAudioSource != null)
                        {
                            engineAudioSource.enabled = true;
                        }
                        if (steeringControl != null)
                        {
                            steeringControl.enabled = true;
                        }
                        if (hoverSteer != null)
                        {
                            hoverSteer.enabled = true;
                        }
                        if(gearboxTransmission != null)
                        {
                            gearboxTransmission.enabled = true;
                        }
                        for(int i = 0; i < suspensionList.Count; i++)
                        {
                            suspensionList[i].enabled = true;
                        }
                        for(int i = 0; i < wheelList.Count; i++)
                        {
                            wheelList[i].enabled = true;
                        }
                        for (int i = 0; i < hoverWheelList.Count; i++)
                        {
                            hoverWheelList[i].enabled = true;
                        }
                        for (int i = 0; i < customTireMarkList.Count; i++)
                        {
                            customTireMarkList[i].enabled = true;
                        }
                        if(tireScreech != null)
                        {
                            tireScreech.enabled = true;
                        }
                    }
                    break;

                case State.Playing:
                    {
                        m_Tr.localPosition = new Vector3(m_Tr.localPosition.x, m_Tr.localPosition.y + 0.1f, m_Tr.localPosition.z);

#if BWTEST
                        GameObject hpTextObject = HUDManager.instance.GetHpTextObject();

                        AttachObjectUI attachObjectUI = hpTextObject.GetComponent<AttachObjectUI>();
                        attachObjectUI.Attach(m_Tr);
                        m_HpTextLabel = hpTextObject.GetComponent<UILabel>();
#endif

                        if (balloonCarAI != null)
                        {
                            balloonCarAI.StartUpdate();
                        }
                        else  if (skullCarAI != null)
                        {
                            skullCarAI.StartUpdate();
                        }
                        else if (akiraBikeAI != null)
                        {
                            akiraBikeAI.StartUpdate();
                        }
                        else if (xwingAI != null)
                        {
                            xwingAI.StartUpdate();
                        }
                        else if (shrinkAttackAI != null)
                        {
                            shrinkAttackAI.StartUpdate();
                        }
                        else if(witchWandAI != null)
                        {
                            witchWandAI.StartUpdate();
                        }
                    }
                    break;

                case State.GameOver:
                    {
                        if (gasMoter != null)
                        {
                            gasMoter.enabled = false;
                        }
                        if (hoverMoter != null)
                        {
                            hoverMoter.enabled = false;
                        }
                        if (engineAudioSource != null)
                        {
                            engineAudioSource.enabled = false;
                        }
                        if (tireScreech != null)
                        {
                            tireScreech.enabled = false;
                        }

                        if (balloonCarAI != null)
                        {
                            balloonCarAI.StopUpdate();
                        }
                        else if (skullCarAI != null)
                        {
                            skullCarAI.StopUpdate();
                        }
                        else if (akiraBikeAI != null)
                        {
                            akiraBikeAI.StopUpdate();
                        }
                        else if (xwingAI != null)
                        {
                            xwingAI.StopUpdate();
                        }
                        else if (shrinkAttackAI != null)
                        {
                            shrinkAttackAI.StopUpdate();
                        }
                        else if (witchWandAI != null)
                        {
                            witchWandAI.StopUpdate();
                        }

                        if (m_HpGage != null)
                        {
                            HUDManager.instance.PutHpGageObject(m_HpGage.gameObject);
                            m_HpGage = null;
                        }
                    }
                    break;
            }
        }        
    }

    public void CreateHpGage()
    {
        if (m_HpGage == null)
        {
            GameObject hpGageObject = HUDManager.instance.GetHpGageObject();

            AttachObjectUI attachObjectUI = hpGageObject.GetComponent<AttachObjectUI>();
            attachObjectUI.Attach(m_Tr);
            m_HpGage = hpGageObject.GetComponent<PlayerHPGage>();
            m_HpGage.SetGage((float)gameParameter.currentHealth / (float)gameParameter.maxHealth);
        }
    }

    //IEnumerator CheckBlock()
    //{
    //    yield return new WaitForSeconds(0.1f);

    //    float distance = Vector3.Distance(m_PrePos, m_Rigidbody.transform.position);
    //    //Debug.Log("CheckBlock : " + distance);
    //    m_PrePos = m_Rigidbody.transform.position;

    //    // 전진 중인데 1초 동안 0.1M도 이동 못했을 경우 후진하게 해준다.
    //    if (distance < 0.01f && m_isForward && !m_isRespiteCheck)
    //    {
    //        m_isForward = false;

    //        m_ForwardValue = 0.0f;
    //        m_BackwordValue = 1.0f;
    //        StartCoroutine(ChangeForward());
    //    }

    //    StartCoroutine(CheckBlock());
    //}

    IEnumerator ChangeForward()
    {
        yield return new WaitForSeconds(2.0f);

        //Debug.Log("다시 전진!");
        m_isForward = true;
        m_EnableBackward = false;
        m_ForwardValue = 1.0f;
        m_BackwordValue = 0.0f;
    }

    public void DisableControl()
    {
        m_isDisable = true;

        vehicleParent.SetAccel(0.0f);
        vehicleParent.SetBrake(0.0f);
        vehicleParent.SetEbrake(1.0f);
    }

    private void Update()
    {
        if(m_State == State.Waiting)
        {
        }
        else if(m_State == State.Preparing)
        {
            vehicleParent.SetAccel(m_CurrentForwardValue);
            vehicleParent.SetBrake(0.0f);
            vehicleParent.SetEbrake(0.0f);
        }
        else if(m_State == State.Playing)
        {
#if BWTEST
            m_HpTextLabel.text = string.Format("{0}/{1}", gameParameter.currentHealth, gameParameter.maxHealth);
#endif
            if (m_HpGage != null)
            {
                m_HpGage.SetGage((float)gameParameter.currentHealth / (float)gameParameter.maxHealth);
            }

            if (m_isDisable == false)
            {
                m_Speed = Mathf.RoundToInt(vehicleParent.velMag * 2.23694f * 1.609344f);

                if(!m_EnableBackward)
                {
                    if(vehicleParent.localVelocity.z > m_BackwardLocalVelocity)
                    {
                        //Debug.Log("후진 체크 시작 가능");
                        m_EnableBackward = true;
                    }
                }

                if (m_isForward && m_EnableBackward)
                {
                    //Debug.Log("@@ : " + stoppedTime + ", " + vehicleParent.localVelocity.z + ", " + vehicleParent.groundedWheels);
                    stoppedTime = Mathf.Abs(vehicleParent.localVelocity.z) < 1.2 && vehicleParent.groundedWheels > 0 ? stoppedTime + Time.fixedDeltaTime : 0;
                    //Debug.Log("$$ : " + stoppedTime + ", " + vehicleParent.localVelocity.z + ", " + vehicleParent.groundedWheels);
                    if (stoppedTime > 0.5f)
                    {
                        stoppedTime = 0.0f;
                        m_isForward = false;
                        m_ForwardValue = 0.0f;
                        m_BackwordValue = 1.0f;

                        StartCoroutine(ChangeForward());
                    }
                }

                //m_CurrentForwardValue = Mathf.Lerp(m_CurrentForwardValue, m_ForwardValue, Mathf.Min(Time.deltaTime * 1.0f, 1.0f));
                //m_CurrentBrakeValue = Mathf.Lerp(m_CurrentBrakeValue, m_BrakeValue, Mathf.Min(Time.deltaTime * 1.0f, 1.0f));
                if (m_isForward && !m_ReadyManualBackward)
                {
                    vehicleParent.SetAccel(m_ForwardValue);
                    vehicleParent.SetBrake(0.0f);
                    vehicleParent.SetEbrake(0.0f);

                    //vehicleParent.SetAccel(0.0f);
                    //vehicleParent.SetBrake(1.0f);
                    //vehicleParent.SetEbrake(1.0f);
                }

                //m_CurrentBackwordValue = Mathf.Lerp(m_CurrentBackwordValue, m_BackwordValue, Mathf.Min(Time.deltaTime * 1.0f, 1.0f));
                if (!m_isForward && !m_ReadyManualBackward)
                {
                    vehicleParent.SetAccel(0.0f);
                    vehicleParent.SetBrake(m_BackwordValue);
                    vehicleParent.SetEbrake(0.0f);

                    //vehicleParent.SetAccel(0.0f);
                    //vehicleParent.SetBrake(1.0f);
                    //vehicleParent.SetEbrake(1.0f);
                }

                //Debug.Log(string.Format("MovementInput : {0} / {1}", m_CurrentForwardValue, m_CurrentBackwordValue));
                // Store the value of both input axes.
                //m_MovementInputValue = Input.GetAxis (m_MovementAxisName);
                //Debug.Log("m_Rigidbody.transform.position : " + m_Rigidbody.transform.position);
                //m_TurnValue = Input.GetAxis(m_TurnAxisName);

                //float currentTurnValue = m_TurnValue;
                //m_TurnValue = Mathf.Lerp(currentTurnValue, m_SideValue, Mathf.Min(Time.deltaTime * 5.0f, 1.0f));

                ////Debug.Log(string.Format("MovementInput({0}) : {1}", m_MovementAxisName, m_MovementInputValue));
                //Debug.Log(string.Format("TurnInput : {0}", m_TurnValue));

                float currentTurnValue = m_TurnValue;
                m_TurnValue = Mathf.Lerp(currentTurnValue, m_SideValue, Mathf.Min(Time.deltaTime * 20.0f, 1.0f));
                vehicleParent.SetSteer(m_SideValue);
                //Debug.Log("SideValue : " + currentTurnValue + ", " + m_SideValue);
                //m_VehicleParent.SetSteer(m_SideValue);

                //m_CurrentEBrakeValue = Mathf.Lerp(m_CurrentEBrakeValue, m_EBrakeValue, Mathf.Min(Time.deltaTime * 20.0f, 1.0f));
                //m_VehicleParent.SetEbrake(m_EBrakeValue);

                if (m_SideValue == 1.0f || m_SideValue == -1.0f)
                {
                    m_BrakeCheckTime += Time.deltaTime;

                    if (m_BrakeCheckTime > 0.2f)
                    {
                        if (m_Speed > breakLimitSpeed)
                        {
                            //Debug.Log(string.Format("커브 중 너무 빨라 브레이키~! : {0}, {1}",m_Speed, breakLimitSpeed));
                            vehicleParent.SetAccel(0.0f);
                            vehicleParent.SetBrake(1.0f);
                        }
                    }
                }
                else
                {
                    m_BrakeCheckTime = 0.0f;
                }

                if (smokeObjectArray.Length > 0 && gameParameter != null)
                {
                    int smokeState = (int)GameUtil.CalcSmokeState(gameParameter.currentHealth, gameParameter.maxHealth);
                    if (smokeState != (int)GameConfig.SmokeState.None &&
                       m_PreSmokeIndex != smokeState)
                    {
                        if (m_PreSmokeIndex != -1)
                        {
                            smokeObjectArray[m_PreSmokeIndex].SetActive(false);
                        }
                        m_PreSmokeIndex = (int)smokeState;
                        smokeObjectArray[m_PreSmokeIndex].SetActive(true);
                    }
                    else if(smokeState == (int)GameConfig.SmokeState.None &&
                       m_PreSmokeIndex != smokeState)
                    {
                        if (m_PreSmokeIndex != -1)
                        {
                            smokeObjectArray[m_PreSmokeIndex].SetActive(false);
                        }
                        m_PreSmokeIndex = (int)smokeState;
                    }
                }

                if (gameParameter.currentHealth <= 0)
                {
                    //Debug.Log("##################### Bug! : " + gameParameter.currentHealth + ", " + gameParameter.maxHealth);
                    gameMgr.destroyPlayer = true;
                    for (int i = 0; i < bodyRendererArray.Length; i++)
                    {
                        bodyRendererArray[i].material = damageMaterial;
                    }
                    for (int i = 0; i < tireRendererArray.Length; i++)
                    {
                        tireRendererArray[i].material = damageMaterial;
                    }
                }
            }            
        }
    }

    void FixedUpdate()
    {
        if (m_State == State.Playing)
        {
            //Decrease timer for collisionTimeGap
            hitTime = Mathf.Max(0, hitTime - Time.fixedDeltaTime);
            //Make sure damageFactor is not negative
            damageFactor = Mathf.Max(0, damageFactor);
        }
    }

    //Apply damage on collision
    void OnCollisionEnter(Collision col)
    {
        if (m_State == State.Playing)
        {
            if (hitTime == 0 && col.relativeVelocity.sqrMagnitude * damageFactor > 1 && strength < 1 && m_isDisable == false)
            {
                Vector3 normalizedVel = col.relativeVelocity.normalized;
                int colsChecked = 0;
                bool soundPlayed = false;
                bool sparkPlayed = false;
                hitTime = collisionTimeGap;

                foreach (ContactPoint curCol in col.contacts)
                {
                    if (!curCol.thisCollider.CompareTag("Underside"))
                    {
                        if (Tr.InverseTransformPoint(curCol.point).y > collisionIgnoreHeight && GlobalControl.damageMaskStatic == (GlobalControl.damageMaskStatic | (1 << curCol.otherCollider.gameObject.layer)))
                        {
                            colsChecked++;

                            //Play crash sound
                            if (vehicleParent.crashSnd && vehicleParent.crashClips.Length > 0 && !soundPlayed)
                            {
                                vehicleParent.crashSnd.PlayOneShot(vehicleParent.crashClips[Random.Range(0, vehicleParent.crashClips.Length)], Mathf.Clamp01(col.relativeVelocity.magnitude * 0.1f) * m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl);
                                soundPlayed = true;
                            }

                            //Play crash sparks
                            if (vehicleParent.sparks && !sparkPlayed)
                            {
                                vehicleParent.sparks.transform.position = curCol.point;
                                vehicleParent.sparks.transform.rotation = Quaternion.LookRotation(normalizedVel, curCol.normal);
                                vehicleParent.sparks.Play();
                                sparkPlayed = true;
                            }

                            DamageApplication(col.relativeVelocity, curCol.otherCollider.gameObject);
                        }

                        //Stop checking collision points when limit reached
                        if (colsChecked >= maxCollisionPoints)
                        {
                            break;
                        }
                    }                    
                }

                //FinalizeDamage();
            }
        }
    }

    //Where the damage is actually applied
    void DamageApplication(Vector3 damageForce, GameObject otherObject)
    {
        float colMag = Mathf.Min(damageForce.magnitude, maxCollisionMagnitude) * (1 - strength) * damageFactor;//Magnitude of collision
        float damageRatio = colMag / maxCollisionMagnitude;

        //Debug.Log("colMag : " + colMag + "damageRatio : " + damageRatio);

        CollisionConnector collisionConnector = otherObject.GetComponent<CollisionConnector>();
        
        if(collisionConnector != null)
        {
            int sendDamage = Mathf.FloorToInt(gameParameter.damage * damageRatio);
            gameMgr.ApplyDamage(collisionConnector, otherObject, sendDamage, false);

            GameParameter otherGameParameter = collisionConnector.gameParameter;
            if (otherGameParameter != null)
            {
                if (otherObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
                {
                    if (otherGameParameter.currentHealth > 0)
                    {
                        // 추적 차량이 살아 있을 경우만 데미지를 받게 한다.
                        int receiveDamage = Mathf.FloorToInt(otherGameParameter.damage * damageRatio);
                        gameParameter.currentHealth -= receiveDamage;
                    }
                }

                if (gameParameter.currentHealth <= 0)
                {
                    //Debug.Log("##################### Bug! : " + gameParameter.currentHealth + ", " + gameParameter.maxHealth);
                    gameMgr.destroyPlayer = true;
                    for (int i = 0; i < bodyRendererArray.Length; i++)
                    {
                        bodyRendererArray[i].material = damageMaterial;
                    }
                    for (int i = 0; i < tireRendererArray.Length; i++)
                    {
                        tireRendererArray[i].material = damageMaterial;
                    }
                }
            }
        }
    }
    
    IEnumerator PlayManualBackward()
    {
        while (true)
        {
            if (m_Speed <= 10)
            {
                m_ReadyManualBackward = false;

                m_SideValue = m_ManualBackwardSideValue;
                stoppedTime = 0.0f;
                m_isForward = false;
                m_IsManualBackward = true;
                m_ForwardValue = 0.0f;
                m_BackwordValue = 1.0f;

                m_ManualBackwardCoroutine = null;

                break;
            }
            else
            {
                //m_CurrentForwardValue = Mathf.Lerp(m_CurrentForwardValue, m_ForwardValue, Mathf.Min(Time.deltaTime * 1.0f, 1.0f));
                //m_CurrentBackwordValue = Mathf.Lerp(m_CurrentBackwordValue, m_BackwordValue, Mathf.Min(Time.deltaTime * 1.0f, 1.0f));
                //Mathf.SmoothDamp(m_CurrentForwardValue, m_ForwardValue, 10.0f, 1.0f)
                m_CurrentForwardValue = Mathf.Lerp(m_CurrentForwardValue, m_ForwardValue, Time.deltaTime * 0.3f);
                m_CurrentBackwordValue = Mathf.Lerp(m_CurrentBackwordValue, m_BackwordValue, Time.deltaTime * 0.3f);

                vehicleParent.SetAccel(m_CurrentForwardValue);
                vehicleParent.SetBrake(m_CurrentBackwordValue);
                vehicleParent.SetEbrake(0.0f);
            }                

            yield return null;
        }
    }

    public void StartBackward()
    {
        m_IsStraightBackward = true;

        m_ManualBackwardSideValue = 0.0f;

        m_ReadyManualBackward = true;
        m_CurrentForwardValue = 1.0f;
        m_ForwardValue = 0.0f;
        m_CurrentBackwordValue = 0.0f;
        m_BackwordValue = 1.0f;
        m_SideValue = 0.0f;

        if (m_ManualBackwardCoroutine == null)
        {
            m_ManualBackwardCoroutine = StartCoroutine(PlayManualBackward());
        }
    }

    public void StopBackward()
    {
        if(m_IsStraightBackward)
        {
            m_IsStraightBackward = false;
            if(!m_IsLeftRightBackward)
            {
                if (m_ReadyManualBackward)
                {
                    StopCoroutine(m_ManualBackwardCoroutine);
                    m_ManualBackwardCoroutine = null;

                    m_ReadyManualBackward = false;
                    m_isForward = true;
                    m_ForwardValue = 1.0f;
                    m_BackwordValue = 0.0f;
                }

                if (m_IsManualBackward)
                {
                    m_IsManualBackward = false;
                    m_isForward = true;
                    m_ForwardValue = 1.0f;
                    m_BackwordValue = 0.0f;
                }
            }            
        }        
    }

    public void StartLeftTurn()
    {
        if (state == State.Playing)
        {
            m_IsLeftTurn = true;

            if (m_IsRightTurn == true)
            {
                m_IsLeftRightBackward = true;

                m_ManualBackwardSideValue = -1.0f;

                m_ReadyManualBackward = true;
                m_CurrentForwardValue = 1.0f;
                m_ForwardValue = 0.0f;
                m_CurrentBackwordValue = 0.0f;
                m_BackwordValue = 1.0f;
                m_SideValue = 0.0f;

                if(m_ManualBackwardCoroutine == null)
                {
                    m_ManualBackwardCoroutine = StartCoroutine(PlayManualBackward());
                }
            }
            else
            {
                if (m_isForward)
                {
                    m_ForwardValue = 1.0f;
                }
                else
                {
                    m_ForwardValue = 0.0f;
                }
                m_SideValue = -1.0f;
            }
        }
    }

    public void StopLeftTurn()
    {
        if (state == State.Playing)
        {
            if(m_IsLeftRightBackward)
            {
                m_IsLeftRightBackward = false;
                if(!m_IsStraightBackward)
                {
                    if (m_ReadyManualBackward)
                    {
                        StopCoroutine(m_ManualBackwardCoroutine);
                        m_ManualBackwardCoroutine = null;

                        m_ReadyManualBackward = false;
                        m_isForward = true;
                        m_ForwardValue = 1.0f;
                        m_BackwordValue = 0.0f;
                    }

                    if (m_IsManualBackward)
                    {
                        m_IsManualBackward = false;
                        m_isForward = true;
                        m_ForwardValue = 1.0f;
                        m_BackwordValue = 0.0f;
                    }
                }
            }            

            m_IsLeftTurn = false;

            if (m_IsRightTurn == true)
            {
                StartRightTurn();
            }
            else
            {
                m_SideValue = 0.0f;
            }
        }
    }

    public void StartRightTurn()
    {
        if (state == State.Playing)
        {
            m_IsRightTurn = true;

            if (m_IsLeftTurn == true)
            {
                m_IsLeftRightBackward = true;

                m_ManualBackwardSideValue = 1.0f;

                m_ReadyManualBackward = true;
                m_CurrentForwardValue = 1.0f;
                m_ForwardValue = 0.0f;
                m_CurrentBackwordValue = 0.0f;
                m_BackwordValue = 1.0f;
                m_SideValue = 0.0f;

                if (m_ManualBackwardCoroutine == null)
                {
                    m_ManualBackwardCoroutine = StartCoroutine(PlayManualBackward());
                }
            }
            else
            {
                if (m_isForward)
                {
                    m_ForwardValue = 1.0f;
                }
                else
                {
                    m_ForwardValue = 0.0f;
                }
                m_SideValue = 1.0f;
            }
        }
    }

    public void StopRightTurn()
    {
        if (state == State.Playing)
        {
            if (m_IsLeftRightBackward)
            {
                m_IsLeftRightBackward = false;
                if (!m_IsStraightBackward)
                {
                    if (m_ReadyManualBackward)
                    {
                        StopCoroutine(m_ManualBackwardCoroutine);
                        m_ManualBackwardCoroutine = null;

                        m_ReadyManualBackward = false;
                        m_isForward = true;
                        m_ForwardValue = 1.0f;
                        m_BackwordValue = 0.0f;
                    }

                    if (m_IsManualBackward)
                    {
                        m_IsManualBackward = false;
                        m_isForward = true;
                        m_ForwardValue = 1.0f;
                        m_BackwordValue = 0.0f;
                    }
                }
            }

            m_IsRightTurn = false;

            if (m_IsLeftTurn == true)
            {
                StartLeftTurn();
            }
            else
            {
                m_SideValue = 0.0f;
            }
        }
    }
}

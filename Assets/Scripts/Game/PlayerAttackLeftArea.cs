﻿using UnityEngine;
using System.Collections;

public class PlayerAttackLeftArea : MonoBehaviour {

    public PlayerGangAI gangAI;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            //Debug.Log("PlayerAttackLeftArea ON!! : ");

            gangAI.leftTargetVisible = true;
            gangAI.leftTargetObject = other.gameObject;
            gangAI.leftTargetTransform = other.gameObject.transform;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            //Debug.Log("PlayerAttackLeftArea OFF!! : ");

            gangAI.leftTargetVisible = false;
            gangAI.leftTargetObject = null;
            gangAI.leftTargetTransform = null;
        }
    }
}

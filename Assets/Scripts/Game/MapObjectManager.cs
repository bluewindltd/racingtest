﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;

[Serializable]
public class MapObjectCreator
{
    [HideInInspector][SerializeField] public GameObject mapObjectPrefab;
    [HideInInspector][SerializeField] public GameConfig.ObjectType mapObjectType;
    //[HideInInspector][SerializeField] public Transform objectPoolRoot;
    //[HideInInspector][SerializeField] public int objectPoolCount;
    //[HideInInspector] public ObjectPool objectPool;
}

[Serializable] public class DictionaryOfMapObjectCreator : SerializableDictionary<int, MapObjectCreator> { }
[Serializable] public class DictionaryOfMapObjectTheme : SerializableDictionary<int, DictionaryOfMapObjectCreator> { }

[ExecuteInEditMode]
public class MapObjectManager : Singleton<MapObjectManager>
{
    [HideInInspector][SerializeField] DictionaryOfMapObjectTheme m_ThemeDic = new DictionaryOfMapObjectTheme();
    public DictionaryOfMapObjectTheme themeDic
    {
        get
        {
            return m_ThemeDic;
        }
        set
        {
            m_ThemeDic = value;
        }
    }

    [HideInInspector][SerializeField] MapObjectCreator m_MissionPointObject;
    public MapObjectCreator missionPointObject
    {
        get
        {
            return m_MissionPointObject;
        }
        set
        {
            m_MissionPointObject = value;
        }
    }

    [HideInInspector][SerializeField] MapObjectCreator m_ItemPointObject;
    public MapObjectCreator itemPointObject
    {
        get
        {
            return m_ItemPointObject;
        }
        set
        {
            m_ItemPointObject = value;
        }
    }

    //public void Initialize()
    //{
    //    DictionaryOfMapObjectTheme.Enumerator etor = m_ThemeDic.GetEnumerator();
    //    while(etor.MoveNext())
    //    {
    //        DictionaryOfMapObjectCreator mapObjectCreatorDic = etor.Current.Value;
    //        DictionaryOfMapObjectCreator.Enumerator etor2 = mapObjectCreatorDic.GetEnumerator();
    //        while(etor2.MoveNext())
    //        {
    //            MapObjectCreator mapObjectCreator = etor2.Current.Value;
    //            if(mapObjectCreator.objectPool == null)
    //            {
    //                mapObjectCreator.objectPool = new ObjectPool(mapObjectCreator.objectPoolCount, mapObjectCreator.mapObjectPrefab, mapObjectCreator.objectPoolRoot);
    //            }
    //        }
    //    }
    //}

    void Awake()
    {
        if (Application.isPlaying)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void AddTheme(int themeIndex)
    {
        if(!m_ThemeDic.ContainsKey(themeIndex))
        {
            DictionaryOfMapObjectCreator mapCreatorDic = new DictionaryOfMapObjectCreator();
            m_ThemeDic.Add(themeIndex, mapCreatorDic);
        }
    }

    public MapObjectCreator GetMapObjectCreator(int themeIndex, int objectIndex)
    {
        if(m_ThemeDic.ContainsKey(themeIndex))
        {
            DictionaryOfMapObjectCreator mapObjectCreatorDic = m_ThemeDic[themeIndex];
            if(mapObjectCreatorDic.ContainsKey(objectIndex))
            {
                return mapObjectCreatorDic[objectIndex];
            }
            else
            {
                return null;
            }
        }
        else
        {
            if(themeIndex == -1 && objectIndex == -1)
            {
                return m_MissionPointObject;
            }
            else if(themeIndex == -1 && objectIndex == -2)
            {
                return m_ItemPointObject;
            }
            else
            {
                return null;
            }            
        }
    }
}

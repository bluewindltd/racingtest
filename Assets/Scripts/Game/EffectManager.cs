﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EffectManager : Singleton<EffectManager>
{
    public enum EffectType
    {
        GunEffect = 0,
        MachineGunEffect = 1,
        RocketEffect = 2,
        PetRocketEffect = 3,
        LandMineEffect = 4,
        PumpkinEffect = 5,
    }
    public GameObject gunEffectPrefab;
    public GameObject machineGunEffectPrefab;
    public GameObject rocketEffectPrefab;
    public GameObject petRocketEffectPrefab;
    public GameObject landMineEffectPrefab;
    public GameObject pumpkinEffectPrefab;

    ObjectPool m_GunEffectObjectPool;
    ObjectPool m_MachineGunEffectObjectPool;
    ObjectPool m_RocketEffectObjectPool;
    ObjectPool m_PetRocketEffectObjectPool;
    ObjectPool m_LandMineEffectObjectPool;
    ObjectPool m_PumpkinEffectObjectPool;

    Transform m_Transform;

    Dictionary<int, EffectInfo> m_EffectInfoDic = new Dictionary<int, EffectInfo>();
    int m_EffectCount = 0;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start ()
    {
        m_Transform = GetComponent<Transform>();
        m_GunEffectObjectPool = new ObjectPool(30, gunEffectPrefab, m_Transform);
        m_MachineGunEffectObjectPool = new ObjectPool(30, machineGunEffectPrefab, m_Transform);
        m_RocketEffectObjectPool = new ObjectPool(30, rocketEffectPrefab, m_Transform);
        m_PetRocketEffectObjectPool = new ObjectPool(10, petRocketEffectPrefab, m_Transform);
        m_LandMineEffectObjectPool = new ObjectPool(15, landMineEffectPrefab, m_Transform);
        m_PumpkinEffectObjectPool = new ObjectPool(20, pumpkinEffectPrefab, m_Transform);
    }

    public void Restore()
    {
        EffectInfo effectInfo = null;
        Dictionary<int, EffectInfo>.Enumerator etor = m_EffectInfoDic.GetEnumerator();
        while(etor.MoveNext())
        {
            effectInfo = etor.Current.Value;
            effectInfo.transform.parent = m_Transform;
            switch (effectInfo.effectType)
            {
                case EffectType.GunEffect:
                    {
                        m_GunEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
                    }
                    break;

                case EffectType.MachineGunEffect:
                    {
                        m_MachineGunEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
                    }
                    break;

                case EffectType.RocketEffect:
                    {
                        m_RocketEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
                    }
                    break;
                case EffectType.PetRocketEffect:
                    {
                        m_PetRocketEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
                    }
                    break;
                case EffectType.LandMineEffect:
                    {
                        m_LandMineEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
                    }
                    break;
                case EffectType.PumpkinEffect:
                    {
                        m_PumpkinEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
                    }
                    break;
            }
        }
        m_EffectCount = 0;
        m_EffectInfoDic.Clear();
    }

    public void CreateGunEffect(Transform parent, Vector3 position, Quaternion rotation)
    {
        GameObject gunEffectObject = m_GunEffectObjectPool.GetObjectFromPool();
        EffectInfo effectInfo = gunEffectObject.GetComponent<EffectInfo>();
        effectInfo.effectID = m_EffectCount;
        effectInfo.effectType = EffectType.GunEffect;
        m_EffectInfoDic.Add(effectInfo.effectID, effectInfo);
        m_EffectCount++;
        if (m_EffectCount > 10000)
        {
            m_EffectCount = 0;
        }
        Transform gunEffectTransform = gunEffectObject.transform;
        gunEffectTransform.parent = parent;
        gunEffectTransform.position = position;
        gunEffectTransform.rotation = rotation;
        gunEffectTransform.Rotate(Vector3.up, UnityEngine.Random.Range(0.0f, 360.0f));
        Animator gunMuzzleEffectAnimator = gunEffectObject.GetComponent<Animator>();
        gunMuzzleEffectAnimator.Play("Effect_Hit_Gun_Shoot");
        StartCoroutine(ResetGunEffect(effectInfo));
    }

    public void CreateMachineGunEffect(Transform parent, Vector3 position, Quaternion rotation)
    {
        GameObject machineGunEffectObject = m_MachineGunEffectObjectPool.GetObjectFromPool();
        EffectInfo effectInfo = machineGunEffectObject.GetComponent<EffectInfo>();
        effectInfo.effectID = m_EffectCount;
        effectInfo.effectType = EffectType.MachineGunEffect;
        m_EffectInfoDic.Add(effectInfo.effectID, effectInfo);
        m_EffectCount++;
        if (m_EffectCount > 10000)
        {
            m_EffectCount = 0;
        }
        Transform machineGunEffectObjectTransform = machineGunEffectObject.transform;
        machineGunEffectObjectTransform.parent = parent;
        machineGunEffectObjectTransform.position = position;
        machineGunEffectObjectTransform.rotation = rotation;
        machineGunEffectObjectTransform.Rotate(Vector3.up, UnityEngine.Random.Range(0.0f, 360.0f));
        Animator machineGunEffectAnimator = machineGunEffectObject.GetComponent<Animator>();
        machineGunEffectAnimator.Play("Effect_Muzzle_Gun_Shoot");
        StartCoroutine(ResetMachineGunEffect(effectInfo));
    }

    public void CreateRocketEffect(Transform parent, Vector3 position, Quaternion rotation, float radius)
    {
        GameObject rocketEffectObject = m_RocketEffectObjectPool.GetObjectFromPool();
        EffectInfo effectInfo = rocketEffectObject.GetComponent<EffectInfo>();
        effectInfo.effectID = m_EffectCount;
        effectInfo.effectType = EffectType.RocketEffect;
        effectInfo.radius = radius;
        m_EffectInfoDic.Add(effectInfo.effectID, effectInfo);
        m_EffectCount++;
        if (m_EffectCount > 10000)
        {
            m_EffectCount = 0;
        }
        rocketEffectObject.transform.parent = null;
        Transform rocketEffectObjectTransform = rocketEffectObject.transform;
        rocketEffectObjectTransform.parent = parent;
        rocketEffectObjectTransform.position = position;
        effectInfo.radius = radius;
        rocketEffectObjectTransform.Rotate(Vector3.up, UnityEngine.Random.Range(0.0f, 360.0f));
        Animator rocketEffectAnimator = rocketEffectObject.GetComponent<Animator>();
        rocketEffectAnimator.Play("Effect_Hit_Missile_Bomb");
        StartCoroutine(ResetRocketEffect(effectInfo));
    }

    public void CreatePetRocketEffect(Transform parent, Vector3 position, Quaternion rotation, float radius)
    {
        GameObject petRocketEffectObject = m_PetRocketEffectObjectPool.GetObjectFromPool();
        EffectInfo effectInfo = petRocketEffectObject.GetComponent<EffectInfo>();
        effectInfo.effectID = m_EffectCount;
        effectInfo.effectType = EffectType.PetRocketEffect;
        effectInfo.radius = radius;
        m_EffectInfoDic.Add(effectInfo.effectID, effectInfo);
        m_EffectCount++;
        if (m_EffectCount > 10000)
        {
            m_EffectCount = 0;
        }
        petRocketEffectObject.transform.parent = null;
        Transform petRocketEffectObjectTransform = petRocketEffectObject.transform;
        petRocketEffectObjectTransform.parent = parent;
        petRocketEffectObjectTransform.position = position;
        petRocketEffectObjectTransform.Rotate(Vector3.up, UnityEngine.Random.Range(0.0f, 360.0f));
        Animator petRocketEffectAnimator = petRocketEffectObject.GetComponent<Animator>();
        petRocketEffectAnimator.Play("Effect_Hit_DroneMissile_Bomb");
        StartCoroutine(ResetPetRocketEffect(effectInfo));
    }

    public void CreateLandMineEffect(Transform parent, Vector3 position, Quaternion rotation, float radius)
    {
        GameObject landMineEffectObject = m_LandMineEffectObjectPool.GetObjectFromPool();
        EffectInfo effectInfo = landMineEffectObject.GetComponent<EffectInfo>();
        effectInfo.effectID = m_EffectCount;
        effectInfo.effectType = EffectType.LandMineEffect;
        effectInfo.radius = radius;
        m_EffectInfoDic.Add(effectInfo.effectID, effectInfo);
        m_EffectCount++;
        if (m_EffectCount > 10000)
        {
            m_EffectCount = 0;
        }
        landMineEffectObject.transform.parent = null;
        Transform landMineEffectObjectTransform = landMineEffectObject.transform;
        landMineEffectObjectTransform.parent = parent;
        landMineEffectObjectTransform.position = position;
        landMineEffectObjectTransform.Rotate(Vector3.up, UnityEngine.Random.Range(0.0f, 360.0f));
        Animator landMineEffectAnimator = landMineEffectObject.GetComponent<Animator>();
        landMineEffectAnimator.Play("Effect_Hit_Mine_Bomb");
        StartCoroutine(ResetLandMineEffect(effectInfo));
    }

    public void CreatePumpkinEffect(Transform parent, Vector3 position, Quaternion rotation, float radius)
    {
        GameObject pumpkinEffectObject = m_PumpkinEffectObjectPool.GetObjectFromPool();
        EffectInfo effectInfo = pumpkinEffectObject.GetComponent<EffectInfo>();
        effectInfo.effectID = m_EffectCount;
        effectInfo.effectType = EffectType.PumpkinEffect;
        effectInfo.radius = radius;
        m_EffectInfoDic.Add(effectInfo.effectID, effectInfo);
        m_EffectCount++;
        if (m_EffectCount > 10000)
        {
            m_EffectCount = 0;
        }
        pumpkinEffectObject.transform.parent = null;
        Transform pumpkinEffectObjectTransform = pumpkinEffectObject.transform;
        pumpkinEffectObjectTransform.parent = parent;
        pumpkinEffectObjectTransform.position = position;
        pumpkinEffectObjectTransform.Rotate(Vector3.up, UnityEngine.Random.Range(0.0f, 360.0f));
        Animator landMineEffectAnimator = pumpkinEffectObject.GetComponent<Animator>();
        landMineEffectAnimator.Play("Effect_Hit_PumpkinMissile_Bomb");
        StartCoroutine(ResetPumpkinEffect(effectInfo));
    }

    IEnumerator ResetGunEffect(EffectInfo effectInfo)
    {
        yield return new WaitForSeconds(0.1f);

        effectInfo.transform.parent = m_Transform;
        m_GunEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
        m_EffectInfoDic.Remove(effectInfo.effectID);
    }

    IEnumerator ResetMachineGunEffect(EffectInfo effectInfo)
    {
        yield return new WaitForSeconds(0.1f);

        effectInfo.transform.parent = m_Transform;
        m_MachineGunEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
        m_EffectInfoDic.Remove(effectInfo.effectID);
    }

    IEnumerator ResetRocketEffect(EffectInfo effectInfo)
    {
        yield return new WaitForSeconds(0.583f);

        effectInfo.transform.parent = m_Transform;
        m_RocketEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
        m_EffectInfoDic.Remove(effectInfo.effectID);
    }

    IEnumerator ResetPetRocketEffect(EffectInfo effectInfo)
    {
        yield return new WaitForSeconds(0.533f);

        effectInfo.transform.parent = m_Transform;
        m_PetRocketEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
        m_EffectInfoDic.Remove(effectInfo.effectID);
    }

    IEnumerator ResetLandMineEffect(EffectInfo effectInfo)
    {
        yield return new WaitForSeconds(0.5f);

        effectInfo.transform.parent = m_Transform;
        m_LandMineEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
        m_EffectInfoDic.Remove(effectInfo.effectID);
    }

    IEnumerator ResetPumpkinEffect(EffectInfo effectInfo)
    {
        yield return new WaitForSeconds(0.5f);

        effectInfo.transform.parent = m_Transform;
        m_PumpkinEffectObjectPool.PutObjectInPool(effectInfo.gameObject);
        m_EffectInfoDic.Remove(effectInfo.effectID);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerGangAI : WeMonoBehaviour
{
    public BattleParameter battleParam;
    public Transform leftGunMuzzleTransform;
    public Transform rightGunMuzzleTransform;
    public Transform leftGunMuzzleEffectTransform;
    public Transform rightGunMuzzleEffectTransform;
    public ParticleSystem leftGunParticle;
    public ParticleSystem rightGunParticle;
    public AudioSource gunMuzzleSFX;
    public Transform leftGunRotorTransform;
    public Transform rightGunRotorTransform;

    float m_CurrentAttackDelay = 0.0f;
    
    bool m_IsTargetVisible;

    bool m_LeftTargetVisible = false;
    public bool leftTargetVisible
    {
        get
        {
            return m_LeftTargetVisible;
        }
        set
        {
            m_LeftTargetVisible = value;
        }
    }

    bool m_RightTargetVisible = false;
    public bool rightTargetVisible
    {
        get
        {
            return m_RightTargetVisible;
        }
        set
        {
            m_RightTargetVisible = value;
        }
    }

    GameObject m_LeftTargetObject = null;
    public GameObject leftTargetObject
    {
        set
        {
            m_LeftTargetObject = value;
        }
    }
    Transform m_LeftTargetTransform = null;
    public Transform leftTargetTransform
    {
        set
        {
            m_LeftTargetTransform = value;
        }
    }

    GameObject m_RightTargetObject = null;
    public GameObject rightTargetObject
    {
        set
        {
            m_RightTargetObject = value;
        }
    }
    Transform m_RightTargetTransform = null;
    public Transform rightTargetTransform
    {
        set
        {
            m_RightTargetTransform = value;
        }
    }

    GameObject m_TargetObject = null;
    Transform m_TargetTransform = null;

    SoundManager m_SoundMgr;
    GameManager m_GameMgr;

    void Start()
    {
        m_CurrentAttackDelay = 0.0f;
        m_IsTargetVisible = false;
        m_LeftTargetVisible = false;
        m_RightTargetVisible = false;

        m_SoundMgr = SoundManager.instance;
        m_GameMgr = GameManager.instance;
    }

    void Update()
    {
        if (GameManager.instance.state == GameManager.State.Playing)
        {
            m_CurrentAttackDelay += Time.deltaTime;
            if (m_CurrentAttackDelay >= battleParam.attackDelay)
            {
                if (m_LeftTargetVisible || m_RightTargetVisible)
                {
                    Transform gunShaftTransform = null;
                    Transform gunMuzzleTransform = null;
                    Transform gunMuzzleEffectTransform = null;
                    ParticleSystem gunParticle = null;
                    if (m_LeftTargetVisible)
                    {
                        gunShaftTransform = leftGunRotorTransform;
                        gunMuzzleTransform = leftGunMuzzleTransform;
                        gunMuzzleEffectTransform = leftGunMuzzleEffectTransform;
                        gunParticle = leftGunParticle;
                        m_TargetObject = m_LeftTargetObject;
                        m_TargetTransform = m_LeftTargetTransform;
                    }
                    else if (m_RightTargetVisible)
                    {
                        gunShaftTransform = rightGunRotorTransform;
                        gunMuzzleTransform = rightGunMuzzleTransform;
                        gunMuzzleEffectTransform = rightGunMuzzleEffectTransform;
                        gunParticle = rightGunParticle;
                        m_TargetObject = m_RightTargetObject;
                        m_TargetTransform = m_RightTargetTransform;
                    }

                    Vector3 localTarget = gunShaftTransform.InverseTransformPoint(m_TargetTransform.position);
                    float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

                    bool isShoot = false;
                    float testAngle = gunShaftTransform.localEulerAngles.y + angle;
                    if (m_LeftTargetVisible)
                    {
                        //Debug.Log("Left : " + angle + ", " + gunShaftTransform.localEulerAngles.y + ", " + testAngle);
                        if (testAngle >= 195 && testAngle <= 345)
                        {
                            isShoot = true;
                        }
                    }
                    if (m_RightTargetVisible)
                    {
                        //Debug.Log("Right : " + angle + ", " + gunShaftTransform.localEulerAngles.y + ", " + testAngle);
                        if (testAngle >= 15 && testAngle <= 165)
                        {
                            isShoot = true;
                        }
                    }

                    if (isShoot)
                    {
                        m_CurrentAttackDelay = 0.0f;
                        
                        gunMuzzleSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                        gunMuzzleSFX.Play();

                        gunParticle.Emit(1);

                        EffectManager.instance.CreateGunEffect(m_Tr, gunMuzzleEffectTransform.position, gunMuzzleEffectTransform.rotation);

                        Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
                        Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity);
                        gunShaftTransform.rotation = gunShaftTransform.rotation * deltaRotation;

                        //gunMuzzleEmitter.emit = true;

                        //Vector3 gunShootDirection = gunMuzzleTransform.forward + (-gunMuzzleTransform.up * 0.2f);
                        Debug.DrawRay(gunMuzzleTransform.position, gunMuzzleTransform.forward * 10.0f, Color.red);
                        RaycastHit hit;
                        if (Physics.Raycast(gunMuzzleTransform.position, gunMuzzleTransform.forward, out hit, 10.0f, BattleControl.playerAttackDamageMaskStatic))
                        {
                            StartCoroutine(BulletFlight(hit.point, hit.collider.gameObject, gunMuzzleTransform.forward,
                                (Vector3.Distance(gunMuzzleTransform.transform.position, hit.point)) * 0.012f));
                            //Collider[] colliders = Physics.OverlapSphere(hit.point, m_GunHitRadius, BattleControl.enemyAttackDamageMaskStatic);
                            //for (int i = 0; i < colliders.Length; i++)
                            //{
                            //    Collider tempCollider = colliders[i];
                            //    if (tempCollider)
                            //    {
                            //        CollisionConnector collisionConnector = tempCollider.GetComponent<CollisionConnector>();
                            //        if (collisionConnector != null)
                            //        {
                            //            if (collisionConnector.connectRigidbody != null)
                            //            {
                            //                //collisionConnector.connectRigidbody.AddExplosionForce(10.0f, hit.point, m_GunHitRadius, 10.0f);
                            //                collisionConnector.connectRigidbody.AddForce(gunShootDirection.normalized * 300.0f, ForceMode.Acceleration);
                            //                Instantiate(bulletImpactEmitterObject, hit.point, Quaternion.LookRotation(hit.normal));
                            //                //GameObject emitterObject = m_BulletImpactEmitterObjectPool.GetObjectFromPool();
                            //                //Transform emitterTransform = emitterObject.transform;
                            //                //emitterTransform.position = hit.point;
                            //                //emitterTransform.rotation = Quaternion.LookRotation(hit.normal);
                            //                if (collisionConnector.gameParameter != null)
                            //                {
                            //                    collisionConnector.gameParameter.currentHealth -= gunDamage;
                            //                }
                            //            }
                            //        }
                            //    }
                            //}
                        }
                    }
                }
            }
        }
    }

    IEnumerator BulletFlight(Vector3 point, GameObject target, Vector3 direction, float time)
    {
        yield return new WaitForSeconds(time);

        CollisionConnector collisionConnector = target.GetComponent<CollisionConnector>();

        if (collisionConnector != null)
        {
            if (collisionConnector.connectRigidbody != null)
            {
                if (target.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    collisionConnector.connectRigidbody.AddForce(direction * 500.0f, ForceMode.Acceleration);
                }
                //Instantiate(bulletImpactEmitterObject, point, Quaternion.LookRotation(hitNormal));

                m_GameMgr.ApplyDamage(collisionConnector, target, battleParam.attackDamage, false);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class AttackLeftArea : MonoBehaviour
{
    public GangAI gangAI;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles"))
        {
            //Debug.Log("AttackLeftArea ON!! : ");

            gangAI.leftTargetVisible = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles"))
        {
            //Debug.Log("AttackLeftArea OFF!! : ");

            gangAI.leftTargetVisible = false;
        }
    }
}

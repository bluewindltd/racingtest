﻿using UnityEngine;
using System.Collections;

public class ApacheAI : WeMonoBehaviour
{
    public Helicopter helicopter;
    public BattleParameter battleParam;
    public Transform heliTransform;
    public Transform leftRocketTransform;
    //public ParticleSystem leftRocketParticle;
    public AudioSource leftRocketSFX;
    public Transform rightRocketTransform;    
    //public ParticleSystem rightRocketParticle;    
    public AudioSource rightRocketSFX;
    float m_CurrentAttackDelay = 0.0f;
    
    Rigidbody m_Rigidbody;
    bool m_IsTargetVisible;

    float m_TempDistance = 0.0f;
    Vector3 m_LookDirection = Vector3.zero;
    float m_LookDot = 0.0f;

    Coroutine m_UpdateCoroutine = null;

    SoundManager m_SoundMgr;
    RegionManager m_RegionMgr;

    protected override void Awake()
    {
        base.Awake();

        m_Rigidbody = GetComponent<Rigidbody>();
        m_SoundMgr = SoundManager.instance;
        m_RegionMgr = RegionManager.instance;
    }

    public void Restore()
    {
        if(m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    public void Initialize()
    {
        m_CurrentAttackDelay = 0.0f;
        m_IsTargetVisible = false;
        m_TempDistance = 0.0f;
        m_LookDirection = Vector3.zero;
        m_LookDot = 0.0f;
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            if (helicopter.state == Helicopter.State.Appear)
            {
                m_CurrentAttackDelay += Time.deltaTime;
                if (m_CurrentAttackDelay >= battleParam.attackDelay)
                {
                    // 40m 이내일 경우만 공격하게 거리체크
                    m_TempDistance = Vector3.Distance(m_Tr.position, m_RegionMgr.playerTransform.position);
                    if (m_TempDistance <= 40.0f)
                    {
                        m_IsTargetVisible = !Physics.Linecast(m_Tr.position, m_RegionMgr.playerTransform.position, BattleControl.enemyAttackBlockMaskStatic);
                        // 헬기와 플레이어 간에 가리는 장애물이 없는지 체크
                        if (m_IsTargetVisible)
                        {
                            m_LookDirection = m_RegionMgr.playerTransform.position - leftRocketTransform.position;
                            m_LookDirection.Normalize();
                            m_LookDot = Vector3.Dot(m_RegionMgr.playerTransform.up, m_LookDirection);
                            if (-0.65 <= m_LookDot && m_LookDot <= -0.35f)
                            {
                                m_CurrentAttackDelay = 0.0f;

                                leftRocketTransform.LookAt(m_RegionMgr.playerTransform.position);
                                rightRocketTransform.LookAt(m_RegionMgr.playerTransform.position);

                                if (GameManager.instance.state == GameManager.State.Playing)
                                {
                                    leftRocketSFX.pitch = Random.Range(0.9f, 1.1f);
                                    leftRocketSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                                    leftRocketSFX.Play();
                                }

                                Fire(leftRocketTransform);

                                //leftRocketParticle.Emit(1);

                                //RaycastHit leftRocketHit;
                                //if (Physics.Raycast(leftRocketTransform.position, leftRocketTransform.forward, out leftRocketHit, 50.0f, (1 << LayerMask.NameToLayer("Vehicles"))))
                                //{
                                //    StartCoroutine(RocketFlight(leftRocketHit.point, leftRocketHit.collider.gameObject,
                                //                            (Vector3.Distance(leftRocketTransform.transform.position, leftRocketHit.point)) * 0.018f));
                                //}

                                yield return new WaitForSeconds(0.1f);

                                if (GameManager.instance.state == GameManager.State.Playing)
                                {
                                    rightRocketSFX.pitch = Random.Range(0.9f, 1.1f);
                                    rightRocketSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                                    rightRocketSFX.Play();
                                }

                                Fire(rightRocketTransform);

                                //rightRocketParticle.Emit(1);

                                //RaycastHit rightRocketHit;
                                //if (Physics.Raycast(rightRocketTransform.position, rightRocketTransform.forward, out rightRocketHit, 50.0f, (1 << LayerMask.NameToLayer("Vehicles"))))
                                //{
                                //    StartCoroutine(RocketFlight(rightRocketHit.point, rightRocketHit.collider.gameObject,
                                //                            (Vector3.Distance(rightRocketTransform.transform.position, rightRocketHit.point)) *
                                //                            0.018f));
                                //}
                            }
                        }
                    }
                }
            }

            yield return null;
        }
    }

    private void Fire(Transform fireTransform)
    {
        GameObject shellObject = ShellManager.instance.GetApacheShell();
        shellObject.transform.parent = null;
        shellObject.transform.position = fireTransform.position;
        shellObject.transform.rotation = fireTransform.rotation;
        ApacheShellExplosion shell = shellObject.GetComponent<ApacheShellExplosion>();
        shell.battleParam = battleParam;
        Rigidbody shellRigidbody = shellObject.GetComponent<Rigidbody>();

        //Debug.Log("TankAI Velocity : " + m_Rigidbody.velocity + ", " + m_Rigidbody.velocity.magnitude + ", " + ratio.magnitude);        
        shellRigidbody.velocity = (battleParam.shellSpeed + m_Rigidbody.velocity.magnitude) * fireTransform.forward;
    }

    //IEnumerator RocketFlight(Vector3 point, GameObject target, float time)
    //{
    //    yield return new WaitForSeconds(time);

    //    EffectManager.instance.CreateRocketEffect(null, point, Quaternion.identity);

    //    //CollisionConnector collisionConnector = target.GetComponent<CollisionConnector>();

    //    //if (collisionConnector != null)
    //    //{
    //    //    if (collisionConnector.connectRigidbody != null)
    //    //    {
    //    //        collisionConnector.connectRigidbody.AddForce(heliTransform.forward * 300);

    //    //        if (collisionConnector.gameParameter != null)
    //    //        {
    //    //            collisionConnector.gameParameter.currentHealth -= gunDamage;
    //    //        }
    //    //    }
    //    //}

    //    CollisionConnector collisionConnector = null;
    //    Collider[] colliders = Physics.OverlapSphere(point, battleParam.explosionRadius, BattleControl.enemyAttackDamageMaskStatic);
    //    for (int i = 0; i < colliders.Length; i++)
    //    {
    //        Collider hit = colliders[i];
    //        if (hit)
    //        {
    //            collisionConnector = hit.GetComponent<CollisionConnector>();
    //            if (collisionConnector != null)
    //            {
    //                if (collisionConnector.connectRigidbody != null)
    //                {
    //                    //collisionConnector.connectRigidbody.AddForce(Vector3.up * 300.0f, ForceMode.Acceleration);
    //                    collisionConnector.connectRigidbody.GetComponent<Rigidbody>().AddExplosionForce(50, point, 0.1f, 2.0f, ForceMode.Acceleration);
    //                }

    //                if (collisionConnector.gameParameter != null)
    //                {
    //                    collisionConnector.gameParameter.currentHealth -= battleParam.attackDamage;
    //                }
    //            }
    //        }
    //    }
    //}
}

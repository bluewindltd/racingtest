﻿using UnityEngine;
using System.Collections;

public class AttackRightArea : MonoBehaviour
{
    public GangAI gangAI;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles"))
        {
            //Debug.Log("AttackRightArea ON!! : ");

            gangAI.rightTargetVisible = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles"))
        {
            //Debug.Log("AttackRightArea OFF!! : ");

            gangAI.rightTargetVisible = false;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class IntroDecoration : MonoBehaviour
{
    public Animator animator;
    public GameObject[] rootCarObjectArray;
    public GameObject[] cinemaNpcCarPrefabArray;

    int m_PreAniIndex = 0;

    public void PlayDriving()
    {
        ClearCar();

        for (int i = 0; i < rootCarObjectArray.Length; i++)
        {
            int index = Random.Range(0, cinemaNpcCarPrefabArray.Length);
            GameObject cinemaNpcCarObject = Instantiate(cinemaNpcCarPrefabArray[index], Vector3.zero, Quaternion.identity) as GameObject;
            cinemaNpcCarObject.transform.parent = rootCarObjectArray[i].transform;
            cinemaNpcCarObject.transform.localPosition = Vector3.zero;
            cinemaNpcCarObject.transform.localEulerAngles = Vector3.zero;
        }

        m_PreAniIndex++;
        if(m_PreAniIndex > 2)
        {
            m_PreAniIndex = 1;
        }
        animator.Play(string.Format("IntroDeco VehicleSet_Ani{0}", m_PreAniIndex));
    }

    void ClearCar()
    {
        for (int i = 0; i < rootCarObjectArray.Length; i++)
        {
            CommonUtil.DestroyChildren(rootCarObjectArray[i].transform);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class ArrestGageIcon : MonoBehaviour
{
    const float StartPositionX = -177.0f;
    const float MaxSizeX = 354.0f;

    public Transform iconTransform;

    float m_Time = 0.0f;
    float m_ArrestTime = 0.0f;
    float m_CurrentPercent = 0.0f;
    public float currentPercent
    {
        get
        {
            return m_CurrentPercent;
        }
    }
    Coroutine m_UpdateGageCoroutine = null;
    GameManager m_GameMgr = null;

    public void StartArrestGage(GameManager gameMgr)
    {
        m_GameMgr = gameMgr;
        iconTransform.localPosition = new Vector3(StartPositionX, 13, 0);
        m_Time = 0.0f;
        m_ArrestTime = EtcDocs.instance.arrestTime;
        if(m_UpdateGageCoroutine == null)
        {
            m_UpdateGageCoroutine = StartCoroutine(UpdateArrestGage());
        }
    }

    public void StopArrestGage()
    {
        if (m_UpdateGageCoroutine != null)
        {
            StopCoroutine(m_UpdateGageCoroutine);
            m_UpdateGageCoroutine = null;
        }
    }

    IEnumerator UpdateArrestGage()
    {
        while(true)
        {
            yield return new WaitForEndOfFrame();

            m_Time += Time.deltaTime;
            if(m_Time > m_ArrestTime)
            {
                m_Time = m_ArrestTime;
            }
            m_CurrentPercent = m_Time / m_ArrestTime;
            float x = Mathf.Lerp(StartPositionX, StartPositionX + MaxSizeX, m_CurrentPercent);
            iconTransform.localPosition = new Vector3(x, 13, 0);

            if(Mathf.Abs(StartPositionX + MaxSizeX - iconTransform.localPosition.x) < 0.1f)
            {
                iconTransform.localPosition = new Vector3(StartPositionX + MaxSizeX, 13, 0);
                break;
            }

            if(m_GameMgr.state != GameManager.State.Playing)
            {
                break;
            }
        }

        m_UpdateGageCoroutine = null;
    }
}

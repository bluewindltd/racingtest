﻿using UnityEngine;
using System.Collections;

public class PlayerAttackRightArea : MonoBehaviour
{
    public PlayerGangAI gangAI;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            //Debug.Log("PlayerAttackRightArea ON!! : ");

            gangAI.rightTargetVisible = true;
            gangAI.rightTargetObject = other.gameObject;
            gangAI.rightTargetTransform = other.gameObject.transform;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            //Debug.Log("PlayerAttackRightArea OFF!! : ");

            gangAI.rightTargetVisible = false;
            gangAI.rightTargetObject = null;
            gangAI.rightTargetTransform = null;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkullCarAI : MonoBehaviour
{
    public Animator animator;
    public Renderer bodyRenderer;
    public Color origColor;
    public Color destColor;
    public BoxCollider attackCollider;
    public BattleParameter battleParam;

    Coroutine m_UpdateCoroutine = null;
    Coroutine m_CollectPowerCoroutine = null;
    float m_Timer = 0.0f;

    GameManager m_GameMgr;

    void Start()
    {
        m_GameMgr = GameManager.instance;
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            CollisionConnector collisionConnector = other.gameObject.GetComponent<CollisionConnector>();
            if (collisionConnector != null)
            {
                m_GameMgr.ApplyDamage(collisionConnector, other.gameObject, battleParam.attackDamage, true);
            }
        }
    }

    public void StopUpdate()
    {
        if (m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            attackCollider.enabled = false;

            animator.Play("Effect_Vehicle037_Fire_Idle");

            if (m_CollectPowerCoroutine == null)
            {
                m_CollectPowerCoroutine = StartCoroutine(CollectPower());
            }

            yield return new WaitForSeconds(battleParam.attackDelay);

            attackCollider.enabled = true;

            animator.Play("Effect_Vehicle037_Fire_Shoot");

            if (m_CollectPowerCoroutine != null)
            {
                StopCoroutine(m_CollectPowerCoroutine);
                m_CollectPowerCoroutine = null;
            }

            yield return new WaitForSeconds(battleParam.rotorSpeed);
        }
    }

    IEnumerator CollectPower()
    {
        while(true)
        {
            m_Timer += Time.deltaTime;

            if(m_Timer > battleParam.attackDelay)
            {
                m_Timer = 0.0f;
                bodyRenderer.material.color = origColor;
                m_CollectPowerCoroutine = null;
                break;
            }
                        
            Color colorTemp = Color.Lerp(origColor, destColor, m_Timer / battleParam.attackDelay);
            bodyRenderer.material.color = colorTemp;
            //bodyRenderer.sharedMaterial.SetColor("Color (A)Opacity", colorTemp);

            yield return null;
        }
    }
}

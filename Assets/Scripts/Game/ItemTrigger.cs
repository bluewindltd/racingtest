﻿using UnityEngine;
using System.Collections;

public class ItemTrigger : MonoBehaviour
{
    Transform m_ObjectPoolParent = null;
    public ItemManager.ItemType type;

    int m_ItemIndex = -1;
    public int itemIndex
    {
        get
        {
            return m_ItemIndex;
        }
        set
        {
            m_ItemIndex = value;
        }
    }

    public virtual void SetItem(Transform parentItemPoint)
    {
        m_ObjectPoolParent = transform.parent;

        transform.parent = parentItemPoint;
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
    }

    public void Restore()
    {
        if (m_ObjectPoolParent != null)
        {
            transform.parent = m_ObjectPoolParent;
            m_ObjectPoolParent = null;
        }
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;

        ItemManager.instance.RemoveItemTrigger(m_ItemIndex);

        ObjectPool itemTriggerObjectPool = ItemManager.instance.GetItemTriggerPool(type);
        itemTriggerObjectPool.PutObjectInPool(gameObject);
    }
}

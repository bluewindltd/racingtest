﻿using UnityEngine;
using System.Collections;

public class CamHelicopter : WeMonoBehaviour
{
    //public float distance = 10.0f;
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;
    public float offset = 0.0f;
    public float rollSpeed = 15.0f;

    static float[] m_DistanceArray = { 77.0f, 30.0f, 15.0f };
    static float[] m_SpeedArray = { 20.0f, 15.0f, 15.0f };
    float m_Height = 30.0f;
    float m_WantedRotationAngle = 0.0f;
    float m_WantedHeight = 0.0f;

    float m_CurrentRotationAngle = 0.0f;
    float m_CurrentHeight = 0.0f;

    //public float smoothTime = 0.3f;
    //public float xOffset = 13.0f;
    //public float zOffset = 13.0f;
    //public Transform bodyTransform;
    int m_AngleIndex = 0;
    float m_RotationDirection = 1.0f;
    Quaternion m_CurrentRotation;

    RegionManager m_RegionMgr;

    //Vector3 m_TargetVector = Vector3.zero;
    //float m_Angle = 0.0f;
    //Quaternion m_DeltaRotation = Quaternion.identity;
    //Vector3 m_EulerAngleVelocity = Vector3.zero;
    //float m_RollAngle = 0.0f;
    //float m_RollX = 0.0f;
    //float m_RollZ = 0.0f;

    //bool test = false;

    void Start()
    {
        m_RegionMgr = RegionManager.instance;
        m_Tr.position = new Vector3(m_RegionMgr.playerTransform.position.x, 15.0f, m_RegionMgr.playerTransform.position.z);

        //StartCoroutine(BodyShake());
        //StartCoroutine(UpdateCoroutine());

        StartCoroutine(ChangeAngle());
    }

    IEnumerator ChangeAngle()
    {
        int angelIndex = UnityEngine.Random.Range(0, 3);
        if(m_AngleIndex == angelIndex)
        {
            m_AngleIndex = angelIndex + 1 >= 3 ? 0 : angelIndex + 1;
        }
        else
        {
            m_AngleIndex = angelIndex;
        }
        m_RotationDirection *= -1.0f;

        yield return new WaitForSeconds(5.0f);

        StartCoroutine(ChangeAngle());
    }

    void Update()
    {
        m_Tr.eulerAngles = m_Tr.eulerAngles + (Vector3.up * m_SpeedArray[m_AngleIndex] * Time.smoothDeltaTime * m_RotationDirection);
        m_Height = UnityEngine.Random.Range(30.0f, 33.0f);

        m_WantedRotationAngle = m_RegionMgr.playerTransform.eulerAngles.y;
        m_WantedHeight = m_RegionMgr.playerTransform.position.y + m_Height;

        m_CurrentRotationAngle = m_Tr.eulerAngles.y;
        m_CurrentHeight = m_Tr.position.y;

        //if (Mathf.Abs(m_WantedHeight - m_CurrentHeight) < 0.01f)
        //{
        //    if(test == false)
        //    {
        //        height = 31.0f;
        //        test = true;
        //    }
        //    else
        //    {
        //        height = 30.0f;
        //        test = false;
        //    }
        //}

        m_CurrentHeight = Mathf.Lerp(m_CurrentHeight, m_WantedHeight, heightDamping * Time.deltaTime);

        m_CurrentRotation = Quaternion.Euler(0, m_CurrentRotationAngle, 0);

        m_Tr.position = m_RegionMgr.playerTransform.position;
        m_Tr.position -= m_CurrentRotation * Vector3.forward * m_DistanceArray[m_AngleIndex];

        m_Tr.position = new Vector3(m_Tr.position.x, m_CurrentHeight, m_Tr.position.z);

        m_Tr.LookAt(m_RegionMgr.playerTransform.position + new Vector3(0, offset, 0));

        //m_RollZ = Mathf.Sin(m_RollAngle * Mathf.Deg2Rad) * 100.0f;
        //m_RollX = Mathf.Cos(m_RollAngle * Mathf.Deg2Rad) * 100.0f;
        //m_RollAngle += Time.deltaTime * 100.0f;
        ////xOffset = UnityEngine.Random.Range(13.0f, 20.0f);
        ////zOffset = UnityEngine.Random.Range(13.0f, 20.0f);        
        //m_Tr.position = new Vector3(Mathf.Lerp(m_Tr.position.x, m_RegionMgr.playerTransform.position.x + xOffset + m_RollX, Time.deltaTime * smoothTime),
        //    Mathf.Lerp(m_Tr.position.y, 15.0f, Time.deltaTime * smoothTime),
        //    Mathf.Lerp(m_Tr.position.z, m_RegionMgr.playerTransform.position.z + zOffset + m_RollZ, Time.deltaTime * smoothTime));

        //m_TargetVector = m_Tr.InverseTransformPoint(m_RegionMgr.playerTransform.position);

        //m_Angle = Mathf.Atan2(m_TargetVector.x, m_TargetVector.z) * Mathf.Rad2Deg;

        //m_EulerAngleVelocity = new Vector3(0, m_Angle, 0);
        //m_DeltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.deltaTime * 2.0f);
        //m_Tr.rotation = m_Tr.rotation * m_DeltaRotation;
    }

    //IEnumerator UpdateCoroutine()
    //{
    //    while(true)
    //    {
    //        yield return new WaitForSeconds(0.1f);


    //    }
    //}
}

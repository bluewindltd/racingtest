﻿using UnityEngine;
using System.Collections;

public class ChopperAI : WeMonoBehaviour
{
    public Helicopter helicopter;
    public BattleParameter battleParam;
    public Transform heliTransform;
    public Transform machineGunTransform;
    public ParticleSystem machineGunParticle;
    public AudioSource machineGunSFX;

    float m_CurrentAttackDelay = 0.0f;
    
    bool m_IsTargetVisible;

    float m_TempDistance = 0.0f;
    Vector3 m_LookDirection = Vector3.zero;
    float m_LookDot = 0.0f;

    Coroutine m_UpdateCoroutine = null;

    RegionManager m_RegionMgr;

    public void Restore()
    {
        if (m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    public void Initialize()
    {
        m_CurrentAttackDelay = 0.0f;
        m_IsTargetVisible = false;
        m_RegionMgr = RegionManager.instance;
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            if (helicopter.state == Helicopter.State.Appear)
            {
                m_CurrentAttackDelay += Time.deltaTime;
                if (m_CurrentAttackDelay >= battleParam.attackDelay)
                {
                    // 40m 이내일 경우만 공격하게 거리체크
                    m_TempDistance = Vector3.Distance(m_Tr.position, m_RegionMgr.playerTransform.position);
                    if (m_TempDistance <= 40.0f)
                    {
                        m_IsTargetVisible = !Physics.Linecast(m_Tr.position, m_RegionMgr.playerTransform.position, BattleControl.enemyAttackBlockMaskStatic);
                        // 헬기와 플레이어 간에 가리는 장애물이 없는지 체크
                        if (m_IsTargetVisible)
                        {
                            m_LookDirection = m_RegionMgr.playerTransform.position - machineGunTransform.position;
                            m_LookDirection.Normalize();
                            m_LookDot = Vector3.Dot(m_RegionMgr.playerTransform.up, m_LookDirection);
                            if (-0.65 <= m_LookDot && m_LookDot <= -0.35f)
                            {
                                m_CurrentAttackDelay = 0.0f;

                                //Debug.DrawRay(machineGunTransform.position, machineGunTransform.forward * 50.0f, Color.red);

                                if (GameManager.instance.state == GameManager.State.Playing)
                                {
                                    machineGunSFX.pitch = Random.Range(0.8f, 1.2f);
                                    machineGunSFX.Play();
                                }

                                machineGunParticle.Emit(1);

                                RaycastHit hit;

                                if (Physics.Raycast(machineGunTransform.position, machineGunTransform.forward, out hit, 50.0f, BattleControl.enemyAttackDamageMaskStatic))
                                {
                                    StartCoroutine(BulletFlight(hit.point, hit.collider.gameObject, machineGunTransform.forward,
                                        (Vector3.Distance(machineGunTransform.transform.position, hit.point)) * 0.012f));
                                }

                                yield return new WaitForSeconds(0.1f);
                                machineGunParticle.Emit(1);
                                yield return new WaitForSeconds(0.1f);
                                machineGunParticle.Emit(1);                                
                            }
                        }
                    }
                }
            }

            yield return null;
        }
    }

    IEnumerator BulletFlight(Vector3 point, GameObject target, Vector3 direction, float time)
    {
        yield return new WaitForSeconds(time);

        CollisionConnector collisionConnector = target.GetComponent<CollisionConnector>();

        if (collisionConnector != null)
        {
            if (collisionConnector.connectRigidbody != null)
            {
                if (target.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    collisionConnector.connectRigidbody.AddForce(direction * 500.0f, ForceMode.Acceleration);
                }
                //collisionConnector.connectRigidbody.AddForce(heliTransform.forward * 300.0f, ForceMode.Acceleration);

                if (collisionConnector.gameParameter != null)
                {
                    collisionConnector.gameParameter.currentHealth -= battleParam.attackDamage;
                }
            }
        }
    }
}

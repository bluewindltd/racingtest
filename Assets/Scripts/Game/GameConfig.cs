﻿using UnityEngine;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes;
using System.Text;

public class GameConfig
{
    public enum Scene
    {
        IntroScene = 0,
        GameScene = 1,
        ShopScene = 2,
        GachaScene = 3,
    }

    public enum SupportLanguage
    {
        None = 0,
        Korean = 1,
        English = 2,
        Chinese_S = 3,
        Chinese_T = 4,
        Indonesia = 5,
        Thai = 6,
        Japanese = 7,
        Spanish = 8,
        Russian = 9,
        Arabic = 10,
        German = 11,
        Hindi = 12,
        French = 13,
        Italian = 14,
        Portuguese = 15,
        Vietnamese = 16,
        Count,
    }

    public static string[] SupportLanguageName = { "None", "Korean", "English", "ChineseSimplified", "ChineseTraditional", "Indonesian", "Thai", "Japanese", "Spanish", "Russian", "Arabic", "German", "Hindi", "French", "Italian", "Portugees", "Vietnamese" };

    public enum RegionType
    {
        V_Straight = 10,
        H_Straight = 20,
        NE_Corner = 6,
        ES_Corner = 12,
        SW_Corner = 24,
        WN_Corner = 18,
        NES_Intersection = 14,
        ESW_Intersection = 28,
        SWN_Intersection = 26,
        WNE_Intersection = 22,
        NESW_Intersection = 30,
    }

    public enum Direction
    {
        North = 0,
        East = 1,
        South = 2,
        West = 3,
    }

    public enum RoadType
    {
        Straight = 0,
        Corner = 1,
        InterSection_3 = 2,
        InterSection_4 = 3,
    }

    public enum GroundType
    {
        Straight = 0,
        Corner = 1,
        Piece = 2,
    }

    public enum ObjectType
    {
        Building = 0,
        Obstacle = 1,
        Tree = 2,
        MissionPoint = 3,
        ItemPoint = 4,
    }

    public enum MissionType
    {
        Chase = 0,
        Delivery = 1,
    }

    public enum TokenType
    {
        C = 0,
        B = 1,
        A = 2,
        S = 3,
        Count,
    }

    public enum GiftType
    {
        Free = 0,
        Play = 1,
    }

    public enum RewardType
    {
        C = 0,
        B = 1,
        A = 2,
        S = 3,
        Cash = 4,
        Vehicle = 5,
        Pet = 6,
        AllVehicle = 7,
        Count,
    }

    //public enum PackageType
    //{
    //    C = 0,
    //    B = 1,
    //    A = 2,
    //    S = 3,
    //    Cash = 4,
    //    Vehicle = 5,
    //    Pet = 6,
    //    AllVehicle = 7,
    //    Count,
    //}

    public enum BuyResultState
    {
        Success = 0,
        NotEnoughToken = 1,
    }

    public static string[] TokenMark = { "Common_Icon_Class_C", "Common_Icon_Class_B", "Common_Icon_Class_A", "Common_Icon_Class_S" };
    public static string[] TokenMiniMark = { "Common_MiniIcon_Class_C", "Common_MiniIcon_Class_B", "Common_MiniIcon_Class_A", "Common_MiniIcon_Class_S" };
    public static string[] TokenName = { "C", "B", "A", "S" };
    public static string[] GiftIconName = { "Result_QuickGift_C", "Result_QuickGift_B", "Result_QuickGift_A", "Result_QuickGift_S", "Result_QuickGift_Cash" };
    public static string[] VehicleGradeMark = { "Shop_Btn_Deco_C", "Shop_Btn_Deco_B", "Shop_Btn_Deco_A", "Shop_Btn_Deco_S" };
    public static float[] ChaserCheckDegree = { 0.33f, 0.66f, 1.0f };
    public static string[] CouponRewardName = { "ctoken", "btoken", "atoken", "stoken", "cash" };
    public static string[] MoneyImage = { "Common_Icon_Class_C", "Common_Icon_Class_B", "Common_Icon_Class_A", "Common_Icon_Class_S", "Common_Icon_Money" };

    //public static ObscuredInt GachaPoint = 100;

    public enum SmokeState
    {
        None = -1,
        _1 = 0,
        _2 = 1,
        _3 = 2,
        _4 = 3,
    }

    //public enum Scene
    //{
    //    SimpleSplash = 0,
    //    IntroScene = 1,
    //    LobbyScene = 2,
    //    GameScene = 3,
    //}

    //// 인트로씬에서 초기화 작업을 하게 변경하면서 한번만 초기화 되게 설정하는 변수
    //public static bool CompleteInit = false;

    //public static int UseEventNoticePopup = 1;

    public const int HighFrameRate = 40;
    public const float HighFixedFrameRate = 40;

    public const int LowFrameRate = 30;
    public const float LowFixedFrameRate = 30;

    public const int MaxGetCoin = 50;

    public static string GetRewardName(GameConfig.RewardType type, int value)
    {
        StringBuilder rewardName = new StringBuilder();
        if (type == GameConfig.RewardType.Vehicle)
        {
            VehicleData vehicleData = VehicleDocs.instance.vehicleDataDic[value];
            if (vehicleData != null)
            {
                rewardName.Append(GameConfig.TokenName[(int)vehicleData.grade]);
                rewardName.Append(" ");
                rewardName.Append(TextDocs.content(vehicleData.name));
            }
        }
        else if (type == GameConfig.RewardType.AllVehicle)
        {
            rewardName.Append(TextDocs.content("ALL_VEHICLE_STOCK_NAME"));
        }
        else if (type == GameConfig.RewardType.Pet)
        {
            PetData petData = PetDocs.instance.petDataDic[value];
            if (petData != null)
            {
                rewardName.Append(TextDocs.content(petData.name));
            }
        }
        else if (type == GameConfig.RewardType.Cash)
        {
            rewardName.Append(TextDocs.content("UI_CASH_TEXT"));
            rewardName.Append(" ");
            rewardName.Append(value);
        }
        else if (0 <= (int)type && (int)type < 4)
        {
            rewardName.Append(string.Format(TextDocs.content("UI_REWORD_TOKEN"), GameConfig.TokenName[(int)type], value));
        }
        else
        {
            rewardName.Append("-");
        }

        return rewardName.ToString();
    }

    public enum PermissionState
    {
        PASS = 0,
        NON_PASS,
        FIRST_NEED_REQUEST,
        NEED_REQUEST,
    }

    //public const double TimeQuizLimit = 10.0f;

    //public static bool IsResultMode = false;

    //public const int MaxBannerCount = 3;
    //public const int MaxFavoriteCategoryCount = 5;
    //public const int MaxPromoteCategoryCount = 5;

    //public const int MaxCorrectVoiceSFX = 11;
    //public const int MaxWrongVoiceSFX = 10;
    //public const int MaxWinVoiceSFX = 5;
    //public const int MaxDrawVoiceSFX = 5;
    //public const int MaxLoseVoiceSFX = 5;
    //public const int MaxGiveawayVoiceSFX = 3;

    //public const int MaxHintUse = 2;

    //public const string EverlandPassword = "7";

    //public const string EncryptKey = "b7023l395u28e1w07i90n234d9779478";

    //public const int MaxGainScore = 100; // 퀴즈 하나 맞춰서 얻을 수 있는 최대 점수, 일단 최대한 많이 생성

    //public const int MaxDownloadRetryCount = 5; // 다운로드 실패시 재시도할 횟수

    //public const string GPAllWinCountLeaderBoardId = "CgkIydqSu4sNEAIQHg";

    //public static float[] PackageItemCellPosition1 = new[] { 3.2f };
    //public static float[] PackageItemCellPosition2 = new[] { 24.4f, -19f };
    //public static float[] PackageItemCellPosition3 = new[] { 46.6f, 3.2f, -40.0f };

    //public static int[] AnswerBits = new[] { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

    //public static float[] InputBoxPosition1 = new float[] { 0.0f };
    //public static float[] InputBoxPosition2 = new float[] { -42.0f, 43.0f };
    //public static float[] InputBoxPosition3 = new float[] { -85.0f, 0.0f, 85.0f };
    //public static float[] InputBoxPosition4 = new float[] { -127.0f, -42.0f, 43.0f, 128.0f };
    //public static float[] InputBoxPosition5 = new float[] { -170.0f, -85.0f, 0.0f, 85.0f, 170.0f };
    //public static float[] InputBoxPosition6 = new float[] { -212.0f, -127.0f, -42.0f, 43.0f, 128.0f, 213.0f };

    //public enum MatchType
    //{
    //    Normal = 0,
    //    Product = 1,
    //}

    //public enum QuizModeDetail
    //{
    //    None = -1,
    //    NormalQuiz = 0,
    //    TournamentQuiz = 1,
    //    ConsecutiveWinProductQuiz = 2,
    //    TotalWinProductQuiz = 3,
    //};

    //public enum InGameQuizState
    //{
    //    CloseQuiz = 0,
    //    PlayingQuiz,
    //    EndTurn,
    //    PrepareNextQuiz,
    //    ChangeQuiz,
    //};

    //public static string[] ProductTypeLocalKey = { "", "TICKET_ITEMTITLE", "HINT_ITEMTITLE", "CHANCE_ITEMTITLE", "", "PENCIL_ITEMTITLE", "" };
}

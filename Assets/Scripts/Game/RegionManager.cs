﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadInfo
{
    public GameConfig.RoadType m_RoadType;
    public GameConfig.Direction m_Direction;
}

public class GroundInfo
{
    public GameConfig.GroundType m_GroundType;
    public GameConfig.Direction m_Direction;
}

public class RegionInfo
{
    public RoadInfo m_RoadInfo = new RoadInfo();
    public List<GroundInfo> m_GroundInfoList = new List<GroundInfo>();
}

public class RegionDirectionCheck
{
    public float angleIndx = 0.0f;
    public float startAngle = 0.0f;
    public float endAngle = 0.0f;
}

public class RegionManager : Singleton<RegionManager>
{
    public float m_ViewingDistance;
    public float m_RegionSize;

    public GameObject m_RegionPrefab;
    public Transform m_RegionParents;
    public int m_RegionPoolCount;
    ObjectPool m_RegionObjectPool;

    public GameObject[] m_RoadPrefab;
    public Transform[] m_RoadParents;
    public int[] m_RoadPoolCount;
    List<ObjectPool> m_RoadObjectPool = new List<ObjectPool>();

    public GameObject[] m_GroundPrefab;
    public Transform[] m_GroundParents;
    public int[] m_GroundPoolCount;
    //List<ObjectPool> m_GroundObjectPool = new List<ObjectPool>();

    int m_GridLength;
    int m_HalfGridLength;
    float m_RegionHalfSize;

    Transform m_PlayerTransform;
    public Transform playerTransform
    {
        get
        {
            return m_PlayerTransform;
        }
    }

    bool m_Initialize = false;
    bool m_FirstRegion = true;
    bool m_FirstUpRegion = true;

    Dictionary<long, Region> m_RegionDic;
    public Dictionary<long, Region> regionDic
    {
        get
        {
            return m_RegionDic;
        }
    }
    Dictionary<long, Region> m_ViewRegionDic;
    List<long> m_RemoveRegionList;

    int m_CenterX;
    public int centerX
    {
        get
        {
            return m_CenterX;
        }
    }
    int m_CenterZ;
    public int centerZ
    {
        get
        {
            return m_CenterZ;
        }
    }

    List<Vector2>[] m_RoundPreValue = new List<Vector2>[] { new List<Vector2>(), new List<Vector2>() };

    Dictionary<int, List<GameConfig.RegionType>> m_RegionTypeDic;
    Dictionary<GameConfig.RegionType, RegionInfo> m_RegionInfoDic;      // 각각의 RegionType이 어떤 형태로 Region을 구성하고 있는지 저장해둔다.

    int m_Theme = 0;

    float m_AngleIndex = -90.0f;
    public float angleIndex
    {
        get
        {
            return m_AngleIndex;
        }
        set
        {
            m_AngleIndex = value;
        }
    }

    List<RegionDirectionCheck> m_DirectionCheckList = new List<RegionDirectionCheck>();
    public List<RegionDirectionCheck> directionCheckList
    {
        get
        {
            return m_DirectionCheckList;
        }
    }

    Dictionary<float, List<Vector2>> m_SearchOrderDic = new Dictionary<float, List<Vector2>>();
    public Dictionary<float, List<Vector2>> searchOrderDic
    {
        get
        {
            return m_SearchOrderDic;
        }
    }
    List<Vector2> m_ForwardGridList = new List<Vector2>();

    Coroutine m_UpdateCoroutine = null;

    GameManager m_GameMgr = null;
    ChaserManager m_ChaserMgr = null;
    NPCManager m_NPCMgr = null;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        m_RegionObjectPool = new ObjectPool(m_RegionPoolCount, m_RegionPrefab, m_RegionParents);

        for(int i = 0; i < m_RoadParents.Length; i++)
        {
            ObjectPool objectPool = new ObjectPool(m_RoadPoolCount[i], m_RoadPrefab[i], m_RoadParents[i]);
            m_RoadObjectPool.Add(objectPool);
        }

        //for (int i = 0; i < m_GroundParents.Length; i++)
        //{
        //    ObjectPool objectPool = new ObjectPool(m_GroundPoolCount[i], m_GroundPrefab[i], m_GroundParents[i]);
        //    m_GroundObjectPool.Add(objectPool);
        //}

        m_RegionDic = new Dictionary<long, Region>();
        m_ViewRegionDic = new Dictionary<long, Region>();
        m_RemoveRegionList = new List<long>();
        m_RegionTypeDic = new Dictionary<int, List<GameConfig.RegionType>>();
        m_RegionInfoDic = new Dictionary<GameConfig.RegionType, RegionInfo>();

        m_RegionHalfSize = (m_RegionSize * 0.5f);

        m_GridLength = Mathf.RoundToInt(m_ViewingDistance * 2.0f / m_RegionSize);
        EnforceGridSizeRules();
        m_HalfGridLength = Mathf.FloorToInt(m_GridLength / 2.0f);

        m_CenterX = -99999;
        m_CenterZ = -99999;

        m_RoundPreValue[0].Add(new Vector2(0, 1));
        m_RoundPreValue[0].Add(new Vector2(1, 1));
        m_RoundPreValue[0].Add(new Vector2(1, 0));
        m_RoundPreValue[0].Add(new Vector2(1, -1));
        m_RoundPreValue[0].Add(new Vector2(0, -1));
        m_RoundPreValue[0].Add(new Vector2(-1, -1));
        m_RoundPreValue[0].Add(new Vector2(-1, 0));
        m_RoundPreValue[0].Add(new Vector2(-1, 1));

        m_RoundPreValue[1].Add(new Vector2(0, 2));
        m_RoundPreValue[1].Add(new Vector2(1, 2));
        m_RoundPreValue[1].Add(new Vector2(2, 2));
        m_RoundPreValue[1].Add(new Vector2(2, 1));
        m_RoundPreValue[1].Add(new Vector2(2, 0));
        m_RoundPreValue[1].Add(new Vector2(2, -1));
        m_RoundPreValue[1].Add(new Vector2(2, -2));
        m_RoundPreValue[1].Add(new Vector2(1, -2));
        m_RoundPreValue[1].Add(new Vector2(0, -2));
        m_RoundPreValue[1].Add(new Vector2(-1, -2));
        m_RoundPreValue[1].Add(new Vector2(-2, -2));
        m_RoundPreValue[1].Add(new Vector2(-2, -1));
        m_RoundPreValue[1].Add(new Vector2(-2, 0));
        m_RoundPreValue[1].Add(new Vector2(-2, 1));
        m_RoundPreValue[1].Add(new Vector2(-2, 2));
        m_RoundPreValue[1].Add(new Vector2(1, 2));

        // 10 : North, South
        List<GameConfig.RegionType> regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.V_Straight);
        m_RegionTypeDic.Add(10, regionTypeList);

        // 20 : East, West
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.H_Straight);
        m_RegionTypeDic.Add(20, regionTypeList);

        // 6 : North, East
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.NE_Corner);
        m_RegionTypeDic.Add(6, regionTypeList);

        // 12 : East, South
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.ES_Corner);
        m_RegionTypeDic.Add(12, regionTypeList);

        // 24 : South, West
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.SW_Corner);
        m_RegionTypeDic.Add(24, regionTypeList);

        // 18 : West, North
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.WN_Corner);
        m_RegionTypeDic.Add(18, regionTypeList);

        // 14 : North, East, South
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.V_Straight);
        regionTypeList.Add(GameConfig.RegionType.NE_Corner);
        regionTypeList.Add(GameConfig.RegionType.ES_Corner);
        regionTypeList.Add(GameConfig.RegionType.NES_Intersection);
        m_RegionTypeDic.Add(14, regionTypeList);

        // 28 : East, South, West
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.H_Straight);
        regionTypeList.Add(GameConfig.RegionType.ES_Corner);
        regionTypeList.Add(GameConfig.RegionType.SW_Corner);
        regionTypeList.Add(GameConfig.RegionType.ESW_Intersection);
        m_RegionTypeDic.Add(28, regionTypeList);

        // 26 : South, West, North
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.V_Straight);
        regionTypeList.Add(GameConfig.RegionType.SW_Corner);
        regionTypeList.Add(GameConfig.RegionType.WN_Corner);
        regionTypeList.Add(GameConfig.RegionType.SWN_Intersection);
        m_RegionTypeDic.Add(26, regionTypeList);

        // 22 : West, North, East
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.H_Straight);
        regionTypeList.Add(GameConfig.RegionType.WN_Corner);
        regionTypeList.Add(GameConfig.RegionType.NE_Corner);
        regionTypeList.Add(GameConfig.RegionType.WNE_Intersection);
        m_RegionTypeDic.Add(22, regionTypeList);

        // 30 : North, East, South, West
        regionTypeList = new List<GameConfig.RegionType>();
        regionTypeList.Add(GameConfig.RegionType.V_Straight);
        regionTypeList.Add(GameConfig.RegionType.H_Straight);
        regionTypeList.Add(GameConfig.RegionType.NE_Corner);
        regionTypeList.Add(GameConfig.RegionType.ES_Corner);
        regionTypeList.Add(GameConfig.RegionType.SW_Corner);
        regionTypeList.Add(GameConfig.RegionType.WN_Corner);
        regionTypeList.Add(GameConfig.RegionType.NES_Intersection);
        regionTypeList.Add(GameConfig.RegionType.ESW_Intersection);
        regionTypeList.Add(GameConfig.RegionType.SWN_Intersection);
        regionTypeList.Add(GameConfig.RegionType.WNE_Intersection);
        regionTypeList.Add(GameConfig.RegionType.NESW_Intersection);
        m_RegionTypeDic.Add(30, regionTypeList);

        // GameConfig.RegionType.V_Straight
        RegionInfo regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.Straight;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.North;
        GroundInfo groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.North;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.South;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.V_Straight, regionInfo);

        // GameConfig.RegionType.H_Straight
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.Straight;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.East;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.East;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.West;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.H_Straight, regionInfo);

        // GameConfig.RegionType.NE_Corner
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.Corner;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.South;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Corner;
        groundInfo.m_Direction = GameConfig.Direction.South;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.East;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.NE_Corner, regionInfo);

        // GameConfig.RegionType.ES_Cornet
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.Corner;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.West;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Corner;
        groundInfo.m_Direction = GameConfig.Direction.West;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.South;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.ES_Corner, regionInfo);

        // GameConfig.RegionType.SW_Corner
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.Corner;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.North;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Corner;
        groundInfo.m_Direction = GameConfig.Direction.North;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.West;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.SW_Corner, regionInfo);

        // GameConfig.RegionType.WN_Corner
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.Corner;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.East;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Corner;
        groundInfo.m_Direction = GameConfig.Direction.East;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.North;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.WN_Corner, regionInfo);

        // GameConfig.RegionType.NES_Intersection
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.InterSection_3;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.South;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.North;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.East;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.South;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.NES_Intersection, regionInfo);

        // GameConfig.RegionType.ESW_Intersection
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.InterSection_3;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.West;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.East;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.South;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.West;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.ESW_Intersection, regionInfo);

        // GameConfig.RegionType.SWN_Intersection
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.InterSection_3;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.North;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.South;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.West;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.North;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.SWN_Intersection, regionInfo);

        // GameConfig.RegionType.WNE_Intersection
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.InterSection_3;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.East;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Straight;
        groundInfo.m_Direction = GameConfig.Direction.West;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.North;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.East;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.WNE_Intersection, regionInfo);

        // GameConfig.RegionType.NESW_Intersection
        regionInfo = new RegionInfo();
        regionInfo.m_RoadInfo.m_RoadType = GameConfig.RoadType.InterSection_4;
        regionInfo.m_RoadInfo.m_Direction = GameConfig.Direction.North;
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.North;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.East;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.South;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        groundInfo = new GroundInfo();
        groundInfo.m_GroundType = GameConfig.GroundType.Piece;
        groundInfo.m_Direction = GameConfig.Direction.West;
        regionInfo.m_GroundInfoList.Add(groundInfo);
        m_RegionInfoDic.Add(GameConfig.RegionType.NESW_Intersection, regionInfo);

        ////////////////////////////////////////////////////////////////////////
        // -90
        RegionDirectionCheck regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = -90.0f;
        regionDirectionCheck.startAngle = -112.5f;
        regionDirectionCheck.endAngle = -67.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        List<Vector2> searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(0, 1));
        searchOrderList.Add(new Vector2(-1, 1));
        searchOrderList.Add(new Vector2(1, 1));
        searchOrderList.Add(new Vector2(-1, 0));
        searchOrderList.Add(new Vector2(1, 0));
        searchOrderList.Add(new Vector2(-1, -1));
        searchOrderList.Add(new Vector2(0, -1));
        searchOrderList.Add(new Vector2(1, -1));
        m_SearchOrderDic.Add(-90.0f, searchOrderList);

        // -135
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = -135.0f;
        regionDirectionCheck.startAngle = -157.5f;
        regionDirectionCheck.endAngle = -112.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(-1, 1));
        searchOrderList.Add(new Vector2(-1, 0));
        searchOrderList.Add(new Vector2(0, 1));
        searchOrderList.Add(new Vector2(-1, -1));
        searchOrderList.Add(new Vector2(1, 1));
        searchOrderList.Add(new Vector2(0, -1));
        searchOrderList.Add(new Vector2(1, -1));
        searchOrderList.Add(new Vector2(1, 0));
        m_SearchOrderDic.Add(-135.0f, searchOrderList);

        // -180
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = -180.0f;
        regionDirectionCheck.startAngle = -180.0f;
        regionDirectionCheck.endAngle = -157.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        // -180
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = -180.0f;
        regionDirectionCheck.startAngle = 157.5f;
        regionDirectionCheck.endAngle = 180.0f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(-1, 0));
        searchOrderList.Add(new Vector2(-1, -1));
        searchOrderList.Add(new Vector2(-1, 1));
        searchOrderList.Add(new Vector2(0, -1));
        searchOrderList.Add(new Vector2(0, 1));
        searchOrderList.Add(new Vector2(1, -1));
        searchOrderList.Add(new Vector2(1, 0));
        searchOrderList.Add(new Vector2(1, 1));
        m_SearchOrderDic.Add(-180.0f, searchOrderList);

        // 135
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = 135.0f;
        regionDirectionCheck.startAngle = 112.5f;
        regionDirectionCheck.endAngle = 157.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(-1, -1));
        searchOrderList.Add(new Vector2(0, -1));
        searchOrderList.Add(new Vector2(-1, 0));
        searchOrderList.Add(new Vector2(1, -1));
        searchOrderList.Add(new Vector2(-1, 1));
        searchOrderList.Add(new Vector2(1, 0));
        searchOrderList.Add(new Vector2(1, 1));
        searchOrderList.Add(new Vector2(0, 1));
        m_SearchOrderDic.Add(135.0f, searchOrderList);

        // 90
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = 90.0f;
        regionDirectionCheck.startAngle = 67.5f;
        regionDirectionCheck.endAngle = 112.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(0, -1));
        searchOrderList.Add(new Vector2(1, -1));
        searchOrderList.Add(new Vector2(-1, -1));
        searchOrderList.Add(new Vector2(1, 0));
        searchOrderList.Add(new Vector2(-1, 0));
        searchOrderList.Add(new Vector2(1, 1));
        searchOrderList.Add(new Vector2(0, 1));
        searchOrderList.Add(new Vector2(-1, 1));
        m_SearchOrderDic.Add(90.0f, searchOrderList);

        // 45
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = 45.0f;
        regionDirectionCheck.startAngle = 22.5f;
        regionDirectionCheck.endAngle = 67.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(1, -1));
        searchOrderList.Add(new Vector2(1, 0));
        searchOrderList.Add(new Vector2(0, -1));
        searchOrderList.Add(new Vector2(1, 1));
        searchOrderList.Add(new Vector2(-1, -1));
        searchOrderList.Add(new Vector2(0, 1));
        searchOrderList.Add(new Vector2(-1, 1));
        searchOrderList.Add(new Vector2(-1, 0));
        m_SearchOrderDic.Add(45.0f, searchOrderList);

        // 0
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = 0.0f;
        regionDirectionCheck.startAngle = -22.5f;
        regionDirectionCheck.endAngle = 22.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(1, 0));
        searchOrderList.Add(new Vector2(1, 1));
        searchOrderList.Add(new Vector2(1, -1));
        searchOrderList.Add(new Vector2(0, 1));
        searchOrderList.Add(new Vector2(0, -1));
        searchOrderList.Add(new Vector2(-1, 1));
        searchOrderList.Add(new Vector2(-1, 0));
        searchOrderList.Add(new Vector2(-1, -1));
        m_SearchOrderDic.Add(0.0f, searchOrderList);

        // -45
        regionDirectionCheck = new RegionDirectionCheck();
        regionDirectionCheck.angleIndx = -45.0f;
        regionDirectionCheck.startAngle = -67.5f;
        regionDirectionCheck.endAngle = -22.5f;
        m_DirectionCheckList.Add(regionDirectionCheck);
        searchOrderList = new List<Vector2>();
        searchOrderList.Add(new Vector2(1, 1));
        searchOrderList.Add(new Vector2(0, 1));
        searchOrderList.Add(new Vector2(1, 0));
        searchOrderList.Add(new Vector2(-1, 1));
        searchOrderList.Add(new Vector2(1, -1));
        searchOrderList.Add(new Vector2(-1, 0));
        searchOrderList.Add(new Vector2(-1, -1));
        searchOrderList.Add(new Vector2(0, -1));
        m_SearchOrderDic.Add(-45.0f, searchOrderList);

        //MapObjectManager.instance.Initialize();
    }

    public void Restore()
    {
        m_Initialize = false;

        m_RemoveRegionList.Clear();
        Dictionary<long, Region>.Enumerator etor = m_RegionDic.GetEnumerator();
        while (etor.MoveNext())
        {
            etor.Current.Value.Restore();
            m_RegionObjectPool.PutObjectInPool(etor.Current.Value.gameObject);
        }

        m_RegionDic.Clear();

        if(m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    void EnforceGridSizeRules()
    {
        // Cannot be an even number, so reduce by 1 if it is:
        if ((m_GridLength % 2) == 0)
            m_GridLength--;
        // Cannot be less than 3, so set to 3 if it is:
        if (m_GridLength < 3)
            m_GridLength = 3;
    }

    public void StartUpdate()
    {
        m_GameMgr = GameManager.instance;
        m_ChaserMgr = ChaserManager.instance;
        m_NPCMgr = NPCManager.instance;

        m_FirstRegion = true;
        m_FirstUpRegion = true;
        m_CenterX = -99999;
        m_CenterZ = -99999;

        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        m_PlayerTransform = playerObject.transform;

        m_Initialize = true;

        //this.StartCoroutineAsync(UpdateRegion());
        if(m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateRegion());
        }        
    }

    public void SetPlayer(Transform newPlayerTransform)
    {
        m_PlayerTransform = newPlayerTransform;
    }
    //IEnumerator UpdateRegion()
    //{
    //    while (true)
    //    {
    //        if (m_Initialize)
    //        {
    //            float playerX = 0.0f;
    //            float playerZ = 0.0f;
    //            int gridX = 0;
    //            int gridZ = 0;
    //            Region region = null;
    //            long currentRegionIndex = 0;
    //            if (m_FirstRegion)
    //            {
    //                m_CenterX = gridX;
    //                m_CenterZ = gridZ;

    //                m_ForwardGridList.Clear();
    //                m_ForwardGridList.Add(new Vector2(0, 1));
    //                m_ForwardGridList.Add(new Vector2(1, 1));
    //                m_ForwardGridList.Add(new Vector2(1, 0));
    //                m_ForwardGridList.Add(new Vector2(1, -1));
    //                m_ForwardGridList.Add(new Vector2(0, -1));
    //                m_ForwardGridList.Add(new Vector2(-1, -1));
    //                m_ForwardGridList.Add(new Vector2(-1, 0));
    //                m_ForwardGridList.Add(new Vector2(-1, 1));

    //                currentRegionIndex = GetRegionIndex(gridX, gridZ);
    //                // 처음 생성되는 지역일 경우 지역을 생성 해준다.
    //                if (!m_RegionDic.ContainsKey(currentRegionIndex))
    //                {
    //                    GameObject regionObject = m_RegionObjectPool.GetObjectFromPool();
    //                    region = regionObject.GetComponent<Region>();
    //                    Vector3 pos = new Vector3(gridX * m_RegionSize, 0, gridZ * m_RegionSize);
    //                    if (m_FirstRegion)
    //                    {
    //                        region.Initialize(true, false, gridX, gridZ, pos, m_Theme);
    //                        m_FirstRegion = false;
    //                    }
    //                    else
    //                    {
    //                        region.Initialize(false, false, gridX, gridZ, pos, m_Theme);
    //                    }

    //                    m_RegionDic.Add(currentRegionIndex, region);
    //                }

    //                for (int i = 0; i < m_ForwardGridList.Count; i++)
    //                {
    //                    Vector2 forwardGrid = m_ForwardGridList[i];
    //                    long forwardRegionIndex = GetRegionIndex((int)forwardGrid.x, (int)forwardGrid.y);
    //                    if (!m_RegionDic.ContainsKey(forwardRegionIndex))
    //                    {
    //                        GameObject regionObject = m_RegionObjectPool.GetObjectFromPool();
    //                        region = regionObject.GetComponent<Region>();
    //                        Vector3 pos = new Vector3((int)forwardGrid.x * m_RegionSize, 0, (int)forwardGrid.y * m_RegionSize);
    //                        if (m_FirstUpRegion)     // 첫번째로 생성하는 주변 지역은 북쪽이므로 m_FirstUpRegion 조건 체크를 바로 해준다.
    //                        {
    //                            region.Initialize(false, true, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme);
    //                            m_FirstUpRegion = false;
    //                        }
    //                        else
    //                        {
    //                            region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme);
    //                        }

    //                        m_RegionDic.Add(forwardRegionIndex, region);
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                if (m_GameMgr.state == GameManager.State.Playing)
    //                {
    //                    playerX = m_PlayerTransform.position.x;
    //                    playerZ = m_PlayerTransform.position.z;

    //                    gridX = Mathf.RoundToInt(playerX / m_RegionSize);
    //                    gridZ = Mathf.RoundToInt(playerZ / m_RegionSize);

    //                    m_RemoveRegionList.Clear();
    //                    Dictionary<long, Region>.Enumerator etor = m_RegionDic.GetEnumerator();
    //                    while (etor.MoveNext())
    //                    {
    //                        region = etor.Current.Value;
    //                        float distance = Vector3.Distance(m_PlayerTransform.position, region.transform.position);
    //                        if (Mathf.Abs(distance) >= 300.0f)
    //                        {
    //                            etor.Current.Value.Restore();
    //                            m_RegionObjectPool.PutObjectInPool(etor.Current.Value.gameObject);
    //                            m_RemoveRegionList.Add(etor.Current.Key);
    //                        }
    //                    }

    //                    for (int i = 0; i < m_RemoveRegionList.Count; i++)
    //                    {
    //                        m_RegionDic.Remove(m_RemoveRegionList[i]);
    //                    }

    //                    //if (m_CenterX != gridX || m_CenterZ != gridZ)
    //                    //{
    //                    m_CenterX = gridX;
    //                    m_CenterZ = gridZ;

    //                    Vector3 playerForward = m_PlayerTransform.forward;
    //                    //float angle2 = Vector2.Angle(Vector2.right, new Vector2(RegionManager.instance.playerTransform.forward.x, RegionManager.instance.playerTransform.forward.z));
    //                    // 플레이어 차량이 향하고 있는 방향의 각도를 구한다(angle값은 0~180, -180~0 값)
    //                    float angle = Vector3.Angle(Vector3.right, playerForward);
    //                    angle = angle * Mathf.Sign(Vector3.Cross(Vector3.right, playerForward).y);
    //                    //angle* Mathf.Si(Vector3.Cross(a, b).y);
    //                    Debug.Log("Player Forward : " + angle + ", " + playerForward);

    //                    // 플레이어 차량이 향하는 각도에 해당하는 추격차량 생성 포인트 구간이 어딘지 계산한다.
    //                    // ex : -90도를 향하고 있을 때 추격 차량 생성 구간은 -112.5에서 -67.5 구간에 해당된다.
    //                    m_AngleIndex = -90.0f;
    //                    for (int i = 0; i < m_DirectionCheckList.Count; i++)
    //                    {
    //                        RegionDirectionCheck regionDirectionCheck = m_DirectionCheckList[i];
    //                        if (regionDirectionCheck.startAngle <= angle &&
    //                            angle < regionDirectionCheck.endAngle)
    //                        {
    //                            m_AngleIndex = regionDirectionCheck.angleIndx;
    //                            break;
    //                        }
    //                    }

    //                    if (m_SearchOrderDic.ContainsKey(m_AngleIndex))
    //                    {
    //                        List<Vector2> searchOrderList = m_SearchOrderDic[m_AngleIndex];
    //                        Vector2 directionVec = searchOrderList[0];
    //                        int createGridX = m_CenterX + (int)directionVec.x;
    //                        int createGridZ = m_CenterZ + (int)directionVec.y;
    //                        currentRegionIndex = GetRegionIndex(createGridX, createGridZ);
    //                        if (!m_RegionDic.ContainsKey(currentRegionIndex))
    //                        {
    //                            GameObject regionObject = m_RegionObjectPool.GetObjectFromPool();
    //                            region = regionObject.GetComponent<Region>();
    //                            Vector3 pos = new Vector3((int)createGridX * m_RegionSize, 0, (int)createGridZ * m_RegionSize);

    //                            region.Initialize(false, false, createGridX, createGridZ, pos, m_Theme);

    //                            m_RegionDic.Add(currentRegionIndex, region);
    //                        }
    //                    }
    //                    //}
    //                }
    //            }

    //            //Debug.Log(string.Format("gridX : {0}, gridZ : {1}, forwadGridX : {2}, forwadGridX : {3}", gridX, gridZ, forwadGridX, forwadGridZ));
    //            //Region region = null;

    //            //m_RemoveRegionList.Clear();
    //            //Dictionary<long, Region>.Enumerator etor = m_RegionDic.GetEnumerator();
    //            //while (etor.MoveNext())
    //            //{
    //            //    bool excludeRegion = false;
    //            //    region = etor.Current.Value;
    //            //    for (int i = 0; i < m_ForwardGridList.Count; i++)
    //            //    {
    //            //        Vector2 forwardGrid = m_ForwardGridList[i];
    //            //        if (region.regionX == forwardGrid.x && region.regionZ == forwardGrid.y)
    //            //        {
    //            //            excludeRegion = true;
    //            //            break;
    //            //        }
    //            //    }

    //            //    if (excludeRegion == false)
    //            //    {
    //            //        float distance = Vector3.Distance(m_PlayerTransform.position, region.transform.position);
    //            //        if (Mathf.Abs(distance) >= 100.0f)
    //            //        {
    //            //            etor.Current.Value.Restore();
    //            //            m_RegionObjectPool.PutObjectInPool(etor.Current.Value.gameObject);
    //            //            m_RemoveRegionList.Add(etor.Current.Key);
    //            //        }
    //            //    }
    //            //}

    //            //for (int i = 0; i < m_RemoveRegionList.Count; i++)
    //            //{
    //            //    m_RegionDic.Remove(m_RemoveRegionList[i]);
    //            //}

    //            //long currentRegionIndex = GetRegionIndex(gridX, gridZ);
    //            //// 처음 생성되는 지역일 경우 지역을 생성 해준다.
    //            //if (!m_RegionDic.ContainsKey(currentRegionIndex))
    //            //{
    //            //    GameObject regionObject = m_RegionObjectPool.GetObjectFromPool();
    //            //    region = regionObject.GetComponent<Region>();
    //            //    Vector3 pos = new Vector3(gridX * m_RegionSize, 0, gridZ * m_RegionSize);
    //            //    if (m_FirstRegion)
    //            //    {
    //            //        region.Initialize(true, false, gridX, gridZ, pos, m_Theme);
    //            //        m_FirstRegion = false;
    //            //    }
    //            //    else
    //            //    {
    //            //        region.Initialize(false, false, gridX, gridZ, pos, m_Theme);
    //            //    }

    //            //    m_RegionDic.Add(currentRegionIndex, region);
    //            //}

    //            //for (int i = 0; i < m_ForwardGridList.Count; i++)
    //            //{
    //            //    Vector2 forwardGrid = m_ForwardGridList[i];
    //            //    long forwardRegionIndex = GetRegionIndex((int)forwardGrid.x, (int)forwardGrid.y);
    //            //    if (!m_RegionDic.ContainsKey(forwardRegionIndex))
    //            //    {
    //            //        GameObject regionObject = m_RegionObjectPool.GetObjectFromPool();
    //            //        region = regionObject.GetComponent<Region>();
    //            //        Vector3 pos = new Vector3((int)forwardGrid.x * m_RegionSize, 0, (int)forwardGrid.y * m_RegionSize);
    //            //        if (m_FirstUpRegion)     // 첫번째로 생성하는 주변 지역은 북쪽이므로 m_FirstUpRegion 조건 체크를 바로 해준다.
    //            //        {
    //            //            region.Initialize(false, true, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme);
    //            //            m_FirstUpRegion = false;
    //            //        }
    //            //        else
    //            //        {
    //            //            region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme);
    //            //        }

    //            //        m_RegionDic.Add(forwardRegionIndex, region);
    //            //    }
    //            //}
    //        }

    //        yield return new WaitForSeconds(0.1f);
    //    }
    //}

    //void Update()
    //{
    //    if (m_Initialize)
    //    {
            
    //    }
    //}

    IEnumerator UpdateRegion()
    {
        while (true)
        {
            if (m_Initialize)
            {
                //Debug.Log("UpdateRegion 0");
                Vector3 playerForward = m_PlayerTransform.forward;

                //float angle2 = Vector2.Angle(Vector2.right, new Vector2(RegionManager.instance.playerTransform.forward.x, RegionManager.instance.playerTransform.forward.z));
                // 플레이어 차량이 향하고 있는 방향의 각도를 구한다(angle값은 0~180, -180~0 값)
                float angle = Vector3.Angle(Vector3.right, playerForward);
                angle = angle * Mathf.Sign(Vector3.Cross(Vector3.right, playerForward).y);
                //angle* Mathf.Si(Vector3.Cross(a, b).y);
                //Debug.Log("Player Forward : " + angle + ", " + playerForward);

                // 플레이어 차량이 향하는 각도에 해당하는 추격차량 생성 포인트 구간이 어딘지 계산한다.
                // ex : -90도를 향하고 있을 때 추격 차량 생성 구간은 -112.5에서 -67.5 구간에 해당된다.
                m_AngleIndex = -90.0f;
                for (int i = 0; i < m_DirectionCheckList.Count; i++)
                {
                    RegionDirectionCheck regionDirectionCheck = m_DirectionCheckList[i];
                    if (regionDirectionCheck.startAngle <= angle &&
                        angle < regionDirectionCheck.endAngle)
                    {
                        m_AngleIndex = regionDirectionCheck.angleIndx;
                        break;
                    }
                }

                float playerX = 0.0f;
                float playerZ = 0.0f;
                int gridX = 0;
                int gridZ = 0;
                if (m_FirstRegion)
                {
                    m_CenterX = gridX;
                    m_CenterZ = gridZ;

                    m_ForwardGridList.Clear();
                    m_ForwardGridList.Add(new Vector2(0, 1));
                    m_ForwardGridList.Add(new Vector2(1, 1));
                    m_ForwardGridList.Add(new Vector2(1, 0));
                    m_ForwardGridList.Add(new Vector2(1, -1));
                    m_ForwardGridList.Add(new Vector2(0, -1));
                    m_ForwardGridList.Add(new Vector2(-1, -1));
                    m_ForwardGridList.Add(new Vector2(-1, 0));
                    m_ForwardGridList.Add(new Vector2(-1, 1));
                }
                else
                {
                    playerX = m_PlayerTransform.position.x;
                    playerZ = m_PlayerTransform.position.z;

                    gridX = Mathf.RoundToInt(playerX / m_RegionSize);
                    gridZ = Mathf.RoundToInt(playerZ / m_RegionSize);

                    if (m_CenterX != gridX || m_CenterZ != gridZ)
                    {
                        m_CenterX = gridX;
                        m_CenterZ = gridZ;

                        m_ForwardGridList.Clear();
                        // 북, 북동, 동, 남동, 남, 남서, 서, 북서 <- 이순서를 꼭 지켜야 한다.
                        m_ForwardGridList.Add(new Vector2(m_CenterX, m_CenterZ + 1));
                        m_ForwardGridList.Add(new Vector2(m_CenterX + 1, m_CenterZ + 1));
                        m_ForwardGridList.Add(new Vector2(m_CenterX + 1, m_CenterZ));
                        m_ForwardGridList.Add(new Vector2(m_CenterX + 1, m_CenterZ - 1));
                        m_ForwardGridList.Add(new Vector2(m_CenterX, m_CenterZ - 1));
                        m_ForwardGridList.Add(new Vector2(m_CenterX - 1, m_CenterZ - 1));
                        m_ForwardGridList.Add(new Vector2(m_CenterX - 1, m_CenterZ));
                        m_ForwardGridList.Add(new Vector2(m_CenterX - 1, m_CenterZ + 1));
                    }
                }

                //Debug.Log("UpdateRegion 1");

                //Debug.Log(string.Format("gridX : {0}, gridZ : {1}", gridX, gridZ));

                Region region = null;
                bool isCreateRegion = false;
                long currentRegionIndex = GetRegionIndex(gridX, gridZ);
                // 처음 생성되는 지역일 경우 지역을 생성 해준다.
                if (!m_RegionDic.ContainsKey(currentRegionIndex))
                {
                    GameObject regionObject = m_RegionObjectPool.GetObjectFromPool();
                    region = regionObject.GetComponent<Region>();
                    Vector3 pos = new Vector3(gridX * m_RegionSize, 0, gridZ * m_RegionSize);
                    if (m_FirstRegion)
                    {
                        //region.Initialize(true, false, gridX, gridZ, pos, m_Theme, false);
                        StartCoroutine(region.Initialize(true, false, gridX, gridZ, pos, m_Theme, false));
                    }
                    else
                    {
                        //region.Initialize(false, false, gridX, gridZ, pos, m_Theme, false);
                        yield return StartCoroutine(region.Initialize(false, false, gridX, gridZ, pos, m_Theme, false));
                    }

                    m_RegionDic.Add(currentRegionIndex, region);
                    isCreateRegion = true;
                }

                //Debug.Log("UpdateRegion 2");

                List<Vector2> searchOrderList = m_SearchOrderDic[m_AngleIndex];
                for (int i = 0; i < m_ForwardGridList.Count; i++)
                {
                    Vector2 forwardGrid = m_ForwardGridList[i];
                    long forwardRegionIndex = GetRegionIndex((int)forwardGrid.x, (int)forwardGrid.y);
                    if (!m_RegionDic.ContainsKey(forwardRegionIndex))
                    {
                        GameObject regionObject = m_RegionObjectPool.GetObjectFromPool();
                        region = regionObject.GetComponent<Region>();
                        Vector3 pos = new Vector3((int)forwardGrid.x * m_RegionSize, 0, (int)forwardGrid.y * m_RegionSize);
                        if (m_FirstUpRegion)     // 첫번째로 생성하는 주변 지역은 북쪽이므로 m_FirstUpRegion 조건 체크를 바로 해준다.
                        {
                            //region.Initialize(false, true, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, false);
                            StartCoroutine(region.Initialize(false, true, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, false));
                            m_FirstUpRegion = false;
                        }
                        else
                        {
                            if(m_FirstRegion)
                            {
                                //region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, false);
                                StartCoroutine(region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, false));
                            }
                            else
                            {
                                Vector2 searchOrderVec = searchOrderList[0];
                                if ((m_CenterX + searchOrderVec.x) == (int)forwardGrid.x &&
                                   (m_CenterZ + searchOrderVec.y) == (int)forwardGrid.y)
                                {
                                    //region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, true);
                                    yield return StartCoroutine(region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, true));
                                }
                                else
                                {
                                    //region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, false);
                                    yield return StartCoroutine(region.Initialize(false, false, (int)forwardGrid.x, (int)forwardGrid.y, pos, m_Theme, false));
                                }
                            }
                        }

                        m_RegionDic.Add(forwardRegionIndex, region);
                        isCreateRegion = true;
                    }
                }

                //Debug.Log("UpdateRegion 3");

                if (!isCreateRegion)
                {
                    float currentTime = TimeManager.instance.currentGameTime;
                    m_RemoveRegionList.Clear();
                    Dictionary<long, Region>.Enumerator etor = m_RegionDic.GetEnumerator();
                    while (etor.MoveNext())
                    {
                        bool excludeRegion = false;
                        region = etor.Current.Value;
                        for (int i = 0; i < m_ForwardGridList.Count; i++)
                        {
                            Vector2 forwardGrid = m_ForwardGridList[i];
                            if (region.regionX == forwardGrid.x && region.regionZ == forwardGrid.y)
                            {
                                excludeRegion = true;
                                break;
                            }
                        }

                        if (excludeRegion == false)
                        {
                            // 경계선에서 차량이 움직일 경우 쓸모 없이 지형이 삭제되고 생성되는게
                            // 너무 빈번하게 일어나기 때문에 지형 생성 후 10초 동안은 무조건 삭제 안되게 한다.
                            //if (currentTime - region.makeTime >= 10.0f)
                            //{
                                float distance = Vector3.Distance(m_PlayerTransform.position, region.transform.position);
                                if (Mathf.Abs(distance) >= 177.0f)
                                {
                                    //Debug.Log("!!! Check Delete Region Time : " + (currentTime - region.makeTime));
                                    region.Restore();
                                    m_RegionObjectPool.PutObjectInPool(region.gameObject);
                                    m_RemoveRegionList.Add(etor.Current.Key);

                                    yield return new WaitForEndOfFrame();
                                }
                            //}
                            //else
                            //{
                            //    Debug.Log("!!! Delete But Wait!!");
                            //}
                            //else
                            //{
                            //    float distance = Vector3.Distance(m_PlayerTransform.position, region.transform.position);
                            //    if (Mathf.Abs(distance) >= 177.0f)
                            //    {
                            //        Debug.Log("@@@ Dont Check Delete Region Time : " + (currentTime - region.makeTime));
                            //    }
                            //}                       
                        }
                    }

                    for (int i = 0; i < m_RemoveRegionList.Count; i++)
                    {
                        m_RegionDic.Remove(m_RemoveRegionList[i]);
                    }
                }                

                if (m_FirstRegion)
                {
                    m_FirstRegion = false;
                }

                //Debug.Log("UpdateRegion 4");

                if (m_ChaserMgr.IsCreateGangChaser())
                {
                    yield return StartCoroutine(m_ChaserMgr.CreateGangChaser());
                }

                if (m_ChaserMgr.IsCreateChaser())
                {
                    yield return StartCoroutine(m_ChaserMgr.CreateChaser());
                }

                yield return StartCoroutine(m_NPCMgr.UpdateNPC());
            }

            yield return new WaitForEndOfFrame();
        }
    }

    long GetRegionIndex(int x, int z)
    {
        return (x * 100) + z;
    }
    
    public List<GameConfig.RegionType> GetRegionTypeList(int enableDirection)
    {
        if(m_RegionTypeDic.ContainsKey(enableDirection))
        {
            return m_RegionTypeDic[enableDirection];
        }
        else
        {
            return null;
        }        
    }

    public RegionInfo GetRegionInfo(GameConfig.RegionType regionType)
    {
        return m_RegionInfoDic[regionType];
    }

    public ObjectPool GetRoadObjectPool(GameConfig.RoadType roadType)
    {
        return m_RoadObjectPool[(int)roadType];
    }

    //public ObjectPool GetGroundObjectPool(GameConfig.GroundType groundType)
    //{
    //    return m_GroundObjectPool[(int)groundType];
    //}

    public Region GetRegion(int x, int z)
    {
        long regionIndex = GetRegionIndex(x, z);
        if(m_RegionDic.ContainsKey(regionIndex))
        {
            return m_RegionDic[regionIndex];
        }
        else
        {
            return null;
        }
    }

    public bool IsExistRegion(float x, float z)
    {
        if (m_RegionDic.ContainsKey(GetRegionIndex(Mathf.RoundToInt(x / m_RegionSize), Mathf.RoundToInt(z / m_RegionSize))))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ActiveDeliveryMissionDestination(MissionInfo progressMissionInfo)
    {
        Region region = GetRegion((int)progressMissionInfo.destinationGrid.x, (int)progressMissionInfo.destinationGrid.z);
        if (region != null)
        {
            region.ActiveDeliveryMissionDestination();
        }        
    }
}

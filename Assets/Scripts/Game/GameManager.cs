﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using Heyzap;
using CodeStage.AntiCheat.ObscuredTypes;

public class GameManager : Singleton<GameManager>
{
    public enum State
    {
        Waiting = 0,
        Starting = 1,
        Playing = 2,
        GameOver = 3,
        Result = 4,
    };

    public GameObject fpsCounterObject;

    public GameObject behindScrollPopupPanel;
    public GameObject popupPanel;
    public GameObject frontScrollPopupPanel;
    public GameObject commonPopupPanel;

    public GameObject waitingUIRootObject;
    public GameObject playingUIRootObject;
    public GameObject menuObject;
    bool m_IsMenuShow = false;
    bool m_EnableMenu = false;

    public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game.
    public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
    public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases.
    public GameCameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases.    
    public GameObject[] m_PlayerVehiclePrefabArray;
    public GameObject[] m_PetPrefabArray;
    public UILabel m_ZerobackLabel;
    public UILabel m_SpeedLabel;
    public UILabel m_GearLabel;
    public UILabel m_RPMLabel;
    public UILabel evilPointLabel;
    public UILabel startButtonLabel;
    public UILabel questButtonLabel;
    public Animator questTipAnimator;
    public UILabel adStartButtonLabel;
    public UILabel adStartDescLabel;
    public UILabel gameOverBurstedLabel;
    public UILabel gameOverWastedLabel;
    public Animator missionAnimator;
    public UILabel missionNoticeLabel;
    public UILabel missionProgressLabel;
    public UILabel missionRemainTimeLabel;
    public UILabel missionRewardLabel;
    public GameObject arrowObject;
    public Transform arrowTransform;
    public Animator missionResultAnimator;
    public UILabel missionLabel;
    public UILabel missionSuccessLabel;
    public UILabel missionFailLabel;
    public UILabel distanceLabel;
    public UILabel cashLabel;
    public UILabel timeDescLabel;
    public UILabel timeLabel;
    public UILabel scoreDescLabel;
    public UILabel scoreLabel;
    public GameObject inputHelperObject;
    public UILabel inputHelperLabel;
    bool m_HideInputHelper = false;
    public bool hideInputHelper
    {
        get
        {
            return m_HideInputHelper;
        }
    }
    public GameObject pausePopupObject;
    bool m_ExceptionPause = false;
    public bool exceptionPause
    {
        get
        {
            return m_ExceptionPause;
        }
        set
        {
            m_ExceptionPause = value;
        }
    }
    bool m_WaitGameStartWithAD = false;
    public UILabel surrenderLabel;
	public GameObject couponButtonObject;

    int m_PrevCash = 0;
    int m_ExceedCash = 0;
    //bool m_IsPlayingGetCash = false;
    //Coroutine m_GetCashCoroutine = null;
    //public AnimationCurve cashAnimationCurve;

    private GameObject m_VehiclesObject;
    private VehicleParent m_VehicleParent;
    private TestMobileInput m_TestMobileInput;
    private PetShellAttackAI m_PetShellAttackAI = null;

    private GameObject m_VehiclesObjectTest;
    private GameObject m_PetObject;

    public GameObject introBuildingPrefab;
    IntroBuilding m_IntroBuilding = null;

    public IntroDecoration introDecoration;
    public GameObject resultUIPanel;
    public ResultUI resultUI;
    public GameObject camHelicopter;
    public GameObject giftUIPanel;
    public GiftUI giftUI;

    public Animator questAnimator;
    public UILabel questContentLabel;
    public UILabel questSuccessLabel;
    public UILabel questFailLabel;

    public AudioSource introEngineSnd;

    Motor engine;
    Transmission trans;
    GearboxTransmission gearbox;
    ContinuousTransmission varTrans;
    DriveForce driveForce;

    private int m_RoundNumber;                  // Which round the game is currently on.
    private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts.
    private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends.
    //private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won.
    //private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won.

    //public InfiniteMapBuilderSpawn m_InfiniteMapBuilderSpawn;

    private int m_ChaserCarCount = 0;

    bool m_IsCheckZeroback = false;
    System.DateTime m_StartTime;

    bool m_ArrestPlayer = false;
    public bool arrestPlayer
    {
        set
        {
            m_ArrestPlayer = value;
        }
        get
        {
            return m_ArrestPlayer;
        }
    }

    bool m_DestroyPlayer = false;
    public bool destroyPlayer
    {
        set
        {
            m_DestroyPlayer = value;
        }
        get
        {
            return m_DestroyPlayer;
        }
    }

    bool m_Pause = false;
    float m_PrevSFXVolume = 0.0f;
    long m_StartPlayTimeTicks;
    int m_StartPlayTime = 0;
    int m_PlayTime;
    long m_ElapsedTimeL = 0;
    int m_ElapsedTime = 0;
    System.DateTime m_NowTime;

    int m_EvilCount = 0;
    public int evilCount
    {
        get
        {
            return m_EvilCount;
        }
        set
        {
            m_EvilCount = value;
        }
    }
    int m_EvilPoint = 0;
    public int evilPoint
    {
        get
        {
            return m_EvilPoint;
        }
        set
        {
            m_EvilPoint = value;
        }
    }
    public Animator[] evilMarkAnimatorArray;
    public UISprite[] evilMarkProgressArray;

    int m_Score = 0;
    public int score
    {
        get
        {
            return m_Score;
        }
        set
        {
            m_Score = value;
        }
    }

    State m_State = State.Waiting;
    public State state
    {
        get
        {
            return m_State;
        }
    }

    bool m_IsInputStart = false;
    public bool isInputStart
    {
        get
        {
            return m_IsInputStart;
        }
        set
        {
            m_IsInputStart = value;
        }
    }

    bool m_IsInputPlaying = false;
    public bool isInputPlaying
    {
        get
        {
            return m_IsInputPlaying;
        }
        set
        {
            m_IsInputPlaying = value;
        }
    }

    public GameObject finishObject;
    public Animator finishAnimator;
    public GameObject restoreBlockUI;

    public ColorCorrectionCurves colorCorrectionCurves;
    float m_ColorSaturation = 1.0f;
    //public TiltShift tiltShift;
    public CameraFilterPack_Blur_Tilt_Shift tiltShift;
    float m_BlurArea = 1.0f;
    float m_GameOverTimeScale = 1.0f;

    public GetCashEffectGroup m_GetCashEffectGroup;

    public GameObject[] packageButtonObjects;
    public UILabel[] packageButtonLabels;
    public UITable packageButtonTable;
    PackageData[] m_PackageDatas = new PackageData[4];

    // Tip 관련
    public GameObject tipRootObject;
    public Animator tipAnimator;
    public UILabel tipLabel;

    public GameObject[] logoObjectArray;
    public GameObject[] subLogoObjectArray;

    // Quest 관련
    QuestProgressInfo m_QuestProgressInfo = null;
    QuestClientInfo m_QuestClientInfo = null;
    QuestClientData m_QuestClientData = null;
    QuestData m_QuestData = null;
    QuestManager.QuestTipState m_QuestTipState = QuestManager.QuestTipState.None;
    StringBuilder m_QuestStateSB = new StringBuilder();
    public GameObject questTestButton;

    // Arrest Gage 관련
    public UILabel arrestGageLabel;
    public UISprite arrestGageSprite;
    public GameObject[] arrestGageIconObject_POLICE;
    public GameObject[] arrestGageIconObject_SWAT;
    public GameObject[] arrestGageIconObject_HUMMER;
    public GameObject[] arrestGageIconObject_TANK;

    Dictionary<string, ArrestGageIcon> m_ArrestGageIconDic = new Dictionary<string, ArrestGageIcon>();
    Coroutine m_UpdateArrestGageCoroutine = null;

    int m_QuestIndex = -1;
    string m_QuestContent = "{0}";

    VehicleData m_VehicleData = null;
    PetData m_PetData = null;

    UserManager m_UserMgr;
    VehicleDocs m_VehicleDocs;
    PetDocs m_PetDocs;
    QuestManager m_QuestMgr;
    TimeManager m_TimeMgr;

    private void Start()
    {
        System.GC.Collect();
        Resources.UnloadUnusedAssets();

        CommonAdManager.OnHideVideoAd += OnGameStartWithAD;

        if (!UserManager.instance.isPrepare)
        {
            SceneFadeInOut.LoadLevel((int)GameConfig.Scene.IntroScene);
        }
        else
        {
            Debug.Log("@@ Game 0 Screen Resolution : " + Screen.width + ", " + Screen.height);

            SoundManager.instance.StopBGM();

            IGAWorksManager.instance.FirstTimeExperience("Enter_GameScene");

            // 100분이상 플레이한 유저
            if (GameApplication.instance.playTime >= 6000)
            {
                IGAWorksManager.instance.SetTargetingData("PlayTime", 100);
            }            

            m_UserMgr = UserManager.instance;
            m_VehicleDocs = VehicleDocs.instance;
            m_PetDocs = PetDocs.instance;
#if BWQUEST
            m_QuestMgr = QuestManager.instance;
            m_TimeMgr = TimeManager.instance;
#endif

#if BWQUEST && BWTEST
            NGUITools.SetActive(questTestButton, true);            
#endif

            PopupManager.instance.behindScrollPopupPanel = behindScrollPopupPanel;
            PopupManager.instance.popupPanel = popupPanel;
            PopupManager.instance.frontScrollPopupPanel = frontScrollPopupPanel;
            PopupManager.instance.commonPopupPanel = commonPopupPanel;

            SoundManager soundMgr = SoundManager.instance;
            soundMgr.AllStopLoopSFX();

            NGUITools.SetActive(restoreBlockUI, false);

#if SHOW_FPS
            NGUITools.SetActive(fpsCounterObject, true);
#endif

#if BWTEST
            NGUITools.SetActive(m_SpeedLabel.gameObject, true);
            NGUITools.SetActive(m_ZerobackLabel.gameObject, true);
            NGUITools.SetActive(m_GearLabel.gameObject, true);
            NGUITools.SetActive(m_RPMLabel.gameObject, true);
            NGUITools.SetActive(evilPointLabel.gameObject, true);
#endif

#if UNITY_IOS
			NGUITools.SetActive(couponButtonObject, false);
#endif

#if BWQUEST
            QuestManager.OnQuestTip += OnQuestTip;
            //QuestManager.OnQuestRefresh += OnQuestRefresh;
            QuestManager.OnQuestSuccess += OnQuestSuccess;
            QuestManager.OnQuestFail += OnQuestFail;
#endif
            startButtonLabel.text = TextDocs.content("BT_START");
            adStartButtonLabel.text = TextDocs.content("BT_ADSTART");
            adStartDescLabel.text = TextDocs.content("UI_ADSTART_DESC");

            scoreDescLabel.text = TextDocs.content("UI_SCORE_TEXT");
            timeDescLabel.text = TextDocs.content("UI_TIME_TEXT");

            gameOverBurstedLabel.text = TextDocs.content("UI_BUSTED_TEXT");
            gameOverWastedLabel.text = TextDocs.content("UI_WASTED_TEXT");

            surrenderLabel.text = TextDocs.content("UI_SURRENDER_BUTTON_TEXT");

            inputHelperLabel.text = TextDocs.content("UI_BACKWARD_HELP_TEXT");

            missionLabel.text = TextDocs.content("UI_MISSION_TEXT");
            missionSuccessLabel.text = TextDocs.content("UI_SUCCESS_TEXT");
            missionFailLabel.text = TextDocs.content("UI_MISSIONFAILED_TEXT");

#if BWQUEST
            questButtonLabel.text = TextDocs.content("BT_QUEST_TEXT");
            questSuccessLabel.text = TextDocs.content("UI_QUEST_SUCCESS");
            questFailLabel.text = TextDocs.content("UI_QUEST_FAIL");
#endif

            arrestGageLabel.text = TextDocs.content("LOAD_WARNING_TITLE");

            m_PackageDatas[0] = PackageDocs.instance.packageDataDic[10000];
            packageButtonLabels[0].text = TextDocs.content(m_PackageDatas[0].name);            
            m_PackageDatas[1] = PackageDocs.instance.packageDataDic[10001];
            packageButtonLabels[1].text = TextDocs.content(m_PackageDatas[1].name);
            m_PackageDatas[2] = PackageDocs.instance.packageDataDic[10002];
            packageButtonLabels[2].text = TextDocs.content(m_PackageDatas[2].name);
            m_PackageDatas[3] = PackageDocs.instance.packageDataDic[10003];
            packageButtonLabels[3].text = TextDocs.content(m_PackageDatas[3].name);

            RefreshPackageButtons();

            for (int i = 0; i < logoObjectArray.Length; i++)
            {
                NGUITools.SetActive(logoObjectArray[i], false);
            }
            for(int i = 0; i < subLogoObjectArray.Length; i++)
            {
                NGUITools.SetActive(subLogoObjectArray[i], false);
            }
            GameConfig.SupportLanguage selectLanguage = m_UserMgr.selectLanguage;
            if (selectLanguage == GameConfig.SupportLanguage.Japanese)
            {
                NGUITools.SetActive(logoObjectArray[1], true);
                NGUITools.SetActive(subLogoObjectArray[1], true);
            }
            else if (selectLanguage == GameConfig.SupportLanguage.Chinese_S)
            {
                NGUITools.SetActive(logoObjectArray[2], true);
            }
            else if (selectLanguage == GameConfig.SupportLanguage.Chinese_T)
            {
                NGUITools.SetActive(logoObjectArray[3], true);
            }
            else
            {
                NGUITools.SetActive(logoObjectArray[0], true);
                NGUITools.SetActive(subLogoObjectArray[0], true);
            }

            m_HideInputHelper = false;
            m_IsMenuShow = false;
            m_EnableMenu = false;
            m_GameOverTimeScale = 1.0f;
            m_Pause = false;

            //Debug.Log("#### GameManager PlayerPrefs.GetInt(ShadowOn) : " + PlayerPrefs.GetInt("ShadowOn"));
            if (PlayerPrefs.GetInt("ShadowOn") == 1)
            {
                //Debug.Log("#### GameManager ShadowOn");
                QualitySettings.SetQualityLevel(1);
            }
            else
            {
                //Debug.Log("#### GameManager ShadowOff");
                QualitySettings.SetQualityLevel(0);
                m_UserMgr.ChangeLowResolution();
            }

            // Create the delays so they only have to be made once.
            m_StartWait = new WaitForSeconds(m_StartDelay);
            m_EndWait = new WaitForSeconds(m_EndDelay);

            GroundManager.instance.InitializeObstacle();

            GroundManager.instance.InitializeIntroGround();
            MissionManager.instance.StartUpdate();
            PoliceLineManager.instance.StartUpdate();
            ItemManager.instance.StartUpdate();
            RegionManager.instance.StartUpdate();
            NPCManager.instance.StartUpdate();

            ChaserManager.instance.StartUpdate();

            if(m_UserMgr.isPrevSceneIntro)
            {
                m_UserMgr.isPrevSceneIntro = false;

                IGAWorksManager.instance.ShowPopup("start_notice");
                IGAWorksManager.instance.ShowPromotionAd("start_promo");
            }            

            SoundManager.instance.gameSFXVolumeControl = 1.0f;

            // Once the tanks have been created and the camera is using them as targets, start the game.
            StartCoroutine(GameLoop());
        }
    }

    void OnDestroy()
    {
        CommonAdManager.OnHideVideoAd -= OnGameStartWithAD;

#if BWQUEST
        QuestManager.OnQuestTip -= OnQuestTip;
        //QuestManager.OnQuestRefresh -= OnQuestRefresh;
        QuestManager.OnQuestSuccess -= OnQuestSuccess;
        QuestManager.OnQuestFail -= OnQuestFail;
#endif
    }

    public void RestoreGameScene(GameConfig.Scene changeScene)
    {
        NGUITools.SetActive(resultUIPanel, true);
        NGUITools.SetActive(restoreBlockUI, true);

        RegionManager.instance.Restore();
        ChaserManager.instance.Restore();
        NPCManager.instance.Restore();
        TireMarkManager.instance.Restore();
        ShellManager.instance.Restore();
        EffectManager.instance.Restore();
        GroundManager.instance.RestoreIntroGround();

        SceneFadeInOut.LoadLevel((int)changeScene);
    }

    //---------- Start UI Implementation ----------//
    void MenuShow()
    {
        NGUITools.SetActive(menuObject, true);

        m_IsMenuShow = true;
        m_EnableMenu = true;
    }

    void MenuHide()
    {
        NGUITools.SetActive(menuObject, false);

        m_IsMenuShow = false;
        m_EnableMenu = false;
    }

    public void RefreshCash()
    {
        cashLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));
        //if (m_GetCashCoroutine != null)
        //{
        //    StopCoroutine(m_GetCashCoroutine);
        //    m_GetCashCoroutine = null;
        //    cashLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_DestCash));
        //}

        //m_DestCash = m_UserMgr.cash;
        //float increaseAmount = m_DestCash - m_PrevCash;

        //float increaseRange = increaseAmount / 10.0f;
        //float increaseFactor = Mathf.Clamp(cashAnimationCurve.Evaluate(increaseRange), 0.0f, 1.0f);

        //m_GetCashCoroutine = StartCoroutine(GetCash(increaseFactor));
    }

    //IEnumerator GetCash(float increaseFactor)
    //{
    //    while (true)
    //    {
    //        float cash = Mathf.Lerp(m_PrevCash, m_DestCash, Time.deltaTime * 10.0f * increaseFactor);
    //        if (m_DestCash - cash <= 0.5f)
    //        {
    //            cash = m_DestCash;
    //            m_PrevCash = cash;
    //            //Debug.Log(string.Format("! 캐쉬 {0}->{1} 증가 끝...현재 : {2}", m_PrevCash, m_DestCash, Mathf.FloorToInt(cash)));
    //            cashLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", Mathf.FloorToInt(cash)));
    //            m_GetCashCoroutine = null;
    //            break;
    //        }
    //        else
    //        {
    //            //Debug.Log(string.Format("캐쉬 {0}->{1} 증가 중...현재 : {2}", m_PrevCash, m_DestCash, Mathf.FloorToInt(cash)));
    //            m_PrevCash = cash;
    //            cashLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", Mathf.FloorToInt(cash)));
    //        }

    //        yield return null;
    //    }
    //}

    public void IncreaseCash()
    {
        m_PrevCash += 1;

        LeanTween.scale(cashLabel.gameObject, new UnityEngine.Vector3(1.4f, 1.4f, 1.0f), 0.02f);

        Debug.Log("IncreaseCash : " + m_PrevCash);

        if (m_PrevCash >= m_ExceedCash)
        {
            m_PrevCash = m_ExceedCash;
            LeanTween.scale(cashLabel.gameObject, new UnityEngine.Vector3(1.3f, 1.3f, 1.0f), 0.02f).setDelay(0.02f).setOnComplete(CompleteIncreaseScore);
        }
        else
        {
            LeanTween.scale(cashLabel.gameObject, new UnityEngine.Vector3(1.3f, 1.3f, 1.0f), 0.02f).setDelay(0.02f);
        }
        
        cashLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_PrevCash));
    }

    void CompleteIncreaseScore()
    {
        cashLabel.gameObject.transform.localScale = new UnityEngine.Vector3(1.0f, 1.0f, 1.0f);

        // 캐쉬 획득 연출이 끝났을 때 연출로 싱크가 안맞을 경우를 대비해 실제 캐쉬로 동기화 해준다.
        // 1. 연출을 위해 생성한 최대 획득 캐쉬보다 높은 점수를 획득한 경우 예외처리
        // 2. 획득 캐쉬 연출로 최종 캐쉬가 안맞을 경우 맞춰 준다.
        cashLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));
    }

    public void OnClickTestView()
    {
        SoundManager.PlayButtonClickSound();

        if(m_CameraControl.minCameraZoomDistance == 16.0f)
        {
            m_CameraControl.minCameraZoomDistance = 100.0f;
        }
        else if (m_CameraControl.minCameraZoomDistance == 100.0f)
        {
            m_CameraControl.minCameraZoomDistance = 16.0f;
        }
    }

    void Pause()
    {
        //ChaserManager.instance.RemoveAllChaserCar();

        m_Pause = true;

        m_PrevSFXVolume = SoundManager.instance.GetSFXVolume();
        SoundManager.instance.SetSFXVolume(0.0f);
        SoundManager.instance.PauseBGM();
        Time.timeScale = 0;
        NGUITools.SetActive(pausePopupObject, true);

        m_StartPlayTime = m_PlayTime;
    }

    void UnPause()
    {
        m_Pause = false;

        SoundManager.instance.SetSFXVolume(m_PrevSFXVolume);
        SoundManager.instance.UnPauseBGM();
        Time.timeScale = 1;
        NGUITools.SetActive(pausePopupObject, false);

        m_StartPlayTimeTicks = System.DateTime.Now.Ticks;
    }

    public void OnClickShowPausePopup()
    {
        SoundManager.PlayButtonClickSound();

        Pause();
    }

    public void OnClickHidePausePopup()
    {
        SoundManager.PlayButtonClickSound();

        UnPause();
    }

    public void OnClickSurrender()
    {
        SoundManager.PlayButtonClickSound();

        UnPause();

        m_UserMgr.isDirectGamePlay = false;
        m_UserMgr.isDirectGamePlayWithAD = false;
        RestoreGameScene(GameConfig.Scene.GameScene);
    }

    public void OnClickBackward()
    {
        m_TestMobileInput.StartBackward();
    }

    void RefreshPackageButtons()
    {
        PackageData packageData = null;
        for(int i = 0; i < m_PackageDatas.Length; i++)
        {
            packageData = m_PackageDatas[i];
            // 해당 패키지를 구매한 이력이 있으면 패키지 버튼을 비활성화 한다.
            if (m_UserMgr.packagePurchaseDic.ContainsKey(packageData.index))
            {
                NGUITools.SetActive(packageButtonObjects[i], false);
            }
            else
            {
                bool isAllVehiclePackage = false;
                PackageStockData packageStockData = null;
                for (int j = 0; j < packageData.stockList.Count; j++)
                {
                    packageStockData = packageData.stockList[j];
                    if (packageStockData.type == GameConfig.RewardType.AllVehicle)
                    {
                        isAllVehiclePackage = true;
                        break;
                    }
                }
                if(!isAllVehiclePackage)
                {
                    for (int j = 0; j < packageData.stockList.Count; j++)
                    {
                        packageStockData = packageData.stockList[j];
                        if (packageStockData.type == GameConfig.RewardType.Vehicle)
                        {
                            if (m_UserMgr.vehicleInfoDic.ContainsKey(packageStockData.value))
                            {
                                // 패키지에 있는 해당 차량을 보유시 패키지 버튼 비활성화 한다.
                                if (m_UserMgr.vehicleInfoDic[packageStockData.value].state != VehicleInfo.State.NotHave)
                                {
                                    NGUITools.SetActive(packageButtonObjects[i], false);
                                    break;
                                }
                            }
                        }
                        else if (packageStockData.type == GameConfig.RewardType.Pet)
                        {
                            if (m_UserMgr.petInfoDic.ContainsKey(packageStockData.value))
                            {
                                // 패키지에 있는 해당 펫을 보유시 패키지 버튼 비활성화 한다.
                                if (m_UserMgr.petInfoDic[packageStockData.value].state != PetInfo.State.NotHave)
                                {
                                    NGUITools.SetActive(packageButtonObjects[i], false);
                                    break;
                                }
                            }
                        }
                    }
                }                
            }
        }

        packageButtonTable.repositionNow = true;        
    }

    public void RefreshPlayer()
    {
        Destroy(m_VehiclesObjectTest);
        SpawnPlayer();
        RegionManager.instance.SetPlayer(m_VehiclesObjectTest.transform);
    }

    public void PackagePurchaseComplete(PackageData packagaData, bool refreshPlayerVehicle)
    {
        Debug.Log("@@ PackagePurchaseComplete 0");
        RefreshPackageButtons();

        Debug.Log("@@ PackagePurchaseComplete 1");

        if(refreshPlayerVehicle)
        {
            RefreshPlayer();
        }        

        Debug.Log("@@ PackagePurchaseComplete 2");
    }

    public void OnClickPackage0()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowPackage0Popup(m_PackageDatas[0], true);
    }

    public void OnClickPackage1()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowPackage1Popup(m_PackageDatas[1], true);
    }

    public void ShowPetPackage()
    {
        PopupManager.instance.ShowPackage2Popup(m_PackageDatas[2], false);
    }

    public void OnClickPackage2()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowPackage2Popup(m_PackageDatas[2], true);
    }

    public void OnClickPackage3()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowPackage3Popup(m_PackageDatas[3], true);
    }

    public void OnClickMoveFacebookPage()
    {
        SoundManager.PlayButtonClickSound();

        Application.OpenURL("https://www.facebook.com/Burnoutmobile");        
    }

    void GameStart()
    {
        // 구글 전면 광고 미리 로딩
        CommonAdManager.instance.PrepareLoadInterstitialAd();

        NGUITools.SetActive(waitingUIRootObject, false);

        m_State = State.Starting;

        m_IsInputStart = true;

        m_IntroBuilding.animator.Play("Intro Animation");
    }

    void GameStartWithAD()
    {
        m_ExceptionPause = true;
        NGUITools.SetActive(waitingUIRootObject, false);

        m_WaitGameStartWithAD = true;

        CommonAdManager.instance.Show(CommonAdManager.AdType.Video);        
    }

    public void OnClickGameStart()
    {
        SoundManager.PlayButtonClickSound();

        GameStart();
    }

    void OnGameStartWithAD(bool isSuccess)
    {
        if (m_WaitGameStartWithAD)
        {
            Debug.Log("GameManager OnGameStartWithAD : " + isSuccess);

            m_WaitGameStartWithAD = false;

            if (isSuccess)
            {
                GameParameter gameParam = m_VehiclesObjectTest.GetComponent<GameParameter>();
                gameParam.maxHealth *= 2;
                gameParam.currentHealth = gameParam.maxHealth;
            }

            GameStart();

            CommonAdManager.instance.Fetch(CommonAdManager.AdType.Video);
            m_ExceptionPause = false;
        }        
    }

    public void OnClickGameStartWithAD()
    {
        SoundManager.PlayButtonClickSound();

        GameStartWithAD();
    }

    public void OnClickShop()
    {
        SoundManager.PlayButtonClickSound();

        //RestoreGameScene(GameConfig.Scene.GachaScene);
        RestoreGameScene(GameConfig.Scene.ShopScene);
        //Application.OpenURL("http://unity3d.com/");
    }

    public void OnClickToggleMenu()
    {
        SoundManager.PlayButtonClickSound();

        if (m_IsMenuShow)
        {
            MenuHide();
        }
        else
        {
            MenuShow();
        }
    }

    public void OnClickCoupon()
    {
#if !UNITY_IOS
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            PopupManager.instance.ShowCouponPopup();
        }
#endif
    }

    public void OnClickEveryPlay()
    {
        if(m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            PopupManager.instance.ShowEveryPlayPopup();
        }
    }
		
#if UNITY_IOS
	void OnGameCenterAuthComplete(bool isSuccess)
	{
		PopupManager.instance.HideLoadingPopup ();
		UserManager.OnGameCenterAuthComplete -= OnGameCenterAuthComplete;
		if (isSuccess)
		{
			GameCenterManager.ShowLeaderboard("Veteran_racer");
		}
	}
#endif

    public void OnClickLeaderBoard()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            //StartCoroutine (m_UserMgr.AESTest());
#if UNITY_ANDROID
            if (TimeManager.instance.GetTimeState())
            {
                long time = TimeManager.instance.GetOnlineTime();
                long elapsedTimeL = m_UserMgr.aosLeaderBoardTime - time;
                if (elapsedTimeL <= 0 || m_UserMgr.aosLeaderBoardCount < 50)
                {
                    Debug.Log("@@ ShowLeaderBoardCount Menu : " + m_UserMgr.aosLeaderBoardCount);
                    if (elapsedTimeL <= 0)
                    {
                        m_UserMgr.aosLeaderBoardCount = 0;
                        m_UserMgr.aosLeaderBoardTime = time + (24 * 60 * 60 * 1000);
                    }
                    m_UserMgr.aosLeaderBoardCount++;
                    m_UserMgr.SaveUserData();
                    GooglePlayManager.Instance.ShowLeaderBoardById("CgkImbbs5L8bEAIQBQ");
                }                    
            }            
            //GooglePlayManager.Instance.ShowLeaderBoardsUI();
#elif UNITY_IOS
			if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
			{
				//Debug.Log("@@ GameCenter Init 1 : " + GameCenterManager.IsPlayerAuthenticated);
				if(GameCenterManager.IsPlayerAuthenticated == false)
				{
					PopupManager.instance.ShowLoadingPopup();
					ObscuredPrefs.SetInt("TryFirstGPLogin", 1);
					//Debug.Log("@@ GameCenter Init 3");
					UserManager.OnGameCenterAuthComplete += OnGameCenterAuthComplete;
					UserManager.instance.GameCenterInit(false);
				}
				else
				{
					GameCenterManager.ShowLeaderboard("Veteran_racer");
				}
			}
#endif
        }
    }

    public void OnClickAchievement()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

			//StartCoroutine (m_UserMgr.AESTestSS());
			//CommonAdManager.instance.ShowMediationTestSuite();
            PopupManager.instance.ShowAchievementPopup();
        }
    }

    public void OnClickSetup()
    {
        if (m_EnableMenu)
        {
            SoundManager.PlayButtonClickSound();

            PopupManager.instance.ShowSetupPopup();
        }
    }

    public void OnClickQuest()
    {
        SoundManager.PlayButtonClickSound();
#if BWQUEST
        PopupManager.instance.ShowQuestPopup();
#endif
    }

    public void OnClickQuestTest()
    {
        SoundManager.PlayButtonClickSound();

#if BWQUEST && BWTEST
        QuestManager.instance.TestQuestComplete();
#endif
    }
    //---------- End UI Implementation ----------//

    public void HideInputHelperObject()
    {
        if(m_HideInputHelper == false)
        {
            inputHelperObject.SetActive(false);
            m_HideInputHelper = true;

            if(m_TestMobileInput != null)
            {
                m_TestMobileInput.CreateHpGage();
            }            
        }
    }

    // 팁 시작
    public void StartShowTip()
    {        
        NGUITools.SetActive(tipRootObject, true);
        tipLabel.text = TextDocs.content(TipDocs.instance.GetTip());
        tipAnimator.Play("Start_Tips_Show");
    }

    public void StartHideTip()
    {
        tipAnimator.Play("Start_Tips_Hide");
    }
    
    public void RemoveIntroDecoration()
    {
        Destroy(introDecoration.gameObject);        
    }

    public void PlayRollingWheel()
    {
        introEngineSnd.clip = m_TestMobileInput.engineAudioSource.clip;

        m_TestMobileInput.ChangeState(TestMobileInput.State.RollingWheel);
    }

    public void PlayEngineSound()
    {
        introEngineSnd.enabled = true;
        //introEngineSnd.pitch = 0.5f;
        introEngineSnd.volume = SoundManager.instance.GetSFXVolume() * SoundManager.instance.gameSFXVolumeControl;

        m_TestMobileInput.ChangeState(TestMobileInput.State.StartEngineSound);
    }    

    public void PrepareGame()
    {
        m_IntroBuilding.colliderObject.SetActive(true);
        m_VehiclesObjectTest.transform.parent = null;

        m_TestMobileInput.ChangeState(TestMobileInput.State.Preparing);
    }

    public void PlayGame()
    {
        // 팁 비활성화
        NGUITools.SetActive(tipRootObject, false);

        StartCoroutine(StopIntroEngineSound());

        SoundManager.instance.PlayBGM("GAME_BGM");

        m_StartTime = System.DateTime.Now;

        NGUITools.SetActive(playingUIRootObject, true);

        if (m_TimeMgr.GetTimeState())
        {

        }
        m_QuestProgressInfo = m_QuestMgr.questProgressInfo;
        // 퀘스트 진행 상황을 표시해준다.
        if (m_QuestProgressInfo.isProgress && m_TimeMgr.GetTimeState())
        {            
            m_QuestClientInfo = m_QuestProgressInfo.questClientInfo;
            if (m_QuestClientInfo.state == QuestClientInfo.State.Select)
            {
                m_QuestClientData = m_QuestClientInfo.questClientData;
                m_QuestData = QuestManager.instance.curQuestData;

                m_QuestIndex = m_QuestProgressInfo.curQuestIndex;
                m_QuestContent = "{0}";
                if (m_QuestIndex == 0)
                {
                    m_QuestContent = m_QuestClientData.questContent1;
                }
                else if (m_QuestIndex == 1)
                {
                    m_QuestContent = m_QuestClientData.questContent2;
                }

                m_QuestStateSB.Remove(0, m_QuestStateSB.Length);
                m_QuestStateSB.AppendFormat(TextDocs.content(m_QuestContent), m_QuestData.param);
                m_QuestStateSB.AppendLine();
                m_QuestStateSB.Append("[C3A647]");
                m_QuestStateSB.Append(StringUtil.ConvertStringElapseTime((m_QuestProgressInfo.endTime - m_TimeMgr.GetOnlineTime()) / 1000));
                m_QuestStateSB.AppendFormat(" ({0}/{1})", m_QuestProgressInfo.progressParam, m_QuestData.param);
                m_QuestStateSB.Append("[-]");
                questContentLabel.text = m_QuestStateSB.ToString();

                questAnimator.Play("Quest_Show");
            }
        }
        else
        {
            questAnimator.Play("Quest_Start");
        }

        m_IsInputPlaying = true;
        m_State = State.Playing;
        m_TestMobileInput.ChangeState(TestMobileInput.State.Playing);

        m_IntroBuilding.aniCamera.enabled = false;

        SetCameraTargets();

        //m_CameraControl.SetCamera(m_IntroBuilding.aniCamera);

        m_CameraControl.SetStartPositionAndSize();

        m_CameraControl.enableZoom = true;

        // 펫 생성
        bool experiencePet = false;
        int petIndex = m_UserMgr.selectPetIndex;
        if(m_UserMgr.enablePetExperience)
        {
            petIndex = 0;
            experiencePet = true;

            m_UserMgr.enablePetExperience = false;
            m_UserMgr.SaveUserData();
        }
        if (m_UserMgr.petInfoDic.ContainsKey(petIndex) || experiencePet)
        {
            GameObject petPrefab = m_PetPrefabArray[petIndex];
            if (m_PetDocs.petDataDic.ContainsKey(petIndex))
            {                
                m_PetData = m_PetDocs.petDataDic[petIndex];
                GameParameter gameParam = petPrefab.GetComponent<GameParameter>();
                if (gameParam != null)
                {
                    gameParam.maxHealth = m_PetData.health;
                    gameParam.damage = m_PetData.damage;
                    gameParam.currentHealth = gameParam.maxHealth;
                    //Debug.Log("##################### Set Player Vehicle! : " + m_UserMgr.selectVehicleIndex + ", " + gameParam.currentHealth + ", " + gameParam.maxHealth);
                }
                BattleParameter battleParam = petPrefab.GetComponent<BattleParameter>();
                if (battleParam != null)
                {
                    battleParam.attackDamage = m_PetData.attackDamage;
                    battleParam.attackDelay = m_PetData.attackDelay;
                    battleParam.rotorSpeed = m_PetData.rotorSpeed;
                    battleParam.shellSpeed = m_PetData.shellSpeed;
                    battleParam.explosionRadius = m_PetData.explosionRadius;
                }
            }

            m_PetObject = Instantiate(petPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            m_PetObject.transform.parent = null;
            m_PetObject.transform.localEulerAngles = Vector3.zero;
            //m_PetObject.tag = "Player";

            PetAI petAI = m_PetObject.GetComponent<PetAI>();
            petAI.Initialize();

            m_PetShellAttackAI = null;
            switch (m_PetData.type)
            {
                case PetData.Type.ShellAttack:
                    {
                        m_PetShellAttackAI = m_PetObject.GetComponent<PetShellAttackAI>();
                        if(m_PetShellAttackAI != null)
                        {
                            m_PetShellAttackAI.Initialize();
                            m_PetShellAttackAI.StartUpdate();
                        }                        
                    }
                    break;
            }            
        }

        if (m_UpdateArrestGageCoroutine == null)
        {
            m_UpdateArrestGageCoroutine = StartCoroutine(UpdateArrestGage());
        }        
    }

    void OnQuestTip(QuestManager.QuestTipState questTipState)
    {
        if (m_State == State.Waiting)
        {
            if(m_QuestTipState != questTipState)
            {
                m_QuestTipState = questTipState;
                switch(m_QuestTipState)
                {
                    case QuestManager.QuestTipState.None:
                        {
                            questTipAnimator.Play("Game_Quest_None");
                        }
                        break;
                    case QuestManager.QuestTipState.EnableReceiveQuest:
                        {
                            questTipAnimator.Play("Game_Quest_Enable");
                        }
                        break;
                    case QuestManager.QuestTipState.CompleteQuest:
                        {
                            questTipAnimator.Play("Game_Quest_Complete");
                        }
                        break;
                }
            }
        }
    }

    //void OnQuestRefresh()
    //{
    //    if (m_State == State.Playing)
    //    {
    //        if (m_QuestProgressInfo.isProgress)
    //        {
    //            if (m_QuestClientInfo.state == QuestClientInfo.State.Select)
    //            {
    //                m_QuestStateSB.Remove(0, m_QuestStateSB.Length);
    //                m_QuestStateSB.AppendFormat(TextDocs.content(m_QuestContent), m_QuestData.param);
    //                m_QuestStateSB.AppendLine();
    //                m_QuestStateSB.Append("[C3A647]");
    //                m_QuestStateSB.Append(StringUtil.ConvertStringElapseTime((m_QuestProgressInfo.endTime - m_TimeMgr.GetOnlineTime()) / 1000));
    //                m_QuestStateSB.AppendFormat(" {0}/{1}", m_QuestProgressInfo.progressParam, m_QuestData.param);
    //                m_QuestStateSB.Append("[-]");
    //                questContentLabel.text = m_QuestStateSB.ToString();
    //                //string temp = string.Format(TextDocs.content(m_QuestContent), m_QuestData.param) +
    //                //    "\\n" + StringUtil.ConvertStringElapseTime((m_QuestProgressInfo.endTime - m_TimeMgr.GetOnlineTime()) / 1000) + " " +
    //                //    string.Format("{0}/{1}", m_QuestProgressInfo.progressParam, m_QuestData.param);
    //                //temp = temp.Replace("\\n", "\n");
    //                //questContentLabel.text = temp;
    //            }
    //        }
    //    }
    //}

    void OnQuestSuccess()
    {
        if (m_State == State.Playing || m_State == State.GameOver || m_State == State.Result)
        {
            questAnimator.Play("Quest_Success");
        }
    }

    void OnQuestFail()
    {
        if (m_State == State.Playing || m_State == State.GameOver || m_State == State.Result)
        {
            questAnimator.Play("Quest_Failed");
        }
    }

    IEnumerator StopIntroEngineSound()
    {
        yield return new WaitForSeconds(2.0f);

        introEngineSnd.enabled = false;
    }

    public void IntroCinemaPlayDriving()
    {
        introDecoration.PlayDriving();
    }

    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PopupManager.instance.IsShowLoadingPopup() == false)
            {
                if (NGUITools.GetActive(pausePopupObject))
                {
                    UnPause();
                }
                else
                {
                    if (HeyzapAds.OnBackPressed())
                    {
                        return;
                    }
                    else
                    {
                        if (!PopupManager.instance.BackProcess())
                        {
                            if (!BackProcess())
                            {
                                if(m_State == State.Starting ||
                                   m_State == State.Playing ||
                                   m_State == State.GameOver ||
                                   m_State == State.Result)
                                {
                                    // 종료 팝업 띄우기
                                    PopupManager.instance.ShowExitPopup(true, 
                                        delegate()
                                        {
                                            m_Pause = true;
                                            m_StartPlayTime = m_PlayTime;
                                        },
                                        delegate ()
                                        {
                                            m_Pause = false;
                                            m_StartPlayTimeTicks = System.DateTime.Now.Ticks;
                                        });
                                }
                                else
                                {
                                    // 종료 팝업 띄우기
                                    PopupManager.instance.ShowExitPopup(false, null, null);
                                }                                
                            }
                            else
                            {
                                return;
                            }                                
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }
        }

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    m_UserMgr.cash += 1;
        //}
#endif

        if (m_VehicleParent)
        {
#if BWTEST
            int speed = Mathf.RoundToInt(m_VehicleParent.velMag * 2.23694f * 1.609344f);
            m_SpeedLabel.text = (m_VehicleParent.velMag * 2.23694f * 1.609344).ToString("0") + " km/h";

            if (speed >= 100 && m_IsCheckZeroback == false)
            {
                m_IsCheckZeroback = true;
                System.DateTime nowTime = System.DateTime.Now;
                System.TimeSpan elapsedSpan = new System.TimeSpan(nowTime.Ticks - m_StartTime.Ticks);                
                m_ZerobackLabel.text = string.Format("{0} sec", elapsedSpan.TotalSeconds);
            }

            if (trans)
            {
                if (gearbox)
                {
                    m_GearLabel.text = "Gear: " + (gearbox.currentGear == 0 ? "R" : (gearbox.currentGear == 1 ? "N" : (gearbox.currentGear - 1).ToString()));
                }
                else if (varTrans)
                {
                    m_GearLabel.text = "Ratio: " + varTrans.currentRatio.ToString("0.00");
                }
            }

            if (engine)
            {
                //rpmMeter.value = engine.targetPitch / (engine.maxPitch - engine.minPitch);
                //m_RPMLabel.text = engine.targetPitch + "RPM " + engine.maxPitch + ", " + engine.minPitch;
                //m_RPMLabel.text = ((int)(engine.targetPitch / (engine.maxPitch - engine.minPitch) * 10000.0f)) + " RPM";
                //m_RPMLabel.text = driveForce.rpm + " RPM";
                m_RPMLabel.text = driveForce.feedbackRPM + " RPM";
            }
#endif
        }

        //if(Time.frameCount % 30 == 0)
        //{
        //    System.GC.Collect();
        //}
    }

    bool BackProcess()
    {
        if (m_IsMenuShow)
        {
            MenuHide();
            return true;
        }
        else
        {
            //if(m_State == State.Result)
            //{
            //    m_UserMgr.isDirectGamePlay = false;
            //    m_UserMgr.isDirectGamePlayWithAD = false;
            //    RestoreGameScene(GameConfig.Scene.GameScene);

            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if(m_State != State.Waiting)
        {
            if (pauseStatus)
            {
                if (!m_ExceptionPause)
                {
                    Pause();
                }
            }
            else
            {
                if (m_ExceptionPause)
                {
                    m_ExceptionPause = false;
                }
            }
        }
    }

    public void StartMission(string content, int rewardAmount)
    {
        missionNoticeLabel.text = content;
        missionRewardLabel.text = string.Format("{0}", rewardAmount);

        missionAnimator.Play("GameMSG_Show");
    }

    public void ChangeMission()
    {
        missionProgressLabel.text = "";
        missionRemainTimeLabel.text = "";

        missionAnimator.Play("GameMSG_ChangeMSG");
    }

    public void ShowingMission()
    {
        missionNoticeLabel.text = "";
    }

    public void ProgressMission(string content, string remainTimeContent)
    {
        missionProgressLabel.text = content;
        missionRemainTimeLabel.text = remainTimeContent;
    }

    public void RotateArrow(float angle)
    {
        if (NGUITools.GetActive(arrowObject) == false)
        {
            NGUITools.SetActive(arrowObject, true);
        }
        //if (NGUITools.GetActive(distanceLabel.gameObject) == false)
        //{
        //    NGUITools.SetActive(distanceLabel.gameObject, true);
        //}
        Vector3 eulerAngleVelocity = new Vector3(50, 0, -45 - angle - RegionManager.instance.playerTransform.localEulerAngles.y);
        Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity);
        arrowTransform.localRotation = deltaRotation;

        //distanceLabel.text = string.Format("{0} m", distance);
    }

    public void EndMission(string content, bool complete, int rewardAmount)
    {
        NGUITools.SetActive(arrowObject.gameObject, false);
        
        if(complete)
        {
            m_PrevCash = m_UserMgr.cash;
            m_ExceedCash = m_UserMgr.cash;
            int getCash = 0;
            if (m_TestMobileInput.isCashCar)
            {
                getCash = (rewardAmount * 2);
            }
            else
            {
                getCash = rewardAmount;
            }
            if(getCash > GameConfig.MaxGetCoin)
            {
                m_ExceedCash += GameConfig.MaxGetCoin;
            }
            else
            {
                m_ExceedCash += getCash;
            }
            m_UserMgr.cash += getCash;
            m_GetCashEffectGroup.CreateGetCashEffect(getCash);

            missionResultAnimator.Play("Mission_Complete");
        }
        else
        {
            missionResultAnimator.Play("Mission_Failed");
        }
    }

    public void WaitMission()
    {
        missionAnimator.Play("GameMSG_Complete");
    }
    
    public void SpawnPlayer(IntroBuilding introBuilding)
    {
        m_IntroBuilding = introBuilding;

        SpawnPlayer();
    }

    void SpawnPlayer()
    {
        // 플레이어 차량 생성
        GameObject playerVehiclePrefab = m_PlayerVehiclePrefabArray[m_UserMgr.selectVehicleIndex];
        if (m_VehicleDocs.vehicleDataDic.ContainsKey(m_UserMgr.selectVehicleIndex))
        {
            m_VehicleData = m_VehicleDocs.vehicleDataDic[m_UserMgr.selectVehicleIndex];
            GameParameter gameParam = playerVehiclePrefab.GetComponent<GameParameter>();
            if (gameParam != null)
            {
                gameParam.maxHealth = m_VehicleData.health;
                gameParam.damage = m_VehicleData.damage;
                gameParam.currentHealth = gameParam.maxHealth;
                //Debug.Log("##################### Set Player Vehicle! : " + m_UserMgr.selectVehicleIndex + ", " + gameParam.currentHealth + ", " + gameParam.maxHealth);
            }
            BattleParameter battleParam = playerVehiclePrefab.GetComponent<BattleParameter>();
            if (battleParam != null)
            {
                battleParam.attackDamage = m_VehicleData.attackDamage;
                battleParam.attackDelay = m_VehicleData.attackDelay;
                battleParam.rotorSpeed = m_VehicleData.rotorSpeed;
                battleParam.shellSpeed = m_VehicleData.shellSpeed;
                battleParam.explosionRadius = m_VehicleData.explosionRadius;
            }
        }

        m_VehiclesObjectTest = Instantiate(playerVehiclePrefab, Vector3.zero, Quaternion.identity) as GameObject;
        m_VehiclesObjectTest.transform.parent = m_IntroBuilding.carSpawnPoint;
        m_VehiclesObjectTest.transform.localEulerAngles = Vector3.zero;
        m_VehiclesObjectTest.tag = "Player";

        m_TestMobileInput = m_VehiclesObjectTest.GetComponent<TestMobileInput>();
        m_VehiclesObjectTest.transform.localPosition = m_TestMobileInput.startPosition;

        m_VehicleParent = m_VehiclesObjectTest.GetComponent<VehicleParent>();

        m_CameraControl.testMobileInput = m_TestMobileInput;
    }


    private void SetCameraTargets()
    {
        // Create a collection of transforms the same size as the number of tanks.
        Transform[] targets = new Transform[1];

        // For each of these transforms...
        for (int i = 0; i < targets.Length; i++)
        {
            // ... set it to the appropriate tank transform.
            targets[i] = m_VehiclesObjectTest.transform;
        }

        // These are the targets the camera should follow.
        m_CameraControl.m_Targets = targets;
    }


    // This is called from start and will run each phase of the game one after another.
    private IEnumerator GameLoop()
    {
        if(!m_UserMgr.isDirectGamePlay)
        {
            yield return StartCoroutine(RoundWaiting());
        }
        else
        {
            if(!m_UserMgr.isDirectGamePlayWithAD)
            {
                GameStart();
            }
            else
            {
                GameStartWithAD();
            }
        }        

        // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundStarting());

        // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundPlaying());

        // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
        yield return StartCoroutine(RoundEnding());

        //// This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found.
        //if (m_GameWinner != null)
        //{
        //    // If there is a game winner, restart the level.
        //    Application.LoadLevel(Application.loadedLevel);
        //}
        //else
        //{
            // If there isn't a winner yet, restart this coroutine so the loop continues.
            // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end.
            //StartCoroutine(GameLoop());
        //}
    }

//    public void RefreshQuestTip()
//    {
//#if BWQUEST
//        bool isQuestComplete = false;
//        bool isQuestReceiveEnable = false;
//        if (m_QuestMgr.questProgressInfo.isProgress)
//        {
//            if (m_QuestMgr.questProgressInfo.progressParam >= m_QuestMgr.curQuestData.param)
//            {
//                isQuestComplete = true;
//            }
//        }
//        if (m_TimeMgr.GetTimeState())
//        {
//            long time = m_TimeMgr.GetOnlineTime();
//            long elapsedTimeL = m_QuestMgr.questProgressInfo.endReceiveTime - time;
//            if (elapsedTimeL <= 0)
//            {
//                isQuestReceiveEnable = true;
//            }
//        }

//        if (isQuestComplete)
//        {
//            Debug.Log("!! Game_Quest_Complete");
//            questTipAnimator.Play("Game_Quest_Complete");
//        }
//        else
//        {
//            if (isQuestReceiveEnable)
//            {
//                Debug.Log("!! Game_Quest_Enable");
//                questTipAnimator.Play("Game_Quest_Enable");
//            }
//            else
//            {
//                Debug.Log("!! Game_Quest_None");
//                questTipAnimator.Play("Game_Quest_None");
//            }
//        }
//#endif
//    }

    IEnumerator RoundWaiting()
    {
        NGUITools.SetActive(waitingUIRootObject, true);

        //RefreshQuestTip();

        IntroCinemaPlayDriving();

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        // 게임 시작시 전면 광고
        if (!m_UserMgr.isShowStartInterstitialAd)
        {
			Debug.Log("Start Fetch");
			CommonAdManager.instance.Fetch(CommonAdManager.AdType.Video);
			CommonAdManager.instance.Fetch(CommonAdManager.AdType.Interstitial);

            m_UserMgr.isShowStartInterstitialAd = true;
            yield return StartCoroutine(m_UserMgr.ShowInterstitialAd());
        }
#endif

        while (!m_IsInputStart)
        {
            yield return null;
        }
    }

    private IEnumerator RoundStarting()
    {
        while (!m_IsInputPlaying)
        {
            yield return null;
        }

        cashLabel.text = StringUtil.LetMoneyTYPE(string.Format("{0}", m_UserMgr.cash));

        m_PrevCash = m_UserMgr.cash;
        m_ExceedCash = m_UserMgr.cash;

        m_NowTime = System.DateTime.Now;
        m_StartPlayTime = 0;
        m_StartPlayTimeTicks = m_NowTime.Ticks;

        //// As soon as the round starts reset the tanks and make sure they can't move.
        //ResetAllTanks();
        ////DisableTankControl();

        //// Snap the camera's zoom and position to something appropriate for the reset tanks.
        //m_CameraControl.SetStartPositionAndSize();

        //// Increment the round number and display text showing the players what round it is.
        //m_RoundNumber++;

        //// Wait for the specified length of time until yielding control back to the game loop.
        //yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
        // As soon as the round begins playing let the players control the tanks.
        EnableTankControl();

        // While there is not one tank left...
        while (!OneTankLeft())
        {
            if(!m_Pause)
            {
                m_NowTime = System.DateTime.Now;
                m_ElapsedTimeL = m_NowTime.Ticks - m_StartPlayTimeTicks;
                TimeSpan elapsedSpan = new TimeSpan(m_ElapsedTimeL);
                m_ElapsedTime = (int)elapsedSpan.TotalSeconds;
                //long test = m_NowTime.Ticks - m_StartPlayTimeTicks;
                m_PlayTime = m_StartPlayTime + m_ElapsedTime;
                //sb.Length = 0;
                //sb.AppendFormat("{0}", m_PlayTime);
                timeLabel.text = string.Format("{0}", m_PlayTime);

                //sb.Length = 0;
                //sb.AppendFormat("{0}", m_PlayTime + m_Score);
                scoreLabel.text = string.Format("{0}", m_PlayTime + m_Score);

                if (m_QuestProgressInfo.isProgress && m_TimeMgr.GetTimeState())
                {
                    if (m_QuestClientInfo.state == QuestClientInfo.State.Select)
                    {
                        m_QuestStateSB.Remove(0, m_QuestStateSB.Length);
                        m_QuestStateSB.AppendFormat(TextDocs.content(m_QuestContent), m_QuestData.param);
                        m_QuestStateSB.AppendLine();
                        m_QuestStateSB.Append("[C3A647]");
                        m_QuestStateSB.Append(StringUtil.ConvertStringElapseTime((m_QuestProgressInfo.endTime - m_TimeMgr.GetOnlineTime()) / 1000));
                        m_QuestStateSB.AppendFormat(" ({0}/{1})", m_QuestProgressInfo.progressParam, m_QuestData.param);
                        m_QuestStateSB.Append("[-]");
                        questContentLabel.text = m_QuestStateSB.ToString();
                    }
                }

                CheckEvilCount();
            }           

            // ... return on the next frame.
            yield return null;
        }
    }

    void ChangeRemainEvilMark()
    {
        evilMarkAnimatorArray[m_EvilCount - 1].Play("CriminalStar_Filled");
        if(m_EvilCount < evilMarkAnimatorArray.Length)
        {
            evilMarkAnimatorArray[m_EvilCount].Play("CriminalStar_Filling");
            for (int i = m_EvilCount + 1; i < evilMarkAnimatorArray.Length; i++)
            {
                evilMarkAnimatorArray[i].Play("CriminalStar_Empty");
            }
        }        
    }

    void CheckEvilCount()
    {
        bool increaseEvilCount = false;
        if(m_EvilCount == 0)
        {
            increaseEvilCount = true;
            m_EvilCount = 1;
            ChangeRemainEvilMark();
        }
        else if(m_EvilCount != 6)
        {
            EvilCountSection evilCountSection = null;
            EvilCountDocs evilCountDocs = EvilCountDocs.instance;
            int checkEvilCount = m_EvilCount;
            for (int i = m_EvilCount; i <= 6; i++)
            {
                evilCountSection = evilCountDocs.evilCountSectionDic[i];
                if (m_EvilPoint + m_PlayTime >= evilCountSection.achievePoint)
                {
                    checkEvilCount = i;
                }
                else
                {
                    break;
                }
            }

            if (checkEvilCount != m_EvilCount)
            {
                for(int i = m_EvilCount; i < checkEvilCount; i++)
                {
                    UISprite curEvilMarkSprite = evilMarkProgressArray[i];
                    curEvilMarkSprite.fillAmount = 1.0f;

                    bool isExistNextEvilCount = i + 1 + 1 > 6 ? false : true;
                    if (isExistNextEvilCount)
                    {
                        UISprite nextEvilMarkSprite = evilMarkProgressArray[i + 1];

                        EvilCountSection curEvilCountSection = evilCountDocs.evilCountSectionDic[i + 1];
                        EvilCountSection nextEvilCountSection = evilCountDocs.evilCountSectionDic[i + 1 + 1];

                        float curPoint = m_EvilPoint + m_PlayTime - curEvilCountSection.achievePoint;
                        float destPoint = nextEvilCountSection.achievePoint - curEvilCountSection.achievePoint;
                        float pointRatio = curPoint / destPoint;
                        //Debug.Log("pointRatio : " + curPoint + ", " + destPoint + ", " + pointRatio);
                        nextEvilMarkSprite.fillAmount = Mathf.Lerp(nextEvilMarkSprite.fillAmount, pointRatio, Time.deltaTime);
                        //nextEvilMarkSprite.fillAmount = pointRatio;
                        //m_EvilMarkDic.Add(i + 1 + 1, nextEvilMarkSprite);
                    }

                    increaseEvilCount = true;
                    m_EvilCount = i + 1;

#if BWQUEST
                    QuestManager.instance.CheckQuestData(QuestData.Type.EvilCount);
#endif

                    ChangeRemainEvilMark();
                }                
            }
            else
            {
                UISprite preEvilMarkSprite = evilMarkProgressArray[m_EvilCount];

                EvilCountSection preEvilCountSection = evilCountDocs.evilCountSectionDic[m_EvilCount];
                EvilCountSection curEvilCountSection = evilCountDocs.evilCountSectionDic[m_EvilCount + 1];
                float curPoint = m_EvilPoint + m_PlayTime - preEvilCountSection.achievePoint;
                float destPoint = curEvilCountSection.achievePoint - preEvilCountSection.achievePoint;
                float pointRatio = curPoint / destPoint;
                //Debug.Log("pointRatio : " + curPoint + ", " + destPoint + ", " + pointRatio);
                preEvilMarkSprite.fillAmount = Mathf.Lerp(preEvilMarkSprite.fillAmount, pointRatio, Time.deltaTime);
                //preEvilMarkSprite.fillAmount = pointRatio;

                increaseEvilCount = false;
            }
        }

        if(increaseEvilCount)
        {
            SoundManager.instance.PlaySFX("EvilCount");
        }

#if BWTEST
        evilPointLabel.text = string.Format("{0} - {1}", m_EvilCount, m_EvilPoint + m_PlayTime);
#endif
    }

    public void ApplyDamage(CollisionConnector collisionConnector, GameObject otherObject, int sendDamage, bool isDestroyOnly)
    {
        if (collisionConnector != null)
        {
            PointData pointData;
            Dictionary<int, PointData> pointDataDic = PointDocs.instance.pointDataDic;
            GameParameter otherGameParameter = collisionConnector.gameParameter;
            if (otherGameParameter != null)
            {
                if (otherObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
                {
                    if (otherGameParameter.currentHealth > 0)
                    {
                        otherGameParameter.currentHealth -= sendDamage;

                        if (sendDamage > 0 &&
                            collisionConnector.pointType != PointData.Type.None)
                        {
                            pointData = pointDataDic[(int)collisionConnector.pointType];
                            if (otherGameParameter.currentHealth <= 0)
                            {
                                m_EvilPoint += pointData.destroyEvilPoint;
                                m_Score += pointData.destroyScore;

//#if BWQUEST
//                                ChaserAI chaserAI = otherObject.GetComponent<ChaserAI>();
//                                if(chaserAI != null)
//                                {
//                                    if(chaserAI.chaserType == ChaserData.Type.Gangster)
//                                    {
//                                        QuestManager.instance.CheckQuestData(QuestData.Type.DestroyGang);
//                                    }
//                                    else
//                                    {
//                                        QuestManager.instance.CheckQuestData(QuestData.Type.DestroyPolice);
//                                    }
//                                }
//#endif
                            }
                            else
                            {
                                if(!isDestroyOnly)
                                {
                                    m_EvilPoint += pointData.colEvilPoint;
                                    m_Score += pointData.colScore;
                                }                                
                            }
                        }
                    }
                }
                else
                {
                    if (collisionConnector.pointType != PointData.Type.None)
                    {
                        if (sendDamage > 0)
                        {
                            pointData = pointDataDic[(int)collisionConnector.pointType];
                            m_EvilPoint += pointData.colEvilPoint;
                            m_Score += pointData.colScore;
                        }
                    }
                }
            }
        }
    }

    private IEnumerator RoundEnding()
    {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
#elif UNITY_ANDROID
        if (GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED)
        {
            AchievementDocs.instance.CheckAchievement(AchievementData.Kind.SCORE, m_PlayTime + m_Score);
        }
#elif UNITY_IOS
		if(GameCenterManager.IsPlayerAuthenticated)
		{
			AchievementDocs.instance.CheckAchievement(AchievementData.Kind.SCORE, m_PlayTime + m_Score);
		}
#endif

        if (m_ArrestPlayer)
        {
#if BWQUEST
            QuestManager.instance.CheckQuestData(QuestData.Type.Arrest);
#endif
            SoundManager.instance.PlaySFX("Busted");
        }
        else if (m_DestroyPlayer)
        {
#if BWQUEST
            QuestManager.instance.CheckQuestData(QuestData.Type.Waste);
#endif
            SoundManager.instance.PlaySFX("Wasted");
        }

        SoundManager.instance.StopBGM();

        m_State = State.GameOver;

        if(m_UpdateArrestGageCoroutine != null)
        {
            StopCoroutine(m_UpdateArrestGageCoroutine);
            m_UpdateArrestGageCoroutine = null;
        }

        m_TestMobileInput.ChangeState(TestMobileInput.State.GameOver);

        MissionManager.instance.EndMission(false);

        // 공격용 팻이 있다면 Restore 해준다.
        if(m_PetShellAttackAI != null)
        {
            m_PetShellAttackAI.Restore();
        }

        colorCorrectionCurves.enabled = true;
        tiltShift.enabled = true;
        // 게임 오버 연출
        while(true)
        {
            m_GameOverTimeScale = Mathf.Lerp(m_GameOverTimeScale, 0.4f, Mathf.Clamp01(Time.deltaTime * 16.0f));
            m_BlurArea = Mathf.Lerp(m_BlurArea, 0.3f, Mathf.Clamp01(Time.deltaTime * 16.0f));

            //Debug.Log("게임 오버 연출! : " + m_GameOverTimeScale);
            if(Mathf.Abs(m_GameOverTimeScale - 0.4f) < 0.01f)
            {
                Time.timeScale = 0.4f;
                tiltShift.Size = 0.3f;
                //tiltShift.blurArea = 15.0f;
                break;
            }
            else
            {
                Time.timeScale = m_GameOverTimeScale;
                colorCorrectionCurves.saturation = m_ColorSaturation;
                tiltShift.Size = m_BlurArea;
                //tiltShift.blurArea = m_BlurArea;
            }            

            yield return new WaitForEndOfFrame();
        }

        while (true)
        {
            m_ColorSaturation = Mathf.Lerp(m_ColorSaturation, 0.0f, Mathf.Clamp01(Time.deltaTime * 34.0f));

            //Debug.Log("게임 오버 색깔 연출! : " + m_ColorSaturation);
            if (m_ColorSaturation < 0.13f)
            {
                colorCorrectionCurves.saturation = 0.0f;
                break;
            }
            else
            {
                colorCorrectionCurves.saturation = m_ColorSaturation;
            }

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.1f);

        Time.timeScale = 1.0f;

        NGUITools.SetActive(finishObject, true);

        if (m_ArrestPlayer)
        {
            //SoundManager.instance.PlaySFX("Busted");
            finishAnimator.Play("GameOver_Busted");
        }
        else if(m_DestroyPlayer)
        {
            //SoundManager.instance.PlaySFX("Wasted");
            finishAnimator.Play("GameOver_Wasted");
        }

        // Stop tanks from moving.
        DisableTankControl();

        //// Clear the winner from the previous round.
        //m_RoundWinner = null;

        //// See if there is a winner now the round is over.
        //m_RoundWinner = GetRoundWinner();

        //// If there is a winner, increment their score.
        //if (m_RoundWinner != null)
        //    m_RoundWinner.m_Wins++;

        //// Now the winner's score has been incremented, see if someone has one the game.
        //m_GameWinner = GetGameWinner();

        // Get a message based on the scores and whether or not there is a game winner and display it.
        //string message = EndMessage();

        //camHelicopter.SetActive(true);

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_EndWait;

        int prevPlayGiftCount = m_UserMgr.playGiftCount;
        m_UserMgr.playGiftCount += m_PlayTime;
        //m_UserMgr.interstitialAdCount++;

        //Debug.Log("@#$ GameOver ShowInterstitialAd : " + m_UserMgr.interstitialAdCount + ", " + EtcDocs.instance.interstitialAdCount);
        //if (m_UserMgr.interstitialAdCount >= EtcDocs.instance.interstitialAdCount)
        //{
        //    m_ExceptionPause = true;
        //    m_UserMgr.interstitialAdCount = 0;
        //    CommonAdManager.instance.Show(CommonAdManager.AdType.Interstitial);
        //}

        m_UserMgr.SaveUserData();

#if UNITY_ANDROID
        GooglePlayManager.Instance.SubmitScoreById("CgkImbbs5L8bEAIQBQ", m_PlayTime + m_Score);        
        //GooglePlayManager.Instance.SubmitScoreById("CgkIsOGew4oJEAIQBw", m_PlayTime + m_Score);
#elif UNITY_IOS
		GameCenterManager.ReportScore((long)(m_PlayTime + m_Score), "Veteran_racer", 0);
#endif

        colorCorrectionCurves.enabled = false;
        tiltShift.enabled = false;

#if UNITY_EDITOR_WIN || UNITY_EDITOR
#else
        // 게임 오버시 전면 광고
        m_ExceptionPause = true;
        yield return StartCoroutine(m_UserMgr.ShowInterstitialAd());
#endif

        SoundManager.instance.gameSFXVolumeControl = 0.5f;
        NGUITools.SetActive(resultUIPanel, true);
        EveryPlayManager.instance.recordingCarName = TextDocs.content(m_VehicleData.name);
        EveryPlayManager.instance.recordingScore = m_PlayTime + m_Score;
        resultUI.SetResult(m_ArrestPlayer, m_PlayTime, m_PlayTime + m_Score, prevPlayGiftCount);

        yield return new WaitForSeconds(0.1f);

        camHelicopter.SetActive(true);

        m_State = State.Result;
    }

    // This is used to check if there is one or fewer tanks remaining and thus the round should end.
    private bool OneTankLeft()
    {
        //// Start the count of tanks left at zero.
        //int numTanksLeft = 0;

        //// Go through all the tanks...
        //for (int i = 0; i < m_Tanks.Length; i++)
        //{
        //    // ... and if they are active, increment the counter.
        //    if (m_Tanks[i].m_Instance.activeSelf)
        //        numTanksLeft++;
        //}

        //// If there are one or fewer tanks remaining return true, otherwise return false.
        //return numTanksLeft <= 1;
        //return false;

        return m_DestroyPlayer || m_ArrestPlayer;
    }


    //// This function is to find out if there is a winner of the round.
    //// This function is called with the assumption that 1 or fewer tanks are currently active.
    //private TankManager GetRoundWinner()
    //{
    //    //// Go through all the tanks...
    //    //for (int i = 0; i < m_Tanks.Length; i++)
    //    //{
    //    //    // ... and if one of them is active, it is the winner so return it.
    //    //    if (m_Tanks[i].m_Instance.activeSelf)
    //    //        return m_Tanks[i];
    //    //}

    //    // If none of the tanks are active it is a draw so return null.
    //    return null;
    //}


    //// This function is to find out if there is a winner of the game.
    //private TankManager GetGameWinner()
    //{
    //    //// Go through all the tanks...
    //    //for (int i = 0; i < m_Tanks.Length; i++)
    //    //{
    //    //    // ... and if one of them has enough rounds to win the game, return it.
    //    //    if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
    //    //        return m_Tanks[i];
    //    //}

    //    // If no tanks have enough rounds to win, return null.
    //    return null;
    //}


    //// Returns a string message to display at the end of each round.
    //private string EndMessage()
    //{
    //    //// By default when a round ends there are no winners so the default end message is a draw.
    //    //string message = "DRAW!";

    //    //// If there is a winner then change the message to reflect that.
    //    //if (m_RoundWinner != null)
    //    //    message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

    //    //// Add some line breaks after the initial message.
    //    //message += "\n\n\n\n";

    //    ////// Go through all the tanks and add each of their scores to the message.
    //    ////for (int i = 0; i < m_Tanks.Length; i++)
    //    ////{
    //    ////    message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
    //    ////}

    //    //// If there is a game winner, change the entire message to reflect that.
    //    //if (m_GameWinner != null)
    //    //    message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

    //    return message;
    //}


    // This function is used to turn all the tanks back on and reset their positions and properties.
    private void ResetAllTanks()
    {
        //for (int i = 0; i < m_Tanks.Length; i++)
        //{
        //    m_Tanks[i].Reset();
        //}
    }


    private void EnableTankControl()
    {
        //for (int i = 0; i < m_Tanks.Length; i++)
        //{
        //    m_Tanks[i].EnableControl();
        //}
    }

    private void DisableTankControl()
    {
        m_TestMobileInput.DisableControl();
    }

    public TestMobileInput GetTestMobileInput()
    {
        return m_TestMobileInput;
    }

    public void EarnGift()
    {
        giftUI.EarnGift();
    }

    public void ShowGift(GameConfig.GiftType type)
    {
        NGUITools.SetActive(giftUIPanel, true);
        StartCoroutine(giftUI.Show(type));
    }

    public void HideGift()
    {
        StartCoroutine(giftUI.Hide());
    }

    // Arrest Gage 관련
    public GameObject GetArrestGageIcon(ChaserData.Type chaserType)
    {
        GameObject arrestGageIcon = null;
        switch (chaserType)
        {
            case ChaserData.Type.Police:
                {
                    for (int i = 0; i < arrestGageIconObject_POLICE.Length; i++)
                    {
                        if (arrestGageIconObject_POLICE[i].activeSelf == false)
                        {
                            arrestGageIcon = arrestGageIconObject_POLICE[i];
                            break;
                        }                        
                    }
                }
                break;

            case ChaserData.Type.SWAT:
                {
                    for (int i = 0; i < arrestGageIconObject_SWAT.Length; i++)
                    {
                        if (arrestGageIconObject_SWAT[i].activeSelf == false)
                        {
                            arrestGageIcon = arrestGageIconObject_SWAT[i];
                            break;
                        }
                    }
                }
                break;

            case ChaserData.Type.Hummer:
                {
                    for (int i = 0; i < arrestGageIconObject_HUMMER.Length; i++)
                    {
                        if (arrestGageIconObject_HUMMER[i].activeSelf == false)
                        {
                            arrestGageIcon = arrestGageIconObject_HUMMER[i];
                            break;
                        }
                    }
                }
                break;

            case ChaserData.Type.Tank:
                {
                    for (int i = 0; i < arrestGageIconObject_TANK.Length; i++)
                    {
                        if (arrestGageIconObject_TANK[i].activeSelf == false)
                        {
                            arrestGageIcon = arrestGageIconObject_TANK[i];
                            break;
                        }
                    }
                }
                break;
        }

        return arrestGageIcon;
    }

    public void StartArrest(int chaserCarID, ChaserData.Type chaserType)
    {
        GameObject arrestGageIconObject = GetArrestGageIcon(chaserType);
        if (arrestGageIconObject != null)
        {
            ArrestGageIcon arrestGageIcon = arrestGageIconObject.GetComponent<ArrestGageIcon>();
            string arrestGageIconKey = chaserType.ToString() + chaserCarID;
            if (!m_ArrestGageIconDic.ContainsKey(arrestGageIconKey))
            {
                m_ArrestGageIconDic.Add(arrestGageIconKey, arrestGageIcon);
                NGUITools.SetActive(arrestGageIconObject, true);
                arrestGageIcon.StartArrestGage(this);
            }
        }
    }

    public void StopArrest(int chaserCarID, ChaserData.Type chaserType)
    {
        string arrestGageIconKey = chaserType.ToString() + chaserCarID;
        if (m_ArrestGageIconDic.ContainsKey(arrestGageIconKey))
        {
            ArrestGageIcon arrestGageIcon = m_ArrestGageIconDic[arrestGageIconKey];
            arrestGageIcon.StopArrestGage();
            NGUITools.SetActive(arrestGageIcon.gameObject, false);
            m_ArrestGageIconDic.Remove(arrestGageIconKey);
        }
    }

    IEnumerator UpdateArrestGage()
    {
        float percent = 0.0f;
        Dictionary<string, ArrestGageIcon>.Enumerator etor;
        while(m_State == State.Playing)
        {
            yield return new WaitForEndOfFrame();

            percent = 0.0f;
            etor = m_ArrestGageIconDic.GetEnumerator();
            while(etor.MoveNext())
            {
                float temp = etor.Current.Value.currentPercent;
                if(temp > percent)
                {
                    percent = temp;
                }
            }

            arrestGageSprite.fillAmount = percent;
        }
    }
}

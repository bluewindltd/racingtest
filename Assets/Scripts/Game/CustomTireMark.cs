﻿using UnityEngine;
using System.Collections;

//Class for tire mark instances
public class CustomTireMark : MonoBehaviour
{
    [System.NonSerialized]
    public float fadeTime = -1;
    [System.NonSerialized]
    public bool fading = false;
    [System.NonSerialized]
    public float alpha = 1;
    [System.NonSerialized]
    public Mesh mesh;
    [System.NonSerialized]
    public Color[] colors;

    int m_TireMarkID = -1;
    public int tireMarkID
    {
        get
        {
            return m_TireMarkID;
        }
        set
        {
            m_TireMarkID = value;
        }
    }

    ////Fade the tire mark and then destroy it
    //void Update()
    //{
    //    if (fading)
    //    {
    //        if (alpha <= 0)
    //        {
    //            TireMarkManager.instance.PutTireMark(this);
    //        }
    //        else
    //        {
    //            alpha -= Time.deltaTime;

    //            for (int i = 0; i < colors.Length; i++)
    //            {
    //                colors[i].a -= Time.deltaTime;
    //            }

    //            mesh.colors = colors;
    //        }
    //    }
    //    else
    //    {
    //        if (fadeTime > 0)
    //        {
    //            fadeTime = Mathf.Max(0, fadeTime - Time.deltaTime);
    //        }
    //        else if (fadeTime == 0)
    //        {
    //            fading = true;
    //        }
    //    }
    //}
}
﻿using UnityEngine;
using System.Collections;

public class PlayerHPGage : MonoBehaviour
{
    public UISprite gageSprite;
    public Color fullColor;
    public Color emptyColor;

    float m_PrePercent = 1.0f;

    public void SetGage(float percent)
    {
        if(m_PrePercent != percent)
        {
            gageSprite.fillAmount = percent;
            gageSprite.color = Color.Lerp(emptyColor, fullColor, percent);
        }        
    }
}

﻿//using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using SimpleJSON;

[Serializable]
public class GroundCountInfo
{
    [HideInInspector]
    [SerializeField]
    public int straightGroundCount;
    [HideInInspector]
    [SerializeField]
    public int cornerGroundCount;
    [HideInInspector]
    [SerializeField]
    public int pieceGroundCount;
}

[Serializable] public class DictionaryOfGroundCountTheme : SerializableDictionary<int, GroundCountInfo> { }

public class MapObjectData
{
    public int theme = 0;
    public int index = 0;
    public Vector3 pos = Vector3.zero;
    public Vector3 rot = Vector3.zero;
    public Vector3 scale = Vector3.zero;
}

public class MapData
{
    public List<MapObjectData> mapObjectList = new List<MapObjectData>();
}

[Serializable]
public class GroundPoolObjectCreator
{
    [HideInInspector][SerializeField] public GameObject poolObjectPrefab;
}

[Serializable] public class DictionaryOfGroundPoolObjectCreator : SerializableDictionary<int, GroundPoolObjectCreator> { }
[Serializable] public class DictionaryOfGroundPoolObjectTheme : SerializableDictionary<int, DictionaryOfGroundPoolObjectCreator> { }

[ExecuteInEditMode]
public class GroundManager : Singleton<GroundManager>
{
    [HideInInspector][SerializeField] public DictionaryOfGroundCountTheme m_GroundCountDic = new DictionaryOfGroundCountTheme();
    public DictionaryOfGroundCountTheme groundCountDic
    {
        get
        {
            return m_GroundCountDic;
        }
        set
        {
            m_GroundCountDic = value;
        }
    }

    [SerializeField] public int[] groundPoolCounts;
    [SerializeField] public Transform[] groundParents;
    [SerializeField] public GameObject[] groundPrefabs;

    [HideInInspector][SerializeField] public DictionaryOfGroundPoolObjectTheme m_StraightPoolPrefabDic = new DictionaryOfGroundPoolObjectTheme();
    public DictionaryOfGroundPoolObjectTheme straightPoolPrefabDic
    {
        get
        {
            return m_StraightPoolPrefabDic;
        }
        set
        {
            m_StraightPoolPrefabDic = value;
        }
    }
    [HideInInspector][SerializeField] public DictionaryOfGroundPoolObjectTheme m_CornerPoolPrefabDic = new DictionaryOfGroundPoolObjectTheme();
    public DictionaryOfGroundPoolObjectTheme cornerPoolPrefabDic
    {
        get
        {
            return m_CornerPoolPrefabDic;
        }
        set
        {
            m_CornerPoolPrefabDic = value;
        }
    }
    [HideInInspector][SerializeField] public DictionaryOfGroundPoolObjectTheme m_PiecePoolPrefabDic = new DictionaryOfGroundPoolObjectTheme();
    public DictionaryOfGroundPoolObjectTheme piecePoolPrefabDic
    {
        get
        {
            return m_PiecePoolPrefabDic;
        }
        set
        {
            m_PiecePoolPrefabDic = value;
        }
    }


    Dictionary<int, Dictionary<int, Dictionary<int, ObjectPool>>> m_GroundObjectPoolDic = new Dictionary<int, Dictionary<int, Dictionary<int, ObjectPool>>>();

    Dictionary<int, Dictionary<int, GameObject>> m_IntroGroundObjectDic = new Dictionary<int, Dictionary<int, GameObject>>();
    Dictionary<int, GameObject> m_IntroUpGroundObjectDic = new Dictionary<int, GameObject>();

    Dictionary<string, MapData> m_MapDataDic = new Dictionary<string, MapData>();
    public Dictionary<string, MapData> mapDataDic
    {
        get
        {
            return m_MapDataDic;
        }
    }

    int m_IntroTheme = 0;

    void Awake()
    {
        if (Application.isPlaying)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    int GetGroundCount(int theme, GameConfig.GroundType groundType)
    {
        if(m_GroundCountDic.ContainsKey(theme))
        {
            GroundCountInfo groundCountInfo = m_GroundCountDic[theme];
            if(groundType == GameConfig.GroundType.Straight)
            {
                return groundCountInfo.straightGroundCount;
            }
            else if (groundType == GameConfig.GroundType.Corner)
            {
                return groundCountInfo.cornerGroundCount;
            }
            else if (groundType == GameConfig.GroundType.Piece)
            {
                return groundCountInfo.pieceGroundCount;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }

    //public void MakeGroundPrefabs()
    //{
    //    int theme = 0;
    //    string mapDataName = "";
    //    string groundName = "";
    //    MapData mapData = null;
    //    Ground ground = null;

    //    DictionaryOfGroundCountTheme.Enumerator etor = m_GroundCountDic.GetEnumerator();
    //    // 테마별로 3가지 종류의 Ground가 몇개씩 있는지 순회
    //    while (etor.MoveNext())
    //    {
    //        theme = etor.Current.Key;

    //        GroundCountInfo groundCountInfo = etor.Current.Value;
    //        int straightPoolCount = Mathf.CeilToInt((float)groundPoolCounts[0] / (float)groundCountInfo.straightGroundCount);
    //        for (int i = 0; i < groundCountInfo.straightGroundCount; i++)
    //        {
    //            mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", theme, 0, i);
    //            groundName = string.Format("ground_{0}_{1}_{2}", theme, 0, i);
    //            mapData = LoadMapData(mapDataName);
    //            if (mapData != null)
    //            {
    //                m_MapDataDic.Add(mapDataName, mapData);
    //            }

    //            GameObject groundStraightObject = (GameObject)MonoBehaviour.Instantiate(groundPrefabs[0], Vector3.zero, Quaternion.identity);
    //            ground = groundStraightObject.GetComponent<Ground>();
    //            ground.LoadMapData(mapDataName, false, false);

    //            UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/Map/grounds/" + groundName + ".prefab");
    //            PrefabUtility.ReplacePrefab(groundStraightObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

    //            DestroyImmediate(groundStraightObject);
    //        }

    //        int cornerPoolCount = Mathf.CeilToInt((float)groundPoolCounts[1] / (float)groundCountInfo.cornerGroundCount);
    //        for (int i = 0; i < groundCountInfo.cornerGroundCount; i++)
    //        {
    //            mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", theme, 1, i);
    //            groundName = string.Format("ground_{0}_{1}_{2}", theme, 1, i);
    //            mapData = LoadMapData(mapDataName);
    //            if (mapData != null)
    //            {
    //                m_MapDataDic.Add(mapDataName, mapData);
    //            }

    //            GameObject groundCornerObject = (GameObject)MonoBehaviour.Instantiate(groundPrefabs[1], Vector3.zero, Quaternion.identity);
    //            ground = groundCornerObject.GetComponent<Ground>();
    //            ground.LoadMapData(mapDataName, false, false);

    //            UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/Map/grounds/" + groundName + ".prefab");
    //            PrefabUtility.ReplacePrefab(groundCornerObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

    //            DestroyImmediate(groundCornerObject);
    //        }
            
    //        int piecePoolCount = Mathf.CeilToInt((float)groundPoolCounts[2] / (float)groundCountInfo.pieceGroundCount);
    //        for (int i = 0; i < groundCountInfo.pieceGroundCount; i++)
    //        {
    //            mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", theme, 2, i);
    //            groundName = string.Format("ground_{0}_{1}_{2}", theme, 2, i);
    //            mapData = LoadMapData(mapDataName);
    //            if (mapData != null)
    //            {
    //                m_MapDataDic.Add(mapDataName, mapData);
    //            }

    //            GameObject groundPieceObject = (GameObject)MonoBehaviour.Instantiate(groundPrefabs[2], Vector3.zero, Quaternion.identity);
    //            ground = groundPieceObject.GetComponent<Ground>();
    //            ground.LoadMapData(mapDataName, false, false);

    //            UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/Map/grounds/" + groundName + ".prefab");
    //            PrefabUtility.ReplacePrefab(groundPieceObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

    //            DestroyImmediate(groundPieceObject);
    //        }
    //    }
    //}

    public void Initialize()
    {
        int theme = 0;
        string mapDataName = "";
        MapData mapData = null;
        Ground ground = null;

        DictionaryOfGroundCountTheme.Enumerator etor = m_GroundCountDic.GetEnumerator();
        // 테마별로 3가지 종류의 Ground가 몇개씩 있는지 순회
        while(etor.MoveNext())
        {
            theme = etor.Current.Key;

            //
            Dictionary<int, Dictionary<int, ObjectPool>> groundThemeDic = new Dictionary<int, Dictionary<int, ObjectPool>>();          
            m_GroundObjectPoolDic.Add(theme, groundThemeDic);

            GroundCountInfo groundCountInfo = etor.Current.Value;

            Dictionary<int, ObjectPool> groundStraightDic = new Dictionary<int, ObjectPool>();
            groundThemeDic.Add(0, groundStraightDic);
            int straightPoolCount = Mathf.CeilToInt((float)groundPoolCounts[0] / (float)groundCountInfo.straightGroundCount);
            for (int i = 0; i < groundCountInfo.straightGroundCount; i++)
            {
                if(m_StraightPoolPrefabDic.ContainsKey(theme))
                {
                    if(m_StraightPoolPrefabDic[theme].ContainsKey(i))
                    {
                        GameObject groundStraightObject = m_StraightPoolPrefabDic[theme][i].poolObjectPrefab;

                        if (groundStraightObject != null)
                        {
                            groundStraightObject.GetComponent<Ground>().InitializeMapData();
                            ObjectPool objectPool = new ObjectPool(straightPoolCount, groundStraightObject, groundParents[0]);
                            groundStraightDic.Add(i, objectPool);
                        }                        
                    }
                }
            }

            Dictionary<int, ObjectPool> groundCornerDic = new Dictionary<int, ObjectPool>();
            groundThemeDic.Add(1, groundCornerDic);
            int cornerPoolCount = Mathf.CeilToInt((float)groundPoolCounts[1] / (float)groundCountInfo.cornerGroundCount);
            for (int i = 0; i < groundCountInfo.cornerGroundCount; i++)
            {
                if (m_CornerPoolPrefabDic.ContainsKey(theme))
                {
                    if (m_CornerPoolPrefabDic[theme].ContainsKey(i))
                    {
                        GameObject groundCornerObject = m_CornerPoolPrefabDic[theme][i].poolObjectPrefab;

                        if (groundCornerObject != null)
                        {
                            groundCornerObject.GetComponent<Ground>().InitializeMapData();
                            ObjectPool objectPool = new ObjectPool(cornerPoolCount, groundCornerObject, groundParents[1]);
                            groundCornerDic.Add(i, objectPool);
                        }
                    }
                }
            }

            Dictionary<int, ObjectPool> groundPieceDic = new Dictionary<int, ObjectPool>();
            groundThemeDic.Add(2, groundPieceDic);
            int piecePoolCount = Mathf.CeilToInt((float)groundPoolCounts[2] / (float)groundCountInfo.pieceGroundCount);
            for (int i = 0; i < groundCountInfo.pieceGroundCount; i++)
            {
                if (m_PiecePoolPrefabDic.ContainsKey(theme))
                {
                    if (m_PiecePoolPrefabDic[theme].ContainsKey(i))
                    {
                        GameObject groundPieceObject = m_PiecePoolPrefabDic[theme][i].poolObjectPrefab;

                        if (groundPieceObject != null)
                        {
                            groundPieceObject.GetComponent<Ground>().InitializeMapData();
                            ObjectPool objectPool = new ObjectPool(piecePoolCount, groundPieceObject, groundParents[2]);
                            groundPieceDic.Add(i, objectPool);
                        }
                    }
                }
            }
        }
    }

    public void InitializeObstacle()
    {
        ObjectPool objectPool;
        Ground ground;
        Dictionary<int, Dictionary<int, Dictionary<int, ObjectPool>>>.Enumerator etor1 = m_GroundObjectPoolDic.GetEnumerator();
        while(etor1.MoveNext())
        {
            Dictionary<int, Dictionary<int, ObjectPool>>.Enumerator etor2 = etor1.Current.Value.GetEnumerator();
            while(etor2.MoveNext())
            {
                Dictionary<int, ObjectPool>.Enumerator etor3 = etor2.Current.Value.GetEnumerator();
                while(etor3.MoveNext())
                {
                    objectPool = etor3.Current.Value;
                    for(int i = 0; i < objectPool.poolList.Count; i++)
                    {
                        ground = objectPool.poolList[i].GetComponent<Ground>();
                        ground.InitializeObstacle(false);
                    }
                }
            }
        }
    }

    public void InitializeIntroGround()
    {
        string mapDataName = "";
        MapData mapData = null;
        Ground ground = null;

        // 제일 처음 배치되는 인트로 그라운드 구축
        Dictionary<int, GameObject> introGroundThemeDic = new Dictionary<int, GameObject>();
        m_IntroGroundObjectDic.Add(m_IntroTheme, introGroundThemeDic);

        for (int i = 0; i < 4; i++)
        {
            mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", m_IntroTheme, 2, 100 + i);
            if(!m_MapDataDic.ContainsKey(mapDataName))
            {
                mapData = LoadMapData(mapDataName);
                if (mapData != null)
                {
                    m_MapDataDic.Add(mapDataName, mapData);
                }
            }            

            GameObject groundPieceObject = (GameObject)MonoBehaviour.Instantiate(groundPrefabs[2], Vector3.zero, Quaternion.identity);
            ground = groundPieceObject.GetComponent<Ground>();
            if (i == 0)
            {
                ground.LoadMapData(mapDataName, false, true);
            }
            else
            {
                ground.LoadMapData(mapDataName, false, false);
            }

            ground.InitializeObstacle(true);
            introGroundThemeDic.Add(i, groundPieceObject);
        }

        // 인트로 그라운드 바로 윗 그라운드 관련 정보 로드
        mapDataName = string.Format("MapData/ground_{0}_{1}_{2}", m_IntroTheme, 0, 1000);
        if (!m_MapDataDic.ContainsKey(mapDataName))
        {
            mapData = LoadMapData(mapDataName);
            if (mapData != null)
            {
                m_MapDataDic.Add(mapDataName, mapData);
            }
        }
        GameObject introUpGroundObject = (GameObject)MonoBehaviour.Instantiate(groundPrefabs[0], Vector3.zero, Quaternion.identity);
        ground = introUpGroundObject.GetComponent<Ground>();
        ground.LoadMapData(mapDataName, false, false);
        ground.InitializeObstacle(true);
        m_IntroUpGroundObjectDic.Add(m_IntroTheme, introUpGroundObject);
    }

    public void RestoreIntroGround()
    {
        GameObject groundObject;
        Dictionary<int, Dictionary<int, GameObject>>.Enumerator etor1 = m_IntroGroundObjectDic.GetEnumerator();
        while(etor1.MoveNext())
        {
            Dictionary<int, GameObject>.Enumerator etor2 = etor1.Current.Value.GetEnumerator();
            while(etor2.MoveNext())
            {
                groundObject = etor2.Current.Value;
                Destroy(groundObject);
            }
            etor1.Current.Value.Clear();
        }
        m_IntroGroundObjectDic.Clear();

        Dictionary<int, GameObject>.Enumerator etor3 = m_IntroUpGroundObjectDic.GetEnumerator();
        while(etor3.MoveNext())
        {
            groundObject = etor3.Current.Value;
            Destroy(groundObject);
        }
        m_IntroUpGroundObjectDic.Clear();
    }

    public Ground GetIntroGround(int theme, int index)
    {
        Ground ground = null;
        if(m_IntroGroundObjectDic.ContainsKey(theme))
        {
            Dictionary<int, GameObject> introGroundThemeDic = m_IntroGroundObjectDic[theme];
            if(introGroundThemeDic.ContainsKey(index))
            {
                ground = introGroundThemeDic[index].GetComponent<Ground>();
                ground.index = 100 + index;
            }
        }

        return ground;
    }

    public Ground GetIntroUpGround(int theme)
    {
        Ground ground = null;
        if (m_IntroUpGroundObjectDic.ContainsKey(theme))
        {
            GameObject introUpGroundObject = m_IntroUpGroundObjectDic[theme];
            ground = introUpGroundObject.GetComponent<Ground>();
            ground.index = 1000;
        }

        return ground;
    }

    public Ground GetGround(int theme, GameConfig.GroundType groundType)
    {
        Ground ground = null;
        int maxGroundCount = GetGroundCount(theme, groundType);
        if (maxGroundCount > 0)
        {
            if(m_GroundObjectPoolDic.ContainsKey(theme))
            {
                if(m_GroundObjectPoolDic[theme].ContainsKey((int)groundType))
                {
                    int loopCount = 0;
                    int groundIndex = UnityEngine.Random.Range(0, maxGroundCount);
                    while (loopCount < maxGroundCount)
                    {
                        if (m_GroundObjectPoolDic[theme][(int)groundType].ContainsKey(groundIndex))
                        {
                            ObjectPool objectPool = m_GroundObjectPoolDic[theme][(int)groundType][groundIndex];
                            if (!objectPool.IsFull())
                            {
                                ground = objectPool.GetObjectFromPool().GetComponent<Ground>();
                                ground.index = groundIndex;
                                break;
                            }
                            else
                            {
                                groundIndex++;
                                if(groundIndex >= maxGroundCount)
                                {
                                    groundIndex = 0;
                                }
                                loopCount++;
                            }
                        }
                    }

                    if (ground == null)
                    {
                        groundIndex = UnityEngine.Random.Range(0, maxGroundCount);
                        if (m_GroundObjectPoolDic[theme][(int)groundType].ContainsKey(groundIndex))
                        {
                            ObjectPool objectPool = m_GroundObjectPoolDic[theme][(int)groundType][groundIndex];
                            ground = objectPool.GetObjectFromPool().GetComponent<Ground>();
                            ground.index = groundIndex;
                        }
                    }

                    //while (true)
                    //{
                    //    int groundIndex = UnityEngine.Random.Range(0, maxGroundCount);
                    //    if (m_GroundObjectPoolDic[theme][(int)groundType].ContainsKey(groundIndex))
                    //    {
                    //        ObjectPool objectPool = m_GroundObjectPoolDic[theme][(int)groundType][groundIndex];
                    //        if (!objectPool.IsFull())
                    //        {
                    //            ground = objectPool.GetObjectFromPool().GetComponent<Ground>();
                    //            ground.index = groundIndex;
                    //            break;
                    //        }
                    //        else
                    //        {
                    //            if(!m_GroundCheckDic.ContainsKey(groundIndex))
                    //            {
                    //                m_GroundCheckDic.Add(groundIndex, true);
                    //            }
                    //        }

                    //        //else
                    //        //{
                    //        //    ground = objectPool.GetObjectFromPool().GetComponent<Ground>();
                    //        //    ground.index = groundIndex;
                    //        //    break;
                    //        //}
                    //    }
                    //}
                    return ground;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    public void RestoreGround(bool isFirst, bool isFirstUp, int theme, GameConfig.GroundType groundType, int groundIndex, GameObject groundGameObject)
    {
        if (isFirst || isFirstUp)
        {
            Destroy(groundGameObject);
        }
        else
        {
            if (m_GroundObjectPoolDic.ContainsKey(theme))
            {
                if (m_GroundObjectPoolDic[theme].ContainsKey((int)groundType))
                {
                    if (m_GroundObjectPoolDic[theme][(int)groundType].ContainsKey(groundIndex))
                    {
                        ObjectPool objectPool = m_GroundObjectPoolDic[theme][(int)groundType][groundIndex];
                        objectPool.PutObjectInPool(groundGameObject);
                    }
                }
            }
        }
    }

    public MapData GetMapData(string mapDataName)
    {
        if(m_MapDataDic.ContainsKey(mapDataName))
        {
            return m_MapDataDic[mapDataName];
        }
        else
        {
            return null;
        }
    }

    public MapData LoadMapData(string loadPath)
    {
        TextAsset asset = Resources.Load(loadPath) as TextAsset;
        if (asset.text.CompareTo("") != 0)
        {
            MapData mapData = new MapData();

            //JSONNode json = JSON.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(asset.text)));
            JSONNode json = JSON.Parse(asset.text);
            JSONNode jsonMapData = json["map_data"];

            //m_GroundType = (GameConfig.GroundType)jsonMapData["groundType"].AsInt;
            //m_Theme = jsonMapData["theme"].AsInt;
            JSONArray jsonObjectList = jsonMapData["object_list"].AsArray;
            for (int i = 0; i < jsonObjectList.Count; i++)
            {
                MapObjectData mapObjectData = new MapObjectData();

                JSONNode jsonObjectData = jsonObjectList[i];

                //GameConfig.ObjectType objectType = (GameConfig.ObjectType)jsonObjectData["objectType"].AsInt;
                int objectTheme = jsonObjectData["theme"].AsInt;
                int objectIndex = jsonObjectData["index"].AsInt;
                Vector3 pos = new Vector3(jsonObjectData["pos_x"].AsFloat, jsonObjectData["pos_y"].AsFloat, jsonObjectData["pos_z"].AsFloat);
                Vector3 rotation = new Vector3(jsonObjectData["rot_x"].AsFloat, jsonObjectData["rot_y"].AsFloat, jsonObjectData["rot_z"].AsFloat);
                Vector3 scale = new Vector3(jsonObjectData["sc_x"].AsFloat, jsonObjectData["sc_y"].AsFloat, jsonObjectData["sc_z"].AsFloat);

                mapObjectData.theme = objectTheme;
                mapObjectData.index = objectIndex;
                mapObjectData.pos = pos;
                mapObjectData.rot = rotation;
                mapObjectData.scale = scale;

                mapData.mapObjectList.Add(mapObjectData);
            }

            return mapData;
        }
        else
        {
            return null;
        }
    }
}

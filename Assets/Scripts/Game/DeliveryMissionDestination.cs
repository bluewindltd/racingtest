﻿using UnityEngine;
using System.Collections;

public class DeliveryMissionDestination : MonoBehaviour
{
    public Animator animator;
    public BoxCollider boxCollider;

    public void ActiveDestination()
    {
        boxCollider.enabled = true;

        animator.Play("MissionDeliver_Target_Idle");        
    }

    public void DeactiveDestination()
    {
        boxCollider.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles"))
        {
            Debug.Log("!!! Get Delivery Mission");

            StartCoroutine(GetMissionDestination());

            MissionManager.instance.EndMission(true);
        }
    }

    IEnumerator GetMissionDestination()
    {
        boxCollider.enabled = false;

        animator.Play("MissionDeliver_Target_Get");

        yield return new WaitForSeconds(0.65f);

        gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using System.Text;
using System.Collections;

public class ChaserAI : WeMonoBehaviour
{
    ChaserData.Classes m_ChaserClasses;
    public ChaserData.Classes chaserClasses
    {
        get
        {
            return m_ChaserClasses;
        }
        set
        {
            m_ChaserClasses = value;
        }
    }

    ChaserData.Type m_ChaserType;
    public ChaserData.Type chaserType
    {
        get
        {
            return m_ChaserType;
        }
        set
        {
            m_ChaserType = value;
        }
    }
    
    Rigidbody rb;
    VehicleParent vp;
    VehicleAssist va;
    public bool isPoliceLine = false;
    public Transform target;
    bool m_Initialize = false;
    Vector3 m_SpawnPos = Vector3.zero;
    public Vector3 spawnPos
    {
        set
        {
            m_SpawnPos = value;
        }
    }
    Quaternion m_SpawnRot = Quaternion.identity;
    public Quaternion spawnRot
    {
        set
        {
            m_SpawnRot = value;
        }
    }
    Transform targetPrev;
    Rigidbody targetBody;
    Vector3 targetPoint;
    bool m_IsTargetVisible;
    public bool isTargetVisible
    {
        get
        {
            return m_IsTargetVisible;
        }
    }
    //bool targetIsWaypoint;
    //VehicleWaypoint targetWaypoint;

    public float followDistance;
    bool close;

    [Tooltip("Percentage of maximum speed to drive at")]
    [RangeAttribute(0, 1)]
    public float speed = 1;
    float initialSpeed;
    float prevSpeed;
    //float brakeTime;

    [Tooltip("Mask for which objects can block the view of the target")]
    public LayerMask viewBlockMask;
    public LayerMask avoidMask;
    Vector3 dirToTarget;//Normalized direction to target
    float lookDot;//Dot product of forward direction and dirToTarget
    float steerDot;//Dot product of right direction and dirToTarget

    float stoppedTime;
    float reverseTime;

    [Tooltip("Time limit in seconds which the vehicle is stuck before attempting to reverse")]
    public float stopTimeReverse = 1;

    [Tooltip("Duration in seconds the vehicle will reverse after getting stuck")]
    public float reverseAttemptTime = 1;

    [Tooltip("How many times the vehicle will attempt reversing before resetting, -1 = no reset")]
    public int resetReverseCount = 1;
    int reverseAttempts;

    [Tooltip("Seconds a vehicle will be rolled over before resetting, -1 = no reset")]
    public float rollResetTime = 3;
    float rolledOverTime;

    bool avoidDriving = false;
    float turnValue = 0.0f;
    float sideValue = 0.0f;

    float currentBrakeValue = 0.0f;
    float brakeValue = 0.0f;

    float currentEBrakeValue = 0.0f;
    float eBrakeValue = 0.0f;

    bool isLeftTurn = false;
    bool isRightTurn = false;

    public int chaserCarID = -1;
    public float fallLimit = -4;

    Vector3 prevReversePos = Vector3.zero;

    bool m_isNearPlayer = false;
    public bool isNearPlayer
    {
        get
        {
            return m_isNearPlayer;
        }
        set
        {
            m_isNearPlayer = value;
        }
    }

    Vector3 m_PreTargetPos = Vector3.zero;
    public Vector3 preTargetPos
    {
        get
        {
            return m_PreTargetPos;
        }
        set
        {
            m_PreTargetPos = value;
        }
    }

    float health = 1;

    public GameParameter gameParameter;
    public Material originalMaterial;
    public Material damageMaterial;
    public MeshRenderer[] bodyRendererArray;
    public MeshRenderer[] tireRendererArray;

    //public ParticleSystem smoke;
    //float initialSmokeEmission;
    public GameObject[] smokeObjectArray;
    int m_PreSmokeIndex = -1;

    bool m_isDisable = false;
    public bool isDisable
    {
        get
        {
            return m_isDisable;
        }
    }

    UILabel m_HpTextLabel = null;

    // 변수 정리
    public FrontHitArea frontHitArea;

    bool m_RightHit = false;
    bool m_LeftHit = false;
    
    Vector3 m_TempDirection = Vector3.zero;

    Vector3 m_TempRightDirection = Vector3.zero;
    Vector3 m_TempRightCheck = Vector3.zero;

    Vector3 m_TempLeftDirection = Vector3.zero;
    Vector3 m_TempLeftCheck = Vector3.zero;

    Coroutine m_ReverseCoroutine = null;

    bool m_IsAvoiding = false;
    Coroutine m_AvoidingCoroutine = null;

    public CustomTireMarkCreate[] tireMarkCreateArray;

    public AudioSource sirenSnd;

    SoundManager m_SoundMgr;

    protected override void Awake()
    {
        base.Awake();

        if (!isPoliceLine)
        {
            rb = GetComponent<Rigidbody>();
            vp = GetComponent<VehicleParent>();
            va = GetComponent<VehicleAssist>();
        }
    }

    public void Initialize()
    {
        m_SoundMgr = SoundManager.instance;

        initialSpeed = speed;

        turnValue = 0.0f;
        sideValue = 0.0f;
        stoppedTime = 0.0f;
        reverseTime = 0.0f;
        m_ReverseCoroutine = null;
        reverseAttempts = 0;
        rolledOverTime = 0.0f;
        prevReversePos = Vector3.zero;

        isNearPlayer = false;
        m_PreTargetPos = Vector3.zero;

        m_isDisable = false;

        m_IsAvoiding = false;
        m_AvoidingCoroutine = null;

        for (int i = 0; i < smokeObjectArray.Length; i++)
        {
            smokeObjectArray[i].SetActive(false);
        }
        m_PreSmokeIndex = -1;

        InitializeTarget();

        gameParameter.shrink = false;
        gameParameter.currentHealth = gameParameter.maxHealth;
        m_isDisable = false;
        m_Initialize = false;
        for (int i = 0; i < bodyRendererArray.Length; i++)
        {
            bodyRendererArray[i].material = originalMaterial;
        }
        for(int i = 0; i < tireRendererArray.Length; i++)
        {
            tireRendererArray[i].material = GlobalControl.tireMaterialStatic;
        }

        StartCoroutine(InitializePosition());
    }

    public void InitializeTarget()
    {
        if (target)
        {
            //if target is a vehicle
            targetBody = target.root.GetComponent<Rigidbody>();
        }
    }

    IEnumerator InitializePosition()
    {
        if (!isPoliceLine)
        {
            m_Tr.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            m_Tr.position = m_SpawnPos;

            yield return new WaitForFixedUpdate();

            m_Tr.rotation = Quaternion.identity;

#if BWTEST
            GameObject hpTextObject = HUDManager.instance.GetHpTextObject();

            AttachObjectUI attachObjectUI = hpTextObject.GetComponent<AttachObjectUI>();
            attachObjectUI.Attach(m_Tr);
            m_HpTextLabel = hpTextObject.GetComponent<UILabel>();
#endif
        }
        else
        {
            yield return new WaitForFixedUpdate();
        }

        m_Initialize = true;
    }

    void RemoveCar()
    {
        if (!isPoliceLine)
        {
            //for (int i = 0; i < tireMarkCreateArray.Length; i++)
            //{
            //    tireMarkCreateArray[i].Restore();
            //}

#if BWTEST
            HUDManager.instance.PutHpTextObject(m_HpTextLabel.gameObject);
            m_HpTextLabel = null;
#endif

            m_Tr.position = Vector3.zero;
            m_Tr.rotation = Quaternion.identity;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            ChaserManager.instance.RemoveChaserCar(chaserCarID);
        }
    }

    IEnumerator DelayDestroyChaserCar()
    {
        yield return new WaitForSeconds(5.0f);

        RemoveCar();
    }

    public void DisableChaserAI()
    {
        m_isDisable = true;

        if (!isPoliceLine)
        {
            vp.SetAccel(0.0f);
            vp.SetBrake(0.0f);
            vp.SetEbrake(1.0f);
        }
    }

    void FixedUpdate()
    {
        if (!isPoliceLine)
        {
            if (!RegionManager.instance.IsExistRegion(m_Tr.position.x, m_Tr.position.z))
            {
                RemoveCar();

                return;
            }
        }
    }

    void Update()
    {
        if (!isPoliceLine)
        {
            if (!RegionManager.instance.IsExistRegion(m_Tr.position.x, m_Tr.position.z))
            {
                RemoveCar();

                return;
            }
        }

        if (m_Initialize && target && m_isDisable == false)
        {
            if (!isPoliceLine)
            {
#if BWTEST
                if (m_HpTextLabel != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(gameParameter.currentHealth);
                    sb.Append("/");
                    sb.Append(gameParameter.maxHealth);
                    m_HpTextLabel.text = sb.ToString();
                    //m_HpTextLabel.text = string.Format("{0}/{1}", gameParameter.currentHealth, gameParameter.maxHealth);
                }
#endif
            }

            if (target != targetPrev)
            {
                InitializeTarget();
            }

            targetPrev = target;

            if (smokeObjectArray.Length > 0 && gameParameter != null)
            {
                int smokeState = (int)GameUtil.CalcSmokeState(gameParameter.currentHealth, gameParameter.maxHealth);
                if (smokeState != (int)GameConfig.SmokeState.None &&
                   m_PreSmokeIndex != smokeState)
                {
                    if (m_PreSmokeIndex != -1)
                    {
                        smokeObjectArray[m_PreSmokeIndex].SetActive(false);
                    }
                    m_PreSmokeIndex = (int)smokeState;
                    smokeObjectArray[m_PreSmokeIndex].SetActive(true);
                }
            }

            if (gameParameter.currentHealth <= 0)
            {
                for (int i = 0; i < bodyRendererArray.Length; i++)
                {
                    bodyRendererArray[i].material = damageMaterial;
                }
                for (int i = 0; i < tireRendererArray.Length; i++)
                {
                    tireRendererArray[i].material = damageMaterial;
                }
                m_isDisable = true;

                if (!isPoliceLine)
                {
                    vp.SetAccel(0.0f);
                    vp.SetBrake(0.0f);
                    vp.SetEbrake(1.0f);

                    if (m_ChaserClasses == ChaserData.Classes.Illegal)
                    {
                        MissionManager.instance.DestroyChaserCar();
                    }

                    StartCoroutine(DelayDestroyChaserCar());
                }
#if BWQUEST
                if (m_ChaserType == ChaserData.Type.Gangster)
                {
                    QuestManager.instance.CheckQuestData(QuestData.Type.DestroyGang);
                }
                else
                {
                    QuestManager.instance.CheckQuestData(QuestData.Type.DestroyPolice);
                }
#endif

                return;
            }

            //Can I see the target?
            m_IsTargetVisible = !Physics.Linecast(m_Tr.position, target.position, viewBlockMask);
            float targetDistance = (m_Tr.position - target.position).sqrMagnitude;
            // siren login
            if (sirenSnd != null)
            {
                float sirenVolume = Mathf.Max(0.0f, ((400.0f - targetDistance) / 400.0f) * 0.5f);
                sirenSnd.volume = sirenVolume * m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
            }

            if (!isPoliceLine)
            {                
                targetPoint = targetBody ? target.position + (targetBody.velocity * 0.4f) : target.position;

                speed = initialSpeed;

                //brakeTime = Mathf.Max(0, brakeTime - Time.fixedDeltaTime);
                //Is the distance to the target less than the follow distance?

                close = targetDistance <= Mathf.Pow(followDistance, 2);
                dirToTarget = (targetPoint - m_Tr.position).normalized;
                lookDot = Vector3.Dot(vp.forwardDir, dirToTarget);
                steerDot = Vector3.Dot(vp.rightDir, dirToTarget);

                //Attempt to reverse if vehicle is stuck
                stoppedTime = Mathf.Abs(vp.localVelocity.z) < 1.2 && !close && vp.groundedWheels > 0 ? stoppedTime + Time.fixedDeltaTime : 0;

                if ((stoppedTime > stopTimeReverse || (vp.velMag < 1 && lookDot < 0) || (vp.velMag <= 20 && lookDot < -0.8)) && reverseTime == 0)
                {
                    reverseTime = reverseAttemptTime;
                    if(m_ReverseCoroutine == null)
                    {
                        m_ReverseCoroutine = StartCoroutine(ReverseStart());
                    }
                    //reverseAttempts++;

                    if (prevReversePos.Equals(Vector3.zero))
                    {
                        prevReversePos = m_Tr.position;
                    }
                    else
                    {
                        float distance = Vector3.Distance(m_Tr.position, prevReversePos);

                        if (distance < 10.0f)
                        {
                            reverseAttempts++;

                            if (reverseAttempts > resetReverseCount && resetReverseCount >= 0)
                            {
                                float playerDistance = Vector3.Distance(m_Tr.position, target.position);
                                if (playerDistance > 40.0f)
                                {
                                    //Debug.Log("RemoveChaserCar###");
                                    RemoveCar();
                                }
                                //else
                                //{
                                //    //Debug.Log("Force Foward!!!");
                                //    reverseTime = 0.0f;
                                //    lookDot = 1.0f;
                                //}
                            }
                        }
                        else
                        {
                            prevReversePos = Vector3.zero;
                            reverseAttempts = 0;
                        }
                    }
                }

                //reverseTime = Mathf.Max(0, reverseTime - Time.fixedDeltaTime);
                //Debug.Log("ReverTime : " + reverseTime + ", " + reverseAttempts + ", " + stoppedTime);

                //Debug.Log("targetBody.velocity -> " + targetBody.velocity + ", vp.velMag -> " + vp.velMag + ", lookDot -> " + lookDot);

                if (!m_IsTargetVisible)
                {
                    m_TempDirection = m_Tr.localRotation * Vector3.forward;

                    m_TempRightDirection = Quaternion.AngleAxis(90.0f, Vector3.up) * m_TempDirection;
                    m_TempRightCheck = Vector3.zero;// = direction + rightDirection;

                    m_TempLeftDirection = Quaternion.AngleAxis(-90.0f, Vector3.up) * m_TempDirection;
                    m_TempLeftCheck = Vector3.zero;// = direction + leftDirection;

                    for (int i = 0; i < GameConfig.ChaserCheckDegree.Length; i++)
                    {
                        m_TempRightCheck = m_TempDirection + m_TempRightDirection * GameConfig.ChaserCheckDegree[i];
                        m_TempLeftCheck = m_TempDirection + m_TempLeftDirection * GameConfig.ChaserCheckDegree[i];

                        m_RightHit = Physics.Raycast(m_Tr.position, m_TempRightCheck, 25.0f, avoidMask);
                        m_LeftHit = Physics.Raycast(m_Tr.position, m_TempLeftCheck, 25.0f, avoidMask);

                        if (!m_RightHit || !m_LeftHit)
                        {
                            break;
                        }
                    }

                    if (frontHitArea.frontHit || m_RightHit || m_LeftHit)
                    {
                        if (frontHitArea.frontHit && !m_RightHit && m_LeftHit)
                        {
                            if(!m_IsAvoiding)
                            {
                                if(m_AvoidingCoroutine == null)
                                {
                                    m_AvoidingCoroutine = StartCoroutine(Avoiding());
                                }
                                sideValue = 1.0f;                                
                            }                            
                        }
                        else if (frontHitArea.frontHit && m_RightHit && !m_LeftHit)
                        {
                            if (!m_IsAvoiding)
                            {
                                if (m_AvoidingCoroutine == null)
                                {
                                    m_AvoidingCoroutine = StartCoroutine(Avoiding());
                                }
                                sideValue = -1.0f;
                            }
                        }
                        else if (frontHitArea.frontHit && m_RightHit && m_LeftHit)
                        {
                            if (!m_IsAvoiding)
                            {
                                if (m_AvoidingCoroutine == null)
                                {
                                    m_AvoidingCoroutine = StartCoroutine(Avoiding());
                                }
                                sideValue = frontHitArea.sideValue;
                            }
                        }
                        else if (frontHitArea.frontHit && !m_RightHit && !m_LeftHit)
                        {
                            if (!m_IsAvoiding)
                            {
                                if (m_AvoidingCoroutine == null)
                                {
                                    m_AvoidingCoroutine = StartCoroutine(Avoiding());
                                }
                                sideValue = frontHitArea.sideValue;
                            }
                        }
                        else if (!frontHitArea.frontHit)
                        {
                            sideValue = 0.0f;
                            m_IsAvoiding = false;
                            if(m_AvoidingCoroutine != null)
                            {
                                StopCoroutine(m_AvoidingCoroutine);
                                m_AvoidingCoroutine = null;
                            }
                        }

                        float tempAccel = 0.0f;
                        if (!close && (lookDot > 0 || vp.localVelocity.z < 5) && vp.groundedWheels > 0 && reverseTime == 0)
                        {
                            tempAccel = speed;
                        }
                        vp.SetAccel(tempAccel);

                        float tempBrake = 0.0f;
                        if (reverseTime == 0 && !(close && vp.localVelocity.z > 0.1f))
                        {
                            if (lookDot < 0.5f && lookDot > 0 && vp.localVelocity.z > 10)
                            {
                                tempBrake = 0.5f - lookDot;
                            }
                            else
                            {
                                tempBrake = 0.0f;
                            }
                        }
                        else
                        {
                            tempBrake = 1.0f;
                        }
                        vp.SetBrake(tempBrake);

                        vp.SetEbrake((close && vp.localVelocity.z <= 0.1f) || (lookDot <= 0 && vp.velMag > 20) ? 1 : 0);

                        float currentTurnValue = turnValue;
                        turnValue = Mathf.Lerp(currentTurnValue, sideValue, Mathf.Min(Time.deltaTime * 20.0f, 1.0f));
                        vp.SetSteer(sideValue);

                        if (sideValue == 1.0f || sideValue == -1.0f)
                        {
                            int tempSpeed = Mathf.RoundToInt(vp.velMag * 2.23694f * 1.609344f);

                            if (tempSpeed > 60)
                            {
                                vp.SetAccel(0.0f);
                                vp.SetBrake(1.0f);
                            }
                        }
                    }
                    else
                    {
                        avoidDriving = false;
                        sideValue = 0.0f;
                        // Set vehicle inputs
                        float tempAccel = 0.0f;
                        if (!close && (lookDot > 0 || vp.localVelocity.z < 5) && vp.groundedWheels > 0 && reverseTime == 0)
                        {
                            tempAccel = speed;
                        }
                        vp.SetAccel(tempAccel);

                        float tempBrake = 0.0f;
                        if (reverseTime == 0 && !(close && vp.localVelocity.z > 0.1f))
                        {
                            if (lookDot < 0.5f && lookDot > 0 && vp.localVelocity.z > 10)
                            {
                                tempBrake = 0.5f - lookDot;
                            }
                            else
                            {
                                tempBrake = 0.0f;
                            }
                        }
                        else
                        {
                            tempBrake = 1.0f;
                        }
                        vp.SetBrake(tempBrake);

                        float tempSteer = 0.0f;
                        if (reverseTime == 0)
                        {
                            int temp = 2;
                            if ((m_Tr.position - target.position).sqrMagnitude > 20)
                            {
                                temp = 1;
                            }

                            tempSteer = Mathf.Abs(Mathf.Pow(steerDot, temp)) * Mathf.Sign(steerDot);
                        }
                        else
                        {
                            int temp = 1;
                            if (close)
                            {
                                temp = 0;
                            }
                            tempSteer = -Mathf.Sign(steerDot) * temp;
                        }
                        vp.SetSteer(tempSteer);
                        vp.SetEbrake((close && vp.localVelocity.z <= 0.1f) || (lookDot <= 0 && vp.velMag > 20) ? 1 : 0);
                    }
                }
                else
                {
                    avoidDriving = false;
                    sideValue = 0.0f;
                    // Set vehicle inputs
                    float tempAccel = 0.0f;
                    if (!close && (lookDot > 0 || vp.localVelocity.z < 5) && vp.groundedWheels > 0 && reverseTime == 0)
                    {
                        tempAccel = speed;
                    }
                    vp.SetAccel(tempAccel);

                    float tempBrake = 0.0f;
                    if (reverseTime == 0 && !(close && vp.localVelocity.z > 0.1f))
                    {
                        if (lookDot < 0.5f && lookDot > 0 && vp.localVelocity.z > 10)
                        {
                            tempBrake = 0.5f - lookDot;
                        }
                        else
                        {
                            tempBrake = 0.0f;
                        }
                    }
                    else
                    {
                        tempBrake = 1.0f;
                    }
                    vp.SetBrake(tempBrake);

                    float tempSteer = 0.0f;
                    if (reverseTime == 0)
                    {
                        int temp = 2;
                        if ((m_Tr.position - target.position).sqrMagnitude > 20)
                        {
                            temp = 1;
                        }

                        tempSteer = Mathf.Abs(Mathf.Pow(steerDot, temp)) * Mathf.Sign(steerDot);
                    }
                    else
                    {
                        int temp = 1;
                        if (close)
                        {
                            temp = 0;
                        }
                        tempSteer = -Mathf.Sign(steerDot) * temp;
                    }
                    if (lookDot <= 0)
                    {
                        int aa = 0;
                        aa = 10;
                    }
                    vp.SetSteer(tempSteer);
                    vp.SetEbrake((close && vp.localVelocity.z <= 0.1f) || (lookDot <= 0 && vp.velMag > 20) ? 1 : 0);
                }
            }
        }

        if(!isPoliceLine)
        {
            rolledOverTime = va.rolledOver ? rolledOverTime + Time.fixedDeltaTime : 0;

            //Reset if stuck rolled over
            if (rolledOverTime > rollResetTime && rollResetTime >= 0)
            {
                RemoveCar();
            }

            if (m_Tr.position.y < fallLimit)
            {
                RemoveCar();
            }
        }        
    }

    IEnumerator ReverseStart()
    {
        yield return new WaitForSeconds(reverseTime);

        reverseTime = 0.0f;
        stoppedTime = 0.0f;
        m_ReverseCoroutine = null;
    }

    IEnumerator Avoiding()
    {
        m_IsAvoiding = true;

        yield return new WaitForSeconds(2.0f);

        m_IsAvoiding = false;
        m_AvoidingCoroutine = null;
    }

    IEnumerator ReverseReset()
    {
        reverseAttempts = 0;
        reverseTime = 0;
        if(m_ReverseCoroutine != null)
        {
            StopCoroutine(m_ReverseCoroutine);
            m_ReverseCoroutine = null;
        }
        yield return new WaitForFixedUpdate();
        m_Tr.position = targetPoint;
        m_Tr.rotation = Quaternion.LookRotation(Vector3.forward, GlobalControl.worldUpDir);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    IEnumerator ResetRotation()
    {
        yield return new WaitForFixedUpdate();
        m_Tr.eulerAngles = new Vector3(0, m_Tr.eulerAngles.y, 0);
        m_Tr.Translate(Vector3.up, Space.World);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }
}
﻿using UnityEngine;
using System.Collections;

public class BattleParameter : MonoBehaviour
{
    public int attackDamage = 1;
    public float attackDelay = 0.2f;
    public float rotorSpeed = 1.0f;
    public float shellSpeed = 80.0f;
    public float explosionRadius = 0.5f;
}

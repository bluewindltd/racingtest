﻿using UnityEngine;
using System.Collections;

public class PoliceLineGroup : MonoBehaviour
{
    Transform m_ObjectPoolParent = null;
    public ChaserData.Type chaserType;
    public PoliceLineManager.PoliceLineGrade policeLineGrade;
    public Transform[] policeLineTRArray;
    public Transform[] policeLineVehicleTRArray;
    public ChaserAI[] policeLineChaserAIArray;
    public GameParameter[] policeLineGameParamArray;
    public BattleParameter[] policeLineBattleParamArray;
    public HummerAI[] policeLineHummerAIArray;
    public TankAI[] policeLineTankAIArray;

    public void SetPoliceLineGroup(Transform parentPoliceLinePoint)
    {
        m_ObjectPoolParent = transform.parent;

        transform.parent = parentPoliceLinePoint;
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;

        policeLineTRArray[0].localPosition = new Vector3(8, 0, 0);
        policeLineTRArray[0].localScale = new Vector3(1, 1, 1);
        policeLineTRArray[0].localEulerAngles = new Vector3(0, 90, 0);
        policeLineTRArray[1].localPosition = new Vector3(-8, 0, 0);
        policeLineTRArray[1].localScale = new Vector3(1, 1, 1);
        policeLineTRArray[1].localEulerAngles = new Vector3(0, 90, 0);

        if(policeLineVehicleTRArray.Length == 1)
        {
            policeLineVehicleTRArray[0].localPosition = new Vector3(0, 1.3f, 0);
            policeLineVehicleTRArray[0].localScale = new Vector3(1, 1, 1);
            policeLineVehicleTRArray[0].localEulerAngles = new Vector3(0, 90, 0);
        }
        else
        {
            policeLineVehicleTRArray[0].localPosition = new Vector3(3, 1, 0);
            policeLineVehicleTRArray[0].localScale = new Vector3(1, 1, 1);
            policeLineVehicleTRArray[0].localEulerAngles = new Vector3(0, 90, 0);

            policeLineVehicleTRArray[1].localPosition = new Vector3(-3, 1, 0);
            policeLineVehicleTRArray[1].localScale = new Vector3(1, 1, 1);
            policeLineVehicleTRArray[1].localEulerAngles = new Vector3(0, 90, 0);
        }

        for(int i = 0; i < policeLineChaserAIArray.Length; i++)
        {
            policeLineChaserAIArray[i].target = RegionManager.instance.playerTransform;
            policeLineChaserAIArray[i].chaserType = chaserType;
            policeLineChaserAIArray[i].Initialize();
        }

        for (int i = 0; i < policeLineHummerAIArray.Length; i++)
        {
            policeLineHummerAIArray[i].Initialize();
        }

        for (int i = 0; i < policeLineTankAIArray.Length; i++)
        {
            policeLineTankAIArray[i].Initialize();
        }
    }

    public void Restore()
    {
        if (m_ObjectPoolParent != null)
        {
            transform.parent = m_ObjectPoolParent;
            m_ObjectPoolParent = null;
        }
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;

        PoliceLineManager.instance.RestorePoliceLineGroup(this);
    }
}

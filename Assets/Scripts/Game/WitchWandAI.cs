﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WitchWandAI : MonoBehaviour
{
    public Rigidbody playerRigidbody;
    public Transform[] shootPointTransforms;
    public AudioSource shotSFX;
    //public GameObject[] landMineObjectArray;
    //public Transform[] landMineTransformArray;
    //public Rigidbody[] landMineRigidbodyArray;
    //public AkiraBikeLandMine[] landMineArray;
    public BattleParameter battleParam;

    List<Vector3> m_ShootDirection = new List<Vector3>();
    //List<int> m_EnableLandMineList = new List<int>();
    Coroutine m_UpdateCoroutine = null;

    SoundManager m_SoundMgr;

    void Start()
    {
        m_SoundMgr = SoundManager.instance;
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    public void StopUpdate()
    {
        if (m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(battleParam.attackDelay);

            shotSFX.pitch = Random.Range(0.9f, 1.1f);
            shotSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
            shotSFX.Play();

            for (int i = 0; i < shootPointTransforms.Length; i++)
            {
                GameObject shellObject = ShellManager.instance.GetPumpkinShell();
                shellObject.transform.parent = null;
                shellObject.transform.position = shootPointTransforms[i].position;
                shellObject.transform.rotation = shootPointTransforms[i].rotation;
                PumpkinShellExplosion shell = shellObject.GetComponent<PumpkinShellExplosion>();
                shell.battleParam = battleParam;
                Rigidbody shellRigidbody = shellObject.GetComponent<Rigidbody>();
                shellRigidbody.velocity = (battleParam.shellSpeed + playerRigidbody.velocity.magnitude) * shootPointTransforms[i].forward;
            }
        }
    }
}

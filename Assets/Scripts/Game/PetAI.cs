﻿using UnityEngine;
using System.Collections;

public class PetAI : WeMonoBehaviour
{
    public Transform[] mainRotorTransforms;

    public float maxRotorVelocity = 7200;			// degrees per second
    private float rotorRotation = 0.0f; 			// degrees... used for animating rotors

    public float height = 5.0f;
    public float moveSmoothTime = 0.3f;
    public float rotateSmoothTime = 0.3f;
    public float xOffset = 13.0f;
    public float zOffset = 13.0f;

    Transform m_TargetTransform = null;
    public Transform targetTransform
    {
        get
        {
            return m_TargetTransform;
        }
        set
        {
            m_TargetTransform = value;
        }
    }

    Vector3 m_TargetVector = Vector3.zero;
    float m_Angle = 0.0f;
    Quaternion m_DeltaRotation = Quaternion.identity;

    RegionManager m_RegionMgr;

    public void Initialize()
    {
        m_RegionMgr = RegionManager.instance;
        m_TargetTransform = m_RegionMgr.playerTransform;
    }

    void Update()
    {
        for(int i = 0; i < mainRotorTransforms.Length; i++)
        {
            mainRotorTransforms[i].rotation = m_Tr.rotation * Quaternion.Euler(0, rotorRotation, 0);
        }
        rotorRotation += maxRotorVelocity * Time.deltaTime;

        //if (m_State == State.Appear)
        //{
        m_Tr.position = new Vector3(Mathf.Lerp(m_Tr.position.x, m_TargetTransform.position.x + xOffset, Time.deltaTime * moveSmoothTime),
            Mathf.Lerp(m_Tr.position.y, height, Time.deltaTime * moveSmoothTime),
            Mathf.Lerp(m_Tr.position.z, m_TargetTransform.position.z + zOffset, Time.deltaTime * moveSmoothTime));

        m_TargetVector = m_Tr.InverseTransformPoint(m_TargetTransform.position);
        
        m_Angle = Mathf.Atan2(m_TargetVector.x, m_TargetVector.z) * Mathf.Rad2Deg;

        Vector3 eulerAngleVelocity = new Vector3(0, m_Angle, 0);
        m_DeltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime * rotateSmoothTime);
        m_Tr.rotation = m_Tr.rotation * m_DeltaRotation;
        //}
    }
}

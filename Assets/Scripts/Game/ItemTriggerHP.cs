﻿using UnityEngine;
using System.Collections;

public class ItemTriggerHP : ItemTrigger
{
    public Animator animator;
    public BoxCollider triggerCollider;

    int m_RecoveryHP = 0;
    public int recoveryHP
    {
        set
        {
            m_RecoveryHP = value;
        }
    }

    public override void SetItem(Transform parentItemPoint)
    {
        base.SetItem(parentItemPoint);

        // 기획에 따라 밸랜싱을 ItemManager에서 관리하고, 그 수치를 얻어오게 될수도 있다.
        // 현재는 아이템이 한종류만 있기 때문에 EtcTable에서 정보를 얻어온다.(25)
        m_RecoveryHP = EtcDocs.instance.itemRecoveryHP;

        triggerCollider.enabled = true;

        animator.Play("HPItem_Idle");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles"))
        {
            CollisionConnector collisionConnector = other.gameObject.GetComponent<CollisionConnector>();
            if (collisionConnector != null)
            {
                GameParameter otherGameParameter = collisionConnector.gameParameter;
                if (otherGameParameter != null)
                {
                    otherGameParameter.currentHealth += m_RecoveryHP;
                    
                    if(otherGameParameter.currentHealth > otherGameParameter.maxHealth)
                    {
                        otherGameParameter.currentHealth = otherGameParameter.maxHealth;
                    }

                    if(collisionConnector.recoveryHPParticle != null)
                    {
#if BWQUEST
                        QuestManager.instance.CheckQuestData(QuestData.Type.HPItem);
#endif
                        SoundManager.instance.PlaySFX("Healing");
                        collisionConnector.recoveryHPParticle.Play();
                    }
                }
            }

            StartCoroutine(GetItem());
        }
    }

    IEnumerator GetItem()
    {
        triggerCollider.enabled = false;

        animator.Play("HPItem_Get");

        yield return new WaitForSeconds(0.8f);

        gameObject.SetActive(false);
    }
}

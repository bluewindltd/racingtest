﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Region : WeMonoBehaviour
{
    public GameObject deliveryMissionDestinationObject;
    public DeliveryMissionDestination deliveryMissionDestination;

    RegionOutput m_RegionOutput = new RegionOutput();
    Road m_Road = null;
    public Road road
    {
        get
        {
            return m_Road;
        }
    }
    List<Ground> m_GroundList = new List<Ground>();

    int[] m_EnableDirection = new int[] { 0, 0, 0, 0 };
    List<GameConfig.RegionType> m_EnableRegionTypeList = new List<GameConfig.RegionType>();

    GameConfig.RegionType m_RegionType;

    int m_RegionX;
    public int regionX
    {
        get
        {
            return m_RegionX;
        }
    }
    int m_RegionZ;
    public int regionZ
    {
        get
        {
            return m_RegionZ;
        }
    }

    int m_NecessaryDirection;

    Transform m_PoliceLinePoint;
    PoliceLineGroup m_PoliceLineGroup;

    List<Transform> m_MissionPointList = new List<Transform>();

    List<MissionTrigger> m_MissionTriggerList = new List<MissionTrigger>();
    public List<MissionTrigger> missionTriggerList
    {
        get
        {
            return m_MissionTriggerList;
        }
    }

    List<Transform> m_ItemPointList = new List<Transform>();

    List<ItemTrigger> m_ItemTriggerList = new List<ItemTrigger>();
    public List<ItemTrigger> itemTriggerList
    {
        get
        {
            return m_ItemTriggerList;
        }
    }

    float m_MakeTime = 0.0f;
    public float makeTime
    {
        get
        {
            return m_MakeTime;
        }
        set
        {
            m_MakeTime = value;
        }
    }

    MissionManager m_MissionMgr;
    ItemManager m_ItemMgr;

    protected override void Awake()
    {
        base.Awake();

        m_MissionMgr = MissionManager.instance;
        m_ItemMgr = ItemManager.instance;
    }

    // first : 첫번째로 생성되는 타일은 항상 4차선 도로로 생성하기 위한 파라미터
    // firstUp : 첫번째에서 위에 지형은 연출 카메라에 안가리기 위한 지형으로 생성하기 위한 파라미터
    /*
    public void Initialize(bool first, bool firstUp, int x, int z, Vector3 newPos, int theme, bool isMakePoliceLine)
    {
        m_MakeTime = TimeManager.instance.currentGameTime;

        m_RegionOutput.outputDirList.Clear();

        m_RegionX = x;
        m_RegionZ = z;

        m_Tr.position = newPos;

        //Debug.Log("Region : " + m_RegionX + ", " + m_RegionZ);

        m_NecessaryDirection = 0;
        RegionManager regionMgr = RegionManager.instance;
        RegionInfo regionInfo;
        if (first)
        {
            m_RegionType = GameConfig.RegionType.NESW_Intersection;
            //Debug.Log("Region RegionType : " + m_RegionType);
            m_RegionOutput.SetRegionType(m_RegionType);
            regionInfo = regionMgr.GetRegionInfo(m_RegionType);
        }
        else if (firstUp)
        {
            m_RegionType = GameConfig.RegionType.V_Straight;
            //Debug.Log("Region RegionType : " + m_RegionType);
            m_RegionOutput.SetRegionType(m_RegionType);
            regionInfo = regionMgr.GetRegionInfo(m_RegionType);
        }
        else
        {
            m_EnableRegionTypeList.Clear();
            if (CheckRoad(RegionOutput.Direction.N))
            {
                m_EnableDirection[(int)GameConfig.Direction.North] = (int)RegionOutput.Direction.N;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.North] = 0;
            }
            if (CheckRoad(RegionOutput.Direction.E))
            {
                m_EnableDirection[(int)GameConfig.Direction.East] = (int)RegionOutput.Direction.E;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.East] = 0;
            }
            if (CheckRoad(RegionOutput.Direction.S))
            {
                m_EnableDirection[(int)GameConfig.Direction.South] = (int)RegionOutput.Direction.S;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.South] = 0;
            }
            if (CheckRoad(RegionOutput.Direction.W))
            {
                m_EnableDirection[(int)GameConfig.Direction.West] = (int)RegionOutput.Direction.W;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.West] = 0;
            }

            if (m_NecessaryDirection != 0 && IsEnableDirection(m_NecessaryDirection))
            {
                m_EnableRegionTypeList.Add((GameConfig.RegionType)m_NecessaryDirection);
            }
            for (int i = 0; i < 4; i++)
            {
                if (m_EnableDirection[i] != 0 && IsEnableDirection(m_NecessaryDirection + m_EnableDirection[i]))
                {
                    m_EnableRegionTypeList.Add((GameConfig.RegionType)m_NecessaryDirection + m_EnableDirection[i]);
                }
            }

            int emptyEnableDirection = m_EnableDirection[0] + m_EnableDirection[1] + m_EnableDirection[2] + m_EnableDirection[3];
            List<GameConfig.RegionType> emptyRegionTypeList = regionMgr.GetRegionTypeList(emptyEnableDirection);
            if (emptyRegionTypeList != null)
            {
                //Debug.Log("emptyRegionTypeList Count : " + m_EnableRegionTypeList.Count + ", " + emptyRegionTypeList.Count);
                for (int i = 0; i < emptyRegionTypeList.Count; i++)
                {
                    m_EnableRegionTypeList.Add(emptyRegionTypeList[i] + m_NecessaryDirection);
                }
                //Debug.Log("emptyRegionTypeList Count : " + m_EnableRegionTypeList.Count + ", " + emptyRegionTypeList.Count);
            }

            if (m_EnableRegionTypeList.Count > 0)
            {
                int regionTypeIndex = Random.Range(0, m_EnableRegionTypeList.Count);
                m_RegionType = m_EnableRegionTypeList[regionTypeIndex];
            }
            else
            {
                m_RegionType = GameConfig.RegionType.V_Straight;
            }

            //Debug.Log("Region RegionType : " + m_RegionType + ", " + m_NecessaryDirection + ", " + m_EnableDirection[0] + ", " + m_EnableDirection[1] + ", " + m_EnableDirection[2] + ", " + m_EnableDirection[3]);
            m_RegionOutput.SetRegionType(m_RegionType);
            regionInfo = regionMgr.GetRegionInfo(m_RegionType);
        }

        //Debug.Log("Region Road : " + regionInfo.m_RoadInfo.m_RoadType + ", " + regionInfo.m_RoadInfo.m_Direction);
        ObjectPool roadObjectPool = regionMgr.GetRoadObjectPool(regionInfo.m_RoadInfo.m_RoadType);
        GameObject roadObject = roadObjectPool.GetObjectFromPool();
        m_Road = roadObject.GetComponent<Road>();
        m_Road.SetRoad(m_Tr, regionInfo.m_RoadInfo.m_RoadType, regionInfo.m_RoadInfo.m_Direction);

        //// 바리게이트 생성
        if (!first && !firstUp && isMakePoliceLine)
        {
            m_PoliceLineGroup = PoliceLineManager.instance.GeneratePoliceLineGroup();
            if (m_PoliceLineGroup != null)
            {
                int policeLineIndex = UnityEngine.Random.Range(0, m_Road.policeLineSpawnPointList.Count);
                m_PoliceLinePoint = m_Road.policeLineSpawnPointList[policeLineIndex];
                m_PoliceLineGroup.SetPoliceLineGroup(m_PoliceLinePoint);

                //Debug.Log("PoliceLine Add : " + m_PoliceLineGroup.gameObject.name + ", " + m_PoliceLinePoint.gameObject.name);
            }
        }

        m_MissionPointList.Clear();
        m_ItemPointList.Clear();
        m_GroundList.Clear();
        for (int i = 0; i < regionInfo.m_GroundInfoList.Count; i++)
        {
            GroundInfo groundInfo = regionInfo.m_GroundInfoList[i];
            //ObjectPool groundObjectPool = regionMgr.GetGroundObjectPool(groundInfo.m_GroundType);
            //GameObject groundObject = groundObjectPool.GetObjectFromPool();
            //Ground ground = groundObject.GetComponent<Ground>();
            //ground.SetGround(gameObject.transform, groundInfo.m_GroundType, groundInfo.m_Direction, theme);
            //m_GroundList.Add(ground);

            Ground ground = null;
            bool tempFirstUp = false;
            if (first)
            {
                ground = GroundManager.instance.GetIntroGround(theme, i);
            }
            else if (firstUp && i == 1)      // 인트로 연출 카메라 시야에 가리는 지형만 예외처리
            {
                ground = GroundManager.instance.GetIntroUpGround(theme);
                tempFirstUp = true;
            }
            else
            {
                ground = GroundManager.instance.GetGround(theme, groundInfo.m_GroundType);
            }

            ground.SetGround(m_Tr, theme, groundInfo.m_GroundType, groundInfo.m_Direction, first, tempFirstUp);
            m_GroundList.Add(ground);

            for (int j = 0; j < ground.missionPointList.Count; j++)
            {
                m_MissionPointList.Add(ground.missionPointList[j]);
            }

            for (int j = 0; j < ground.itemPointList.Count; j++)
            {
                m_ItemPointList.Add(ground.itemPointList[j]);
            }
        }

        m_MissionTriggerList.Clear();
        MissionTrigger missionTrigger = null;
        for (int i = 0; i < 2; i++)
        {
            if (m_MissionPointList.Count > 0)
            {
                int missionPointIndex = UnityEngine.Random.Range(0, m_MissionPointList.Count);
                missionTrigger = m_MissionMgr.GenerateMission();
                if (missionTrigger != null)
                {
                    missionTrigger.SetMission(m_MissionPointList[missionPointIndex]);
                    m_MissionTriggerList.Add(missionTrigger);
                }

                m_MissionPointList.RemoveAt(missionPointIndex);
            }
        }

        if (m_MissionMgr.missionState == MissionManager.MissionState.Progress)
        {
            if (m_MissionMgr.progressMissionInfo != null)
            {
                if (m_MissionMgr.progressMissionInfo.missionData.type == GameConfig.MissionType.Delivery)
                {
                    if (x == m_MissionMgr.progressMissionInfo.destinationGrid.x &&
                       z == m_MissionMgr.progressMissionInfo.destinationGrid.z)
                    {
                        deliveryMissionDestinationObject.SetActive(true);
                        deliveryMissionDestination.ActiveDestination();
                    }
                }
            }
        }

        m_ItemTriggerList.Clear();
        ItemTrigger itemTrigger = null;
        for (int i = 0; i < 1; i++)
        {
            if (m_ItemPointList.Count > 0)
            {
                int itemPointIndex = UnityEngine.Random.Range(0, m_ItemPointList.Count);
                itemTrigger = m_ItemMgr.GenerateItem();
                if (itemTrigger != null)
                {
                    itemTrigger.SetItem(m_ItemPointList[itemPointIndex]);
                    m_ItemTriggerList.Add(itemTrigger);
                }

                m_ItemPointList.RemoveAt(itemPointIndex);
            }
        }
    }
    */
    // 코루틴 사용 버전
    public IEnumerator Initialize(bool first, bool firstUp, int x, int z, Vector3 newPos, int theme, bool isMakePoliceLine)
    {
        yield return new WaitForEndOfFrame();

        m_MakeTime = TimeManager.instance.currentGameTime;

        m_RegionOutput.outputDirList.Clear();

        m_RegionX = x;
        m_RegionZ = z;

        m_Tr.position = newPos;

        //Debug.Log("Region : " + m_RegionX + ", " + m_RegionZ);

        m_NecessaryDirection = 0;
        RegionManager regionMgr = RegionManager.instance;
        RegionInfo regionInfo;
        if (first)
        {
            m_RegionType = GameConfig.RegionType.NESW_Intersection;
            //Debug.Log("Region RegionType : " + m_RegionType);
            m_RegionOutput.SetRegionType(m_RegionType);
            regionInfo = regionMgr.GetRegionInfo(m_RegionType);
        }
        else if(firstUp)
        {
            m_RegionType = GameConfig.RegionType.V_Straight;
            //Debug.Log("Region RegionType : " + m_RegionType);
            m_RegionOutput.SetRegionType(m_RegionType);
            regionInfo = regionMgr.GetRegionInfo(m_RegionType);
        }
        else
        {
            m_EnableRegionTypeList.Clear();
            if (CheckRoad(RegionOutput.Direction.N))
            {
                m_EnableDirection[(int)GameConfig.Direction.North] = (int)RegionOutput.Direction.N;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.North] = 0;
            }
            if (CheckRoad(RegionOutput.Direction.E))
            {
                m_EnableDirection[(int)GameConfig.Direction.East] = (int)RegionOutput.Direction.E;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.East] = 0;
            }
            if (CheckRoad(RegionOutput.Direction.S))
            {
                m_EnableDirection[(int)GameConfig.Direction.South] = (int)RegionOutput.Direction.S;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.South] = 0;
            }
            if (CheckRoad(RegionOutput.Direction.W))
            {
                m_EnableDirection[(int)GameConfig.Direction.West] = (int)RegionOutput.Direction.W;
            }
            else
            {
                m_EnableDirection[(int)GameConfig.Direction.West] = 0;
            }

            if (m_NecessaryDirection != 0 && IsEnableDirection(m_NecessaryDirection))
            {
                m_EnableRegionTypeList.Add((GameConfig.RegionType)m_NecessaryDirection);
            }
            for (int i = 0; i < 4; i++)
            {
                if (m_EnableDirection[i] != 0 && IsEnableDirection(m_NecessaryDirection + m_EnableDirection[i]))
                {
                    m_EnableRegionTypeList.Add((GameConfig.RegionType)m_NecessaryDirection + m_EnableDirection[i]);
                }
            }

            int emptyEnableDirection = m_EnableDirection[0] + m_EnableDirection[1] + m_EnableDirection[2] + m_EnableDirection[3];
            List<GameConfig.RegionType> emptyRegionTypeList = regionMgr.GetRegionTypeList(emptyEnableDirection);
            if (emptyRegionTypeList != null)
            {
                //Debug.Log("emptyRegionTypeList Count : " + m_EnableRegionTypeList.Count + ", " + emptyRegionTypeList.Count);
                for (int i = 0; i < emptyRegionTypeList.Count; i++)
                {
                    m_EnableRegionTypeList.Add(emptyRegionTypeList[i] + m_NecessaryDirection);
                }
                //Debug.Log("emptyRegionTypeList Count : " + m_EnableRegionTypeList.Count + ", " + emptyRegionTypeList.Count);
            }

            if (m_EnableRegionTypeList.Count > 0)
            {
                int regionTypeIndex = Random.Range(0, m_EnableRegionTypeList.Count);
                m_RegionType = m_EnableRegionTypeList[regionTypeIndex];
            }
            else
            {
                m_RegionType = GameConfig.RegionType.V_Straight;
            }

            //Debug.Log("Region RegionType : " + m_RegionType + ", " + m_NecessaryDirection + ", " + m_EnableDirection[0] + ", " + m_EnableDirection[1] + ", " + m_EnableDirection[2] + ", " + m_EnableDirection[3]);
            m_RegionOutput.SetRegionType(m_RegionType);
            regionInfo = regionMgr.GetRegionInfo(m_RegionType);
        }

        //Debug.Log("Region Road : " + regionInfo.m_RoadInfo.m_RoadType + ", " + regionInfo.m_RoadInfo.m_Direction);
        ObjectPool roadObjectPool = regionMgr.GetRoadObjectPool(regionInfo.m_RoadInfo.m_RoadType);
        GameObject roadObject = roadObjectPool.GetObjectFromPool();
        m_Road = roadObject.GetComponent<Road>();
        m_Road.SetRoad(m_Tr, regionInfo.m_RoadInfo.m_RoadType, regionInfo.m_RoadInfo.m_Direction);

        if (!first && !firstUp)
        {
            yield return new WaitForEndOfFrame();
        }            

        if(!first && !firstUp && isMakePoliceLine)
        {
            m_PoliceLineGroup = PoliceLineManager.instance.GeneratePoliceLineGroup();
            if(m_PoliceLineGroup != null)
            {
                int policeLineIndex = UnityEngine.Random.Range(0, m_Road.policeLineSpawnPointList.Count);
                m_PoliceLinePoint = m_Road.policeLineSpawnPointList[policeLineIndex];
                m_PoliceLineGroup.SetPoliceLineGroup(m_PoliceLinePoint);

                //Debug.Log("PoliceLine Add : " + m_PoliceLineGroup.gameObject.name + ", " + m_PoliceLinePoint.gameObject.name);
            }            
        }

        yield return new WaitForEndOfFrame();

        m_MissionPointList.Clear();
        m_ItemPointList.Clear();
        m_GroundList.Clear();
        for (int i = 0; i < regionInfo.m_GroundInfoList.Count; i++)
        {
            GroundInfo groundInfo = regionInfo.m_GroundInfoList[i];
            //ObjectPool groundObjectPool = regionMgr.GetGroundObjectPool(groundInfo.m_GroundType);
            //GameObject groundObject = groundObjectPool.GetObjectFromPool();
            //Ground ground = groundObject.GetComponent<Ground>();
            //ground.SetGround(gameObject.transform, groundInfo.m_GroundType, groundInfo.m_Direction, theme);
            //m_GroundList.Add(ground);

            Ground ground = null;
            bool tempFirstUp = false;
            if (first)
            {
                ground = GroundManager.instance.GetIntroGround(theme, i);
            }
            else if(firstUp && i == 1)      // 인트로 연출 카메라 시야에 가리는 지형만 예외처리
            {
                ground = GroundManager.instance.GetIntroUpGround(theme);
                tempFirstUp = true;
            }
            else
            {
                ground = GroundManager.instance.GetGround(theme, groundInfo.m_GroundType);
            }

            ground.SetGround(m_Tr, theme, groundInfo.m_GroundType, groundInfo.m_Direction, first, tempFirstUp);
            m_GroundList.Add(ground);

            for (int j = 0; j < ground.missionPointList.Count; j++)
            {
                m_MissionPointList.Add(ground.missionPointList[j]);
            }

            for (int j = 0; j < ground.itemPointList.Count; j++)
            {
                m_ItemPointList.Add(ground.itemPointList[j]);
            }

            if(!first && !firstUp)
            {
                yield return new WaitForEndOfFrame();
            }            
        }

        m_MissionTriggerList.Clear();
        MissionTrigger missionTrigger = null;
        for (int i = 0; i < 2; i++)
        {
            if (m_MissionPointList.Count > 0)
            {
                int missionPointIndex = UnityEngine.Random.Range(0, m_MissionPointList.Count);
                missionTrigger = m_MissionMgr.GenerateMission();
                if (missionTrigger != null)
                {
                    missionTrigger.SetMission(m_MissionPointList[missionPointIndex]);
                    m_MissionTriggerList.Add(missionTrigger);
                }

                m_MissionPointList.RemoveAt(missionPointIndex);
            }
        }

        if (m_MissionMgr.missionState == MissionManager.MissionState.Progress)
        {
            if (m_MissionMgr.progressMissionInfo != null)
            {
                if (m_MissionMgr.progressMissionInfo.missionData.type == GameConfig.MissionType.Delivery)
                {
                    if (x == m_MissionMgr.progressMissionInfo.destinationGrid.x &&
                        z == m_MissionMgr.progressMissionInfo.destinationGrid.z)
                    {
                        deliveryMissionDestinationObject.SetActive(true);
                        deliveryMissionDestination.ActiveDestination();
                    }
                }
            }
        }

        yield return new WaitForEndOfFrame();

        m_ItemTriggerList.Clear();
        ItemTrigger itemTrigger = null;
        for (int i = 0; i < 1; i++)
        {
            if (m_ItemPointList.Count > 0)
            {
                int itemPointIndex = UnityEngine.Random.Range(0, m_ItemPointList.Count);
                itemTrigger = m_ItemMgr.GenerateItem();
                if (itemTrigger != null)
                {
                    itemTrigger.SetItem(m_ItemPointList[itemPointIndex]);
                    m_ItemTriggerList.Add(itemTrigger);
                }

                m_ItemPointList.RemoveAt(itemPointIndex);
            }
        }

        yield return new WaitForEndOfFrame();
    }

    //public void DisableDestionationObject()
    //{
    //    deliveryMissionDestinationObject.SetActive(false);
    //}

    public void SpawnNPCCar(GameConfig.Direction direction)
    {

    }

    bool IsEnableDirection(int direction)
    {
        if (direction != 2 && direction != 4 && direction != 8 && direction != 16)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Restore()
    {
        //Debug.Log("Region Restore : " + gameObject.name);

        m_RegionOutput.Restore();
        if (m_Road != null)
        {
            m_Road.Restore();
            m_Road = null;
        }
        for (int i = 0; i < m_GroundList.Count; i++)
        {
            m_GroundList[i].Restore();
        }
        m_GroundList.Clear();

        if (m_PoliceLineGroup != null)
        {
            m_PoliceLineGroup.Restore();
            m_PoliceLineGroup = null;
        }        

        for (int i = 0; i < m_MissionTriggerList.Count; i++)
        {
            m_MissionTriggerList[i].Restore();
        }
        m_MissionTriggerList.Clear();

        for (int i = 0; i < m_ItemTriggerList.Count; i++)
        {
            m_ItemTriggerList[i].Restore();
        }
        m_ItemTriggerList.Clear();

        deliveryMissionDestination.DeactiveDestination();
        deliveryMissionDestinationObject.SetActive(false);

        //GameObject childObject = null;
        //for (int i = m_Tr.childCount - 1; i >= 0; --i)
        //{
        //    childObject = m_Tr.GetChild(i).gameObject;
        //    if(childObject.name.CompareTo("Tire Mark") == 0)
        //    {
        //        GameObject.Destroy(childObject);
        //    }            
        //}
    }

    bool CheckRoad(RegionOutput.Direction checkDirection)
    {
        RegionManager regionMgr = RegionManager.instance;
        int neighborhoodX = m_RegionX + RegionOutput.dx[(int)checkDirection];
        int neighborhoodZ = m_RegionZ + RegionOutput.dz[(int)checkDirection];
        Region neighborhoodRegion = regionMgr.GetRegion(neighborhoodX, neighborhoodZ);
        if (neighborhoodRegion != null)
        {
            RegionOutput.Direction oppositeDirection = RegionOutput.opposite[(int)checkDirection];
            if(neighborhoodRegion.IsExistRoad(oppositeDirection))
            {
                m_NecessaryDirection += (int)checkDirection;
            }
            return false;
        }
        else
        {
            return true;
        }        
    }

    public bool IsExistRoad(RegionOutput.Direction direction)
    {
        int index = m_RegionOutput.outputDirList.FindIndex(x => x == direction);
        if(index != -1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ActiveDeliveryMissionDestination()
    {
        deliveryMissionDestinationObject.SetActive(true);
        deliveryMissionDestination.ActiveDestination();
    }
}

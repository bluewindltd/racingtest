﻿using UnityEngine;
using System.Collections;

public class ShrinkAttackAI : WeMonoBehaviour
{
    public BattleParameter battleParam;
    public Renderer bodyRenderer;
    public Color origColor;
    public Color destColor;
    public BoxCollider attackCollider;
    public Animator animator;

    Coroutine m_UpdateCoroutine = null;
    Coroutine m_CollectPowerCoroutine = null;
    float m_Timer = 0.0f;

    SoundManager m_SoundMgr;

    void Start()
    {
        m_SoundMgr = SoundManager.instance;
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        CollisionConnector collisionConnector = other.gameObject.GetComponent<CollisionConnector>();
        if (collisionConnector != null)
        {
            if (!collisionConnector.gameParameter.shrink)
            {
                collisionConnector.gameParameter.shrink = true;
                collisionConnector.gameParameter.transform.localScale = new Vector3(battleParam.shellSpeed, battleParam.shellSpeed, battleParam.shellSpeed);
            }
        }
    }

    public void StopUpdate()
    {
        if (m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            if (m_CollectPowerCoroutine == null)
            {
                m_CollectPowerCoroutine = StartCoroutine(CollectPower());
            }

            yield return new WaitForSeconds(battleParam.attackDelay);

            animator.Play("Effect_Poke_Laser_Shoot");

            if (m_CollectPowerCoroutine != null)
            {
                StopCoroutine(m_CollectPowerCoroutine);
                m_CollectPowerCoroutine = null;
            }

            yield return new WaitForSeconds(battleParam.rotorSpeed);

            animator.Play("Effect_Poke_Laser_Close");
        }
    }

    IEnumerator CollectPower()
    {
        while (true)
        {
            m_Timer += Time.deltaTime;

            if (m_Timer > battleParam.attackDelay)
            {
                m_Timer = 0.0f;
                bodyRenderer.material.color = origColor;
                m_CollectPowerCoroutine = null;
                break;
            }

            Color colorTemp = Color.Lerp(origColor, destColor, m_Timer / battleParam.attackDelay);
            bodyRenderer.material.color = colorTemp;
            //bodyRenderer.sharedMaterial.SetColor("Color (A)Opacity", colorTemp);

            yield return null;
        }
    }

    public void EnableCollider()
    {
        attackCollider.enabled = true;
    }

    public void DisableCollider()
    {
        attackCollider.enabled = false;
    }
}

﻿using UnityEngine;
using System.Collections;

public class BackwardButton : MonoBehaviour
{
    public GameManager gameManager;
    TestMobileInput m_TestMobileInput = null;

    void OnPress(bool isPressed)
    {
        //Debug.Log("OnPress : " + isPressed);

        if (m_TestMobileInput == null)
        {
            m_TestMobileInput = gameManager.GetTestMobileInput();
        }
        if (isPressed)
        {
            if (!gameManager.hideInputHelper)
            {
                gameManager.HideInputHelperObject();
            }
            m_TestMobileInput.StartBackward();
        }
        else
        {
            m_TestMobileInput.StopBackward();
        }
    }
}
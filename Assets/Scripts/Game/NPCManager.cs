﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;

[Serializable]
public class NPCCarCreator
{
    [HideInInspector][SerializeField] public GameObject npcCarPrefab;
    [HideInInspector][SerializeField] public int objectPoolCount;
    [HideInInspector] public ObjectPool objectPool;
}

[Serializable] public class DictionaryOfNPCCarCreator : SerializableDictionary<int, NPCCarCreator> { }

public class NPCSpawnInfo
{
    public Vector2 m_GridPos;
    public RegionOutput.Direction m_Direction;
}

public class NPCCarUseInfo
{
    public int useCount;
    public int maxCount;
}

[ExecuteInEditMode]
public class NPCManager : Singleton<NPCManager>
{
    [HideInInspector][SerializeField] DictionaryOfNPCCarCreator m_NPCCarCreatorDic = new DictionaryOfNPCCarCreator();
    public DictionaryOfNPCCarCreator npcCarCreatorDic
    {
        get
        {
            return m_NPCCarCreatorDic;
        }
        set
        {
            m_NPCCarCreatorDic = value;
        }
    }

    Dictionary<int, NPCCarUseInfo> m_NPCCarUseInfoDic = new Dictionary<int, NPCCarUseInfo>();
    List<int> m_RandomIndex = new List<int>();

    Dictionary<int, NPCAI> m_NPCCarDic = new Dictionary<int, NPCAI>();

    public GameObject npcCarPrefab;

    int m_CenterX = -99999;
    int m_CenterZ = -99999;

    List<NPCSpawnInfo> m_NpcSpawnInfoList = new List<NPCSpawnInfo>();

    bool m_Initialize = false;

    int m_NPCCarCount = 0;

    GameManager m_GameMgr = null;

    //Coroutine m_UpdateCoroutine = null;

    void Awake()
    {
        if (Application.isPlaying)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void Initialize()
    {
        DictionaryOfNPCCarCreator.Enumerator etor = m_NPCCarCreatorDic.GetEnumerator();
        while (etor.MoveNext())
        {
            NPCCarCreator npcCarCreator = etor.Current.Value;
            if (npcCarCreator.objectPool == null)
            {
                npcCarCreator.objectPool = new ObjectPool(npcCarCreator.objectPoolCount, npcCarCreator.npcCarPrefab, transform);
            }

            NPCCarUseInfo npcCarUseInfo = new NPCCarUseInfo();
            npcCarUseInfo.useCount = 0;
            npcCarUseInfo.maxCount = npcCarCreator.objectPoolCount;
            m_NPCCarUseInfoDic.Add(etor.Current.Key, npcCarUseInfo);
        }
    }

    public void Restore()
    {
        m_Initialize = false;

        NPCAI npcAI = null;
        NPCCarCreator npcCarCreator = null;
        NPCCarUseInfo npcCarUseInfo = null;
        GameObject npcCarObject = null;
        int index = 0;        
        Dictionary<int, NPCAI>.Enumerator etor = m_NPCCarDic.GetEnumerator();
        while(etor.MoveNext())
        {
            npcAI = etor.Current.Value;

            npcCarObject = npcAI.gameObject;
            index = npcAI.index;

            npcCarCreator = m_NPCCarCreatorDic[index];
            npcCarCreator.objectPool.PutObjectInPool(npcCarObject);

            npcCarUseInfo = m_NPCCarUseInfoDic[index];
            npcCarUseInfo.useCount--;

            m_NPCCarCount--;
        }

        m_NPCCarDic.Clear();

        //if(m_UpdateCoroutine != null)
        //{
        //    StopCoroutine(m_UpdateCoroutine);
        //    m_UpdateCoroutine = null;
        //}
    }

    public void StartUpdate()
    {
        m_Initialize = true;

        m_GameMgr = GameManager.instance;

        //if(m_UpdateCoroutine == null)
        //{
        //    m_UpdateCoroutine = StartCoroutine(UpdateNPC());
        //}        
    }

    public IEnumerator UpdateNPC()
    {
        if (m_Initialize && m_GameMgr.state == GameManager.State.Playing)
        {
            if (m_NPCCarCount < 7)
            {
                BuildSpawnPoint();

                for (int i = 0; i < m_NpcSpawnInfoList.Count; i++)
                {
                    NPCSpawnInfo npcSpawnInfo = m_NpcSpawnInfoList[i];
                    Region region = RegionManager.instance.GetRegion((int)npcSpawnInfo.m_GridPos.x, (int)npcSpawnInfo.m_GridPos.y);
                    if (region != null)
                    {
                        List<Transform> spawnPointList = null;
                        //if (npcSpawnInfo.m_Direction == RegionOutput.Direction.N &&
                        //    region.road.roadType == GameConfig.RoadType.InterSection_3 &&
                        //    region.road.direction == GameConfig.Direction.West)
                        //{
                        //    int aa = 0;
                        //    aa = 10;
                        //}

                        //if (npcSpawnInfo.m_Direction == RegionOutput.Direction.S &&
                        //    region.road.roadType == GameConfig.RoadType.InterSection_3 &&
                        //    region.road.direction == GameConfig.Direction.East)
                        //{
                        //    int aa = 0;
                        //    aa = 10;
                        //}

                        //if (npcSpawnInfo.m_Direction == RegionOutput.Direction.N &&
                        //    region.road.roadType == GameConfig.RoadType.Straight)
                        //{
                        //    int aa = 0;
                        //    aa = 10;
                        //}

                        RegionOutput.Direction direction = npcSpawnInfo.m_Direction;
                        switch (region.road.direction)
                        {
                            case GameConfig.Direction.East:
                                {
                                    direction = RegionOutput.rotateCCW[(int)direction];
                                }
                                break;

                            case GameConfig.Direction.South:
                                {
                                    direction = RegionOutput.opposite[(int)direction];
                                }
                                break;

                            case GameConfig.Direction.West:
                                {
                                    direction = RegionOutput.rotateCW[(int)direction];
                                }
                                break;
                        }

                        switch (direction)
                        {
                            case RegionOutput.Direction.E:
                                {
                                    if (!region.road.npcCarSpawnDic[(int)RegionOutput.Direction.E])
                                    {
                                        spawnPointList = region.road.eastNPCSpawnPointList;
                                        region.road.npcCarSpawnDic[(int)RegionOutput.Direction.E] = true;
                                    }
                                }
                                break;
                            case RegionOutput.Direction.W:
                                {
                                    if (!region.road.npcCarSpawnDic[(int)RegionOutput.Direction.W])
                                    {
                                        spawnPointList = region.road.westNPCSpawnPointList;
                                        region.road.npcCarSpawnDic[(int)RegionOutput.Direction.W] = true;
                                    }
                                }
                                break;
                            case RegionOutput.Direction.S:
                                {
                                    if (!region.road.npcCarSpawnDic[(int)RegionOutput.Direction.S])
                                    {
                                        spawnPointList = region.road.southNPCSpawnPointList;
                                        region.road.npcCarSpawnDic[(int)RegionOutput.Direction.S] = true;
                                    }
                                }
                                break;
                            case RegionOutput.Direction.N:
                                {
                                    if (!region.road.npcCarSpawnDic[(int)RegionOutput.Direction.N])
                                    {
                                        spawnPointList = region.road.northNPCSpawnPointList;
                                        region.road.npcCarSpawnDic[(int)RegionOutput.Direction.N] = true;
                                    }
                                }
                                break;
                        }
                        if (spawnPointList != null)
                        {
                            for (int j = 0; j < spawnPointList.Count; j++)
                            {
                                int index = RandomNPCCarIndex();
                                if (index != -1)
                                {
                                    NPCCarCreator npcCarCreator = m_NPCCarCreatorDic[index];
                                    GameObject npcCar = npcCarCreator.objectPool.GetObjectFromPool();
                                    //npcCar.transform.position = spawnPointList[j].position;
                                    //npcCar.transform.rotation = spawnPointList[j].rotation;
                                    NPCAI npcAI = npcCar.GetComponent<NPCAI>();
                                    npcAI.spawnPos = spawnPointList[j].position;
                                    npcAI.spawnRot = spawnPointList[j].rotation;
                                    npcAI.npcCarID = GetNPCCarID();
                                    npcAI.index = index;
                                    npcAI.targetPosition = spawnPointList[j].position + ((spawnPointList[j].rotation * Vector3.forward) * 500.0f);
                                    npcAI.Initialize();
                                    m_NPCCarDic.Add(npcAI.npcCarID, npcAI);

                                    NPCCarUseInfo npcCarUseInfo = m_NPCCarUseInfoDic[index];
                                    npcCarUseInfo.useCount++;

                                    m_NPCCarCount++;
                                }

                                yield return new WaitForEndOfFrame();
                            }
                        }
                    }
                }
            }
        }

        yield return new WaitForEndOfFrame();
    }

    //IEnumerator UpdateNPC()
    //{
    //    while(true)
    //    {
    //        if (m_Initialize && m_GameMgr.state == GameManager.State.Playing)
    //        {
    //            if(m_NPCCarCount < 7)
    //            {
    //                BuildSpawnPoint();

    //                for (int i = 0; i < m_NpcSpawnInfoList.Count; i++)
    //                {
    //                    NPCSpawnInfo npcSpawnInfo = m_NpcSpawnInfoList[i];
    //                    Region region = RegionManager.instance.GetRegion((int)npcSpawnInfo.m_GridPos.x, (int)npcSpawnInfo.m_GridPos.y);
    //                    if (region != null)
    //                    {
    //                        List<Transform> spawnPointList = null;
    //                        //if (npcSpawnInfo.m_Direction == RegionOutput.Direction.N &&
    //                        //    region.road.roadType == GameConfig.RoadType.InterSection_3 &&
    //                        //    region.road.direction == GameConfig.Direction.West)
    //                        //{
    //                        //    int aa = 0;
    //                        //    aa = 10;
    //                        //}

    //                        //if (npcSpawnInfo.m_Direction == RegionOutput.Direction.S &&
    //                        //    region.road.roadType == GameConfig.RoadType.InterSection_3 &&
    //                        //    region.road.direction == GameConfig.Direction.East)
    //                        //{
    //                        //    int aa = 0;
    //                        //    aa = 10;
    //                        //}

    //                        //if (npcSpawnInfo.m_Direction == RegionOutput.Direction.N &&
    //                        //    region.road.roadType == GameConfig.RoadType.Straight)
    //                        //{
    //                        //    int aa = 0;
    //                        //    aa = 10;
    //                        //}

    //                        RegionOutput.Direction direction = npcSpawnInfo.m_Direction;
    //                        switch (region.road.direction)
    //                        {
    //                            case GameConfig.Direction.East:
    //                                {
    //                                    direction = RegionOutput.rotateCCW[(int)direction];
    //                                }
    //                                break;

    //                            case GameConfig.Direction.South:
    //                                {
    //                                    direction = RegionOutput.opposite[(int)direction];
    //                                }
    //                                break;

    //                            case GameConfig.Direction.West:
    //                                {
    //                                    direction = RegionOutput.rotateCW[(int)direction];
    //                                }
    //                                break;
    //                        }

    //                        switch (direction)
    //                        {
    //                            case RegionOutput.Direction.E:
    //                                {
    //                                    if (!region.road.npcCarSpawnDic[RegionOutput.Direction.E])
    //                                    {
    //                                        spawnPointList = region.road.eastNPCSpawnPointList;
    //                                        region.road.npcCarSpawnDic[RegionOutput.Direction.E] = true;
    //                                    }
    //                                }
    //                                break;
    //                            case RegionOutput.Direction.W:
    //                                {
    //                                    if (!region.road.npcCarSpawnDic[RegionOutput.Direction.W])
    //                                    {
    //                                        spawnPointList = region.road.westNPCSpawnPointList;
    //                                        region.road.npcCarSpawnDic[RegionOutput.Direction.W] = true;
    //                                    }
    //                                }
    //                                break;
    //                            case RegionOutput.Direction.S:
    //                                {
    //                                    if (!region.road.npcCarSpawnDic[RegionOutput.Direction.S])
    //                                    {
    //                                        spawnPointList = region.road.southNPCSpawnPointList;
    //                                        region.road.npcCarSpawnDic[RegionOutput.Direction.S] = true;
    //                                    }
    //                                }
    //                                break;
    //                            case RegionOutput.Direction.N:
    //                                {
    //                                    if (!region.road.npcCarSpawnDic[RegionOutput.Direction.N])
    //                                    {
    //                                        spawnPointList = region.road.northNPCSpawnPointList;
    //                                        region.road.npcCarSpawnDic[RegionOutput.Direction.N] = true;
    //                                    }
    //                                }
    //                                break;
    //                        }
    //                        if (spawnPointList != null)
    //                        {
    //                            for (int j = 0; j < spawnPointList.Count; j++)
    //                            {
    //                                //if(j == 0)
    //                                //{
    //                                    int index = RandomNPCCarIndex();
    //                                    if (index != -1)
    //                                    {
    //                                        NPCCarCreator npcCarCreator = m_NPCCarCreatorDic[index];
    //                                        GameObject npcCar = npcCarCreator.objectPool.GetObjectFromPool();
    //                                        //npcCar.transform.position = spawnPointList[j].position;
    //                                        //npcCar.transform.rotation = spawnPointList[j].rotation;
    //                                        NPCAI npcAI = npcCar.GetComponent<NPCAI>();
    //                                        npcAI.spawnPos = spawnPointList[j].position;
    //                                        npcAI.spawnRot = spawnPointList[j].rotation;
    //                                        npcAI.npcCarID = GetNPCCarID();
    //                                        npcAI.index = index;
    //                                        npcAI.targetPosition = spawnPointList[j].position + ((spawnPointList[j].rotation * Vector3.forward) * 500.0f);
    //                                        npcAI.Initialize();
    //                                        m_NPCCarDic.Add(npcAI.npcCarID, npcAI);

    //                                        NPCCarUseInfo npcCarUseInfo = m_NPCCarUseInfoDic[index];
    //                                        npcCarUseInfo.useCount++;

    //                                        m_NPCCarCount++;
    //                                    }
    //                                //}
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //        yield return new WaitForSeconds(0.5f);
    //    }        
    //}

    int RandomNPCCarIndex()
    {
        int index = -1;

        m_RandomIndex.Clear();
        Dictionary<int, NPCCarUseInfo>.Enumerator etor = m_NPCCarUseInfoDic.GetEnumerator();
        while(etor.MoveNext())
        {
            NPCCarUseInfo npcCarUseInfo = etor.Current.Value;
            if(npcCarUseInfo.useCount < npcCarUseInfo.maxCount)
            {
                m_RandomIndex.Add(etor.Current.Key);
            }
        }

        if (m_RandomIndex.Count > 0)
        {
            int randomIndex = UnityEngine.Random.Range(0, m_RandomIndex.Count);
            index = m_RandomIndex[randomIndex];
        }

        return index;
    }

    void BuildSpawnPoint()
    {
        int centerX = RegionManager.instance.centerX;
        int centerZ = RegionManager.instance.centerZ;

        m_NpcSpawnInfoList.Clear();

        if (m_CenterX != centerX || m_CenterZ != centerZ)
        {
            NPCSpawnInfo npcSpawnInfo1 = new NPCSpawnInfo();
            npcSpawnInfo1.m_GridPos = new Vector2(centerX - 1, centerZ);
            npcSpawnInfo1.m_Direction = RegionOutput.Direction.W;
            m_NpcSpawnInfoList.Add(npcSpawnInfo1);

            NPCSpawnInfo npcSpawnInfo2 = new NPCSpawnInfo();
            npcSpawnInfo2.m_GridPos = new Vector2(centerX + 1, centerZ);
            npcSpawnInfo2.m_Direction = RegionOutput.Direction.E;
            m_NpcSpawnInfoList.Add(npcSpawnInfo2);

            NPCSpawnInfo npcSpawnInfo3= new NPCSpawnInfo();
            npcSpawnInfo3.m_GridPos = new Vector2(centerX, centerZ - 1);
            npcSpawnInfo3.m_Direction = RegionOutput.Direction.S;
            m_NpcSpawnInfoList.Add(npcSpawnInfo3);

            NPCSpawnInfo npcSpawnInfo4 = new NPCSpawnInfo();
            npcSpawnInfo4.m_GridPos = new Vector2(centerX, centerZ + 1);
            npcSpawnInfo4.m_Direction = RegionOutput.Direction.N;
            m_NpcSpawnInfoList.Add(npcSpawnInfo4);

            //if (m_CenterX != centerX && m_CenterZ == centerZ)
            //{
            //    NPCSpawnInfo npcSpawnInfo1 = new NPCSpawnInfo();
            //    npcSpawnInfo1.m_GridPos = new Vector2(centerX - 1, centerZ);
            //    npcSpawnInfo1.m_Direction = RegionOutput.Direction.W;
            //    m_NpcSpawnInfoList.Add(npcSpawnInfo1);

            //    NPCSpawnInfo npcSpawnInfo2 = new NPCSpawnInfo();
            //    npcSpawnInfo2.m_GridPos = new Vector2(centerX + 1, centerZ);
            //    npcSpawnInfo2.m_Direction = RegionOutput.Direction.E;
            //    m_NpcSpawnInfoList.Add(npcSpawnInfo2);
            //}
            //else if (m_CenterX == centerX && m_CenterZ != centerZ)
            //{
            //    NPCSpawnInfo npcSpawnInfo1 = new NPCSpawnInfo();
            //    npcSpawnInfo1.m_GridPos = new Vector2(centerX, centerZ - 1);
            //    npcSpawnInfo1.m_Direction = RegionOutput.Direction.S;
            //    m_NpcSpawnInfoList.Add(npcSpawnInfo1);

            //    NPCSpawnInfo npcSpawnInfo2 = new NPCSpawnInfo();
            //    npcSpawnInfo2.m_GridPos = new Vector2(centerX, centerZ + 1);
            //    npcSpawnInfo2.m_Direction = RegionOutput.Direction.N;
            //    m_NpcSpawnInfoList.Add(npcSpawnInfo2);
            //}
            //else
            //{
            //    NPCSpawnInfo npcSpawnInfo1 = new NPCSpawnInfo();
            //    npcSpawnInfo1.m_GridPos = new Vector2(centerX, centerZ - 1);
            //    npcSpawnInfo1.m_Direction = RegionOutput.Direction.S;
            //    m_NpcSpawnInfoList.Add(npcSpawnInfo1);

            //    NPCSpawnInfo npcSpawnInfo2 = new NPCSpawnInfo();
            //    npcSpawnInfo2.m_GridPos = new Vector2(centerX, centerZ + 1);
            //    npcSpawnInfo2.m_Direction = RegionOutput.Direction.N;
            //    m_NpcSpawnInfoList.Add(npcSpawnInfo2);
            //}

            m_CenterX = centerX;
            m_CenterZ = centerZ;
        }
    }

    public void RemoveNPCCar(int index, int npcCarId, GameObject npcCarObject)
    {
        NPCCarCreator npcCarCreator = m_NPCCarCreatorDic[index];
        npcCarCreator.objectPool.PutObjectInPool(npcCarObject);

        NPCCarUseInfo npcCarUseInfo = m_NPCCarUseInfoDic[index];
        npcCarUseInfo.useCount--;

        m_NPCCarCount--;

        if (m_NPCCarDic.ContainsKey(npcCarId))
        {
            m_NPCCarDic.Remove(npcCarId);
        }
    }

    int GetNPCCarID()
    {
        int npcCarID = 0;
        for (int i = 0; i < 50; i++)
        {
            if (!m_NPCCarDic.ContainsKey(i))
            {
                npcCarID = i;
                break;
            }
        }
        return npcCarID;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PetShellExplosion : WeMonoBehaviour
{
    BattleParameter m_BattleParam;
    public BattleParameter battleParam
    {
        set
        {
            m_BattleParam = value;
        }
    }
    public float m_MaxLifeTime = 2f;

    Coroutine m_PutObjectPoolCoroutine = null;
    Vector3 m_ExplosionPos = Vector3.zero;

    int m_PetShellID = -1;
    public int petShellID
    {
        get
        {
            return m_PetShellID;
        }
        set
        {
            m_PetShellID = value;
        }
    }
    List<GameObject> m_ColliderObject = new List<GameObject>();

    GameManager m_GameMgr;

    private void Start()
    {
        // If it isn't destroyed by then, destroy the shell after it's lifetime.
        //Destroy(gameObject, m_MaxLifeTime);

        m_PutObjectPoolCoroutine = StartCoroutine(DestroyCoroutine());

        m_GameMgr = GameManager.instance;
    }

    IEnumerator DestroyCoroutine()
    {
        yield return new WaitForSeconds(m_MaxLifeTime);

        ShellManager.instance.PutPetShell(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_Tr.position.y < 0.0f)
        {
            m_ExplosionPos = new Vector3(m_Tr.position.x, 0.0f, m_Tr.position.z);
        }
        else
        {
            m_ExplosionPos = m_Tr.position;
        }
        EffectManager.instance.CreatePetRocketEffect(null, m_ExplosionPos, Quaternion.identity, m_BattleParam.explosionRadius);

        bool sameObject = false;
        CollisionConnector collisionConnector = null;        
        Collider[] colliders = Physics.OverlapSphere(m_ExplosionPos, m_BattleParam.explosionRadius, BattleControl.petAttackDamageMaskStatic);
        m_ColliderObject.Clear();
        for (int i = 0; i < colliders.Length; i++)
        {
            Collider hit = colliders[i];
            if (hit)
            {
                collisionConnector = hit.GetComponent<CollisionConnector>();
                if (collisionConnector != null)
                {
                    for (int j = 0; j < m_ColliderObject.Count; j++)
                    {
                        if(hit.gameObject == m_ColliderObject[j])
                        {
                            sameObject = true;
                            break;
                        }
                    }
                    if(!sameObject)
                    {
                        if (collisionConnector.connectRigidbody != null)
                        {
                            collisionConnector.connectRigidbody.AddForce(Vector3.up * 20.0f, ForceMode.Acceleration);
                            if (hit.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
                            {
                                collisionConnector.connectRigidbody.AddExplosionForce(500.0f, m_ExplosionPos, m_BattleParam.explosionRadius, 5.0f, ForceMode.Acceleration);
                            }
                            //collisionConnector.connectRigidbody.AddExplosionForce(30.0f, m_Tr.position, m_BattleParam.explosionRadius, 3.0f, ForceMode.Acceleration);
                        }

                        m_GameMgr.ApplyDamage(collisionConnector, hit.gameObject, m_BattleParam.attackDamage, false);

                        m_ColliderObject.Add(hit.gameObject);
                    }                    
                }
            }
        }

        ////Debug.Log(other.gameObject.layer);
        //// Collect all the colliders in a sphere from the shell's current position to a radius of the explosion radius.
        //Collider[] colliders = Physics.OverlapSphere(m_Tr.position, m_ExplosionRadius, m_TankMask);

        //// Go through all the colliders...
        //for (int i = 0; i < colliders.Length; i++)
        //{
        //    //// ... and find their rigidbody.
        //    //Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

        //    //// If they don't have a rigidbody, go on to the next collider.
        //    //if (!targetRigidbody)
        //    //    continue;

        //    //// Add an explosion force.
        //    //targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);

        //    //// Find the TankHealth script associated with the rigidbody.
        //    //TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();

        //    //// If there is no TankHealth script attached to the gameobject, go on to the next collider.
        //    //if (!targetHealth)
        //    //    continue;

        //    //// Calculate the amount of damage the target should take based on it's distance from the shell.
        //    //float damage = CalculateDamage(targetRigidbody.position);

        //    //// Deal this damage to the tank.
        //    //targetHealth.TakeDamage(damage);
        //}

        if (m_PutObjectPoolCoroutine != null)
        {
            StopCoroutine(m_PutObjectPoolCoroutine);
        }

        ShellManager.instance.PutPetShell(this);
    }

    //private float CalculateDamage(Vector3 targetPosition)
    //{
    //    // Create a vector from the shell to the target.
    //    Vector3 explosionToTarget = targetPosition - transform.position;

    //    // Calculate the distance from the shell to the target.
    //    float explosionDistance = explosionToTarget.magnitude;

    //    // Calculate the proportion of the maximum distance (the explosionRadius) the target is away.
    //    float relativeDistance = (m_ExplosionRadius - explosionDistance) / m_ExplosionRadius;

    //    // Calculate damage as this proportion of the maximum possible damage.
    //    float damage = relativeDistance * m_MaxDamage;

    //    // Make sure that the minimum damage is always 0.
    //    damage = Mathf.Max(0f, damage);

    //    return damage;
    //}
}

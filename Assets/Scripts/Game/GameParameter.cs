﻿using UnityEngine;
using System.Collections;

public class GameParameter : MonoBehaviour
{
    public int maxHealth = 30;
    int m_CurrentHealth;
    public int currentHealth
    {
        set
        {
            m_CurrentHealth = value;
        }
        get
        {
            return m_CurrentHealth;
        }
    }
    public int damage = 10;
    public bool shrink = false;

    void Start()
    {
        m_CurrentHealth = maxHealth;
    }
}

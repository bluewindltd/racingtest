﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.IO;
using System.Text;

using SimpleJSON;

[ExecuteInEditMode]
public class Ground : MonoBehaviour
{
    public static Dictionary<GameConfig.Direction, Vector3> straight_pos = new Dictionary<GameConfig.Direction, Vector3>() { { GameConfig.Direction.North, new Vector3(-30, 0, 0) }, { GameConfig.Direction.East, new Vector3(0, 0, 30) }, { GameConfig.Direction.South, new Vector3(30, 0, 0) }, { GameConfig.Direction.West, new Vector3(0, 0, -30) } };
    public static Dictionary<GameConfig.Direction, Vector3> piece_pos = new Dictionary<GameConfig.Direction, Vector3>() { { GameConfig.Direction.North, new Vector3(-30, 0, 30) }, { GameConfig.Direction.East, new Vector3(30, 0, 30) }, { GameConfig.Direction.South, new Vector3(30, 0, -30) }, { GameConfig.Direction.West, new Vector3(-30, 0, -30) } };

    //[HideInInspector][SerializeField] GameConfig.GroundType m_GroundType;
    //public GameConfig.GroundType groundType
    //{
    //    get
    //    {
    //        return m_GroundType;
    //    }
    //    set
    //    {
    //        m_GroundType = value;
    //    }
    //}

    [HideInInspector][SerializeField] int m_Theme;
    public int theme
    {
        get
        {
            return m_Theme;
        }
        set
        {
            m_Theme = value;
        }
    }

    [HideInInspector][SerializeField] GameObject m_ObjectRoot = null;
    public GameObject objectRoot
    {
        get
        {
            return m_ObjectRoot;
        }
        set
        {
            m_ObjectRoot = value;
        }
    }

    [HideInInspector][SerializeField] Transform m_ObjectRootTransform = null;
    public Transform objectRootTransform
    {
        get
        {
            return m_ObjectRootTransform;
        }
        set
        {
            m_ObjectRootTransform = value;
        }
    }

    Transform m_ObjectPoolParent = null;
    GameConfig.GroundType m_GroundType;
    public GameConfig.GroundType groundType
    {
        get
        {
            return m_GroundType;
        }
        set
        {
            m_GroundType = value;
        }
    }

    int m_Index;
    public int index
    {
        get
        {
            return m_Index;
        }
        set
        {
            m_Index = value;
        }
    }
    //List<MapObject> m_MapObjectList = new List<MapObject>();

    [SerializeField] List<Transform> m_MissionPointList = new List<Transform>();
    public List<Transform> missionPointList
    {
        get
        {
            return m_MissionPointList;
        }
    }

    [SerializeField]
    List<Transform> m_ItemPointList = new List<Transform>();
    public List<Transform> itemPointList
    {
        get
        {
            return m_ItemPointList;
        }
    }

    [SerializeField] List<Obstacle> m_ObstacleList = new List<Obstacle>();

    bool m_IsFirst = false;
    bool m_IsFirstUp = false;

    public void SetGround(Transform parentRegion, int groundTheme, GameConfig.GroundType groundType, GameConfig.Direction direction, bool isFirst, bool isFirstUp)
    {
        m_IsFirst = isFirst;
        m_IsFirstUp = isFirstUp;

        m_ObjectPoolParent = transform.parent;

        transform.parent = parentRegion;
        m_Theme = groundTheme;
        m_GroundType = groundType;
        if (m_GroundType == GameConfig.GroundType.Straight)
        {
            transform.localPosition = straight_pos[direction];
        }
        else if(m_GroundType == GameConfig.GroundType.Corner)
        {
            transform.localPosition = Vector3.zero;
        }
        else if(m_GroundType == GameConfig.GroundType.Piece)
        {
            transform.localPosition = piece_pos[direction];
        }
        transform.rotation = Quaternion.Euler(0.0f, 90.0f * (float)direction, 0f);

        //for(int i = 0; i < m_ObstacleList.Count; i++)
        //{
        //    m_ObstacleList[i].Initialize();
        //}
        //m_MapObjectList.Clear();
        //int maxGroundCount = GroundManager.instance.GetGroundCount(theme, m_GroundType);
        //if(maxGroundCount > 0)
        //{
        //    int groundIndex = UnityEngine.Random.Range(0, maxGroundCount);
        //    //Debug.Log("GroundIndex : " + m_GroundType + ", " + groundIndex);
        //    LoadMapData(string.Format("MapData/ground_{0}_{1}_{2}", theme, (int)m_GroundType, groundIndex), false);
        //}            
    }

    public void InitializeObstacle(bool onlyJointInit)
    {
        for (int i = 0; i < m_ObstacleList.Count; i++)
        {
            m_ObstacleList[i].Initialize(onlyJointInit);
        }
    }

    public void Restore()
    {
        //for(int i = 0; i < m_MapObjectList.Count; i++)
        //{
        //    MapObject mapObject = m_MapObjectList[i];
        //    mapObject.Restore();
        //}

        if(m_ObjectPoolParent != null)
        {
            transform.parent = m_ObjectPoolParent;
            m_ObjectPoolParent = null;
        }
        transform.localPosition = Vector3.zero;
        transform.rotation = Quaternion.identity;

        //ObjectPool groundObjectPool = RegionManager.instance.GetGroundObjectPool(m_GroundType);
        //groundObjectPool.PutObjectInPool(gameObject);

        GroundManager.instance.RestoreGround(m_IsFirst, m_IsFirstUp, m_Theme, m_GroundType, m_Index, gameObject);
    }

    public void InitializeMapData()
    {
        m_MissionPointList.Clear();
        m_ItemPointList.Clear();
        m_ObstacleList.Clear();

        for (int i = m_ObjectRootTransform.childCount - 1; i >= 0; --i)
        {
            GameObject mapObject = m_ObjectRootTransform.GetChild(i).gameObject;

            if (mapObject.layer == LayerMask.NameToLayer("MissionPoint"))
            {
                m_MissionPointList.Add(mapObject.transform);
            }
            else if (mapObject.layer == LayerMask.NameToLayer("ItemPoint"))
            {
                m_ItemPointList.Add(mapObject.transform);
            }
            else if (mapObject.layer == LayerMask.NameToLayer("Obstacle"))
            {
                m_ObstacleList.Add(mapObject.GetComponent<Obstacle>());
            }
        }
    }

    //
    public void LoadMapData(string loadPath, bool editorMode, bool createIntroBuilding)
    {
        if (m_ObjectRoot != null)
        {
            if (editorMode)
            {
                CommonUtil.DestroyChildrenEditor(m_ObjectRoot.transform);

				string fileContents = System.IO.File.ReadAllText(loadPath);
                if (fileContents.CompareTo("") != 0)
                {
                    //JSONNode json = JSON.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(fileContents)));
                    JSONNode json = JSON.Parse(fileContents);
                    JSONNode jsonMapData = json["map_data"];

                    //m_GroundType = (GameConfig.GroundType)jsonMapData["groundType"].AsInt;
                    //m_Theme = jsonMapData["theme"].AsInt;
                    JSONArray jsonObjectList = jsonMapData["object_list"].AsArray;
                    for (int i = 0; i < jsonObjectList.Count; i++)
                    {
                        JSONNode jsonObjectData = jsonObjectList[i];

                        int objectTheme = jsonObjectData["theme"].AsInt;
                        int objectIndex = jsonObjectData["index"].AsInt;
                        Vector3 pos = new Vector3(jsonObjectData["pos_x"].AsFloat, jsonObjectData["pos_y"].AsFloat, jsonObjectData["pos_z"].AsFloat);
                        Vector3 rot = new Vector3(jsonObjectData["rot_x"].AsFloat, jsonObjectData["rot_y"].AsFloat, jsonObjectData["rot_z"].AsFloat);
                        Vector3 sc = new Vector3(jsonObjectData["sc_x"].AsFloat, jsonObjectData["sc_y"].AsFloat, jsonObjectData["sc_z"].AsFloat);

                        MapObjectCreator mapObjectCreator = MapObjectManager.instance.GetMapObjectCreator(objectTheme, objectIndex);
                        if(mapObjectCreator != null)
                        {
                            if(mapObjectCreator.mapObjectPrefab != null)
                            {
                                GameObject mapObject = (GameObject)MonoBehaviour.Instantiate(mapObjectCreator.mapObjectPrefab, Vector3.zero, Quaternion.identity);
                                mapObject.transform.parent = m_ObjectRoot.transform;
                                mapObject.transform.localPosition = pos;
                                mapObject.transform.localEulerAngles = rot;
                                mapObject.transform.localScale = sc;
                            }
                        }
                    }
                }
            }
            else
            {
                m_MissionPointList.Clear();
                m_ItemPointList.Clear();
                m_ObstacleList.Clear();

                MapData mapData = GroundManager.instance.GetMapData(loadPath);
                if (mapData != null)
                {
                    for (int i = 0; i < mapData.mapObjectList.Count; i++)
                    {
                        MapObjectData mapObjectData = mapData.mapObjectList[i];
                        MapObjectCreator mapObjectCreator = MapObjectManager.instance.GetMapObjectCreator(mapObjectData.theme, mapObjectData.index);
                        if (mapObjectCreator != null)
                        {
                            MapObject mapObject = ((GameObject)MonoBehaviour.Instantiate(mapObjectCreator.mapObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<MapObject>();
                            
                            if(mapObject.objectType == GameConfig.ObjectType.MissionPoint)
                            {
                                m_MissionPointList.Add(mapObject.transform);
                            }
                            else if (mapObject.objectType == GameConfig.ObjectType.ItemPoint)
                            {
                                m_ItemPointList.Add(mapObject.transform);
                            }
                            else if (mapObject.objectType == GameConfig.ObjectType.Obstacle)
                            {
                                m_ObstacleList.Add(mapObject.GetComponent<Obstacle>());
                            }
                            mapObject.SetMapObject(m_ObjectRoot.transform, mapObjectData.pos, mapObjectData.rot, mapObjectData.scale);
                        }
                    }
                }

                if(createIntroBuilding)
                {
                    GameObject introBuildingObject = (GameObject)MonoBehaviour.Instantiate(GameManager.instance.introBuildingPrefab, Vector3.zero, Quaternion.identity);
                    MapObject mapObject = introBuildingObject.GetComponent<MapObject>();
                    mapObject.SetMapObject(m_ObjectRoot.transform, new Vector3(2.3f, 0.0f, 0.0f), new Vector3(0, -180, 0), new Vector3(1, 1, 1));
                    IntroBuilding introBuilding = introBuildingObject.GetComponent<IntroBuilding>();
                    GameManager.instance.SpawnPlayer(introBuilding);
                }

                //MapData mapData = GroundManager.instance.GetMapData(loadPath);
                //if(mapData != null)
                //{
                //    for(int i = 0; i < mapData.mapObjectList.Count; i++)
                //    {
                //        MapObjectData mapObjectData = mapData.mapObjectList[i];
                //        MapObjectCreator mapObjectCreator = MapObjectManager.instance.GetMapObjectCreator(mapObjectData.theme, mapObjectData.index);
                //        if (mapObjectCreator != null)
                //        {
                //            MapObject mapObject = mapObjectCreator.objectPool.GetObjectFromPool().GetComponent<MapObject>();
                //            mapObject.SetMapObject(m_ObjectRoot.transform, mapObjectData.pos, mapObjectData.rot, mapObjectData.scale);
                //            m_MapObjectList.Add(mapObject);
                //        }
                //    }
                //}

                //TextAsset asset = Resources.Load(loadPath) as TextAsset;
                //if (asset.text.CompareTo("") != 0)
                //{
                //    //JSONNode json = JSON.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(asset.text)));
                //    JSONNode json = JSON.Parse(asset.text);
                //    JSONNode jsonMapData = json["map_data"];

                //    //m_GroundType = (GameConfig.GroundType)jsonMapData["groundType"].AsInt;
                //    //m_Theme = jsonMapData["theme"].AsInt;
                //    JSONArray jsonObjectList = jsonMapData["object_list"].AsArray;
                //    for (int i = 0; i < jsonObjectList.Count; i++)
                //    {
                //        JSONNode jsonObjectData = jsonObjectList[i];

                //        GameConfig.ObjectType objectType = (GameConfig.ObjectType)jsonObjectData["objectType"].AsInt;
                //        int objectTheme = jsonObjectData["theme"].AsInt;
                //        int objectIndex = jsonObjectData["index"].AsInt;
                //        Vector3 pos = new Vector3(jsonObjectData["pos_x"].AsFloat, jsonObjectData["pos_y"].AsFloat, jsonObjectData["pos_z"].AsFloat);
                //        Vector3 rotation = new Vector3(jsonObjectData["rot_x"].AsFloat, jsonObjectData["rot_y"].AsFloat, jsonObjectData["rot_z"].AsFloat);
                //        Vector3 scale = new Vector3(jsonObjectData["sc_x"].AsFloat, jsonObjectData["sc_y"].AsFloat, jsonObjectData["sc_z"].AsFloat);

                //        MapObjectCreator mapObjectCreator = MapObjectManager.instance.GetMapObjectCreator(objectTheme, objectIndex);
                //        if (mapObjectCreator != null)
                //        {
                //            MapObject mapObject = mapObjectCreator.objectPool.GetObjectFromPool().GetComponent<MapObject>();
                //            mapObject.SetMapObject(m_ObjectRoot.transform, pos, rotation, scale);
                //            m_MapObjectList.Add(mapObject);
                //        }
                //    }
                //}
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class GameCameraControl : MonoBehaviour
{
    public float m_DampTime = 0.2f;                 // Approximate time for the camera to refocus.
    public float m_ScreenEdgeBuffer = 4f;           // Space between the top/bottom most target and the screen edge.
    public float m_MinSize = 6.5f;                  // The smallest orthographic size the camera can be.
    [HideInInspector]
    public Transform[] m_Targets; // All the targets the camera needs to encompass.


    public Camera gameCamera;                                // Used for referencing the camera.
    private float m_ZoomSpeed;                      // Reference speed for the smooth damping of the orthographic size.
    private Vector3 m_MoveVelocity;                 // Reference velocity for the smooth damping of the position.
    private Vector3 m_DesiredPosition;              // The position the camera is moving towards.

    bool m_EnableZoom = false;
    public bool enableZoom
    {
        set
        {
            m_EnableZoom = value;
        }
    }

    float m_MinCameraZoomDistance = 16.0f;
    //float m_MinCameraZoomDistance = 70.0f;
    public float minCameraZoomDistance
    {
        get
        {
            return m_MinCameraZoomDistance;
        }
        set
        {
            m_MinCameraZoomDistance = value;
        }
    }
    float m_CameraZoomRange = 5.0f;
    //float m_CameraZoomRange = 0.0f;
    float m_MaxSpeed = 150.0f;
    public AnimationCurve zoomCurve;
    TestMobileInput m_TestMobileInput;
    public TestMobileInput testMobileInput
    {
        set
        {
            m_TestMobileInput = value;
        }
    }


    //public void SetCamera(Camera newCamera)
    //{
    //    newCamera.transform.parent = transform;
    //    gameCamera = newCamera;
    //    gameCamera.orthographic = true;
    //}

    private void FixedUpdate()
    {
        // Move the camera towards a desired position.
        Move();

        // Change the size of the camera based.
        if (m_EnableZoom)
        {
            Zoom();
        }

        //Rotate();
    }

    private void Move()
    {
        // Find the average position of the targets.
        FindAveragePosition();

        // Smoothly transition to that position.
        transform.position = m_DesiredPosition;// Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
    }

    private void FindAveragePosition()
    {
        Vector3 averagePos = new Vector3();
        int numTargets = 0;

        // Go through all the targets and add their positions together.
        for (int i = 0; i < m_Targets.Length; i++)
        {
            // If the target isn't active, go on to the next one.
            if (!m_Targets[i].gameObject.activeSelf)
                continue;

            // Add to the average and increment the number of targets in the average.
            averagePos += m_Targets[i].position;
            numTargets++;
        }

        // If there are targets divide the sum of the positions by the number of them to find the average.
        if (numTargets > 0)
            averagePos /= numTargets;

        // Keep the same y value.
        averagePos.y = transform.position.y;

        // The desired position is the average position;
        m_DesiredPosition = averagePos;
    }


    private void Zoom()
    {
        if(gameCamera != null)
        {
            float speedRange = (float)m_TestMobileInput.speed / m_MaxSpeed;

            float zoomFactor = Mathf.Clamp(zoomCurve.Evaluate(speedRange), 0.0f, 1.0f);
            // Find the required size based on the desired position and smoothly transition to that size.

            //Debug.Log(string.Format("카메라 줌 : {0}, {1}, {2}", m_TestMobileInput.speed, zoomFactor, m_MinCameraZoomDistance + (m_CameraZoomRange * zoomFactor)));
            gameCamera.orthographicSize = Mathf.SmoothDamp(gameCamera.orthographicSize, m_MinCameraZoomDistance + (m_CameraZoomRange * zoomFactor), ref m_ZoomSpeed, m_DampTime);
            //gameCamera.orthographicSize = Mathf.SmoothDamp(gameCamera.orthographicSize, 64.0f, ref m_ZoomSpeed, m_DampTime);
        }        
    }


    private float FindRequiredSize()
    {
        //// Find the position the camera rig is moving towards in its local space.
        //Vector3 desiredLocalPos = transform.InverseTransformPoint(m_DesiredPosition);

        //// Start the camera's size calculation at zero.
        //float size = 0f;

        //// Go through all the targets...
        //for (int i = 0; i < m_Targets.Length; i++)
        //{
        //    // ... and if they aren't active continue on to the next target.
        //    if (!m_Targets[i].gameObject.activeSelf)
        //        continue;

        //    // Otherwise, find the position of the target in the camera's local space.
        //    Vector3 targetLocalPos = transform.InverseTransformPoint(m_Targets[i].position);

        //    // Find the position of the target from the desired position of the camera's local space.
        //    Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

        //    // Choose the largest out of the current size and the distance of the tank 'up' or 'down' from the camera.
        //    size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));

        //    // Choose the largest out of the current size and the calculated size based on the tank being to the left or right of the camera.
        //    size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / m_Camera.aspect);
        //}

        //// Add the edge buffer to the size.
        //size += m_ScreenEdgeBuffer;

        //// Make sure the camera's size isn't below the minimum.
        //size = Mathf.Max (size, m_MinSize);
        //size = 16.0f;

        //return size;
        return 16.0f;
        //return 67.0f;
    }

    void Rotate()
    {
        if (gameCamera != null)
        {
            //float posX = 0.0f;
            //if (Mathf.Abs(m_Camera.transform.localPosition.x) > 1.0f)
            //{
            //    posX = Mathf.Lerp(m_Camera.transform.localPosition.x, 0.0f, Time.deltaTime);
            //}
            //float posY = 0.0f;
            //if (Mathf.Abs(m_Camera.transform.localPosition.y) > 1.0f)
            //{
            //    posY = Mathf.Lerp(m_Camera.transform.localPosition.y, 0.0f, Time.deltaTime);
            //}
            //float posZ = -65.0f;
            //if (Mathf.Abs(m_Camera.transform.localPosition.z - (-65.0f)) > 1.0f)
            //{
            //    posZ = Mathf.Lerp(m_Camera.transform.localPosition.z, -65, Time.deltaTime);
            //}
            //m_Camera.transform.localPosition = new Vector3(posX, posY, posZ);

            //float rotX = 0.0f;
            //if (Mathf.Abs(m_Camera.transform.localEulerAngles.x) > 1.0f)
            //{
            //    rotX = Mathf.Lerp(m_Camera.transform.localEulerAngles.x, 0.0f, Time.deltaTime);
            //}
            //float rotY = 0.0f;
            //if (Mathf.Abs(m_Camera.transform.localEulerAngles.y) > 1.0f)
            //{
            //    rotY = Mathf.Lerp(m_Camera.transform.localEulerAngles.y, 0.0f, Time.deltaTime);
            //}
            //float rotZ = 0.0f;
            //if (Mathf.Abs(m_Camera.transform.localEulerAngles.z) > 1.0f)
            //{
            //    rotZ = Mathf.Lerp(m_Camera.transform.localEulerAngles.z, 0.0f, Time.deltaTime);
            //}
            //m_Camera.transform.localEulerAngles = new Vector3(rotX, rotY, rotZ);

            //float gap = Mathf.Abs(m_Camera.transform.localPosition.z + 65.0f);
            //if(gap < 1.0f)
            //{
            //    m_Camera.transform.localPosition = new Vector3(0, 0, -65);
            //}
            //else
            //{
            //    m_Camera.transform.localPosition = new Vector3(0, 0, Mathf.Lerp(m_Camera.transform.localPosition.z, -65, Time.deltaTime));
            //}
            //m_Camera.transform.localPosition = new Vector3(0, 0, Mathf.Lerp(m_Camera.transform.localPosition.z, -65, Time.deltaTime));
            //m_Camera.transform.localEulerAngles = new Vector3(Mathf.Lerp(m_Camera.transform.localEulerAngles.x, 35, Time.deltaTime),
            //    Mathf.Lerp(m_Camera.transform.localEulerAngles.y, 315, Time.deltaTime),
            //    0);
        }
    }

    public void SetStartPositionAndSize()
    {
        if (gameCamera != null)
        {
            // Find the desired position.
            FindAveragePosition();

            // Set the camera's position to the desired position without damping.
            transform.position = m_DesiredPosition;

            // Find and set the required size of the camera.
            //gameCamera.orthographicSize = FindRequiredSize();
        }
    }
}

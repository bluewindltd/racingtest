﻿using UnityEngine;
using System;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes;

public class ResultUI : MonoBehaviour
{
    enum ExperienceState
    {
        Enable = 0,
        End,
        HavePet,
    };
    ExperienceState m_ExperienceState = ExperienceState.Enable;

    public UILabel titleLabel;
    public UILabel gameOverLabel;
    public UILabel scoreDescLabel;
    public UILabel scoreLabel;
    public UILabel timeDescLabel;
    public UILabel timeLabel;

    public GameObject playGiftObject;
    public UISprite playGiftProgressSprite;
    public UILabel playGiftLabel;
    //public UILabel playGiftFreeButtonLabel;

    public GameObject playGiftCompleteObject;
    public UILabel playGiftCompleteLabel;

    public GameObject gachaButtonObject;
    public UILabel gachaButtonLabel;

    public GameObject needCashObject;
    public UILabel needCashLabel;

    public GameObject rewardObject;
    public GameObject adviewObject;
    public UILabel adviewButtonLabel;
    public GameObject freeGiftObject;
    public UILabel freeGiftButtonLabel;

    public GameObject rewardResultObject;
    public UILabel rewardResultLabel;

    // 체험하기 관련
    public GameObject petExperienceButtonObject;
    public UILabel petExperienceButtonLabel;
    public GameObject petExperienceCountObject;
    public UILabel petExperienceCountLabel;

    public UILabel restartButtonLabel;
    public UILabel restartWithADButtonLabel;
    public UILabel restartWithADDescLabel;
    public UILabel exitButtonLabel;

    public GameObject[] logoObjectArray;

    bool m_FreeGiftEnable = false;
    float m_StartPlayGiftCount = 0.0f;
    float m_DestPlayGiftCount = 0.0f;

    ObscuredInt m_AdRewardAmount = 30;

    UserManager m_UserMgr;
    EtcDocs m_EtcDocs;

    void Awake()
    {
        m_UserMgr = UserManager.instance;
        m_EtcDocs = EtcDocs.instance;

        titleLabel.text = TextDocs.content("UI_RESULT_TEXT");
        scoreDescLabel.text = TextDocs.content("UI_SCORE_TEXT");
        timeDescLabel.text = TextDocs.content("UI_TIME_TEXT");
        //playGiftFreeButtonLabel.text = TextDocs.content("UI_FREE_TEXT");

        // 플레이 기프트 뽑을 수 있을 때
        playGiftCompleteLabel.text = TextDocs.content("UI_TRIED_COMPLETE_TEXT");

        gachaButtonLabel.text = TextDocs.content("UI_GACHA_BUTTON_TEXT");

        // 광고 시청시 라벨
        m_AdRewardAmount = m_EtcDocs.firstAdRewardCash;
        adviewButtonLabel.text = string.Format(TextDocs.content("BT_ADVIEW_TEXT"), m_AdRewardAmount);

        // 무료 캐쉬 라벨
        freeGiftButtonLabel.text = TextDocs.content("BT_FREEGIFT_TEXT");

        restartButtonLabel.text = TextDocs.content("BT_RESTART_TEXT");
        restartWithADButtonLabel.text = TextDocs.content("BT_ADRESTART_TEXT");
        restartWithADDescLabel.text = TextDocs.content("UI_ADSTART_DESC");
        exitButtonLabel.text = TextDocs.content("BT_EXIT_TEXT");
        
        for (int i = 0; i < logoObjectArray.Length; i++)
        {
            NGUITools.SetActive(logoObjectArray[i], false);
        }
        GameConfig.SupportLanguage selectLanguage = m_UserMgr.selectLanguage;
        if (selectLanguage == GameConfig.SupportLanguage.Japanese)
        {
            NGUITools.SetActive(logoObjectArray[1], true);
        }
        else if (selectLanguage == GameConfig.SupportLanguage.Chinese_S)
        {
            NGUITools.SetActive(logoObjectArray[2], true);
        }
        else if (selectLanguage == GameConfig.SupportLanguage.Chinese_T)
        {
            NGUITools.SetActive(logoObjectArray[3], true);
        }
        else
        {
            NGUITools.SetActive(logoObjectArray[0], true);
        }
    }

    void Start()
    {
        GiftUI.OnRefreshCash += OnRefreshCash;
        CommonAdManager.OnHideVideoAd += FreeReward;
        UserManager.OnPurchaseComplete += PurchaseComplete;
    }

    void OnDestroy()
    {
        GiftUI.OnRefreshCash -= OnRefreshCash;
        CommonAdManager.OnHideVideoAd -= FreeReward;
        UserManager.OnPurchaseComplete -= PurchaseComplete;
    }

    public void SetResult(bool isBursted, int playTime, int score, int prePlayGiftCount)
    {
        SoundManager.instance.PlaySFX("result");

        if (isBursted)
        {
            gameOverLabel.text = TextDocs.content("UI_BUSTED_TEXT");
        }
        else
        {
            gameOverLabel.text = TextDocs.content("UI_WASTED_TEXT");
        }

        scoreLabel.text = string.Format("{0}", playTime + score);
        timeLabel.text = string.Format("{0}", playTime);

        m_StartPlayGiftCount = prePlayGiftCount;
        if (m_StartPlayGiftCount >= m_EtcDocs.playGiftCount)
        {
            m_StartPlayGiftCount = m_EtcDocs.playGiftCount;
        }
        m_DestPlayGiftCount = m_UserMgr.playGiftCount;
        if(m_DestPlayGiftCount >= m_EtcDocs.playGiftCount)
        {
            m_DestPlayGiftCount = m_EtcDocs.playGiftCount;
        }
        StartCoroutine(UpProgressPlayGiftCount());

        //// 플레이 보상 얻을 수 있으면 플레이 보상 버튼 활성화
        //if (m_UserMgr.playGiftCount >= m_EtcDocs.playGiftCount)
        //{
        //    playGiftObject.SetActive(false);
        //    playGiftCompleteObject.SetActive(true);
        //}
        //else
        //{
        //    playGiftObject.SetActive(true);
        //    playGiftCompleteObject.SetActive(false);

        //    playGiftProgressSprite.fillAmount = (float)m_UserMgr.playGiftCount / (float)m_EtcDocs.playGiftCount;
        //    playGiftLabel.text = string.Format(TextDocs.content("UI_TRIED_TEXT"), m_UserMgr.playGiftCount, m_EtcDocs.playGiftCount);
        //}

        CheckPlayGacha();

        rewardObject.SetActive(true);
        rewardResultObject.SetActive(false);

        // 무료 선물 받을 수 있는 상황 이면 무료 선물 버튼, 아니면 광고 시청 버튼 표시
        if(TimeManager.instance.GetTimeState())
        {
            long time = TimeManager.instance.GetOnlineTime();
            long elapsedTimeL = m_UserMgr.freeGiftTime - time;

            if (elapsedTimeL > 0)
            {
                adviewObject.SetActive(true);
                freeGiftObject.SetActive(false);

                m_FreeGiftEnable = false;
            }
            else
            {
                adviewObject.SetActive(false);
                freeGiftObject.SetActive(true);

                m_FreeGiftEnable = true;
            }
        }
        else
        {
            adviewObject.SetActive(true);
            freeGiftObject.SetActive(false);

            m_FreeGiftEnable = false;
        }

        // 드론펫 보유 여부를 판단 후 펫 체험하기를 할지 펫 구매 유도를 할지 판단
        bool havePet = false;
        if(m_UserMgr.petInfoDic.ContainsKey(0))
        {
            if (m_UserMgr.petInfoDic[0].state != PetInfo.State.NotHave)
            {
                havePet = true;
            }
        }
        // 드론펫을 보유하고 있지 않을 경우에만
        if (!havePet)
        {            
            NGUITools.SetActive(petExperienceButtonObject, true);

            // 체험하기를 모두 했을 경우 구매 유도를 한다.
            if(m_UserMgr.curPetExperienceCount >= m_EtcDocs.petExperienceCount)
            {
                NGUITools.SetActive(petExperienceCountObject, false);

                petExperienceButtonLabel.text = TextDocs.content("BT_BUY_TEXT");

                m_ExperienceState = ExperienceState.End;
            }
            else
            {
                NGUITools.SetActive(petExperienceCountObject, true);

                petExperienceButtonLabel.text = TextDocs.content("BT_EXPERIENCE_TEXT");
                petExperienceCountLabel.text = string.Format("{0}", m_EtcDocs.petExperienceCount - m_UserMgr.curPetExperienceCount);

                m_ExperienceState = ExperienceState.Enable;
            }
        }
        else
        {
            NGUITools.SetActive(petExperienceButtonObject, false);

            m_ExperienceState = ExperienceState.HavePet;
        }
    }

    IEnumerator UpProgressPlayGiftCount()
    {
        playGiftObject.SetActive(true);
        playGiftCompleteObject.SetActive(false);

        while (Mathf.Abs(m_DestPlayGiftCount - m_StartPlayGiftCount) >= 0.7f)
        {
            playGiftProgressSprite.fillAmount = m_StartPlayGiftCount / (float)m_EtcDocs.playGiftCount;
            playGiftLabel.text = string.Format(TextDocs.content("UI_TRIED_TEXT"), (int)m_StartPlayGiftCount, m_EtcDocs.playGiftCount);
            
            LeanTween.scale(playGiftLabel.gameObject, new UnityEngine.Vector3(1.1f, 1.1f, 1.0f), 0.02f).setDelay(0.02f);

            yield return null;

            m_StartPlayGiftCount = Mathf.Lerp(m_StartPlayGiftCount, m_DestPlayGiftCount, Time.deltaTime * 4.0f);

            //Debug.Log("UpProgressPlayGiftCount : " + m_StartPlayGiftCount);
        }

        if (m_UserMgr.playGiftCount >= m_EtcDocs.playGiftCount)
        {
            playGiftObject.SetActive(false);
            playGiftCompleteObject.SetActive(true);

            playGiftProgressSprite.fillAmount = 1.0f;
            playGiftLabel.text = string.Format(TextDocs.content("UI_TRIED_TEXT"), m_EtcDocs.playGiftCount, m_EtcDocs.playGiftCount);
        }
        else
        {
            playGiftObject.SetActive(true);
            playGiftCompleteObject.SetActive(false);

            playGiftProgressSprite.fillAmount = (float)m_UserMgr.playGiftCount / (float)m_EtcDocs.playGiftCount;
            playGiftLabel.text = string.Format(TextDocs.content("UI_TRIED_TEXT"), m_UserMgr.playGiftCount, m_EtcDocs.playGiftCount);
        }

        LeanTween.scale(playGiftLabel.gameObject, new UnityEngine.Vector3(1.1f, 1.1f, 1.0f), 0.02f).setDelay(0.02f).setOnComplete(CompletePlayGiftCount);
    }

    void CompletePlayGiftCount()
    {
        playGiftLabel.gameObject.transform.localScale = new UnityEngine.Vector3(1.0f, 1.0f, 1.0f);
    }

    // 가챠를 돌릴 수 있는 상황인지 판단하고 UI 상태 변경
    void CheckPlayGacha()
    {
        // 현재 캐쉬가 가챠 뽑기에 부족한지 아닌지 판단 후 가챠 버튼 or 캐쉬 부족 알림 표시
        if (m_UserMgr.cash >= m_EtcDocs.gachaCash)
        {
            gachaButtonObject.SetActive(true);
            needCashObject.SetActive(false);
        }
        else
        {
            gachaButtonObject.SetActive(false);
            needCashObject.SetActive(true);
            needCashLabel.text = string.Format(TextDocs.content("UI_NEED_CASH_TEXT"), m_EtcDocs.gachaCash - m_UserMgr.cash);
        }
    }

    void OnEarnGift(GameConfig.GiftType type)
    {
        if (type == GameConfig.GiftType.Play)
        {
            playGiftObject.SetActive(true);
            playGiftCompleteObject.SetActive(false);

            playGiftProgressSprite.fillAmount = (float)m_UserMgr.playGiftCount / (float)m_EtcDocs.playGiftCount;
            playGiftLabel.text = string.Format(TextDocs.content("UI_TRIED_TEXT"), m_UserMgr.playGiftCount, m_EtcDocs.playGiftCount);
        }

        GameManager.instance.RefreshCash();

        GiftUI.OnEarnGift -= OnEarnGift;
    }

    void OnRefreshCash(GameConfig.GiftType type, int earnCash)
    {
        if(type == GameConfig.GiftType.Free)
        {
            rewardObject.SetActive(false);
            rewardResultObject.SetActive(true);

            // 광고 시청 혹은 무료 캐쉬로 캐쉬를 얻었을 때 라벨
            rewardResultLabel.text = string.Format(TextDocs.content("UI_EARN_CASH_TEXT"), earnCash);   
        }

        CheckPlayGacha();

        GameManager.instance.RefreshCash();
    }

    public void OnClickLeaderBoard()
    {
        SoundManager.PlayButtonClickSound();

#if UNITY_ANDROID
        GooglePlayManager.Instance.ShowLeaderBoardById("CgkImbbs5L8bEAIQBQ");
        //GooglePlayManager.Instance.ShowLeaderBoardsUI();
#endif
    }

    public void OnClickAchievement()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowAchievementPopup();

        //m_StartPlayGiftCount = m_UserMgr.playGiftCount;
        //m_UserMgr.playGiftCount += 100;

        //if (m_StartPlayGiftCount >= m_EtcDocs.playGiftCount)
        //{
        //    m_StartPlayGiftCount = m_EtcDocs.playGiftCount;
        //}
        //m_DestPlayGiftCount = m_UserMgr.playGiftCount;
        //if (m_DestPlayGiftCount >= m_EtcDocs.playGiftCount)
        //{
        //    m_DestPlayGiftCount = m_EtcDocs.playGiftCount;
        //}
        //StartCoroutine(UpProgressPlayGiftCount());
    }

    public void OnClickSetup()
    {
        SoundManager.PlayButtonClickSound();

        PopupManager.instance.ShowSetupPopup();
    }

    public void OnClickExit()
    {
        SoundManager.PlayButtonClickSound();

        m_UserMgr.isDirectGamePlay = false;
        m_UserMgr.isDirectGamePlayWithAD = false;
        GameManager.instance.RestoreGameScene(GameConfig.Scene.GameScene);
    }    

    public void OnClickRestart()
    {
        SoundManager.PlayButtonClickSound();

        m_UserMgr.isDirectGamePlay = true;
        m_UserMgr.isDirectGamePlayWithAD = false;
        GameManager.instance.RestoreGameScene(GameConfig.Scene.GameScene);
    }

    public void OnClickRestartWithAd()
    {
        SoundManager.PlayButtonClickSound();

        m_UserMgr.isDirectGamePlay = true;
        m_UserMgr.isDirectGamePlayWithAD = true;
        GameManager.instance.RestoreGameScene(GameConfig.Scene.GameScene);
    }

    public void OnClickExperiencePet()
    {
        SoundManager.PlayButtonClickSound();
    }

    //void AdReward(bool isSuccess)
    //{
    //    CommonAdManager.OnHideVideoAd -= AdReward;
    //}
    void FreeReward(bool isSuccess)
    {
        Debug.Log("ResultUI FreeReward");
        if (isSuccess)
        {
            //GameManager.instance.ShowGift(GameConfig.GiftType.Free);
            //rewardObject.SetActive(false);
            //rewardResultObject.SetActive(true);

            m_UserMgr.cash += m_AdRewardAmount;
            m_UserMgr.SaveUserData();

            m_AdRewardAmount = m_EtcDocs.idleAdRewardCash;
            adviewButtonLabel.text = string.Format(TextDocs.content("BT_ADVIEW_TEXT"), m_AdRewardAmount);

            // 광고 시청 혹은 무료 캐쉬로 캐쉬를 얻었을 때 라벨
            //rewardResultLabel.text = string.Format(TextDocs.content("UI_EARN_CASH_TEXT"), EtcDocs.instance.adRewardCash);

            CheckPlayGacha();

            GameManager.instance.RefreshCash();

            CommonAdManager.instance.Fetch(CommonAdManager.AdType.Video);
        }
    }
    public void OnClickFreeReward()
    {
        SoundManager.PlayButtonClickSound();

        if(m_FreeGiftEnable)
        {
            GiftUI.OnEarnGift += OnEarnGift;

            GameManager.instance.ShowGift(GameConfig.GiftType.Free);
        }
        else
        {
            // 광고 시청
            GameManager.instance.exceptionPause = true;
            CommonAdManager.instance.Show(CommonAdManager.AdType.Video);            
        }
    }

    public void OnClickPlayGift()
    {
        SoundManager.PlayButtonClickSound();

        GiftUI.OnEarnGift += OnEarnGift;

        GameManager.instance.ShowGift(GameConfig.GiftType.Play);
    }

    public void OnClickGacha()
    {
        SoundManager.PlayButtonClickSound();

        m_UserMgr.preScene = GameConfig.Scene.GameScene;
        GameManager.instance.RestoreGameScene(GameConfig.Scene.GachaScene);
    }

    public void OnClickPetExperience()
    {
        if(m_ExperienceState != ExperienceState.HavePet)
        {
            SoundManager.PlayButtonClickSound();

            if(m_ExperienceState == ExperienceState.Enable)
            {
                m_UserMgr.curPetExperienceCount++;
                m_UserMgr.enablePetExperience = true;

                m_UserMgr.SaveUserData();

                NGUITools.SetActive(petExperienceButtonObject, false);
            }
            else if(m_ExperienceState == ExperienceState.End)
            {
                GameManager.instance.ShowPetPackage();
            }
        }
    }

    void PurchaseComplete(UserManager.PurchaseCompleteState purchaseCompleteState)
    {
        if (purchaseCompleteState == UserManager.PurchaseCompleteState.Success)
        {
            NGUITools.SetActive(petExperienceButtonObject, false);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class FrontHitArea : MonoBehaviour
{
    public Transform vehicleTransform;
    public float frontHitCheckDelay = 0.5f;

    bool m_FrontHit = false;
    public bool frontHit
    {
        get
        {
            return m_FrontHit;
        }
    }
    bool m_PauseCheck = false;


    float m_SideValue = 0.0f;
    public float sideValue
    {
        get
        {
            return m_SideValue;
        }
    }
    
    Vector3 m_VehiclePos = Vector3.zero;
    Vector3 m_ContactPos = Vector3.zero;
    Vector3 m_CrossResult = Vector3.zero;

    IEnumerator PauseCheck()
    {
        m_PauseCheck = true;

        yield return new WaitForSeconds(frontHitCheckDelay);

        m_PauseCheck = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if(!m_PauseCheck)
        {
            m_VehiclePos = vehicleTransform.position.normalized;
            m_VehiclePos.y = 0.0f;
            m_ContactPos = other.ClosestPointOnBounds(vehicleTransform.position).normalized;
            m_ContactPos.y = 0.0f;
            m_CrossResult = Vector3.Cross(m_VehiclePos, m_ContactPos);
            if (m_CrossResult.y < 0.0f)
            {
                m_SideValue = -1.0f;
            }
            else
            {
                m_SideValue = 1.0f;
            }

            m_FrontHit = true;

            StartCoroutine(PauseCheck());
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (!m_PauseCheck)
        {
            m_VehiclePos = vehicleTransform.position.normalized;
            m_VehiclePos.y = 0.0f;
            m_ContactPos = other.ClosestPointOnBounds(vehicleTransform.position).normalized;
            m_ContactPos.y = 0.0f;
            m_CrossResult = Vector3.Cross(m_VehiclePos, m_ContactPos);
            if (m_CrossResult.y < 0.0f)
            {
                m_SideValue = -1.0f;
            }
            else
            {
                m_SideValue = 1.0f;
            }

            m_FrontHit = true;

            StartCoroutine(PauseCheck());
        }
    }

    void OnTriggerExit(Collider other)
    {
        m_SideValue = 0.0f;
        m_FrontHit = false;
    }
}

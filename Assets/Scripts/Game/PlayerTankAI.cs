﻿using UnityEngine;
using System.Collections;

public class PlayerTankAI : WeMonoBehaviour
{
    public BattleParameter battleParam;
    public Transform fireTransform;
    public AudioSource rocketSFX;
    public Transform rocketBodyTransform;
    public Rigidbody playerRigidbody;

    float m_CurrentAttackDelay = 0.0f;

    bool m_IsTargetVisible;
    float m_TempDistance = 0.0f;

    GameObject m_TargetObject = null;
    Transform m_TargetTransform = null;

    GameObject m_NewTargetObject = null;
    Transform m_NewTargetTransform = null;

    Coroutine m_ExplosionCoroutine = null;
    SoundManager m_SoundMgr;

    void Start()
    {
        m_IsTargetVisible = false;
        m_CurrentAttackDelay = 0.0f;

        m_SoundMgr = SoundManager.instance;
    }

    void OnTriggerStay(Collider other)
    {
        if (GameManager.instance.state == GameManager.State.Playing)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
            {
                m_NewTargetObject = other.gameObject;
                CollisionConnector collisionConnector = m_NewTargetObject.GetComponent<CollisionConnector>();
                if (collisionConnector != null && collisionConnector.chaserAI != null)
                {
                    if (!collisionConnector.chaserAI.isDisable)
                    {
                        // 공격이 가능할 때만 거리 체크를 한다.
                        if (m_CurrentAttackDelay >= battleParam.attackDelay)
                        {
                            m_NewTargetTransform = other.gameObject.transform;
                            if (m_TargetTransform == null)
                            {
                                m_TargetObject = m_NewTargetObject;
                                m_TargetTransform = m_NewTargetTransform;
                            }
                            else
                            {
                                float oldTargetDistance = Vector3.Distance(m_Tr.position, m_TargetTransform.position);
                                float newTargetDistance = Vector3.Distance(m_Tr.position, m_NewTargetTransform.position);

                                if (newTargetDistance < oldTargetDistance)
                                {
                                    m_TargetObject = m_NewTargetObject;
                                    m_TargetTransform = m_NewTargetTransform;
                                }
                            }
                        }
                    }
                    else
                    {
                        m_TargetObject = null;
                        m_TargetTransform = null;
                    }
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            if (m_TargetObject != null && m_TargetObject == other.gameObject)
            {
                m_TargetObject = null;
                m_TargetTransform = null;
            }
        }
    }

    void Update()
    {
        if (GameManager.instance.state == GameManager.State.Playing)
        {
            m_CurrentAttackDelay += Time.deltaTime;

            if (m_TargetTransform != null)
            {
                Vector3 localTarget = rocketBodyTransform.InverseTransformPoint(m_TargetTransform.position);

                float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

                Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
                Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity);
                rocketBodyTransform.rotation = rocketBodyTransform.rotation * deltaRotation;

                if (m_CurrentAttackDelay >= battleParam.attackDelay)
                {
                    m_TempDistance = Vector3.Distance(m_Tr.position, m_TargetTransform.position);
                    if (m_TempDistance < 35.0f)
                    {
                        m_IsTargetVisible = !Physics.Linecast(fireTransform.position, m_TargetTransform.position, BattleControl.playerAttackBlockMaskStatic);
                        if (m_IsTargetVisible)
                        {
                            m_CurrentAttackDelay = 0.0f;

                            rocketSFX.pitch = Random.Range(0.9f, 1.1f);
                            rocketSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                            rocketSFX.Play();

                            Fire();
                        }
                    }
                }
            }
        }
    }

    private void Fire()
    {
        GameObject shellObject = ShellManager.instance.GetPlayerTankShell();
        shellObject.transform.parent = null;
        shellObject.transform.position = fireTransform.position;
        shellObject.transform.rotation = fireTransform.rotation;
        PlayerTankShellExplosion shell = shellObject.GetComponent<PlayerTankShellExplosion>();
        shell.battleParam = battleParam;
        Rigidbody shellRigidbody = shellObject.GetComponent<Rigidbody>();

        //Debug.Log("TankAI Velocity : " + m_Rigidbody.velocity + ", " + m_Rigidbody.velocity.magnitude + ", " + ratio.magnitude);        
        shellRigidbody.velocity = (battleParam.shellSpeed + playerRigidbody.velocity.magnitude) * fireTransform.forward;
    }
    
}

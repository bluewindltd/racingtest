﻿using UnityEngine;
using System.Collections;

public class Tree : MapObject
{
    protected override void Awake()
    {
        base.Awake();

        objectType = GameConfig.ObjectType.Tree;
        gameObject.layer = LayerMask.NameToLayer("Tree");
    }
}

﻿using UnityEngine;
using System.Collections;

public class GetCashEffect : MonoBehaviour
{
    //Vector3[] m_Path = null;
    //Vector3 m_StartPosition;
    //Vector3 m_MiddlePosition;
    //Vector3 m_EndPosition;

    //LTSpline m_Spline = null;

    public AnimationCurve animationCurve;
    public GameObject getCashEffectObject;

    GetCashEffectGroup m_GetCashEffectGroup;

    int m_PoolIndex = 0;

    public void Create(GetCashEffectGroup getCashEffectGroup, int poolIndex, Vector3 startPosition, LTSpline ltSpline, float delay, float aniTime)
    {
        getCashEffectObject.transform.localPosition = startPosition;
        getCashEffectObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        m_GetCashEffectGroup = getCashEffectGroup;
        m_PoolIndex = poolIndex;
        //Debug.Log("GetCashEffect : " + m_PoolIndex + ", " + delay + ", " + aniTime);        

        LeanTween.cancel(getCashEffectObject, true);
        LeanTween.delayedCall(getCashEffectObject, delay, ActiveEffect);
        LeanTween.moveSplineLocal(getCashEffectObject, ltSpline.pts, aniTime).setDelay(delay).setOrientToPath2d(true).setEase(animationCurve);
        LeanTween.scale(getCashEffectObject, new Vector3(0.5f, 0.5f, 1.0f), aniTime).setDelay(delay).setEase(animationCurve);
        LeanTween.delayedCall(getCashEffectObject, delay + 1.0f, IncreaseScore);
        LeanTween.scale(getCashEffectObject, new Vector3(1.2f, 1.2f, 1.0f), 0.1f).setDelay(delay + aniTime);
        LeanTween.scale(getCashEffectObject, new Vector3(0.5f, 0.5f, 1.0f), 0.2f).setDelay(delay + aniTime + 0.1f);
        LeanTween.delayedCall(getCashEffectObject, delay + aniTime + 0.3f, CompletedDestroy);
    }

    void IncreaseScore()
    {
        Debug.Log("GetCashEffect -> IncreaseScore: " + m_PoolIndex);
        GameManager.instance.IncreaseCash();
    }

    void ActiveEffect()
    {
        NGUITools.SetActive(getCashEffectObject, true);
    }

    void CompletedDestroy()
    {
        NGUITools.SetActive(getCashEffectObject, false);
        m_GetCashEffectGroup.UseEndedEffect(m_PoolIndex);
    }
}

﻿using UnityEngine;
using System.Collections;

public class ItemPoint : MapObject
{
    protected override void Awake()
    {
        base.Awake();

        objectType = GameConfig.ObjectType.ItemPoint;
        gameObject.layer = LayerMask.NameToLayer("ItemPoint");
    }
}

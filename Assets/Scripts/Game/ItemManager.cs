﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemManager : Singleton<ItemManager>
{
    public enum ItemType
    {
        HP = 0,
        Count,
    };

    public List<GameObject> itemTriggerPrefabList;
    List<ObjectPool> m_ItemTriggerPool = new List<ObjectPool>();

    int m_ItemCount = 0;

    Dictionary<int, ItemTrigger> m_ItemTriggerDic = new Dictionary<int, ItemTrigger>();
    public Dictionary<int, ItemTrigger> itemTriggerDic
    {
        get
        {
            return m_ItemTriggerDic;
        }
    }

    bool m_Initialize = false;
    GameManager m_GameMgr;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Initialize()
    {
        for (int i = 0; i < itemTriggerPrefabList.Count; i++)
        {
            ObjectPool itemObjectPool = new ObjectPool(10, itemTriggerPrefabList[i], transform);
            m_ItemTriggerPool.Add(itemObjectPool);
        }
    }

    public void StartUpdate()
    {
        m_Initialize = true;
        m_GameMgr = GameManager.instance;

        m_ItemCount = 0;
        m_ItemTriggerDic.Clear();
    }

    public ItemTrigger GenerateItem()
    {
        int itemType = UnityEngine.Random.Range(0, (int)ItemType.Count);

        if (itemType < m_ItemTriggerPool.Count)
        {
            GameObject itemTriggerObject = m_ItemTriggerPool[itemType].GetObjectFromPool();
            ItemTrigger itemTrigger = itemTriggerObject.GetComponent<ItemTrigger>();
            itemTrigger.itemIndex = m_ItemCount;
            m_ItemTriggerDic.Add(m_ItemCount, itemTrigger);
            m_ItemCount++;

            return itemTrigger;
        }
        else
        {
            return null;
        }
    }

    public ObjectPool GetItemTriggerPool(ItemType itemType)
    {
        if ((int)itemType < m_ItemTriggerPool.Count)
        {
            return m_ItemTriggerPool[(int)itemType];
        }
        else
        {
            return null;
        }
    }

    public void RemoveItemTrigger(int itemIndex)
    {
        if (m_ItemTriggerDic.ContainsKey(itemIndex))
        {
            m_ItemTriggerDic.Remove(itemIndex);
        }
    }
}

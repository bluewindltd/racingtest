﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RegionOutput
{
    public enum Direction
    {
        NotSpecified = -1,
        N = 2,
        E = 4,
        S = 8,
        W = 16
    }

    public static Dictionary<int, int> dx = new Dictionary<int, int>() { { (int)Direction.N, 0 }, { (int)Direction.E, 1 }, { (int)Direction.S, 0 }, { (int)Direction.W, -1 } };
    public static Dictionary<int, int> dz = new Dictionary<int, int>() { { (int)Direction.N, 1 }, { (int)Direction.E, 0 }, { (int)Direction.S, -1 }, { (int)Direction.W, 0 } };
    public static Dictionary<int, Direction> opposite = new Dictionary<int, Direction>() { { (int)Direction.N, Direction.S }, { (int)Direction.E, Direction.W }, { (int)Direction.S, Direction.N }, { (int)Direction.W, Direction.E } };
    public static Dictionary<int, Direction> rotateCW = new Dictionary<int, Direction>() { { (int)Direction.N, Direction.E }, { (int)Direction.E, Direction.S }, { (int)Direction.S, Direction.W }, { (int)Direction.W, Direction.N } };
    public static Dictionary<int, Direction> rotateCCW = new Dictionary<int, Direction>() { { (int)Direction.N, Direction.W }, { (int)Direction.W, Direction.S }, { (int)Direction.S, Direction.E }, { (int)Direction.E, Direction.N } };

    public List<Direction> outputDirList;

    public RegionOutput()
    {
        outputDirList = new List<Direction>();
    }

    public void SetRegionType(GameConfig.RegionType regionType)
    {
        switch (regionType)
        {
            case GameConfig.RegionType.V_Straight:
                {
                    outputDirList.Add(Direction.N);
                    outputDirList.Add(Direction.S);
                }
                break;
            case GameConfig.RegionType.H_Straight:
                {
                    outputDirList.Add(Direction.E);
                    outputDirList.Add(Direction.W);
                }
                break;
            case GameConfig.RegionType.NE_Corner:
                {
                    outputDirList.Add(Direction.N);
                    outputDirList.Add(Direction.E);
                }
                break;
            case GameConfig.RegionType.ES_Corner:
                {
                    outputDirList.Add(Direction.E);
                    outputDirList.Add(Direction.S);
                }
                break;
            case GameConfig.RegionType.SW_Corner:
                {
                    outputDirList.Add(Direction.S);
                    outputDirList.Add(Direction.W);
                }
                break;
            case GameConfig.RegionType.WN_Corner:
                {
                    outputDirList.Add(Direction.W);
                    outputDirList.Add(Direction.N);
                }
                break;
            case GameConfig.RegionType.NES_Intersection:
                {
                    outputDirList.Add(Direction.N);
                    outputDirList.Add(Direction.E);
                    outputDirList.Add(Direction.S);
                }
                break;
            case GameConfig.RegionType.ESW_Intersection:
                {
                    outputDirList.Add(Direction.E);
                    outputDirList.Add(Direction.S);
                    outputDirList.Add(Direction.W);
                }
                break;
            case GameConfig.RegionType.SWN_Intersection:
                {
                    outputDirList.Add(Direction.S);
                    outputDirList.Add(Direction.W);
                    outputDirList.Add(Direction.N);
                }
                break;
            case GameConfig.RegionType.WNE_Intersection:
                {
                    outputDirList.Add(Direction.W);
                    outputDirList.Add(Direction.N);
                    outputDirList.Add(Direction.E);
                }
                break;
            case GameConfig.RegionType.NESW_Intersection:
                {
                    outputDirList.Add(Direction.N);
                    outputDirList.Add(Direction.E);
                    outputDirList.Add(Direction.S);
                    outputDirList.Add(Direction.W);
                }
                break;
        }
    }

    public void Restore()
    {
        outputDirList.Clear();
    }
}

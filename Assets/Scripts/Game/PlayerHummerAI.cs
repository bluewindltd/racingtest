﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerHummerAI : WeMonoBehaviour
{
    public BattleParameter battleParam;
    public Transform gunMuzzleTransform;
    public Transform gunMuzzleEffectTransform;
    public ParticleSystem machineGunParticle;
    public AudioSource gunMuzzleSFX;
    public Transform gunRotorTransform;

    float m_CurrentAttackDelay = 0.0f;

    bool m_IsTargetVisible;
    float m_TempDistance = 0.0f;

    GameObject m_TargetObject = null;
    Transform m_TargetTransform = null;

    GameObject m_NewTargetObject = null;
    Transform m_NewTargetTransform = null;

    SoundManager m_SoundMgr;
    GameManager m_GameMgr;

    void Start()
    {
        m_CurrentAttackDelay = 0.0f;
        m_IsTargetVisible = false;

        m_SoundMgr = SoundManager.instance;
        m_GameMgr = GameManager.instance;
    }

    void OnTriggerStay(Collider other)
    {
        if (GameManager.instance.state == GameManager.State.Playing)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
            {
                m_NewTargetObject = other.gameObject;
                CollisionConnector collisionConnector = m_NewTargetObject.GetComponent<CollisionConnector>();
                if (collisionConnector != null && collisionConnector.chaserAI != null)
                {
                    if (!collisionConnector.chaserAI.isDisable)
                    {
                        // 공격이 가능할 때만 거리 체크를 한다.
                        if (m_CurrentAttackDelay >= battleParam.attackDelay)
                        {
                            m_NewTargetTransform = other.gameObject.transform;
                            if (m_TargetTransform == null)
                            {
                                m_TargetObject = m_NewTargetObject;
                                m_TargetTransform = m_NewTargetTransform;
                            }
                            else
                            {
                                float oldTargetDistance = Vector3.Distance(m_Tr.position, m_TargetTransform.position);
                                float newTargetDistance = Vector3.Distance(m_Tr.position, m_NewTargetTransform.position);

                                if (newTargetDistance < oldTargetDistance)
                                {
                                    m_TargetObject = m_NewTargetObject;
                                    m_TargetTransform = m_NewTargetTransform;
                                }
                            }
                        }
                    }
                    else
                    {
                        m_TargetObject = null;
                        m_TargetTransform = null;
                    }
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            if (m_TargetObject != null && m_TargetObject == other.gameObject)
            {
                m_TargetObject = null;
                m_TargetTransform = null;
            }
        }
    }

    void Update()
    {
        if (GameManager.instance.state == GameManager.State.Playing)
        {
            m_CurrentAttackDelay += Time.deltaTime;

            if (m_TargetTransform != null)
            {
                Vector3 localTarget = gunRotorTransform.InverseTransformPoint(m_TargetTransform.position);

                float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

                Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
                Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime * battleParam.rotorSpeed);
                gunRotorTransform.rotation = gunRotorTransform.rotation * deltaRotation;

                if (m_CurrentAttackDelay >= battleParam.attackDelay)
                {
                    m_TempDistance = Vector3.Distance(m_Tr.position, m_TargetTransform.position);
                    if (m_TempDistance < 15.0f)
                    {
                        m_IsTargetVisible = !Physics.Linecast(gunMuzzleTransform.position, m_TargetTransform.position, BattleControl.playerAttackBlockMaskStatic);
                        // Hummer 차량의 머신건 총구와 플레이어 간에 가리는 장애물이 없는지 체크
                        if (m_IsTargetVisible)
                        {
                            m_CurrentAttackDelay = 0.0f;

                            gunMuzzleSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                            gunMuzzleSFX.Play();

                            machineGunParticle.Emit(1);

                            EffectManager.instance.CreateGunEffect(m_Tr, gunMuzzleEffectTransform.position, gunMuzzleEffectTransform.rotation);

                            //Vector3 gunShootDirection = gunMuzzleTransform.forward + (-gunMuzzleTransform.up * 0.2f);
                            Debug.DrawRay(gunMuzzleTransform.position, gunMuzzleTransform.forward * 10.0f, Color.red);
                            RaycastHit hit;
                            if (Physics.Raycast(gunMuzzleTransform.position, gunMuzzleTransform.forward, out hit, 10.0f, BattleControl.playerAttackDamageMaskStatic))
                            {
                                StartCoroutine(BulletFlight(hit.point, hit.collider.gameObject, gunMuzzleTransform.forward,
                                        (Vector3.Distance(gunMuzzleTransform.transform.position, hit.point)) * 0.012f));
                            }
                        }
                    }
                }
            }
        }
    }

    IEnumerator BulletFlight(Vector3 point, GameObject target, Vector3 direction, float time)
    {
        yield return new WaitForSeconds(time);

        CollisionConnector collisionConnector = target.GetComponent<CollisionConnector>();

        if (collisionConnector != null)
        {
            if (collisionConnector.connectRigidbody != null)
            {
                if (target.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    collisionConnector.connectRigidbody.AddForce(direction * 500.0f, ForceMode.Acceleration);
                }
                //Instantiate(bulletImpactEmitterObject, point, Quaternion.LookRotation(hitNormal));

                m_GameMgr.ApplyDamage(collisionConnector, target, battleParam.attackDamage, false);
            }
        }
    }
}

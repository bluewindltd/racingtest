﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AkiraBikeAI : WeMonoBehaviour
{
    public Transform shootRoot;
    public Transform[] shootPointTransforms;
    public GameObject[] landMineObjectArray;
    public Transform[] landMineTransformArray;
    public Rigidbody[] landMineRigidbodyArray;
    public AkiraBikeLandMine[] landMineArray;
    public BattleParameter battleParam;

    List<Vector3> m_ShootDirection = new List<Vector3>();
    List<int> m_EnableLandMineList = new List<int>();
    Coroutine m_UpdateCoroutine = null;

    void Start()
    {
        for (int i = 0; i < landMineArray.Length; i++)
        {
            landMineArray[i].landMineIndex = i;
            landMineArray[i].akiraBikeAI = this;
            landMineArray[i].battleParam = battleParam;
            m_EnableLandMineList.Add(i);
        }
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    public void StopUpdate()
    {
        if (m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(battleParam.attackDelay);

            if (m_EnableLandMineList.Count > 0)
            {
                for (int i = 0; i < shootPointTransforms.Length; i++)
                {
                    if (m_EnableLandMineList.Count > 0)
                    {
                        int enableLandMineIndex = 0;
                        int landMineIndex = m_EnableLandMineList[enableLandMineIndex];

                        GameObject landMineObject = landMineObjectArray[landMineIndex];
                        Transform landMineTransform = landMineTransformArray[landMineIndex];
                        Rigidbody landMineRigidbody = landMineRigidbodyArray[landMineIndex];
                        AkiraBikeLandMine landMine = landMineArray[landMineIndex];

                        landMineObject.SetActive(true);

                        landMineTransform.parent = null;
                        //bulletTransform.position = shootPointTransform.position;
                        landMineTransform.localEulerAngles = new Vector3(UnityEngine.Random.Range(0.0f, 360.0f),
                            UnityEngine.Random.Range(0.0f, 360.0f),
                            UnityEngine.Random.Range(0.0f, 360.0f));
                        landMineRigidbody.velocity = (battleParam.shellSpeed/* + m_Rigidbody.velocity.magnitude*/) * shootPointTransforms[i].forward;

                        m_EnableLandMineList.RemoveAt(enableLandMineIndex);

                        landMine.Fire();
                    }
                    else
                    {
                        break;
                    }
                }                
            }
        }
    }

    public void ReturnLandMine(AkiraBikeLandMine landMine)
    {
        int landMineIndex = landMine.landMineIndex;

        GameObject landMineObject = landMineObjectArray[landMineIndex];
        Transform landMineTransform = landMineTransformArray[landMineIndex];
        Rigidbody landMineRigidbody = landMineRigidbodyArray[landMineIndex];

        landMineTransform.parent = shootRoot;
        landMineTransform.localPosition = Vector3.zero;
        landMineTransform.localEulerAngles = Vector3.zero;
        landMineRigidbody.velocity = Vector3.zero;
        landMineRigidbody.angularVelocity = Vector3.zero;

        landMineObject.SetActive(false);
        m_EnableLandMineList.Add(landMineIndex);
    }


    //public BattleParameter battleParam;
    //public Transform fireTransform;
    //public AudioSource rocketSFX;
    //public Transform rocketBodyTransform;
    //public Rigidbody playerRigidbody;

    //float m_CurrentAttackDelay = 0.0f;

    //bool m_IsTargetVisible;

    //GameObject m_TargetObject = null;
    //Transform m_TargetTransform = null;

    //GameObject m_NewTargetObject = null;
    //Transform m_NewTargetTransform = null;

    //SoundManager m_SoundMgr;

    //void Start()
    //{
    //    m_IsTargetVisible = false;
    //    m_CurrentAttackDelay = 0.0f;

    //    m_SoundMgr = SoundManager.instance;
    //}

    //void OnTriggerStay(Collider other)
    //{
    //    if (GameManager.instance.state == GameManager.State.Playing)
    //    {
    //        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
    //        {
    //            m_NewTargetObject = other.gameObject;
    //            CollisionConnector collisionConnector = m_NewTargetObject.GetComponent<CollisionConnector>();
    //            if (collisionConnector != null && collisionConnector.chaserAI != null)
    //            {
    //                if (!collisionConnector.chaserAI.isDisable)
    //                {
    //                    // 공격이 가능할 때만 거리 체크를 한다.
    //                    if (m_CurrentAttackDelay >= battleParam.attackDelay)
    //                    {
    //                        m_NewTargetTransform = other.gameObject.transform;
    //                        if (m_TargetTransform == null)
    //                        {
    //                            m_TargetObject = m_NewTargetObject;
    //                            m_TargetTransform = m_NewTargetTransform;
    //                        }
    //                        else
    //                        {
    //                            float oldTargetDistance = Vector3.Distance(m_Tr.position, m_TargetTransform.position);
    //                            float newTargetDistance = Vector3.Distance(m_Tr.position, m_NewTargetTransform.position);

    //                            if (newTargetDistance < oldTargetDistance)
    //                            {
    //                                m_TargetObject = m_NewTargetObject;
    //                                m_TargetTransform = m_NewTargetTransform;
    //                            }
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    m_TargetObject = null;
    //                    m_TargetTransform = null;
    //                }
    //            }
    //        }
    //    }
    //}

    //void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
    //    {
    //        if (m_TargetObject != null && m_TargetObject == other.gameObject)
    //        {
    //            m_TargetObject = null;
    //            m_TargetTransform = null;
    //        }
    //    }
    //}

    //void Update()
    //{
    //    if (GameManager.instance.state == GameManager.State.Playing)
    //    {
    //        m_CurrentAttackDelay += Time.deltaTime;

    //        Debug.DrawRay(rocketBodyTransform.position, rocketBodyTransform.forward * 50.0f, Color.red);

    //        if (m_TargetTransform != null)
    //        {
    //            if (m_CurrentAttackDelay >= battleParam.attackDelay)
    //            {
    //                m_IsTargetVisible = !Physics.Linecast(fireTransform.position, m_TargetTransform.position, BattleControl.playerAttackBlockMaskStatic);
    //                if (m_IsTargetVisible)
    //                {
    //                    Vector3 localTarget = rocketBodyTransform.InverseTransformPoint(m_TargetTransform.position);

    //                    float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

    //                    Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
    //                    Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity);
    //                    rocketBodyTransform.rotation = rocketBodyTransform.rotation * deltaRotation;

    //                    m_CurrentAttackDelay = 0.0f;

    //                    rocketSFX.pitch = Random.Range(0.9f, 1.1f);
    //                    rocketSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
    //                    rocketSFX.Play();

    //                    Fire();
    //                }
    //            }
    //        }
    //    }
    //}

    //private void Fire()
    //{
    //    GameObject shellObject = ShellManager.instance.GetPlayerTankShell();
    //    shellObject.transform.parent = null;
    //    shellObject.transform.position = rocketBodyTransform.position;
    //    shellObject.transform.rotation = rocketBodyTransform.rotation;
    //    PlayerTankShellExplosion shell = shellObject.GetComponent<PlayerTankShellExplosion>();
    //    shell.battleParam = battleParam;
    //    Rigidbody shellRigidbody = shellObject.GetComponent<Rigidbody>();

    //    //Debug.Log("TankAI Velocity : " + m_Rigidbody.velocity + ", " + m_Rigidbody.velocity.magnitude + ", " + ratio.magnitude);        
    //    shellRigidbody.velocity = (battleParam.shellSpeed + playerRigidbody.velocity.magnitude) * rocketBodyTransform.forward;
    //}
}

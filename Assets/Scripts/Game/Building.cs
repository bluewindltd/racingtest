﻿using UnityEngine;
using System.Collections;

public class Building : MapObject
{
    protected override void Awake()
    {
        base.Awake();

        objectType = GameConfig.ObjectType.Building;
        gameObject.layer = LayerMask.NameToLayer("Building");
    }
}

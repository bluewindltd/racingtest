﻿using UnityEngine;
using System.Collections;

public class BalloonCarBullet : MonoBehaviour
{
    int m_BulletIndex = 0;
    public int bulletIndex
    {
        get
        {
            return m_BulletIndex;
        }
        set
        {
            m_BulletIndex = value;
        }
    }

    BalloonCarAI m_BalloonCarAI;
    public BalloonCarAI balloonCarAI
    {
        set
        {
            m_BalloonCarAI = value;
        }
    }

    //public Rigidbody bulletRigidbody;

    //bool test = false;

    //void OnCollisionEnter(Collision col)
    //{
    //    if (col.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
    //    {
    //        if (!test)
    //        {
    //            test = true;
    //            Collider[] colliders = Physics.OverlapSphere(transform.position, 15.0f, BattleControl.playerAttackDamageMaskStatic);
    //            foreach (Collider c in colliders)
    //            {
    //                CollisionConnector collisionConnector = c.gameObject.GetComponent<CollisionConnector>();
    //                if (collisionConnector != null)
    //                {
    //                    collisionConnector.connectRigidbody.AddExplosionForce(10.0f, transform.position, 15.0f, 15.0f, ForceMode.Impulse);
    //                }
    //            }
    //        }
    //    }
    //}

    public IEnumerator StartReturnBullet()
    {
        //test = false;

        yield return new WaitForSeconds(5.0f);

        m_BalloonCarAI.ReturnBullet(this);
    }
}

﻿using UnityEngine;
using System.Collections;

public class Obstacle : MapObject
{
    public GameObject jointObject = null;
    public Transform jointTransform = null;

    Rigidbody m_RigidBody = null;
    Vector3 m_InitPos;
    Vector3 m_InitRot;

    protected override void Awake()
    {
        base.Awake();

        objectType = GameConfig.ObjectType.Obstacle;
        gameObject.layer = LayerMask.NameToLayer("Obstacle");

        m_RigidBody = GetComponent<Rigidbody>();

        m_InitPos = LocalPosition;
        m_InitRot = LocalEulerAngles;

        //if (m_Rigidbody != null)
        //{
        //    m_Rigidbody.isKinematic = true;
        //    m_Rigidbody.detectCollisions = false;
        //}
    }

    public void Initialize(bool onlyJointInit)
    {
        if(!onlyJointInit)
        {
            LocalPosition = m_InitPos;
            LocalEulerAngles = m_InitRot;
        }        
        if(jointObject != null)
        {
            FixedJoint fixedJoint = jointObject.AddMissingComponent<FixedJoint>();
            fixedJoint.connectedBody = m_RigidBody;
            fixedJoint.breakForce = 100;
            fixedJoint.breakTorque = 100;
            fixedJoint.enableCollision = false;
            fixedJoint.enablePreprocessing = true;

            jointTransform.localPosition = Vector3.zero;
            jointTransform.localEulerAngles = Vector3.zero;
        }
    }

    //void Start()
    //{
    //    StartCoroutine(TestRigidbody());
    //}

    //IEnumerator TestRigidbody()
    //{
    //    yield return new WaitForSeconds(1.0f);

    //    Rigidbody temp = gameObject.GetComponent<Rigidbody>();
    //    if (temp == null)
    //    {
    //        gameObject.AddComponent<Rigidbody>();
    //    }
    //}

    //void OnCollisionEnter(Collision col)
    //{
    //    Debug.Log("OnCollisionEnter : " + col.collider.name + ", " + col.relativeVelocity.sqrMagnitude);
    //    if(col.collider.name.CompareTo("frontcollider") == 0)
    //    {
    //        if(m_Rigidbody != null)
    //        {
    //            m_Rigidbody.isKinematic = false;
    //            m_Rigidbody.AddForce(col.impulse, ForceMode.Acceleration);
    //        }            
    //    }
    //    //if (m_Rigidbody != null)
    //    //{
    //    //    m_Rigidbody.isKinematic = false;
    //    //    m_Rigidbody.detectCollisions = true;
    //    //}
    //}
}

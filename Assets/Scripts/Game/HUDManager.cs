﻿using UnityEngine;
using System.Collections;

public class HUDManager : Singleton<HUDManager>
{
    public GameObject inGameUIRoot;
    public GameObject hpTextPrefab;
    ObjectPoolForNGUI m_HpTextObjectPool;
    public GameObject hpGagePrefab;
    ObjectPoolForNGUI m_HpGageObjectPool;

    void Start()
    {
#if BWTEST
        m_HpTextObjectPool = new ObjectPoolForNGUI(50, hpTextPrefab, inGameUIRoot);
#endif
        m_HpGageObjectPool = new ObjectPoolForNGUI(3, hpGagePrefab, inGameUIRoot);
    }

    public GameObject GetHpTextObject()
    {
#if BWTEST
        return m_HpTextObjectPool.GetObjectFromPool();
#else
        return null;
#endif
    }

    public void PutHpTextObject(GameObject hpTextObject)
    {
#if BWTEST
        m_HpTextObjectPool.PutObjectInPool(hpTextObject);
#endif
    }

    public GameObject GetHpGageObject()
    {
        return m_HpGageObjectPool.GetObjectFromPool();
    }

    public void PutHpGageObject(GameObject hpGageObject)
    {
        m_HpGageObjectPool.PutObjectInPool(hpGageObject);
    }
}

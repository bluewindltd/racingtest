﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BalloonCarAI : MonoBehaviour
{
    public Animator animator;
    public GameObject shootPointObject;
    public Transform shootPointTransform;
    public GameObject[] bulletObjectArray;
    public Transform[] bulletTransformArray;
    public Rigidbody[] bulletRigidbodyArray;
    public BalloonCarBullet[] bulletArray;
    public BattleParameter battleParam;

    List<int> m_EnableBulletList = new List<int>();
    Coroutine m_UpdateCoroutine = null;

    void Start()
    {
        for(int i = 0; i < bulletArray.Length; i++)
        {
            bulletArray[i].bulletIndex = i;
            bulletArray[i].balloonCarAI = this;
            m_EnableBulletList.Add(i);
        }
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    public void StopUpdate()
    {
        if(m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(battleParam.attackDelay);

            if(m_EnableBulletList.Count > 0)
            {
                animator.Play("Effect_Vehicle036_Muzzle_Shoot");

                int enableBulletIndex = UnityEngine.Random.Range(0, m_EnableBulletList.Count);
                int bulletIndex = m_EnableBulletList[enableBulletIndex];

                GameObject bulletObject = bulletObjectArray[bulletIndex];
                Transform bulletTransform = bulletTransformArray[bulletIndex];
                Rigidbody bulletRigidbody = bulletRigidbodyArray[bulletIndex];
                BalloonCarBullet bullet = bulletArray[bulletIndex];                

                StartCoroutine(bullet.StartReturnBullet());

                bulletObject.SetActive(true);

                bulletTransform.parent = null;
                //bulletTransform.position = shootPointTransform.position;
                bulletTransform.localEulerAngles = new Vector3(UnityEngine.Random.Range(0.0f, 360.0f),
                    UnityEngine.Random.Range(0.0f, 360.0f),
                    UnityEngine.Random.Range(0.0f, 360.0f));
                bulletRigidbody.velocity = (battleParam.shellSpeed/* + m_Rigidbody.velocity.magnitude*/) * shootPointTransform.forward;

                m_EnableBulletList.RemoveAt(enableBulletIndex);
            }
        }
    }

    public void ReturnBullet(BalloonCarBullet bullet)
    {
        int bulletIndex = bullet.bulletIndex;

        GameObject bulletObject = bulletObjectArray[bulletIndex];
        Transform bulletTransform = bulletTransformArray[bulletIndex];
        Rigidbody bulletRigidbody = bulletRigidbodyArray[bulletIndex];

        bulletTransform.parent = shootPointTransform;
        bulletTransform.localPosition= Vector3.zero;
        bulletTransform.localEulerAngles = Vector3.zero;
        bulletRigidbody.velocity = Vector3.zero;
        bulletRigidbody.angularVelocity = Vector3.zero;

        bulletObject.SetActive(false);
        m_EnableBulletList.Add(bulletIndex);
    }
}

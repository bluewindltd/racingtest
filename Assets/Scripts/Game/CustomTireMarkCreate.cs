﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Wheel))]
[DisallowMultipleComponent]
[AddComponentMenu("RVP/C#/Effects/Custom Tire Mark Creator", 0)]

//Class for creating tire marks
public class CustomTireMarkCreate : MonoBehaviour
{
    Transform tr;
    Wheel w;
    Mesh mesh;
    int[] tris;
    Vector3[] verts;
    Vector2[] uvs;
    Color[] colors;

    Vector3 leftPoint;
    Vector3 rightPoint;
    Vector3 leftPointPrev;
    Vector3 rightPointPrev;

    bool creatingMark;
    bool continueMark;//Continue making mark after current one ends
    GameObject curMark;//Current mark
    Transform curMarkTr;
    int curEdge;
    float gapDelay;//Gap between segments

    int curSurface = -1;//Current surface type
    int prevSurface = -1;//Previous surface type

    bool popped = false;
    bool poppedPrev = false;

    [Tooltip("How much the tire must slip before marks are created")]
    public float slipThreshold;
    float alwaysScrape;

    [Tooltip("Materials in array correspond to indices in surface types in GroundSurfaceMaster")]
    public Material[] tireMarkMaterials;

    [Tooltip("Materials in array correspond to indices in surface types in GroundSurfaceMaster")]
    public Material[] rimMarkMaterials;

    [Tooltip("Particles in array correspond to indices in surface types in GroundSurfaceMaster")]
    public ParticleSystem[] debrisParticles;
    public ParticleSystem sparks;
    float[] initialEmissionRates;
    ParticleSystem.MinMaxCurve zeroEmission = new ParticleSystem.MinMaxCurve(0);

    CustomTireMark m_CustomTireMark;

    //void Awake()
    //{
    //    verts = new Vector3[GlobalControl.tireMarkLengthStatic * 2];
    //    tris = new int[GlobalControl.tireMarkLengthStatic * 3];
    //    uvs = new Vector2[verts.Length];
    //    colors = new Color[verts.Length];

    //    initialEmissionRates = new float[debrisParticles.Length + 1];
    //}

    //void Start()
    //{
    //    tr = transform;
    //    w = GetComponent<Wheel>();

    //    for (int i = 0; i < debrisParticles.Length; i++)
    //    {
    //        initialEmissionRates[i] = debrisParticles[i].emission.rate.constantMax;
    //    }

    //    if (sparks)
    //    {
    //        initialEmissionRates[debrisParticles.Length] = sparks.emission.rate.constantMax;
    //    }
    //}

    //void Update()
    //{
    //    //Check for continuous marking
    //    if (w.grounded)
    //    {
    //        alwaysScrape = GroundSurfaceMaster.surfaceTypesStatic[w.contactPoint.surfaceType].alwaysScrape ? slipThreshold + Mathf.Min(0.5f, Mathf.Abs(w.rawRPM * 0.001f)) : 0;
    //    }
    //    else
    //    {
    //        alwaysScrape = 0;
    //    }

    //    //Create mark
    //    //Debug.Log(string.Format("SlipThreshold : {0}, {1}, {2}", w.sidewaysSlip, w.forwardSlip, (Mathf.Abs(F.MaxAbs2(w.sidewaysSlip, w.forwardSlip)))));
    //    if (w.grounded && (Mathf.Abs(F.MaxAbs2(w.sidewaysSlip, w.forwardSlip)) > slipThreshold || alwaysScrape > 0) && w.connected)
    //    {
    //        prevSurface = curSurface;
    //        curSurface = w.grounded ? w.contactPoint.surfaceType : -1;

    //        poppedPrev = popped;
    //        popped = w.popped;

    //        if (!creatingMark)
    //        {
    //            prevSurface = curSurface;
    //            StartMark();
    //        }
    //        else if (curSurface != prevSurface || popped != poppedPrev)
    //        {
    //            EndMark();
    //        }

    //        //Calculate segment points
    //        if (curMark)
    //        {
    //            Vector3 pointDir = Quaternion.AngleAxis(90, w.contactPoint.normal) * tr.right * (w.popped ? w.rimWidth : w.tireWidth);
    //            leftPoint = curMarkTr.InverseTransformPoint(w.contactPoint.point + pointDir * w.suspensionParent.flippedSideFactor * Mathf.Sign(w.rawRPM) + w.contactPoint.normal * GlobalControl.tireMarkHeightStatic);
    //            rightPoint = curMarkTr.InverseTransformPoint(w.contactPoint.point - pointDir * w.suspensionParent.flippedSideFactor * Mathf.Sign(w.rawRPM) + w.contactPoint.normal * GlobalControl.tireMarkHeightStatic);
    //        }
    //    }
    //    else if (creatingMark)
    //    {
    //        EndMark();
    //    }

    //    //Update mark if it's short enough, otherwise end it
    //    if (curEdge < GlobalControl.tireMarkLengthStatic && creatingMark)
    //    {
    //        UpdateMark();
    //    }
    //    else if (creatingMark)
    //    {
    //        EndMark();
    //    }

    //    //Set particle emission rates
    //    ParticleSystem.EmissionModule em;
    //    for (int i = 0; i < debrisParticles.Length; i++)
    //    {
    //        if (w.connected)
    //        {
    //            if (i == w.contactPoint.surfaceType)
    //            {
    //                if (GroundSurfaceMaster.surfaceTypesStatic[w.contactPoint.surfaceType].leaveSparks && w.popped)
    //                {
    //                    em = debrisParticles[i].emission;
    //                    em.rate = zeroEmission;

    //                    if (sparks)
    //                    {
    //                        em = sparks.emission;
    //                        em.rate = new ParticleSystem.MinMaxCurve(initialEmissionRates[debrisParticles.Length] * Mathf.Clamp01(Mathf.Abs(F.MaxAbs3(w.sidewaysSlip, w.forwardSlip, alwaysScrape)) - slipThreshold));
    //                    }
    //                }
    //                else
    //                {
    //                    em = debrisParticles[i].emission;
    //                    em.rate = new ParticleSystem.MinMaxCurve(initialEmissionRates[i] * Mathf.Clamp01(Mathf.Abs(F.MaxAbs3(w.sidewaysSlip, w.forwardSlip, alwaysScrape)) - slipThreshold));

    //                    if (sparks)
    //                    {
    //                        em = sparks.emission;
    //                        em.rate = zeroEmission;
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                em = debrisParticles[i].emission;
    //                em.rate = zeroEmission;
    //            }
    //        }
    //        else
    //        {
    //            em = debrisParticles[i].emission;
    //            em.rate = zeroEmission;

    //            if (sparks)
    //            {
    //                em = sparks.emission;
    //                em.rate = zeroEmission;
    //            }
    //        }
    //    }
    //}

    void StartMark()
    {
        //creatingMark = true;
        //curMark = TireMarkManager.instance.GetTireMark();
        //curMarkTr = curMark.transform;
        //// [dunkhoon] : parent가 active false 되면서 제대로 삭제 안되는 버그가 있어서 null로 수정
        //curMarkTr.parent = null;
        //m_CustomTireMark = curMark.GetComponent<CustomTireMark>();
        //m_CustomTireMark.fadeTime = -1;
        //m_CustomTireMark.fading = false;
        //m_CustomTireMark.alpha = 1;
        //MeshRenderer tempRend = curMark.GetComponent<MeshRenderer>();
        //tempRend.material = tireMarkMaterials[Mathf.Min(w.contactPoint.surfaceType, tireMarkMaterials.Length - 1)];
        
        //tempRend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        //mesh = curMark.GetComponent<MeshFilter>().mesh;
        ////verts = new Vector3[GlobalControl.tireMarkLengthStatic * 2];
        ////tris = new int[GlobalControl.tireMarkLengthStatic * 3];

        //verts[0] = Vector3.zero;
        //verts[1] = Vector3.zero;

        //tris[0] = 0;
        //tris[1] = 0;
        //tris[2] = 0;
        //tris[3] = 0;
        //tris[4] = 0;
        //tris[5] = 0;

        //if (continueMark)
        //{
        //    verts[0] = leftPointPrev;
        //    verts[1] = rightPointPrev;

        //    tris[0] = 0;
        //    tris[1] = 3;
        //    tris[2] = 1;
        //    tris[3] = 0;
        //    tris[4] = 2;
        //    tris[5] = 3;
        //}

        ////uvs = new Vector2[verts.Length];
        //uvs[0] = new Vector2(0, 0);
        //uvs[1] = new Vector2(1, 0);
        //uvs[2] = new Vector2(0, 1);
        //uvs[3] = new Vector2(1, 1);

        ////colors = new Color[verts.Length];
        //colors[0].a = 0;
        //colors[1].a = 0;

        //curEdge = 2;
        //gapDelay = GlobalControl.tireMarkGapStatic;
    }

    void UpdateMark()
    {
        //if (gapDelay == 0)
        //{
        //    float alpha = (curEdge < GlobalControl.tireMarkLengthStatic - 2 && curEdge > 5 ? 1 : 0) * Random.Range(Mathf.Clamp01(Mathf.Abs(F.MaxAbs3(w.sidewaysSlip, w.forwardSlip, alwaysScrape)) - slipThreshold) * 0.9f, Mathf.Clamp01(Mathf.Abs(F.MaxAbs3(w.sidewaysSlip, w.forwardSlip, alwaysScrape)) - slipThreshold));
        //    gapDelay = GlobalControl.tireMarkGapStatic;
        //    curEdge += 2;

        //    verts[curEdge] = leftPoint;
        //    verts[curEdge + 1] = rightPoint;

        //    for (int i = curEdge + 2; i < verts.Length; i++)
        //    {
        //        verts[i] = Mathf.Approximately(i * 0.5f, Mathf.Round(i * 0.5f)) ? leftPoint : rightPoint;
        //        colors[i].a = 0;
        //    }

        //    tris[curEdge * 3 - 3] = curEdge;
        //    tris[curEdge * 3 - 2] = curEdge + 3;
        //    tris[curEdge * 3 - 1] = curEdge + 1;
        //    tris[Mathf.Min(curEdge * 3, tris.Length - 1)] = curEdge;
        //    tris[Mathf.Min(curEdge * 3 + 1, tris.Length - 1)] = curEdge + 2;
        //    tris[Mathf.Min(curEdge * 3 + 2, tris.Length - 1)] = curEdge + 3;

        //    uvs[curEdge] = new Vector2(0, curEdge * 0.5f);
        //    uvs[curEdge + 1] = new Vector2(1, curEdge * 0.5f);

        //    colors[curEdge] = new Color(1, 1, 1, alpha);
        //    colors[curEdge + 1] = colors[curEdge];

        //    mesh.vertices = verts;
        //    mesh.triangles = tris;
        //    mesh.uv = uvs;
        //    mesh.colors = colors;
        //    mesh.RecalculateBounds();
        //}
        //else
        //{
        //    gapDelay = Mathf.Max(0, gapDelay - Time.deltaTime);
        //    verts[curEdge] = leftPoint;
        //    verts[curEdge + 1] = rightPoint;

        //    for (int i = curEdge + 2; i < verts.Length; i++)
        //    {
        //        verts[i] = Mathf.Approximately(i * 0.5f, Mathf.Round(i * 0.5f)) ? leftPoint : rightPoint;
        //        colors[i].a = 0;
        //    }

        //    mesh.vertices = verts;
        //    mesh.RecalculateBounds();
        //}
    }

    void EndMark()
    {
        //creatingMark = false;
        //leftPointPrev = verts[Mathf.RoundToInt(verts.Length * 0.5f)];
        //rightPointPrev = verts[Mathf.RoundToInt(verts.Length * 0.5f + 1)];
        //continueMark = false;

        //m_CustomTireMark = curMark.GetComponent<CustomTireMark>();
        //m_CustomTireMark.fadeTime = GlobalControl.tireFadeTimeStatic;
        //m_CustomTireMark.mesh = mesh;
        //m_CustomTireMark.colors = colors;
        //curMark = null;
        //curMarkTr = null;
        //mesh = null;
    }

    // [dunkhoon] : 차량이 비활성화 될 때 타이어 마크도 같이 사라지게 한다.
    public void Restore()
    {
        //if (creatingMark && curMark)
        //{
        //    EndMark();
        //}
    }

    //void OnDestroy()
    //{
    //    if (creatingMark && curMark)
    //    {
    //        EndMark();
    //    }
    //}
}
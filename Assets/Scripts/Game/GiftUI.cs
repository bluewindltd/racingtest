﻿using UnityEngine;
using System.Collections;

public class GiftUI : MonoBehaviour
{
    public delegate void EarnGiftDelegate(GameConfig.GiftType type);
    public static EarnGiftDelegate OnEarnGift = delegate (GameConfig.GiftType type) { };

    public delegate void RefreshCashDelegate(GameConfig.GiftType type, int earnCash);
    public static RefreshCashDelegate OnRefreshCash = delegate (GameConfig.GiftType type, int earnCash) { };

    GameConfig.GiftType m_Type;

    public Animator animator;
    public GameObject darkBGObject;
    public UISprite giftIconSprite;
    public UILabel giftGetButtonLabel;

    int m_Index = -1;
    int m_Amount = 0;

    public IEnumerator Show(GameConfig.GiftType type)
    {
        m_Type = type;

        NGUITools.SetActive(darkBGObject, true);

        yield return new WaitForEndOfFrame();

        animator.Play("Result_QuickLotto_Show");
    }

    public IEnumerator Hide()
    {
        NGUITools.SetActive(darkBGObject, false);

        animator.Play("Result_QuickLotto_Start");

        yield return new WaitForEndOfFrame();

        NGUITools.SetActive(gameObject, false);
    }

    public void EarnGift()
    {
        UserManager userMgr = UserManager.instance;

        if (m_Index != 4)
        {
            userMgr.tokenDic[m_Index] += m_Amount;            
        }
        else if (m_Index == 4)
        {
            userMgr.cash += m_Amount;

            if(OnRefreshCash != null)
            {
                OnRefreshCash(m_Type, m_Amount);
            }
        }

        if (m_Type == GameConfig.GiftType.Free)
        {
            userMgr.freeGiftTime = TimeManager.instance.GetOnlineTime() + (EtcDocs.instance.freeGiftTime * 60 * 1000);
        }
        else
        {
            userMgr.playGiftCount = 0;
        }

        userMgr.SaveUserData();

        if (OnEarnGift != null)
        {
            OnEarnGift(m_Type);
        }
    }

    public void OnClickOpenGift()
    {
        SoundManager.PlayButtonClickSound();

        UserManager userMgr = UserManager.instance;

        animator.Play("Result_QuickLotto_Open");

        m_Index = -1;
        m_Amount = 0;
        string giftName = "";
        if (m_Type == GameConfig.GiftType.Free)
        {
            m_Index = 4;
            giftName = TextDocs.content("UI_CASH_TEXT");
            m_Amount = userMgr.EarnFreeGift();
        }
        else
        {
            GiftDocs.instance.PlayGift(ref m_Index, ref m_Amount);
            if (m_Index != 4)
            {
                giftName = string.Format(TextDocs.content("UI_TOKEN_TEXT"), GameConfig.TokenName[m_Index]);
            }
            else if (m_Index == 4)
            {
                giftName = TextDocs.content("UI_CASH_TEXT");
                m_Amount = userMgr.EarnFreeGift();
            }
        }

        giftIconSprite.spriteName = GameConfig.GiftIconName[m_Index];
        giftGetButtonLabel.text = string.Format(TextDocs.content("BT_GIFT_GET_TEXT"), giftName, m_Amount);
    }

    public void OnClickGetGift()
    {
        SoundManager.PlayButtonClickSound();

        GameManager.instance.HideGift();
    }
}

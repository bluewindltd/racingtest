﻿using UnityEngine;
using System.Collections;

public class TankAI : WeMonoBehaviour
{
    public ChaserAI chaserAI;
    public BattleParameter battleParam;
    public Transform fireTransform;
    public AudioSource rocketSFX;
    //public float cannonDelay = 2.0f;
    //public int cannonDamage = 5;
    public Transform rocketBodyTransform;

    float m_CurrentAttackDelay = 0.0f;

    bool m_isStarupLaunch = false;  // 발사 시동 상태 체크

    Rigidbody m_Rigidbody;
    Coroutine m_ExplosionCoroutine = null;
    SoundManager m_SoundMgr;
    RegionManager m_RegionMgr;

    protected override void Awake()
    {
        base.Awake();

        m_Rigidbody = GetComponent<Rigidbody>();
        m_SoundMgr = SoundManager.instance;
    }

    public void Initialize()
    {
        m_CurrentAttackDelay = 0.0f;
        m_RegionMgr = RegionManager.instance;
        m_isStarupLaunch = false;
    }

    void Update()
    {
        if (chaserAI.isDisable == false)
        {
            Vector3 localTarget = rocketBodyTransform.InverseTransformPoint(m_RegionMgr.playerTransform.position);

            float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

            Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
            //Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime * battleParam.rotorSpeed);
            Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity);
            rocketBodyTransform.rotation = rocketBodyTransform.rotation * deltaRotation;

            m_CurrentAttackDelay += Time.deltaTime;
            if (m_CurrentAttackDelay >= battleParam.attackDelay)
            {
                float distance = Vector3.Distance(m_Tr.position, m_RegionMgr.playerTransform.position);
                if (distance < 35.0f)
                {
                    if (chaserAI.isTargetVisible)
                    {
                        m_CurrentAttackDelay = 0.0f;

                        if (GameManager.instance.state == GameManager.State.Playing)
                        {
                            rocketSFX.pitch = Random.Range(0.9f, 1.1f);
                            rocketSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                            rocketSFX.Play();
                        }

                        Fire();
                    }
                }
            }
            //if (chaserAI.isTargetVisible)
            //{
            //    if (m_isStarupLaunch == false)
            //    {
                    
            //            m_isStarupLaunch = true;
            //        }
            //    }
            //    else
            //    {
            //        m_CurrentCannonDelay += Time.deltaTime;
            //        if (m_CurrentCannonDelay >= cannonDelay)
            //        {
            //            m_isStarupLaunch = false;

            //            Fire();
            //        }
            //    }
            //}
        }
    }

    private void Fire()
    {
        GameObject shellObject = ShellManager.instance.GetTankShell();
        shellObject.transform.parent = null;
        shellObject.transform.position = fireTransform.position;
        shellObject.transform.rotation = fireTransform.rotation;
        TankShellExplosion shell = shellObject.GetComponent<TankShellExplosion>();
        shell.battleParam = battleParam;
        Rigidbody shellRigidbody = shellObject.GetComponent<Rigidbody>();

        //Debug.Log("TankAI Velocity : " + m_Rigidbody.velocity + ", " + m_Rigidbody.velocity.magnitude + ", " + ratio.magnitude);        
        shellRigidbody.velocity = (battleParam.shellSpeed + m_Rigidbody.velocity.magnitude) * fireTransform.forward;
    }
}

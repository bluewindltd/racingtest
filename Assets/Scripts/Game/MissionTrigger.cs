﻿using UnityEngine;
using System.Collections;

public class MissionTrigger : MonoBehaviour
{
    Transform m_ObjectPoolParent = null;
    public GameConfig.MissionType type;
    public Animator animator;
    public BoxCollider triggerCollider;

    int m_MissionIndex = -1;
    public int missionIndex
    {
        get
        {
            return m_MissionIndex;
        }
        set
        {
            m_MissionIndex = value;
        }
    }

    public void SetMission(Transform parentMissionPoint)
    {
        m_ObjectPoolParent = transform.parent;

        transform.parent = parentMissionPoint;
        transform.localPosition = Vector3.zero;
    }

    public void IdleMission()
    {
        triggerCollider.enabled = true;
        if (type == GameConfig.MissionType.Chase)
        {
            animator.Play("MissionIcon_Chase_Idle");
        }
        else if(type == GameConfig.MissionType.Delivery)
        {
            animator.Play("MissionIcon_Deliver_Idle");
        }
    }

    public void Restore()
    {
        if (m_ObjectPoolParent != null)
        {
            transform.parent = m_ObjectPoolParent;
            m_ObjectPoolParent = null;
        }
        transform.localPosition = Vector3.zero;

        MissionManager.instance.RemoveMissionTrigger(m_MissionIndex);

        ObjectPool missionTriggerObjectPool = MissionManager.instance.GetMissionTriggerPool(type);
        missionTriggerObjectPool.PutObjectInPool(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (GameManager.instance.state == GameManager.State.Playing)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Vehicles"))
            {
                //Debug.Log("Mission Active!!");
                StartCoroutine(GetMission());

                MissionManager.instance.ProgressMissionTrigger(m_MissionIndex);
            }
        }
    }

    IEnumerator GetMission()
    {
        triggerCollider.enabled = false;
        if (type == GameConfig.MissionType.Chase)
        {
            animator.Play("MissionIcon_Chase_Get");
        }
        else if (type == GameConfig.MissionType.Delivery)
        {
            animator.Play("MissionIcon_Deliver_Get");
        }

        yield return new WaitForSeconds(0.8f);

        gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChaserSpawnPoint
{
    public Vector3 spawnPoint = new Vector3();
}

public class ChaserManager : Singleton<ChaserManager>
{
    public GameObject[] chaserObjectArray;
    //public GameObject policePrefab;
    //public GameObject swatPrefab;
    //public GameObject hummerPrefab;
    //public GameObject tankPrefab;
    //public GameObject gangsterPrefab;
    //public GameObject chopperHelicopterObject;
    //public GameObject apacheHelicopterObject;
    public Helicopter chopperHelicopter;
    public Helicopter apacheHelicopter;
    public ChopperAI chopperAI;
    public ApacheAI apacheAI;

    GameObject m_CurrentHelicopterObject = null;
    Helicopter m_CurrentHelicopter = null;

    int m_CenterX = -99999;
    int m_CenterZ = -99999;
    int m_CurrentChaserCarCount = 0;
    int m_CurrentGangChaserCarCount = 0;
    //SortedList m_SpawnPointList = new SortedList();
    List<Vector3> m_SpawnPointList = new List<Vector3>();

    Dictionary<int, ChaserAI> m_ChaserCarDic = new Dictionary<int, ChaserAI>();
    Dictionary<int, GangAI> m_GangCarDic = new Dictionary<int, GangAI>();

    Transform m_Tr;
    Dictionary<ChaserData.Type, ObjectPool> m_ObjectPoolDic = new Dictionary<ChaserData.Type, ObjectPool>();
    //Dictionary<ChaserData.Type, List<GameObject>> m_RestChaserDic = new Dictionary<ChaserData.Type, List<GameObject>>();

    GameManager m_GameMgr = null;
    RegionManager m_RegionMgr = null;
    ChaserDocs m_ChaserDocs = null;
    ChaserCountDocs m_ChaserCountDocs = null;
    MissionManager m_MissionMgr = null;

    int m_MaxChaserCar = 0;

    bool m_Initialize = false;

    float m_Timer = 0.0f;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        m_Tr = transform;

        m_ChaserDocs = ChaserDocs.instance;

        ChaserData chaserData;
        GameParameter gameParam;
        BattleParameter battleParam;
        for (int i = 0; i < chaserObjectArray.Length; i++)
        {
            if(m_ChaserDocs.chaserDataDic.ContainsKey((ChaserData.Type)i))
            {
                chaserData = m_ChaserDocs.chaserDataDic[(ChaserData.Type)i];
                gameParam = chaserObjectArray[i].GetComponent<GameParameter>();
                if(gameParam != null)
                {
                    gameParam.maxHealth = chaserData.health;
                    gameParam.damage = chaserData.damage;
                }
                battleParam = chaserObjectArray[i].GetComponent<BattleParameter>();
                if(battleParam != null)
                {
                    battleParam.attackDamage = chaserData.attackDamage;
                    battleParam.attackDelay = chaserData.attackDelay;
                    battleParam.shellSpeed = chaserData.shellSpeed;
                    battleParam.explosionRadius = chaserData.explosionRadius;
                }
            }
        }

        m_ObjectPoolDic.Add(ChaserData.Type.Police, new ObjectPool(10, chaserObjectArray[(int)ChaserData.Type.Police], m_Tr));
        m_ObjectPoolDic.Add(ChaserData.Type.SWAT, new ObjectPool(10, chaserObjectArray[(int)ChaserData.Type.SWAT], m_Tr));
        m_ObjectPoolDic.Add(ChaserData.Type.Hummer, new ObjectPool(10, chaserObjectArray[(int)ChaserData.Type.Hummer], m_Tr));
        m_ObjectPoolDic.Add(ChaserData.Type.Tank, new ObjectPool(10, chaserObjectArray[(int)ChaserData.Type.Tank], m_Tr));
        m_ObjectPoolDic.Add(ChaserData.Type.Gangster, new ObjectPool(10, chaserObjectArray[(int)ChaserData.Type.Gangster], m_Tr));

        //m_RestChaserDic.Add(ChaserData.Type.Police, new List<GameObject>());
        //m_RestChaserDic.Add(ChaserData.Type.SWAT, new List<GameObject>());
        //m_RestChaserDic.Add(ChaserData.Type.Hummer, new List<GameObject>());
        //m_RestChaserDic.Add(ChaserData.Type.Tank, new List<GameObject>());
        //m_RestChaserDic.Add(ChaserData.Type.Gangster, new List<GameObject>());
    }

    public void Restore()
    {
        m_Initialize = false;

        int chaserCarID = -1;
        Dictionary<int, ChaserAI>.Enumerator etor = m_ChaserCarDic.GetEnumerator();
        while(etor.MoveNext())
        {
            chaserCarID = etor.Current.Key;
            ObjectPool chaserObjectPool = null;
            ChaserAI chaserAI = etor.Current.Value;
            GameObject chaserCar = chaserAI.gameObject;
            switch (chaserAI.chaserClasses)
            {
                case ChaserData.Classes.Legal:
                    {
                        chaserObjectPool = m_ObjectPoolDic[chaserAI.chaserType];
                        m_CurrentChaserCarCount--;
                    }
                    break;

                case ChaserData.Classes.Illegal:
                    {
                        chaserObjectPool = m_ObjectPoolDic[ChaserData.Type.Gangster];
                        m_CurrentGangChaserCarCount--;

                        m_GangCarDic.Remove(chaserCarID);
                    }
                    break;
            }

            chaserObjectPool.PutObjectInPool(chaserCar);
        }

        m_ChaserCarDic.Clear();

        chopperAI.Restore();
        apacheAI.Restore();

        if (m_CurrentHelicopterObject != null)
        {
            m_CurrentHelicopterObject.SetActive(false);
            m_CurrentHelicopterObject = null;
        }
        if(m_CurrentHelicopter != null)
        {
            m_CurrentHelicopter.Exit();
            m_CurrentHelicopter = null;
        }
    }

    public void StartUpdate()
    {
        m_Initialize = true;

        m_CenterX = -99999;
        m_CenterZ = -99999;
        m_CurrentChaserCarCount = 0;
        m_CurrentGangChaserCarCount = 0;
        m_SpawnPointList.Clear();        

        chopperHelicopter.Initialize();
        apacheHelicopter.Initialize();

        chopperAI.Initialize();
        apacheAI.Initialize();

        m_GameMgr = GameManager.instance;
        m_RegionMgr = RegionManager.instance;
        m_MissionMgr = MissionManager.instance;
        m_ChaserCountDocs = ChaserCountDocs.instance;

        int evilCount = m_GameMgr.evilCount;
        m_MaxChaserCar = m_ChaserCountDocs.chaserCountDic[evilCount];
    }

    ChaserData.Type SeleteChaserType(int evilCount)
    {
        ChaserData.Type type = ChaserData.Type.Police;
        ChaserAppearRateInfo chaserAppearRateInfo = ChaserDocs.instance.appearRateDic[evilCount];
        float resultRate = UnityEngine.Random.Range(0.0f, chaserAppearRateInfo.sum);
        for(int i = 0; i < chaserAppearRateInfo.rateList.Count; i++)
        {
            ChaserAppearRate chaserAppearRate = chaserAppearRateInfo.rateList[i];
            if(chaserAppearRate.startRate <= resultRate &&
               resultRate < chaserAppearRate.endRate)
            {
                type = chaserAppearRate.type;
                break;
            }
        }

        return type;
    }

    void CheckApperHelicopter(int evilCount)
    {
        ChaserData.Type type = ChaserData.Type.None;
        ChaserAppearRateInfo chaserAppearRateInfo = ChaserDocs.instance.appearRateDic[evilCount];
        if(chaserAppearRateInfo.forceAppearList.Count > 0)
        {
            type = chaserAppearRateInfo.forceAppearList[0];
            if(m_CurrentHelicopter != null)
            {
                if(m_CurrentHelicopter.type != type)
                {
                    m_CurrentHelicopter.Exit();

                    m_CurrentHelicopterObject = null;
                    m_CurrentHelicopter = null;
                }
            }

            if (m_CurrentHelicopter == null)
            {
                if (type == ChaserData.Type.Chopper)
                {
                    if (chopperHelicopter.state == Helicopter.State.Hide)
                    {
                        m_CurrentHelicopterObject = chaserObjectArray[(int)ChaserData.Type.Chopper];
                        m_CurrentHelicopter = chopperHelicopter;
                        m_CurrentHelicopter.type = ChaserData.Type.Chopper;
                    }
                }
                else if (type == ChaserData.Type.Apache)
                {
                    if (apacheHelicopter.state == Helicopter.State.Hide)
                    {
                        m_CurrentHelicopterObject = chaserObjectArray[(int)ChaserData.Type.Apache];
                        m_CurrentHelicopter = apacheHelicopter;
                        m_CurrentHelicopter.type = ChaserData.Type.Apache;
                    }
                }

                m_CurrentHelicopterObject.SetActive(true);
                m_CurrentHelicopter.Appear();

                if (type == ChaserData.Type.Chopper)
                {
                    chopperAI.StartUpdate();
                }
                if (type == ChaserData.Type.Apache)
                {
                    chopperAI.Restore();
                    apacheAI.StartUpdate();
                }
            }            
        }
    }

    public bool IsCreateGangChaser()
    {
        bool result = false;
        if (m_Initialize)
        {
            if (m_GameMgr.state == GameManager.State.Playing)
            {
                int chaserGangCarCount = 0;
                if (m_MissionMgr.missionState == MissionManager.MissionState.Progress)
                {
                    if (m_MissionMgr.progressMissionInfo != null)
                    {
                        if (m_MissionMgr.progressMissionInfo.missionData.type == GameConfig.MissionType.Chase)
                        {
                            chaserGangCarCount = m_MissionMgr.progressMissionInfo.missionData.paramX - m_MissionMgr.progressMissionInfo.destroyCar;
                            if (m_CurrentGangChaserCarCount < chaserGangCarCount)
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    public IEnumerator CreateGangChaser()
    {
        BuildSpawnPoint();

        if (m_SpawnPointList.Count > 0)
        {
            GameObject chaseCar = m_ObjectPoolDic[ChaserData.Type.Gangster].GetObjectFromPool();

            //Vector3 spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
            //chaseCar.transform.position = spawnPos;

            //Vector3 targetDir = playerPos - spawnPos;
            //Vector3 localTarget = transform.InverseTransformPoint(playerPos);
            //float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
            //Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
            //Quaternion spawnRot = Quaternion.Euler(eulerAngleVelocity);
            //chaseCar.transform.rotation = spawnRot;

            ChaserAI chaserAI = chaseCar.GetComponent<ChaserAI>();
            GangAI gangAI = chaseCar.GetComponent<GangAI>();
            chaserAI.target = m_RegionMgr.playerTransform;
            chaserAI.chaserCarID = GetChaserCarID();
            chaserAI.chaserType = ChaserData.Type.Gangster;
            chaserAI.chaserClasses = ChaserData.Classes.Illegal;
            //chaserAI.spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
            chaserAI.spawnPos = m_SpawnPointList[0];
            chaserAI.Initialize();
            gangAI.Initialize();

            m_ChaserCarDic.Add(chaserAI.chaserCarID, chaserAI);
            m_GangCarDic.Add(chaserAI.chaserCarID, gangAI);

            m_CurrentGangChaserCarCount++;

            m_SpawnPointList.RemoveAt(0);            
        }

        yield return new WaitForEndOfFrame();
    }

    public bool IsCreateChaser()
    {
        bool result = false;
        if (m_Initialize)
        {
            if (m_GameMgr.state == GameManager.State.Playing)
            {
                int evilCount = m_GameMgr.evilCount;
                m_MaxChaserCar = m_ChaserCountDocs.chaserCountDic[evilCount];
                if (0 < evilCount && evilCount <= 6)
                {
                    if (m_CurrentChaserCarCount < m_MaxChaserCar)
                    {
                        result = true;
                    }

                    CheckApperHelicopter(evilCount);
                }
            }
        }

        return result;
    }

    public IEnumerator CreateChaser()
    {
        BuildSpawnPoint();

        if (m_SpawnPointList.Count > 0)
        {
            int evilCount = m_GameMgr.evilCount;

            ChaserData.Type type = SeleteChaserType(evilCount);
            GameObject chaseCar = m_ObjectPoolDic[type].GetObjectFromPool();

            //Vector3 spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
            //chaseCar.transform.position = spawnPos;

            //Vector3 targetDir = playerPos - spawnPos;
            //Vector3 localTarget = transform.InverseTransformPoint(playerPos);
            //float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
            //Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
            //Quaternion spawnRot = Quaternion.Euler(eulerAngleVelocity);
            //chaseCar.transform.rotation = spawnRot;

            ChaserAI chaserAI = chaseCar.GetComponent<ChaserAI>();
            chaserAI.target = m_RegionMgr.playerTransform;
            chaserAI.chaserCarID = GetChaserCarID();
            chaserAI.chaserType = type;
            chaserAI.chaserClasses = ChaserData.Classes.Legal;
            //chaserAI.spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
            chaserAI.spawnPos = m_SpawnPointList[0];
            chaserAI.Initialize();
            InitializeChaserCar(type, chaseCar);
            m_ChaserCarDic.Add(chaserAI.chaserCarID, chaserAI);

            m_CurrentChaserCarCount++;

            m_SpawnPointList.RemoveAt(0);
        }

        yield return new WaitForEndOfFrame();
    }

    //void Update()
    //{
    //    if (m_Initialize)
    //    {
    //        if (m_GameMgr.state == GameManager.State.Playing)
    //        {
    //            m_Timer += Time.deltaTime;

    //            BuildSpawnPoint();

    //            if (m_Timer > 2.0f)
    //            {
    //                int chaserGangCarCount = 0;
    //                if (m_MissionMgr.missionState == MissionManager.MissionState.Progress)
    //                {
    //                    if (m_MissionMgr.progressMissionInfo != null)
    //                    {
    //                        if (m_MissionMgr.progressMissionInfo.missionData.type == GameConfig.MissionType.Chase)
    //                        {
    //                            chaserGangCarCount = m_MissionMgr.progressMissionInfo.missionData.paramX - m_MissionMgr.progressMissionInfo.destroyCar;
    //                            if (m_CurrentGangChaserCarCount < chaserGangCarCount)
    //                            {
    //                                if (m_SpawnPointList.Count > 0)
    //                                {
    //                                    m_Timer = 0.0f;

    //                                    GameObject chaseCar = m_ObjectPoolDic[ChaserData.Type.Gangster].GetObjectFromPool();

    //                                    //Vector3 spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
    //                                    //chaseCar.transform.position = spawnPos;

    //                                    //Vector3 targetDir = playerPos - spawnPos;
    //                                    //Vector3 localTarget = transform.InverseTransformPoint(playerPos);
    //                                    //float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
    //                                    //Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
    //                                    //Quaternion spawnRot = Quaternion.Euler(eulerAngleVelocity);
    //                                    //chaseCar.transform.rotation = spawnRot;

    //                                    ChaserAI chaserAI = chaseCar.GetComponent<ChaserAI>();
    //                                    GangAI gangAI = chaseCar.GetComponent<GangAI>();
    //                                    chaserAI.target = m_RegionMgr.playerTransform;
    //                                    chaserAI.chaserCarID = GetChaserCarID();
    //                                    chaserAI.chaserType = ChaserData.Type.Gangster;
    //                                    chaserAI.chaserClasses = ChaserData.Classes.Illegal;
    //                                    //chaserAI.spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
    //                                    chaserAI.spawnPos = m_SpawnPointList[0];
    //                                    chaserAI.Initialize();
    //                                    gangAI.Initialize();

    //                                    m_ChaserCarDic.Add(chaserAI.chaserCarID, chaserAI);
    //                                    m_GangCarDic.Add(chaserAI.chaserCarID, gangAI);

    //                                    m_CurrentGangChaserCarCount++;

    //                                    m_SpawnPointList.RemoveAt(0);

    //                                    return;
    //                                }
    //                            }
    //                        }
    //                    }
    //                }

    //                int evilCount = m_GameMgr.evilCount;
    //                if (0 < evilCount && evilCount <= 6)
    //                {
    //                    if (m_CurrentChaserCarCount < m_MaxChaserCar)
    //                    {
    //                        if (m_SpawnPointList.Count > 0)
    //                        {
    //                            m_Timer = 0.0f;

    //                            ChaserData.Type type = SeleteChaserType(evilCount);
    //                            GameObject chaseCar = m_ObjectPoolDic[type].GetObjectFromPool();

    //                            //Vector3 spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
    //                            //chaseCar.transform.position = spawnPos;

    //                            //Vector3 targetDir = playerPos - spawnPos;
    //                            //Vector3 localTarget = transform.InverseTransformPoint(playerPos);
    //                            //float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
    //                            //Vector3 eulerAngleVelocity = new Vector3(0, angle, 0);
    //                            //Quaternion spawnRot = Quaternion.Euler(eulerAngleVelocity);
    //                            //chaseCar.transform.rotation = spawnRot;

    //                            ChaserAI chaserAI = chaseCar.GetComponent<ChaserAI>();
    //                            chaserAI.target = m_RegionMgr.playerTransform;
    //                            chaserAI.chaserCarID = GetChaserCarID();
    //                            chaserAI.chaserType = type;
    //                            chaserAI.chaserClasses = ChaserData.Classes.Legal;
    //                            //chaserAI.spawnPos = (Vector3)m_SpawnPointList.GetByIndex(0);
    //                            chaserAI.spawnPos = m_SpawnPointList[0];
    //                            chaserAI.Initialize();
    //                            InitializeChaserCar(type, chaseCar);
    //                            m_ChaserCarDic.Add(chaserAI.chaserCarID, chaserAI);

    //                            m_CurrentChaserCarCount++;

    //                            m_SpawnPointList.RemoveAt(0);
    //                        }
    //                    }

    //                    CheckApperHelicopter(evilCount);
    //                }
    //            }
    //        }
    //    }  
    //}

    void InitializeChaserCar(ChaserData.Type type, GameObject chaserCar)
    {
        switch(type)
        {
            case ChaserData.Type.Hummer:
                {
                    HummerAI hummerAI = chaserCar.GetComponent<HummerAI>();
                    hummerAI.Initialize();
                }
                break;

            //case ChaserData.Type.Police:
            //case ChaserData.Type.SWAT:
            //case ChaserData.Type.Hummer:
            case ChaserData.Type.Tank:
                {
                    TankAI tankAI = chaserCar.GetComponent<TankAI>();
                    tankAI.Initialize();
                }
                break;
        }
    }

    public void RemoveAllChaserCar()
    {
        List<int> chaserAIList = new List<int>();
        Dictionary<int, ChaserAI>.Enumerator etor = m_ChaserCarDic.GetEnumerator();
        while(etor.MoveNext())
        {
            chaserAIList.Add(etor.Current.Key);
        }

        for (int i = 0; i < chaserAIList.Count; i++)
        {
            RemoveChaserCar(chaserAIList[i]);
        }
    }

    public void RemoveAllGangCar()
    {
        StartCoroutine(RemoveAllGangCarCoroutine());
    }

    IEnumerator RemoveAllGangCarCoroutine()
    {
        List<int> gangAIList = new List<int>();
        Dictionary<int, GangAI>.Enumerator etor = m_GangCarDic.GetEnumerator();
        while (etor.MoveNext())
        {
            etor.Current.Value.ExplosionGangCar();
            gangAIList.Add(etor.Current.Key);
        }

        yield return new WaitForSeconds(2.0f);

        for (int i = 0; i < gangAIList.Count; i++)
        {
            RemoveChaserCar(gangAIList[i]);
        }
    }

    public void RemoveChaserCar(int chaserCarID)
    {
        if(m_ChaserCarDic.ContainsKey(chaserCarID))
        {
            ObjectPool chaserObjectPool = null;
            ChaserAI chaserAI = m_ChaserCarDic[chaserCarID];
            GameObject chaserCar = chaserAI.gameObject;
            switch(chaserAI.chaserClasses)
            {
                case ChaserData.Classes.Legal:
                    {
                        chaserObjectPool = m_ObjectPoolDic[chaserAI.chaserType];
                        m_CurrentChaserCarCount--;
                    }
                    break;

                case ChaserData.Classes.Illegal:
                    {
                        chaserObjectPool = m_ObjectPoolDic[ChaserData.Type.Gangster];
                        m_CurrentGangChaserCarCount--;

                        m_GangCarDic.Remove(chaserCarID);
                    }
                    break;
            }
            
            chaserObjectPool.PutObjectInPool(chaserCar);

            //Destroy(chaserCar);
            m_ChaserCarDic.Remove(chaserCarID);

            m_Timer = 0.0f;
        }
    }

    int GetChaserCarID()
    {
        int chaserCarID = 0;
        for(int i = 0; i < 50; i++)
        {
            if(!m_ChaserCarDic.ContainsKey(i))
            {
                chaserCarID = i;
                break;
            }
        }
        return chaserCarID;
    }

    public void BuildSpawnPoint()
    {
        int centerX = m_RegionMgr.centerX;
        int centerZ = m_RegionMgr.centerZ;

        if (m_CenterX != centerX || m_CenterZ != centerZ || (m_SpawnPointList.Count <= 0 && m_CurrentChaserCarCount < m_MaxChaserCar))
        {
            m_CenterX = centerX;
            m_CenterZ = centerZ;

            Vector3 playerPos = m_RegionMgr.playerTransform.position;
            Vector3 playerForward = m_RegionMgr.playerTransform.forward;

            m_SpawnPointList.Clear();

            // 위에서 얻어낸 추격 차량 생성 구간을 바탕으로 순서에 맞게 Region을 순회하며,
            // 추격 차량 생성 포인트 리스트를 구축한다.
            if (m_RegionMgr.searchOrderDic.ContainsKey(m_RegionMgr.angleIndex))
            {
                List<Vector2> searchOrderList = m_RegionMgr.searchOrderDic[m_RegionMgr.angleIndex];
                Vector2 directionVec = Vector2.zero;
                int x = 0;
                int z = 0;
                for(int i = 0; i < searchOrderList.Count; i++)
                {
                    directionVec = searchOrderList[i];
                    x = centerX + (int)directionVec.x;
                    z = centerZ + (int)directionVec.y;

                    if (centerX != x || centerZ != z)
                    {
                        Region region = m_RegionMgr.GetRegion(x, z);
                        if (region != null)
                        {
                            for (int j = 0; j < region.road.chaserSpawnPointList.Count; j++)
                            {
                                float distance = Vector3.Distance(region.road.chaserSpawnPointList[j].position, playerPos);

                                if (distance > 50.0f)
                                {
                                    Vector3 spawnPoint = region.road.chaserSpawnPointList[j].position;
                                    spawnPoint.y = 2.0f;
                                    m_SpawnPointList.Add(spawnPoint);
                                }
                            }
                        }
                    }
                }
            }


            //for (int x = centerX - 1; x <= centerX + 1; x++)
            //{
            //    for (int z = centerZ - 1; z <= centerZ + 1; z++)
            //    {
            //        if(centerX != x || centerZ != z)
            //        {
            //            Region region = m_RegionMgr.GetRegion(x, z);
            //            if (region != null)
            //            {
            //                for (int i = 0; i < region.road.chaserSpawnPointList.Count; i++)
            //                {
            //                    float distance = Vector3.Distance(region.road.chaserSpawnPointList[i].position, playerPos);

            //                    if (distance > 50.0f)
            //                    {
            //                        if (m_SpawnPointList.ContainsKey(distance))
            //                        {
            //                            while (true)
            //                            {
            //                                distance += 1.0f;
            //                                if (!m_SpawnPointList.ContainsKey(distance))
            //                                {
            //                                    break;
            //                                }
            //                            }
            //                        }

            //                        Vector3 spawnPoint = region.road.chaserSpawnPointList[i].position;
            //                        spawnPoint.y = 2.0f;
            //                        m_SpawnPointList.Add(distance, spawnPoint);
            //                    }
            //                }
            //            }
            //        }                    
            //    }
            //}
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShellManager : Singleton<ShellManager>
{
    public GameObject tankShellPrefab;
    public GameObject apacheShellPrefab;
    public GameObject petShellPrefab;
    public GameObject playerTankShellPrefab;
    public GameObject pumpkinShellPrefab;

    ObjectPool m_TankShellObjectPool;
    ObjectPool m_ApacheShellObjectPool;
    ObjectPool m_PetShellObjectPool;
    ObjectPool m_PlayerTankShellObjectPool;
    ObjectPool m_PumpkinShellObjectPool;

    Transform m_Transform;

    Dictionary<int, GameObject> m_TankShellObjectDic = new Dictionary<int, GameObject>();
    int m_TankShellCount = 0;

    Dictionary<int, GameObject> m_ApacheShellObjectDic = new Dictionary<int, GameObject>();
    int m_ApacheShellCount = 0;

    Dictionary<int, GameObject> m_PetShellObjectDic = new Dictionary<int, GameObject>();
    int m_PetShellCount = 0;

    Dictionary<int, GameObject> m_PlayerTankShellObjectDic = new Dictionary<int, GameObject>();
    int m_PlayerTankShellCount = 0;

    Dictionary<int, GameObject> m_PumpkinShellObjectDic = new Dictionary<int, GameObject>();
    int m_PumpkinShellCount = 0;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        m_Transform = GetComponent<Transform>();
        m_TankShellObjectPool = new ObjectPool(10, tankShellPrefab, m_Transform);
        m_ApacheShellObjectPool = new ObjectPool(4, apacheShellPrefab, m_Transform);
        m_PetShellObjectPool = new ObjectPool(4, petShellPrefab, m_Transform);
        m_PlayerTankShellObjectPool = new ObjectPool(4, playerTankShellPrefab, m_Transform);
        m_PumpkinShellObjectPool = new ObjectPool(20, pumpkinShellPrefab, m_Transform);
    }

    public void Restore()
    {
        GameObject shellObject;
        Dictionary<int, GameObject>.Enumerator etor = m_TankShellObjectDic.GetEnumerator();
        while (etor.MoveNext())
        {
            shellObject = etor.Current.Value;
            shellObject.transform.parent = m_Transform;
            m_TankShellObjectPool.PutObjectInPool(shellObject);
        }
        m_TankShellObjectDic.Clear();
        m_TankShellCount = 0;

        etor = m_ApacheShellObjectDic.GetEnumerator();
        while (etor.MoveNext())
        {
            shellObject = etor.Current.Value;
            shellObject.transform.parent = m_Transform;
            m_ApacheShellObjectPool.PutObjectInPool(shellObject);
        }
        m_ApacheShellObjectDic.Clear();
        m_ApacheShellCount = 0;

        etor = m_PetShellObjectDic.GetEnumerator();
        while (etor.MoveNext())
        {
            shellObject = etor.Current.Value;
            shellObject.transform.parent = m_Transform;
            m_PetShellObjectPool.PutObjectInPool(shellObject);
        }
        m_PetShellObjectDic.Clear();
        m_PetShellCount = 0;

        etor = m_PlayerTankShellObjectDic.GetEnumerator();
        while (etor.MoveNext())
        {
            shellObject = etor.Current.Value;
            shellObject.transform.parent = m_Transform;
            m_PlayerTankShellObjectPool.PutObjectInPool(shellObject);
        }
        m_PlayerTankShellObjectDic.Clear();
        m_PlayerTankShellCount = 0;

        etor = m_PumpkinShellObjectDic.GetEnumerator();
        while (etor.MoveNext())
        {
            shellObject = etor.Current.Value;
            shellObject.transform.parent = m_Transform;
            m_PumpkinShellObjectPool.PutObjectInPool(shellObject);
        }
        m_PumpkinShellObjectDic.Clear();
        m_PumpkinShellCount = 0;
    }

    public GameObject GetTankShell()
    {
        GameObject tankShellObject = m_TankShellObjectPool.GetObjectFromPool();
        TankShellExplosion tankShellExplosion = tankShellObject.GetComponent<TankShellExplosion>();
        tankShellExplosion.tankShellID = m_TankShellCount;
        m_TankShellObjectDic.Add(m_TankShellCount, tankShellObject);
        m_TankShellCount++;
        if (m_TankShellCount > 1000)
        {
            m_TankShellCount = 0;
        }
        return tankShellObject;
    }

    public void PutTankShell(TankShellExplosion tankShellExplosion)
    {
        tankShellExplosion.transform.parent = m_Transform;
        m_TankShellObjectPool.PutObjectInPool(tankShellExplosion.gameObject);
        m_TankShellObjectDic.Remove(tankShellExplosion.tankShellID);
    }

    public GameObject GetApacheShell()
    {
        GameObject apacheShellObject = m_ApacheShellObjectPool.GetObjectFromPool();
        ApacheShellExplosion apacheShellExplosion = apacheShellObject.GetComponent<ApacheShellExplosion>();
        apacheShellExplosion.apacheShellID = m_ApacheShellCount;
        m_ApacheShellObjectDic.Add(m_ApacheShellCount, apacheShellObject);
        m_ApacheShellCount++;
        if (m_ApacheShellCount > 1000)
        {
            m_ApacheShellCount = 0;
        }
        return apacheShellObject;
    }

    public void PutApacheShell(ApacheShellExplosion apacheShellExplosion)
    {
        apacheShellExplosion.transform.parent = m_Transform;
        m_ApacheShellObjectPool.PutObjectInPool(apacheShellExplosion.gameObject);
        m_ApacheShellObjectDic.Remove(apacheShellExplosion.apacheShellID);
    }

    public GameObject GetPetShell()
    {
        GameObject petShellObject = m_PetShellObjectPool.GetObjectFromPool();
        PetShellExplosion petShellExplosion = petShellObject.GetComponent<PetShellExplosion>();
        petShellExplosion.petShellID = m_PetShellCount;
        m_PetShellObjectDic.Add(m_PetShellCount, petShellObject);
        m_PetShellCount++;
        if (m_PetShellCount > 1000)
        {
            m_PetShellCount = 0;
        }
        return petShellObject;
    }

    public void PutPetShell(PetShellExplosion petShellExplosion)
    {
        petShellExplosion.transform.parent = m_Transform;
        m_PetShellObjectPool.PutObjectInPool(petShellExplosion.gameObject);
        m_PetShellObjectDic.Remove(petShellExplosion.petShellID);
    }

    public GameObject GetPlayerTankShell()
    {
        GameObject playerTankShellObject = m_PlayerTankShellObjectPool.GetObjectFromPool();
        PlayerTankShellExplosion playerTankShellExplosion = playerTankShellObject.GetComponent<PlayerTankShellExplosion>();
        playerTankShellExplosion.tankShellID = m_PlayerTankShellCount;
        m_PlayerTankShellObjectDic.Add(m_PlayerTankShellCount, playerTankShellObject);
        m_PlayerTankShellCount++;
        if (m_PlayerTankShellCount > 1000)
        {
            m_PlayerTankShellCount = 0;
        }
        return playerTankShellObject;
    }

    public void PutPlayerTankShell(PlayerTankShellExplosion playerTankShellExplosion)
    {
        playerTankShellExplosion.transform.parent = m_Transform;
        m_PlayerTankShellObjectPool.PutObjectInPool(playerTankShellExplosion.gameObject);
        m_PlayerTankShellObjectDic.Remove(playerTankShellExplosion.tankShellID);
    }

    public GameObject GetPumpkinShell()
    {
        GameObject pumpkinShellObject = m_PumpkinShellObjectPool.GetObjectFromPool();
        PumpkinShellExplosion pumpkinShellExplosion = pumpkinShellObject.GetComponent<PumpkinShellExplosion>();
        pumpkinShellExplosion.shellID = m_PumpkinShellCount;
        m_PumpkinShellObjectDic.Add(m_PumpkinShellCount, pumpkinShellObject);
        m_PumpkinShellCount++;
        if (m_PumpkinShellCount > 1000)
        {
            m_PumpkinShellCount = 0;
        }
        return pumpkinShellObject;
    }

    public void PutPumpkinShell(PumpkinShellExplosion pumpkinShellExplosion)
    {
        pumpkinShellExplosion.transform.parent = m_Transform;
        m_PumpkinShellObjectPool.PutObjectInPool(pumpkinShellExplosion.gameObject);
        m_PumpkinShellObjectDic.Remove(pumpkinShellExplosion.shellID);
    }
}

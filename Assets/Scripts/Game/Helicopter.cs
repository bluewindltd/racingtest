﻿using UnityEngine;
using System.Collections;

public class Helicopter : WeMonoBehaviour
{
    public enum State
    {
        Hide = 0,
        Appear = 1,
        Exit = 2,
    };
    public Transform mainRotorTransform;
    public Transform tailRotorTransform;

    public float maxRotorVelocity = 7200;			// degrees per second
    private float rotorRotation = 0.0f; 			// degrees... used for animating rotors

    public float maxTailRotorVelocity = 2200.0f;    // degrees per second
    private float tailRotorRotation = 0.0f;         // degrees... used for animating rotors
    
    public float smoothTime = 0.3f;
    public float xOffset = 13.0f;
    public float zOffset = 13.0f;

    Vector3 m_TargetVector = Vector3.zero;
    float m_Angle = 0.0f;
    Quaternion m_DeltaRotation = Quaternion.identity;
    State m_State = State.Hide;
    public State state
    {
        get
        {
            return m_State;
        }
    }
    ChaserData.Type m_Type;
    public ChaserData.Type type
    {
        get
        {
            return m_Type;
        }
        set
        {
            m_Type = value;
        }
    }

    RegionManager m_RegionMgr;

    public void Initialize()
    {
        m_State = State.Hide;
        m_RegionMgr = RegionManager.instance;
    }

    public void Appear()
    {
        float randomAppearX = UnityEngine.Random.Range(20.0f, 50.0f);
        float randomAppearZ = UnityEngine.Random.Range(20.0f, 50.0f);
        m_Tr.position = new Vector3(m_RegionMgr.playerTransform.position.x + randomAppearX, 50.0f, m_RegionMgr.playerTransform.position.z + randomAppearZ);

        m_State = State.Appear;
    }

    public void Exit()
    {
        m_State = State.Exit;
    }
    
    void Update()
    {
        mainRotorTransform.rotation = m_Tr.rotation * Quaternion.Euler(0, rotorRotation, 0);
        tailRotorTransform.rotation = m_Tr.rotation * Quaternion.Euler(tailRotorRotation, 0, 0);
        rotorRotation += maxRotorVelocity * Time.deltaTime;
        tailRotorRotation += maxTailRotorVelocity * Time.deltaTime;

        if(m_State == State.Appear)
        {
            m_Tr.position = new Vector3(Mathf.Lerp(m_Tr.position.x, m_RegionMgr.playerTransform.position.x + xOffset, Time.deltaTime * smoothTime),
                Mathf.Lerp(m_Tr.position.y, 15.0f, Time.deltaTime * smoothTime),
                Mathf.Lerp(m_Tr.position.z, m_RegionMgr.playerTransform.position.z + zOffset, Time.deltaTime * smoothTime));

            m_TargetVector = m_Tr.InverseTransformPoint(m_RegionMgr.playerTransform.position);

            m_Angle = Mathf.Atan2(m_TargetVector.x, m_TargetVector.z) * Mathf.Rad2Deg;

            Vector3 eulerAngleVelocity = new Vector3(0, m_Angle, 0);
            m_DeltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime * 2.0f);
            m_Tr.rotation = m_Tr.rotation * m_DeltaRotation;
        }
        else if(m_State == State.Exit)
        {
            if (Mathf.Abs(m_Tr.position.y - 50.0f) < 1.0f)
            {
                gameObject.SetActive(false);
            }
            else
            {
                m_Tr.position = new Vector3(Mathf.Lerp(m_Tr.position.x, m_RegionMgr.playerTransform.position.x + xOffset, Time.deltaTime * smoothTime),
                    Mathf.Lerp(m_Tr.position.y, 50.0f, Time.deltaTime * smoothTime),
                    Mathf.Lerp(m_Tr.position.z, m_RegionMgr.playerTransform.position.z + zOffset, Time.deltaTime * smoothTime));
            }
        }   
    }
}

﻿using UnityEngine;
using System.Collections;

public class MissionPoint : MapObject
{
    protected override void Awake()
    {
        base.Awake();

        objectType = GameConfig.ObjectType.MissionPoint;
        gameObject.layer = LayerMask.NameToLayer("MissionPoint");
    }
}

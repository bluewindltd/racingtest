﻿using UnityEngine;
using System.Collections;

public class PetShellAttackAI : WeMonoBehaviour
{
    public enum State
    {
        Idle = 0,
        Approach = 1, // 플레이어에게 접근 중
    }
    State m_State = State.Idle;

    public PetAI petAI;
    public BattleParameter battleParam;
    public Transform petTransform;
    public Transform centerRocketTransform;
    public AudioSource centerRocketSFX;
    public float approachDistance = 30.0f;
    float m_CurrentAttackDelay = 0.0f;

    public Rigidbody m_Rigidbody;
    bool m_IsTargetVisible;

    float m_TempDistance = 0.0f;
    Vector3 m_LookDirection = Vector3.zero;
    float m_LookDot = 0.0f;

    Coroutine m_UpdateCoroutine = null;

    SoundManager m_SoundMgr;
    RegionManager m_RegionMgr;

    public GameObject m_TargetObject = null;
    public Transform m_TargetTransform = null;

    GameObject m_NewTargetObject = null;
    Transform m_NewTargetTransform = null;

    protected override void Awake()
    {
        base.Awake();
        
        m_SoundMgr = SoundManager.instance;
        m_RegionMgr = RegionManager.instance;
    }

    public void Restore()
    {
        if (m_UpdateCoroutine != null)
        {
            StopCoroutine(m_UpdateCoroutine);
            m_UpdateCoroutine = null;
        }
    }

    public void Initialize()
    {
        m_CurrentAttackDelay = 0.0f;
        m_IsTargetVisible = false;
        m_TempDistance = 0.0f;
        m_LookDirection = Vector3.zero;
        m_LookDot = 0.0f;
    }

    public void StartUpdate()
    {
        if (m_UpdateCoroutine == null)
        {
            m_UpdateCoroutine = StartCoroutine(UpdateCoroutine());
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (GameManager.instance.state == GameManager.State.Playing)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
            {
                m_NewTargetObject = other.gameObject;
                CollisionConnector collisionConnector = m_NewTargetObject.GetComponent<CollisionConnector>();
                if (collisionConnector != null && collisionConnector.chaserAI != null)
                {
                    if (!collisionConnector.chaserAI.isDisable)
                    {
                        if (m_State == State.Idle)
                        {
                            // 공격이 가능할 때만 거리 체크를 한다.
                            if (m_CurrentAttackDelay >= battleParam.attackDelay)
                            {
                                m_NewTargetTransform = other.gameObject.transform;
                                if (m_TargetTransform == null)
                                {
                                    m_TargetObject = m_NewTargetObject;
                                    m_TargetTransform = m_NewTargetTransform;

                                    petAI.targetTransform = m_TargetTransform;
                                }
                                else
                                {
                                    float oldTargetDistance = Vector3.Distance(m_Tr.position, m_TargetTransform.position);
                                    float newTargetDistance = Vector3.Distance(m_Tr.position, m_NewTargetTransform.position);

                                    if (newTargetDistance < oldTargetDistance)
                                    {
                                        m_TargetObject = m_NewTargetObject;
                                        m_TargetTransform = m_NewTargetTransform;

                                        petAI.targetTransform = m_TargetTransform;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        m_TargetObject = null;
                        m_TargetTransform = null;
                        petAI.targetTransform = m_RegionMgr.playerTransform;
                    }
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Chase Vehicles"))
        {
            if(m_TargetObject != null && m_TargetObject == other.gameObject)
            {
                m_TargetObject = null;
                m_TargetTransform = null;
                petAI.targetTransform = m_RegionMgr.playerTransform;
            }
        }
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            m_CurrentAttackDelay += Time.deltaTime;

            // 플레이어와 거리 체크후 너무 멀리 떨어져있으면 무조건 플레이어에 붙게 한다.
            float playerDistance = Vector3.Distance(m_Tr.position, m_RegionMgr.playerTransform.position);
            if(playerDistance > approachDistance)
            {                
                m_TargetObject = null;
                m_TargetTransform = null;
                petAI.targetTransform = m_RegionMgr.playerTransform;

                m_State = State.Approach;
            }
            else
            {
                if (m_State == State.Approach)
                {
                    m_State = State.Idle;
                }
                else if(m_State == State.Idle)
                {
                    if (m_CurrentAttackDelay >= battleParam.attackDelay)
                    {
                        if (m_TargetTransform != null)
                        {
                            // 40m 이내일 경우만 공격하게 거리체크
                            m_TempDistance = Vector3.Distance(m_Tr.position, m_TargetTransform.position);
                            if (m_TempDistance <= 40.0f)
                            {
                                m_IsTargetVisible = !Physics.Linecast(m_Tr.position, m_TargetTransform.position, BattleControl.petAttackBlockMaskStatic);
                                // 헬기와 플레이어 간에 가리는 장애물이 없는지 체크
                                if (m_IsTargetVisible)
                                {
                                    m_LookDirection = m_TargetTransform.position - centerRocketTransform.position;
                                    m_LookDirection.Normalize();
                                    m_LookDot = Vector3.Dot(m_TargetTransform.up, m_LookDirection);
                                    if (-0.8f <= m_LookDot && m_LookDot <= -0.35f)
                                    {
                                        m_CurrentAttackDelay = 0.0f;

                                        centerRocketTransform.LookAt(m_TargetTransform.position);

                                        if (GameManager.instance.state == GameManager.State.Playing)
                                        {
                                            centerRocketSFX.pitch = Random.Range(0.9f, 1.1f);
                                            centerRocketSFX.volume = m_SoundMgr.GetSFXVolume() * m_SoundMgr.gameSFXVolumeControl;
                                            centerRocketSFX.Play();
                                        }

                                        Fire(centerRocketTransform);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            yield return null;
        }
    }

    private void Fire(Transform fireTransform)
    {
        GameObject shellObject = ShellManager.instance.GetPetShell();
        shellObject.transform.parent = null;
        shellObject.transform.position = fireTransform.position;
        shellObject.transform.rotation = fireTransform.rotation;
        PetShellExplosion shell = shellObject.GetComponent<PetShellExplosion>();
        shell.battleParam = battleParam;
        Rigidbody shellRigidbody = shellObject.GetComponent<Rigidbody>();

        //Debug.Log("TankAI Velocity : " + m_Rigidbody.velocity + ", " + m_Rigidbody.velocity.magnitude + ", " + ratio.magnitude);        
        shellRigidbody.velocity = (battleParam.shellSpeed + m_Rigidbody.velocity.magnitude) * fireTransform.forward;
    }

    //IEnumerator RocketFlight(Vector3 point, GameObject target, float time)
    //{
    //    yield return new WaitForSeconds(time);

    //    EffectManager.instance.CreateRocketEffect(null, point, Quaternion.identity);

    //    //CollisionConnector collisionConnector = target.GetComponent<CollisionConnector>();

    //    //if (collisionConnector != null)
    //    //{
    //    //    if (collisionConnector.connectRigidbody != null)
    //    //    {
    //    //        collisionConnector.connectRigidbody.AddForce(heliTransform.forward * 300);

    //    //        if (collisionConnector.gameParameter != null)
    //    //        {
    //    //            collisionConnector.gameParameter.currentHealth -= gunDamage;
    //    //        }
    //    //    }
    //    //}

    //    CollisionConnector collisionConnector = null;
    //    Collider[] colliders = Physics.OverlapSphere(point, battleParam.explosionRadius, BattleControl.enemyAttackDamageMaskStatic);
    //    for (int i = 0; i < colliders.Length; i++)
    //    {
    //        Collider hit = colliders[i];
    //        if (hit)
    //        {
    //            collisionConnector = hit.GetComponent<CollisionConnector>();
    //            if (collisionConnector != null)
    //            {
    //                if (collisionConnector.connectRigidbody != null)
    //                {
    //                    //collisionConnector.connectRigidbody.AddForce(Vector3.up * 300.0f, ForceMode.Acceleration);
    //                    collisionConnector.connectRigidbody.GetComponent<Rigidbody>().AddExplosionForce(50, point, 0.1f, 2.0f, ForceMode.Acceleration);
    //                }

    //                if (collisionConnector.gameParameter != null)
    //                {
    //                    collisionConnector.gameParameter.currentHealth -= battleParam.attackDamage;
    //                }
    //            }
    //        }
    //    }
    //}
}

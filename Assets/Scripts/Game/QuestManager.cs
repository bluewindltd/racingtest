﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using CodeStage.AntiCheat.ObscuredTypes;

public class QuestClientInfo
{
    public enum State
    {
        Normal = 0,
        Select = 1,
        Success = 2,
        Fail = 3,
    }

    public State state = State.Normal;
    public QuestClientData questClientData;
    public int friendshipLevel = 1;
    public int allExp = 0;
    public Dictionary<int, bool> getRewardDic = new Dictionary<int, bool>();
}

public class QuestProgressInfo
{
    public int curSeasonIndex = 0;
    public int curClientIndex = -1;
    public long endReceiveTime = 0;

    public bool isProgress = false;
    public int curQuestIndex = -1;
    public long endTime = 0;
    public int progressParam = 0;

    // 현재 진행 중인 퀘스트의 클라이언트 정보
    public QuestClientInfo questClientInfo = null;
    // 현재 진행 중인 시즌에 포함되있는 클라이언트들 목록
    public Dictionary<int, QuestClientInfo> questClientInfoDic = new Dictionary<int, QuestClientInfo>();
}

public class QuestManager : Singleton<QuestManager>
{
    public enum QuestTipState
    {
        None = 0,
        EnableReceiveQuest,
        CompleteQuest,
    };

    //public delegate void OnQuestRefreshDelegate();
    //public static OnQuestRefreshDelegate OnQuestRefresh = delegate () { };

    public delegate void OnQuestSuccessDelegate();
    public static OnQuestSuccessDelegate OnQuestSuccess = delegate () { };

    public delegate void OnQuestFailDelegate();
    public static OnQuestFailDelegate OnQuestFail = delegate () { };

    public delegate void OnQuestReturnDelegate();
    public static OnQuestReturnDelegate OnQuestReturn = delegate () { };

    public delegate void OnQuestTipDelegate(QuestTipState questTipState);
    public static OnQuestTipDelegate OnQuestTip = delegate (QuestTipState questTipState) { };

    // 경험치, 퀘스트 완료 보상 수량, 레벨업 여부, 레벨업 보상 지급 여부, 레벨업 보상, 레벨업 보상 수량
    public delegate void OnQuestCompleteDelegate(int exp, int completeRewardAmount, bool isLevelup, bool isLevelupReward, QuestFriendshipData nextFriendshipData);
    public static OnQuestCompleteDelegate OnQuestComplete = delegate (int exp, int completeRewardAmount, bool isLevelup, bool isLevelupReward, QuestFriendshipData nextFriendshipData) { };

    bool m_Initialize = false;

    QuestSeasonData m_CurSeasonData = null;
    public QuestSeasonData curSeasonData
    {
        get
        {
            return m_CurSeasonData;
        }
    }
    QuestClientData m_CurClientData = null;
    public QuestClientData curClientData
    {
        get
        {
            return m_CurClientData;
        }
    }
    QuestFriendshipData m_CurFriendshipData = null;
    public QuestFriendshipData curFriendshipData
    {
        get
        {
            return m_CurFriendshipData;
        }
    }
    QuestData m_CurQuestData = null;
    public QuestData curQuestData
    {
        get
        {
            return m_CurQuestData;
        }
    }

    QuestProgressInfo m_QuestProgressInfo = new QuestProgressInfo();
    public QuestProgressInfo questProgressInfo
    {
        get
        {
            return m_QuestProgressInfo;
        }
    }
    Dictionary<int, QuestClientInfo> m_QuestClientInfoDic = new Dictionary<int, QuestClientInfo>();
    public Dictionary<int, QuestClientInfo> questClientInfoDic
    {
        get
        {
            return m_QuestClientInfoDic;
        }
    }

    QuestDocs m_QuestDocs;
    TimeManager m_TimeMgr;

    void Awake()
    {
        DontDestroyOnLoad(this);

        m_QuestDocs = QuestDocs.instance;
        m_TimeMgr = TimeManager.instance;
    }

    void Update()
    {
        if (m_Initialize)
        {
            if (TimeManager.instance.GetTimeState())
            {
                long time = 0;
                long elapsedTimeL = 0;
                bool isQuestComplete = false;
                bool isQuestReceiveEnable = false;
                // 퀘스트 진행 중이다.
                // 시간을 체크해보고 아직 유효하면, 퀘스트 조건을 체크한다.
                // 완료를 했다면 m_QuestProgressInfo.curClientIndex를 Success로 변경
                // 진행 중이라면 m_QuestProgressInfo.curClientIndex를 Select로 변경
                // 시간이 지났다면 m_QuestProgressInfo.curClientIndex를 Fail로 변경
                if (m_QuestProgressInfo.isProgress && m_QuestProgressInfo.questClientInfo != null)
                {
                    time = TimeManager.instance.GetOnlineTime();
                    elapsedTimeL = m_QuestProgressInfo.endTime - time;
                    if (elapsedTimeL > 0)
                    {
                        // 완료
                        if (m_CurQuestData.param <= m_QuestProgressInfo.progressParam)
                        {
                            isQuestComplete = true;

                            if (m_QuestProgressInfo.questClientInfo.state == QuestClientInfo.State.Select)
                            {
                                m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Success;
                                if (OnQuestSuccess != null)
                                {
                                    OnQuestSuccess();
                                }

                                SaveQuestData();
                            }
                        }
                        // 진행 중
                        else
                        {
                            if (m_QuestProgressInfo.questClientInfo.state != QuestClientInfo.State.Select)
                            {
                                m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Select;

                                SaveQuestData();
                            }
                        }
                    }
                    // 실패
                    else
                    {
                        if (m_QuestProgressInfo.questClientInfo.state == QuestClientInfo.State.Select)
                        {
                            m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Fail;
                            if (OnQuestFail != null)
                            {
                                OnQuestFail();
                            }

                            SaveQuestData();
                        }
                    }
                }
                // 퀘스트 진행 중이 아니다.
                // 모든 클라이언트를 Normal 상태로 변경
                else
                {

                }

                time = TimeManager.instance.GetOnlineTime();
                elapsedTimeL = m_QuestProgressInfo.endReceiveTime - time;
                if (elapsedTimeL <= 0)
                {
                    isQuestReceiveEnable = true;
                }

                if (isQuestComplete)
                {
                    OnQuestTip(QuestTipState.CompleteQuest);
                }
                else
                {
                    if (isQuestReceiveEnable)
                    {
                        OnQuestTip(QuestTipState.EnableReceiveQuest);
                    }
                    else
                    {
                        OnQuestTip(QuestTipState.None);                        
                    }
                }
            }
        }
    }

    public JSONClass SaveJsonQuestData()
    {
        JSONClass jsonQuestData = new JSONClass();

        jsonQuestData.Add("curSeasonIndex", new JSONData((int)m_QuestProgressInfo.curSeasonIndex));
        jsonQuestData.Add("curClientIndex", new JSONData((int)m_QuestProgressInfo.curClientIndex));
        jsonQuestData.Add("endReceiveTime", new JSONData((double)m_QuestProgressInfo.endReceiveTime));
        jsonQuestData.Add("isProgress", new JSONData(m_QuestProgressInfo.isProgress));
        jsonQuestData.Add("curQuestIndex", new JSONData(m_QuestProgressInfo.curQuestIndex));
        jsonQuestData.Add("endTime", new JSONData((double)m_QuestProgressInfo.endTime));
        jsonQuestData.Add("progressParam", new JSONData(m_QuestProgressInfo.progressParam));

        QuestClientInfo questClientInfo = null;
        JSONArray jsonQuestClients = new JSONArray();
        jsonQuestData.Add("questClients", jsonQuestClients);
        Dictionary<int, QuestClientInfo>.Enumerator etor = m_QuestClientInfoDic.GetEnumerator();
        while (etor.MoveNext())
        {
            questClientInfo = etor.Current.Value;
            JSONClass jsonQuestClientnClass = new JSONClass();
            jsonQuestClientnClass.Add("index", new JSONData(etor.Current.Key));
            jsonQuestClientnClass.Add("friendshipLevel", new JSONData(questClientInfo.friendshipLevel));
            jsonQuestClientnClass.Add("allExp", new JSONData(questClientInfo.allExp));
            JSONArray jsonGetRewards = new JSONArray();
            jsonQuestClientnClass.Add("getRewards", jsonGetRewards);
            Dictionary<int, bool>.Enumerator etorGetReward = questClientInfo.getRewardDic.GetEnumerator();
            while (etorGetReward.MoveNext())
            {
                JSONClass jsonGetRewardClass = new JSONClass();
                jsonGetRewardClass.Add("level", new JSONData(etorGetReward.Current.Key));
                jsonGetRewardClass.Add("isGet", new JSONData(etorGetReward.Current.Value));
                jsonGetRewards.Add(jsonGetRewardClass);
            }

            jsonQuestClients.Add(jsonQuestClientnClass);
        }

        return jsonQuestData;
    }

    public string SaveQuestData()
    {
        JSONClass rootNode = new JSONClass();
        JSONClass jsonQuestData = SaveJsonQuestData();
        rootNode.Add("quest_data", jsonQuestData);

#if UNITY_EDITOR_WIN || UNITY_EDITOR
        string saveString = Convert.ToBase64String(Encoding.UTF8.GetBytes(rootNode.ToString()));
#else
        
        string saveString = Convert.ToBase64String(easy.Crypto.encrypt(rootNode.ToString(), UserManager.instance.encryptKey));
#endif

        ObscuredPrefs.SetString("QuestData", saveString);

        return saveString;
    }

    public void LoadJsonQuestData(JSONNode jsonQuestData)
    {
        JSONClass jsonClassQuestData = jsonQuestData.AsObject;
        if (jsonClassQuestData.Count > 0)
        {
            m_QuestProgressInfo.curSeasonIndex = jsonQuestData["curSeasonIndex"].AsInt;
            m_QuestProgressInfo.curClientIndex = jsonQuestData["curClientIndex"].AsInt;
            m_QuestProgressInfo.endReceiveTime = (long)jsonQuestData["endReceiveTime"].AsDouble;
            m_QuestProgressInfo.isProgress = jsonQuestData["isProgress"].AsBool;
            m_QuestProgressInfo.curQuestIndex = jsonQuestData["curQuestIndex"].AsInt;
            m_QuestProgressInfo.endTime = (long)jsonQuestData["endTime"].AsDouble;
            m_QuestProgressInfo.progressParam = jsonQuestData["progressParam"].AsInt;

            QuestClientInfo questClientInfo = null;
            QuestClientData questClientData = null;
            m_QuestClientInfoDic.Clear();
            JSONArray jsonQuestClients = jsonQuestData["questClients"].AsArray;
            for (int i = 0; i < jsonQuestClients.Count; i++)
            {
                JSONNode jsonQuestClientnClass = jsonQuestClients[i];
                int clientIndex = jsonQuestClientnClass["index"].AsInt;
                questClientInfo = new QuestClientInfo();

                CheckClientState(questClientInfo);

                if (m_QuestDocs.questClientDataDic.ContainsKey(clientIndex))
                {
                    questClientData = m_QuestDocs.questClientDataDic[clientIndex];
                    questClientInfo.questClientData = questClientData;
                }
                questClientInfo.friendshipLevel = jsonQuestClientnClass["friendshipLevel"].AsInt;
                questClientInfo.allExp = jsonQuestClientnClass["allExp"].AsInt;
                // 보상 획득 여부 정보 로드
                JSONArray jsonGetRewards = jsonQuestClientnClass["getRewards"].AsArray;
                for (int j = 0; j < jsonGetRewards.Count; j++)
                {
                    JSONNode jsonGetRewardClass = jsonGetRewards[j];
                    int level = jsonGetRewardClass["level"].AsInt;
                    bool isGet = jsonGetRewardClass["isGet"].AsBool;
                    questClientInfo.getRewardDic.Add(level, isGet);
                }

                m_QuestClientInfoDic.Add(clientIndex, questClientInfo);
            }

            m_QuestProgressInfo.questClientInfoDic.Clear();

            m_CurSeasonData = null;
            m_CurClientData = null;
            m_CurFriendshipData = null;
            m_CurQuestData = null;

            // 퀘스트가 진행 중일 때
            if (m_QuestProgressInfo.isProgress)
            {
                SetCurrentQuest(false);
            }
            else
            {
                m_QuestProgressInfo.questClientInfo = null;
                m_QuestProgressInfo.questClientInfoDic.Clear();

                m_CurSeasonData = null;
                m_CurClientData = null;
                m_CurFriendshipData = null;
                m_CurQuestData = null;
            }
        }        
    }

    public void LoadQuestData()
    {
        string loadString = ObscuredPrefs.GetString("QuestData", "");
        if (loadString.CompareTo("") != 0)
        {
#if UNITY_EDITOR_WIN || UNITY_EDITOR
            JSONNode json = JSON.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(loadString)));
#else
            JSONNode json = JSON.Parse(easy.Crypto.decrypt(Convert.FromBase64String(loadString), UserManager.instance.encryptKey));
#endif
            JSONNode jsonQuestData = json["quest_data"];

            LoadJsonQuestData(jsonQuestData);
        }
        else
        {
            m_QuestProgressInfo.curSeasonIndex = 0;
            m_QuestProgressInfo.curClientIndex = -1;
            m_QuestProgressInfo.endReceiveTime = 0;
            m_QuestProgressInfo.isProgress = false;
            m_QuestProgressInfo.curQuestIndex = -1;
            m_QuestProgressInfo.endTime = -1;
            m_QuestProgressInfo.progressParam = 0;
            m_QuestProgressInfo.questClientInfo = null;
            m_QuestProgressInfo.questClientInfoDic.Clear();

            QuestClientData questClientData = null;
            QuestClientInfo questClientInfo = null;
            m_QuestClientInfoDic.Clear();
            Dictionary<int, QuestClientData>.Enumerator etorClient = m_QuestDocs.questClientDataDic.GetEnumerator();
            while(etorClient.MoveNext())
            {
                questClientData = etorClient.Current.Value;
                questClientInfo = new QuestClientInfo();
                questClientInfo.state = QuestClientInfo.State.Normal;
                questClientInfo.questClientData = questClientData;
                questClientInfo.friendshipLevel = 1;
                questClientInfo.allExp = 0;
                for(int i = 1; i <= questClientData.friendshipDic.Count; i++)
                {
                    questClientInfo.getRewardDic.Add(i, false);
                }

                m_QuestClientInfoDic.Add(questClientData.index, questClientInfo);
            }

            m_CurSeasonData = null;
            m_CurClientData = null;
            m_CurFriendshipData = null;
            m_CurQuestData = null;
        }

        m_Initialize = true;
    }

    public void CheckClientState(QuestClientInfo questClientInfo)
    {
        if (m_TimeMgr.GetTimeState())
        {
            // 클라이언트 마다 퀘스트 상태를 체크한다.
            if (m_QuestProgressInfo.isProgress)
            {
                if (questClientInfo.questClientData.index == m_QuestProgressInfo.curClientIndex)
                {
                    long time = m_TimeMgr.GetOnlineTime();
                    long elapsedTimeL = m_QuestProgressInfo.endTime - time;
                    if (elapsedTimeL > 0)
                    {
                        // 완료
                        if (m_CurQuestData.param <= m_QuestProgressInfo.progressParam)
                        {
                            questClientInfo.state = QuestClientInfo.State.Success;
                        }
                        // 진행 중
                        else
                        {
                            questClientInfo.state = QuestClientInfo.State.Select;
                        }
                    }
                    // 실패
                    else
                    {
                        questClientInfo.state = QuestClientInfo.State.Fail;
                    }
                }
                else
                {
                    questClientInfo.state = QuestClientInfo.State.Normal;
                }
            }
            else
            {
                questClientInfo.state = QuestClientInfo.State.Normal;
            }
        }
        else
        {
            questClientInfo.state = QuestClientInfo.State.Normal;
        }
    }

    // 현재 진행되고 있는 퀘스트가 있는지 확인하고 있다면 상태를 갱신한다.
    public void RefreshCurrentQuest()
    {
        if(m_QuestProgressInfo.isProgress)
        {
            if (m_QuestProgressInfo.progressParam >= m_CurQuestData.param)
            {
                m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Success;
            }
            else
            {
                if(m_TimeMgr.GetTimeState())
                {
                    long time = m_TimeMgr.GetOnlineTime();
                    long elapsedTimeL = m_QuestProgressInfo.endTime - time;
                    if (elapsedTimeL <= 0)
                    {
                        m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Fail;
                    }
                    else
                    {
                        m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Select;
                    }
                }                
            }
        }        
    }

    // 현재 설정된 시즌, 클라이언트, 퀘스트 인덱스 정보로 퀘스트 정보를 세팅 한다.
    void SetCurrentQuest(bool newQuest)
    {
        QuestClientInfo questClientInfo = null;
        // 진행 중인 시즌 데이터를 찾고,
        // 진행 중인 시즌에 해당하는 클라이언트들을 찾는다.
        if (m_QuestDocs.questSeasonDataDic.ContainsKey(m_QuestProgressInfo.curSeasonIndex))
        {
            m_CurSeasonData = m_QuestDocs.questSeasonDataDic[m_QuestProgressInfo.curSeasonIndex];

            // QuestSeasonData에 클라이언트들 목록이 있다.
            m_CurSeasonData = m_QuestDocs.questSeasonDataDic[m_QuestProgressInfo.curSeasonIndex];
            int tempClientIndex = 0;
            // 클라이언트 목록을 순회하면서 진행되있는 클라이언트 저장데이터를 찾는다.
            Dictionary<int, QuestClientData>.Enumerator clientEtor = m_CurSeasonData.clientDic.GetEnumerator();
            while (clientEtor.MoveNext())
            {
                tempClientIndex = clientEtor.Current.Key;
                if (m_QuestClientInfoDic.ContainsKey(tempClientIndex))
                {
                    m_QuestProgressInfo.questClientInfoDic.Add(tempClientIndex, m_QuestClientInfoDic[tempClientIndex]);
                }
            }
        }
        if (m_QuestDocs.questClientDataDic.ContainsKey(m_QuestProgressInfo.curClientIndex))
        {
            // 현재 클라이언트 데이터를 찾는다.
            m_CurClientData = m_QuestDocs.questClientDataDic[m_QuestProgressInfo.curClientIndex];
            if (m_QuestClientInfoDic.ContainsKey(m_CurClientData.index))
            {
                // 현재 클라이언트의 저장데이터를 찾아 친밀도 레벨에 따른 정보를 찾는다.
                questClientInfo = m_QuestClientInfoDic[m_CurClientData.index];
                m_QuestProgressInfo.questClientInfo = questClientInfo;
                if (m_CurClientData.friendshipDic.ContainsKey(questClientInfo.friendshipLevel))
                {
                    m_CurFriendshipData = m_CurClientData.friendshipDic[questClientInfo.friendshipLevel];

                    // 새로운 퀘스트를 할당할 경우
                    if(newQuest)
                    {
                        m_QuestProgressInfo.curQuestIndex = UnityEngine.Random.Range(0, m_CurFriendshipData.questDataList.Count);
                        m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Select;
                    }

                    if (0 <= m_QuestProgressInfo.curQuestIndex &&
                        m_QuestProgressInfo.curQuestIndex < m_CurFriendshipData.questDataList.Count)
                    {
                        m_CurQuestData = m_CurFriendshipData.questDataList[m_QuestProgressInfo.curQuestIndex];
                    }
                }
            }
        }
    }

    // 퀘스트가 선택되었을 때 데이터 설정을 한다.
    public void SelectQuest(int seasonIndex, int clientIndex)
    {
        m_QuestProgressInfo.curSeasonIndex = seasonIndex;
        m_QuestProgressInfo.curClientIndex = clientIndex;
        m_QuestProgressInfo.isProgress = true;
        m_QuestProgressInfo.progressParam = 0;

        m_QuestProgressInfo.questClientInfoDic.Clear();

        m_CurSeasonData = null;
        m_CurClientData = null;
        m_CurFriendshipData = null;
        m_CurQuestData = null;

        SetCurrentQuest(true);

        // 퀘스트 수주 시간과 퀘스트 제한 시간을 각각 설정한다.
        long time = TimeManager.instance.GetOnlineTime();
        m_QuestProgressInfo.endReceiveTime = time + (m_CurSeasonData.receiveLimitTime * 1000);

        m_QuestProgressInfo.endTime = time + (m_CurQuestData.time * 1000);

        SaveQuestData();
    }

    public void CheckQuestData(QuestData.Type type)
    {
#if BWQUEST
        if(m_QuestProgressInfo.isProgress == true &&
           m_CurQuestData != null && m_CurQuestData.type == type &&
           m_QuestProgressInfo.progressParam < m_CurQuestData.param &&
           m_QuestProgressInfo.questClientInfo.state == QuestClientInfo.State.Select &&
           GameManager.instance.state == GameManager.State.Playing)
        {
            //switch (type)
            //{
            //    case QuestData.Type.DestroyGang:
            //        {

            //        }
            //        break;
            //    case QuestData.Type.ChaseMission:
            //        {

            //        }
            //        break;
            //    case QuestData.Type.DestroyPolice:
            //        {

            //        }
            //        break;
            //    case QuestData.Type.DeliveryMission:
            //        {

            //        }
            //        break;
            //    case QuestData.Type.EvilCount:
            //        {

            //        }
            //        break;
            //    case QuestData.Type.Arrest:
            //        {

            //        }
            //        break;
            //    case QuestData.Type.HPItem:
            //        {

            //        }
            //        break;
            //    case QuestData.Type.Waste:
            //        {

            //        }
            //        break;
            //}

            m_QuestProgressInfo.progressParam++;

            SaveQuestData();

            //if(OnQuestRefresh != null)
            //{
            //    OnQuestRefresh();
            //}
        }
#endif    
    }

    public void CompleteQuest()
    {
        if(m_QuestProgressInfo.isProgress)
        {
            int gapExp = 0;
            int exp = 0;
            bool isLevelup = false;
            bool isLevelupReward = false;
            QuestClientInfo questClientInfo = m_QuestProgressInfo.questClientInfo;
            QuestFriendshipData nextFriendshipData = null;
            if (m_CurQuestData.param <= m_QuestProgressInfo.progressParam)
            {
                if (m_CurClientData.lastFriendshipLevel > questClientInfo.friendshipLevel)
                {
                    gapExp = m_CurFriendshipData.endExp - questClientInfo.allExp;
                    questClientInfo.allExp += m_CurQuestData.exp;
                    exp = m_CurQuestData.exp;
                }
                else
                {
                    exp = 0;
                    questClientInfo.allExp = m_CurFriendshipData.endExp;
                }

                // 퀘스트 완료 기본 보상으로 rewardAmount 만큼 캐쉬 지급
                int rewardAmount = UnityEngine.Random.Range(m_CurQuestData.minRewardAmount, m_CurQuestData.maxRewardAmount);
                // 첫번째 보상은 무조건 캐쉬이므로 간단히 처리
                UserManager.instance.cash += rewardAmount;

                // 친밀도 레벨업!
                if (0 < exp &&
                    m_CurFriendshipData.endExp <= questClientInfo.allExp)
                {
                    isLevelup = true;

                    questClientInfo.friendshipLevel++;

                    if(m_CurClientData.lastFriendshipLevel <= questClientInfo.friendshipLevel)
                    {
                        exp = gapExp;
                        questClientInfo.allExp = m_CurFriendshipData.endExp;
                    }

                    if (questClientInfo.getRewardDic.ContainsKey(questClientInfo.friendshipLevel))
                    {
                        // 레벨업 보상을 얻은적 없어야 보상을 지급
                        if (!questClientInfo.getRewardDic[questClientInfo.friendshipLevel])
                        {
                            isLevelupReward = true;

                            if (m_CurClientData.friendshipDic.ContainsKey(questClientInfo.friendshipLevel))
                            {
                                nextFriendshipData = m_CurClientData.friendshipDic[questClientInfo.friendshipLevel];
                            }

                            // 레벨업 보상 지급
                            UserManager.instance.EarnRewardData(nextFriendshipData.levelUpRewardType, nextFriendshipData.levelUpRewardValue);

                            // 보상이 차량일 경우 게임내 플레이어 차량을 갱신해준다.
                            if(nextFriendshipData.levelUpRewardType == GameConfig.RewardType.Vehicle)
                            {
                                GameManager.instance.RefreshPlayer();
                            }

                            // 친밀도 레벨업 보상 지급했다고 표시
                            questClientInfo.getRewardDic[questClientInfo.friendshipLevel] = true;
                        }
                    }
                    else
                    {
                        // 마지막 레벨에 도달한 경우
                    }
                }

                if(OnQuestReturn != null)
                {
                    OnQuestReturn();
                }

                if (OnQuestComplete != null)
                {
                    // 경험치, 퀘스트 완료 보상 수량, 레벨업 여부, 레벨업 보상 지급 여부, 레벨업 보상, 레벨업 보상 수량
                    OnQuestComplete(exp, rewardAmount, isLevelup, isLevelupReward, nextFriendshipData);
                }

                m_QuestProgressInfo.curSeasonIndex = 0;
                m_QuestProgressInfo.curClientIndex = -1;
                m_QuestProgressInfo.isProgress = false;
                m_QuestProgressInfo.curQuestIndex = -1;
                m_QuestProgressInfo.endTime = 0;
                m_QuestProgressInfo.progressParam = 0;
                m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Normal;
                m_QuestProgressInfo.questClientInfo = null;
                m_QuestProgressInfo.questClientInfoDic.Clear();
                m_CurSeasonData = null;
                m_CurClientData = null;
                m_CurFriendshipData = null;
                m_CurQuestData = null;

                SaveQuestData();
            }
        }        
    }

    // 퀘스트 상태를 초기 상태로 돌려놓고, OnQuestReturn 델리게이트로 UI에 변경을 알린다.
    public void RefreshQuest()
    {
        if (m_QuestProgressInfo.isProgress)
        {
            QuestClientInfo questClientInfo = null;
            Dictionary<int, QuestClientInfo>.Enumerator etor = m_QuestProgressInfo.questClientInfoDic.GetEnumerator();
            while(etor.MoveNext())
            {
                questClientInfo = etor.Current.Value;
                questClientInfo.state = QuestClientInfo.State.Normal;
            }

            if (OnQuestReturn != null)
            {
                OnQuestReturn();
            }

            m_QuestProgressInfo.curSeasonIndex = 0;
            m_QuestProgressInfo.curClientIndex = -1;
            m_QuestProgressInfo.endReceiveTime = 0;
            m_QuestProgressInfo.isProgress = false;
            m_QuestProgressInfo.curQuestIndex = -1;
            m_QuestProgressInfo.endTime = 0;
            m_QuestProgressInfo.progressParam = 0;
            m_QuestProgressInfo.questClientInfo.state = QuestClientInfo.State.Normal;
            m_QuestProgressInfo.questClientInfo = null;
            m_QuestProgressInfo.questClientInfoDic.Clear();
            m_CurSeasonData = null;
            m_CurClientData = null;
            m_CurFriendshipData = null;
            m_CurQuestData = null;

            SaveQuestData();
        }
    }

#if BWTEST
    public void TestQuestComplete()
    {
        m_QuestProgressInfo.progressParam = m_CurQuestData.param;

        SaveQuestData();

        //if (OnQuestRefresh != null)
        //{
        //    OnQuestRefresh();
        //}
    }
#endif
}

﻿using UnityEngine;
using System.Collections;

public class TestGooglePlay : MonoBehaviour
{
    public UILabel testLabel;

	// Use this for initialization
	void Start ()
    {
        int systemMemorySize = SystemInfo.systemMemorySize;
        Debug.Log("FrameCheck : " + systemMemorySize);
        if (systemMemorySize >= 3500)
        {
            Application.targetFrameRate = 40;
            Time.fixedDeltaTime = 1.0f / 40.0f;
        }
        else
        {
            Application.targetFrameRate = 30;
            Time.fixedDeltaTime = 1.0f / 30.0f;
        }
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        GooglePlayConnection.ActionConnectionResultReceived += OnConnectionResultReceived;
        GooglePlayConnection.ActionPlayerDisconnected += OnDisconnectionResultReceived;
    }

    void OnDestroy()
    {
        GooglePlayConnection.ActionConnectionResultReceived -= OnConnectionResultReceived;
        GooglePlayConnection.ActionPlayerDisconnected -= OnDisconnectionResultReceived;
    }

    void OnConnectionResultReceived(GooglePlayConnectionResult result)
    {
        if (result.IsSuccess)
        {
            Debug.Log("UserManager Connected!");
            testLabel.text = "Connect Success!!";
        }
        else
        {
            testLabel.text = "Connect Fail!! : " + result.code.ToString();
        }
    }

    void OnDisconnectionResultReceived()
    {
        testLabel.text = "Disconnect!!";
    }

    public void OnClickConnect()
    {
        GooglePlayConnection.Instance.Connect();
    }

    public void OnClickDisconnect()
    {
        GooglePlayConnection.Instance.Disconnect();
    }

    public void OnClickAc()
    {
        GooglePlayManager.Instance.ShowAchievementsUI();
    }

    public void OnClickLeader()
    {
        GooglePlayManager.Instance.ShowLeaderBoardById("CgkImbbs5L8bEAIQBQ");
    }

    public void OnClickSave()
    {
        int maxNumberOfSavedGamesToShow = 1;
        GooglePlaySavedGamesManager.Instance.ShowSavedGamesUI("Save", maxNumberOfSavedGamesToShow, true, false);
    }

    public void OnClickLoad()
    {
        int maxNumberOfSavedGamesToShow = 1;
        GooglePlaySavedGamesManager.Instance.ShowSavedGamesUI("Load", maxNumberOfSavedGamesToShow, false, false);
    }
}

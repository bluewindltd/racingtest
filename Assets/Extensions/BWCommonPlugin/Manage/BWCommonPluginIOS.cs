﻿#if UNITY_IOS && !UNITY_EDITOR
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace BW.Common
{
    public class BWCommonPlugin : Singleton<BWCommonPlugin>
    {
        //--------------------------------------
        // Constants
        //--------------------------------------
        public const string DATA_SPLITTER = "|";

        //--------------------------------------
        // Variables
        //--------------------------------------

        //--------------------------------------
        // Actions
        //--------------------------------------
        public static Action<BW_PhoneNumberResult> OnGetPhoneNumberResult = delegate { };
        public static Action<string> OnGetDisplayCountryResult = delegate { };
        public static Action<string> OnGetCountryResult = delegate { };
        public static Action<string> OnGetLanguageResult = delegate { };
        public static Action<bool> OnSSLTrustResult = delegate { };
        public static Action<bool> OnTrustAllHostsResult = delegate { };
        public static Action<Dictionary<string, string>> OnCheckSelfPermissionResult = delegate { };
        public static Action<Dictionary<string, string>> OnShouldShowRequestPermissionRationaleResult = delegate { };
        public static Action<Dictionary<string, string>> OnRequestPermissionsResult = delegate { };
        public static Action OnGoDetailSettingResult = delegate () { };

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        // --------------------------------------
        // Public Methods
        // --------------------------------------

        public int GetSDKInt()
        {
            return 0;
        }

        public void AppLaunchOrGotoStore(string marketURL, string packageName)
        {
            // Android Only
            //if (Application.platform == RuntimePlatform.IPhonePlayer)
            //{
            //    _BWCommonAppLaunchOrGotoStore(marketURL, packageName);
            //}
        }

        public void GetPhoneNumber()
        {
            // Android Only
            //if (Application.platform == RuntimePlatform.IPhonePlayer)
            //{
            //    _BWCommonGetPhoneNumber();
            //}
        }

        public void SendSMS(string phoneNumber, string message)
        {
            // Android Only
            //if (Application.platform == RuntimePlatform.IPhonePlayer)
            //{
            //    _BWCommonSendSMS(phoneNumber, message);
            //}
        }

        public void GetDisplayCountry()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                _BWCommonGetDisplayCountry();
            }
        }

        public void GetCountry()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                _BWCommonGetCountry();
            }
        }

        public void GetLanguage()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                _BWCommonGetLanguage();
            }
        }

        public void SSLTrust(byte[] crtFileContent)
        {
        }

        public void TrustAllHosts()
        {
        }

        public void CheckSelfPermission(params string[] permissionNames)
        {
        }

        public void ShouldShowRequestPermissionRationale(params string[] permissionNames)
        {
        }

        public void RequestPermissions(params string[] permissionNames)
        {
        }

        public void GoDetailSetting()
        {
        }

        //--------------------------------------
        // Events
        //--------------------------------------

        private void OnGetPhoneNumberSuccess(string data)
        {
            Debug.Log("OnGetPhoneNumberSuccess : " + data);

            string[] phoneNumberData;
            phoneNumberData = data.Split(DATA_SPLITTER[0]);

            string phoneNumber = phoneNumberData[0];
            string countryCode = phoneNumberData[1];

            BW_PhoneNumberResult result = new BW_PhoneNumberResult(phoneNumber, countryCode, true);
            OnGetPhoneNumberResult(result);
        }

        private void OnGetPhoneNumberFail(string data)
        {
            Debug.Log("OnGetPhoneNumberFail : " + data);

            BW_PhoneNumberResult result = new BW_PhoneNumberResult("None", "None", false);
            OnGetPhoneNumberResult(result);
        }

        private void OnGetDisplayCountry(string data)
        {
            Debug.Log("OnGetDisplayCountry : " + data);

            OnGetDisplayCountryResult(data);
        }

        private void OnGetCountry(string data)
        {
            Debug.Log("OnGetCountry : " + data);

            OnGetCountryResult(data);
        }

        private void OnGetLanguage(string data)
        {
            Debug.Log("OnGetLanguage : " + data);

            OnGetLanguageResult(data);
        }

        private void OnSSLTrust(string data)
        {
            Debug.Log("OnSSLTrust : " + data);
        }

        private void OnTrustAllHosts(string data)
        {
            Debug.Log("OnTrustAllHosts : " + data);
        }

        private void OnCheckSelfPermission(string data)
        {
        }

        private void OnShouldShowRequestPermissionRationale(string data)
        {
        }

        private void OnRequestPermissions(string data)
        {
        }

        private void OnGoDetailSetting(string data)
        {
        }

        // --------------------------------------
        // Native Bridge
        // --------------------------------------
        [DllImport("__Internal")]
        private static extern void _BWCommonGetDisplayCountry();
        [DllImport("__Internal")]
        private static extern void _BWCommonGetCountry();
        [DllImport("__Internal")]
        private static extern void _BWCommonGetLanguage();

    }
}
#endif
﻿#if UNITY_ANDROID && !UNITY_EDITOR
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BW.Common
{
    public class BWCommonPlugin : Singleton<BWCommonPlugin>
    {
        //--------------------------------------
        // Constants
        //--------------------------------------
        public const string DATA_SPLITTER = "|";

        //--------------------------------------
        // Variables
        //--------------------------------------

        //--------------------------------------
        // Actions
        //--------------------------------------
        public static Action<BW_PhoneNumberResult> OnGetPhoneNumberResult = delegate { };
        public static Action<string> OnGetDisplayCountryResult = delegate { };
        public static Action<string> OnGetCountryResult = delegate { };
        public static Action<string> OnGetLanguageResult = delegate { };
        public static Action<bool> OnSSLTrustResult = delegate { };
        public static Action<bool> OnTrustAllHostsResult = delegate { };
        public static Action<Dictionary<string, string>> OnCheckSelfPermissionResult = delegate { };
        public static Action<Dictionary<string, string>> OnShouldShowRequestPermissionRationaleResult = delegate { };
        public static Action<Dictionary<string, string>> OnRequestPermissionsResult = delegate { };
        public static Action OnGoDetailSettingResult = delegate () { };

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        // --------------------------------------
        // Public Methods
        // --------------------------------------

        public int GetSDKInt()
        {
            using (var version = new AndroidJavaClass("android.os.Build$VERSION"))
            {
                return version.GetStatic<int>("SDK_INT");
            }
        }

        public void AppLaunchOrGotoStore(string marketURL, string packageName)
        {
            CallBWCommonPluginBridge("AppLaunchOrGotoStore", marketURL, packageName);
        }

        public void GetPhoneNumber()
        {
            CallBWCommonPluginBridge("GetPhoneNumber");
        }

        public void SendSMS(string phoneNumber, string message)
        {
            CallBWCommonPluginBridge("SendSMS", phoneNumber, message);
        }

        public void GetDisplayCountry()
        {
            CallBWCommonPluginBridge("GetDisplayCountry");
        }

        public void GetCountry()
        {
            CallBWCommonPluginBridge("GetCountry");
        }

        public void GetLanguage()
        {
            CallBWCommonPluginBridge("GetLanguage");
        }

        public void SSLTrust(byte[] crtFileContent)
        {
            CallBWCommonPluginBridge("SSLTrust", crtFileContent);
        }

        public void TrustAllHosts()
        {
            CallBWCommonPluginBridge("TrustAllHosts");
        }

        public void CheckSelfPermission(params string[] permissionNames)
        {
            Debug.Log("BWCommonPlugin CheckSelfPermission 1 : " + permissionNames.Length);
            if (permissionNames.Length > 0)
            {
                Debug.Log("BWCommonPlugin CheckSelfPermission 2 : " + string.Join(DATA_SPLITTER, permissionNames));
                CallBWCommonPluginBridge("CheckSelfPermission", string.Join(DATA_SPLITTER, permissionNames));
            }
        }

        public void ShouldShowRequestPermissionRationale(params string[] permissionNames)
        {
            Debug.Log("BWCommonPlugin ShouldShowRequestPermissionRationale 1 : " + permissionNames.Length);
            if (permissionNames.Length > 0)
            {
                Debug.Log("BWCommonPlugin ShouldShowRequestPermissionRationale 2 : " + string.Join(DATA_SPLITTER, permissionNames));
                CallBWCommonPluginBridge("ShouldShowRequestPermissionRationale", string.Join(DATA_SPLITTER, permissionNames));
            }
        }

        public void RequestPermissions(params string[] permissionNames)
        {
            Debug.Log("BWCommonPlugin RequestPermissions 1 : " + permissionNames.Length);
            if (permissionNames.Length > 0)
            {
                Debug.Log("BWCommonPlugin RequestPermissions 2 : " + string.Join(DATA_SPLITTER, permissionNames));
                CallBWCommonPluginBridge("RequestPermissions", string.Join(DATA_SPLITTER, permissionNames));
            }
        }

        public void GoDetailSetting()
        {
            CallBWCommonPluginBridge("GoDetailSetting");
        }

        //--------------------------------------
        // Events
        //--------------------------------------

        private void OnGetPhoneNumberSuccess(string data)
        {
            Debug.Log("OnGetPhoneNumberSuccess : " + data);

            string[] phoneNumberData;
            phoneNumberData = data.Split(DATA_SPLITTER[0]);

            string phoneNumber = phoneNumberData[0];
            string countryCode = phoneNumberData[1];

            BW_PhoneNumberResult result = new BW_PhoneNumberResult(phoneNumber, countryCode, true);
            OnGetPhoneNumberResult(result);
        }

        private void OnGetPhoneNumberFail(string data)
        {
            Debug.Log("OnGetPhoneNumberFail : " + data);

            BW_PhoneNumberResult result = new BW_PhoneNumberResult("None", "None", false);
            OnGetPhoneNumberResult(result);
        }

        private void OnGetDisplayCountry(string data)
        {
            Debug.Log("OnGetDisplayCountry : " + data);

            OnGetDisplayCountryResult(data);
        }

        private void OnGetCountry(string data)
        {
            Debug.Log("OnGetCountry : " + data);

            OnGetCountryResult(data);
        }

        private void OnGetLanguage(string data)
        {
            Debug.Log("OnGetLanguage : " + data);

            OnGetLanguageResult(data);
        }

        private void OnSSLTrust(string data)
        {
            Debug.Log("OnSSLTrust : " + data);

            if(data.CompareTo("TRUE") == 0)
            {
                OnSSLTrustResult(true);
            }
            else
            {
                OnSSLTrustResult(false);
            }
        }

        private void OnTrustAllHosts(string data)
        {
            Debug.Log("OnTrustAllHosts : " + data);

            if(data.CompareTo("TRUE") == 0)
            {
                OnTrustAllHostsResult(true);
            }
            else
            {
                OnTrustAllHostsResult(false);
            }
        }

        private void OnCheckSelfPermission(string data)
        {
            Debug.Log("OnCheckSelfPermission : " + data);

            Dictionary<string, string> resultDic = new Dictionary<string, string>();
            if (data.CompareTo("") != 0)
            {
                string[] result;
                result = data.Split(DATA_SPLITTER[0]);

                if (result.Length > 0 && (result.Length % 2 == 0))
                {
                    for (int i = 0; i < Mathf.FloorToInt((float)result.Length / 2.0f); i++)
                    {
                        string permissionName = result[i * 2];
                        string state = result[i * 2 + 1];

                        Debug.Log("OnCheckSelfPermission : " + i + ", " + permissionName + ", " + state);

                        resultDic.Add(permissionName, state);
                    }

                    OnCheckSelfPermissionResult(resultDic);
                }
                else
                {
                    OnCheckSelfPermissionResult(resultDic);
                }
            }
            else
            {
                OnCheckSelfPermissionResult(resultDic);
            }
        }

        private void OnShouldShowRequestPermissionRationale(string data)
        {
            Debug.Log("OnShouldShowRequestPermissionRationale : " + data);

            Dictionary<string, string> resultDic = new Dictionary<string, string>();
            if (data.CompareTo("") != 0)
            {
                string[] result;
                result = data.Split(DATA_SPLITTER[0]);

                if (result.Length > 0 && (result.Length % 2 == 0))
                {
                    for (int i = 0; i < Mathf.FloorToInt((float)result.Length / 2.0f); i++)
                    {
                        string permissionName = result[i * 2];
                        string state = result[i * 2 + 1];

                        Debug.Log("OnShouldShowRequestPermissionRationale : " + i + ", " + permissionName + ", " + state);

                        resultDic.Add(permissionName, state);
                    }

                    OnShouldShowRequestPermissionRationaleResult(resultDic);
                }
                else
                {
                    OnShouldShowRequestPermissionRationaleResult(resultDic);
                }
            }
            else
            {
                OnShouldShowRequestPermissionRationaleResult(resultDic);
            }
        }

        private void OnRequestPermissions(string data)
        {
            Debug.Log("OnRequestPermissions : " + data);

            Dictionary<string, string> resultDic = new Dictionary<string, string>();
            if(data.CompareTo("") != 0)
            {
                string[] result;
                result = data.Split(DATA_SPLITTER[0]);

                if (result.Length > 0 && (result.Length % 2 == 0))
                {
                    for (int i = 0; i < Mathf.FloorToInt((float)result.Length / 2.0f); i++)
                    {
                        string permissionName = result[i * 2];
                        string state = result[i * 2 + 1];

                        Debug.Log("OnRequestPermissions : " + i + ", " + permissionName + ", " + state);

                        resultDic.Add(permissionName, state);
                    }

                    OnRequestPermissionsResult(resultDic);
                }
                else
                {
                    OnRequestPermissionsResult(resultDic);
                }
            }
            else
            {
                OnRequestPermissionsResult(resultDic);
            }
        }

        private void OnGoDetailSetting(string data)
        {
            OnGoDetailSettingResult();
        }

        // --------------------------------------
        // Native Bridge
        // --------------------------------------
        private const string CLASS_NAME = "com.abluewind.bwcommonplugin.BCP_Bridge";

        private static void CallBWCommonPluginBridge(string methodName, params object[] args)
        {
            BCP_ProxyPool.CallStatic(CLASS_NAME, methodName, args);
        }

    }
}
#endif
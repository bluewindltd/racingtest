﻿#if UNITY_EDITOR
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BW.Common
{
    public class BWCommonPlugin : Singleton<BWCommonPlugin>
    {
        //--------------------------------------
        // Constants
        //--------------------------------------


        //--------------------------------------
        // Variables
        //--------------------------------------

        //--------------------------------------
        // Actions
        //--------------------------------------
        public static Action<BW_PhoneNumberResult> OnGetPhoneNumberResult = delegate { };
        public static Action<string> OnGetDisplayCountryResult = delegate { };
        public static Action<string> OnGetCountryResult = delegate { };
        public static Action<string> OnGetLanguageResult = delegate { };
        public static Action<bool> OnSSLTrustResult = delegate { };
        public static Action<bool> OnTrustAllHostsResult = delegate { };
        public static Action<Dictionary<string, string>> OnCheckSelfPermissionResult = delegate { };
        public static Action<Dictionary<string, string>> OnShouldShowRequestPermissionRationaleResult = delegate { };
        public static Action<Dictionary<string, string>> OnRequestPermissionsResult = delegate { };
        public static Action OnGoDetailSettingResult = delegate () { };

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        // --------------------------------------
        // Public Methods
        // --------------------------------------

        public int GetSDKInt()
        {
            return 0;
        }

        public void AppLaunchOrGotoStore(string marketURL, string packageName)
        {
        }

        public void GetPhoneNumber()
        {
        }

        public void SendSMS(string phoneNumber, string message)
        {
        }

        public void GetDisplayCountry()
        {
        }

        public void GetCountry()
        {
        }

        public void GetLanguage()
        {
        }

        public void SSLTrust(byte[] crtFileContent)
        {
        }

        public void TrustAllHosts()
        {
        }

        public void CheckSelfPermission(params string[] permissionNames)
        {
        }

        public void ShouldShowRequestPermissionRationale(params string[] permissionNames)
        {
        }

        public void RequestPermissions(params string[] permissionNames)
        {
        }

        public void GoDetailSetting()
        {
        }

        //--------------------------------------
        // Events
        //--------------------------------------

        private void OnGetPhoneNumberSuccess(string data)
        {
        }

        private void OnGetPhoneNumberFail(string data)
        {
        }

        private void OnGetDisplayCountry(string data)
        {
        }

        private void OnGetCountry(string data)
        {
        }

        private void OnGetLanguage(string data)
        {
        }

        private void OnSSLTrust(string data)
        {
        }

        private void OnTrustAllHosts(string data)
        {
        }

        private void OnCheckSelfPermission(string data)
        {
        }

        private void OnShouldShowRequestPermissionRationale(string data)
        {
        }

        private void OnRequestPermissions(string data)
        {
        }

        private void OnGoDetailSetting(string data)
        {
        }
    }
}
#endif
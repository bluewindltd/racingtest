﻿using UnityEngine;
using System.Collections;

namespace BW.Common
{
    public class BW_PhoneNumberResult : BW_Result
    {
        private string _phoneNumber;
        private string _countryCode;

        public BW_PhoneNumberResult(string phoneNumber, string countryCode, bool IsResultSucceeded)
            : base(IsResultSucceeded)
        {
            _phoneNumber = phoneNumber;
            _countryCode = countryCode;
        }


        public string phoneNumber
        {
            get
            {
                return _phoneNumber;
            }
        }

        public string countryCode
        {
            get
            {
                return _countryCode;
            }
        }
    }
}
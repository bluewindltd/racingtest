﻿using UnityEngine;
using System.Collections;

namespace BW.Common
{
    public class BW_Result
    {
        protected bool _IsSucceeded = true;

        public BW_Result(bool IsResultSucceeded)
        {
            _IsSucceeded = IsResultSucceeded;
        }

        public bool IsSucceeded
        {
            get
            {
                return _IsSucceeded;
            }
        }

        public bool IsFailed
        {
            get
            {
                return !_IsSucceeded;
            }
        }
    }
}
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//
//      This file was generated from this data file:
//      ./Assets/GameDataEditor/Resources/gde_data.txt
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDETextTableData : IGDEData
    {
        static string KoreanKey = "Korean";
		string _Korean;
        public string Korean
        {
            get { return _Korean; }
            set {
                if (_Korean != value)
                {
                    _Korean = value;
					GDEDataManager.SetString(_key, KoreanKey, _Korean);
                }
            }
        }

        static string EnglishKey = "English";
		string _English;
        public string English
        {
            get { return _English; }
            set {
                if (_English != value)
                {
                    _English = value;
					GDEDataManager.SetString(_key, EnglishKey, _English);
                }
            }
        }

        static string JapaneseKey = "Japanese";
		string _Japanese;
        public string Japanese
        {
            get { return _Japanese; }
            set {
                if (_Japanese != value)
                {
                    _Japanese = value;
					GDEDataManager.SetString(_key, JapaneseKey, _Japanese);
                }
            }
        }

        static string ChineseSimplifiedKey = "ChineseSimplified";
		string _ChineseSimplified;
        public string ChineseSimplified
        {
            get { return _ChineseSimplified; }
            set {
                if (_ChineseSimplified != value)
                {
                    _ChineseSimplified = value;
					GDEDataManager.SetString(_key, ChineseSimplifiedKey, _ChineseSimplified);
                }
            }
        }

        static string ChineseTraditionalKey = "ChineseTraditional";
		string _ChineseTraditional;
        public string ChineseTraditional
        {
            get { return _ChineseTraditional; }
            set {
                if (_ChineseTraditional != value)
                {
                    _ChineseTraditional = value;
					GDEDataManager.SetString(_key, ChineseTraditionalKey, _ChineseTraditional);
                }
            }
        }

        static string FrenchKey = "French";
		string _French;
        public string French
        {
            get { return _French; }
            set {
                if (_French != value)
                {
                    _French = value;
					GDEDataManager.SetString(_key, FrenchKey, _French);
                }
            }
        }

        static string GermanKey = "German";
		string _German;
        public string German
        {
            get { return _German; }
            set {
                if (_German != value)
                {
                    _German = value;
					GDEDataManager.SetString(_key, GermanKey, _German);
                }
            }
        }

        static string ItalianKey = "Italian";
		string _Italian;
        public string Italian
        {
            get { return _Italian; }
            set {
                if (_Italian != value)
                {
                    _Italian = value;
					GDEDataManager.SetString(_key, ItalianKey, _Italian);
                }
            }
        }

        static string SpanishKey = "Spanish";
		string _Spanish;
        public string Spanish
        {
            get { return _Spanish; }
            set {
                if (_Spanish != value)
                {
                    _Spanish = value;
					GDEDataManager.SetString(_key, SpanishKey, _Spanish);
                }
            }
        }

        static string RussianKey = "Russian";
		string _Russian;
        public string Russian
        {
            get { return _Russian; }
            set {
                if (_Russian != value)
                {
                    _Russian = value;
					GDEDataManager.SetString(_key, RussianKey, _Russian);
                }
            }
        }

        static string PortugeesKey = "Portugees";
		string _Portugees;
        public string Portugees
        {
            get { return _Portugees; }
            set {
                if (_Portugees != value)
                {
                    _Portugees = value;
					GDEDataManager.SetString(_key, PortugeesKey, _Portugees);
                }
            }
        }

        static string ThaiKey = "Thai";
		string _Thai;
        public string Thai
        {
            get { return _Thai; }
            set {
                if (_Thai != value)
                {
                    _Thai = value;
					GDEDataManager.SetString(_key, ThaiKey, _Thai);
                }
            }
        }

        static string VietnameseKey = "Vietnamese";
		string _Vietnamese;
        public string Vietnamese
        {
            get { return _Vietnamese; }
            set {
                if (_Vietnamese != value)
                {
                    _Vietnamese = value;
					GDEDataManager.SetString(_key, VietnameseKey, _Vietnamese);
                }
            }
        }

        public GDETextTableData(string key) : base(key)
        {
            GDEDataManager.RegisterItem(this.SchemaName(), key);
        }
        public override Dictionary<string, object> SaveToDict()
		{
			var dict = new Dictionary<string, object>();
			dict.Add(GDMConstants.SchemaKey, "TextTable");
			
            dict.Merge(true, Korean.ToGDEDict(KoreanKey));
            dict.Merge(true, English.ToGDEDict(EnglishKey));
            dict.Merge(true, Japanese.ToGDEDict(JapaneseKey));
            dict.Merge(true, ChineseSimplified.ToGDEDict(ChineseSimplifiedKey));
            dict.Merge(true, ChineseTraditional.ToGDEDict(ChineseTraditionalKey));
            dict.Merge(true, French.ToGDEDict(FrenchKey));
            dict.Merge(true, German.ToGDEDict(GermanKey));
            dict.Merge(true, Italian.ToGDEDict(ItalianKey));
            dict.Merge(true, Spanish.ToGDEDict(SpanishKey));
            dict.Merge(true, Russian.ToGDEDict(RussianKey));
            dict.Merge(true, Portugees.ToGDEDict(PortugeesKey));
            dict.Merge(true, Thai.ToGDEDict(ThaiKey));
            dict.Merge(true, Vietnamese.ToGDEDict(VietnameseKey));
            return dict;
		}

        public override void UpdateCustomItems(bool rebuildKeyList)
        {
        }

        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetString(KoreanKey, out _Korean);
                dict.TryGetString(EnglishKey, out _English);
                dict.TryGetString(JapaneseKey, out _Japanese);
                dict.TryGetString(ChineseSimplifiedKey, out _ChineseSimplified);
                dict.TryGetString(ChineseTraditionalKey, out _ChineseTraditional);
                dict.TryGetString(FrenchKey, out _French);
                dict.TryGetString(GermanKey, out _German);
                dict.TryGetString(ItalianKey, out _Italian);
                dict.TryGetString(SpanishKey, out _Spanish);
                dict.TryGetString(RussianKey, out _Russian);
                dict.TryGetString(PortugeesKey, out _Portugees);
                dict.TryGetString(ThaiKey, out _Thai);
                dict.TryGetString(VietnameseKey, out _Vietnamese);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _Korean = GDEDataManager.GetString(_key, KoreanKey, _Korean);
            _English = GDEDataManager.GetString(_key, EnglishKey, _English);
            _Japanese = GDEDataManager.GetString(_key, JapaneseKey, _Japanese);
            _ChineseSimplified = GDEDataManager.GetString(_key, ChineseSimplifiedKey, _ChineseSimplified);
            _ChineseTraditional = GDEDataManager.GetString(_key, ChineseTraditionalKey, _ChineseTraditional);
            _French = GDEDataManager.GetString(_key, FrenchKey, _French);
            _German = GDEDataManager.GetString(_key, GermanKey, _German);
            _Italian = GDEDataManager.GetString(_key, ItalianKey, _Italian);
            _Spanish = GDEDataManager.GetString(_key, SpanishKey, _Spanish);
            _Russian = GDEDataManager.GetString(_key, RussianKey, _Russian);
            _Portugees = GDEDataManager.GetString(_key, PortugeesKey, _Portugees);
            _Thai = GDEDataManager.GetString(_key, ThaiKey, _Thai);
            _Vietnamese = GDEDataManager.GetString(_key, VietnameseKey, _Vietnamese);
        }

        public GDETextTableData ShallowClone()
		{
			string newKey = Guid.NewGuid().ToString();
			GDETextTableData newClone = new GDETextTableData(newKey);

            newClone.Korean = Korean;
            newClone.English = English;
            newClone.Japanese = Japanese;
            newClone.ChineseSimplified = ChineseSimplified;
            newClone.ChineseTraditional = ChineseTraditional;
            newClone.French = French;
            newClone.German = German;
            newClone.Italian = Italian;
            newClone.Spanish = Spanish;
            newClone.Russian = Russian;
            newClone.Portugees = Portugees;
            newClone.Thai = Thai;
            newClone.Vietnamese = Vietnamese;

            return newClone;
		}

        public GDETextTableData DeepClone()
		{
			GDETextTableData newClone = ShallowClone();
            return newClone;
		}

        public void Reset_Korean()
        {
            GDEDataManager.ResetToDefault(_key, KoreanKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(KoreanKey, out _Korean);
        }

        public void Reset_English()
        {
            GDEDataManager.ResetToDefault(_key, EnglishKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(EnglishKey, out _English);
        }

        public void Reset_Japanese()
        {
            GDEDataManager.ResetToDefault(_key, JapaneseKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(JapaneseKey, out _Japanese);
        }

        public void Reset_ChineseSimplified()
        {
            GDEDataManager.ResetToDefault(_key, ChineseSimplifiedKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ChineseSimplifiedKey, out _ChineseSimplified);
        }

        public void Reset_ChineseTraditional()
        {
            GDEDataManager.ResetToDefault(_key, ChineseTraditionalKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ChineseTraditionalKey, out _ChineseTraditional);
        }

        public void Reset_French()
        {
            GDEDataManager.ResetToDefault(_key, FrenchKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(FrenchKey, out _French);
        }

        public void Reset_German()
        {
            GDEDataManager.ResetToDefault(_key, GermanKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(GermanKey, out _German);
        }

        public void Reset_Italian()
        {
            GDEDataManager.ResetToDefault(_key, ItalianKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ItalianKey, out _Italian);
        }

        public void Reset_Spanish()
        {
            GDEDataManager.ResetToDefault(_key, SpanishKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(SpanishKey, out _Spanish);
        }

        public void Reset_Russian()
        {
            GDEDataManager.ResetToDefault(_key, RussianKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(RussianKey, out _Russian);
        }

        public void Reset_Portugees()
        {
            GDEDataManager.ResetToDefault(_key, PortugeesKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(PortugeesKey, out _Portugees);
        }

        public void Reset_Thai()
        {
            GDEDataManager.ResetToDefault(_key, ThaiKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ThaiKey, out _Thai);
        }

        public void Reset_Vietnamese()
        {
            GDEDataManager.ResetToDefault(_key, VietnameseKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(VietnameseKey, out _Vietnamese);
        }

        public void ResetAll()
        {
            GDEDataManager.ResetToDefault(_key, KoreanKey);
            GDEDataManager.ResetToDefault(_key, EnglishKey);
            GDEDataManager.ResetToDefault(_key, JapaneseKey);
            GDEDataManager.ResetToDefault(_key, ChineseSimplifiedKey);
            GDEDataManager.ResetToDefault(_key, ChineseTraditionalKey);
            GDEDataManager.ResetToDefault(_key, FrenchKey);
            GDEDataManager.ResetToDefault(_key, GermanKey);
            GDEDataManager.ResetToDefault(_key, ItalianKey);
            GDEDataManager.ResetToDefault(_key, SpanishKey);
            GDEDataManager.ResetToDefault(_key, RussianKey);
            GDEDataManager.ResetToDefault(_key, PortugeesKey);
            GDEDataManager.ResetToDefault(_key, ThaiKey);
            GDEDataManager.ResetToDefault(_key, VietnameseKey);


            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
